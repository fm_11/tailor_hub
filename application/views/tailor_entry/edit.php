<script type="text/javascript">

    function getAreaByDivisionId(division_id){
        //alert(division_id);
        if (window.XMLHttpRequest)
        {
            xmlhttp = new XMLHttpRequest();
        }
        else
        {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function()
        {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            {
                document.getElementById("area_id").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>tailor_entry/getAreaByDivisionId?division_id=" + division_id, true);
        xmlhttp.send();
    }
</script>
<style>
   .required_class{
     color: red;
   }
</style>
<div class="row">
            <div class="col-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <h5 class="mb-4"><?php echo $title; ?></h5>

                            <form action="<?php echo base_url(); ?>tailor_entry/edit/<?php echo $tailor_info->id;  ?>" method="post" class="needs-validation" novalidate enctype="multipart/form-data">

                              <div class="form-row">
                                  <div class="form-group col-md-6">
                                    <label for="validationTooltip001">Name</label><span class="required_class">*</span>
                                    <input type="text" class="form-control" id="validationTooltip001" required name="name"
                                    value="<?php echo $tailor_info->name;  ?>">
                                    <input type="hidden" class="form-control" id="validationTooltip001" required name="id"
                                    value="<?php echo $tailor_info->id;  ?>">
                                 </div>
                                    <div class="form-group col-md-6">
                                        <label for="inputPassword4">Tailor ID</label><span class="required_class">*</span>
                                        <input type="text" readonly class="form-control" id="txtCode" required
                                            name="code" value="<?php echo $tailor_info->code;  ?>">
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="branches">Division</label><span class="required_class">*</span>
                                        <select  onchange="getAreaByDivisionId(this.value)" class="form-control select2-single"  name="division_id" required>
                                            <option  value="">--Select--</option>
                                            <?php
                                            if (count($divisions)) {
                                                foreach ($divisions as $list) {
                                                  ?>
                                                  <option
                                                      <?php if ($list['id'] == $tailor_info->division_id) {
                                                      echo 'selected';
                                                  } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                                  <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>

                                    <div class="form-group col-md-4">
                                            <label for="DOB">Area</label><span class="required_class">*</span>
                                            <select class="form-control select2-single" id="area_id"  name="area_id" required>
                                                <option value="">--Select--</option>
                                                <?php
                                                if (count($areas)) {
                                                    foreach ($areas as $list) {
                                                      ?>
                                                      <option
                                                          <?php if ($list['id'] == $tailor_info->area_id) {
                                                          echo 'selected';
                                                      } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                                      <?php
                                                    }
                                                }
                                                ?>

                                            </select>
                                      </div>
                                      <div class="form-group col-md-4">
                                          <label for="Country">Gender</label>
                                          <div class="panel-body">
                                                <label class="radio-inline">
                                                  <div class="custom-control custom-radio">
                                                      <input type="radio" id="customRadio1" name="txtGender"
                                                         value="M" <?php if($tailor_info->gender == 'M'){ echo 'checked'; } ?> required class="custom-control-input">
                                                      <label class="custom-control-label" for="customRadio1">Male</label>
                                                  </div>
                                                </label>
                                                <label class="radio-inline">
                                                  <div class="custom-control custom-radio">
                                                      <input type="radio" id="customRadio2" name="txtGender"
                                                        value="F" <?php if($tailor_info->gender == 'F'){ echo 'checked'; } ?> class="custom-control-input">
                                                      <label class="custom-control-label" for="customRadio2">Female</label>
                                                  </div>
                                                </label>
                                          </div>
                                      </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="txtMobile">Location</label><span class="required_class">*</span>
                                        <input type="text" class="form-control" name="location" value="<?php echo $tailor_info->location;  ?>" required id="txtMobile">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="inputPassword4">Rating</label><span class="required_class">*</span>
                                        <input type="number" class="form-control" id="rating" required min="0.5" max="5" step="any"  name="rating" value="<?php echo $tailor_info->rating;  ?>">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="txtProcessCode">Cancel Policy</label><span class="required_class">*</span>
                                        <input type="text" class="form-control"  name="tailor_cancel_policy" value="<?php echo $tailor_info->tailor_cancel_policy;  ?>" id="tailor_cancel_policy">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="txtMobile">Property Highlights</label><span class="required_class">*</span>
                                        <input type="text" class="form-control" name="property_highlights" value="<?php echo $tailor_info->property_highlights;  ?>" required id="property_highlights">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="inputPassword4">Establishment Year</label><span class="required_class">*</span>
                                        <input type="number" class="form-control" id="establishment_year" required  name="establishment_year" value="<?php echo $tailor_info->establishment_year;  ?>">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="txtProcessCode">Payment Method</label><span class="required_class">*</span>
                                        <input type="text" class="form-control"  name="payment_method" value="<?php echo $tailor_info->payment_method;  ?>" id="payment_method">
                                    </div>
                                </div>
                                <div class="form-row">
                                  <div class="form-group col-md-4">
                                          <label for="Timezone">Currencies</label>
                                          <select class="form-control select2-single"  name="cc_currency_id">
                                              <option value="">--Select--</option>
                                              <?php
                                              if (count($currencies)) {
                                                  foreach ($currencies as $list) {
                                                    ?>
                                                    <option
                                                        <?php if ($list['id'] == $tailor_info->cc_currency_id) {
                                                        echo 'selected';
                                                    } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                                    <?php
                                                  }
                                              }
                                              ?>

                                          </select>
                                  </div>
                                    <div class="form-group col-md-4">
                                        <label for="Timezone">Shop Opening Time</label><span class="required_class">*</span>
                                        <input type="time" autocomplete="off"  class="form-control" id="shop_opening_time"  name="shop_opening_time" value="<?php echo $tailor_info->shop_opening_time;  ?>">
                                    </div>
                                    <div class="form-group col-md-4">
                                            <label for="Timezone">Shop Closing Time</label><span class="required_class">*</span>
                                            <input type="time" autocomplete="off"  class="form-control" id="shop_closing_time"  name="shop_closing_time" value="<?php echo $tailor_info->shop_closing_time;  ?>">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="txtMobile">Contact Person Name</label><span class="required_class">*</span>
                                        <input type="text" class="form-control" name="contact_person_name" value="<?php echo $tailor_info->contact_person_name;  ?>" required>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="inputPassword4">Contact Person Email</label><span class="required_class">*</span>
                                        <input type="emial" class="form-control"  required  name="contact_person_email" value="<?php echo $tailor_info->contact_person_email;  ?>">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="txtProcessCode">Contact Person Phone</label><span class="required_class">*</span>
                                        <input type="text" class="form-control"  name="contact_person_phone" value="<?php echo $tailor_info->contact_person_phone;  ?>">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="txtMobile">Contact Person Address</label><span class="required_class">*</span>
                                        <textarea name="contact_person_adrres" required class="form-control"><?php echo $tailor_info->contact_person_adrres;  ?></textarea>
                                    </div>
                                </div>
                                <div class="form-row">
                                  <div class="form-group col-md-4">
                                      <label for="inputPassword4">Image 1</label>
                                      <br>
                                    <?php if(!empty($tailor_info->slider_image_1)){?>
                                    <img style="width: 100px;height:100px;" src="<?php echo base_url(); ?>media/website/images/<?php echo $tailor_info->slider_image_1;?>" />
                                  <?php }?>
                                   <input type="hidden" name="oldtxtPhoto1"  value="<?php echo $tailor_info->slider_image_1;?>">
                                   <input type="file" class="form-control"  name="txtPhoto1"  value="">
                                  </div>
                                    <div class="form-group col-md-4">
                                      <label for="inputPassword4">Image 2</label>
                                      <br>
                                    <?php if(!empty($tailor_info->slider_image_2)){?>
                                    <img style="width: 100px;height:100px;" src="<?php echo base_url(); ?>media/website/images/<?php echo $tailor_info->slider_image_2;?>" />
                                  <?php }?>
                                   <input type="hidden" name="oldtxtPhoto2"  value="<?php echo $tailor_info->slider_image_2;?>">
                                      <input type="file" class="form-control"  name="txtPhoto2"  value="">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="inputPassword4">Image 3</label>
                                        <br>
                                      <?php if(!empty($tailor_info->slider_image_3)){?>
                                      <img style="width: 100px;height:100px;" src="<?php echo base_url(); ?>media/website/images/<?php echo $tailor_info->slider_image_3;?>" />
                                    <?php }?>
                                     <input type="hidden" name="oldtxtPhoto3"  value="<?php echo $tailor_info->slider_image_3;?>">
                                        <input type="file" class="form-control"  name="txtPhoto3"  value="">
                                    </div>

                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                      <label for="inputPassword4">Image 4</label>
                                      <br>
                                    <?php if(!empty($tailor_info->slider_image_4)){?>
                                    <img style="width: 100px;height:100px;" src="<?php echo base_url(); ?>media/website/images/<?php echo $tailor_info->slider_image_4;?>" />
                                  <?php }?>
                                   <input type="hidden" name="oldtxtPhoto4"  value="<?php echo $tailor_info->slider_image_4;?>">
                                      <input type="file" class="form-control"  name="txtPhoto4"  value="">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="inputPassword4">Image 5</label>
                                        <br>
                                      <?php if(!empty($tailor_info->slider_image_5)){?>
                                      <img style="width: 100px;height:100px;" src="<?php echo base_url(); ?>media/website/images/<?php echo $tailor_info->slider_image_5;?>" />
                                    <?php }?>
                                     <input type="hidden" name="oldtxtPhoto5"  value="<?php echo $tailor_info->slider_image_5;?>">
                                        <input type="file" class="form-control"  name="txtPhoto5"  value="">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="inputPassword4">Image 6</label>
                                        <br>
                                      <?php if(!empty($tailor_info->slider_image_6)){?>
                                      <img style="width: 100px;height:100px;" src="<?php echo base_url(); ?>media/website/images/<?php echo $tailor_info->slider_image_6;?>" />
                                    <?php }?>
                                     <input type="hidden" name="oldtxtPhoto6"  value="<?php echo $tailor_info->slider_image_6;?>">
                                        <input type="file" class="form-control"  name="txtPhoto6"  value="">
                                    </div>
                                  </div>
                                <div class="form-row">
                                  <div class="form-group col-md-4">
                                      <label for="inputPassword4">Shop Amenities </label>
                                      <table>
                                        <tr>
                                          <th style="width:40%;">Name</th>
                                          <th style="width:20%;text-align:right;">Active</th>
                                        </tr>
                                        <?php   (int)$row=0; foreach ($shop_amenities as $list): ?>
                                          <tr>
                                          <td>
                                            <span class="from-control"><?php echo $list['name']; ?></span>
                                            <input type="hidden"
                                                   name="shop_amenities[<?php echo $row;?>][id]" value="<?php echo $list['id']; ?>">
                                          </td>
                                          <td style="text-align:right;">
                                            <input type="checkbox"
                                                   name="shop_amenities[<?php echo $row; ?>][active]" <?php if($list['active']){echo  'checked'; }?> value="1" >
                                          </td>
                                        </tr>
                                       <?php $row++; ?>
                                        <?php endforeach; ?>
                                      </table>
                                  </div>
                                  <div class="form-group col-md-4">
                                      <label for="inputPassword4">Items </label>
                                      <table>
                                        <tr>
                                          <th style="width:40%;">Name</th>
                                          <th style="width:40%;">Price</th>
                                          <th style="width:20%;text-align:right;">Active</th>
                                        </tr>
                                        <?php   (int)$row=0; foreach ($items as $list): ?>
                                          <tr>
                                          <td>
                                            <span class="from-control"><?php echo $list['name']; ?></span>
                                            <input type="hidden"
                                                   name="items[<?php echo $row;?>][id]" value="<?php echo $list['id']; ?>">
                                          </td>
                                          <td style="text-align:right;">
                                            <input type="number"
                                                   name="items[<?php echo $row; ?>][price]" step="any"  value="<?php echo $list['price']; ?>" >
                                          </td>
                                          <td style="text-align:right;">
                                            <input type="checkbox"
                                                   name="items[<?php echo $row; ?>][active]" <?php if($list['active']){echo  'checked'; }?> value="1" >
                                          </td>
                                        </tr>
                                       <?php $row++; ?>
                                        <?php endforeach; ?>
                                      </table>
                                  </div>
                                    <div class="form-group col-md-4">
                                          <label for="Timezone">Photo</label><span class="required_class">*</span>
                                          <br>
                                          <input type="hidden" name="old_image_path" value="<?php echo $tailor_info->image_path; ?>"/>
                                          <img src="<?php echo base_url(); ?>media/tailor/<?php echo $tailor_info->image_path; ?>" style=" border:1px solid #666666;" width="150" height="150">
                                          <br><br>
                                          <input type="file" id="inputImage" name="txtPhoto"
                                              accept=".jpg,.jpeg,.png,.gif,.bmp,.tiff">
                                      </div>
                                </div>

                                <div class="form-row">
                                   <div class="form-group col-md-8">
                                      <label for="inputEmail4">About<span class="required_class">*</span></label><br>
                                       <textarea class="form-control" id="module_subject" name="about"><?php echo $tailor_info->about; ?></textarea>
                                   </div>
                                  </div>

                                <button type="submit" class="btn btn-primary d-block mt-3">Update</button>
                    </form>
            </div>
       </div>
      </div>
</div>
