<script type="text/javascript">

    function getAreaByDivisionId(division_id){
        //alert(division_id);
        if (window.XMLHttpRequest)
        {
            xmlhttp = new XMLHttpRequest();
        }
        else
        {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function()
        {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            {
                document.getElementById("area_id").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>tailor_entry/getAreaByDivisionId?division_id=" + division_id, true);
        xmlhttp.send();
    }
</script>
<style>
   .required_class{
     color: red;
   }
</style>

<div class="row">
            <div class="col-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <h5 class="mb-4"><?php echo $title; ?></h5>

                            <form action="<?php echo base_url(); ?>tailor_entry/add" method="post" class="needs-validation" novalidate enctype="multipart/form-data">
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                      <label for="validationTooltip001">Name</label><span class="required_class">*</span>
                                      <input type="text" class="form-control" id="validationTooltip001" required name="txtName"
                                      value="">
                                   </div>
                                      <div class="form-group col-md-6">
                                          <label for="inputPassword4">Tailor ID</label><span class="required_class">*</span>
                                          <input type="text" <?php if($is_employee_id_auto == '1'){ echo 'readonly'; } ?> class="form-control" id="txtCode" required
                                              name="txtEmployeeId" value="">
                                      </div>
                                  </div>

                                  <div class="form-row">
                                      <div class="form-group col-md-4">
                                          <label for="branches">Division</label><span class="required_class">*</span>
                                          <select  onchange="getAreaByDivisionId(this.value)" class="form-control select2-single"  name="division_id" required>
                                              <option  value="">--Select--</option>
                                              <?php

                                              if (count($divisions)) {
                                                  foreach ($divisions as $list) {

                                                      ?>
                                                      <option
                                                          value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                                  <?php
                                                  }
                                              }
                                              ?>
                                          </select>
                                      </div>

                                      <div class="form-group col-md-4">
                                              <label for="DOB">Area</label><span class="required_class">*</span>
                                              <select class="form-control select2-single" id="area_id"  name="area_id" required>
                                                  <option value="">--Select--</option>
                                                  <?php

                                                  if (count($areas)) {
                                                      foreach ($areas as $list) {

                                                          ?>
                                                          <option
                                                              value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                                      <?php
                                                      }
                                                  }
                                                  ?>
                                              </select>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="Country">Gender</label><span class="required_class">*</span>
                                              <div class="panel-body">
                                                    <label class="radio-inline">
                                                      <div class="custom-control custom-radio">
                                                          <input type="radio" id="customRadio1" name="txtGender"
                                                             value="M" required class="custom-control-input">
                                                          <label class="custom-control-label" for="customRadio1">Male</label>
                                                      </div>
                                                    </label>

                                                    <label class="radio-inline">
                                                      <div class="custom-control custom-radio">
                                                          <input type="radio" id="customRadio2" name="txtGender"
                                                            value="F"  class="custom-control-input">
                                                          <label class="custom-control-label" for="customRadio2">Female</label>
                                                      </div>
                                                    </label>
                                              </div>
                                        </div>
                                  </div>
                                  <div class="form-row">
                                      <div class="form-group col-md-4">
                                          <label for="txtMobile">Location</label><span class="required_class">*</span>
                                          <input type="text" class="form-control" name="location" value="" required id="txtMobile">
                                      </div>
                                      <div class="form-group col-md-4">
                                          <label for="inputPassword4">Rating</label><span class="required_class">*</span>
                                          <input type="number" min="0.5" max="5" step="any" class="form-control" id="rating" required  name="rating" value="">
                                      </div>
                                      <div class="form-group col-md-4">
                                          <label for="txtProcessCode">Cancel Policy</label><span class="required_class">*</span>
                                          <input type="text" class="form-control"  name="tailor_cancel_policy" value="" id="tailor_cancel_policy">
                                      </div>
                                  </div>
                                  <div class="form-row">
                                      <div class="form-group col-md-4">
                                          <label for="txtMobile">Property Highlights</label><span class="required_class">*</span>
                                          <input type="text" class="form-control" name="property_highlights" value="" required id="property_highlights">
                                      </div>
                                      <div class="form-group col-md-4">
                                          <label for="inputPassword4">Establishment Year</label><span class="required_class">*</span>
                                          <input type="number" class="form-control" id="establishment_year" required  name="establishment_year" value="">
                                      </div>
                                      <div class="form-group col-md-4">
                                          <label for="txtProcessCode">Payment Method</label><span class="required_class">*</span>
                                          <input type="text" class="form-control"  name="payment_method" value="" id="payment_method">
                                      </div>
                                  </div>
                                  <div class="form-row">
                                    <div class="form-group col-md-4">
                                            <label for="Timezone">Currencies</label>
                                            <select class="form-control select2-single"  name="cc_currency_id">
                                                <option value="">--Select--</option>
                                                <?php
                                                if (count($currencies)) {
                                                    foreach ($currencies as $list) {
                                                        ?>
                                                        <option
                                                            value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                                    <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                    </div>
                                      <div class="form-group col-md-4">
                                          <label for="Timezone">Shop Opening Time</label><span class="required_class">*</span>
                                          <input type="time" autocomplete="off"  class="form-control" id="shop_opening_time"  name="shop_opening_time" value="">
                                      </div>
                                      <div class="form-group col-md-4">
                                              <label for="Timezone">Shop Closing Time</label><span class="required_class">*</span>
                                              <input type="time" autocomplete="off"  class="form-control" id="shop_closing_time"  name="shop_closing_time" value="">
                                      </div>
                                  </div>
                                  <div class="form-row">
                                      <div class="form-group col-md-4">
                                          <label for="txtMobile">Contact Person Name</label><span class="required_class">*</span>
                                          <input type="text" class="form-control" name="contact_person_name" value="" required>
                                      </div>
                                      <div class="form-group col-md-4">
                                          <label for="inputPassword4">Contact Person Email</label><span class="required_class">*</span>
                                          <input type="emial" class="form-control"  required  name="contact_person_email" value="">
                                      </div>
                                      <div class="form-group col-md-4">
                                          <label for="txtProcessCode">Contact Person Phone</label><span class="required_class">*</span>
                                          <input type="text" class="form-control"  name="contact_person_phone" value="">
                                      </div>
                                  </div>
                                  <div class="form-row">
                                      <div class="form-group col-md-12">
                                          <label for="txtMobile">Contact Person Address</label><span class="required_class">*</span>
                                          <textarea name="contact_person_adrres" class="form-control" required></textarea>
                                      </div>
                                  </div>
                                  <div class="form-row">
                                    <div class="form-group col-md-4">
                                      <label for="inputPassword4">Image 1</label>
                                      <input type="file" class="form-control"  name="txtPhoto1"  value="">
                                    </div>
                                    <div class="form-group col-md-4">
                                      <label for="inputPassword4">Image 2</label>
                                      <input type="file" class="form-control"  name="txtPhoto2"  value="">
                                    </div>
                                    <div class="form-group col-md-4">
                                      <label for="inputPassword4">Image 3</label>
                                      <input type="file" class="form-control"  name="txtPhoto3"  value="">
                                    </div>
                                  </div>
                                  <div class="form-row">
                                    <div class="form-group col-md-4">
                                      <label for="inputPassword4">Image 4</label>
                                      <input type="file" class="form-control"  name="txtPhoto4"  value="">
                                    </div>
                                    <div class="form-group col-md-4">
                                      <label for="inputPassword4">Image 5</label>
                                      <input type="file" class="form-control"  name="txtPhoto5"  value="">
                                    </div>
                                    <div class="form-group col-md-4">
                                      <label for="inputPassword4">Image 6</label>
                                      <input type="file" class="form-control"  name="txtPhoto6"  value="">
                                    </div>
                                  </div>
                                  <div class="form-row">
                                      <div class="form-group col-md-12">
                                          <label for="txtMobile">Remarks</label>
                                          <textarea name="remarks" class="form-control" maxlength="200"></textarea>
                                      </div>

                                  </div>
                                  <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="inputPassword4">Shop Amenities </label>
                                        <table>
                                          <tr>
                                            <th style="width:40%;">Name</th>
                                            <th style="width:20%;text-align:right;">Active</th>
                                          </tr>
                                          <?php   (int)$row=0; foreach ($shop_amenities as $list): ?>
                                            <tr>
                                            <td>
                                              <span class="from-control"><?php echo $list['name']; ?></span>
                                              <input type="hidden"
                                                     name="shop_amenities[<?php echo $row;?>][id]" value="<?php echo $list['id']; ?>">
                                            </td>
                                            <td style="text-align:right;">
                                              <input type="checkbox"
                                                     name="shop_amenities[<?php echo $row; ?>][active]"  value="1" >
                                            </td>
                                          </tr>
                                         <?php $row++; ?>
                                          <?php endforeach; ?>
                                        </table>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="inputPassword4">Items </label>
                                        <table>
                                          <tr>
                                            <th style="width:40%;">Name</th>
                                              <th style="width:40%;">Price</th>
                                            <th style="width:20%;text-align:right;">Active</th>
                                          </tr>
                                          <?php   (int)$row=0; foreach ($items as $list): ?>
                                            <tr>
                                            <td>
                                              <span class="from-control"><?php echo $list['name']; ?></span>
                                              <input type="hidden"
                                                     name="items[<?php echo $row;?>][id]" value="<?php echo $list['id']; ?>">
                                            </td>
                                            <td style="text-align:right;">
                                              <input type="number"
                                                     name="items[<?php echo $row; ?>][price]" step="any"  value="" >
                                            </td>
                                            <td style="text-align:right;">
                                              <input type="checkbox"
                                                     name="items[<?php echo $row; ?>][active]"  value="1" >
                                            </td>
                                          </tr>
                                         <?php $row++; ?>
                                          <?php endforeach; ?>
                                        </table>
                                    </div>
                                      <div class="form-group col-md-4">
                                            <label for="Timezone">Photo</label><span class="required_class">*</span>
                                            <input type="file" class="form-control"  id="inputImage" required name="txtPhoto"
                                                accept=".jpg,.jpeg,.png,.gif,.bmp,.tiff">
                                        </div>
                                  </div>
                                  <div class="form-row">
                                     <div class="form-group col-md-8">
                                        <label for="inputEmail4">About<span class="required_class">*</span></label><br>
                                         <textarea class="form-control" id="module_subject" name="about"></textarea>
                                     </div>
                                    </div>
                                <button type="submit" class="btn btn-primary d-block mt-3">Submit</button>
                    </form>
            </div>
       </div>
      </div>
</div>
