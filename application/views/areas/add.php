<style>
   .required_class{
     color: red;
   }
</style>
<div class="row">
            <div class="col-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <h5 class="mb-4"><?php echo $title; ?> <small style="color: red;">All Red marked are required</small></h5>

                            <form action="<?php echo base_url(); ?>areas/add" method="post" enctype="multipart/form-data">
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                      <label for="inputEmail4">Name<span style="color:red;">*</span></label>
                                      <input type="text" class="form-control" id="name" required name="name" value="" placeholder="Division Name">
                                   </div>
                                   <div class="form-group col-md-6">
                                       <label for="branches">Division<span style="color:red;">*</span></label>
                                       <select class="form-control select2-single"  name="division_id" required>
                                           <option value="">--Select--</option>
                                           <?php

                                           if (count($divisions)) {
                                               foreach ($divisions as $list) {

                                                   ?>
                                                   <option
                                                       value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                               <?php
                                               }
                                           }
                                           ?>
                                       </select>
                                   </div>
                                  </div>

                                  <div class="form-row">
                                       <div class="form-group col-md-6">
                                         <label for="Timezone">Photo</label><span class="required_class">*</span>
                                         <input type="file" class="form-control"  id="inputImage" required name="txtPhoto"
                                             accept=".jpg,.jpeg,.png,.gif,.bmp,.tiff">
                                       </div>
                                  </div>

                                <button type="submit" class="btn btn-primary d-block mt-3">Submit</button>
                    </form>
            </div>
</div>
</div>
</div>
