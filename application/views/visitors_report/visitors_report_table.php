<hr>
<style>
.my-custom-scrollbar {
/* position: relative;
height: 500px;
overflow: auto; */
}
.table-wrapper-scroll-y {
display: block;
}
</style>
<?php
if(isset($excel)){
  header('Content-type: application/excel');
  $filename = 'Visitors Report.xls';
  header('Content-Disposition: attachment; filename='.$filename);
}
?>
<div class="table-wrapper-scroll-y my-custom-scrollbar">
<table class="table table-responsive">
    <thead>
      <tr>
          <th  class="text-center" colspan="6" scope="col">
            <h3>
               <?php echo $org_info['org_name']; ?>
            </h3>
            <p><?php echo $org_info['address']; ?></p>
            <p><?php echo $org_info['email']; ?></p>
            <p>Visitors Report</p>
            <p>Branch: <?php echo $org_info['branch_name']; ?></p>
          </th>
      </tr>
      <tr>
            <th scope="col" class="text-left" colspan="">Employee Name</th>
            <td scope="col" class="text-left" colspan=""><?php echo $employee_info[0]['name']; ?></td>
     </tr>
     <tr>
           <th scope="col" class="text-left" colspan="">Employee Code</th>
           <td scope="col" class="text-left" colspan=""><?php echo $employee_info[0]['code']; ?></td>
    </tr>
    <tr>
          <th scope="col" class="text-left" colspan="">Designation</th>
          <td scope="col" class="text-left" colspan=""><?php echo $employee_info[0]['designation']; ?></td>
   </tr>
   <tr>
         <th scope="col" class="text-left" colspan="">Report Period</th>
         <td scope="col" class="text-left" colspan=""><?php echo $from_date; ?> To <?php echo $to_date; ?></td>
  </tr>
  <tr>
        <th scope="col" class="text-left" colspan="">Total Day</th>
        <td scope="col" class="text-left" colspan=""><?php echo $total_day; ?></td>
 </tr>
 <tr>
       <th scope="col" class="text-left" colspan="">Report To</th>
       <td scope="col" class="text-left" colspan=""><?php echo $report_to[0]['designation']; ?></td>
</tr>
<tr>
      <th scope="col">No Of Student Visited</th>

</tr>
      <tr>
            <th scope="col">#</th>
            <th scope="col">Country</th>
            <th scope="col">No. Of Student</th>

     </tr>
    </thead>
    <tbody>
      <?php
      $i = 0;
      foreach ($rDataCountry as $row):
          $i++;
          ?>
        <tr>
            <th scope="row"><?php echo $i; ?></th>
            <td><?php echo $row['name']; ?></td>
            <td><?php echo $row['student']; ?></td>


        </tr>
      <?php endforeach; ?>
      <tr>
            <th colspan="3">List of Student Visit From Media Netork</th>

     </tr>
     <tr>
           <th scope="col">#</th>
           <th scope="col">Country</th>
           <th scope="col">No. Of Student</th>

    </tr>
     <?php
     $j = 0;
     $total=0;
     foreach ($rDataLead as $row):
         $j++;
         $total=$total+$row['student'];
         ?>
       <tr>
           <th scope="row"><?php echo $j; ?></th>
           <td><?php echo $row['name']; ?></td>
           <td><?php echo $row['student']; ?></td>
       </tr>
     <?php endforeach; ?>
     <tr>

           <td colspan="2">Total : </td>
           <td colspan=""><?php echo $total; ?></td>
    </tr>
      <tr>
            <th colspan=""></th>
            <td colspan=""></td>
            <td colspan=""></td>
     </tr>
     <tr>
           <th colspan="2" style="height: 150px;">Coments on Provided SupportComents on Provided Support</th>
           <th colspan="">Overall Comments</th>

    </tr>
     <tr>
           <th colspan=""><?php echo $printed_by[0]['name']; ?></th>
           <th  class="text-left" colspan=""> </th>
           <td colspan=""></td>
    </tr>
    <tr>

          <th  class="text-left" colspan=""><span style="border-top: 1px solid #3a2626;">PREPARED BY</span> </th>
          <th scope="col" class="text-left" colspan=""><span style="border-top: 1px solid #3a2626;">Varified By </span></th>
          <th scope="col" class="text-left" colspan=""><span style="border-top: 1px solid #3a2626;">Approved By </span></th>


   </tr>
   <tr>
         <th scope="col" class="text-left" colspan=""><?php echo$printed_by[0]['designation_name']; ?> </th>
         <th scope="col" class="text-left" colspan="">Head of Engagement</th>
         <th scope="col" class="text-left" colspan=""><?php echo $employee_info[0]['reporting_boss_designation']; ?></th>

  </tr>

    </tbody>
</table>
</div>
