<div class="row">
            <div class="col-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <h5 class="mb-4"><?php echo $title; ?></h5>

                            <form action="<?php echo base_url(); ?>notices/edit" method="post" enctype="multipart/form-data">
                              <div class="form-group">
                                <label for="inputTitle">Title</label>
                                <input type="text" class="form-control" id="title" required name="title" value="<?php echo $notice_info[0]['title']; ?>">
                                <input type="hidden" class="form-control" id="id" required name="id" value="<?php echo $notice_info[0]['id']; ?>" >
                             </div>

                             <div class="form-group">
                               <label for="inputDescription">Description</label>
                               <textarea class="form-control" id="description" required name="description" ><?php echo $notice_info[0]['description']; ?></textarea>
                            </div>

                             <div class="form-group">
                               <label for="inputFile">File</label>
                               <input type="file" id="inputImage" class="form-control"  name="txtFile">
                             </div>

                                <button type="submit" class="btn btn-primary d-block mt-3">Submit</button>
                    </form>
            </div>
</div>
</div>
</div>
