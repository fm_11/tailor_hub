<div class="row">
            <div class="col-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <h5 class="mb-4"><?php echo $title; ?> <small style="color: red;">All Red marked are required</small></h5>

                            <form action="<?php echo base_url(); ?>designations/edit" method="post" enctype="multipart/form-data">
                              <div class="form-group">
                                <label for="inputEmail4">Name<span style="color:red;">*</span></label>
                                <input type="text" class="form-control" id="name" required name="name" value="<?php echo $designation->name;  ?>" placeholder="Department Name">
                                  <input type="hidden" name="id"  class="smallInput wide" value="<?php echo $designation->id;  ?>">
                             </div>

                             <div class="form-group">
                               <label for="inputEmail4">Number of Post<span style="color:red;">*</span></label>
                               <input type="text" class="form-control" id="num_of_post" required name="num_of_post" value="<?php echo $designation->num_of_post;  ?>" placeholder="Department Name">

                            </div>

                            <div class="form-group">
                              <label for="inputEmail4">Shorting Order<span style="color:red;">*</span></label>
                              <input type="text" class="form-control" id="shorting_order" required name="shorting_order" value="<?php echo $designation->shorting_order;  ?>" placeholder="Department Name">

                           </div>

                                <button type="submit" class="btn btn-primary d-block mt-3">Update</button>
                    </form>
            </div>
</div>
</div>
</div>
