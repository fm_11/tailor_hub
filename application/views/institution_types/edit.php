<div class="row">
    <div class="col-12">
        <div class="card mb-4">
            <div class="card-body">
                <h5 class="mb-4"><?php echo $title; ?></h5>

                <form action="<?php echo base_url(); ?>Institution_types/edit" method="post" enctype="multipart/form-data">
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="inputEmail4">Name</label>
                            <input type="text" class="form-control" id="name" required name="name" value="<?php echo $institution_types->name;  ?>" placeholder="Name">
                            <input type="hidden" name="id"  class="smallInput wide" value="<?php echo $institution_types->id;  ?>">
                        </div>
                        <!-- <div class="form-group col-md-6">
                            <label for="inputPassword4">Code</label>
                            <input type="text" class="form-control" id="code" required
                                   name="code" value="<?php echo $institution_types->code;  ?>">
                        </div> -->
                    </div>

                    <button type="submit" class="btn btn-primary d-block mt-3">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
