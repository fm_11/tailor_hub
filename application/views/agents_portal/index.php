<div class="col-12 mb-4">
    <div class="card">
        <div class="card-body">
            <h5 class="card-title"><?php echo $title; ?></h5>

            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Company Name</th>
                    <th scope="col">Person Name</th>
                    <th scope="col">Mobile</th>
                    <th scope="col">Email</th>
                    <th scope="col">Contact Person</th>
                    <th scope="col">Allocate Branch</th>
                    <th scope="col">Comission</th>
                    <th scope="col">Date</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $i = (int)$this->uri->segment(3);
                foreach ($agents_portal as $row):
                    $i++;
                    ?>
                    <tr>
                        <th scope="row"><?php echo $i; ?></th>
                        <td><?php echo $row['company_name']; ?></td>
                        <td><?php echo $row['principal_name']; ?></td>
                        <td><?php echo $row['mobile']; ?></td>
                        <td><?php echo $row['email']; ?></td>
                        <td><?php echo $row['contact_person_id']; ?></td>
                        <td><?php echo $row['allocate_branch_id']; ?></td>
                        <td><?php echo $row['comission']; ?></td>
                        <td><?php echo $row['date']; ?></td>


                        <td>

                            <a href="<?php echo base_url(); ?>agents_portal/edit/<?php echo $row['id']; ?>"
                               title="Edit">
                                <button type="button" class="btn btn-primary btn-xs mb-1">
                                    Edit
                                </button>
                            </a>

                            <a href="<?php echo base_url(); ?>agents_portal/delete/<?php echo $row['id']; ?>"
                               onclick="return deleteConfirm()" title="Delete">
                                <button type="button" class="btn btn-primary btn-xs mb-1">
                                    Delete
                                </button>

                            </a>

                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <?php echo $this->pagination->create_links(); ?>
        </div>
    </div>
</div>




