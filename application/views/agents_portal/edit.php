<script type="text/javascript">
    function getCityByCountryId(city_id){
        //alert(city_id);
        if (window.XMLHttpRequest)
        {
            xmlhttp = new XMLHttpRequest();
        }
        else
        {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function()
        {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            {
                document.getElementById("city_id").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>institutions/getCityByCountryId?city_id=" + city_id, true);
        xmlhttp.send();
    }

</script>


<div class="row">
    <div class="col-12">
        <div class="card mb-4">
            <div class="card-body">
                <h5 class="mb-4"><?php echo $title; ?></h5>

                <form action="<?php echo base_url(); ?>agents_portal/edit" method="post" enctype="multipart/form-data">

                    <input type="hidden" class="form-control" id="id"
                           name="id" value="<?php echo $agents_portal->id;  ?>">
                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="inputPassword4">Company Name</label>
                            <input type="text" class="form-control" id="company_name"
                                   name="company_name" value="<?php echo $agents_portal->company_name;  ?>">

                        </div>
                        <div class="form-group col-md-3">
                            <label for="inputPassword4">Street Address</label>
                            <input type="text" class="form-control" id="street_address"
                                   name="street_address" value="<?php echo $agents_portal->street_address; ?>">

                        </div>
                        <div class="form-group col-md-3">
                            <label for="inputEmail4">Country</label>
                            <select onchange="getCityByCountryId(this.value)" class="form-control select2-single" required name="country_id">
                                <option value="">--Select--</option>
                                <?php

                                if (count($countries)) {
                                    foreach ($countries as $list) {

                                        ?>
                                        <option
                                            <?php if($list['id'] == $agents_portal->country_id){ echo 'selected'; } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="inputPassword4">City</label>
                            <select name="city_id" id="city_id" required class="form-control select2-single">
                                <option value="">Select</option>
                                <?php
                                if (count($country_cities)) {
                                    foreach ($country_cities as $list) {
                                        ?>
                                        <option
                                            <?php if($list['id'] == $agents_portal->city_id){ echo 'selected'; } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>

                            </select>
                        </div>

                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="inputPassword4">Postal Code</label>
                            <input type="text" class="form-control" id="postal_code"
                                   name="postal_code" value="<?php echo $agents_portal->postal_code; ?>">

                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputPassword4">Phone</label>
                            <input type="number" class="form-control" id="phone"
                                   name="phone" value="<?php echo $agents_portal->phone; ?>">

                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputPassword4">Mobile</label>
                            <input type="number" class="form-control" id="mobile"
                                   name="mobile" value="<?php echo $agents_portal->mobile; ?>">

                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="inputPassword4">Website</label>
                            <input type="text" class="form-control" id="website"
                                   name="website" value="<?php echo $agents_portal->website; ?>">

                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputPassword4">Skype</label>
                            <input type="text" class="form-control" id="skype"
                                   name="skype" value="<?php echo $agents_portal->skype; ?>">

                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputPassword4">Comission</label>
                            <input type="text" class="form-control" id="comission"
                                   name="comission" value="<?php echo $agents_portal->comission; ?>">

                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="inputAddress">Upload Logo (120 x 120)</label><br>
                            <input type="file" id="logo_image_path" name="logo_image_path"
                                   accept=".jpg,.jpeg,.png,.gif,.bmp,.tiff">
                        </div>
                        <div class="form-group col-md-4">
                            <div class="radio">
                                <label><input type="radio" name="status" <?php if($agents_portal->status){ echo 'checked'; }  ?> value="1">Active</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="status" <?php if($agents_portal->status){ echo 'checked'; }  ?> value="2">Inactive</label>
                            </div>
                        </div>
                    </div>

                    <hr>
                    <h2>Principal Officer</h2>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Name</label>
                            <input type="text" class="form-control" id="principal_name"
                                   name="principal_name" value="<?php echo $agents_portal->principal_name; ?>">

                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Position</label>
                            <input type="text" class="form-control" id="principal_position"
                                   name="principal_position" value="<?php echo $agents_portal->principal_position; ?>">

                        </div>

                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Phone</label>
                            <input type="principal_phone" class="form-control" id="skype"
                                   name="principal_phone" value="<?php echo $agents_portal->principal_phone; ?>">

                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Email</label>
                            <input type="text" class="form-control" id="email"
                                   name="email" value="<?php echo $agents_portal->email; ?>">

                        </div>
                    </div>

                    <hr>
                    <h2>Allocate</h2>

                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="inputPassword4">Contact Person</label>
                            <select onchange="" class="form-control select2-single" required name="contact_person_id">
                                <option value="">--Select--</option>
                                <?php

                                if (count($employee)) {
                                    foreach ($employee as $list) {

                                        ?>
                                        <option
                                            <?php if($list['id'] == $agents_portal->contact_person_id){ echo 'selected'; } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>

                        </div>
                        <div class="form-group col-md-3">
                            <label for="inputPassword4">Branch</label>
                            <select onchange="" class="form-control select2-single" required name="allocate_branch_id">
                                <option value="">--Select--</option>
                                <?php

                                if (count($branch)) {
                                    foreach ($branch as $list) {

                                        ?>
                                        <option
                                            <?php if($list['id'] == $agents_portal->allocate_branch_id){ echo 'selected'; } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>

                        </div>

                        <div class="form-group col-md-3">
                            <label for="inputPassword4">Engagement Officer</label>
                            <select onchange="" class="form-control select2-single" required name="engagement_officer_id">
                                <option value="">--Select--</option>
                                <?php

                                if (count($employee)) {
                                    foreach ($employee as $list) {

                                        ?>
                                        <option
                                            <?php if($list['id'] == $agents_portal->engagement_officer_id){ echo 'selected'; } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>

                        </div>
                        <div class="form-group col-md-3">
                            <label for="txtMobile">Date</label>
                            <div class='input-group date' id='date'>
                                <input type='text' name="date" class="form-control" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>

                        </div>
                    </div>

                    <hr>
                    <h2>Bank Details</h2>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Account Name</label>
                            <input type="text" class="form-control" id="account_name"
                                   name="account_name" value="<?php echo $agents_portal->account_name; ?>">

                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Account Number</label>
                            <input type="text" class="form-control" id="account_number"
                                   name="account_number" value="<?php echo $agents_portal->account_number; ?>">

                        </div>

                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Bank Name</label>
                            <input type="text" class="form-control" id="bank_name"
                                   name="bank_name" value="<?php echo $agents_portal->bank_name; ?>">

                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Branch Name</label>
                            <input type="text" class="form-control" id="branch_name"
                                   name="branch_name" value="<?php echo $agents_portal->branch_name; ?>">

                        </div>

                    </div>


                    <button type="submit" class="btn btn-primary d-block mt-3">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>



