
<div class="row">
    <div class="col-12">
        <div class="card mb-4">
            <div class="card-body">
                <h5 class="mb-4"><?php echo $title; ?> <small style="color: red;">All Red marked are required</small></h5>
             <span style="float: right;color: red;margin-top: -43px;font-size: 16px;">File/Photo(3000 x 3000)px & Size(5MB) & Format Allow(jpg|png|jpeg|pdf|csv|doc|docx)</span>
                <form action="<?php echo base_url(); ?>application_status/change_status" method="post" enctype="multipart/form-data">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputEmail4">Status<span style="color:red;">*</span></label>
                            <select class="form-control select2-single" required name="current_status_id">
                                <option value="">--Select--</option>
                                <?php

                                if (count($application_status)) {
                                    foreach ($application_status as $list) {
                                        ?>
                                        <option
                                            value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                            <input type="hidden" class="form-control" id="id"
                                   name="id" required value="<?php echo $id;?>">
                        </div>
                    </div>

                    <div class="form-row">
                      <div class="form-group col-md-6">
                          <label for="txtMobile">File</label>
                          <input type="file" class="form-control"  id="file_path" name="file_path">
                      </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="contact">Remarks</label>
                            <textarea name="remarks" rows="8" class="form-control"></textarea>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary d-block mt-3">Change</button>
                </form>
            </div>
        </div>
    </div>
</div>
