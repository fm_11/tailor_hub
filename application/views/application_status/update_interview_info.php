
<div class="row">
    <div class="col-12">
        <div class="card mb-4">
            <div class="card-body">
                <h5 class="mb-4"><?php echo $title; ?> <small style="color: red;">All Red marked are required</small></h5>

                <form action="<?php echo base_url(); ?>application_status/update_interview_info" method="post" enctype="multipart/form-data">
                 <div class="form-row">
                    <div class="form-group col-md-6">
                            <label for="UID">University Interview Date<span style="color:red;">*</span></label>
                            <div class="input-group date">
                               <input type="text" name="university_interview_date" value="<?php echo $application_info[0]['university_interview_date']; ?>" required class="form-control">
                               <span class="input-group-text input-group-append input-group-addon">
                                   <i class="simple-icon-calendar"></i>
                               </span>
                           </div>
                      </div>
                    </div>

                    <div class="form-row">
                      <div class="form-group col-md-6">
                              <label for="VAD">Visa Appointment Date<span style="color:red;">*</span></label>
                              <div class="input-group date">
                                 <input type="text" name="visa_appointment_date" value="<?php echo $application_info[0]['visa_appointment_date']; ?>" required class="form-control">
                                 <span class="input-group-text input-group-append input-group-addon">
                                     <i class="simple-icon-calendar"></i>
                                 </span>
                             </div>
                        </div>
                    </div>
                    <input type="hidden" class="form-control" id="id"
                           name="id" required value="<?php echo $id;?>">
                    <button type="submit" class="btn btn-primary d-block mt-3">Update</button>
                </form>
            </div>
        </div>
    </div>
</div>
