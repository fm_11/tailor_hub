<style>
.txt-span {
  display: block;
  width: 100%;

  font-size: 1rem;
  line-height: 1.5;
  color:#495057;
  background-color:#fff;
  background-clip: padding-box;

  border-radius: .25rem;
  -webkit-transition: border-color 0.15s ease-in-out,-webkit-box-shadow 0.15s ease-in-out;
  transition: border-color 0.15s ease-in-out,-webkit-box-shadow 0.15s ease-in-out;
  transition: border-color 0.15s ease-in-out,box-shadow 0.15s ease-in-out;
  transition: border-color 0.15s ease-in-out,box-shadow 0.15s ease-in-out,-webkit-box-shadow 0.15s ease-in-out;
}
</style>




<div class="row">
    <div class="col-12">
        <div class="card mb-4">
            <div class="card-body">
                <h5 class="mb-4"><?php echo $title; ?></h5>

                <form action="<?php echo base_url(); ?>" method="post" enctype="multipart/form-data">
                    <input type="hidden" class="form-control" id="id"
                           name="id" value="<?php echo $applicant_info[0]['id']; ?>">

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="studentFirstName">Given Name / First Name<span style="color:red;font-weight: bold;"> (As Passport)</span></label>
                            <span class="txt-span"><?php echo $applicant_info[0]['first_name']; ?></span>

                        </div>
                        <div class="form-group col-md-4">


                        </div>
                        <div class="form-group col-md-4">
                            <label for="studentLastName">Surename / Last Name</label>
                              <span class="txt-span"><?php echo $applicant_info[0]['last_name']; ?></span>

                        </div>

                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="contact">Date Of Birth</label>
                            <div class='input-group' id=''>
                              <span class="txt-span"><?php echo $applicant_info[0]['date_of_birth']; ?></span>

                            </div>
                        </div>
                        <div class="form-group col-md-4">

                          <label for="email">Student Id</label>
                            <span class="txt-span"><?php echo $applicant_info[0]['applicant_code']; ?></span>

                        </div>
                        <div class="form-group col-md-4">
                            <label for="email">Nationality</label>
                              <span class="txt-span"><?php echo $applicant_info[0]['nationality']; ?></span>

                        </div>

                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="contact">Mobile Number</label>
                              <span class="txt-span"><?php echo $applicant_info[0]['country_code'].' '.$applicant_info[0]['mobile']; ?></span>

                        </div>
                        <div class="form-group col-md-4">
                            <label for="email">Email</label>
                              <span class="txt-span"><?php echo $applicant_info[0]['email']; ?></span>

                        </div>

                        <div class="form-group col-md-4">
                            <label for="email">Skype Id</label>
                              <span class="txt-span"><?php echo $applicant_info[0]['skype_id']; ?></span>

                        </div>

                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="contact">Passport Number</label>
                              <span class="txt-span"><?php echo $applicant_info[0]['passport_no']; ?></span>

                        </div>
                        <div class="form-group col-md-4">
                            <label for="email">Issue Date</label>
                            <span class="txt-span"><?php echo $applicant_info[0]['issue_date']; ?></span>

                        </div>

                        <div class="form-group col-md-4">
                            <label for="email">Expair Date</label>
                              <span class="txt-span"><?php echo $applicant_info[0]['expair_date']; ?></span>

                        </div>
                    </div>
                    <div class="form-row">

                        <div class="form-group col-md-4">
                            <label for="is_valid_passport">Valid passport?</label>
                            <span class="txt-span">
                            <?php if ($applicant_info[0]['is_valid_passport'] == '1') {
    echo 'Yes';
} else {
    echo "No";
}?>
                           </span>
                          </div>

                        <div class="form-group col-md-4">
                            <label for="originOffice">Gender</label>
                               <span class="txt-span">
                                <?php

                                if (count($genders)) {
                                    foreach ($genders as $list) {
                                        ?>

                                            <?php if ($list['id'] == $applicant_info[0]['gender_id']) {
                                            echo $list['name'];
                                        } ?>
                                        <?php
                                    }
                                }
                                ?>
                            </span>
                        </div>


                        <div class="form-group col-md-4">
                            <label for="originOffice">Marital Status</label>
                            <span class="txt-span">
                                <?php

                                if (count($marital_status)) {
                                    foreach ($marital_status as $list) {
                                        ?>
                                            <?php if ($list['id'] == $applicant_info[0]['marital_status_id']) {
                                            echo $list['name'];
                                        } ?>
                                        <?php
                                    }
                                }
                                ?>
                               </span>
                        </div>

                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="contact">Lead source</label>
                            <span class="txt-span">
                                <?php
                                if (count($lead_source)) {
                                    foreach ($lead_source as $list) {
                                        ?>
                                            <?php if ($list['id'] == $applicant_info[0]['lead_source_id']) {
                                            echo $list['name'];
                                        } ?>
                                        <?php
                                    }
                                }
                                ?>
                              </span>
                          </div>
                        <div class="form-group col-md-4">
                          <label for="originOffice">Origin Office</label>
                            <span class="txt-span">
                              <?php

                              if (count($origin_office)) {
                                  foreach ($origin_office as $list) {
                                      ?>

                                          <?php if ($list['id'] == $applicant_info[0]['origin_office']) {
                                          echo $list['name'];
                                      } ?>
                                      <?php
                                  }
                              }
                              ?>
                            </span>
                          </div>

                        <div class="form-group col-md-4">
                            <label for="contact">Admission Officer</label>
                            <span class="txt-span">
                                <?php

                                if (count($tbl_employee)) {
                                    foreach ($tbl_employee as $list) {
                                        ?>

                                            <?php if ($list['id'] == $applicant_info[0]['admission_officer_id']) {
                                            echo $list['name'];
                                        } ?>
                                        <?php
                                    }
                                }
                                ?>
                            </span>
                        </div>
                    </div>

                    <hr>
                    <h2>Address Details</h2>

                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="contact">Address</label>
                            <span class="txt-span"><?php echo $applicant_info[0]['cor_address']; ?></span>

                        </div>
                        <div class="form-group col-md-3">
                            <label for="contact">City / Town</label>
                             <span class="txt-span"><?php echo $applicant_info[0]['cor_city']; ?></span>
                        </div>

                        <div class="form-group col-md-3">
                            <label for="contact">Postal / Zip Code</label>
                            <span class="txt-span"><?php echo $applicant_info[0]['zip_code']; ?></span>
                        </div>

                        <div class="form-group col-md-3">
                            <label for="contact">Country</label>
                            <span class="txt-span">
                                <?php
                                if (count($countries)) {
                                    foreach ($countries as $list) {
                                        ?>
                                            <?php if ($list['id'] == $applicant_info[0]['country_id']) {
                                            echo $list['name'];
                                        } ?>
                                        <?php
                                    }
                                }
                                ?>
                          </span>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <h2 for="inputAddress"> Passport</h2>
                          </br>
                          <!-- <span class="txt-span">

                              <img src="<?php echo base_url(); ?>media/applicant_edu/<?php echo $applicant_info[0]['passport_file_location']; ?>" style=" border:1px solid #666666;" width="150" height="150">
                         </span> -->
                         <?php if(isset($applicant_info[0]['passport_file_location']) && $applicant_info[0]['passport_file_location']!='0'){?>
                            <a target="_blank" href="<?php echo base_url(); ?>media/applicant_edu/<?php echo $applicant_info[0]['passport_file_location']; ?>">
                              <button type="button" class="btn btn-secondary btn-xs mb-1">Download</button>
                            </a>
                          <?php }?>
                        </div>
                        <div class="form-group col-md-6">
                            <h2 for="inputAddress"> CV</h2>
                          </br>
                            <!-- <span class="txt-span">
                                <img src="<?php echo base_url(); ?>media/applicant_edu/<?php echo $applicant_info[0]['cv_file_location']; ?>" style=" border:1px solid #666666;" width="150" height="150">
                           </span> -->
                        <?php if(isset($applicant_info[0]['cv_file_location']) && $applicant_info[0]['cv_file_location']!='0'){?>
                            <a target="_blank"  href="<?php echo base_url(); ?>media/applicant_edu/<?php echo $applicant_info[0]['cv_file_location']; ?>">
                              <button type="button" class="btn btn-secondary btn-xs mb-1">Download</button>
                            </a>
                         <?php }?>
                        </div>
                    </div>

                    <div class="form-group">

                    </div>

                  </form>

                    <hr>
                    <h5 class="card-title">Student Education Information</h5>
                    <div class="text-right">
                        <!-- <button type="button" class="btn btn-dark default btn-sm mb-1" data-toggle="modal" data-target="#myStudent">Add Education</button> -->
                    </div>

                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Institute Name</th>
                            <th scope="col">Qualification</th>
                            <th scope="col">Country of Education</th>
                            <th scope="col">From Date</th>
                            <th scope="col">To Date</th>
                            <th scope="col">Subject</th>
                            <th scope="col">Passing Year</th>
                            <th scope="col">Result</th>
                           <th scope="col">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $i = 0;
                        foreach ($applicant_education as $eud_row):
                            $i++;
                            ?>
                            <tr>
                                <th scope="row"><?php echo $i; ?></th>
                                <td><?php echo $eud_row['institute_1']; ?></td>
                                <td><?php echo $eud_row['qualification_1']; ?></td>
                                <td><?php echo $eud_row['country_name']; ?></td>
                                <td><?php echo $eud_row['from_date']; ?></td>
                                <td><?php echo $eud_row['to_date']; ?></td>
                                <td><?php echo $eud_row['SUBJECT']; ?></td>
                                <td><?php echo $eud_row['year_of_passing_1']; ?></td>
                                <td><?php echo $eud_row['grade_1']; ?></td>
                                <td>
                                  <?php if(isset($eud_row['transcript_img_path']) && $eud_row['transcript_img_path']!='0'){?>
                                  <a target="_blank" href="<?php  echo base_url(); ?>media/applicant_edu/<?php echo $eud_row['transcript_img_path']; ?>">
                                    <button type="button" class="btn btn-secondary btn-xs mb-1">Download(T)</button>
                                  </a>
                                <?php }?>
                                  <?php if(isset($eud_row['certificate_img_path']) && $eud_row['certificate_img_path']!='0'){?>
                                  <a target="_blank" href="<?php  echo base_url(); ?>media/applicant_edu/<?php echo $eud_row['certificate_img_path']; ?>">
                                    <button type="button" class="btn btn-secondary btn-xs mb-1">Download(C)</button>
                                  </a>
                                <?php }?>
                                </td>
                                </tr>

                        <?php endforeach; ?>
                        </tbody>
                    </table>


                    <hr>

                    <h5 class="card-title">Language</h5>
                    <div class="text-right">
                        <!-- <button type="button" class="btn btn-dark default btn-sm mb-1" data-toggle="modal" data-target="#myLanguage">Add Language</button> -->
                    </div>

                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Language</th>
                            <th scope="col">Listening</th>
                            <th scope="col">Speaking</th>
                            <th scope="col">Reading</th>
                            <th scope="col">Writing</th>
                            <th scope="col">overall</th>
                            <th scope="col">Exam date</th>
                              <th scope="col">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $i = 0;
                        foreach ($applicant_language as $lang_row):
                            $i++;
                            ?>
                            <tr>
                                <th scope="row"><?php echo $i; ?></th>
                                <td><?php echo $lang_row['is_ielts']; ?></td>
                                <td><?php echo $lang_row['ielts_listening']; ?></td>
                                <td><?php echo $lang_row['ielts_speaking']; ?></td>
                                <td><?php echo $lang_row['ielts_reading']; ?></td>
                                <td><?php echo $lang_row['ielts_writing']; ?></td>
                                <td><?php echo $lang_row['ielts_overall_score']; ?></td>
                                <td><?php echo $lang_row['exam_date']; ?></td>
                                <td>
                                  <?php if(isset($lang_row['certificate_file']) && $lang_row['certificate_file']!='0'){?>
                                  <a target="_blank" href="<?php echo base_url(); ?>media/applicant_edu/<?php echo $lang_row['certificate_file']; ?>">
                                    <button type="button" class="btn btn-secondary btn-xs mb-1">Download</button>
                                  </a>
                                  <?php }?>
                                </td>
                            </tr>

                        <?php endforeach; ?>
                        </tbody>

                    </table>

                    <hr>
                    <h5 class="card-title">Work Experience </h5>
                    <div class="text-right">
                        <!-- <button type="button" class="btn btn-dark default btn-sm mb-1" data-toggle="modal" data-target="#myExperience">Add Experience</button> -->
                    </div>

                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name of Employer</th>
                            <th scope="col">Position</th>
                            <th scope="col">Country</th>
                            <th scope="col">Form </th>
                            <th scope="col">To </th>
                           <th scope="col">Action </th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $i = 0;
                        foreach ($applicant_experience as $exp_row):
                            $i++;
                            ?>
                            <tr>
                                <th scope="row"><?php echo $i; ?></th>
                                <td><?php echo $exp_row['employer_name_1']; ?></td>
                                <td><?php echo $exp_row['position_1']; ?></td>
                                <td><?php echo $exp_row['country_name']; ?></td>
                                <td><?php echo $exp_row['from']; ?></td>
                                <td>
                                  <?php
                                  if ($exp_row['is_till_now'] == '0') {
                                      echo $exp_row['to'];
                                  } else {
                                      echo 'Till Now';
                                  }

                                   ?>
                                </td>
                                <td>
                                  <?php if(isset($exp_row['experience_letter']) && $exp_row['experience_letter']!='0'){?>
                                  <a target="_blank" href="<?php echo base_url(); ?>media/applicant_edu/<?php echo $exp_row['experience_letter']; ?>">
                                    <button type="button" class="btn btn-secondary btn-xs mb-1">Download</button>
                                  </a>
                                 <?php }?>
                                </td>
                            </tr>

                        <?php endforeach; ?>
                        </tbody>

                    </table>
                    <hr>

                    <h5 class="card-title">Reference</h5>
                    <div class="text-right">
                        <!-- <button type="button" class="btn btn-dark default btn-sm mb-1" data-toggle="modal" data-target="#myReference">Add Reference</button> -->
                    </div>

                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Designation</th>
                            <th scope="col">Email</th>
                            <th scope="col">Phone </th>
                            <th scope="col">Fax </th>
                            <th scope="col">Address </th>
                            <th scope="col">City </th>
                            <th scope="col">State </th>
                            <th scope="col">Country </th>
                           <th scope="col">Action </th>

                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $i = 0;
                        foreach ($applicant_reference as $ref_row):
                            $i++;
                            ?>
                            <tr>
                                <th scope="row"><?php echo $i; ?></th>
                                <td><?php echo $ref_row['ref1_name']; ?></td>
                                <td><?php echo $ref_row['ref1_designation']; ?></td>
                                <td><?php echo $ref_row['ref1_email']; ?></td>
                                <td><?php echo $ref_row['ref1_phone']; ?></td>
                                <td><?php echo $ref_row['ref1_fax']; ?></td>
                                <td><?php echo $ref_row['ref1_address']; ?></td>
                                <td><?php echo $ref_row['ref1_city']; ?></td>
                                <td><?php echo $ref_row['ref1_state']; ?></td>
                                <td><?php echo $ref_row['country_name']; ?></td>
                                <td>
                                  <?php if(isset($ref_row['reference_letter']) && $ref_row['reference_letter']!='0'){?>
                                  <a target="_blank" href="<?php echo base_url(); ?>media/applicant_edu/<?php echo $ref_row['reference_letter']; ?>">
                                    <button type="button" class="btn btn-secondary btn-xs mb-1">Download</button>
                                  </a>
                                <?php }?>
                                </td>
                            </tr>

                        <?php endforeach; ?>
                        </tbody>

                    </table>
                    <hr>

                      <hr>
                      <h5 class="card-title">University Application List</h5>
                      <div class="text-right">
                          <!-- <button type="button" class="btn btn-dark default btn-sm mb-1" data-toggle="modal" data-target="#myReference">Add Reference</button> -->
                      </div>

                      <table class="table table-striped">
                          <thead>
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">Application Code</th>
                            <th scope="col">University Name</th>
                            <th scope="col">Course Level</th>
                            <th scope="col">Subject</th>
                            <th scope="col">Month </th>
                            <th scope="col">Year </th>
                            <th scope="col">Current Status </th>
                            <th scope="col">Country </th>
                            <th scope="col">Process Officer </th>
                          <th scope="col">Action </th>

                          </tr>
                          </thead>
                          <tbody>
                          <?php
                          $i = 0;
                          foreach ($university_application as $ref_row):
                              $i++;
                              ?>
                              <tr>
                                  <th scope="row"><?php echo $i; ?></th>
                                  <td>
                                    <a style="text-decoration:underline;"  href="<?php echo base_url(); ?>applicants/view_status/<?php echo $ref_row['id']; ?>">
                                      <?php echo $ref_row['application_code']; ?>
                                    </a>
                                  </td>
                                  <td>
                                    <a style="text-decoration:underline;"   href="<?php echo base_url(); ?>institutions/view/<?php echo $ref_row['institution_id']; ?>">
                                      <?php echo $ref_row['university_name']; ?>
                                    </a>
                                  </td>
                                  <td><?php echo $ref_row['course_level']; ?></td>
                                  <td>
                                    <a style="text-decoration:underline;"   href="<?php echo base_url(); ?>Courses/view/<?php echo $ref_row['subject_id']; ?>">
                                      <?php echo $ref_row['course_title']; ?>
                                    </a>
                                  </td>
                                  <td><?php echo $ref_row['cc_month']; ?></td>
                                  <td><?php echo $ref_row['cc_year']; ?></td>
                                  <td><?php  echo $ref_row['current_status']; ?></td>
                                  <td><?php echo $ref_row['country']; ?></td>
                                <td><?php echo $ref_row['process_officer']; ?></td>
                                <td>
                                  <?php if(isset($ref_row['sop_img_path']) && $ref_row['sop_img_path']!='0'){?>
                                  <a target="_blank" href="<?php echo base_url(); ?>media/applicant_edu/<?php echo $ref_row['sop_img_path']; ?>">
                                    <button type="button" class="btn btn-secondary btn-xs mb-1">Download(I)</button>
                                  </a>
                                 <?php }?>
                                <?php if(isset($ref_row['others_img_path']) && $ref_row['others_img_path']!='0'){?>
                                  <a target="_blank" href="<?php echo base_url(); ?>media/applicant_edu/<?php echo $ref_row['others_img_path']; ?>">
                                    <button type="button" class="btn btn-secondary btn-xs mb-1">Download(O)</button>
                                  </a>
                                <?php }?>
                                </td>
                              </tr>

                          <?php endforeach; ?>
                          </tbody>

                      </table>
                      <hr>
                <!-- Modal -->


            </div>

        </div>

    </div>
</div>
</div>
