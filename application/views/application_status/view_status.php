<style>
.txt-span {
  display: block;
  width: 100%;

  font-size: 1rem;
  line-height: 1.5;
  color:#495057;
  background-color:#fff;
  background-clip: padding-box;

  border-radius: .25rem;
  -webkit-transition: border-color 0.15s ease-in-out,-webkit-box-shadow 0.15s ease-in-out;
  transition: border-color 0.15s ease-in-out,-webkit-box-shadow 0.15s ease-in-out;
  transition: border-color 0.15s ease-in-out,box-shadow 0.15s ease-in-out;
  transition: border-color 0.15s ease-in-out,box-shadow 0.15s ease-in-out,-webkit-box-shadow 0.15s ease-in-out;
}
</style>

<div class="row">
    <div class="col-12">
        <div class="card mb-4">
            <div class="card-body">
                <!-- <h5 class="mb-4"><?php echo $title; ?></h5> -->
                    <hr>
                      <h5 class="card-title">Application Status List</h5>
                      <div class="text-right">
                          <button type="button" class="btn btn-dark default btn-sm mb-1" data-toggle="modal" data-target="#myapplication_status">Add Status</button>
                      </div>
                      <?php if(count($applicants)){?>
                      <div>
                         <h6>Student:<b> <?php echo $applicants[0]['applicant_name']; ?></b> </h6>
                         <h6>Institution:<b>  <?php echo $applicants[0]['institution_name']; ?></b></h6>
                        <h6>Application Code:<b>  <?php echo $applicants[0]['application_code']; ?></b></h6>
                        <h6>Subject:<b>  <?php echo $applicants[0]['course_title']; ?></b></h6>
                       <h6>Intake:<b>  <?php echo $applicants[0]['intake_month'].'-'.$applicants[0]['intake_year']; ?></b></h6>
                       <?php if(isset($applicants[0]['sop_img_path']) && $applicants[0]['sop_img_path']!='0'){?>
                       <a target="_blank" href="<?php  echo base_url(); ?>media/applicant_edu/<?php echo $applicants[0]['sop_img_path']; ?>">
                         <button type="button" class="btn btn-secondary btn-xs mb-1">Download(Statement of Purpose)</button>
                       </a>
                     <?php }?>
                       <?php if(isset($applicants[0]['others_img_path']) && $applicants[0]['others_img_path']!='0'){?>
                       <a target="_blank" href="<?php  echo base_url(); ?>media/applicant_edu/<?php echo $applicants[0]['others_img_path']; ?>">
                         <button type="button" class="btn btn-secondary btn-xs mb-1">Download(Others)</button>
                       </a>
                     <?php }?>
                      </div>
                    <?php }?>
                      <table class="table table-striped">
                          <thead>
                          <tr>
                              <th scope="col">#</th>
                              <th scope="col">Changes Date</th>
                              <th scope="col">New Status</th>
                              <th scope="col">Process Offer</th>
                              <th scope="col">Remarks </th>
                              <th scope="col">Action </th>
                          </tr>
                          </thead>
                          <tbody>
                          <?php
                          $i = 0;
                          foreach ($application_status_view as $ref_row):
                              $i++;
                              ?>
                              <tr>
                                  <th scope="row"><?php echo $i; ?></th>
                                  <td>
                                    <?php
                                       if('0000-00-00 00:00:00'!=$ref_row['date_time'])
                                       {
                                         $date=date_create($ref_row['date_time']);
                                          echo date_format($date,"Y-m-d");
                                       }
                                     ?>
                                  </td>
                                 <td><?php echo $ref_row['new_status']; ?></td>
                                  <td><?php echo $ref_row['process_officer']; ?></td>
                                    <td><?php echo $ref_row['remarks']; ?></td>
                                  <td>
                                    <?php if(isset($ref_row['file_location']) && $ref_row['file_location']!='0'){?>
                                    <a target="_blank"  href="<?php echo base_url(); ?>media/applicantion_document/<?php echo $ref_row['file_location']; ?>">
                                      <button type="button" class="btn btn-secondary btn-xs mb-1">Download</button>
                                    </a>
                                  <?php }?>
                                  </td>


                              </tr>

                          <?php endforeach; ?>
                          </tbody>

                      </table>
                <!-- Modal -->
                <div id="myapplication_status" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Change Application Status <small style="color: red;">All Red marked are required</small></h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>

                            </div>
                            <div class="modal-body">
                                  <form action="<?php echo base_url(); ?>application_status/view_status/<?php echo $id;?>" method="post" enctype="multipart/form-data">
                                    <div class="form-group">
                                      <label for="inputEmail4">Status<span style="color:red;">*</span></label>
                                      <select class="form-control select2-single" required name="current_status_id">
                                          <option value="">--Select--</option>
                                          <?php

                                          if (count($application_status)) {
                                              foreach ($application_status as $list) {
                                                  ?>
                                                  <option
                                                      value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                                  <?php
                                              }
                                          }
                                          ?>
                                      </select>
                                      <input type="hidden" class="form-control" id="id"
                                             name="id" required value="<?php echo $id;?>">
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label for="contact">Remarks<span style="color:red;">*</span></label>
                                            <textarea name="remarks" rows="8" class="form-control" required></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputAddress">Upload File</label>
                                      <input type="file" class="form-control"  id="file_path" name="file_path">
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-danger d-block mt-3" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-success d-block mt-3">Change</button>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>


            </div>

        </div>

    </div>
</div>
</div>
