<div class="col-12 mb-4">
    <div class="card">
        <div class="card-body" style="overflow-x: auto;">
            <h5 class="card-title"><?php echo $title; ?></h5>

            <?php
              $courses_id = $this->session->userdata('courses_id');
              $institutions_id = $this->session->userdata('institutions_id');
              $country_id = $this->session->userdata('country_id');
              $name = $this->session->userdata('name');
              $application_status_id = $this->session->userdata('application_status_id');
              $intake_year_id = $this->session->userdata('intake_year_id');
              $intake_month_id = $this->session->userdata('intake_month_id');
                $admission_officer_id = $this->session->userdata('admission_officer');
             ?>
            <form class="form-inline" method="post" action="<?php echo base_url(); ?>application_status/index">
              <div class="form-group col-md-3 mb-3">
                <label>Name/Email/Contact No./Id</label>
                <input type="text" class="form-control" value="<?php echo $name; ?>" name="name" id="inlineFormInputName2">
             </div>
             <div class="form-group col-md-3 mb-3">
              <label>Institutions</label>
              <select  class="form-control col-md-12" name="institutions_id" >
                  <option value="">--Select--</option>
                  <?php
                  if (count($institutions)) {
                      foreach ($institutions as $list) {
                          ?>
                          <option
                              <?php if ($list['id'] == $institutions_id) {
                              echo 'selected';
                          } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                          <?php
                      }
                  }
                  ?>
              </select>
            </div>
            <div class="form-group col-md-3 mb-3">
             <label>Courses</label>
             <select  class="form-control col-md-12" name="courses_id" >
                 <option value="">--Select--</option>
                 <?php
                 if (count($courses)) {
                     foreach ($courses as $list) {
                         ?>
                         <option
                             <?php if ($list['id'] == $courses_id) {
                             echo 'selected';
                         } ?> value="<?php echo $list['id']; ?>"><?php echo $list['course_title']; ?></option>
                         <?php
                     }
                 }
                 ?>
             </select>
           </div>
            <div class="form-group col-md-3 mb-3">
              <label for="contact">Country</label>
              <select  class="form-control col-md-12"  name="country_id" >
                  <option value="">--Select--</option>
                  <?php

                  if (count($countries)) {
                      foreach ($countries as $list) {
                          ?>
                          <option
                              <?php if ($list['id'] == $country_id) {
                              echo 'selected';
                          } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                          <?php
                      }
                  }
                  ?>
              </select>
            </div>
            <div class="form-group col-md-3 mb-3">
              <label for="contact">Application Status</label>
              <select  class="form-control col-md-12"  name="application_status_id" >
                  <option value="">--Select--</option>
                  <?php

                  if (count($application_status)) {
                      foreach ($application_status as $list) {
                          ?>
                          <option
                              <?php if ($list['id'] == $application_status_id) {
                              echo 'selected';
                          } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                          <?php
                      }
                  }
                  ?>
              </select>
            </div>
            <div class="form-group col-md-2 mb-2">
              <label for="contact">Intake Month</label>
              <select  class="form-control col-md-12"  name="intake_month_id" >
                  <option value="">--Select--</option>
                  <?php

                  if (count($intake_month)) {
                      foreach ($intake_month as $list) {
                          ?>
                          <option
                              <?php if ($list['id'] == $intake_month_id) {
                              echo 'selected';
                          } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                          <?php
                      }
                  }
                  ?>
              </select>
            </div>
            <div class="form-group col-md-2 mb-2">
              <label for="contact">Intake Year</label>
              <select  class="form-control col-md-12"  name="intake_year_id" >
                  <option value="">--Select--</option>
                  <?php

                  if (count($intake_year)) {
                      foreach ($intake_year as $list) {
                          ?>
                          <option
                              <?php if ($list['id'] == $intake_year_id) {
                              echo 'selected';
                          } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                          <?php
                      }
                  }
                  ?>
              </select>
            </div>
            <div class="form-group col-md-4 mb-4">
              <label for="contact">Admission Officer</label>
              <select  class="form-control col-md-12"  name="admission_officer" >
                  <option value="">--Select--</option>
                  <?php

                  if (count($admission_officer)) {
                      foreach ($admission_officer as $list) {
                          ?>
                          <option
                              <?php if ($list['id'] == $admission_officer_id) {
                              echo 'selected';
                          } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                          <?php
                      }
                  }
                  ?>
              </select>
            </div>
             <button type="submit" class="btn btn-sm btn-outline-primary mb-2">Search</button>
            </form>


            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Applicatin ID</th>
                    <th scope="col">Student ID</th>
                    <th scope="col">Student</th>
                    <th scope="col">Country</th>
                    <th scope="col">Institute</th>
                    <th scope="col">Course</th>
                    <th scope="col">Intake</th>
                    <th scope="col">Current Status</th>
                    <th scope="col">Process Officer</th>
                    <th scope="col">Admission Officer</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $i = (int)$this->uri->segment(3);
                foreach ($applications as $row):
                    $i++;
                    ?>
                    <tr>
                        <th scope="row"><?php echo $i; ?></th>
                        <td style="min-width: 101px;">
                          <a style="text-decoration:underline;"   href="<?php echo base_url(); ?>application_status/view_status/<?php echo $row['id']; ?>">
                            <?php echo $row['application_code']; ?>
                          </a>
                        </td>
                        <td style="min-width: 101px;">
                          <a style="text-decoration:underline;" href="<?php echo base_url(); ?>application_status/view/<?php echo $row['applicantid']; ?>">
                              <?php echo $row['applicant_code']; ?>
                            </a>
                        </td>
                        <td><?php echo $row['first_name'] . ' ' .$row['last_name']; ?></td>
                        <td><?php echo $row['country_name']; ?></td>
                        <td><?php echo $row['institute_name']; ?></td>
                        <td><?php echo $row['course_name']; ?></td>
                        <td><?php echo $row['intake_months'].'-'.$row['intake_years']; ?></td>
                        <td><?php echo $row['current_status_name']; ?></td>
                        <td><?php echo $row['process_officer']; ?></td>
                        <td><?php echo $row['admission_officer']; ?></td>
                        <td>

                          <a href="<?php echo base_url(); ?>application_status/view_status/<?php echo $row['id']; ?>"
                             title="Edit">
                              <button type="button" class="btn btn-secondary btn-xs mb-1">
                                  Change Status
                              </button>
                          </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <?php echo $this->pagination->create_links(); ?>
        </div>
    </div>
</div>
