<hr>

<?php
if(isset($excel)){
  header('Content-type: application/excel');
  $filename = 'My Attendance Details Report.xls';
  header('Content-Disposition: attachment; filename='.$filename);
}
?>

<table class="table">
    <thead>
      <tr>
          <th  class="text-center" colspan="5" scope="col">
            <h3>
               <?php echo $org_info['org_name']; ?>
            </h3>
            <p><?php echo $org_info['address']; ?></p>
            <p><?php echo $org_info['email']; ?></p>
            <p>Attendance Report for month of <?php echo date('F', mktime(0, 0, 0, $month, 10)). ', ' . $year; ?></p>
            <p>Name: <?php echo $rData[0]['name'].' (' . $rData[0]['code'] . ')'; ?></p>
            <p>Designation: <?php echo $rData[0]['post_name']; ?></p>
          </th>
      </tr>

      <tr>
            <th scope="col">#</th>
            <th scope="col">Date</th>
              <th scope="col">Status</th>
            <th scope="col">Login time</th>
            <th scope="col">Logout time</th>

      </tr>
    </thead>
    <tbody>

      <?php
      $i = 0;
      foreach ($rData[0]['data_array'] as $row):
          $i++;
          ?>
        <tr>
            <th scope="row"><?php echo $i; ?></th>
            <td><?php echo $row['date']; ?></td>
            <td><?php echo $row['status']; ?></td>
            <td><?php echo $row['login_time']; ?></td>
            <td><?php echo $row['logout_time']; ?></td>
        </tr>

      <?php endforeach; ?>
    </tbody>
</table>
