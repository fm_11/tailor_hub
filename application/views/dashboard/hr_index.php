
    <div class="row  ">
        <div class="col-12 col-xl-6">
            <div class="icon-cards-row">
                <div class="owl-container">
                    <div class="owl-carousel dashboard-numbers">

                        <a href="<?php echo base_url(); ?>tailor_entry/index" class="card">
                            <div class="card-body text-center">
                                <i class="iconsminds-user"></i>
                                <p class="card-text mb-0">Total Tailor</p>
                                <p class="lead text-center"><?php echo $total_employee; ?></p>
                            </div>
                        </a>
                        <a href="<?php echo base_url(); ?>areas/index" class="card">
                            <div class="card-body text-center">
                                <i class="iconsminds-user"></i>
                                <p class="card-text mb-0">Total Area</p>
                                <p class="lead text-center"><?php echo $total_area; ?></p>
                            </div>
                        </a>
                        <a href="<?php echo base_url(); ?>tailor_order/index" class="card">
                            <div class="card-body text-center">
                                <i class="iconsminds-user"></i>
                                <p class="card-text mb-0">Total Order</p>
                                <p class="lead text-center"><?php echo $total_order; ?></p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>


        </div>

        <!-- <div class="col-xl-6 col-lg-12 mb-4">
            <div class="card">
                <div class="position-absolute card-top-buttons">
                    <button class="btn btn-header-light icon-button">
                        <i class="simple-icon-refresh"></i>
                    </button>
                </div>

                <div class="card-body">
                    <h5 class="card-title">New Notice List</h5>
                    <div class="scroll dashboard-list-with-thumbs">

                      <?php
                        $j = 0;
                        foreach ($notices as $n_row):
                      ?>

                        <div class="d-flex flex-row mb-3">
                            <a class="d-block position-relative" href="#">
                                <img src="<?php echo base_url(); ?>media/admin_panel/img/contract.png" alt="Marble Cake"
                                    class="list-thumbnail border-0" />
                                <span class="badge badge-pill badge-theme-2 position-absolute badge-top-right">New</span>
                            </a>
                            <div class="pl-3 pt-2 pr-2 pb-2">
                                <a href="#" data-toggle="modal" data-target="#exampleModal_<?php echo $j; ?>">
                                    <p class="list-item-heading"><?php echo $n_row['title']; ?></p>
                                    <div class="pr-4 d-none d-sm-block">
                                        <p class="text-muted mb-1 text-small"><?php echo $n_row['name'].' ('. $n_row['code'] .')'; ?></p>
                                    </div>
                                    <div class="text-primary text-small font-weight-medium d-none d-sm-block">
                                        <?php echo $n_row['date_time']; ?></div>
                                </a>



                                <div class="modal fade" id="exampleModal_<?php echo $j; ?>" tabindex="-1" role="dialog" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModal_<?php echo $j; ?>Label"><?php echo $n_row['title']; ?></h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <?php echo $n_row['description']; ?>
                                                 <?php if($n_row['file_location'] != ''){ ?>
                                                  <hr>
                                                  <p class="d-sm-inline-block mb-1">
                                                      <a target="_blank" href="<?php echo base_url(); ?>media/notice/<?php echo $n_row['file_location']; ?>">
                                                         <span class="badge badge-pill badge-outline-primary mb-1">Downlod File</span>
                                                      </a>
                                                  </p>
                                                <?php } ?>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <?php $j++; endforeach; ?>
                    </div>
                </div>
            </div>
        </div> -->
    </div>
