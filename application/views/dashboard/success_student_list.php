<script type="text/javascript">
    function deleteConfirm() {
        var result = confirm("Are you sure to delete?");
        if (result == true) {
            return true;
        }
        else {
            return false;
        }
    }
</script>

<h2>
    <a class="button_grey_round" style="margin-bottom: 5px;"
       href="<?php echo base_url(); ?>dashboard/success_student_add"><span>Add New Success Student</span></a>
</h2>
<table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <thead>
    <tr>
        <th width="50" scope="col">SL</th>
        <th width="100" scope="col">Name</th>
		<th width="100" scope="col">Year of Passing</th>
        <th width="100" scope="col">Educational Qualification</th>
        <th width="100" scope="col">Organization</th>  
        <th width="100" scope="col">Position</th>  	
		<th width="100" scope="col">Mobile</th> 
		<th width="100" scope="col">Email</th> 	
        <th width="100" scope="col">Address</th> 				
        <th width="100" scope="col">Actions</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = 0;
    foreach ($success_student as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td width="34" style="vertical-align:middle">
                <?php echo $i; ?>
            </td>
            <td style="vertical-align:middle"><?php echo $row['name']; ?></td>
			<td style="vertical-align:middle"><?php echo $row['year_of_passing']; ?></td>
			<td style="vertical-align:middle"><?php echo $row['edu_qua']; ?></td>
			<td style="vertical-align:middle"><?php echo $row['org']; ?></td>
			<td style="vertical-align:middle"><?php echo $row['position']; ?></td>
            <td style="vertical-align:middle"><?php echo $row['mobile']; ?></td>
            <td style="vertical-align:middle"><?php echo $row['email']; ?></td>
			<td style="vertical-align:middle"><?php echo $row['address']; ?></td>
		
            <td style="vertical-align:middle">
                <a href="<?php echo base_url(); ?>dashboard/success_student_delete/<?php echo $row['id']; ?>"
                   onclick="return deleteConfirm()" class="delete_icon" title="Delete"></a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>