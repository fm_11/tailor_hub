<div class="row">
            <div class="col-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <h5 class="mb-4"><?php echo $title; ?></h5>
                <?php if(isset($present_data)){ ?>
                    
                    <table class="table">
                        <thead>
                          <tr>
                                <th scope="col">#</th>
                                <th scope="col">Name</th>
                                <th scope="col">Employee ID</th>
                                <th scope="col">Designation</th>
                                <th scope="col">Login Time</th>
                                <th scope="col">Logout Time</th>
                          </tr>
                        </thead>
                        <tbody>

                          <?php
                          $i = 0;
                          foreach ($present_data as $row):
                              $i++;
                              ?>

                            <tr>
                                <th scope="row"><?php echo $i; ?></th>
                                <td><?php echo $row['name']; ?></td>
                                <td><?php echo $row['code']; ?></td>
                                <td><?php echo $row['post_name']; ?></td>
                                <td><?php echo date('h:i:s A', strtotime($row['login_time'])); ?></td>
                                <td>
                                  <?php
                                  if($row['logout_time'] != ''){
                                    echo date('h:i:s A', strtotime($row['logout_time']));
                                  }else{
                                    echo '-';
                                  }
                                   ?>
                                </td>
                            </tr>

                          <?php endforeach; ?>

                        </tbody>
                    </table>
                  <?php } ?>
               </div>
          </div>
      </div>
</div>
