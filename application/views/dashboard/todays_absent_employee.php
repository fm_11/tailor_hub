<div class="row">
            <div class="col-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <h5 class="mb-4"><?php echo $title; ?></h5>
                <?php if(isset($absentee_info)){ ?>
                  
                    <table class="table">
                        <thead>
                          <tr>
                                <th scope="col">#</th>
                                <th scope="col">Name</th>
                                <th scope="col">Employee ID</th>
                                <th scope="col">Designation</th>
                                <th scope="col">Branch</th>

                          </tr>
                        </thead>
                        <tbody>

                          <?php
                          $i = 0;
                          foreach ($absentee_info as $row):
                              $i++;
                              ?>

                            <tr>
                                <th scope="row"><?php echo $i; ?></th>
                                <td><?php echo $row['name']; ?></td>
                                <td><?php echo $row['code']; ?></td>
                                <td><?php echo $row['post_name']; ?></td>
                                <td><?php echo $row['branch_name']; ?></td>

                            </tr>

                          <?php endforeach; ?>

                        </tbody>
                    </table>
                  <?php } ?>
               </div>
          </div>
      </div>
</div>
