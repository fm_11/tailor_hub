
<div class="col-12">
<div class="card mb-4">
 <div class="card-body ">
     <h5 class="mb-4">Access Denied</h5>
     <div class="alert alert-danger rounded" role="alert">
         Sorry... You are not allowed to have this page!
     </div>
   </div>
 </div>
</div>
