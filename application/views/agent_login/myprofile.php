
<style>
.txt-span {
  display: block;
  width: 100%;

  font-size: 1rem;
  line-height: 1.5;
  color:#495057;
  background-color:#fff;
  background-clip: padding-box;

  border-radius: .25rem;
  -webkit-transition: border-color 0.15s ease-in-out,-webkit-box-shadow 0.15s ease-in-out;
  transition: border-color 0.15s ease-in-out,-webkit-box-shadow 0.15s ease-in-out;
  transition: border-color 0.15s ease-in-out,box-shadow 0.15s ease-in-out;
  transition: border-color 0.15s ease-in-out,box-shadow 0.15s ease-in-out,-webkit-box-shadow 0.15s ease-in-out;
}
</style>

<div class="row">
    <div class="col-12">
        <div class="card mb-4">
            <div class="card-body">
                <h5 class="mb-4"><?php echo $title; ?></h5>

                <div class="form-row">


                        <div class="form-group col-md-4">
                            <label for="Companyname">Company Name</label>
                                <span class="txt-span"><?php echo $agents->company_name;?></span>


                        </div>
                        <div class="form-group col-md-4">
                            <label for="Personalname">Person Name</label>
                              <span class="txt-span"><?php echo $agents->personal_name;?></span>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="Personalname">Agent code</label>
                              <span class="txt-span"><?php echo $agents->code;?></span>
                        </div>
                    </div>

                    <div class="form-row">

                        <div class="form-group col-md-4">
                            <label for="address">Address</label>
                                <span class="txt-span"><?php echo $agents->address;?></span>


                        </div>
                        <div class="form-group col-md-4">
                            <label for="phone">Phone</label>
                              <span class="txt-span"><?php echo $agents->country_code.' '.$agents->phone;?></span>


                        </div>
                        <div class="form-group col-md-4">
                            <label for="email">Email</label>
                                <span class="txt-span"><?php echo $agents->email;?></span>


                        </div>
                    </div>

                    <div class="form-row">

                        <div class="form-group col-md-4">
                            <label for="txtMobile">Admission officer (NE)</label>
                                <?php
                                if (count($employees)) {
                                    foreach ($employees as $list) {
                                     if ($list['id'] == $agents->admission_officer) {
                                          ?>
                                              <span class="txt-span"><?php echo $list['name'];?></span>
                                        <?php
                                      }
                                    }
                                }
                                ?>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="txtMobile">Marketing Officer (NE)</label>
                            <?php
                            if (count($employees)) {
                                foreach ($employees as $list) {
                                 if ($list['id'] == $agents->marketing_officer) {
                                      ?>
                                          <span class="txt-span"><?php echo $list['name'];?></span>
                                    <?php
                                  }
                                }
                            }
                            ?>

                        </div>
                        <div class="form-group col-md-4">
                            <label for="bankName">Bank Name</label>
                              <span class="txt-span"><?php echo $agents->bank_name;?></span>

                        </div>
                    </div>

                    <div class="form-row">

                        <div class="form-group col-md-4">
                            <label for="accountNumber">Account Number</label>
                              <span class="txt-span"><?php echo $agents->account_number;?></span>

                        </div>
                        <div class="form-group col-md-4">
                            <label for="accountName">Account Name</label>
                              <span class="txt-span"><?php echo $agents->account_name;?></span>


                        </div>
                        <div class="form-group col-md-4">
                            <label for="branchName">Branch Name</label>
                              <span class="txt-span"><?php echo $agents->branch_name;?></span>


                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputAddress"> Logo </label><br>
                            <?php if(!empty($agents->logo)){?>
                           <div class="col-12" >
                              <img style="width: 50%;border-radius: 50%;" src="<?php echo base_url(); ?>media/agent/<?php echo $agents->logo;?>" />
                           </div>
                         <br>   <br>
                         <?php }?>
                            <!-- <a target="_blank"  href="<?php echo base_url(); ?>media/agent/<?php echo $agents->logo; ?>">
                              <button type="button" class="btn btn-secondary btn-xs mb-1">Download</button>
                            </a> -->
                        </div>

                    </div>

            </div>
        </div>
    </div>
</div>
