
<style>
.txt-span {
  display: block;
  width: 100%;

  font-size: 1rem;
  line-height: 1.5;
  color:#495057;
  background-color:#fff;
  background-clip: padding-box;

  border-radius: .25rem;
  -webkit-transition: border-color 0.15s ease-in-out,-webkit-box-shadow 0.15s ease-in-out;
  transition: border-color 0.15s ease-in-out,-webkit-box-shadow 0.15s ease-in-out;
  transition: border-color 0.15s ease-in-out,box-shadow 0.15s ease-in-out;
  transition: border-color 0.15s ease-in-out,box-shadow 0.15s ease-in-out,-webkit-box-shadow 0.15s ease-in-out;
}

</style>

<div class="row">
    <div class="col-12">
        <div class="card mb-4">
            <div class="card-body">
                <h5 class="mb-4"><?php echo $title; ?></h5>
                <?php if(!empty($admission_officer[0]['photo_location'])){?>
               <div class="col-6" style="text-align: center;">
                  <img style="width: 25%;border-radius: 50%;" src="<?php echo base_url(); ?>media/employee/<?php echo $admission_officer[0]['photo_location'];?>" />
               </div>
             <br>   <br>
             <?php }?>

               <table style="margin: auto;" class="table">
                 <tr>
                   <th>Name:</th>
                   <td><?php echo $admission_officer[0]['name'];?></td>
                 </tr>
                 <tr>
                   <th>Email:</th>
                   <td><?php echo $admission_officer[0]['email'];?></td>
                 </tr>
                 <tr>
                   <th>Mobile:</th>
                   <td><?php echo $admission_officer[0]['country_code'].' '.$admission_officer[0]['mobile'];?></td>
                 </tr>
                 <tr>
                   <th>Designation:</th>
                   <td><?php echo $admission_officer[0]['post'];?></td>
                 </tr>
                 <tr>
                   <th>Branch:</th>
                   <td><?php echo $admission_officer[0]['branch_name'];?></td>
                 </tr>
               </table>

            </div>
        </div>
    </div>
</div>
