<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Login to Agent</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<?php echo base_url(); ?>media/admin_panel/favicon.png" type="image/png">
    <link rel="stylesheet" href="<?php echo base_url(); ?>media/admin_panel/font/iconsmind-s/css/iconsminds.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>media/admin_panel/font/simple-line-icons/css/simple-line-icons.css" />

    <link rel="stylesheet" href="<?php echo base_url(); ?>media/admin_panel/css/vendor/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>media/admin_panel/css/vendor/bootstrap.rtl.only.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>media/admin_panel/css/vendor/bootstrap-float-label.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>media/admin_panel/css/main.css" />
</head>

<body class="background show-spinner">
    <div class="fixed-background"></div>
    <main>
        <div class="container">
            <div class="row h-100">
                <div class="col-12 col-md-10 mx-auto my-auto">
                    <div class="card auth-card" style="opacity: .85;">
                        <div class="position-relative image-side ">

                            <p class=" text-white h2">Attendance360</p>

                            <p class="white mb-0">
                                Please use your credentials to login.
                            </p>


                        </div>
                        <div class="form-side">
                            <!--<a href="javascript:void">
                                <span class="logo-single"></span>
                            </a>!-->
                            <h6 class="mb-4">Login to <span style="color:#0059b3;">Agent</span><span style="color:#ff8080;"></span></h6>

							 <?php
									$message = $this->session->userdata('message');
									if ($message != '') {
										?>
										<div class="alert alert-success" role="alert">
												<?php
												echo $message;
												$this->session->unset_userdata('message');
												?>
										</div>
										<?php
							   }
						   ?>

						   <?php
									$exception = $this->session->userdata('exception');
									if ($exception != '') {
										?>
										<div class="alert alert-danger" role="alert">
												<?php
												echo $exception;
												$this->session->unset_userdata('exception');
												?>
										</div>
										<?php
							   }
						   ?>


                            <form method="post" action="<?php echo base_url(); ?>agent_login/authentication">
                                <label class="form-group has-float-label mb-4">
                                    <input class="form-control" required="1" name="user_name"/>
                                    <span>Username</span>
                                </label>

                                <label class="form-group has-float-label mb-4">
                                    <input class="form-control" type="password" required="1"  name="password" />
                                    <span>Password</span>
                                </label>
                                <div class="d-flex justify-content-between align-items-center">
                                    <a href="<?php echo base_url(); ?>agent_login/forgot_password">Forget password?</a>
                                    <button class="btn btn-primary btn-lg btn-shadow" type="submit">LOGIN</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
	 <!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-142794531-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-142794531-1');
	</script>
    <script src="<?php echo base_url(); ?>media/admin_panel/js/vendor/jquery-3.3.1.min.js"></script>
    <script src="<?php echo base_url(); ?>media/admin_panel/js/vendor/bootstrap.bundle.min.js"></script>
    <script src="<?php echo base_url(); ?>media/admin_panel/js/dore.script.js"></script>
    <script src="<?php echo base_url(); ?>media/admin_panel/js/scripts.js"></script>
</body>

</html>
