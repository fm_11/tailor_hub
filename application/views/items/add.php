
<div class="row">
    <div class="col-12">
        <div class="card mb-4">
            <div class="card-body">
                <h5 class="mb-4"><?php echo $title; ?> <small style="color: red;">All Red marked are required</small></h5>

                <form action="<?php echo base_url(); ?>items/add" method="post" enctype="multipart/form-data">
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="inputEmail4">Item Name<span style="color:red;">*</span></label>
                            <input type="text" class="form-control" id="name"
                                   name="name" required value="">
                        </div>


                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                          <label for="inputPassword4">Measurement Level 1<span style="color:red;">*</span></label>
                          <input type="text" class="form-control" required name="measurement_level_1"  value="">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Measurement Level 2</label>
                            <input type="text" class="form-control"  name="measurement_level_2"  value="">
                        </div>

                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                          <label for="inputPassword4">Measurement Level 3</label>
                          <input type="text" class="form-control"  name="measurement_level_3"  value="">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Measurement Level 4</label>
                            <input type="text" class="form-control"  name="measurement_level_4"  value="">
                        </div>

                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                          <label for="inputPassword4">Measurement Level 5</label>
                          <input type="text" class="form-control"  name="measurement_level_5"  value="">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Measurement Level 6</label>
                            <input type="text" class="form-control"  name="measurement_level_6"  value="">
                        </div>

                    </div>
                    <div class="form-row">
                      <div class="form-group col-md-12">
                          <label for="inputPassword4">Image 1<span style="color:red;">*</span></label>
                          <input type="file" required class="form-control"  name="txtPhoto1"  value="">
                      </div>


                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                          <label for="inputPassword4">Image 2</label>
                          <input type="file" class="form-control"  name="txtPhoto2"  value="">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Image 3</label>
                            <input type="file" class="form-control"  name="txtPhoto3"  value="">
                        </div>

                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                          <label for="inputPassword4">Image 4</label>
                          <input type="file" class="form-control"  name="txtPhoto4"  value="">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Image 5</label>
                            <input type="file" class="form-control"  name="txtPhoto5"  value="">
                        </div>

                    </div>

                    <button type="submit" class="btn btn-primary d-block mt-3">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
