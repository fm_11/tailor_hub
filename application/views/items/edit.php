
<div class="row">
    <div class="col-12">
        <div class="card mb-4">
            <div class="card-body">
                <h5 class="mb-4"><?php echo $title; ?> <small style="color: red;">All Red marked are required</small></h5>

                <form action="<?php echo base_url(); ?>items/edit/<?php echo $items->id;?>" method="post" enctype="multipart/form-data">

                    <div class="form-row">

                        <div class="form-group col-md-12">
                            <label for="inputPassword4">Item Name<span style="color:red;">*</span></label>
                            <input type="text" class="form-control" id="name"
                                   name="name" required value="<?php echo $items->name;?>">
                            <input type="hidden" class="form-control" id="id"
                                   name="id" required value="<?php echo $items->id;?>">
                        </div>


                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                          <label for="inputPassword4">Measurement Level 1<span style="color:red;">*</span></label>
                          <input type="text" class="form-control" required name="measurement_level_1"  value="<?php echo $items->measurement_level_1;?>">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Measurement Level 2</label>
                            <input type="text" class="form-control"  name="measurement_level_2"  value="<?php echo $items->measurement_level_2;?>">
                        </div>

                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                          <label for="inputPassword4">Measurement Level 3</label>
                          <input type="text" class="form-control"  name="measurement_level_3"  value="<?php echo $items->measurement_level_3;?>">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Measurement Level 4</label>
                            <input type="text" class="form-control"  name="measurement_level_4"  value="<?php echo $items->measurement_level_4;?>">
                        </div>

                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                          <label for="inputPassword4">Measurement Level 5</label>
                          <input type="text" class="form-control"  name="measurement_level_5"  value="<?php echo $items->measurement_level_5;?>">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Measurement Level 6</label>
                            <input type="text" class="form-control"  name="measurement_level_6"  value="<?php echo $items->measurement_level_6;?>">
                        </div>

                    </div>
                    <div class="form-row">
                      <div class="form-group col-md-12">
                          <label for="inputPassword4">Image 1</label>
                          <br>
                        <?php if(!empty($items->image_path_1)){?>
                        <img style="width: 100px;height:100px;" src="<?php echo base_url(); ?>media/tailor_item/<?php echo $items->image_path_1;?>" />
                      <?php }?>
                       <input type="hidden" name="oldtxtPhoto1"  value="<?php echo $items->image_path_1;?>">
                       <input type="file" class="form-control"  name="txtPhoto1"  value="">
                      </div>

                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                          <label for="inputPassword4">Image 2</label>
                          <br>
                        <?php if(!empty($items->image_path_2)){?>
                        <img style="width: 100px;height:100px;" src="<?php echo base_url(); ?>media/tailor_item/<?php echo $items->image_path_2;?>" />
                      <?php }?>
                       <input type="hidden" name="oldtxtPhoto2"  value="<?php echo $items->image_path_2;?>">
                          <input type="file" class="form-control"  name="txtPhoto2"  value="">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Image 3</label>
                            <br>
                          <?php if(!empty($items->image_path_3)){?>
                          <img style="width: 100px;height:100px;" src="<?php echo base_url(); ?>media/tailor_item/<?php echo $items->image_path_3;?>" />
                        <?php }?>
                         <input type="hidden" name="oldtxtPhoto3"  value="<?php echo $items->image_path_3;?>">
                            <input type="file" class="form-control"  name="txtPhoto3"  value="">
                        </div>

                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                          <label for="inputPassword4">Image 4</label>
                          <br>
                        <?php if(!empty($items->image_path_4)){?>
                        <img style="width: 100px;height:100px;" src="<?php echo base_url(); ?>media/tailor_item/<?php echo $items->image_path_4;?>" />
                      <?php }?>
                       <input type="hidden" name="oldtxtPhoto4"  value="<?php echo $items->image_path_4;?>">
                          <input type="file" class="form-control"  name="txtPhoto4"  value="">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Image 5</label>
                            <br>
                          <?php if(!empty($items->image_path_5)){?>
                          <img style="width: 100px;height:100px;" src="<?php echo base_url(); ?>media/tailor_item/<?php echo $items->image_path_5;?>" />
                        <?php }?>
                         <input type="hidden" name="oldtxtPhoto5"  value="<?php echo $items->image_path_5;?>">
                            <input type="file" class="form-control"  name="txtPhoto5"  value="">
                        </div>

                    </div>
                    <button type="submit" class="btn btn-primary d-block mt-3">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
