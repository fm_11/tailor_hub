<div class="col-12 mb-4">
    <div class="card">
        <div class="card-body">
            <h5 class="card-title"><?php echo $title; ?></h5>
            <?php
              $name = $this->session->userdata('name');
             ?>
            <form class="form-inline" method="post" action="<?php echo base_url(); ?>items/index">
              <div class="form-group col-md-4 mb-4">
                <label>Item</label>
                <input type="text" class="form-control col-md-12" value="<?php echo $name; ?>" name="name" id="inlineFormInputName2">
             </div>
             <button type="submit" class="btn btn-sm btn-outline-primary mb-2">Search</button>
            </form>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Item</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $i = (int)$this->uri->segment(3);
                foreach ($items as $row):
                    $i++;
                    ?>
                    <tr>
                        <th scope="row"><?php echo $i; ?></th>
                        <td><?php echo $row['name']; ?></td>

                        <td>

                            <a href="<?php echo base_url(); ?>items/edit/<?php echo $row['id']; ?>"
                               title="Edit">
                                <button type="button" class="btn btn-primary btn-xs mb-1">
                                    Edit
                                </button>
                            </a>

                            <a href="<?php echo base_url(); ?>items/delete/<?php echo $row['id']; ?>"
                               onclick="return deleteConfirm()" title="Delete">
                                <button type="button" class="btn btn-primary btn-xs mb-1">
                                    Delete
                                </button>

                            </a>

                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <?php echo $this->pagination->create_links(); ?>
        </div>
    </div>
</div>
