<div class="row">
            <div class="col-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <h5 class="mb-4"><?php echo $title; ?> <small style="color: red;">All Red marked are required</small></h5>

                            <?php echo form_open("user_roles/" . $action); ?>
                              <div class="form-group">
                                <label for="inputEmail4">Name<span style="color:red;">*</span></label>
                                <input value="<?php echo isset($row->role_name) ? $row->role_name : ''; ?>" class="form-control"
                                           type="text"
                                           required="1"
                                           name="role_name">
                             </div>

                             <div class="form-group">
                               <label for="inputEmail4">Description<span style="color:red;">*</span></label>
                               <input value="<?php echo isset($row->role_description) ? $row->role_description : ''; ?>" class="form-control"
                                          type="text"
                                          required="1"
                                          name="role_description">
                            </div>

                                <button type="submit" class="btn btn-primary d-block mt-3">Submit</button>
                    <?php echo form_close(); ?>
            </div>
</div>
</div>
</div>
