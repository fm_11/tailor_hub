
<div class="col-12 mb-4">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title"><?php echo $title; ?></h5>

                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Role Name</th>
                                    <th scope="col">Description</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                              <?php
                              $i = 0;
                              foreach ($user_roles as $row):
                                  $i++;
                                  ?>
                                <tr>
                                    <th scope="row"><?php echo $i; ?></th>
                                    <td><?php echo $row->role_name; ?></td>
                                    <td><?php echo $row->role_description; ?></td>
                                    <td>

                                           <a href="<?php echo base_url(); ?>user_role_wise_privileges/index/<?php echo $row->id; ?>"
                                              title="Edit">
                                              <button type="button" class="btn btn-primary btn-xs mb-1">
                                                Chnage Privilege
                                              </button>
                                            </a>

                                            <a href="<?php echo base_url(); ?>user_roles/edit/<?php echo $row->id; ?>"
                                               title="Edit">
                                               <button type="button" class="btn btn-primary btn-xs mb-1">
                                                 Edit
                                               </button>
                                             </a>

                                           <a href="<?php echo base_url(); ?>user_roles/delete/<?php echo $row->id; ?>"
                                              onclick="return deleteConfirm()" title="Delete">
                                              <button type="button" class="btn btn-primary btn-xs mb-1">
                                                Delete
                                              </button>
                                            </a>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
