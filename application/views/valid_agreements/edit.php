

<div class="row">
            <div class="col-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <h5 class="mb-4"><?php echo $title; ?> <small style="color: red;">All Red marked are required</small></h5>

                            <form action="<?php echo base_url(); ?>valid_agreements/edit" method="post" enctype="multipart/form-data">
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                      <label for="inputEmail4">Name<span style="color:red;">*</span></label>
                                      <input type="text" class="form-control" id="name" required name="name" value="<?php echo $valid_agreement->name;?>" placeholder="Name">
                                        <input type="hidden" name="id"  class="smallInput wide" value="<?php echo $valid_agreement->id;  ?>">
                                   </div>

                                  </div>


                                <button type="submit" class="btn btn-primary d-block mt-3">Submit</button>
                    </form>
            </div>
</div>
</div>
</div>
