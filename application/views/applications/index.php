<div class="col-12 mb-4">
    <div class="card">
        <div class="card-body">
            <h5 class="card-title"><?php echo $title; ?></h5>

            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Applicatin ID</th>
                    <th scope="col">Student ID</th>
                    <th scope="col">Student Name</th>
                    <th scope="col">Country</th>
                    <th scope="col">Institute</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $i = (int)$this->uri->segment(3);
                foreach ($applications as $row):
                    $i++;
                    ?>
                    <tr>
                        <th scope="row"><?php echo $i; ?></th>
                        <td style="min-width: 101px;">
                          <a style="text-decoration:underline;"   href="<?php echo base_url(); ?>application_status/view_status/<?php echo $row['id']; ?>">
                            <?php echo $row['application_code']; ?>
                          </a>
                        </td>
                        <td><?php echo $row['applicant_code']; ?></td>
                        <td><?php echo $row['first_name'] . ' ' .$row['last_name']; ?></td>
                        <td><?php echo $row['country_name']; ?></td>
                        <td><?php echo $row['institute_name']; ?></td>

                        <td>

                          <a href="<?php echo base_url(); ?>applications/assign_process_officer/<?php echo $row['id']; ?>"
                             title="Edit">
                              <button type="button" class="btn btn-secondary btn-xs mb-1">
                                  Assign Process Officer
                              </button>
                          </a>


                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <?php echo $this->pagination->create_links(); ?>
        </div>
    </div>
</div>
