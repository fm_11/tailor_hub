

	<!-- Footer -->
	<footer class="bg3 p-t-75 p-b-32">
		<div class="container">
			<div class="row">

			     <div class="col-sm-6 col-lg-3 p-b-50 col-6">
					<h4 class="stext-301 cl0 p-b-30">Find Tailor</h4>
					<ul>
						<?php foreach ($divisions as $value): ?>
								<li class="p-b-10"><a href="<?php echo base_url(); ?>homes/tailor_area/<?php echo $value['id'];?>" class="cl7 hov-cl1 trans-04"><?php echo $value['name'];?></a></li>

						<?php endforeach; ?>
					</ul>
				</div>

				<div class="col-sm-6 col-lg-3 p-b-50 col-6">
					<h4 class="stext-301 cl0 p-b-30">Categories</h4>
					<ul>
						<li class="p-b-10"><a href="#" class="cl7 hov-cl1 trans-04">Tailor</a></li>
						<li class="p-b-10"><a href="#" class="cl7 hov-cl1 trans-04">Fashion House</a></li>
						<li class="p-b-10"><a href="#" class="cl7 hov-cl1 trans-04">Login</a></li>
						<li class="p-b-10"><a href="#" class="cl7 hov-cl1 trans-04">Register</a></li>
						<li class="p-b-10"><a href="#" class="cl7 hov-cl1 trans-04">News</a></li>
					</ul>
				</div>

				<div class="col-sm-6 col-lg-3 p-b-50 col-6">
					<h4 class="stext-301 cl0 p-b-30">
						Help
					</h4>

					<ul>
						<li class="p-b-10">
							<a href="#" class="cl7 hov-cl1 trans-04">
								Track Order
							</a>
						</li>

						<li class="p-b-10">
							<a href="#" class="cl7 hov-cl1 trans-04">
								Returns
							</a>
						</li>

						<li class="p-b-10">
							<a href="#" class="cl7 hov-cl1 trans-04">
								Shipping
							</a>
						</li>

						<li class="p-b-10">
							<a href="#" class="cl7 hov-cl1 trans-04">
								FAQs
							</a>
						</li>
					</ul>
				</div>

				<div class="col-sm-6 col-lg-3 p-b-50 col-6">
					<h4 class="stext-301 cl0 p-b-30">
						Address
					</h4>
          <?php if(!empty($contact_info)){?>
					<ul>
						<li class="p-b-10"><a href="#" class="cl7 hov-cl1 trans-04"><i class="fa fa-map-marker"></i> <?php echo $contact_info->address;?></a></li>
						<li class="p-b-10"><a href="#" class="cl7 hov-cl1 trans-04"><i class="fa fa-phone"></i> <?php echo $contact_info->phone;?></a></li>
						<li class="p-b-10"><a href="#" class="cl7 hov-cl1 trans-04"><i class="fa fa-envelope"></i> <?php echo $contact_info->email;?></a></li>
						<li class="p-b-10"><a href="#" class="cl7 hov-cl1 trans-04"><i class="fa fa-clock-o"></i> <?php echo date('h:i A', strtotime($contact_info->open_time)).' - '.date('h:i A', strtotime($contact_info->closed_time));?> (<?php echo $contact_info->open_day.' - '.$contact_info->closed_day;?>)</a></li>
					</ul>
        <?php }?>
					<!--<div class="p-t-27">
						<a href="#" class="fs-18 cl7 hov-cl1 trans-04 m-r-16">
							<i class="fa fa-facebook"></i>
						</a>

						<a href="#" class="fs-18 cl7 hov-cl1 trans-04 m-r-16">
							<i class="fa fa-instagram"></i>
						</a>

						<a href="#" class="fs-18 cl7 hov-cl1 trans-04 m-r-16">
							<i class="fa fa-pinterest-p"></i>
						</a>
					</div>-->
				</div>


			</div>

			<div class="p-t-40" style="border-top: 1px solid #555;">
				<!--<div class="flex-c-m flex-w p-b-18">
					<a href="#" class="m-all-1">
						<img src="images/icons/icon-pay-01.png" alt="ICON-PAY">
					</a>

					<a href="#" class="m-all-1">
						<img src="images/icons/icon-pay-02.png" alt="ICON-PAY">
					</a>

					<a href="#" class="m-all-1">
						<img src="images/icons/icon-pay-03.png" alt="ICON-PAY">
					</a>

					<a href="#" class="m-all-1">
						<img src="images/icons/icon-pay-04.png" alt="ICON-PAY">
					</a>

					<a href="#" class="m-all-1">
						<img src="images/icons/icon-pay-05.png" alt="ICON-PAY">
					</a>
				</div>-->

				<p class="cl6 txt-center">
					<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Made by <a href="#" target="_blank">DORJI HUB</a> &amp; distributed by <a href="#" target="_blank">Dorjihub Operation Team</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->

				</p>
			</div>
		</div>
	</footer>


	<!-- Back to top -->
	<div class="btn-back-to-top" id="myBtn">
		<span class="symbol-btn-back-to-top">
			<i class="zmdi zmdi-chevron-up"></i>
		</span>
	</div>


<!--===============================================================================================-->
	<script src="<?php echo base_url(); ?>media/website/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url(); ?>media/website/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url(); ?>media/website/vendor/bootstrap/js/popper.js"></script>
	<script src="<?php echo base_url(); ?>media/website/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url(); ?>media/website/vendor/select2/select2.min.js"></script>
	<script>
		$(".js-select2").each(function(){
			$(this).select2({
				minimumResultsForSearch: 20,
				dropdownParent: $(this).next('.dropDownSelect2')
			});
		})
	</script>
<!--===============================================================================================-->
	<script src="<?php echo base_url(); ?>media/website/vendor/daterangepicker/moment.min.js"></script>
	<script src="<?php echo base_url(); ?>media/website/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url(); ?>media/website/vendor/slick/slick.min.js"></script>
	<script src="<?php echo base_url(); ?>media/website/js/slick-custom.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url(); ?>media/website/vendor/parallax100/parallax100.js"></script>
	<script>
        $('.parallax100').parallax100();
	</script>
<!--===============================================================================================-->
	<script src="<?php echo base_url(); ?>media/website/vendor/MagnificPopup/jquery.magnific-popup.min.js"></script>
	<script>
		$('.gallery-lb').each(function() { // the containers for all your galleries
			$(this).magnificPopup({
		        delegate: 'a', // the selector for gallery item
		        type: 'image',
		        gallery: {
		        	enabled:true
		        },
		        mainClass: 'mfp-fade'
		    });
		});
	</script>
<!--===============================================================================================-->
	<script src="<?php echo base_url(); ?>media/website/vendor/isotope/isotope.pkgd.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url(); ?>media/website/vendor/sweetalert/sweetalert.min.js"></script>
	<script>
		$('.js-addwish-b2').on('click', function(e){
			e.preventDefault();
		});

		$('.js-addwish-b2').each(function(){
			var nameProduct = $(this).parent().parent().find('.js-name-b2').html();
			$(this).on('click', function(){
				swal(nameProduct, "is added to wishlist !", "success");

				$(this).addClass('js-addedwish-b2');
				$(this).off('click');
			});
		});

		$('.js-addwish-detail').each(function(){
			var nameProduct = $(this).parent().parent().parent().find('.js-name-detail').html();

			$(this).on('click', function(){
				swal(nameProduct, "is added to wishlist !", "success");

				$(this).addClass('js-addedwish-detail');
				$(this).off('click');
			});
		});

		/*---------------------------------------------*/

		$('.js-addcart-detail').each(function(){
			var nameProduct = $(this).parent().parent().parent().parent().find('.js-name-detail').html();
			$(this).on('click', function(){
				swal(nameProduct, "is added to cart !", "success");
			});
		});
	</script>
<!--===============================================================================================-->
	<script src="<?php echo base_url(); ?>media/website/vendor/perfect-scrollbar/perfect-scrollbar.min.js"></script>
	<script>
		$('.js-pscroll').each(function(){
			$(this).css('position','relative');
			$(this).css('overflow','hidden');
			var ps = new PerfectScrollbar(this, {
				wheelSpeed: 1,
				scrollingThreshold: 1000,
				wheelPropagation: false,
			});

			$(window).on('resize', function(){
				ps.update();
			})
		});
	</script>
<!--===============================================================================================-->
	<script src="<?php echo base_url(); ?>media/website/js/main.js"></script>

</body>
</html>
