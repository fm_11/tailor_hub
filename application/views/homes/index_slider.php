

<style>
.input-group.md-form.form-sm.form-1 input{
  border: 1px solid #bdbdbd;
  border-top-right-radius: 0.25rem;
  border-bottom-right-radius: 0.25rem;
}
.input-group.md-form.form-sm.form-2 input {
  border: 1px solid #bdbdbd;
  border-top-left-radius: 0.25rem;
  border-bottom-left-radius: 0.25rem;
}
.input-group.md-form.form-sm.form-2 input.red-border {
  border: 1px solid #e6e8d0;
}
.input-group.md-form.form-sm.form-2 input.lime-border {
  border: 1px solid #e6e8d0;
}
.input-group.md-form.form-sm.form-2 input.amber-border {
  border: 1px solid #e6e8d0;
}
.input-group {

    height: 50px;
    padding-bottom: 10px;
}
.search_icon{
	font-size: 40px;
   cursor: pointer;
}
</style>
	<!-- Slider -->


	<section class="section-slide">
		<div class="wrap-slick1">
			<div class="slick1">
				<?php if(!empty($contact_info)){?>
					<!-- <?php echo $value['banner_path'];?> -->
				<div class="item-slick1" style="background-image: url(<?php echo base_url(); ?>media/website/banner_image/<?php echo $contact_info->banner_path;?>);">
					<?php } ?>
					<div class="container h-full">
						<div class="flex-col-c-m h-full p-t-100 p-b-30 respon5">


							<div class="layer-slick1 animated visible-false" data-appear="rotateInUpRight" data-delay="800">
								<h2 class="ltext-104s txt-center cl0 p-t-22 p-b-40 respon1">
									Find a Great Tailor in Your Area
								</h2>
							</div>
							<form action="<?php echo base_url(); ?>homes/tailor_area/0" method="post">
							<div class="input-group md-form form-sm form-2 pl-0 layer-slick1 animated visible-false" data-appear="fadeIn" data-delay="1600">

							  <input class="form-control my-0 py-1 lime-border" type="text" required name="search" placeholder="search area" maxlength="50" aria-label="Search">

							  <div class="input-group-append">
									<button type="submit">
							    <span class="input-group-text lime lighten-2" id="basic-text1"><i class="fa fa-search text-grey search_icon"
							        aria-hidden="true"></i></span>
											</button>
							  </div>

							</div>
						 </form>
							<div class="layer-slick1 animated visible-false" data-appear="fadeIn" data-delay="2000">
								<?php foreach ($divisions as $value): ?>
										<a href="<?php echo base_url(); ?>homes/tailor_area/<?php echo $value['id'];?>" class="btn btn-success btn-lg" style="margin:5px;"><?php echo $value['name'];?></a>
								<?php endforeach; ?>

								<!-- <a href="#" class="btn btn-success btn-lg" style="margin:5px;">Chittagong</a>
								<a href="#" class="btn btn-success btn-lg" style="margin:5px;">Barishal</a>
								<a href="#" class="btn btn-success btn-lg" style="margin:5px;">Khulna</a>
								<a href="#" class="btn btn-success btn-lg" style="margin:5px;">Mymensingh</a>
								<a href="#" class="btn btn-success btn-lg" style="margin:5px;">Rajshahi</a>
								<a href="#" class="btn btn-success btn-lg" style="margin:5px;">Rangpur</a>
								<a href="#" class="btn btn-success btn-lg" style="margin:5px;">Sylhet</a> -->
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</section>
