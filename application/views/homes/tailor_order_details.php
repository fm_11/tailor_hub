


<?php echo $index_header; ?>
<?php echo $main_menu; ?>
<?php echo $mobile_menu; ?>
<?php echo $index_sidebar; ?>

	<!-- Title page -->
	<section class="bg-img1 txt-center p-lr-15 p-tb-50" style="background-image: url('<?php echo base_url(); ?>media/website/images/bg-01.jpg');">
		<h2 class="ltext-105 cl0 txt-center">
			Tailor Order
		</h2>
	</section>
	<div class="container" style="align: left">
    <br><br>
    <h4 >
			Order Details
		</h4>
    <br><br>
    <?php
    $i = (int)$this->uri->segment(3);
    foreach ($tailor_order as $row):
        $i++;
        ?>

          <p><b>Customer name: <?php echo $row['customer_name']; ?></b></p>
          <p>Phone: <?php echo $row['customer_phone']; ?></p>
          <p>Email: <?php echo $row['customer_email']; ?></p>
          <p>Address: <?php echo $row['shipping_address']; ?></p>

      <?php endforeach; ?>
  </div>

	<!-- Content page -->
	<div class="sec-banner bg0 p-t-40 p-b-55">
		<div class="container">
			<table class="table">
					<thead>
							<tr>
									<th scope="col">#</th>
									<th scope="col">Product</th>
									<th scope="col">Quantity</th>
									<th scope="col">Price</th>
									<th scope="col">Total Price</th>
							</tr>
					</thead>
					<tbody>
						<?php
						$i = (int)$this->uri->segment(3);
						foreach ($tailor_order_details as $row):
								$i++;
								?>
							<tr>
									<th scope="row"><?php echo $i; ?></th>
									<td><?php echo $row['item_image_path']; ?></td>
									<td><?php echo $row['quantity']; ?></td>
									<td><?php echo $row['item_cost']; ?></td>
									<td><?php echo $row['total_cost']; ?></td>
							</tr>
							<?php endforeach; ?>
					</tbody>
			</table>
		</div>
		<div class="container">
			<table class="table" style="align: right">
				<tr>
					<td>Sub Total:<?php echo $row['total_cost']; ?></td>
				</tr>
				<tr>
					<td>Delivery Cost:</td>
				</tr>
				<tr>
					<td>Total:</td>
				</tr>
			</table>
		</div>
	</div>


<?php echo $index_footer; ?>
