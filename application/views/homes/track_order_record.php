


<?php echo $index_header; ?>
<?php echo $main_menu; ?>
<?php echo $mobile_menu; ?>
<?php echo $index_sidebar; ?>

	<!-- Title page -->
	<section class="bg-img1 txt-center p-lr-15 p-tb-50" style="background-image: url('<?php echo base_url(); ?>media/website/images/bg-01.jpg');">
		<h2 class="ltext-105 cl0 txt-center">
			Track Order Record
		</h2>
	</section>

	<!-- <div class="container">
			<div class="bread-crumb flex-w p-l-25 p-r-15 p-t-30 p-lr-0-lg">
				<a href="<?php echo base_url(); ?>homes" class="stext-109 cl8 hov-cl1 trans-04">
					Home <i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
				</a>

			</div>
	</div> -->


	<!-- Content page -->
	<div class="sec-banner bg0 p-t-40 p-b-55">
		<div class="container">

				<p align="left" class="titleblock">Track Order Record List</p>

			<div class="row" style="border: 1px solid #0923d8;padding: 10px;">


        <table class="table table-hover">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Tailor Name</th>
              <th scope="col">Customer Name</th>
              <th scope="col">Order Date</th>
              <th scope="col">Phone</th>
              <th scope="col">Address</th>
              <th scope="col">Total Amount</th>
              <th scope="col">Order Status</th>
            </tr>
          </thead>
          <tbody>
            
            <?php (int)$row=1; foreach ($order_track_list as  $value): ?>
              <tr>
                <th scope="row"><?php echo $row;?></th>
                <td><?php echo $value['tailor_name'];?></td>
                <td><?php echo $value['customer_name'];?></td>
                <td><?php echo $value['booking_date'];?></td>
                <td><?php echo $value['customer_phone'];?></td>
                <td><?php echo $value['shipping_address'];?></td>
                <td><?php echo $value['total_cost'];?></td>
                <td>

                  <?php if($row['status']=='R')
                  {
                    echo "Rejected";
                  }elseif ($row['status']=='D') {
                    echo "Delivered";
                  }
                  elseif ($row['status']=='O'){
                    echo "On Process";
                    }
                  else{
                      echo "Pending";
                   } ?>

                </td>

              </tr>
                <?php $row=$row+1; ?>
            <?php endforeach; ?>


          </tbody>
        </table>


			</div>
		</div>
	</div>


<?php echo $index_footer; ?>
