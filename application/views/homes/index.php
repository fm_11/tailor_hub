
<?php echo $index_header; ?>
<?php echo $main_menu; ?>
<?php echo $mobile_menu; ?>
<?php echo $index_sidebar; ?>
<?php echo $index_slider; ?>

	<!-- Banner -->
	<div class="sec-banner bg0 p-t-95 p-b-20">
		<div class="container">

				<p align="center" class="titleblock">Find Your Favorite Tailor</p>

	   <div class="row">
     <?php foreach ($divisions as $value): ?>
       <div class="col-md-3">
         <div class="card bg-dark text-white ovrs"><a href="<?php echo base_url(); ?>homes/tailor_area/<?php echo $value['id'];?>">
           <img class="card-img" src="<?php echo base_url(); ?>media/website/images/division/<?php echo $value['image_path'];?>" alt="Card image">
           <div class="card-img-overlay divs">
           <h5 class="card-title"><?php echo $value['name'];?></h5>
           </div>
          </a></div>
       </div>
     <?php endforeach; ?>
		</div>

		</div>
	</div>

	<!-- <div class="sec-banner bg0 p-b-55">
		<div class="container">

				<p align="center" class="titleblock">Top Deals</p>

			<div class="row">

				<div class="col-md-3 col-lg-3 p-b-30 m-lr-auto">

					<div class="block1 wrap-pic-w">
						<img src="<?php echo base_url(); ?>media/website/images/banner-07.jpg" alt="IMG-BANNER">

						<a href="product.html" class="block1-txt ab-t-l s-full flex-col-l-sb p-lr-38 p-tb-34 trans-03 respon3">
							<div class="block1-txt-child1 flex-col-l">
								<span class="block1-name ltext-102 trans-04 p-b-8">
									Watches
								</span>

								<span class="block1-info stext-102 trans-04">
									Spring 2018
								</span>
							</div>

							<div class="block1-txt-child2 p-b-4 trans-05">
								<div class="block1-link stext-101 cl0 trans-09">
									Shop Now
								</div>
							</div>
						</a>
					</div>
				</div>

				<div class="col-md-3 col-lg-3 p-b-30 m-lr-auto">

					<div class="block1 wrap-pic-w">
						<img src="<?php echo base_url(); ?>media/website/images/banner-08.jpg" alt="IMG-BANNER">

						<a href="product.html" class="block1-txt ab-t-l s-full flex-col-l-sb p-lr-38 p-tb-34 trans-03 respon3">
							<div class="block1-txt-child1 flex-col-l">
								<span class="block1-name ltext-102 trans-04 p-b-8">
									Bags
								</span>

								<span class="block1-info stext-102 trans-04">
									Spring 2018
								</span>
							</div>

							<div class="block1-txt-child2 p-b-4 trans-05">
								<div class="block1-link stext-101 cl0 trans-09">
									Shop Now
								</div>
							</div>
						</a>
					</div>
				</div>

				<div class="col-md-3 col-lg-3 p-b-30 m-lr-auto">

					<div class="block1 wrap-pic-w">
						<img src="<?php echo base_url(); ?>media/website/images/banner-09.jpg" alt="IMG-BANNER">

						<a href="product.html" class="block1-txt ab-t-l s-full flex-col-l-sb p-lr-38 p-tb-34 trans-03 respon3">
							<div class="block1-txt-child1 flex-col-l">
								<span class="block1-name ltext-102 trans-04 p-b-8">
									Accessories
								</span>

								<span class="block1-info stext-102 trans-04">
									Spring 2018
								</span>
							</div>

							<div class="block1-txt-child2 p-b-4 trans-05">
								<div class="block1-link stext-101 cl0 trans-09">
									Shop Now
								</div>
							</div>
						</a>
					</div>
				</div>

				<div class="col-md-3 col-lg-3 p-b-30 m-lr-auto">

					<div class="block1 wrap-pic-w">
						<img src="<?php echo base_url(); ?>media/website/images/banner-08.jpg" alt="IMG-BANNER">

						<a href="product.html" class="block1-txt ab-t-l s-full flex-col-l-sb p-lr-38 p-tb-34 trans-03 respon3">
							<div class="block1-txt-child1 flex-col-l">
								<span class="block1-name ltext-102 trans-04 p-b-8">
									Bags
								</span>

								<span class="block1-info stext-102 trans-04">
									Spring 2018
								</span>
							</div>

							<div class="block1-txt-child2 p-b-4 trans-05">
								<div class="block1-link stext-101 cl0 trans-09">
									Shop Now
								</div>
							</div>
						</a>
					</div>
				</div>

				<div class="col-md-3 col-lg-3 p-b-30 m-lr-auto">

					<div class="block1 wrap-pic-w">
						<img src="<?php echo base_url(); ?>media/website/images/banner-07.jpg" alt="IMG-BANNER">

						<a href="product.html" class="block1-txt ab-t-l s-full flex-col-l-sb p-lr-38 p-tb-34 trans-03 respon3">
							<div class="block1-txt-child1 flex-col-l">
								<span class="block1-name ltext-102 trans-04 p-b-8">
									Watches
								</span>

								<span class="block1-info stext-102 trans-04">
									Spring 2018
								</span>
							</div>

							<div class="block1-txt-child2 p-b-4 trans-05">
								<div class="block1-link stext-101 cl0 trans-09">
									Shop Now
								</div>
							</div>
						</a>
					</div>
				</div>

				<div class="col-md-3 col-lg-3 p-b-30 m-lr-auto">

					<div class="block1 wrap-pic-w">
						<img src="<?php echo base_url(); ?>media/website/images/banner-08.jpg" alt="IMG-BANNER">

						<a href="product.html" class="block1-txt ab-t-l s-full flex-col-l-sb p-lr-38 p-tb-34 trans-03 respon3">
							<div class="block1-txt-child1 flex-col-l">
								<span class="block1-name ltext-102 trans-04 p-b-8">
									Bags
								</span>

								<span class="block1-info stext-102 trans-04">
									Spring 2018
								</span>
							</div>

							<div class="block1-txt-child2 p-b-4 trans-05">
								<div class="block1-link stext-101 cl0 trans-09">
									Shop Now
								</div>
							</div>
						</a>
					</div>
				</div>

				<div class="col-md-3 col-lg-3 p-b-30 m-lr-auto">

					<div class="block1 wrap-pic-w">
						<img src="<?php echo base_url(); ?>media/website/images/banner-09.jpg" alt="IMG-BANNER">

						<a href="product.html" class="block1-txt ab-t-l s-full flex-col-l-sb p-lr-38 p-tb-34 trans-03 respon3">
							<div class="block1-txt-child1 flex-col-l">
								<span class="block1-name ltext-102 trans-04 p-b-8">
									Accessories
								</span>

								<span class="block1-info stext-102 trans-04">
									Spring 2018
								</span>
							</div>

							<div class="block1-txt-child2 p-b-4 trans-05">
								<div class="block1-link stext-101 cl0 trans-09">
									Shop Now
								</div>
							</div>
						</a>
					</div>
				</div>

				<div class="col-md-3 col-lg-3 p-b-30 m-lr-auto">

					<div class="block1 wrap-pic-w">
						<img src="<?php echo base_url(); ?>media/website/images/banner-08.jpg" alt="IMG-BANNER">

						<a href="product.html" class="block1-txt ab-t-l s-full flex-col-l-sb p-lr-38 p-tb-34 trans-03 respon3">
							<div class="block1-txt-child1 flex-col-l">
								<span class="block1-name ltext-102 trans-04 p-b-8">
									Bags
								</span>

								<span class="block1-info stext-102 trans-04">
									Spring 2018
								</span>
							</div>

							<div class="block1-txt-child2 p-b-4 trans-05">
								<div class="block1-link stext-101 cl0 trans-09">
									Shop Now
								</div>
							</div>
						</a>
					</div>
				</div>


				<div class="col-md-12"><p align="right" class="btncst"><a class="btn btn-md" href="#">Explore All Tailor</a></p></div>
			

			</div>
		</div>
	</div> -->

	<div class="sec-banner bg0 p-b-55">
		<div class="container">

				<p align="center" class="titleblocks">Designed Lifestyle</p>
		<div class="wrap-slick2">
							<div class="slick2">
               <?php foreach ($design_lifestyle as $value): ?>
								 <div class="item-slick2 p-l-15 p-r-15 p-t-15 p-b-15">
 									<div class="block2">
 										<div class="block2-pic hov-img0 imgd"><img src="<?php echo base_url(); ?>media/tailor_item/<?php echo $value; ?>" alt="IMG-PRODUCT"></div>
 									</div>
 								</div>
               <?php endforeach; ?>


								<!-- <div class="item-slick2 p-l-15 p-r-15 p-t-15 p-b-15">
									<div class="block2">
										<div class="block2-pic hov-img0 imgd"><img src="<?php echo base_url(); ?>media/website/images/t2.jpg" alt="IMG-PRODUCT"></div>
									</div>
								</div>

								<div class="item-slick2 p-l-15 p-r-15 p-t-15 p-b-15">
									<div class="block2">
										<div class="block2-pic hov-img0 imgd"><img src="<?php echo base_url(); ?>media/website/images/t3.jpg" alt="IMG-PRODUCT"></div>
									</div>
								</div>

								<div class="item-slick2 p-l-15 p-r-15 p-t-15 p-b-15">
									<div class="block2">
										<div class="block2-pic hov-img0 imgd"><img src="<?php echo base_url(); ?>media/website/images/t4.jpg" alt="IMG-PRODUCT"></div>
									</div>
								</div>

								<div class="item-slick2 p-l-15 p-r-15 p-t-15 p-b-15">
									<div class="block2">
										<div class="block2-pic hov-img0 imgd"><img src="<?php echo base_url(); ?>media/website/images/t1.jpg" alt="IMG-PRODUCT"></div>
									</div>
								</div> -->



							</div>
						</div>

		</div>
	</div>

	<div class="sec-banner bg0 p-b-55">
		<div class="container">

		<p align="center" class="titleblocks">Our Milestone</p><br />
		<?php if(!empty($milestones)){?>
		<div class="row">
						<div class="col-lg-3 col-md-3 col-12">
							<div class="text-content sts">
								<h2 align="center"><?php echo $milestones[0]['first_number'];?>+</h2>
								<p align="center"><?php echo $milestones[0]['first_label'];?></p>
							</div>
						</div>

						<div class="col-lg-3 col-md-3 col-12">
							<div class="text-content sts">
								<h2 align="center"><?php echo $milestones[0]['second_number'];?>+</h2>
								<p align="center"><?php echo $milestones[0]['second_label'];?></p>
							</div>
						</div>

						<div class="col-lg-3 col-md-3 col-12">
							<div class="text-content sts">
								<h2 align="center"><?php echo $milestones[0]['third_number'];?>+</h2>
								<p align="center"><?php echo $milestones[0]['third_label'];?></p>
							</div>
						</div>

						<div class="col-lg-3 col-md-3 col-12">
							<div class="text-content sts">
								<h2 align="center"><?php echo $milestones[0]['fourth_number'];?>+</h2>
								<p align="center"><?php echo $milestones[0]['fourth_label'];?></p>
							</div>
						</div>

					</div>
				<?php }?>

		</div>
	</div>

	<div class="sec-banner bg0 p-b-55">
		<div class="container">

			<p align="center" class="titleblock">Customer Review</p><br /><br />


	<div class="row">
		<div class="col-sm-12">
			<div id="myCarousel" class="carousel slide" data-ride="carousel">
				<!-- Carousel indicators -->
				<ol class="carousel-indicators">
					<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
					<li data-target="#myCarousel" data-slide-to="1"></li>
					<li data-target="#myCarousel" data-slide-to="2"></li>
				</ol>

				<!-- Wrapper for carousel items -->
				<?php if(!empty($customer_review)){?>
				<div class="carousel-inner">
					<?php  for((int)$row=0; $row<count($customer_review); $row=$row+2){?>
					<div class="carousel-item <?php if($row==0){echo "active";} ?> ">
						<div class="row">
						<?php if(isset($customer_review[$row])){?>
							 <div class="col-sm-6" style="margin-bottom: 20px;">
								 <div class="testimonial-wrapper">
									 <div class="testimonial"><?php echo $customer_review[$row]['description'];?></div>
									 <div class="media">
										 <img src="<?php echo base_url(); ?>media/customer_review/<?php echo $customer_review[$row]['image_path'];?>" class="mr-3" alt="">
										 <div class="media-body">
											 <div class="overview">
												 <div class="name"><b><?php echo $customer_review[$row]['name'];?></b></div>
												 <!-- <div class="details">Media Analyst / SkyNet</div> -->
												 <div class="star-rating">
													 <ul class="list-inline">
														 <li class="list-inline-item"><i class="fa fa-star"></i></li>
														 <li class="list-inline-item"><i class="fa fa-star"></i></li>
														 <li class="list-inline-item"><i class="fa fa-star"></i></li>
														 <li class="list-inline-item"><i class="fa fa-star"></i></li>
														 <li class="list-inline-item"><i class="fa fa-star-half-o"></i></li>
													 </ul>
												 </div>
											 </div>
										 </div>
									 </div>
								 </div>
							 </div>
						 <?php } ?>
							 <?php if(isset($customer_review[$row+1])){?>
							 <div class="col-sm-6" style="margin-bottom: 20px;">
							 	<div class="testimonial-wrapper">
							 		<div class="testimonial"><?php echo $customer_review[$row+1]['description'];?></div>
							 		<div class="media">
							 			<img src="<?php echo base_url(); ?>media/customer_review/<?php echo $customer_review[$row+1]['image_path'];?>" class="mr-3" alt="">
							 			<div class="media-body">
							 				<div class="overview">
							 					<div class="name"><b><?php echo $customer_review[$row+1]['name'];?></b></div>
							 					<!-- <div class="details">Media Analyst / SkyNet</div> -->
							 					<div class="star-rating">
							 						<ul class="list-inline">
							 							<li class="list-inline-item"><i class="fa fa-star"></i></li>
							 							<li class="list-inline-item"><i class="fa fa-star"></i></li>
							 							<li class="list-inline-item"><i class="fa fa-star"></i></li>
							 							<li class="list-inline-item"><i class="fa fa-star"></i></li>
							 							<li class="list-inline-item"><i class="fa fa-star-half-o"></i></li>
							 						</ul>
							 					</div>
							 				</div>
							 			</div>
							 		</div>
							 	</div>
							 </div>
						 <?php } ?>
						</div>
					</div>

				<?php  }?>
				</div>
			<?php }?>




			</div>
		</div>
	</div><br /><br />



		</div>
	</div>

	<div class="sec-banner bg0 p-b-55">
		<div class="container">

				<p align="center" class="titleblock">Connect With Us</p>

				<div class="row">

				<div class="col-md-4">

				<p align="center"><iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FdorjihubBD%2F&tabs=timeline&width=300&height=300&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="100%" height="300" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe></p>
				</div>

				<div class="col-md-8">
				<img src="<?php echo base_url(); ?>media/website/images/map.png" style="height:300px;width:100%"/>
				</div>

				</div>


		</div>
	</div>

	<div class="sec-banner bg0 p-b-55">
		<div class="container">

				<p align="center" class="titleblock">News Coverage</p>

				<div class="row">

          <?php foreach ($news as $value): ?>
            <div class="col-sm-6 col-md-4 p-b-40">
              <div class="blog-item">
                <div class="hov-img0">
                  <a href="<?php echo base_url(); ?>homes/blog_details/<?php echo $value['id'];?>">
                    <img src="<?php echo base_url(); ?>media/website/images/news/<?php echo $value['image_path'];?>" alt="IMG-BLOG">
                  </a>
                </div>

                <div class="p-t-15">
                  <div class="stext-107 flex-w p-b-14">
                    <span class="m-r-3">
                      <!-- <span class="cl4">
                        By
                      </span>

                      <span class="cl5">
                        Nancy Ward
                      </span>
                    </span> -->

                    <span>
                      <!-- <span class="cl4">
                        on
                      </span> -->

                      <span class="cl5">
                        <!-- July 22, 2017 -->
                        <?php echo date('jS F Y', strtotime($value['date']));?>
                      </span>
                    </span>
                  </div>

                  <h4 class="p-b-12">
                    <a href="<?php echo base_url(); ?>homes/blog_details/<?php echo $value['id'];?>" class="mtext-101 cl2 hov-cl1 trans-04">
                    <?php echo $value['title'];?>
                    </a>
                  </h4>

                  <p class="stext-108 cl6">
                  <?php echo $value['description'];?>
                  </p>
                </div>
              </div>
            </div>
          <?php endforeach; ?>

			</div>


		</div>
	</div>

	<div class="sec-banner bg0 p-b-55" style="background: #acadaf17;">
		<div class="container">
			<div class="row"><div class="col-md-2"></div><div class="col-md-8"><p align="center"><br /><br /><img src="<?php echo base_url(); ?>media/website/images/pay.png" style="width:100%"/></p></div></div>
		</div>
	</div>



	<!-- Product -->





	<!-- Modal1 -->
	<div class="wrap-modal1 js-modal1 p-t-60 p-b-20">
		<div class="overlay-modal1 js-hide-modal1"></div>

		<div class="container">
			<div class="bg0 p-t-60 p-b-30 p-lr-15-lg how-pos3-parent">
				<button class="how-pos3 hov3 trans-04 js-hide-modal1">
					<img src="<?php echo base_url(); ?>media/website/images/icons/icon-close.png" alt="CLOSE">
				</button>

				<div class="row">
					<div class="col-md-6 col-lg-7 p-b-30">
						<div class="p-l-25 p-r-30 p-lr-0-lg">
							<div class="wrap-slick3 flex-sb flex-w">
								<div class="wrap-slick3-dots"></div>
								<div class="wrap-slick3-arrows flex-sb-m flex-w"></div>

								<div class="slick3 gallery-lb">
									<div class="item-slick3" data-thumb="<?php echo base_url(); ?>media/website/images/product-detail-01.jpg">
										<div class="wrap-pic-w pos-relative">
											<img src="<?php echo base_url(); ?>media/website/images/product-detail-01.jpg" alt="IMG-PRODUCT">

											<a class="flex-c-m size-108 how-pos1 bor0 fs-16 cl10 bg0 hov-btn3 trans-04" href="<?php echo base_url(); ?>media/website/images/product-detail-01.jpg">
												<i class="fa fa-expand"></i>
											</a>
										</div>
									</div>

									<div class="item-slick3" data-thumb="<?php echo base_url(); ?>media/website/images/product-detail-02.jpg">
										<div class="wrap-pic-w pos-relative">
											<img src="<?php echo base_url(); ?>media/website/images/product-detail-02.jpg" alt="IMG-PRODUCT">

											<a class="flex-c-m size-108 how-pos1 bor0 fs-16 cl10 bg0 hov-btn3 trans-04" href="<?php echo base_url(); ?>media/website/images/product-detail-02.jpg">
												<i class="fa fa-expand"></i>
											</a>
										</div>
									</div>

									<div class="item-slick3" data-thumb="<?php echo base_url(); ?>media/website/images/product-detail-03.jpg">
										<div class="wrap-pic-w pos-relative">
											<img src="<?php echo base_url(); ?>media/website/images/product-detail-03.jpg" alt="IMG-PRODUCT">

											<a class="flex-c-m size-108 how-pos1 bor0 fs-16 cl10 bg0 hov-btn3 trans-04" href="<?php echo base_url(); ?>media/website/images/product-detail-03.jpg">
												<i class="fa fa-expand"></i>
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-6 col-lg-5 p-b-30">
						<div class="p-r-50 p-t-5 p-lr-0-lg">
							<h4 class="mtext-105 cl2 js-name-detail p-b-14">
								Lightweight Jacket
							</h4>

							<span class="mtext-106 cl2">
								$58.79
							</span>

							<p class="stext-102 cl3 p-t-23">
								Nulla eget sem vitae eros pharetra viverra. Nam vitae luctus ligula. Mauris consequat ornare feugiat.
							</p>

							<!--  -->
							<div class="p-t-33">
								<div class="flex-w flex-r-m p-b-10">
									<div class="size-203 flex-c-m respon6">
										Size
									</div>

									<div class="size-204 respon6-next">
										<div class="rs1-select2 bor8 bg0">
											<select class="js-select2" name="time">
												<option>Choose an option</option>
												<option>Size S</option>
												<option>Size M</option>
												<option>Size L</option>
												<option>Size XL</option>
											</select>
											<div class="dropDownSelect2"></div>
										</div>
									</div>
								</div>

								<div class="flex-w flex-r-m p-b-10">
									<div class="size-203 flex-c-m respon6">
										Color
									</div>

									<div class="size-204 respon6-next">
										<div class="rs1-select2 bor8 bg0">
											<select class="js-select2" name="time">
												<option>Choose an option</option>
												<option>Red</option>
												<option>Blue</option>
												<option>White</option>
												<option>Grey</option>
											</select>
											<div class="dropDownSelect2"></div>
										</div>
									</div>
								</div>

								<div class="flex-w flex-r-m p-b-10">
									<div class="size-204 flex-w flex-m respon6-next">
										<div class="wrap-num-product flex-w m-r-20 m-tb-10">
											<div class="btn-num-product-down cl8 hov-btn3 trans-04 flex-c-m">
												<i class="fs-16 zmdi zmdi-minus"></i>
											</div>

											<input class="mtext-104 cl3 txt-center num-product" type="number" name="num-product" value="1">

											<div class="btn-num-product-up cl8 hov-btn3 trans-04 flex-c-m">
												<i class="fs-16 zmdi zmdi-plus"></i>
											</div>
										</div>

										<button class="flex-c-m stext-101 cl0 size-101 bg1 bor1 hov-btn1 p-lr-15 trans-04 js-addcart-detail">
											Add to cart
										</button>
									</div>
								</div>
							</div>

							<!--  -->
							<div class="flex-w flex-m p-l-100 p-t-40 respon7">
								<div class="flex-m bor9 p-r-10 m-r-11">
									<a href="#" class="fs-14 cl3 hov-cl1 trans-04 lh-10 p-lr-5 p-tb-2 js-addwish-detail tooltip100" data-tooltip="Add to Wishlist">
										<i class="zmdi zmdi-favorite"></i>
									</a>
								</div>

								<a href="#" class="fs-14 cl3 hov-cl1 trans-04 lh-10 p-lr-5 p-tb-2 m-r-8 tooltip100" data-tooltip="Facebook">
									<i class="fa fa-facebook"></i>
								</a>

								<a href="#" class="fs-14 cl3 hov-cl1 trans-04 lh-10 p-lr-5 p-tb-2 m-r-8 tooltip100" data-tooltip="Twitter">
									<i class="fa fa-twitter"></i>
								</a>

								<a href="#" class="fs-14 cl3 hov-cl1 trans-04 lh-10 p-lr-5 p-tb-2 m-r-8 tooltip100" data-tooltip="Google Plus">
									<i class="fa fa-google-plus"></i>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php echo $index_footer; ?>
