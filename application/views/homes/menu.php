


<!-- Header -->
	<header class="header-v4">
		<!-- Header desktop -->
		<div class="container-menu-desktop">
			<!-- Topbar -->


			<div class="wrap-menu-desktop how-shadow1">
				<nav class="limiter-menu-desktop container">

					<!-- Logo desktop -->
					<a href="index.php" class="logo">
						<img src="<?php echo base_url(); ?>media/website/images/logo.png" alt="Official-LOGO of Dorji Hub" style="max-height: 90px;">
					</a>

					<!-- Menu desktop -->
					<div class="menu-desktop">
						<ul class="main-menu">
							<!--<li>
								<a href="index.html">Home</a>
								<ul class="sub-menu">
									<li><a href="index.html">Homepage 1</a></li>
									<li><a href="home-02.html">Homepage 2</a></li>
									<li><a href="home-03.html">Homepage 3</a></li>
								</ul>
							</li>-->

							<li class="label1" data-label1="hot">
								<a href="#">Tailor</a>
							</li>

							<li>
								<a href="#">Fashion House</a>
							</li>

							<!--<li>
								<a href="blog.html">Blog</a>
							</li>

							<li>
								<a href="about.html">About</a>
							</li>

							<li>
								<a href="contact.html">Contact</a>
							</li>-->
						</ul>
					</div>

					<!-- Icon header -->
					<div class="wrap-icon-header flex-w flex-r-m">

					<ul class="main-menu">
<!--
							<li>
								<a href="#"><i class="fa fa-sign-in"></i> Login</a>
							</li>

							<li>
								<a href="#"><i class="fa fa-user"></i> Register</a>
							</li>

							<li>
								<a href="#">News</a>
							</li> -->
							<li>
									 <button type="button" class="btn btn-dark default btn-sm mb-1" data-toggle="modal" data-target="#myapplication_status"> Order Tracking</button>
							</li>
						</ul>
<!--
						<div class="icon-header-item cl2 hov-cl1 trans-04 p-l-22 p-r-11 js-show-modal-search">
							<i class="zmdi zmdi-search"></i>
						</div>

						<div class="icon-header-item cl2 hov-cl1 trans-04 p-l-22 p-r-11 icon-header-noti js-show-cart" data-notify="2">
							<i class="zmdi zmdi-shopping-cart"></i>
						</div>

						<a href="#" class="icon-header-item cl2 hov-cl1 trans-04 p-l-22 p-r-11 icon-header-noti" data-notify="0">
							<i class="zmdi zmdi-favorite-outline"></i>
						</a> -->
					</div>
				</nav>
			</div>
		</div>
		<!-- Modal -->
		<div id="myapplication_status" class="modal fade" role="dialog" style="margin-top: 50px;">
				<div class="modal-dialog">

						<!-- Modal content-->
						<div class="modal-content">
								<div class="modal-header">
										<h4 class="modal-title">Track Order Record</h4>
										<button type="button" class="close" data-dismiss="modal">&times;</button>

								</div>
								<div class="modal-body">
											<form action="<?php echo base_url(); ?>homes/track_order_record/" method="post" enctype="multipart/form-data">

												<div class="form-row">
														<div class="form-group col-md-12">
															<input type="text" name="order_track" class="form-control" placeholder="please type your order no." required/>

														</div>
												</div>
												<div class="modal-footer">
														<button type="button" class="btn btn-danger d-block mt-3" data-dismiss="modal">Close</button>
														<button type="submit" class="btn btn-success d-block mt-3">Search</button>
												</div>
										</form>
								</div>
						</div>

				</div>
		</div>
