


<?php echo $index_header; ?>
<?php echo $main_menu; ?>
<?php echo $mobile_menu; ?>
<?php echo $index_sidebar; ?>

	<!-- Title page -->
	<section class="bg-img1 txt-center p-lr-15 p-tb-50" style="background-image: url('<?php echo base_url(); ?>media/website/images/bg-01.jpg');">
		<h2 class="ltext-105 cl0 txt-center">
			Tailor in <?php if(!empty($tailor_list)){ echo $tailor_list[0]['area_name'];}?>
		</h2>
	</section>

	<div class="container">
			<div class="bread-crumb flex-w p-l-25 p-r-15 p-t-30 p-lr-0-lg">
				<a href="<?php echo base_url(); ?>homes" class="stext-109 cl8 hov-cl1 trans-04">
					Home <i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
				</a>
				<?php if(!empty($tailor_list)){?>
				<a href="<?php echo base_url(); ?>homes/tailor_area/<?php echo $tailor_list[0]['division_id']; ?>" class="stext-109 cl8 hov-cl1 trans-04">
					<?php echo $tailor_list[0]['division_name'];?> <i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
				</a>
				<span class="stext-109 cl4"> 	<?php echo $tailor_list[0]['area_name'];?> </span>
			<?php }?>
			</div>
	</div>


	<!-- Content page -->
	<div class="sec-banner bg0 p-t-40 p-b-55">
		<div class="container">

				<p align="left" class="titleblock">Find Tailor</p>

			<div class="row">
        <?php foreach ($tailor_list as $value): ?>
					<div class="col-md-3 col-lg-3 col-sm-6">
					 <a href="<?php echo base_url(); ?>homes/tailor_profile/<?php echo $value['id'];?>"><div class="tailorlist"><img src="<?php echo base_url(); ?>media/tailor/<?php echo $value['image_path']; ?>"/>
					 <h6><i class="fa fa-id-card-o"></i>&emsp;<?php echo $value['name'];?> </h6></div></a>
				  </div>
        <?php endforeach; ?>


			   <!-- <div class="col-md-3 col-lg-3 col-sm-6">
				 <a href="tailor_profile.php"><div class="tailorlist"><img src="http://localhost/projects/dorjihub/images/t3.jpg"/>
				 <h6><i class="fa fa-id-card-o"></i>&emsp;Elegant Shop </h6></div></a>
			  </div>

			   <div class="col-md-3 col-lg-3 col-sm-6">
				 <a href="tailor_profile.php"><div class="tailorlist"><img src="https://live.staticflickr.com/4910/31030442847_2dc9041882_b.jpg"/>
				 <h6><i class="fa fa-id-card-o"></i>&emsp;Elegant Shop </h6></div></a>
			  </div> -->


			</div>
		</div>
	</div>


<?php echo $index_footer; ?>
