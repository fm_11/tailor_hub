

<?php echo $index_header; ?>
<?php echo $main_menu; ?>
<?php echo $mobile_menu; ?>
<?php echo $index_sidebar; ?>
<style>
   .required_class{
     color: red;
   }
</style>
	<!-- Title page -->
	<section class="bg-img1 txt-center p-lr-15 p-tb-50" style="background-image: url('<?php echo base_url(); ?>media/website/images/bg-01.jpg');">
		<h2 class="ltext-105 cl0 txt-center">
			<?php echo $tailor_info->name;?>
		</h2>
	</section>


    <div class="sec-banner bg0">
		<div class="container">

		<div class="wrap-slick2">
							<div class="slick2">
               <?php if(!empty($tailor_info->slider_image_1)){?>
								<div class="item-slick2 p-l-15 p-r-15 p-t-15 p-b-15">
									<div class="block2">
										<div class="block2-pic hov-img0 imgd"><img src="<?php echo base_url(); ?>media/website/images/<?php echo $tailor_info->slider_image_1;?>" alt="IMG-PRODUCT"></div>
									</div>
								</div>
                  <?php }?>
              <?php if(!empty($tailor_info->slider_image_2)){?>
								<div class="item-slick2 p-l-15 p-r-15 p-t-15 p-b-15">
									<div class="block2">
										<div class="block2-pic hov-img0 imgd"><img src="<?php echo base_url(); ?>media/website/images/<?php echo $tailor_info->slider_image_2;?>" alt="IMG-PRODUCT"></div>
									</div>
								</div>
                  <?php }?>
                <?php if(!empty($tailor_info->slider_image_3)){?>
								<div class="item-slick2 p-l-15 p-r-15 p-t-15 p-b-15">
									<div class="block2">
										<div class="block2-pic hov-img0 imgd"><img src="<?php echo base_url(); ?>media/website/images/<?php echo $tailor_info->slider_image_3;?>" alt="IMG-PRODUCT"></div>
									</div>
								</div>
                  <?php }?>
               <?php if(!empty($tailor_info->slider_image_4)){?>
								<div class="item-slick2 p-l-15 p-r-15 p-t-15 p-b-15">
									<div class="block2">
										<div class="block2-pic hov-img0 imgd"><img src="<?php echo base_url(); ?>media/website/images/<?php echo $tailor_info->slider_image_4;?>" alt="IMG-PRODUCT"></div>
									</div>
								</div>
                  <?php }?>
                   <?php if(!empty($tailor_info->slider_image_5)){?>
								<div class="item-slick2 p-l-15 p-r-15 p-t-15 p-b-15">
									<div class="block2">
										<div class="block2-pic hov-img0 imgd"><img src="<?php echo base_url(); ?>media/website/images/<?php echo $tailor_info->slider_image_5;?>" alt="IMG-PRODUCT"></div>
									</div>
								</div>
                  <?php }?>
               <?php if(!empty($tailor_info->slider_image_6)){?>
                <div class="item-slick2 p-l-15 p-r-15 p-t-15 p-b-15">
									<div class="block2">
										<div class="block2-pic hov-img0 imgd"><img src="<?php echo base_url(); ?>media/website/images/<?php echo $tailor_info->slider_image_6;?>" alt="IMG-PRODUCT"></div>
									</div>
								</div>
              <?php }?>
							</div>
						</div>

		</div>
	</div>




	<div class="sec-banner bg0  p-b-55">

		<div class="container">
      <?php  $message = $this->session->userdata('message');
          if ($message != '') {?>
      <div class="alert alert-success rounded" role="alert">
        <em>Thank you!</em><br>
        <?php echo $message;
             $this->session->unset_userdata('message');
         ?>

        </div>
        <?php }?>
        <?php
        $exception = $this->session->userdata('exception');
        if ($exception != '') {?>
        <div class="alert alert-danger rounded" role="alert">
            <em>Sorry!! Order Cannot Submitted.</em><br>
            <?php echo $exception;
             $this->session->unset_userdata('exception');
             ?>

        </div>
        <?php }?>
        <div class="alert alert-danger rounded" style="display:none;"role="alert" id="div_exception_msg">
            <em>Sorry!</em><br>
             <span id="exception_msg">
            </span>
        </div>
			<hr /><p class="rate">Tailor Ratings: <strong>
        <div class="star-rating">
          <ul class="list-inline">
        <?php for ($i=0; $i <floor($tailor_info->rating) ; $i++) {?>
          <li class="list-inline-item"><i class="fa fa-star"></i></li>
        <?php }?>
      <?php if($tailor_info->rating>$i){?>
       <li class="list-inline-item"><i class="fa fa-star-half-o"></i></li>
        <?php }?>
          </ul>
        </div>

      </strong></p><hr />

			<h6><i class="fa fa-map-marker"></i> <?php echo $tailor_info->location.', '.$tailor_info->area_name.', '.$tailor_info->division_name;?></h6><br />

			<p align="justify"><strong>About <?php echo $tailor_info->name;?></strong><br />  <?php echo $tailor_info->about;?></p>
			<hr />
				<!--<p align="left" class="titleblock">Find Tailor</p>-->
			<p align="left" class=""><h3>Tailor Checklist</h3></p><br />
			<div class="row">

<div class="col-md-8">
	<div class="row">
		  <form action="<?php echo base_url(); ?>homes/order_submit" method="post" enctype="multipart/form-data" onSubmit="return from_validation()">
<?php (int)$row=0;
 foreach ($item_details as $value): ?>

	<div class="col-md-12">
		<div class="tailorprof">
		<div class="form-check" style="border-bottom: 1px solid #f5f5f5;padding-bottom: 11px;">
			<input type="hidden" name="txt_tailor_id"   class="form-control" value="<?php echo $value['tailor_id'];?>">
      <input type="hidden" name="item_details[<?php echo $row;?>][item_id]"   class="form-control" value="<?php echo $value['id'];?>">
			<input type="checkbox" name="item_details[<?php echo $row;?>][colorCheckbox]" class="form-check-input" id="item_<?php echo $row;?>" style="width: 21px;height: 30px;margin-left: 7px;">
			<label class="form-check-label" for="exampleCheck1" style="margin-top: 9px;margin-left: 18px;">Dress: <?php echo $value['name'];?></label>
		</div>

		<div class="panjabi selectt item_<?php echo $row;?>">
			<div class="row">

				<div class="col-md-2">
				<label><strong>Image Pattern</strong></label><br />
				<img style="height: 100px;width: 100px;" src="<?php echo base_url(); ?>media/tailor_item/<?php echo $value['image_path_1'];?>"/><br />

				<label><strong>Price</strong></label><span class="required_class">*</span><br />
				<div class="form-group">
					<strong><?php echo $value['price'];?> </strong> Tk.
					<input type="hidden" id="price_<?php echo $row;?>"  class="form-control" name="item_details[<?php echo $row;?>][price]" value="<?php echo $value['price'];?>">
				</div>
				<label><strong>Quantity</strong></label><span class="required_class">*</span><br />
				<div class="form-group"><input type="number" id="quantity_<?php echo $row;?>" name="item_details[<?php echo $row;?>][quantity]"  class="form-control" min="1" value="" onchange="calculate_total_amount();"></div>
				<label><strong>Total Cost</strong></label><span class="required_class">*</span><br />
				<div class="form-group">
					<span id="span_total_price_<?php echo $row;?>">	<strong></strong> </span>Tk.
					<input type="hidden" id="total_price_<?php echo $row;?>" name="item_details[<?php echo $row;?>][total_price]"  class="form-control" value="">
				</div>
				</div>

				<div class="col-md-3">
	<label><strong>Measurement</strong></label><br />
  <?php if($value['measurement_level_1']!=''){?>
	<div class="form-group"><input type="text" name="item_details[<?php echo $row;?>][measurement_level_1]" class="form-control" placeholder="<?php echo $value['measurement_level_1'];?>"></div>
  <?php }?>
  <?php if($value['measurement_level_2']!=''){?>
  <div class="form-group"><input type="text" name="item_details[<?php echo $row;?>][measurement_level_2]" class="form-control" placeholder="<?php echo $value['measurement_level_2'];?>"></div>
  <?php }?>
  <?php if($value['measurement_level_3']!=''){?>
	<div class="form-group"><input type="text" name="item_details[<?php echo $row;?>][measurement_level_3]" class="form-control" placeholder="<?php echo $value['measurement_level_3'];?>"></div>
  <?php }?>
  <?php if($value['measurement_level_4']!=''){?>
  <div class="form-group"><input type="text" name="item_details[<?php echo $row;?>][measurement_level_4]" class="form-control" placeholder="<?php echo $value['measurement_level_4'];?>"></div>
  <?php }?>
  <?php if($value['measurement_level_5']!=''){?>
  <div class="form-group"><input type="text" name="item_details[<?php echo $row;?>][measurement_level_5]" class="form-control" placeholder="<?php echo $value['measurement_level_5'];?>"></div>
  <?php }?>
  <?php if($value['measurement_level_6']!=''){?>
  <div class="form-group"><input type="text" name="item_details[<?php echo $row;?>][measurement_level_6]" class="form-control" placeholder="<?php echo $value['measurement_level_6'];?>"></div>
  <?php }?>
				</div>


	<div class="col-md-3">
	<label><strong>Neck Pattern</strong></label><br />
	<div class="form-group"><input type="text" name="item_details[<?php echo $row;?>][neck_attern]" class="form-control" placeholder="Neck Pattern"></div>
	<div class="form-group">
	<select class="form-control" name="item_details[<?php echo $row;?>][fabric]">
	<option>Selcet Fabric</option>
	<option value="Cotton">Cotton</option>
	<option value="Jorjet">Georgette</option>
	<option value="Silk">Silk</option>
	<option value="Velvet">Velvet</option>
	</select>
	</div>
	<div class="form-group">Upload Your Design<br /><input type="file" name="client_design_<?php echo $row;?>" class="form-control"></div>
	<div class="form-group">Embroidery<br /><input type="file" name="client_embroidery_<?php echo $row;?>" class="form-control" placeholder="Waist"></div>
	</div>

	<div class="col-md-3">
	<label><strong>Choose Our Design</strong></label><br />
	<?php for ($i=1; $i < 6; $i++) {
		$image='image_path_'.$i;
	  if(!empty($value[$image]))
		{?>
			<a><img src="<?php echo base_url(); ?>media/tailor_item/<?php echo $value[$image];?>" style="height:80px; width:80px;"/></a>
			<input type="radio" name="item_details[<?php echo $row;?>][choose_image]" value="<?php echo $value[$image];?>"/>
	<?php	}
	}?>

	<!-- <a><img src="<?php echo base_url(); ?>media/tailor_item/<?php echo $value['image_path_1'];?>" style="height:80px; width:80px;"/></a> -->
	</div>


			</div>
		</div>

	</div>
	</div>
	 <?php $row++; ?>
<?php  endforeach; ?>


	</div>
</div>

<div class="col-md-4">

<ul class="list-group">
  <li class="list-group-item active"><strong>Shop Amenities</strong></li>
	<?php foreach ($shop_amenities as $value): ?>
		  <li class="list-group-item"><i class="<?php echo $value['font_awosome'];?>" style="color:green"></i> <?php echo $value['name'];?></li>
	<?php endforeach; ?>

  <!-- <li class="list-group-item"><img src="<?php echo base_url(); ?>media/website/images/sewing.png" style="height:25px;"/> Sewing Machine</li>
  <li class="list-group-item"><i class="fa fa-car" style="color:green"></i> Parking</li>
  <li class="list-group-item"><i class="fa fa-bath" style="color:green"></i> Cleaning</li> -->
</ul>
<br /><br />
<ul class="list-group">
  <li class="list-group-item active"><strong>Tailor Booking</strong></li>
  <li class="list-group-item">
	<div class="form-group">Customer Name<span class="required_class">*</span> <input type="text" required name="txt_customer_name" class="form-control" placeholder="name"></div>
	<div class="form-group">Customer Phone<span class="required_class">*</span> <input type="number" id="customer_phone" required name="txt_customer_phone" class="form-control" placeholder="phone no."></div>
	<div class="form-group">Customer Email <input type="email" name="txt_customer_email" class="form-control" placeholder="email"></div>
	<div class="form-group">Shipping Address<span class="required_class">*</span> <input type="text" required name="txt_customer_address" class="form-control" placeholder="address"></div>
  <label for="inputEmail4">Remarks</label><br>
  <textarea class="form-control" name="txt_remarks" maxlength="200" placeholder="remarks"></textarea>
  <!-- <div class="form-group">Choose Date<span class="required_class">*</span><input type="date" required name="txt_date" class="form-control" placeholder="date">	</div>
  <div class="form-group">Choose Time<span class="required_class">*</span> <input type="time" name="txt_time" required class="form-control" placeholder="time"></div> -->
  <div class="form-group">Making Charge<span class="required_class">*</span> <span id="span_total_amount"><strong> </strong></span>Tk.</div>
	<input type="hidden" name="txt_total_amount" id="txt_total_amount" reduired class="form-control" value="0">
  <input type="hidden"  id="txt_total_row"  class="form-control" value="<?php echo count($item_details);?>">
  <p align="right"><input type="submit" class="btn btn-success btn-md" value="Confirm Order"/></p>
   </li>
</ul>


</div>

  </form>


			</div>


			<div>


</div>



		</div>
	</div>


<div class="sec-banner bg0  p-b-55">
		<div class="container">
	<hr />
	    	<!--<p align="left" class="titleblock">Find Tailor</p>-->
			<p align="left" class=""><h3>Tailor Additional Info</h3></p>	<br /><br />
			<p align="justify"><strong>Cancellation Policy:</strong><br />
<?php echo $tailor_info->tailor_cancel_policy;?><br /><br />
<strong>Property highlights:</strong><br />
<?php echo $tailor_info->property_highlights;?><br /><br />
<strong>Establishment year:</strong><br />
<?php echo $tailor_info->establishment_year;?><br /><br />
<strong>Shop Opening time:</strong><br />
<?php echo date('h:i A', strtotime($tailor_info->shop_opening_time));?><br /><br />
<strong>Shop Closing time:</strong><br />
<?php echo date('h:i A', strtotime($tailor_info->shop_closing_time));?><br /><br />
<strong>Payment method:</strong><br />
<?php echo $tailor_info->payment_method;?><br /><br />
<strong>Currencies:</strong><br />
<?php echo $tailor_info->currency_name;?></p>
			<hr />

		</div>
	</div>



	<!-- Content page -->
	<div class="sec-banner bg0  p-b-55">
		<div class="container">

	    	<!--<p align="left" class="titleblock">Find Tailor</p>-->
			<p align="left" class="titleblock">Tailor in this Area</p>
			<div class="row">


				<?php foreach ($tailor_list_without_this_tailor as $value): ?>
					<div class="col-md-3 col-lg-3 col-sm-6">
					 <a href="<?php echo base_url(); ?>homes/tailor_profile/<?php echo $value['id'];?>"><div class="tailorlist"><img src="<?php echo base_url(); ?>media/tailor/<?php echo $value['image_path']; ?>"/>
					 <h6><i class="fa fa-id-card-o"></i>&emsp;<?php echo $value['name'];?> </h6></div></a>
					</div>
				<?php endforeach; ?>
			</div>

		</div>
	</div>


    <script src="https://code.jquery.com/jquery-1.12.4.min.js"> </script>
    <style type="text/css">
        .selectt {

            padding: 30px 0px;
            display: none;
            width: 100%;

        }

        label {

        }
    </style>

        <script type="text/javascript">
            $(document).ready(function() {
              $('input[type="checkbox"]').click(function() {

									  var inputname = $(this).attr("id");
                    var checkedValue = $('#'+inputname).is(':checked');
                    var arr = inputname.split('_');
                    $("." + inputname).toggle();
                    if(checkedValue)
                    {
                       $('#quantity_'+arr[1]).prop('required',true);
                    }else{
                      $('#quantity_'+arr[1]).prop('required',false);
                    }
                    //alert("Checkbox " + inputname + " is selected "+inputValue);
                });

            });

						function calculate_total_amount() {
							var totalRow = Number(document.getElementById("txt_total_row").value);
							//alert(totalRow);
							var totalAmount = 0;
							var i = 0;
							while (i < totalRow) {
								debugger;
								var txtPrice = 0;
								if(document.getElementById("price_"+i).value!='')
								{
									txtPrice = Number(document.getElementById("price_"+i).value);
								}
								var txtQuantity =0;
								if(document.getElementById("quantity_"+i).value!='')
								{
									txtQuantity = Number(document.getElementById("quantity_"+i).value);
								}

								var totalPrice = Number(txtPrice*txtQuantity);
								document.getElementById("total_price_"+i).value = totalPrice.toFixed(2);
								document.getElementById("span_total_price_"+i).innerHTML=totalPrice.toFixed(2);
								if(document.getElementById("total_price_" + i).value != ''){
									totalAmount = totalAmount + totalPrice;
								}
								i++;
							}
							document.getElementById("txt_total_amount").value=totalAmount.toFixed(2);
							document.getElementById("span_total_amount").innerHTML=totalAmount.toFixed(2);
						}
            function from_validation()
            {
              var phone=	document.getElementById("customer_phone").value;
              if (/^\d{11}$/.test(phone)) {

              } else {
                  document.getElementById("div_exception_msg").style.display='block';
                  document.getElementById("exception_msg").innerHTML='Invalid Customer Phone No.! must be 11 digits';
                  document.body.scrollTop = 0;
                  document.documentElement.scrollTop = 0;
                  return false
              }
              var count=0;
              var chec=document.getElementsByTagName('input');
               for (var i = 0; i<chec.length; i++){
                   if (chec[i].type=='checkbox')
                   {
                       if (chec[i].checked){
                        count=1;
                       }
                   }
               }
              // alert(count);
              if(count==0)
              {
                document.getElementById("div_exception_msg").style.display='block';
                document.getElementById("exception_msg").innerHTML='Unable to order submit. Please check input data.';
                document.body.scrollTop = 0;
                document.documentElement.scrollTop = 0;
                return false
              }
              return true;
            }
        </script>


<?php echo $index_footer; ?>
