

<?php echo $index_header; ?>
<?php echo $main_menu; ?>
<?php echo $mobile_menu; ?>
<?php echo $index_sidebar; ?>


	<!-- Title page -->
	<section class="bg-img1 txt-center p-lr-15 p-tb-50" style="background-image: url('<?php echo base_url(); ?>media/website/images/bg-01.jpg');">
		<h2 class="ltext-105 cl0 txt-center">
			Tailor in <?php if(!empty($area_list)){ echo $area_list[0]['division_name'];}?>
		</h2>
	</section>

	<div class="container">
			<div class="bread-crumb flex-w p-l-25 p-r-15 p-t-30 p-lr-0-lg">
				<a href="<?php echo base_url(); ?>homes" class="stext-109 cl8 hov-cl1 trans-04">
					Home <i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
				</a>
				<span class="stext-109 cl4"> <?php if(!empty($area_list)){ echo $area_list[0]['division_name'];}?></span>
			</div>
	</div>


	<!-- Content page -->
	<div class="sec-banner bg0 p-t-40 p-b-55">
		<div class="container">

				<p align="left" class="titleblock">Find Tailor</p>

			<div class="row">
     <?php foreach ($area_list as $value): ?>
			 <div class="col-md-3 col-lg-3 p-b-30">
 				<div class="block1 wrap-pic-w">
 					<img src="<?php echo base_url(); ?>media/website/images/area/<?php echo $value['image_path']; ?>" alt="IMG-BANNER">

 					<a href="<?php echo base_url(); ?>homes/tailor_list/<?php echo $value['id']; ?>" class="block1-txt ab-t-l s-full flex-col-l-sb p-lr-10 p-tb-10 trans-03 respon3">
 						<div class="block1-txt-child1 flex-col-l">
 							<span class="block1-name ltext-50 trans-04 p-b-8" style="background: #0a187a; padding: 3px 13px; color: #fff;">
 								<?php echo $value['name']; ?>
 							</span>

 						</div>

 						<div class="block1-txt-child2 p-b-4 trans-05">
 							<div class="block1-link stext-101 cl0 trans-09">
 								Tailor List
 							</div>
 						</div>
 					</a>
 				</div>
 			</div>
     <?php endforeach; ?>


				<!-- <div class="col-md-3 col-lg-3 p-b-30">

					<div class="block1 wrap-pic-w">
						<img src="https://live.staticflickr.com/5266/5638317167_3b4aa938a4_b.jpg" alt="IMG-BANNER">

						<a href="tailor_list.php" class="block1-txt ab-t-l s-full flex-col-l-sb p-lr-10 p-tb-10 trans-03 respon3">
							<div class="block1-txt-child1 flex-col-l">
								<span class="block1-name ltext-50 trans-04 p-b-8" style="background: #0a187a; padding: 3px 13px; color: #fff;">
									Banani
								</span>

							</div>

							<div class="block1-txt-child2 p-b-4 trans-05">
								<div class="block1-link stext-101 cl0 trans-09">
									Tailor List
								</div>
							</div>
						</a>
					</div>
				</div>

				<div class="col-md-3 col-lg-3 p-b-30">

					<div class="block1 wrap-pic-w">
						<img src="https://media-cdn.tripadvisor.com/media/photo-s/0b/ba/57/45/view-from-10-th-floor.jpg" alt="IMG-BANNER">

						<a href="tailor_list.php" class="block1-txt ab-t-l s-full flex-col-l-sb p-lr-10 p-tb-10 trans-03 respon3">
							<div class="block1-txt-child1 flex-col-l">
								<span class="block1-name ltext-50 trans-04 p-b-8" style="background: #0a187a; padding: 3px 13px; color: #fff;">
									Uttara
								</span>

							</div>

							<div class="block1-txt-child2 p-b-4 trans-05">
								<div class="block1-link stext-101 cl0 trans-09">
									Tailor List
								</div>
							</div>
						</a>
					</div>
				</div>

				<div class="col-md-3 col-lg-3 p-b-30">

					<div class="block1 wrap-pic-w">
						<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQ9RKcHz3EbEdo3KrxBA0ZVyIoZ9-D3_5-3Sg&usqp=CAU" alt="IMG-BANNER">

						<a href="tailor_list.php" class="block1-txt ab-t-l s-full flex-col-l-sb p-lr-10 p-tb-10 trans-03 respon3">
							<div class="block1-txt-child1 flex-col-l">
								<span class="block1-name ltext-50 trans-04 p-b-8" style="background: #0a187a; padding: 3px 13px; color: #fff;">
									Dhanmondi
								</span>

							</div>

							<div class="block1-txt-child2 p-b-4 trans-05">
								<div class="block1-link stext-101 cl0 trans-09">
									Tailor List
								</div>
							</div>
						</a>
					</div>
				</div> -->




			</div>
		</div>
	</div>


<?php echo $index_footer; ?>
