


<?php echo $index_header; ?>
<?php echo $main_menu; ?>
<?php echo $mobile_menu; ?>
<?php echo $index_sidebar; ?>

	<!-- Title page -->
	<section class="bg-img1 txt-center p-lr-15 p-tb-50" style="background-image: url('<?php echo base_url(); ?>media/website/images/bg-01.jpg');">
		<h2 class="ltext-105 cl0 txt-center">
			Tailor Order
		</h2>
	</section>

	<!-- <div class="container">
			<div class="bread-crumb flex-w p-l-25 p-r-15 p-t-30 p-lr-0-lg">
				<a href="<?php echo base_url(); ?>homes" class="stext-109 cl8 hov-cl1 trans-04">
					Home <i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
				</a>
				<?php if(!empty($tailor_list)){?>
				<a href="<?php echo base_url(); ?>homes/tailor_area/<?php echo $tailor_list[0]['division_id']; ?>" class="stext-109 cl8 hov-cl1 trans-04">
					<?php echo $tailor_list[0]['division_name'];?> <i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
				</a>
				<span class="stext-109 cl4"> 	<?php echo $tailor_list[0]['area_name'];?> </span>
			<?php }?>
			</div>
	</div> -->


	<!-- Content page -->
	<div class="sec-banner bg0 p-t-40 p-b-55">
		<div class="container">
			<table class="table">
					<thead>
							<tr>
									<th scope="col">#</th>
									<th scope="col">Order Date</th>
									<th scope="col">Customer Name</th>
									<th scope="col">Phone</th>
									<th scope="col">Address</th>
									<th scope="col">Total Amount</th>
									<th scope="col">Action</th>
							</tr>
					</thead>
					<tbody>
						<?php
						$i = (int)$this->uri->segment(3);
						foreach ($tailor_order as $row):
								$i++;
								?>
							<tr>
									<th scope="row"><?php echo $i; ?></th>
									<td><?php echo $row['booking_date']; ?></td>
									<td><?php echo $row['customer_name']; ?></td>
									<td><?php echo $row['customer_phone']; ?></td>
									<td><?php echo $row['shipping_address']; ?></td>
									<td><?php echo $row['total_cost']; ?></td>
									<td>
												 <a href="<?php echo base_url(); ?>homes/tailor_order_details/<?php echo $row['id']; ?>"
														title="Edit">
														<button type="button" class="btn btn-primary btn-xs mb-1">
															View
														</button>
													</a>
									</td>
							</tr>
							<?php endforeach; ?>
					</tbody>
			</table>
		</div>
	</div>


<?php echo $index_footer; ?>
