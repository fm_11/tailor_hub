<div class="col-12 mb-4">
    <div class="card">
        <div class="card-body">
            <h5 class="card-title"><?php echo $title; ?></h5>

            <table class="table table-striped">
                <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Application Code</th>
                  <th scope="col">University Name</th>
                  <th scope="col">Course Level</th>
                  <th scope="col">Subject</th>
                  <th scope="col">Month </th>
                  <th scope="col">Year </th>
                  <th scope="col">Current Status </th>
                  <th scope="col">Country </th>
                  <th scope="col">Process Officer </th>
                <th scope="col">Action </th>
                </tr>
                </thead>
                <tbody>
                <?php
                $i = (int)$this->uri->segment(3);
                foreach ($university as $row):
                    $i++;
                    ?>
                    <tr>
                        <th scope="row"><?php echo $i; ?></th>
                        <td style="min-width: 101px;">
                          <a style="text-decoration:underline;"   href="<?php echo base_url(); ?>students_dashboard/view_status/<?php echo $row['id']; ?>">
                            <?php echo $row['application_code']; ?>
                          </a>
                        </td>

                        <td>
                            <?php echo $row['university_name']; ?>
                        </td>
                        <td><?php echo $row['course_level']; ?></td>
                        <td><?php echo $row['course_title']; ?></td>
                        <td><?php echo $row['cc_month']; ?></td>
                        <td><?php echo $row['cc_year']; ?></td>
                        <td><?php  echo $row['current_status']; ?></td>
                        <td><?php echo $row['country']; ?></td>
                      <td><?php echo $row['process_officer']; ?></td>
                      <td>
                          <?php if(isset($row['sop_img_path']) && $row['sop_img_path']!='0'){?>
                        <a target="_blank" href="<?php echo base_url(); ?>media/applicant_edu/<?php echo $row['sop_img_path']; ?>">
                          <button type="button" class="btn btn-secondary btn-xs mb-1">Download(I)</button>
                        </a>
                          <?php }?>
                          <?php if(isset($row['others_img_path']) && $row['others_img_path']!='0'){?>
                        <a target="_blank" href="<?php echo base_url(); ?>media/applicant_edu/<?php echo $row['others_img_path']; ?>">
                          <button type="button" class="btn btn-secondary btn-xs mb-1">Download(O)</button>
                        </a>
                          <?php }?>
                      </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <?php echo $this->pagination->create_links(); ?>
        </div>
    </div>
</div>
