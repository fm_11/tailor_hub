<script type="text/javascript">
    function getInstituteByCountryId(country_id){
        //alert(city_id);
        getCourseByCourseLevelId();
        if (window.XMLHttpRequest)
        {
            xmlhttp = new XMLHttpRequest();
        }
        else
        {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function()
        {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            {
                document.getElementById("institution_id").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>students_dashboard/getInstituteByCountryId?country_id=" + country_id, true);
        xmlhttp.send();
    }


    function getCourseByCourseLevelId(){
        //alert(city_id);
        var country_id= document.getElementById("country_id").value;
        var institution_id= document.getElementById("institution_id").value;
        var courses_id= document.getElementById("course_level_id").value;
        if (window.XMLHttpRequest)
        {
            xmlhttp = new XMLHttpRequest();
        }
        else
        {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function()
        {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            {
                document.getElementById("subject_id").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>students_dashboard/getCourseByCourseLevelId?course_level_id=" + courses_id+"&institution_id="+institution_id+"&country_id="+country_id, true);

        xmlhttp.send();
    }
</script>



<div class="row">
    <div class="col-12">
        <div class="card mb-4">
            <div class="card-body">
                <h5 class="mb-4"><?php echo $title; ?> <small style="color: red;">All Red marked are required</small></h5>
        <span style="float: right;color: red;margin-top: -43px;font-size: 16px;">File/Photo(3000 x 3000)px & Size(5MB) & Format Allow(jpg|png|jpeg|pdf|csv|doc|docx)</span>
                <form action="<?php echo base_url(); ?>students_dashboard/apply/<?php echo $applicant_info[0]['id']; ?>" method="post" enctype="multipart/form-data">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputEmail4">Country<span style="color:red;">*</span></label>
                            <select onchange="getInstituteByCountryId(this.value)" class="form-control select2-single" required name="country_id" id="country_id">
                                <option value="">--Select--</option>
                                <?php

                                if (count($countries)) {
                                    foreach ($countries as $list) {
                                        ?>
                                        <option
                                            value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Institution <span style="color:red;">*</span></label>
                            <select onchange="getCourseByCourseLevelId()" required name="institution_id" id="institution_id" class="form-control select2-single">
                                <option value="">Select</option>

                            </select>
                        </div>

                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Course Level<span style="color:red;">*</span></label>
                            <select onchange="getCourseByCourseLevelId()" class="form-control select2-single" required name="course_level_id" id="course_level_id">
                                <option value="">--Select--</option>
                                <?php

                                if (count($course_level)) {
                                    foreach ($course_level as $list) {
                                        ?>
                                        <option
                                                value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="subject">Subject<span style="color:red;">*</span></label>
                            <select required name="subject_id" id="subject_id" class="form-control select2-single">
                                <option value="">Select</option>

                            </select>
                        </div>

                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="txtMobile">Month<span style="color:red;">*</span></label>
                            <select onchange="" class="form-control select2-single" required name="intake_month" required>
                                <option value="">--Select--</option>
                                <?php

                                if (count($intake_months)) {
                                    foreach ($intake_months as $list) {
                                        ?>
                                        <option
                                            value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="txtMobile">Year<span style="color:red;">*</span></label>
                            <select onchange="" class="form-control select2-single" required name="intake_year" required>
                                <option value="">--Select--</option>
                                <?php

                                if (count($intake_years)) {
                                    foreach ($intake_years as $list) {
                                        ?>
                                        <option
                                            value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="txtMobile">Application Fees</label>
                            <input type="text" class="form-control" name="application_fees" value="" id="application_fees">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="txtCurrency">Currency</label>
                            <select onchange="" class="form-control select2-single" name="currency_id">
                                <option value="">--Select--</option>
                                <?php

                                if (count($currencies)) {
                                    foreach ($currencies as $list) {
                                        ?>
                                        <option
                                            value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="txtMobile">Payment Date</label>
                            <div class='input-group date' id='payment_date'>
                                <input type='text' name="payment_date" class="form-control" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="txtMobile">Statement of Purpose<span style="color:red;">*</span></label>
                            <input type="file" required class="form-control"  id="sop_path" name="sop_path">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="txtMobile">Others</label>
                            <input type="file" class="form-control"  id="others_file_path" name="others_file_path">
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary d-block mt-3">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
