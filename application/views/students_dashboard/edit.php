
<div class="row">
    <div class="col-12">
        <div class="card mb-4">
            <div class="card-body">
                <h5 class="mb-4"><?php echo $title; ?> <small style="color: red;">All Red marked are required</small></h5>
            <span style="float: right;color: red;margin-top: -43px;font-size: 16px;">File/Photo(3000 x 3000)px & Size(5MB) & Format Allow(jpg|png|jpeg|pdf|csv|doc|docx)</span>
                <form action="<?php echo base_url(); ?>students_dashboard/edit/" method="post" enctype="multipart/form-data">
                    <input type="hidden" class="form-control" id="id"
                           name="id" value="<?php echo $applicant_info[0]['id']; ?>">

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="studentFirstName">Given Name / First Name<span style="color:red;font-weight: bold;"> (As Passport)</span><span style="color:red;">*</span></label>
                            <input type="text" class="form-control" id="given_name"
                                   name="first_name" required value="<?php echo $applicant_info[0]['first_name']; ?>">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="studentLastName">Surname / Last Name</label>
                            <input type="text" class="form-control" id="last_name"
                                   name="last_name"  value="<?php echo $applicant_info[0]['last_name']; ?>">
                        </div>
                        <div class="form-group col-md-4">
                          <label for="inputPassword4">Password<span style="color:red;">*</span></label>
                          <input type="password" class="form-control" id="password"
                                 name="password" value="password" placeholder="" required>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="contact">Date Of Birth</label>
                            <div class='input-group date' id='collection_date'>
                                <input type='text' name="date_of_birth" value="<?php echo $applicant_info[0]['date_of_birth']; ?>" class="form-control" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="email">Nationality<span style="color:red;">*</span></label>
                            <input type="text" class="form-control" id="nationality"
                                   name="nationality" required value="<?php echo $applicant_info[0]['nationality']; ?>">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="email">Reference</label>
                            <select onchange="" class="form-control select2-single"  name="agent_id" >
                                <option value="">--Select--</option>
                                <?php
                                if (count($agent_id)) {
                                    foreach ($agent_id as $list) {
                                        ?>
                                        <option
                                            <?php if ($list['id'] == $applicant_info[0]['agent_id']) {
                                            echo 'selected';
                                        } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="contact">Mobile Number<span style="color:red;">*</span></label>
                            <div class="row">
                              <div class="form-group col-md-6">
                                <select class="form-control select2-single"  name="country_code" required>
                                    <option value="">--Select--</option>
                                    <?php

                                    if (count($country_code)) {
                                        foreach ($country_code as $list) {
                                          ?>
                                          <option
                                              <?php if ($list['id'] == $applicant_info[0]['country_code']) {
                                              echo 'selected';
                                          } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                          <?php
                                        }
                                    }
                                    ?>
                                </select>
                              </div>
                              <div class="form-group col-md-6" style="margin-left: -30px;">
                                <input type="text" class="form-control" id="mobile"
                                       name="mobile" required value="<?php echo $applicant_info[0]['mobile']; ?>">
                              </div>
                            </div>

                        </div>
                        <div class="form-group col-md-4">
                            <label for="email">Email<span style="color:red;">*</span></label>
                            <input type="text" class="form-control" id="email"
                                   name="email" required value="<?php echo $applicant_info[0]['email']; ?>">
                        </div>

                        <div class="form-group col-md-4">
                            <label for="email">Skype Id</label>
                            <input type="text" class="form-control" id="skype_id"
                                   name="skype_id" value="<?php echo $applicant_info[0]['skype_id']; ?>">
                        </div>

                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="contact">Passport Number<span style="color:red;">*</span></label>
                            <input type="text" class="form-control" id="passport_no"
                                   name="passport_no" required value="<?php echo $applicant_info[0]['passport_no']; ?>">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="email">Issue Date</label>
                            <div class='input-group date' id='issue_date'>
                                <input type='text' name="issue_date"  value="<?php echo $applicant_info[0]['issue_date']; ?>" class="form-control" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="email">Expair Date</label>
                            <div class='input-group date' id='expair_date'>
                                <input type='text' value="<?php echo $applicant_info[0]['expair_date']; ?>" name="expair_date" class="form-control" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">

                        <div class="form-group col-md-4">
                            <label for="is_valid_passport">Valid passport?</label>
                            <br>

                            <label class="radio-inline radio-right">
                                <input name="is_valid_passport" value="1" <?php if ($applicant_info[0]['is_valid_passport'] == '1') {
    echo 'checked';
} ?> type="radio">Yes</label>
                            <label class="radio-inline radio-right">
                                <input name="is_valid_passport" <?php if ($applicant_info[0]['is_valid_passport'] == '0') {
    echo 'checked';
} ?> value="0" type="radio">No</label>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="originOffice">Gender<span style="color:red;">*</span></label>
                            <select onchange="" class="form-control select2-single"  name="gender_id" required>
                                <option value="">--Select--</option>
                                <?php

                                if (count($genders)) {
                                    foreach ($genders as $list) {
                                        ?>
                                        <option
                                            <?php if ($list['id'] == $applicant_info[0]['gender_id']) {
                                            echo 'selected';
                                        } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>


                        <div class="form-group col-md-4">
                            <label for="originOffice">Marital Status<span style="color:red;">*</span></label>
                            <select onchange="" class="form-control select2-single"  name="marital_status_id" required>
                                <option value="">--Select--</option>
                                <?php

                                if (count($marital_status)) {
                                    foreach ($marital_status as $list) {
                                        ?>
                                        <option
                                            <?php if ($list['id'] == $applicant_info[0]['marital_status_id']) {
                                            echo 'selected';
                                        } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>

                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="contact">Lead source<span style="color:red;">*</span></label>
                            <select onchange="" class="form-control select2-single" name="lead_source_id" disabled>
                                <option value="">--Select--</option>
                                <?php

                                if (count($lead_source)) {
                                    foreach ($lead_source as $list) {
                                        ?>
                                        <option
                                            <?php if ($list['id'] == $applicant_info[0]['lead_source_id']) {
                                            echo 'selected';
                                        } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                              <input type="hidden" name="lead_source_id" value="<?php echo $applicant_info[0]['lead_source_id']; ?>" />
                        </div>
                        <div class="form-group col-md-4">
                          <label for="originOffice">Origin Office<span style="color:red;">*</span></label>
                          <select class="form-control select2-single" disabled>
                              <option value="">--Select--</option>
                              <?php

                              if (count($origin_office)) {
                                  foreach ($origin_office as $list) {
                                      ?>
                                      <option
                                          <?php if ($list['id'] == $applicant_info[0]['origin_office']) {
                                          echo 'selected';
                                      } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                      <?php
                                  }
                              }
                              ?>
                          </select>
                            <input type="hidden" name="origin_office" value="<?php echo $applicant_info[0]['origin_office']; ?>" />
                        </div>

                        <div class="form-group col-md-4">
                            <label for="contact">Admission Officer<span style="color:red;">*</span></label>
                            <select class="form-control select2-single" disabled>
                                <option value="">--Select--</option>
                                <?php

                                if (count($tbl_employee)) {
                                    foreach ($tbl_employee as $list) {
                                        ?>
                                        <option
                                            <?php if ($list['id'] == $applicant_info[0]['admission_officer_id']) {
                                            echo 'selected';
                                        } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="originOffice">Month<span style="color:red;">*</span></label>
                            <select onchange="" class="form-control select2-single"  name="intake_month_id" required>
                                <option value="">--Select--</option>
                                <?php
                                  if (count($intake_months)) {
                                      foreach ($intake_months as $list) {
                                          ?>
                                          <option
                                              value="<?php echo $list['id']; ?>" <?php if ($applicant_info[0]['intake_month_id'] == $list['id']) {
                                              echo 'selected';
                                          } ?>><?php echo $list['name']; ?></option>
                                          <?php
                                      }
                                  }
                                ?>
                            </select>
                        </div>


                        <div class="form-group col-md-6">
                            <label for="originOffice">Year<span style="color:red;">*</span></label>
                            <select onchange="" class="form-control select2-single" name="intake_year_id" required>
                                <option value="">--Select--</option>
                                <?php
                                    if (count($intake_years)) {
                                        foreach ($intake_years as $list) {
                                            ?>
                                            <option
                                                value="<?php echo $list['id']; ?>" <?php if ($applicant_info[0]['intake_year_id'] == $list['id']) {
                                                echo 'selected';
                                            } ?>><?php echo $list['name']; ?></option>
                                            <?php
                                        }
                                    }
                                ?>
                            </select>
                        </div>

                    </div>
                    <hr>
                    <h2>Address Details</h2>

                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="contact">Address</label>
                            <textarea name="cor_address" class="form-control"><?php echo $applicant_info[0]['cor_address']; ?></textarea>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="contact">City / Town</label>

                            <input type="text" class="form-control" id="cor_city"
                                   name="cor_city" required value="<?php echo $applicant_info[0]['cor_city']; ?>">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="contact">Postal / Zip Code</label>
                            <input type="text" class="form-control" id="zip_code"
                                   name="zip_code" required value="<?php echo $applicant_info[0]['zip_code']; ?>">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="contact">Country</label>
                            <select onchange="" class="form-control select2-single" required name="country_id" required>
                                <option value="">--Select--</option>
                                <?php

                                if (count($countries)) {
                                    foreach ($countries as $list) {
                                        ?>
                                        <option
                                            <?php if ($list['id'] == $applicant_info[0]['country_id']) {
                                            echo 'selected';
                                        } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                          <input type="hidden" name="passport_file_name" value="<?php echo $applicant_info[0]['passport_file_location']; ?>" />
                            <label for="inputAddress">Upload Passport</label>
                            <input type="file" class="form-control"  id="passport_file" name="passport_file">
                            <?php if(isset($applicant_info[0]['passport_file_location']) && $applicant_info[0]['passport_file_location']!='0'){?>
                            <a target="_blank" href="<?php  echo base_url(); ?>media/applicant_edu/<?php echo $applicant_info[0]['passport_file_location']; ?>">
                              <button type="button" class="btn btn-secondary btn-xs mb-1">Download</button>
                            </a>
                          <?php }?>
                        </div>
                        <div class="form-group col-md-6">
                            <input type="hidden" name="cv_file_name" value="<?php echo $applicant_info[0]['cv_file_location']; ?>" />
                            <label for="inputAddress">Upload CV</label>
                            <input type="file" class="form-control"  id="cv_file" name="cv_file">
                           <?php if(isset($applicant_info[0]['cv_file_location']) && $applicant_info[0]['cv_file_location']!='0'){?>
                            <a target="_blank"  href="<?php echo base_url(); ?>media/applicant_edu/<?php echo $applicant_info[0]['cv_file_location']; ?>">
                              <button type="button" class="btn btn-secondary btn-xs mb-1">Download</button>
                            </a>
                              <?php }?>
                        </div>

                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary d-block mt-3">Submit</button>
                    </div>

                  </form>

                    <hr>
                    <h5 class="card-title"> Education Information</h5>
                    <div class="text-right">
                        <button type="button" class="btn btn-dark default btn-sm mb-1" data-toggle="modal" data-target="#myStudent">Add Education</button>
                    </div>

                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Institute Name</th>
                            <th scope="col">Qualification</th>
                            <th scope="col">Country of Education</th>
                            <th scope="col">From Date</th>
                            <th scope="col">To Date</th>
                            <th scope="col">Subject</th>
                            <th scope="col">Passing Year</th>
                            <th scope="col">Result</th>
                            <th scope="col">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $i = 0;
                        foreach ($applicant_education as $eud_row):
                            $i++;
                            ?>
                            <tr>
                                <th scope="row"><?php echo $i; ?></th>
                                <td><?php echo $eud_row['institute_1']; ?></td>
                                <td><?php echo $eud_row['qualification_1']; ?></td>
                                <td><?php echo $eud_row['country_name']; ?></td>
                                <td><?php echo $eud_row['from_date']; ?></td>
                                <td><?php echo $eud_row['to_date']; ?></td>
                                <td><?php echo $eud_row['SUBJECT']; ?></td>
                                <td><?php echo $eud_row['year_of_passing_1']; ?></td>
                                <td><?php echo $eud_row['grade_1']; ?></td>
                                <td style="width: 223px;">
                                  <?php if(isset($eud_row['transcript_img_path']) && $eud_row['transcript_img_path']!='0'){?>
                                  <a target="_blank" href="<?php  echo base_url(); ?>media/applicant_edu/<?php echo $eud_row['transcript_img_path']; ?>">
                                    <button type="button" class="btn btn-secondary btn-xs mb-1">Download(T)</button>
                                  </a>
                                <?php }?>
                                  <?php if(isset($eud_row['certificate_img_path']) && $eud_row['certificate_img_path']!='0'){?>
                                  <a target="_blank" href="<?php  echo base_url(); ?>media/applicant_edu/<?php echo $eud_row['certificate_img_path']; ?>">
                                    <button type="button" class="btn btn-secondary btn-xs mb-1">Download(C)</button>
                                  </a>
                                <?php }?>
                                    <a href="<?php echo base_url(); ?>students_dashboard/edit_education/<?php echo $eud_row['id']; ?>"
                                       title="Edit">
                                        <button type="button" class="btn btn-primary btn-xs mb-1">
                                            Edit
                                        </button>
                                    </a>

                                    <a href="<?php echo base_url(); ?>students_dashboard/delete_education/<?php echo $eud_row['id']; ?>"
                                       title="Delete">
                                        <button type="button" class="btn btn-primary btn-xs mb-1">
                                            Delete
                                        </button>
                                    </a>



                                </td>
                            </tr>

                        <?php endforeach; ?>
                        </tbody>
                    </table>


                    <hr>

                    <h5 class="card-title">Language</h5>
                    <div class="text-right">
                        <button type="button" class="btn btn-dark default btn-sm mb-1" data-toggle="modal" data-target="#myLanguage">Add Language</button>
                    </div>

                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Language</th>
                            <th scope="col">Listening</th>
                            <th scope="col">Speaking</th>
                            <th scope="col">Reading</th>
                            <th scope="col">Writing</th>
                            <th scope="col">overall</th>
                            <th scope="col">Exam date</th>
                            <th scope="col">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $i = 0;
                        foreach ($applicant_language as $lang_row):
                            $i++;
                            ?>
                            <tr>
                                <th scope="row"><?php echo $i; ?></th>
                                <td><?php echo $lang_row['is_ielts']; ?></td>
                                <td><?php echo $lang_row['ielts_listening']; ?></td>
                                <td><?php echo $lang_row['ielts_speaking']; ?></td>
                                <td><?php echo $lang_row['ielts_reading']; ?></td>
                                <td><?php echo $lang_row['ielts_writing']; ?></td>
                                <td><?php echo $lang_row['ielts_overall_score']; ?></td>
                                <td><?php echo $lang_row['exam_date']; ?></td>
                                <td>

                                  <?php if(isset($lang_row['certificate_file']) && $lang_row['certificate_file']!='0'){?>
                                  <a target="_blank" href="<?php echo base_url(); ?>media/applicant_edu/<?php echo $lang_row['certificate_file']; ?>">
                                    <button type="button" class="btn btn-secondary btn-xs mb-1">Download</button>
                                  </a>
                                  <?php }?>
                                    <a href="<?php echo base_url(); ?>students_dashboard/edit_language/<?php echo $lang_row['id']; ?>"
                                       title="Edit">
                                        <button type="button" class="btn btn-primary btn-xs mb-1">
                                            Edit
                                        </button>
                                    </a>
                                    <a href="<?php echo base_url(); ?>students_dashboard/delete_language/<?php echo $lang_row['id']; ?>"
                                       title="Delete">
                                        <button type="button" class="btn btn-primary btn-xs mb-1">
                                            Delete
                                        </button>
                                    </a>
                                </td>

                            </tr>

                        <?php endforeach; ?>
                        </tbody>

                    </table>

                    <hr>
                    <h5 class="card-title">Work Experience </h5>
                    <div class="text-right">
                        <button type="button" class="btn btn-dark default btn-sm mb-1" data-toggle="modal" data-target="#myExperience">Add Experience</button>
                    </div>

                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name of Employer</th>
                            <th scope="col">Position</th>
                            <th scope="col">Country</th>
                            <th scope="col">Form </th>
                            <th scope="col">To </th>
                            <th scope="col">Action </th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $i = 0;
                        foreach ($applicant_experience as $exp_row):
                            $i++;
                            ?>
                            <tr>
                                <th scope="row"><?php echo $i; ?></th>
                                <td><?php echo $exp_row['employer_name_1']; ?></td>
                                <td><?php echo $exp_row['position_1']; ?></td>
                                <td><?php echo $exp_row['country_name']; ?></td>
                                <td><?php echo $exp_row['from']; ?></td>
                                <td>
                                  <?php
                                  if ($exp_row['is_till_now'] == '0') {
                                      echo $exp_row['to'];
                                  } else {
                                      echo 'Till Now';
                                  }

                                   ?>
                                </td>

                                <td>
                                  <?php if(isset($exp_row['experience_letter']) && $exp_row['experience_letter']!='0'){?>
                                  <a target="_blank" href="<?php echo base_url(); ?>media/applicant_edu/<?php echo $exp_row['experience_letter']; ?>">
                                    <button type="button" class="btn btn-secondary btn-xs mb-1">Download</button>
                                  </a>
                                 <?php }?>

                                    <a href="<?php echo base_url(); ?>students_dashboard/edit_work_experience/<?php echo $exp_row['id']; ?>"
                                       title="Edit">
                                        <button type="button" class="btn btn-primary btn-xs mb-1">
                                            Edit
                                        </button>
                                    </a>

                                    <a href="<?php echo base_url(); ?>students_dashboard/delete_work_experience/<?php echo $exp_row['id']; ?>"
                                       title="Delete">
                                        <button type="button" class="btn btn-primary btn-xs mb-1">
                                            Delete
                                        </button>
                                    </a>



                                </td>


                            </tr>

                        <?php endforeach; ?>
                        </tbody>

                    </table>
                    <hr>

                    <h5 class="card-title">Reference</h5>
                    <div class="text-right">
                        <button type="button" class="btn btn-dark default btn-sm mb-1" data-toggle="modal" data-target="#myReference">Add Reference</button>
                    </div>

                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Designation</th>
                            <th scope="col">Email</th>
                            <th scope="col">Phone </th>
                            <th scope="col">Fax </th>
                            <th scope="col">Address </th>
                            <th scope="col">City </th>
                            <th scope="col">State </th>
                            <th scope="col">Country </th>
                            <th scope="col">Action </th>

                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $i = 0;
                        foreach ($applicant_reference as $ref_row):
                            $i++;
                            ?>
                            <tr>
                                <th scope="row"><?php echo $i; ?></th>
                                <td><?php echo $ref_row['ref1_name']; ?></td>
                                <td><?php echo $ref_row['ref1_designation']; ?></td>
                                <td><?php echo $ref_row['ref1_email']; ?></td>
                                <td><?php echo $ref_row['ref1_phone']; ?></td>
                                <td><?php echo $ref_row['ref1_fax']; ?></td>
                                <td><?php echo $ref_row['ref1_address']; ?></td>
                                <td><?php echo $ref_row['ref1_city']; ?></td>
                                <td><?php echo $ref_row['ref1_state']; ?></td>
                                <td><?php echo $ref_row['country_name']; ?></td>

                                <td>

                                  <?php if(isset($ref_row['reference_letter']) && $ref_row['reference_letter']!='0'){?>
                                  <a target="_blank" href="<?php echo base_url(); ?>media/applicant_edu/<?php echo $ref_row['reference_letter']; ?>">
                                    <button type="button" class="btn btn-secondary btn-xs mb-1">Download</button>
                                  </a>
                                <?php }?>

                                    <a href="<?php echo base_url(); ?>students_dashboard/edit_reference/<?php echo $ref_row['id']; ?>"
                                       title="Edit">
                                        <button type="button" class="btn btn-primary btn-xs mb-1">
                                            Edit
                                        </button>
                                    </a>

                                    <a href="<?php echo base_url(); ?>students_dashboard/delete_reference/<?php echo $ref_row['id']; ?>"
                                       title="Delete">
                                        <button type="button" class="btn btn-primary btn-xs mb-1">
                                            Delete
                                        </button>
                                    </a>


                                </td>


                            </tr>

                        <?php endforeach; ?>
                        </tbody>

                    </table>
                    <hr>


                <!-- Modal -->
                <div id="myStudent" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Education <small style="color: red;">All Red marked are required</small></h4>

                                <button type="button" class="close" data-dismiss="modal">&times;</button>

                            </div>
                            <div class="modal-body">
                                <form action="<?php echo base_url(); ?>students_dashboard/education_save/<?php echo $applicant_info[0]['id']; ?>" method="post" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label for="contact">Country of education <span style="color:red;">*</span></label>
                                        <select onchange="" class="form-control select2-single" required name="country_id" required>
                                            <option value="">--Select--</option>
                                            <?php

                                            if (count($countries)) {
                                                foreach ($countries as $list) {
                                                    ?>
                                                    <option
                                                        value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="contact">Subject <span style="color:red;">*</span></label>
                                        <select onchange="" class="form-control select2-single" required name="course_id" required>
                                            <option value="">--Select--</option>
                                            <?php

                                            if (count($courses)) {
                                                foreach ($courses as $list) {
                                                    ?>
                                                    <option
                                                        value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="contact">Level of education <span style="color:red;">*</span></label>
                                        <input type="text" class="form-control" id="qualification_1"
                                               name="qualification_1" required value="">
                                    </div>
                                    <div class="form-group">
                                        <label for="contact">Result <span style="color:red;">*</span></label>
                                        <input type="text" class="form-control" id="grade_1"
                                               name="grade_1" required value="">
                                    </div>
                                    <div class="form-group">
                                        <label for="contact">Name of Institution <span style="color:red;">*</span></label>
                                        <input type="text" class="form-control" id="institute_1"
                                               name="institute_1" required value="">
                                    </div>

                                    <div class="form-group">
                                        <label for="contact">Passing Year <span style="color:red;">*</span></label>
                                        <input type="number" class="form-control" id="year_of_passing_1"
                                               name="year_of_passing_1" required value="">
                                    </div>
                                    <div class="form-group">
                                        <label for="contact">Attended Institution From</label>
                                        <div class='input-group date' id='from_date'>
                                            <input type='text' name="from_date" class="form-control" />
                                            <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="contact">Attended Institution To</label>
                                        <div class='input-group date' id='to_date'>
                                            <input type='text' name="to_date" class="form-control" />
                                            <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputAddress">Upload Certificate</label>
                                        <input type="file" class="form-control"  id="certificate_img" name="certificate_img">
                                    </div>

                                    <div class="form-group">
                                        <label for="inputAddress">Upload Transcript</label>
                                        <input type="file" class="form-control"  id="transcript_img" name="transcript_img">
                                    </div>

                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-danger d-block mt-3" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-success d-block mt-3">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
                <div id="myLanguage" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">

                                <h4 class="modal-title">Language <small style="color: red;">All Red marked are required</small></h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body">
                                <form action="<?php echo base_url(); ?>students_dashboard/language_save/<?php echo $applicant_info[0]['id']; ?>" method="post"  enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label for="contact">English Language Exam <span style="color:red;">*</span></label>
                                        <input type="text" class="form-control" id="is_ielts"
                                               name="is_ielts" required value="">
                                    </div>
                                    <div class="form-group">
                                        <label for="contact">Listening <span style="color:red;">*</span></label>
                                        <input type="text" class="form-control" id="ielts_listening"
                                               name="ielts_listening" required value="">
                                    </div>
                                    <div class="form-group">
                                        <label for="contact">Writing <span style="color:red;">*</span></label>
                                        <input type="text" class="form-control" id="ielts_writing"
                                               name="ielts_writing" required value="">
                                    </div>
                                    <div class="form-group">
                                        <label for="contact">Reading <span style="color:red;">*</span></label>
                                        <input type="text" class="form-control" id="ielts_reading"
                                               name="ielts_reading" required value="">
                                    </div>
                                    <div class="form-group">
                                        <label for="contact">Speaking <span style="color:red;">*</span></label>
                                        <input type="text" class="form-control" id="ielts_speaking"
                                               name="ielts_speaking" required value="">
                                    </div>

                                    <div class="form-group">
                                        <label for="contact">Overall <span style="color:red;">*</span></label>
                                        <input type="text" class="form-control" id="ielts_overall_score"
                                               name="ielts_overall_score" required value="">
                                    </div>

                                    <div class="form-group">
                                        <label for="contact">Exam date</label>
                                        <div class='input-group date' id='exam_date'>
                                            <input type='text' name="exam_date" class="form-control" />
                                            <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputAddress">Upload Certificate</label>
                                        <input type="file" class="form-control"  id="certificate_file" name="certificate_file">
                                    </div>

                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                        <button type="Submit" class="btn btn-success">Save</button>
                                    </div>
                            </div>
                                </form>
                            </div>

                    </div>
                </div>
                <div id="myExperience" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <script>
                                function actionForTillNow() {
                                  var x = document.getElementById("to_date_div");
                                   if (x.style.display === "none") {
                                     x.style.display = "block";
                                   } else {
                                     x.style.display = "none";
                                     document.getElementById("to_date_ex").value = "";
                                   }
                                }
                                </script>
                                <h4 class="modal-title">Work Experience <small style="color: red;">All Red marked are required</small></h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body">
                                <form action="<?php echo base_url(); ?>students_dashboard/work_experience/<?php echo $applicant_info[0]['id']; ?>" method="post" enctype="multipart/form-data"      autocomplete="off">
                                    <div class="form-group">
                                        <label for="contact">Name of Employer <span style="color:red;">*</span></label>
                                        <input type="text" class="form-control" id="employer_name_1"
                                               name="employer_name_1" required value="">
                                    </div>
                                    <div class="form-group">
                                        <label for="contact">Position <span style="color:red;">*</span></label>
                                        <input type="text" class="form-control" id="position_1"
                                               name="position_1" required value="">

                                    </div>
                                    <div class="form-group">
                                        <label for="contact">Country <span style="color:red;">*</span></label>
                                        <select onchange="" class="form-control select2-single" required name="country_id" required>
                                            <option value="">--Select--</option>
                                            <?php

                                            if (count($countries)) {
                                                foreach ($countries as $list) {
                                                    ?>
                                                    <option
                                                         value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="contact">From <span style="color:red;">*</span></label>
                                        <div class='input-group date' id='from'>
                                            <input type='text' name="from" class="form-control" required/>
                                            <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                  </div>

                                    <div class="form-group" id="to_date_div">
                                        <label for="contact">To</label>
                                        <div class='input-group date' id='to'>
                                            <input type='text' id="to_date_ex" name="to" class="form-control" />

                                            <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label for="tilnow">Till Now</label>
                                          <input type="checkbox" onclick="actionForTillNow()" name="is_till_now" value="1">
                                    </div>


                                    <div class="form-group">
                                        <label for="inputAddress">Upload Work Experience</label>
                                        <input type="file" class="form-control"  id="experience_letter" name="experience_letter">
                                    </div>

                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                        <button type="Submit" class="btn btn-success">Save</button>
                                    </div>
                            </div>
                            </form>
                        </div>

                    </div>
                </div>
                <div id="myReference" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">

                                <h4 class="modal-title">Reference <small style="color: red;">All Red marked are required</small></h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body">
                                <form action="<?php echo base_url(); ?>students_dashboard/reference/<?php echo $applicant_info[0]['id']; ?>" method="post" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label for="contact">Name <span style="color:red;">*</span></label>
                                        <input type="text" class="form-control" id="ref1_name"
                                               name="ref1_name" required value="">
                                    </div>
                                    <div class="form-group">
                                        <label for="contact">Designation <span style="color:red;">*</span></label>
                                        <input type="text" class="form-control" id="ref1_designation"
                                               name="ref1_designation" required value="">
                                    </div>

                                    <div class="form-group">
                                        <label for="contact">Email <span style="color:red;">*</span></label>
                                        <input type="email" class="form-control" id="ref1_email"
                                               name="ref1_email" required value="">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="contact">Phone <span style="color:red;">*</span></label>
                                        <div class="row">
                                          <div class="form-group col-md-6">
                                            <select class="form-control select2-single"  name="country_code" required>
                                                <option value="">--Select--</option>
                                                <?php

                                                if (count($country_code)) {
                                                    foreach ($country_code as $list) {
                                                        ?>
                                                        <option
                                                            value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                          </div>
                                          <div class="form-group col-md-6" style="margin-left: -30px;">
                                            <input type="number" class="form-control" id="ref1_phone"
                                                   name="ref1_phone" required value="">
                                          </div>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label for="contact">Fax</label>
                                        <input type="text" class="form-control" id="ref1_fax"
                                               name="ref1_fax" value="">
                                    </div>

                                    <div class="form-group">
                                        <label for="contact">Address</label>
                                        <textarea class="form-control" name="ref1_address"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="contact">City</label>
                                        <input type="text" class="form-control" id="ref1_city"
                                               name="ref1_city" value="">
                                    </div>
                                    <div class="form-group">
                                        <label for="contact">State</label>
                                        <input type="text" class="form-control" id="ref1_state"
                                               name="ref1_state" value="">
                                    </div>

                                    <div class="form-group">
                                        <label for="contact">Country<span style="color:red;">*</span></label>
                                        <select onchange="" class="form-control select2-single" name="ref1_country_id" required>
                                            <option value="">--Select--</option>
                                            <?php

                                            if (count($countries)) {
                                                foreach ($countries as $list) {
                                                    ?>
                                                    <option
                                                         value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputAddress">Upload Reference Letter</label>
                                        <input type="file" class="form-control"  id="reference_image" name="reference_image">
                                    </div>



                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                        <button type="Submit" class="btn btn-success">Save</button>
                                    </div>
                            </div>
                            </form>
                        </div>

                    </div>
                </div>

            </div>

        </div>

    </div>
</div>
</div>
