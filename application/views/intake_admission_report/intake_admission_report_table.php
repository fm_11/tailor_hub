<hr>
<style>
.my-custom-scrollbar {
/* position: relative;
height: 500px;
overflow: auto; */
}
.table-wrapper-scroll-y {
display: block;
}
</style>
<?php
if(isset($excel)){
  header('Content-type: application/excel');
  $filename = 'Admission Report(Individual).xls';
  header('Content-Disposition: attachment; filename='.$filename);
}
?>
<div class="table-wrapper-scroll-y my-custom-scrollbar">
<table class="table table-responsive">
    <thead>
      <tr>
          <th  class="text-center" colspan="9" scope="col">
            <h3>
               <?php echo $org_info['org_name']; ?>
            </h3>
            <p><?php echo $org_info['address']; ?></p>
            <p><?php echo $org_info['email']; ?></p>
            <p><?php echo $title; ?></p>
            <p>Branch: <?php echo $org_info['branch_name']; ?></p>
          </th>
      </tr>
      <tr>
            <th scope="col" class="text-left" colspan="2">Employee Name</th>
            <td scope="col" class="text-left" colspan="7"><?php echo $employee_info[0]['name']; ?></td>
     </tr>
     <tr>
           <th scope="col" class="text-left" colspan="2">Employee Code</th>
           <td scope="col" class="text-left" colspan="7"><?php echo $employee_info[0]['code']; ?></td>
    </tr>
    <tr>
          <th scope="col" class="text-left" colspan="2">Designation</th>
          <td scope="col" class="text-left" colspan="7"><?php echo $employee_info[0]['designation']; ?></td>
   </tr>
   <tr>
         <th scope="col" class="text-left" colspan="2">Report Period</th>
         <td scope="col" class="text-left" colspan="7"><?php echo $intake; ?></td>
  </tr>
  <tr>
        <th scope="col" class="text-left" colspan="2">Total Day</th>
        <td scope="col" class="text-left" colspan="7"><?php echo $total_day; ?></td>
 </tr>
 <tr>
       <th scope="col" class="text-left" colspan="2">Report To</th>
       <td scope="col" class="text-left" colspan="7"><?php echo $report_to[0]['designation']; ?></td>
</tr>
<tr>
      <th scope="col" class="text-center" colspan="3">No of Student Visited</th>
      <th scope="col" class="text-center" colspan="3">No of Student Registration</th>
      <th scope="col" class="text-center" colspan="3">No of Student Cancel</th>
</tr>
<tr>
      <th scope="col" class="text-center" colspan="3"><?php echo $status_Data[0]['visited']; ?></th>
      <th scope="col" class="text-center" colspan="3"><?php echo $status_Data[0]['registration']; ?></th>
      <th scope="col" class="text-center" colspan="3"><?php echo $status_Data[0]['application_cancel']; ?></th>
</tr>
      <tr>
            <th scope="col">#SL</th>
            <th scope="col">Student</th>
            <th scope="col">Student Code</th>
            <th scope="col">Lead Source</th>
            <th scope="col">University</th>
            <th scope="col">Application Code</th>
            <th scope="col">Country</th>
            <th scope="col">Current Status</th>
            <th scope="col">Intake</th>
     </tr>
    </thead>
    <tbody>
      <?php
      $i = 0;
      foreach ($rData as $row):
          $i++;
          ?>
        <tr>
            <th scope="row"><?php echo $i; ?></th>
            <td><?php echo $row['s_name']; ?></td>
            <td><?php echo $row['applicant_code']; ?></td>
            <td><?php echo $row['lead_sources']; ?></td>
            <td><?php echo $row['institutions']; ?></td>
            <td><?php echo $row['u_code']; ?></td>
            <td><?php  echo $row['country']; ?></td>
            <td><?php echo $row['current_status']; ?></td>
            <td><?php  echo $row['intake_months'].'-'.$row['intake_years']; ?></td>
        </tr>
      <?php endforeach; ?>
      <tr>
            <th colspan="5"> </th>
            <td colspan=""></td>
     </tr>
     <tr>
           <th scope="col" class="text-left" colspan="3">No of Visa Application Made</th>
           <th scope="col" class="text-left" colspan="2">Visa Received </th>
           <th scope="col" class="text-left" colspan="3">Visa Refused</th>
           <th scope="col" class="text-left" colspan="1">Success Rate</th>

     </tr>
     <tr>
           <th scope="col" class="text-left" colspan="3"><?php echo $status_Data[0]['visa_application_submitted']; ?></th>
           <th scope="col" class="text-left" colspan="2"><?php echo $status_Data[0]['visa_received']; ?></th>
           <th scope="col" class="text-left" colspan="3"><?php echo $status_Data[0]['visa_rejected']; ?></th>
           <th scope="col" class="text-left" colspan="1">
             <?php $success=0;
             if($status_Data[0]['visa_application_submitted']!='0')
             {
                 $success=($status_Data[0]['visa_received']/$status_Data[0]['visa_application_submitted'])*100;
             }
             echo round($success,2);
             ?>
             %
           </th>
     </tr>
     <tr>
           <th colspan="3"><?php echo $printed_by[0]['name']; ?></th>
           <td colspan="3"></td>
           <td colspan="3"></td>
    </tr>
    <tr>
          <th  class="text-left" colspan="3"><span style="border-top: 1px solid #3a2626;">Prepared By</span> </th>
          <th scope="col" class="text-center" colspan="3"><span style="border-top: 1px solid #3a2626;">Varified By</span></th>
          <th scope="col" class="text-right" colspan="3"><span style="border-top: 1px solid #3a2626;">Approved By</span></th>

   </tr>
   <tr>
         <th scope="col" class="text-left" colspan="3"><?php echo $printed_by[0]['designation_name']; ?> </th>
         <th scope="col" class="text-center" colspan="3">Engagement Officer </th>
         <th scope="col" class="text-right" colspan="3"><?php echo $approved_by[0]['designation']; ?> </th>
  </tr>

    </tbody>
</table>
</div>
