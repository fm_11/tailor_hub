<?php
if(isset($excel)){
  header('Content-type: application/excel');
  $filename = 'Daily Attendance Report.xls';
  header('Content-Disposition: attachment; filename='.$filename);
}
?>
<div class="table-responsive">
<table class="table">
    <thead>
      <tr>
          <th  class="text-center" colspan="6" scope="col">
            <h3>
               <?php echo $org_info['org_name']; ?>
            </h3>
            <p><?php echo $org_info['address']; ?></p>
            <p><?php echo $org_info['email']; ?></p>
            <p>Attendance Report for <?php echo $date; ?></p>
            <p>Branch: <?php echo $org_info['branch_name']; ?></p>
          </th>
      </tr>
      <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Employee ID</th>
            <th scope="col">Designation</th>
            <th scope="col">Login Time</th>
            <th scope="col">Logout Time</th>
      </tr>
    </thead>
    <tbody>

      <?php
      $i = 0;
      foreach ($rData as $row):
          $i++;
          ?>

        <tr>
            <th scope="row"><?php echo $i; ?></th>
            <td><?php echo $row['name']; ?></td>
            <td><?php echo $row['code']; ?></td>
            <td><?php echo $row['post_name']; ?></td>
            <td><?php echo date('h:i:s A', strtotime($row['login_time'])); ?></td>
            <td>
              <?php
              if($row['logout_time'] != ''){
                echo date('h:i:s A', strtotime($row['logout_time']));
              }else{
                echo '-';
              }
               ?>
            </td>
        </tr>

      <?php endforeach; ?>

    </tbody>
</table>
</div>
