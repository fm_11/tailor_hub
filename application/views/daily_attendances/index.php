<div class="row">
            <div class="col-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <h5 class="mb-4"><?php echo $title; ?></h5>

                            <form action="<?php echo base_url(); ?>daily_attendances/index" method="post" enctype="multipart/form-data">


                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                          <label for="Branch">Date</label>
                                          <div class="input-group date">
                                             <input  autocomplete="off" type="text" name="date" value="<?php if(isset($date)){ echo $date; } ?>" required class="form-control">
                                             <span class="input-group-text input-group-append input-group-addon">
                                                 <i class="simple-icon-calendar"></i>
                                             </span>
                                         </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="Branch">Branch</label>
                                        <select class="form-control select2-single"  name="branch_id">
                                            <option value="">--Select--</option>
                                            <?php
                                            foreach ($branches as $row):
                                            ?>
                                               <option value="<?php echo $row['id']; ?>" <?php if(isset($branch_id)){ if($row['id'] == $branch_id){ echo 'selected'; } } ?>><?php echo $row['name']; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="btn-group">
                                      <input type="submit" value="Report View" name="view" class="btn btn-primary d-block mt-3">
                                      <input type="submit" name="excel" value="Download Excel" class="btn btn-outline-dark mt-3">
                                </div>
                    </form>

                <?php if(isset($rData)){ echo $report; } ?>
               </div>
          </div>
      </div>
</div>
