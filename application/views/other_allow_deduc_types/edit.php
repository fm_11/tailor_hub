<div class="row">
            <div class="col-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <h5 class="mb-4"><?php echo $title; ?></h5>

                            <form action="<?php echo base_url(); ?>other_allow_deduc_types/edit" method="post" enctype="multipart/form-data">
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                      <label for="inputEmail4">Name</label>
                                      <input type="text" class="form-control" id="name" required name="name" value="<?php echo $salary_type_info->name;  ?>" placeholder="Name">
                                      <input type="hidden" name="id"  class="smallInput wide" value="<?php echo $salary_type_info->id;  ?>">
                                   </div>
                                   <div class="form-group col-md-6">
                                       <label for="txtType">Type</label>
                                       <select class="form-control select2-single"  name="type" required>
                                           <option value="A" <?php if($salary_type_info->type == 'A'){ echo 'selected'; } ?>>Allowance</option>
                                           <option value="D" <?php if($salary_type_info->type == 'D'){ echo 'selected'; } ?>>Deduction</option>
                                       </select>
                                   </div>
                                  </div>



                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="rounding_type">Rounding Type</label>
                                        <select class="form-control select2-single"  name="rounding_type" required>
                                            <option value="R" <?php if($salary_type_info->rounding_type == 'R'){ echo 'selected'; } ?>>Round</option>
                                            <option value="C" <?php if($salary_type_info->rounding_type == 'C'){ echo 'selected'; } ?>>Ceiling</option>
                                            <option value="F" <?php if($salary_type_info->rounding_type == 'F'){ echo 'selected'; } ?>>Floor</option>
                                        </select>
                                    </div>

                                    <div class="form-group col-md-6">
                                          <label for="report_order">Report Order</label>
                                          <input type="text" class="form-control" required id="report_order" name="report_order"
                                              value="<?php echo $salary_type_info->report_order;  ?>">
                                      </div>
                                </div>



                                <button type="submit" class="btn btn-primary d-block mt-3">Submit</button>
                    </form>
            </div>
</div>
</div>
</div>
