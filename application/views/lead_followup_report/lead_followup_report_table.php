<hr>
<style>
.my-custom-scrollbar {
/* position: relative;
height: 500px;
overflow: auto; */
}
.table-wrapper-scroll-y {
display: block;
}
</style>
<?php
if(isset($excel)){
  header('Content-type: application/excel');
  $filename = 'Lead Followup Report.xls';
  header('Content-Disposition: attachment; filename='.$filename);
}
?>
<div class="table-wrapper-scroll-y my-custom-scrollbar">
<table class="table table-responsive">
    <thead>
      <tr>
          <th  class="text-center" colspan="6" scope="col">
            <h3>
               <?php echo $org_info['org_name']; ?>
            </h3>
            <p><?php echo $org_info['address']; ?></p>
            <p><?php echo $org_info['email']; ?></p>
            <p><?php echo $title; ?></p>
            <p>Branch: <?php echo $org_info['branch_name']; ?></p>
          </th>
      </tr>
      <tr>
            <th scope="col" class="text-left" colspan="2">Employee Name</th>
            <td scope="col" class="text-left" colspan="6"><?php echo $employee_info[0]['name']; ?></td>
     </tr>
     <tr>
           <th scope="col" class="text-left" colspan="2">Employee Code</th>
           <td scope="col" class="text-left" colspan="6"><?php echo $employee_info[0]['code']; ?></td>
    </tr>
    <tr>
          <th scope="col" class="text-left" colspan="2">Designation</th>
          <td scope="col" class="text-left" colspan="6"><?php echo $employee_info[0]['designation']; ?></td>
   </tr>
   <tr>
         <th scope="col" class="text-left" colspan="2">Report Period</th>
         <td scope="col" class="text-left" colspan="6"><?php echo $from_date; ?> To <?php echo $to_date; ?></td>
  </tr>
  <tr>
        <th scope="col" class="text-left" colspan="2">Total Day</th>
        <td scope="col" class="text-left" colspan="6"><?php echo $total_day; ?></td>
 </tr>
 <tr>
       <th scope="col" class="text-left" colspan="2">Report To</th>
       <td scope="col" class="text-left" colspan="6"><?php echo $report_to[0]['designation']; ?></td>
</tr>
      <tr>
            <th scope="col">#SL</th>
            <th scope="col">Lead Source</th>
            <th scope="col">Follow Up</th>
            <th scope="col">Delete</th>
            <th scope="col">Favourite</th>
            <th scope="col">My Desk</th>
     </tr>
    </thead>
    <tbody>
      <?php
      $i = 0;
      $total_my_desk=0;
      foreach ($rData as $row):
          $i++;
          $total_my_desk=$total_my_desk+$row['my_desk'];
          ?>
        <tr>
            <th scope="row"><?php echo $i; ?></th>
            <td><?php echo $row['name']; ?></td>
            <td><?php echo $row['leads_followup']; ?></td>
            <td><?php echo $row['is_deleted']; ?></td>
            <td><?php echo $row['is_favourite']; ?></td>
            <td><?php  echo $row['my_desk']; ?></td>
        </tr>
      <?php endforeach; ?>
      <tr>
            <th colspan="5" style="text-align:right;">Total : </th>
            <td colspan=""><?php  echo $total_my_desk; ?></td>
     </tr>
    
     <tr>
           <th colspan="4"><?php echo $printed_by[0]['name']; ?></th>
           <td colspan="2"></td>
    </tr>
    <tr>
          <th  class="text-left" colspan="4"><span style="border-top: 1px solid #3a2626;">Prepared By</span> </th>
          <th scope="col" class="text-left" colspan="2"><span style="border-top: 1px solid #3a2626;">Varified By</span></th>

   </tr>
   <tr>
         <th scope="col" class="text-left" colspan="4"><?php echo$printed_by[0]['designation_name']; ?> </th>
         <th scope="col" class="text-left" colspan="2">Senior Engagement Officer</th>

  </tr>

    </tbody>
</table>
</div>
