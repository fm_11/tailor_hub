<style>
table th{
  text-align: right;
}
</style>
<div class="row">
            <div class="col-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <h5 class="mb-4"><?php echo $title; ?></h5>
                            <?php if(!empty($employee_info[0]['photo_location'])){?>
                            <div class="col-10" style="text-align: center;">
                              <!-- <img src="<?php echo base_url(); ?>media/employee/<?php echo $employee_info[0]['photo_location']; ?>" style=" border:1px solid #666666;" width="150" height="150">
                             -->
                              <img style="width: 25%;border-radius: 50%;" src="<?php echo base_url(); ?>media/employee/<?php echo $employee_info[0]['photo_location'];?>" />
                            </div>
                            <br>   <br>
                            <?php }?>
                            <table style="margin: auto;" class="table">
                              <tr>
                                <th>Name:</th>
                                <td><?php echo $employee_info[0]['name'];?></td>
                                <th>Employee ID:</th>
                                <td><?php echo $employee_info[0]['code'];?></td>
                              </tr>
                              <tr>
                                <th>Branch:</th>
                                <td><?php echo $employee_info[0]['branch_name'];?></td>
                                <th>Date Of Birth:</th>
                                <td><?php echo $employee_info[0]['date_of_birth'];?></td>
                              </tr>
                              <tr>
                                <th>Joining Date:</th>
                                <td><?php echo $employee_info[0]['joining_date'];?></td>
                                <th>Confirmation Date:</th>
                                <td><?php echo $employee_info[0]['confirmation_date'];?></td>
                              </tr>
                              <tr>
                                <th>Designation:</th>
                                <td><?php echo $employee_info[0]['post_name'];?></td>
                                <th>Deartment:</th>
                                <td><?php echo $employee_info[0]['section_name'];?></td>
                              </tr>
                              <tr>
                                <th>Gender:</th>
                                <td><?php if($employee_info[0]['gender'] == 'M'){ echo 'Male'; } else {
                                echo 'Female';
                                }?> </td>
                                <th>Is Permanent ?</th>
                                <td><?php if($employee_info[0]['is_permanent'] == 'P'){ echo 'Permanent'; }elseif ($employee_info[0]['is_permanent'] == 'PT') {
                                echo 'Per Time';
                              }else {
                                echo '';
                              } ?></td>
                              </tr>
                              <tr>
                                <th>National ID/Passport:</th>
                                <td><?php echo $employee_info[0]['national_id'];?></td>
                                <th>Is Leave Auto Approve ?</th>
                                <td><?php if($employee_info[0]['leave_auto_approve'] == '0'){ echo 'No'; }elseif ($employee_info[0]['leave_auto_approve'] == '1') {
                                echo 'Yes';
                              }else {
                                echo '';
                              } ?></td>
                              </tr>
                              <tr>
                                <th>Reporting Boss:</th>
                                <td><?php echo $employee_info[0]['reporting_boss'].'('.$employee_info[0]['repo_code'].')';?></td>
                                <th>Shift:</th>
                                <td><?php echo $employee_info[0]['shift_name'];?></td>
                              </tr>
                              <tr>
                                <th>Mobile:</th>
                                <td><?php echo $employee_info[0]['mobile'];?></td>
                                <th>Email:</th>
                                <td><?php echo $employee_info[0]['email'];?></td>
                              </tr>
                              <tr>
                                <th>Process Code:</th>
                                <td><?php echo $employee_info[0]['process_code'];?></td>
                                <th>Address:</th>
                                <td><?php echo $employee_info[0]['address'];?></td>
                              </tr>

                            </table>

            </div>
       </div>
      </div>
</div>
