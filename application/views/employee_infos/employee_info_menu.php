<script type="text/javascript" src="<?php echo base_url(); ?>media/js/jquery-latest.min.js"></script>
<script>
    $(document).ready(function() {
        var str = location.href.toLowerCase();
        $(".sub-header li a").each(function() {
            if (str.indexOf(this.href.toLowerCase()) > -1) {
                $("a.current").removeClass("current");
                $(this).closest('a').addClass('current');
            }
        });
    })

</script>
<?php
$session_user = $this->session->userdata('user_info');
if($session_user[0]->is_integrate_with_website == 1) {
?>
    <ul class="sub-header">
        <li><a href="<?php echo base_url(); ?>employee_infos/employee_section_info"><span>Section</span></a></li>
        <li><a href="<?php echo base_url(); ?>employee_infos/employee_post_info"><span>Post</span></a></li>
        <li><a href="<?php echo base_url(); ?>employee_infos/index"><span>Employee Info.</span></a></li>
        <li><a href="<?php echo base_url(); ?>employee_infos/staff_info"><span>Staff Info.</span></a></li>
    </ul>
<?php
}else {
?>
    <ul class="sub-header">
        <li><a href="<?php echo base_url(); ?>employee_infos/employee_section_info"><span>Section</span></a></li>
        <li><a href="<?php echo base_url(); ?>employee_infos/employee_post_info"><span>Post</span></a></li>
        <li><a href="<?php echo base_url(); ?>employee_infos/index"><span>Employee Info.</span></a></li>
        <li><a href="<?php echo base_url(); ?>employee_infos/staff_info"><span>Staff Info.</span></a></li>
    </ul>
<?php
}
?>


