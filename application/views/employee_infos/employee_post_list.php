<script type="text/javascript">
    function deleteConfirm() {
        var result = confirm("Are you sure to delete?");
        if (result == true) {
            return true;
        }
        else {
            return false;
        }
    }
</script>

<?php $session_user = $this->session->userdata('user_info'); ?>
<h2>
    <?php
    if ($session_user[0]->is_integrate_with_website == 1) {
        ?>
        <a class="button_grey_round" style="margin-bottom: 5px;"
           href="<?php echo base_url(); ?>employee_infos/employee_post_synchronize"><span>Post Synchronize</span></a>
    <?php
    } else {
        ?>
        <a class="button_grey_round" style="margin-bottom: 5px;"
           href="<?php echo base_url(); ?>employee_infos/employee_post_add"><span>Add New Post</span></a>
    <?php
    }
    ?>
</h2>


<table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <thead>
    <tr>
        <th width="50" scope="col">SL</th>
        <th width="200" scope="col">Name</th>
        <th width="150" scope="col">Number of Post</th>
        <th width="150" scope="col">Shorting Order</th>
        <th width="150" scope="col">Is <?php
            if ($session_user[0]->is_integrate_with_website == 1) {
                echo 'Employee';
            } else {
                echo 'Employee';
            }
            ?> Post</th>
        <th width="150" scope="col">Actions</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = 0;
    foreach ($post as $row):
        $i++;
        ?>
        <tr>
        <tr>
            <td width="34">
                <?php echo $i; ?>
            </td>
            <td><?php echo $row['name']; ?></td>
            <td><?php echo $row['num_of_post']; ?></td>
            <td><?php echo $row['shorting_order']; ?></td>
            <td>
                <?php
                if ($row['is_employee'] == 1) {
                    echo 'Yes';
                } else {
                    echo 'No';
                }
                ?>
            </td>
            <td style="vertical-align:middle">
                <?php
                if ($session_user[0]->is_integrate_with_website == 1) {
                    ?>
                    <a href="<?php echo $web_address; ?>employee_infos/employee_post_edit/<?php echo $row['id']; ?>"
                       target="_blank" title="View">Edit by Website</a>
                <?php
                } else {
                    ?>
                    <a href="<?php echo base_url(); ?>employee_infos/employee_post_edit/<?php echo $row['id']; ?>"
                       class="edit_icon" title="Edit"></a>
                    <a href="<?php echo base_url(); ?>employee_infos/employee_post_delete/<?php echo $row['id']; ?>"
                       onclick="return deleteConfirm()" class="delete_icon" title="Delete"></a>
                <?php
                }
                ?>
            </td>

        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

