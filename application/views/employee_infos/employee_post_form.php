<?php
$session_user = $this->session->userdata('user_info');
if ($action == 'edit') {
    ?>
    <form id="add" action="<?php echo base_url(); ?>employee_infos/employee_post_edit" method="post">
        <label>Post Name</label>
        <input type="text" class="smallInput wide" name="txtPost" value="<?php echo $post[0]['name']; ?>" required="1"/>

        <label>Number of Post</label>
        <input type="text" class="smallInput wide" name="num_of_post" value="<?php echo $post[0]['num_of_post']; ?>"
               required="1"/>

        <label>Shorting Order</label>
        <input type="text" class="smallInput wide" value="<?php echo $post[0]['shorting_order']; ?>"
               name="shorting_order" required="1"/>

        <label>Is <?php
            if ($session_user[0]->is_integrate_with_website == 1) {
                echo 'Employee';
            } else {
                echo 'Employee';
            }
            ?> Post</label>
        <select class="smallInput" required="1" name="is_employee">
            <option value="">--Please Select --</option>
            <option value="1" <?php if ($post[0]['is_employee'] == '1') {
                echo 'selected';
            } ?>>Yes
            </option>
            <option value="0"<?php if ($post[0]['is_employee'] == '0') {
                echo 'selected';
            } ?>>No
            </option>
        </select>
        <br>
        <br>
        <input type="hidden" name="id" value="<?php echo $post[0]['id']; ?>">
        <input type="submit" class="submit" value="Update">
    </form>
    <br/>

    <div class="clear"></div><br/>


<?php
} else {
    ?>
    <form id="add" action="<?php echo base_url(); ?>employee_infos/employee_post_add" method="post">
        <label>Post Name</label>
        <input type="text" class="smallInput wide" name="txtPost" required="1"/>

        <label>Number of Post</label>
        <input type="text" class="smallInput wide" name="num_of_post" required="1"/>

        <label>Shorting Order</label>
        <input type="text" class="smallInput wide" name="shorting_order" required="1"/>
        <label>Is <?php
            if ($session_user[0]->is_integrate_with_website == 1) {
                echo 'Employee';
            } else {
                echo 'Employee';
            }
            ?>
            Post</label>
        <select class="smallInput" required="1" name="is_employee">
            <option value="">--Please Select --</option>
            <option value="1">Yes</option>
            <option value="0">No</option>
        </select>
        <br>
        <br>
        <input type="submit" class="submit" value="Submit">
        <input type="reset" class="submit" value="Reset">
    </form>
    <br/>

    <div class="clear"></div><br/>

<?php
}
?>
