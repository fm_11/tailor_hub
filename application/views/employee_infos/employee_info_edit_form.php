<div class="row">
            <div class="col-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <h5 class="mb-4"><?php echo $title; ?></h5>

                            <form action="<?php echo base_url(); ?>employees/edit" method="post" class="needs-validation" novalidate enctype="multipart/form-data">
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                      <label for="validationTooltip001">Name</label>
                                      <input type="text" class="form-control" id="validationTooltip001" required name="txtName"
                                      value="<?php echo $employee_info[0]['name']; ?>">
                                      <input type="hidden" class="form-control" id="validationTooltip001" required name="id"
                                      value="<?php echo $employee_info[0]['id']; ?>">
                                   </div>
                                      <div class="form-group col-md-6">
                                          <label for="inputPassword4">Employee ID</label>
                                          <input type="text" readonly class="form-control" id="txtCode" required
                                              name="txtEmployeeId" value="<?php echo $employee_info[0]['code']; ?>">
                                      </div>
                                  </div>

                                  <div class="form-row">
                                      <div class="form-group col-md-6">
                                          <label for="branches">Branch</label>
                                          <select class="form-control select2-single"  name="txtBranch" required>
                                              <option value="">--Select--</option>
                                              <?php

                                              if (count($branches)) {
                                                  foreach ($branches as $list) {

                                                      ?>
                                                      <option value="<?php echo $list['id']; ?>"
                                                        <?php if($employee_info[0]['branch_id'] == $list['id']){ echo 'selected'; } ?>><?php echo $list['name']; ?></option>
                                                  <?php
                                                  }
                                              }
                                              ?>
                                          </select>
                                      </div>

                                      <div class="form-group col-md-6">
                                              <label for="Timezone">Date Of Birth</label>
                                              <div class="input-group date">
                                                 <input type="text" name="txtDOB" value="<?php echo $employee_info[0]['date_of_birth']; ?>" required class="form-control">
                                                 <span class="input-group-text input-group-append input-group-addon">
                                                     <i class="simple-icon-calendar"></i>
                                                 </span>
                                             </div>
                                        </div>
                                  </div>


                                  <div class="form-row">
                                      <div class="form-group col-md-6">
                                          <label for="Timezone">Joining Date</label>
                                          <div class="input-group date">
                                             <input type="text" name="txtDateOfJoining"  value="<?php echo $employee_info[0]['joining_date']; ?>" required class="form-control">
                                             <span class="input-group-text input-group-append input-group-addon">
                                                 <i class="simple-icon-calendar"></i>
                                             </span>
                                         </div>
                                      </div>

                                      <div class="form-group col-md-6">
                                              <label for="Timezone">Confirmation Date</label>
                                              <div class="input-group date">
                                                 <input type="text" name="txtDateOfConfirmation" value="<?php echo $employee_info[0]['confirmation_date']; ?>" required class="form-control">
                                                 <span class="input-group-text input-group-append input-group-addon">
                                                     <i class="simple-icon-calendar"></i>
                                                 </span>
                                             </div>
                                      </div>
                                  </div>

                                  <div class="form-row">
                                      <div class="form-group col-md-6">
                                          <label for="Country">Designation</label>
                                          <select class="form-control select2-single" required name="txtPost" required>
                                              <option value="">--Select--</option>
                                              <?php

                                              if (count($post)) {
                                                  foreach ($post as $list) {

                                                      ?>
                                                      <option
                                                          value="<?php echo $list['id']; ?>" <?php if($employee_info[0]['post'] == $list['id']){ echo 'selected'; } ?>><?php echo $list['name']; ?></option>
                                                  <?php
                                                  }
                                              }
                                              ?>
                                          </select>
                                      </div>

                                      <div class="form-group col-md-6">
                                            <label for="Timezone">Deartment</label>
                                            <select class="form-control select2-single" required name="txtSection" required>
                                                <option value="">--Select--</option>
                                                <?php

                                                if (count($sections)) {
                                                    foreach ($sections as $list) {

                                                        ?>
                                                        <option
                                                            value="<?php echo $list['id']; ?>" <?php if($employee_info[0]['section'] == $list['id']){ echo 'selected'; } ?>><?php echo $list['name']; ?></option>
                                                    <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                  </div>

                                  <div class="form-row">
                                      <div class="form-group col-md-4">
                                          <label for="Country">Gender</label>
                                          <div class="panel-body">
                                                <label class="radio-inline">
                                                  <div class="custom-control custom-radio">
                                                      <input type="radio" id="customRadio1" name="txtGender"
                                                         value="M" <?php if($employee_info[0]['gender'] == 'M'){ echo 'checked'; } ?> required class="custom-control-input">
                                                      <label class="custom-control-label" for="customRadio1">Male</label>
                                                  </div>
                                                </label>
                                                <label class="radio-inline">
                                                  <div class="custom-control custom-radio">
                                                      <input type="radio" id="customRadio2" name="txtGender"
                                                        value="F" <?php if($employee_info[0]['gender'] == 'F'){ echo 'checked'; } ?> class="custom-control-input">
                                                      <label class="custom-control-label" for="customRadio2">Female</label>
                                                  </div>
                                                </label>
                                          </div>
                                      </div>

                                      <div class="form-group col-md-4">
                                            <label for="Timezone">Is Permanent ?</label>
                                            <select class="form-control select2-single" name="txtIsPermanent" required>
                                                <option value="P" <?php if($employee_info[0]['is_permanent'] == 'P'){ echo 'selected'; } ?>>Permanent</option>
                                                <option value="PT" <?php if($employee_info[0]['is_permanent'] == 'PT'){ echo 'selected'; } ?>>Per Time</option>
                                            </select>
                                        </div>

                                        <div class="form-group col-md-4">
                                            <label for="txtMobile">National ID/Passport</label>
                                            <input type="text" class="form-control"  name="txtNID"  value="<?php echo $employee_info[0]['national_id']; ?>"  required id="txtNID">
                                        </div>
                                  </div>



                                  <div class="form-row">

                                    <div class="form-group col-md-4">
                                        <label for="txtMobile">Is Leave Auto Approve ?</label>
                                        <select class="form-control select2-single" required name="leave_auto_approve">
                                            <option value="0" <?php if($employee_info[0]['leave_auto_approve'] == '0'){ echo 'selected'; } ?>>No</option>
                                            <option value="1" <?php if($employee_info[0]['leave_auto_approve'] == '1'){ echo 'selected'; } ?>>Yes</option>
                                        </select>
                                    </div>

                                      <div class="form-group col-md-4">
                                          <label for="Country">Reporting Boss</label>
                                          <select class="form-control select2-single" name="txtReportingBoss">
                                              <option value="">--Select--</option>
                                              <?php

                                              if (count($employees)) {
                                                  foreach ($employees as $list) {

                                                      ?>
                                                      <option
                                                          value="<?php echo $list['id']; ?>" <?php if($employee_info[0]['reporting_boss_id'] == $list['id']){ echo 'selected'; } ?>><?php echo $list['name'].'('.$list['code'].')'; ?></option>
                                                  <?php
                                                  }
                                              }
                                              ?>
                                          </select>
                                      </div>

                                      <div class="form-group col-md-4">
                                          <label for="Shift">Shift</label>
                                          <select class="form-control select2-single" name="shift_id">
                                              <!-- <option value="0">Branch Shift</option> -->
                                              <?php
                                              if (count($shifts)) {
                                                  foreach ($shifts as $list) {
                                                      ?>
                                                      <option value="<?php echo $list['id']; ?>" <?php if($employee_info[0]['shift_id'] == $list['id']){ echo 'selected'; } ?>><?php echo $list['name']; ?></option>
                                                  <?php
                                                  }
                                              }
                                              ?>
                                          </select>
                                      </div>
                                  </div>



                              <div class="form-row">
                                  <div class="form-group col-md-4">
                                      <label for="txtMobile">Mobile</label>
                                      <input type="text" class="form-control" name="txtMobile" value="<?php echo $employee_info[0]['mobile']; ?>" required id="txtMobile">
                                  </div>
                                  <div class="form-group col-md-4">
                                      <label for="inputPassword4">Email</label>
                                      <input type="text" class="form-control" id="txtEmail" required
                                          name="txtEmail"  value="<?php echo $employee_info[0]['email']; ?>" >
                                  </div>
                                  <div class="form-group col-md-4">
                                      <label for="txtProcessCode">Process Code</label>
                                      <input type="text" class="form-control" name="txtProcessCode" value="<?php echo $employee_info[0]['process_code']; ?>" id="txtProcessCode">
                                  </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputAddress">Address</label>
                                    <input type="text" class="form-control" required id="txtAddress" name="txtAddress"
                                      required   value="<?php echo $employee_info[0]['address']; ?>" >
                                </div>


                                <div class="form-group">
                                  <label for="inputAddress">Photo</label>
                                  <br>
                                  <img src="<?php echo base_url(); ?>media/employee/<?php echo $employee_info[0]['photo_location']; ?>" style=" border:1px solid #666666;" width="150" height="150">
                                  <br><br>
                                  <input type="file" id="inputImage" name="txtPhoto"
                                      accept=".jpg,.jpeg,.png,.gif,.bmp,.tiff">
                                </div>

                                <button type="submit" class="btn btn-primary d-block mt-3">Update</button>
                    </form>
            </div>
       </div>
      </div>
</div>
