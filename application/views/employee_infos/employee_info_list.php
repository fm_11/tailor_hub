<script type="text/javascript">
    function msgStatusUpdate(id, status) {
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        }
        else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("status_sction_" + id).innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>employees/updateMsgStatusEmployeeStatus?id=" + id + '&&status=' + status, true);
        xmlhttp.send();
    }

</script>


<div class="col-12 mb-4">
    <div class="card">
        <div class="card-body">
           <h5 class="card-title"><?php echo $title; ?></h5>

           <?php
            $name = $this->session->userdata('search_employee_name');
            ?>

          <form class="form-inline" method="post" action="<?php echo base_url(); ?>employees/index">
              <label class="sr-only" for="inlineFormInputName2">Name</label>
              <input type="text" class="form-control mb-2 mr-sm-2" value="<?php echo $name; ?>" name="name" id="inlineFormInputName2" placeholder="Name or Employee ID">

             <!--<div class="form-group col-md-3 mb-3">
              <label class="sr-only" for="inlineFormInputGroupUsername2">Branch</label>
              <select class="select2-single"  name="txtBranch">
                  <option value="">--Select--</option>
                  <?php

                  if (count($branches)) {
                      foreach ($branches as $list) {

                          ?>
                          <option
                              value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                      <?php
                      }
                  }
                  ?>
              </select>
            </div>!-->

            <button type="submit" class="btn btn-sm btn-outline-primary mb-2">Search</button>
          </form>



            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Employee ID</th>
                        <th scope="col">Branch</th>
                        <th scope="col">Designation</th>
                        <th scope="col">Mobile</th>
                        <th scope="col">Status</th>
                        <th scope="col">Action</th>

                    </tr>
                </thead>
                <tbody>
                  <?php
                    $i = (int)$this->uri->segment(3);
                    foreach ($employees as $row):
                      $i++;
                      ?>
                    <tr>
                        <th scope="row"><?php echo $i; ?></th>
                        <td><?php echo $row['name']; ?></td>
                        <td><?php echo $row['code']; ?></td>
                        <td><?php echo $row['branch_name']; ?></td>
                        <td><?php echo $row['post_name']; ?></td>
                        <td><?php echo $row['mobile']; ?></td>
                        <td id="status_sction_<?php echo $row['id']; ?>">
                            <?php
                            if ($row['status'] == 1) {
                                ?>
                                <a title="Active" href="#"
                                   onclick="msgStatusUpdate(<?php echo $row['id']; ?>,<?php echo $row['status']; ?>)">
                                   <button type="button" class="btn btn-primary btn-xs mb-1">Active</button>
                                 </a>
                            <?php
                            } else {
                                ?>
                                <a title="Inactive" href="#"
                                   onclick="msgStatusUpdate(<?php echo $row['id']; ?>,<?php echo $row['status']; ?>)">
                                   <button type="button" class="btn btn-primary btn-xs mb-1">Inactive</button>
                                 </a>
                            <?php
                            }
                            ?>

                        </td>
                        <td>

                               <a href="<?php echo base_url(); ?>employees/edit/<?php echo $row['id']; ?>"
                                  title="Edit">
                                  <button type="button" class="btn btn-primary btn-xs mb-1">
                                    Edit
                                  </button>
                                </a>

                               <!-- <a href="<?php echo base_url(); ?>employees/delete/<?php echo $row['id']; ?>"
                                  onclick="return deleteConfirm()" title="Delete">
                                  <button type="button" class="btn btn-primary btn-xs mb-1">
                                    Delete
                                  </button>

                                </a> -->

                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <?php echo $this->pagination->create_links(); ?>
        </div>
    </div>
</div>
