<div class="col-12 mb-4">
    <div class="card">
        <div class="card-body">
            <h5 class="card-title"><?php echo $title; ?></h5>

            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Branch</th>
                        <th scope="col">Designation</th>
                        <th scope="col">Mobile</th>
                        <th scope="col">Email</th>
                        <th scope="col">Status</th>
                        <th scope="col">Current Time</th>
                        <th scope="col">
                          <div class="btn-group mb-1">
                                  <button class="btn btn-secondary btn-xs dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                      Action
                                  </button>
                                  <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 25px, 0px); top: 0px; left: 0px; will-change: transform;">
                                      <span class="dropdown-item" href="javascript:void">Select All
                                        <input type="checkbox" class="checkAll">
                                      </span>
                                      <span class="dropdown-item">
                                        <button type="button" id="dashboard_email_send" class="btn btn-secondary btn-xs mb-1" data-toggle="modal"
                                            data-target="#SendEmailModalContent" data-whatever="@getbootstrap">
                                            Send Email
                                        </button>
                                      </span>
                                  </div>
                          </div>
                        </th>
                    </tr>
                </thead>
                <tbody>
                  <?php
                     $i = 0;
                    foreach ($employees as $row):
                      $i++;
                      ?>
                    <tr>
                        <th scope="row">
                          <?php echo $i; ?>
                        </th>
                        <td><?php echo $row['name'].' ('.$row['code'].')'; ?></td>
                        <td><?php echo $row['branch_name']; ?></td>
                        <td><?php echo $row['post_name']; ?></td>
                        <td>
                          <?php echo $row['mobile']; ?>
                          <input type="hidden" id="email_<?php echo $i; ?>" value="<?php echo $row['email']; ?>"/>
                        </td>
                        <td><?php echo $row['email']; ?></td>
                        <td><?php echo $row['todays_status']; ?></td>
                        <td><?php echo date('h:i:s A', strtotime($row['current_time'])); ?></td>
                        <td><input type="checkbox" value="<?php echo $i; ?>" class="checkbox"  name="is_allow"></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>


<div class="modal fade" id="SendEmailModalContent" tabindex="-1" role="dialog"
                                aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalContentLabel">Send New message</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form action="<?php echo base_url(); ?>employees/dashboard_email_send" method="post" enctype="multipart/form-data">
                                                <div class="form-group">
                                                    <label for="recipient-name"
                                                        class="col-form-label">Recipient:</label>
                                                    <input type="text" class="form-control" name="recipientList" id="recipientList">
                                                </div>
                                                <div class="form-group">
                                                    <label for="header-name"
                                                        class="col-form-label">Header:</label>
                                                    <input type="text" class="form-control" name="header" id="Header">
                                                </div>
                                                <div class="form-group">
                                                    <label for="message-text" class="col-form-label">Message:</label>
                                                    <textarea class="form-control" name="message" id="message-text"></textarea>
                                                </div>
                                                <button type="submit" class="btn btn-primary d-block mt-3">Send message</button>
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary"
                                                data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
