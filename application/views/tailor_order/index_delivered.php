
<script type="text/javascript">
    function msgStatusUpdate(status,id) {
        // alert(id);
        // alert(status);

        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        }
        else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                // document.getElementById("status_sction_" + id).innerHTML = xmlhttp.responseText;
                window.location.reload(true);
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>tailor_order/updateMsgOrderStatus?id=" + id + '&&status=' + status, true);
        xmlhttp.send();
    }

</script>


<div class="col-12 mb-4">
    <div class="card">
        <div class="card-body">
           <h5 class="card-title"><?php echo $title; ?></h5>

           <?php
            $name = $this->session->userdata('customer_name');
            // $order_status = $this->session->userdata('order_status');
            ?>

          <form class="form-inline" method="post" action="<?php echo base_url(); ?>tailor_order/index_delivered">

              <div class="form-group col-md-6 mb-6">
                <label class="sr-only" for="inlineFormInputName2">Name</label>
                <input type="text" class="form-control col-md-12 mr-sm-12" value="<?php echo $name; ?>" name="customer_name" id="inlineFormInputName2" placeholder="Name or Phone Number">

             </div>
               <!-- <div class="form-group col-md-4 mb-4">
               <label class="sr-only" for="inlineFormInputName2">Status</label>
               <select class="select2-single col-md-12"  name="order_status">
                   <option value="all" <?php if ($order_status == 'all' || $order_status == '') {echo 'selected';} ?>>--All Status--</option>
                   <option value="O" <?php if ($order_status == 'O' && $order_status != '') {echo 'selected';} ?>>On Process</option>
                   <option value="R" <?php if ($order_status == 'R' && $order_status != '') {echo 'selected';} ?>>Reject</option>
                   <option value="D" <?php if ($order_status == 'D' && $order_status != '') {echo 'selected';} ?>>Delivered</option>
                   <option value="P" <?php if ($order_status == 'P' && $order_status != '') {echo 'selected';} ?>>Pending</option>
               </select>
             </div> -->
            <button type="submit" class="btn btn-sm btn-outline-primary mb-2">Search</button>
          </form>



            <table class="table table-striped">
                <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Tailor Name</th>
                      <th scope="col">Customer Name</th>
                      <th scope="col">Order Date</th>
                      <th scope="col">Phone</th>
                      <th scope="col">Address</th>
                      <th scope="col">Total Amount</th>
                      <th scope="col">Order Status</th>
                      <th scope="col">Action</th>

                    </tr>
                </thead>
                <tbody>
                  <?php
                    $i = (int)$this->uri->segment(3);
                    foreach ($orders as $row):
                      $i++;
                      ?>
                    <tr>
                      <th scope="row"><?php echo $i; ?></th>
                      <td><?php echo $row['tailor_name']; ?></td>
                      <td><?php echo $row['customer_name']; ?></td>
                      <td><?php echo $row['booking_date']; ?></td>
                      <td><?php echo $row['customer_phone']; ?></td>
                      <td><?php echo $row['shipping_address']; ?></td>

                      <td><?php echo $row['total_cost']; ?></td>
                      <td><?php if($row['status']=='R')
                      {
                        echo "Rejected";
                      }elseif ($row['status']=='D') {
                        echo "Delivered";
                      }
                      elseif ($row['status']=='O'){ ?>
                       <select onchange="msgStatusUpdate(this.value,<?php echo $row['id']; ?>)" class="select2-single col-md-12"  >
                        <option value="O" selected >On Process</option>
                        <option value="R">Reject</option>
                        <option value="D">Delivered</option>
                        <option value="P">Pending</option>
                        </select>
                     <?php }else{ ?>

                       <select onchange="msgStatusUpdate(this.value,<?php echo $row['id']; ?>)" class="select2-single col-md-12">
                        <option value="O" >On Process</option>
                        <option value="R">Reject</option>
                        <option value="D">Delivered</option>
                        <option value="P" selected>Pending</option>
                        </select>
                       <?php } ?>

                      </td>
                      <td>
                       <a href="<?php echo base_url(); ?>tailor_order/view/<?php echo $row['id']; ?>"
                          title="Edit">
                          <button type="button" class="btn btn-primary btn-xs mb-1">
                            View
                          </button>
                        </a>
                      </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <?php echo $this->pagination->create_links(); ?>
        </div>
    </div>
</div>
