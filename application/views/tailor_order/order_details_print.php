<hr>
<style>
.my-custom-scrollbar {
/* position: relative;
height: 500px;
overflow: auto; */
}
.table-wrapper-scroll-y {
display: block;
}
</style>
<?php
if(isset($excel)){
  header('Content-type: application/excel');
  $filename = 'Process Report.xls';
  header('Content-Disposition: attachment; filename='.$filename);
}
?>
<div class="table-wrapper-scroll-y my-custom-scrollbar">
  <div class="row">
      <div class="col-12">
          <div class="card mb-4">
              <div class="card-body">
                  <h5 class="mb-4"><?php echo $title; ?></h5>
                      <div class="row">
                          <div class="form-group col-md-6">
                              <h6 style="font-weight:bold;">Customer Info</h6>
                              <span><?php echo $order_details[0]['customer_name'];?></span><br>
                              <span><b>Phone:</b> <?php echo $order_details[0]['customer_phone'];?></span><br>
                              <span><b>Email:</b> <?php echo $order_details[0]['customer_email'];?></span><br>
                              <span><b>Address:</b> <?php echo $order_details[0]['shipping_address'];?></span><br>
                          </div>
                          <div class="form-group col-md-6">
                            <span><b>Order No:</b> <?php echo $order_details[0]['transaction_id'];?></span><br>
                            <span><b>Order Date:</b> <?php echo $order_details[0]['booking_date'];?></span><br>
                            <span><b>Order Time:</b> <?php echo date('h:i A', strtotime($order_details[0]['booking_time']));?></span><br>
                            <span><b>Status: </b> <?php if($order_details[0]['status']=='R')
                            {
                              echo "Rejected";
                            }elseif ($order_details[0]['status']=='A') {
                              echo "Approved";
                            }elseif ($order_details[0]['status']=='C') {
                              echo "Cancel";
                            }else{
                                echo "Pending";
                            }
                            ?></span><br>


                          </div>

                      </div>


                  <hr>
                  <table class="table table-striped">

                      <tbody>
                      <?php
                      $i = 0;
                      foreach ($order_details as $details):
                          $i++;
                          ?>
                          <tr>
                              <th scope="col">SL#</th>
                              <th scope="col">Design</th>
                              <?php if(!empty($details['measurement_level_1'])){?>
                              <th scope="col"><?php echo $details['measurement_level_1'];?></th>
                              <?php }?>
                              <?php if(!empty($details['measurement_level_2'])){?>
                              <th scope="col"><?php echo $details['measurement_level_2'];?></th>
                              <?php }?>
                              <?php if(!empty($details['measurement_level_3'])){?>
                              <th scope="col"><?php echo $details['measurement_level_3'];?></th>
                              <?php }?>
                              <?php if(!empty($details['measurement_level_4'])){?>
                              <th scope="col"><?php echo $details['measurement_level_4'];?></th>
                              <?php }?>
                              <?php if(!empty($details['measurement_level_5'])){?>
                              <th scope="col"><?php echo $details['measurement_level_5'];?></th>
                              <?php }?>
                              <?php if(!empty($details['measurement_level_6'])){?>
                              <th scope="col"><?php echo $details['measurement_level_6'];?></th>
                              <?php }?>
                              <th scope="col">Neck Pattern</th>
                              <th scope="col">Fabric</th>
                              <th scope="col">Quantity</th>
                              <th scope="col">Itam Cost</th>
                              <th scope="col">Total</th>

                          </tr>
                          <tr>
                              <th ><?php echo $i; ?></th>
                              <td><?php if(!empty($details['item_image_path'])){?>
                                  <span>Item Image</span></br>
                                    <img style="width: 50px;height:50px;" src="<?php echo base_url(); ?>media/tailor_item/<?php echo $details['item_image_path'];?>" /></br>
                              <?php }?>
                              <?php if(!empty($details['client_design'])){?>
                                <span>User Choose Image</span></br>
                                    <img style="width: 50px;height:50px;" src="<?php echo base_url(); ?>media/website/order_image/<?php echo $details['client_design'];?>" /></br>
                              <?php }?>
                              <?php if(!empty($details['embroidery'])){?>
                                  <span>Embroidery Image</span></br>
                                    <img style="width: 50px;height:50px;" src="<?php echo base_url(); ?>media/website/order_image/<?php echo $details['embroidery'];?>" />
                              <?php }?>
                              </td>
                              <?php if(!empty($details['measurement_level_1'])){?>
                              <td><?php echo $details['measurement_value_1'];?></td>
                              <?php }?>
                              <?php if(!empty($details['measurement_level_2'])){?>
                              <td><?php echo $details['measurement_value_2'];?></td>
                              <?php }?>
                              <?php if(!empty($details['measurement_level_3'])){?>
                              <td><?php echo $details['measurement_value_3'];?></td>
                              <?php }?>
                              <?php if(!empty($details['measurement_level_4'])){?>
                              <td><?php echo $details['measurement_value_4'];?></td>
                              <?php }?>
                              <?php if(!empty($details['measurement_level_5'])){?>
                              <td><?php echo $details['measurement_value_5'];?></td>
                              <?php }?>
                              <?php if(!empty($details['measurement_level_6'])){?>
                              <td><?php echo $details['measurement_value_6'];?></td>
                              <?php }?>
                              <td><?php echo $details['neck_pattern']; ?></td>
                              <td><?php echo $details['fabric']; ?></td>
                              <td><?php echo $details['quantity']; ?></td>
                              <td><?php echo $details['item_cost']; ?></td>
                              <td><?php echo $details['total_cost']; ?></td>


                          </tr>

                      <?php endforeach; ?>
                      </tbody>
                      <tfoot>
                          <tr>
                            <td scope="row" class="text-left" colspan="2"><b>Grand Total:<b></td>
                            <td scope="row"><?php echo $order_details[0]['grand_total'];?></td>
                          </tr>
                        </tfoot>
                        <br>
                  </table>

                  <div class="form-row">
                     <div class="form-group col-md-12">
                        <label for="inputEmail4">Remarks:</label><br>
                        <?php echo $order_details[0]['remarks'];?>
                         <!-- <textarea class="form-control" name="remarks" readonly><?php echo $order_details[0]['remarks'];?></textarea> -->
                     </div>
                    </div>
</div>
