<style>
   .required_class{
     color: red;
   }
</style>
<div class="row">
            <div class="col-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <h5 class="mb-4"><?php echo $title; ?></h5>

                            <form action="<?php echo base_url(); ?>contact/update_contact_info/" method="post" enctype="multipart/form-data">

                                <div class="form-row">
                                  <div class="form-group col-md-6">
                                      <label for="txtMobile">Mobile<span class="required_class">*</span></label>
                                      <input type="text" class="form-control" name="phone" value="<?php echo $contact_info->phone;  ?>" id="phone">
                                      <input type="hidden" class="form-control" name="id" value="<?php echo $contact_info->id;  ?>" id="id">
                                  </div>
                                    <div class="form-group col-md-6">
                                        <label for="inputPassword4">Email<span class="required_class">*</span></label>
                                        <input type="text" class="form-control" id="email"
                                            name="email" required value="<?php echo $contact_info->email;  ?>">
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="inputAddress">Address<span class="required_class">*</span></label>
                                    <input type="text" class="form-control" id="address" name="address"
                                      required  value="<?php echo $contact_info->address;  ?>">
                                </div>

                                <div class="form-row">
                                  <div class="form-group col-md-3">
                                      <label for="txtMobile">Opening Time<span class="required_class">*</span></label>
                                      <input type="time" class="form-control" name="open_time" required value="<?php echo $contact_info->open_time;  ?>" id="open_time">
                                  </div>
                                    <div class="form-group col-md-3">
                                        <label for="inputPassword4">Closing Time<span class="required_class">*</span></label>
                                        <input type="time" class="form-control" id="closed_time" required name="closed_time" value="<?php echo $contact_info->closed_time;  ?>">
                                    </div>

                                    <div class="form-group col-md-3">
                                        <label for="txtMobile">Opening Day<span class="required_class">*</span></label>
                                        <input type="text" class="form-control" name="open_day" required placeholder="Sun" value="<?php echo $contact_info->open_day;  ?>" id="open_day">
                                    </div>
                                      <div class="form-group col-md-3">
                                          <label for="inputPassword4">Closing Day<span class="required_class">*</span></label>
                                          <input type="text" class="form-control" id="closed_day" required name="closed_day" placeholder="Fri" value="<?php echo $contact_info->closed_day;?>">
                                      </div>
                                </div>
                                <div class="form-row">
                                  <div class="form-group col-md-6">
                                    <?php if(!empty($contact_info->banner_path)){?>
                                      <label for="txtMobile">Upload Banner</label>
                                      <input type="file" class="form-control" name="banner">
                                      <?php }  else { ?>
                                        <label for="txtMobile">Upload Banner<span class="required_class">*</span></label>
                                        <input type="file" class="form-control" required name="banner">
                                      <?php }?>
                                  </div>
                                  <div class="form-group col-md-6">
                                        <?php if(!empty($contact_info->banner_path)){?>
                                       <img style="width: 100px;height:100px;" src="<?php echo base_url(); ?>media/website/banner_image/<?php echo $contact_info->banner_path;?>" />
                                        <?php }?>
                                  </div>

                                </div>
                                <button type="submit" class="btn btn-primary d-block mt-3">Update</button>
                    </form>
            </div>
</div>
</div>
</div>
