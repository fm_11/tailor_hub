<div class="row">
    <div class="col-12">
        <div class="card mb-4">
            <div class="card-body">
                <h5 class="mb-4"><?php echo $title; ?> <small style="color: red;">All Red marked are required</small></h5>

                <form action="<?php echo base_url(); ?>Course_prefixes/edit" method="post" enctype="multipart/form-data">
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="inputEmail4">Name<span style="color:red;">*</span></label>
                            <input type="text" class="form-control" id="name" required name="name" value="<?php echo $course_prefix_info->name;  ?>" placeholder="Branch Name">
                            <input type="hidden" name="id"  class="smallInput wide" value="<?php echo $course_prefix_info->id;  ?>">
                        </div>
                        <!-- <div class="form-group col-md-6">
                            <label for="inputPassword4">Code</label>
                            <input type="text" class="form-control" id="code" required
                                   name="code" value="<?php echo $course_prefix_info->code;  ?>">
                        </div> -->
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="txtMobile">Weight<span style="color:red;">*</span></label>
                            <input type="text" class="form-control" name="weight" value="<?php echo $course_prefix_info->weight;  ?>" required id="weight">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Status<span style="color:red;">*</span></label>

                            <select class="form-control select2-single" required name="is_active">
                                <option <?php if ($course_prefix_info->is_active == 0) {echo 'selected';}  ?> value="0">No</option>
                                <option <?php if ($course_prefix_info->is_active == 1) {echo 'selected';}  ?> value="1">Yes</option>
                            </select>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary d-block mt-3">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
