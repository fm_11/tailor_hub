<style>
.txt-span {
  display: block;
  width: 100%;

  font-size: 1rem;
  line-height: 1.5;
  color:#495057;
  background-color:#fff;
  background-clip: padding-box;

  border-radius: .25rem;
  -webkit-transition: border-color 0.15s ease-in-out,-webkit-box-shadow 0.15s ease-in-out;
  transition: border-color 0.15s ease-in-out,-webkit-box-shadow 0.15s ease-in-out;
  transition: border-color 0.15s ease-in-out,box-shadow 0.15s ease-in-out;
  transition: border-color 0.15s ease-in-out,box-shadow 0.15s ease-in-out,-webkit-box-shadow 0.15s ease-in-out;
}
</style>

<div class="row">
    <div class="col-12">
        <div class="card mb-4">
            <div class="card-body">
                <h5 class="mb-4"><?php echo $title; ?></h5>


                    <hr>

                      <hr>
                      <h5 class="card-title">University Application List</h5>
                      <div class="text-right">
                          <!-- <button type="button" class="btn btn-dark default btn-sm mb-1" data-toggle="modal" data-target="#myReference">Add Reference</button> -->
                      </div>

                      <table class="table table-striped">
                          <thead>
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">Application Code</th>
                            <th scope="col">University Name</th>
                            <th scope="col">Course Level</th>
                            <th scope="col">Subject</th>
                            <th scope="col">Month </th>
                            <th scope="col">Year </th>
                            <th scope="col">Current Status </th>
                            <th scope="col">Country </th>
                            <th scope="col">Process Officer </th>
                          <th scope="col">Action </th>

                          </tr>
                          </thead>
                          <tbody>
                          <?php
                          $i = 0;
                          foreach ($university_application as $ref_row):
                              $i++;
                              ?>
                              <tr>
                                  <th scope="row"><?php echo $i; ?></th>
                                  <td>
                                    <a style="text-decoration:underline;"   href="<?php echo base_url(); ?>applicants/view_status/<?php echo $ref_row['id']; ?>">
                                      <?php echo $ref_row['application_code']; ?>
                                    </a>
                                  </td>
                                  <td>
                                    <a style="text-decoration:underline;"   href="<?php echo base_url(); ?>institutions/view/<?php echo $ref_row['institution_id']; ?>">
                                      <?php echo $ref_row['university_name']; ?>
                                    </a>
                                  </td>
                                  <td><?php echo $ref_row['course_level']; ?></td>
                                  <td>
                                    <a style="text-decoration:underline;"   href="<?php echo base_url(); ?>Courses/view/<?php echo $ref_row['subject_id']; ?>">
                                      <?php echo $ref_row['course_title']; ?>
                                    </a>
                                  </td>
                                  <td><?php echo $ref_row['cc_month']; ?></td>
                                  <td><?php echo $ref_row['cc_year']; ?></td>
                                  <td><?php  echo $ref_row['current_status']; ?></td>
                                  <td><?php echo $ref_row['country']; ?></td>
                                <td><?php echo $ref_row['process_officer']; ?></td>
                                <td>
                                <?php if(isset($ref_row['sop_img_path']) && $ref_row['sop_img_path']!='0'){?>
                                  <a target="_blank" href="<?php echo base_url(); ?>media/applicant_edu/<?php echo $ref_row['sop_img_path']; ?>">
                                    <button type="button" class="btn btn-secondary btn-xs mb-1">Download(I)</button>
                                  </a>
                                <?php }?>
                                  <?php if(isset($ref_row['others_img_path']) && $ref_row['others_img_path']!='0'){?>
                                  <a target="_blank" href="<?php echo base_url(); ?>media/applicant_edu/<?php echo $ref_row['others_img_path']; ?>">
                                    <button type="button" class="btn btn-secondary btn-xs mb-1">Download(O)</button>
                                  </a>
                                    <?php }?>
                                </td>
                              </tr>

                          <?php endforeach; ?>
                          </tbody>

                      </table>
                      <hr>

            </div>

        </div>

    </div>
</div>
</div>
