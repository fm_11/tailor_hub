<div class="row">
    <div class="col-12">
        <div class="card mb-4">
            <div class="card-body">
                <h5 class="mb-4"><?php echo $title; ?> <small style="color: red;">All Red marked are required</small></h5>

                <form action="<?php echo base_url(); ?>agents/edit" method="post" enctype="multipart/form-data">
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="inputAddress">Upload Logo (120 x 120)</label><br>
                            <input type="file" id="logo" name="logo"
                                   accept=".jpg,.jpeg,.png,.gif,.bmp,.tiff">
                                   <!-- <a target="_blank"  href="<?php echo base_url(); ?>media/agent/<?php echo $agents->logo; ?>">
                                     <button type="button" class="btn btn-secondary btn-xs mb-1">Download</button>
                                   </a> -->
                        </div>


                        <div class="form-group col-md-4">
                            <label for="Companyname">Company Name<span style="color:red;">*</span></label>
                            <input type="text" class="form-control" id="company_name"
                                   name="company_name" value="<?php echo $agents->company_name;  ?>" required>

                        </div>
                        <div class="form-group col-md-4">
                            <label for="Personalname">Person Name<span style="color:red;">*</span></label>
                            <input type="text" class="form-control" id="personal_name"
                                   name="personal_name" value="<?php echo $agents->personal_name;  ?>" required>

                        </div>
                    </div>

                    <div class="form-row">

                        <div class="form-group col-md-4">
                            <label for="address">Address</label>
                            <input type="text" class="form-control" id="address"
                                   name="address" value="<?php echo $agents->address;  ?>">

                        </div>
                        <div class="form-group col-md-4">
                            <label for="phone">Phone<span style="color:red;">*</span></label>
                            <div class="row">
                              <div class="form-group col-md-6">
                                <select class="form-control select2-single"  name="country_code" required>
                                    <option value="">--Select--</option>
                                    <?php

                                    if (count($country_code)) {
                                        foreach ($country_code as $list) {
                                          ?>
                                          <option
                                              <?php if ($list['id'] == $agents->country_code) {
                                              echo 'selected';
                                          } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                          <?php
                                        }
                                    }
                                    ?>
                                </select>
                              </div>
                              <div class="form-group col-md-6" style="margin-left: -30px;">
                                <input type="text" class="form-control" id="phone"
                                       name="phone" required value="<?php echo $agents->phone;  ?>">
                              </div>
                            </div>


                        </div>
                        <div class="form-group col-md-4">
                            <label for="email">Email<span style="color:red;">*</span></label>
                            <input type="text" class="form-control" id="email"
                                   name="email" required value="<?php echo $agents->email;  ?>">

                        </div>
                    </div>

                    <div class="form-row">

                        <div class="form-group col-md-4">
                            <label for="txtMobile">Admission officer (NE)<span style="color:red;">*</span></label>
                            <select class="form-control select2-single" name="admission_officer" required>
                                <option value="">Select</option>
                                <?php

                                if (count($employees)) {
                                    foreach ($employees as $list) {
                                        ?>
                                        <option
                                        <?php if ($list['id'] == $agents->admission_officer) {
                                            echo 'selected';
                                        } ?>
                                                value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="txtMobile">Marketing Officer (NE)<span style="color:red;">*</span></label>
                            <select class="form-control select2-single" name="marketing_officer" required>
                                <option value="">Select</option>
                                <?php

                                if (count($employees)) {
                                    foreach ($employees as $list) {
                                        ?>
                                        <option
                                        <?php if ($list['id'] == $agents->marketing_officer) {
                                            echo 'selected';
                                        } ?>

                                            value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="bankName">Bank Name</label>
                            <input type="text" class="form-control" id="bank_name"
                                   name="bank_name" value="<?php echo $agents->bank_name;  ?>">

                        </div>
                    </div>

                    <div class="form-row">

                        <div class="form-group col-md-4">
                            <label for="accountNumber">Account Number</label>
                            <input type="number" class="form-control" id="account_number"
                                   name="account_number" value="<?php echo $agents->account_number;  ?>">

                        </div>
                        <div class="form-group col-md-4">
                            <label for="accountName">Account Name</label>
                            <input type="text" class="form-control" id="account_name"
                                   name="account_name" value="<?php echo $agents->account_name;  ?>">

                        </div>
                        <div class="form-group col-md-4">
                            <label for="branchName">Branch Name</label>
                            <input type="text" class="form-control" id="branch_name"
                                   name="branch_name" value="<?php echo $agents->branch_name;  ?>">

                        </div>

                    </div>
                    <div class="form-row">


                        <div class="form-group col-md-3">
                          <label class="checkbox-inline"></label>
                          <input name="status" <?php if ($agents->status == '1') {
                                  echo 'checked';
                              } ?> type="checkbox" value="1">   Is Active?

                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputPassword4">Password<span style="color:red;">*</span></label>
                            <input type="password" class="form-control" id="password"
                                   name="password" placeholder="Optional" required>

                        </div>
                    </div>
                 <input type="hidden" class="form-control" id="id"
                        name="id" value="<?php echo $agents->id; ?>">
                    <button type="submit" class="btn btn-primary d-block mt-3">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
