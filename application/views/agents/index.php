<script type="text/javascript">
    function msgStatusUpdate(id, status) {
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        }
        else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("status_sction_" + id).innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>agents/updateMsgStatusAgentStatus?id=" + id + '&&status=' + status, true);
        xmlhttp.send();
    }
    function send_lead_email(){
            var leadList = [];
            $.each($("input[name='is_allow_email']:checked"), function(){
                leadList.push($(this).val());
            });
            if(leadList.length == 0){
              alert("Please select atleast one agents.");
              return false;
            }else{
              var arrayLength = leadList.length;
              var returnString = '';
              for (var i = 0; i < arrayLength; i++) {
                  console.log(leadList[i]);
                  if(i == 0){
                    returnString = (returnString + $("#email_" + leadList[i]).val());
                  }else{
                    returnString = (returnString + ', ' + $("#email_" + leadList[i]).val());
                  }
              }
              $("#recipientList").val(returnString);
              return true;
            }
    };
</script>
<div class="col-12 mb-4">
    <div class="card">
        <div class="card-body" style="overflow-x:auto;">
            <h5 class="card-title"><?php echo $title; ?></h5>
            <?php
             $code = $this->session->userdata('code');
              $lead_source_id = $this->session->userdata('lead_source_id');
              $country_id = $this->session->userdata('country_id');
              $name = $this->session->userdata('name');
             ?>
            <form class="form-inline" method="post" action="<?php echo base_url(); ?>agents/index">
              <div class="form-group col-md-4 mb-6">
               <label>Code</label>
                 <input type="text" class="form-control col-md-12" value="<?php echo $code; ?>" name="code" id="code">
             </div>

            <div class="form-group col-md-6 mb-6">

              <label>Name/Email/Contact No.</label>
              <input type="text" class="form-control col-md-12" value="<?php echo $name; ?>" name="name" id="inlineFormInputName2">

            </div>
             <button type="submit" class="btn btn-sm btn-outline-primary mb-2">Search</button>
            </form>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Code</th>
                    <th scope="col">Company Name</th>
                    <th scope="col">Person Name</th>
                    <th scope="col">Phone</th>
                    <th scope="col">Email</th>
                    <th scope="col">Registration Date</th>
                    <th scope="col">Admission Officer</th>
                    <th scope="col">Status</th>
                    <th scope="col">Action</th>
                    <th scope="col">
                      <div class="btn-group mb-1">
                              <button class="btn btn-secondary btn-xs dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  Action
                              </button>
                              <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 25px, 0px); top: 0px; left: -60px; will-change: transform;">
                                  <span class="dropdown-item" href="javascript:void">Select All
                                    <input type="checkbox" class="checkAll">
                                  </span>
                                  <span class="dropdown-item">
                                    <button type="button" onclick="send_lead_email();" id="send_lead_email" class="btn btn-secondary btn-xs mb-1" data-toggle="modal"
                                        data-target="#SendMailLeadModalContent" data-whatever="">
                                        Send Email
                                    </button>
                                  </span>
                              </div>
                      </div>
                    </th>
                </tr>
                </thead>
                <tbody>
                <?php
                $i = (int)$this->uri->segment(3);
                foreach ($agents as $row):
                    $i++;
                    ?>
                    <tr>
                        <th scope="row"><?php echo $i; ?></th>
                        <td style="min-width: 101px;">
                          <a style="text-decoration:underline;" target="_blank"  href="<?php echo base_url(); ?>agents/view/<?php echo $row['id']; ?>">
                            <?php echo $row['code']; ?>
                          </a>
                        </td>
                        <td><?php echo $row['company_name']; ?></td>
                        <td><?php echo $row['personal_name']; ?></td>
                        <td><?php echo $row['phone']; ?></td>
                        <td><?php echo $row['email']; ?></td>
                        <td><?php
                          if(!empty($row['process_date']))
                          {
                            $date=date_create($row['process_date']);
                             echo date_format($date,"Y-m-d");
                          }
                         ?>
                        </td>
                          <td><?php echo $row['admission_officer_name']; ?></td>
                        <td id="status_sction_<?php echo $row['id']; ?>">
                            <?php
                            if ($row['status'] == 1) {
                                ?>
                                <a title="Active" href="#"
                                   onclick="msgStatusUpdate(<?php echo $row['id']; ?>,<?php echo $row['status']; ?>)">
                                   <button type="button" class="btn btn-primary btn-xs mb-1">Active</button>
                                 </a>
                            <?php
                            } else {
                                ?>
                                <a title="Inactive" href="#"
                                   onclick="msgStatusUpdate(<?php echo $row['id']; ?>,<?php echo $row['status']; ?>)">
                                   <button type="button" class="btn btn-primary btn-xs mb-1">Inactive</button>
                                 </a>
                            <?php
                            }
                            ?>
                        </td>

                        <td>
                            <a href="<?php echo base_url(); ?>agents/edit/<?php echo $row['id']; ?>"
                               title="Edit">
                                <button type="button" class="btn btn-primary btn-xs mb-1">
                                    Edit
                                </button>
                            </a>
                        </td>
                        <td>
                        <input type="checkbox" value="<?php echo $i; ?>" class="checkbox"  name="is_allow_email">
                        <input type="hidden" id="email_<?php echo $i; ?>" value="<?php echo $row['email']; ?>"/>
                      </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <?php echo $this->pagination->create_links(); ?>
        </div>
    </div>
</div>


          <div class="modal fade" id="SendMailLeadModalContent" tabindex="-1" role="dialog"
                                          aria-hidden="true">
                                          <div class="modal-dialog" role="document">
                                              <div class="modal-content">
                                                  <div class="modal-header">
                                                      <h5 class="modal-title" id="exampleModalContentLabel">Send New message</h5>
                                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                          <span aria-hidden="true">&times;</span>
                                                      </button>
                                                  </div>
                                                  <div class="modal-body">
                                                      <form action="<?php echo base_url(); ?>agents/agents_email_send" method="post" enctype="multipart/form-data">
                                                          <div class="form-group">
                                                              <label for="recipient-name"
                                                                  class="col-form-label">Recipient:</label>
                                                                  <textarea style="height: 120px;" required class="form-control" name="recipientList" id="recipientList"></textarea>
                                                              <!-- <input type="text" class="form-control" name="recipientList" id="recipientList"> -->
                                                          </div>
                                                          <div class="form-group">
                                                              <label for="header-name"
                                                                  class="col-form-label">Header:</label>
                                                              <input type="text" class="form-control" required name="header" id="Header">
                                                          </div>
                                                          <div class="form-group">
                                                              <label for="message-text" class="col-form-label">Message:</label>
                                                              <textarea class="form-control" required name="message" id="message-text"></textarea>
                                                          </div>
                                                          <button type="submit" class="btn btn-primary d-block mt-3">Send message</button>
                                                      </form>
                                                  </div>
                                                  <div class="modal-footer">
                                                      <button type="button" class="btn btn-secondary"
                                                          data-dismiss="modal">Close</button>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
