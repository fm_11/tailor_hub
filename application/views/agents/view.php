<div class="row">
    <div class="col-12">
        <div class="card mb-4">
            <div class="card-body">
                <h5 class="mb-4"><?php echo $title; ?></h5>

                <div class="form-row">


                        <div class="form-group col-md-6">
                            <label for="Companyname">Company Name</label>
                            <input type="text" class="form-control" id="company_name" disabled
                                   name="company_name" value="<?php echo $agents->company_name;  ?>">

                        </div>
                        <div class="form-group col-md-6">
                            <label for="Personalname">Person Name</label>
                            <input type="text" class="form-control" id="personal_name" disabled
                                   name="personal_name" value="<?php echo $agents->personal_name;  ?>">

                        </div>
                    </div>

                    <div class="form-row">

                        <div class="form-group col-md-4">
                            <label for="address">Address</label>
                            <input type="text" class="form-control" id="address" disabled
                                   name="address" value="<?php echo $agents->address;  ?>">

                        </div>
                        <div class="form-group col-md-4">
                            <label for="phone">Phone</label>
                            <input type="text" class="form-control" id="phone" disabled
                                   name="phone" value="<?php echo $agents->phone;  ?>">

                        </div>
                        <div class="form-group col-md-4">
                            <label for="email">Email</label>
                            <input type="text" class="form-control" id="email" disabled
                                   name="email" value="<?php echo $agents->email;  ?>">

                        </div>
                    </div>

                    <div class="form-row">

                        <div class="form-group col-md-4">
                            <label for="txtMobile">Admission officer (NE)</label>
                            <select disabled class="form-control select2-single" name="admission_officer" required>
                                <option value="">Select</option>
                                <?php

                                if (count($employees)) {
                                    foreach ($employees as $list) {
                                        ?>
                                        <option
                                        <?php if ($list['id'] == $agents->admission_officer) {
                                            echo 'selected';
                                        } ?>
                                                value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="txtMobile">Marketing Officer (NE)</label>
                            <select disabled class="form-control select2-single" name="marketing_officer" required>
                                <option value="">Select</option>
                                <?php

                                if (count($employees)) {
                                    foreach ($employees as $list) {
                                        ?>
                                        <option
                                        <?php if ($list['id'] == $agents->marketing_officer) {
                                            echo 'selected';
                                        } ?>

                                            value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="bankName">Bank Name</label>
                            <input type="text" class="form-control" id="bank_name" disabled
                                   name="bank_name" value="<?php echo $agents->bank_name;  ?>">

                        </div>
                    </div>

                    <div class="form-row">

                        <div class="form-group col-md-4">
                            <label for="accountNumber">Account Number</label>
                            <input type="number" class="form-control" id="account_number" disabled
                                   name="account_number" value="<?php echo $agents->account_number;  ?>">

                        </div>
                        <div class="form-group col-md-4">
                            <label for="accountName">Account Name</label>
                            <input type="text" class="form-control" id="account_name" disabled
                                   name="account_name" value="<?php echo $agents->account_name;  ?>">

                        </div>
                        <div class="form-group col-md-4">
                            <label for="branchName">Branch Name</label>
                            <input type="text" class="form-control" id="branch_name" disabled
                                   name="branch_name" value="<?php echo $agents->branch_name;  ?>">

                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputAddress"> Logo </label><br>
                            <a target="_blank"  href="<?php echo base_url(); ?>media/agent/<?php echo $agents->logo; ?>">
                              <button type="button" class="btn btn-secondary btn-xs mb-1">Download</button>
                            </a>
                        </div>

                    </div>

            </div>
        </div>
    </div>
</div>
