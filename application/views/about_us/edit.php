<div class="row">
            <div class="col-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <h5 class="mb-4"><?php echo $title; ?> <small style="color: red;">All Red marked are required</small></h5>

                            <form action="<?php echo base_url(); ?>about_us/edit" method="post" enctype="multipart/form-data">

                                  <div class="form-row">
                                     <div class="form-group col-md-8">
                                        <label for="inputEmail4">About Us Description<span style="color:red;">*</span></label><br>
                                         <textarea class="form-control" id="module_subject" name="about_us"><?php echo $about->about_us;  ?></textarea>
                                     </div>
                                     <input type="hidden" name="id"  class="smallInput wide" value="<?php echo $about->id;  ?>">
                                    </div>


                                <button type="submit" class="btn btn-primary d-block mt-3">Submit</button>
                    </form>
            </div>
</div>
</div>
</div>
