<script type="text/javascript">
    function msgStatusUpdate(id, status) {
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        }
        else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("status_section_" + id).innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>news/updateMsgStatusNewsStatus?id=" + id + '&&status=' + status, true);
        xmlhttp.send();
    }

</script>

<div class="col-12 mb-4">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title"><?php echo $title; ?></h5>

                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Title</th>
                                        <th scope="col">Description</th>
                                        <th scope="col">Date</th>
                                        <th scope="col">Image Path</th>
                                        <th scope="col">Status</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  <?php
                                  $i = 0;
                                  foreach ($news as $row):
                                      $i++;
                                      ?>
                                    <tr>
                                        <th scope="row"><?php echo $i; ?></th>
                                        <td><?php echo $row['title']; ?></td>
                                        <td><?php echo $row['description']; ?></td>
                                        <td><?php echo date('d/m/Y', strtotime($row['date'])); ?></td>
                                        <td><?php echo $row['image_path']; ?></td>
                                        <td id="status_section_<?php echo $row['id']; ?>">
                                            <?php
                                            if ($row['status'] == 1) {
                                                ?>
                                                <a class="deleteTag" title="Active" href="#"
                                                   onclick="msgStatusUpdate(<?php echo $row['id']; ?>,<?php echo $row['status']; ?>)">
                                                   <button type="button" class="btn btn-primary btn-xs mb-1">Active</button>
                                                 </a>
                                            <?php
                                            } else {
                                                ?>
                                                <a class="deleteTag" title="Inactive" href="#"
                                                   onclick="msgStatusUpdate(<?php echo $row['id']; ?>,<?php echo $row['status']; ?>)">
                                                   <button type="button" class="btn btn-primary btn-xs mb-1">Inactive</button>
                                                 </a>
                                            <?php
                                            }
                                            ?>
                                        </td>

                                        <td>

                                               <a href="<?php echo base_url(); ?>news/edit/<?php echo $row['id']; ?>"
                                                  title="Edit">
                                                  <button type="button" class="btn btn-primary btn-xs mb-1">
                                                    Edit
                                                  </button>
                                                </a>

                                               <a href="<?php echo base_url(); ?>news/delete/<?php echo $row['id']; ?>"
                                                  onclick="return deleteConfirm()" title="Delete">
                                                  <button type="button" class="btn btn-primary btn-xs mb-1">
                                                    Delete
                                                  </button>

                                                </a>

                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
