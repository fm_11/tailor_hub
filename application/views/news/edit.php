<div class="row">
            <div class="col-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <h5 class="mb-4"><?php echo $title; ?> <small style="color: red;">All Red marked are required</small></h5>

                            <form action="<?php echo base_url(); ?>news/edit" method="post" enctype="multipart/form-data" autocomplete="off">


                                    <div class="form-row">
                                      <div class="form-group col-md-6">
                                        <label for="inputEmail4">Title<span style="color:red;">*</span></label>
                                        <input type="text" class="form-control" id="title" required name="title" value="<?php echo $news->title;  ?>" placeholder="Title">
                                     </div>

                                     <div class="form-group col-md-6">
                                             <label for="inputEmail4">Date<span style="color:red;">*</span></label>
                                             <div class="input-group date">
                                                <input type="text" name="date" id="date" value="<?php echo  $news->date; ?>" required class="form-control">
                                                <span class="input-group-text input-group-append input-group-addon">
                                                    <i class="simple-icon-calendar"></i>
                                                </span>
                                            </div>
                                       </div>

                                    <div class="form-group col-md-6">
                                      <label for="Timezone">Photo</label><span style="color:red;">*</span>
                                      <br>
                                      <input type="hidden" name="old_image_path" value="<?php echo $news->image_path; ?>"/>
                                      <img src="<?php echo base_url(); ?>media/website/images/news/<?php echo $news->image_path; ?>" style=" border:1px solid #666666;" width="150" height="150">
                                      <br><br>
                                      <input type="file" id="inputImage" name="txtPhoto"
                                          accept=".jpg,.jpeg,.png,.gif,.bmp,.tiff">
                                   </div>
                                      </div>
                                      <div class="form-row">
                                         <div class="form-group col-md-8">
                                            <label for="inputEmail4">Description<span style="color:red;">*</span></label><br>
                                             <textarea class="form-control" id="module_subject" name="description"><?php echo $news->description;  ?></textarea>
                                         </div>
                                        </div>
                                     <input type="hidden" name="id"  class="smallInput wide" value="<?php echo $news->id;  ?>">




                                <button type="submit" class="btn btn-primary d-block mt-3">Submit</button>
                    </form>
            </div>
</div>
</div>
</div>
