<script type="text/javascript" src="<?php echo base_url(); ?>media/js/jquery-latest.min.js"></script>
<script>
    $(document).ready(function() {
        var str = location.href.toLowerCase();
        $(".sub-header li a").each(function() {
            if (str.indexOf(this.href.toLowerCase()) > -1) {
                $("a.current").removeClass("current");
                $(this).closest('a').addClass('current');
            }
        });
    })

</script>


<ul class="sub-header">
    <li><a href="<?php echo base_url(); ?>students/class_index"><span>Class</span></a></li>
    <li><a href="<?php echo base_url(); ?>students/student_section_index"><span>Section</span></a></li>
    <li><a href="<?php echo base_url(); ?>students/student_info_index"><span>Student Information</span></a></li>
</ul>

