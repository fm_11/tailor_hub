<table>
    <tr>
        <td>
            <h2>
                <?php $session_user = $this->session->userdata('user_info'); ?>
                <h2>
                    <?php
                    if ($session_user[0]->is_integrate_with_website == 1) {
                        ?>
                        <a class="button_grey_round" style="margin-bottom: 5px;"
                           href="<?php echo base_url(); ?>students/student_synchronize"><span>Student Synchronize</span></a>
                    <?php
                    }
                    ?>
                </h2>
            </h2>
        </td>
    </tr>
    <tr>
        <td>
            <form id="add" action="<?php echo base_url(); ?>students/student_info_index" method="post">
                <?php
                $roll = $this->session->userdata('roll');
                $class_id = $this->session->userdata('class_id');
                $section_id = $this->session->userdata('section_id');
                $group = $this->session->userdata('group');
                $gender = $this->session->userdata('gender');
                $student_status = $this->session->userdata('student_status');
                //echo $student_status; die;
                ?>
                <table>
                    <tr>
                        <td style="width: 17%;">
                            <input type="text" name="roll" placeholder="Roll" size="15" value="<?php if (isset($roll)) {
                                echo $roll;
                            } ?>" style="display: -moz-stack !important;" class="smallInput" id="roll">&nbsp;
                        </td>
                        <td>
                            <select name="class_id" class="smallInput" id="class_id">
                                <option value="">-- Class --</option>
                                <?php foreach ($class_list as $row) { ?>
                                    <option value="<?php echo $row['id']; ?>"<?php if ($row['id'] == $class_id) {
                                        echo 'selected';
                                    } ?>><?php echo $row['name']; ?></option>
                                <?php } ?>
                            </select> &nbsp;
                        </td>
                        <td>
                            <select name="section_id" class="smallInput" id="section_id">
                                <option value="">-- Section --</option>
                                <?php foreach ($section_list as $row) { ?>
                                    <option value="<?php echo $row['id']; ?>"<?php if ($row['id'] == $section_id) {
                                        echo 'selected';
                                    } ?>><?php echo $row['name']; ?></option>
                                <?php } ?>
                            </select> &nbsp;
                        </td>
                        <td>
                            <select name="group" class="smallInput">
                                <option value="">-- Group--</option>
                                <option value="N" <?php if ($group == 'N') {
                                    echo 'selected';
                                } ?>>N/A
                                </option>
                                <option value="S" <?php if ($group == 'S') {
                                    echo 'selected';
                                } ?>>Science
                                </option>
                                <option value="A" <?php if ($group == 'A') {
                                    echo 'selected';
                                } ?>>Arts
                                </option>
                                <option value="C" <?php if ($group == 'C') {
                                    echo 'selected';
                                } ?>>Commerce
                                </option>
                            </select> &nbsp;
                        </td>
                        <td>
                            <select name="gender" class="smallInput">
                                <option value="">-- Gender --</option>
                                <option value="M" <?php if (isset($gender)) {
                                    if ($gender == 'M') {
                                        echo 'selected';
                                    }
                                } ?>>Male
                                </option>
                                <option value="F" <?php if (isset($gender)) {
                                    if ($gender == 'F') {
                                        echo 'selected';
                                    }
                                } ?>>Female
                                </option>
                            </select> &nbsp;
                        </td>
                        <td>
                            <select name="student_status" class="smallInput">
                                <option value="">-- Status --</option>
                                <option value="1" <?php if (isset($student_status)) {
                                    if ($student_status == '1') {
                                        echo 'selected';
                                    }
                                } ?>>Active
                                </option>
                                <option value="0" <?php if (isset($student_status)) {
                                    if ($student_status == '0') {
                                        echo 'selected';
                                    }
                                } ?>>Inactive
                                </option>
                            </select> &nbsp;
                        </td>
                        <td>
                            <input type="submit" name="Search" value="Search"/>
                        </td>
                    </tr>
                </table>

            </form>
        </td>
    </tr>
</table>

<table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <thead>
    <tr>
        <th width="20" scope="col">SL</th>
        <th width="140" scope="col">Name</th>
        <th width="100" scope="col">Process Code</th>
        <th width="20" scope="col">Class</th>
        <th width="20" scope="col">Section</th>
        <th width="30" scope="col">Group</th>
        <th width="50" scope="col">Roll</th>
        <th width="50" scope="col">Status</th>
        <th width="100" scope="col">Action</th>

    </tr>
    </thead>
    <tbody>
    <?php
    $i = (int)$this->uri->segment(3);
    foreach ($student as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td width="34">
                <?php echo $i; ?>
            </td>
            <td><?php echo $row['name']; ?></td>
            <td><?php echo $row['process_code']; ?></td>
            <td><?php echo $row['class_name']; ?></td>
            <td><?php echo $row['section_name']; ?></td>
            <td><?php if ($row['group'] == 'S') {
                    echo "Science";
                } elseif ($row['group'] == 'A') {
                    echo "Arts";
                } elseif ($row['group'] == "C") {
                    echo 'Commerce';
                } else {
                    echo "N/A";
                } ?></td>
            <td><?php echo $row['roll_no']; ?></td>
            <td style="vertical-align:middle">
                <?php
                if ($row['status'] == 1) {
                    ?>
                    <a class="approve_icon" title="Approve" href="#"></a>
                <?php
                } else {
                    ?>
                    <a class="reject_icon" title="Reject" href="#"></a>
                <?php
                }
                ?>
            </td>
            <td style="vertical-align:middle">
                <a href="<?php echo base_url(); ?>students/student_info_view/<?php echo $row['id']; ?>"
                   title="View">View</a>&nbsp;
            </td>

        </tr>
    <?php endforeach; ?>
    </tbody>

    <tr class="footer">
        <td colspan="10" style="color: #000000;" align="right">
            <!--  PAGINATION START  -->
            <div class="pagination">
                <?php echo $this->pagination->create_links(); ?>
            </div>
            <!--  PAGINATION END  -->
        </td>
    </tr>
</table>

