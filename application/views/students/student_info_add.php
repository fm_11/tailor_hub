<script>
    function subject_by_class() {
        var class_id = document.getElementById("class_id").value;
        if (class_id != '') {
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    document.getElementById("subject_space").innerHTML = xmlhttp.responseText;
                    // alert(xmlhttp.responseText);
                }
            }
            xmlhttp.open("GET", "<?php echo base_url(); ?>students/ajax_subject_by_class?class_id=" + class_id, true);
            xmlhttp.send();
        } else {
            document.getElementById("subject_space").innerHTML = 'Please Select Class....';
        }


    }

    function check_optional_subject(id) {
        // if (document.getElementById("is_optional_" + id).checked == true) {
        document.getElementById("subject_id_" + id).checked = true
        // }
    }

</script>
<h2>
    <?php
    if ($action == 'edit') {
        ?>
        Note: Class and Subjects are not allowed to edit.
    <?php
    } else {
        ?>
        Please Input All Correct Information
    <?php
    }
    ?>
</h2>
<form id="add"
      action="<?php echo base_url(); ?>students/<?php if ($action == 'edit') { ?>student_info_edit<?php } else { ?>student_info_add<?php } ?>"
      method="post"
      enctype="multipart/form-data">
    <label>Name</label>
    <input type="text" <?php if (isset($student_info_by_id)) { ?> value="<?php echo $student_info_by_id[0]['name']; ?>"
    <?php } ?> class="smallInput wide" name="name" required="1"/>
    <label>Class</label>
    <select name="class_id" required="required" class="smallInput" onchange="subject_by_class()" <?php if(isset($student_info_by_id)){ ?>disabled="disabled"<?php } ?> id="class_id">
        <option value="">-- Please Select --</option>
        <?php foreach ($class_list as $row) { ?>
            <option value="<?php echo $row['id']; ?>" <?php if (isset($student_info_by_id)) {
                if ($row['id'] == $student_info_by_id[0]['class_id']) {
                    echo 'selected';
                }
            } ?>><?php echo $row['name']; ?></option>

        <?php } ?>
    </select>
    <label>Subject List</label>
    <?php
    if ($action == 'edit') {
        ?>
        <table width="40%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
            <tr>
                <td>Sl</td>
                <td>Subject</td>
                <td>Is Optional</td>
            </tr>
            <?php for ($l = 0; $l < count($subject_list_by_student); $l++) { ?>
                <tr>
                    <td>
                        <?php echo $l + 1; ?>
                    </td>
                    <td>
                        <?php echo $subject_list_by_student[$l]['name'].' ('. $subject_list_by_student[$l]['code'] .')'; ?>
                    </td>
                    <td>
                        <?php if ($subject_list_by_student[$l]['is_optional'] == '1') {
                            echo "YES";
                        } ?>
                    </td>
                </tr>
            <?php } ?>
        </table>
        <br>
    <?php
    } else {
        ?>
        <span id="subject_space">Please Select Class.....</span>
    <?php } ?>
    <label>Section:</label>
    <select name="section_id" class="smallInput" required="required">
        <option value="">-- Please Select --</option>
        <?php foreach ($section_list as $row) { ?>
            <option value="<?php echo $row['id']; ?>" <?php if (isset($student_info_by_id)) {
                if ($row['id'] == $student_info_by_id[0]['section_id']) {
                    echo 'selected';
                }
            } ?>><?php echo $row['name']; ?></option>

        <?php } ?>
    </select>
    <label>Group:</label>
    <select name="group" class="smallInput" required="required">
        <option value="">-- Please Select --</option>
        <option value="N"<?php if (isset($student_info_by_id)) {
            if ($student_info_by_id[0]['group'] == 'N') {
                echo 'selected';
            }
        } ?>>N/A
        </option>
        <option value="S"<?php if (isset($student_info_by_id)) {
            if ($student_info_by_id[0]['group'] == 'S') {
                echo 'selected';
            }
        } ?>>Science
        </option>
        <option value="A"<?php if (isset($student_info_by_id)) {
            if ($student_info_by_id[0]['group'] == 'A') {
                echo 'selected';
            }
        } ?>>Arts
        </option>
        <option value="C"<?php if (isset($student_info_by_id)) {
            if ($student_info_by_id[0]['group'] == 'C') {
                echo 'selected';
            }
        } ?>>Commerce
        </option>
    </select>

    <label>Roll No:</label>
    <input <?php if (isset($student_info_by_id)) { ?> value="<?php echo $student_info_by_id[0]['roll_no']; ?>"
    <?php } ?> type="text" name="roll_no" class="smallInput wide" required="1">
    <label>Reg No:</label>
    <input <?php if (isset($student_info_by_id)) { ?> value="<?php echo $student_info_by_id[0]['reg_no']; ?>"
    <?php }else{ ?> value="0" <?php } ?> type="text" name="reg_no" class="smallInput wide" required="1">


    <label>Date of Birth</label>
    <input <?php if (isset($student_info_by_id)) { ?> value="<?php echo $student_info_by_id[0]['date_of_birth']; ?>"
    <?php }else{ ?> value="0000-00-00" <?php } ?>  type="text" name="date_of_birth" class="smallInput wide" id="date_of_birth" required="1">
    <label>Date of Admission</label>
    <input <?php if (isset($student_info_by_id)) { ?> value="<?php echo $student_info_by_id[0]['date_of_admission']; ?>"
    <?php }else{ ?> value="0000-00-00" <?php } ?>  type="text" name="date_of_admission" id="date_of_admission" class="smallInput wide" required="1">

    <label>Gender:</label>
    <select name="gender" class="smallInput" required="required">
        <option value="">-- Please Select --</option>
        <option value="M" <?php if (isset($student_info_by_id)) {
            if ($student_info_by_id[0]['gender'] == 'M') {
                echo 'selected';
            }
        } ?>>Male
        </option>
        <option value="F" <?php if (isset($student_info_by_id)) {
            if ($student_info_by_id[0]['gender'] == 'F') {
                echo 'selected';
            }
        } ?>>Female
        </option>
    </select>

    <label>Religion</label>
    <select name="txtReligion" class="smallInput" required>
        <option value="">-- Please Select --</option>
        <option value="I" <?php if (isset($student_info_by_id)) {
            if ($student_info_by_id[0]['religion'] == 'I') {
                echo 'selected';
            }
        } ?>>Islam
        </option>
        <option value="H" <?php if (isset($student_info_by_id)) {
            if ($student_info_by_id[0]['religion'] == 'H') {
                echo 'selected';
            }
        } ?>>Hindu
        </option>
    </select>

    <label>Blood Group</label>
    <select name="blood_group_id" class="smallInput" required="required">
        <option value="">-- Please Select --</option>
        <?php foreach ($blood_group_list as $row) { ?>
            <option value="<?php echo $row['id']; ?>" <?php if (isset($student_info_by_id)) {
                if ($row['id'] == $student_info_by_id[0]['blood_group_id']) {
                    echo 'selected';
                }
            } ?>><?php echo $row['name']; ?></option>

        <?php } ?>
    </select>

    <label>Father's Name</label>
    <input <?php if (isset($student_info_by_id)) { ?> value="<?php echo $student_info_by_id[0]['father_name']; ?>"
   <?php }else{ ?> value="N/A" <?php } ?>  type="text" name="father_name" class="smallInput wide">
    <label>Father's National ID</label>
    <input <?php if (isset($student_info_by_id)) { ?> value="<?php echo $student_info_by_id[0]['father_nid']; ?>"
     <?php }else{ ?> value="N/A" <?php } ?> type="text" name="father_nid" class="smallInput wide">

    <label>Mother's Name</label>
    <input <?php if (isset($student_info_by_id)) { ?> value="<?php echo $student_info_by_id[0]['mother_name']; ?>"
    <?php }else{ ?> value="N/A" <?php } ?> type="text" name="mother_name" class="smallInput wide">
    <label>Mother's National ID</label>
    <input <?php if (isset($student_info_by_id)) { ?> value="<?php echo $student_info_by_id[0]['mother_nid']; ?>"
     <?php }else{ ?> value="N/A" <?php } ?> type="text" name="mother_nid" class="smallInput wide">

    <label>Present Address</label>
    <textarea id="wysiwyg" class="smallInput wide" rows="7" cols="30"
              name="present_address"><?php if (isset($student_info_by_id)) {
            echo $student_info_by_id[0]['present_address'];
        } ?></textarea>

    <label>Permanent Address</label>
    <textarea id="wysiwyg" class="smallInput wide" rows="7" cols="30"
              name="permanent_address"><?php if (isset($student_info_by_id)) {
            echo $student_info_by_id[0]['permanent_address'];
        } ?></textarea>

    <label>Guardian Mobile</label>
    <input <?php if (isset($student_info_by_id)) { ?> value="<?php echo $student_info_by_id[0]['guardian_mobile']; ?>"
     <?php }else{ ?> value="N/A" <?php } ?> type="text" name="guardian_mobile" class="smallInput wide" required="1">

    <label>Email</label>
    <input <?php if (isset($student_info_by_id)) { ?> value="<?php echo $student_info_by_id[0]['email']; ?>"
     <?php }else{ ?> value="N/A" <?php } ?> type="text" name="email" class="smallInput wide">

    <label>Photo</label>
    <?php
    if ($action == 'edit') {
        ?>
        <input value="<?php echo $student_info_by_id[0]['id']; ?>" type="hidden" name="id" class="smallInput wide">
        <img src="<?php echo base_url(); ?>media/student/<?php echo $student_info_by_id[0]['photo']; ?>" height="200"
             width="200"><br>
        <input type="file" name="photo" class="smallInput"> * Image Format -> JPEG , JPG and PNG.
    <?php } else { ?>
        <input type="file" name="photo" required="1" class="smallInput"> * Image Format -> JPEG , JPG and PNG.
    <?php } ?>


    <br>
    <br>
    <?php
    if ($action == 'edit') {
        ?>
        <input type="submit" class="submit" value="Update">
    <?php } else { ?>
        <input type="submit" class="submit" value="Submit">
        <input type="reset" class="submit" value="Reset">
    <?php } ?>
</form><br/>

<div class="clear"></div><br/>

<script>
    $(function () {
        $("#date_of_birth").datepicker({
            dateFormat: "yy-mm-dd"
        });
        $("#date_of_admission").datepicker({
            dateFormat: "yy-mm-dd"
        });
    });
</script>

<link rel="stylesheet" href="<?php echo base_url(); ?>media/calender/all.css">
<script src="<?php echo base_url(); ?>media/calender/jquery.js"></script>
<script src="<?php echo base_url(); ?>media/calender/core.js"></script>
<script src="<?php echo base_url(); ?>media/calender/widget.js"></script>
<script src="<?php echo base_url(); ?>media/calender/datepicker.js"></script>

