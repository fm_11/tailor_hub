<?php $session_user = $this->session->userdata('user_info'); ?>
<h2>
    <?php
    if ($session_user[0]->is_integrate_with_website == 1) {
        ?>
        <a class="button_grey_round" style="margin-bottom: 5px;"
           href="<?php echo base_url(); ?>students/class_synchronize"><span>Class Synchronize</span></a>
    <?php
    }
    ?>
</h2>

<table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <thead>
    <tr>
        <th width="50" scope="col">SL</th>
        <th width="200" scope="col">Name</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = 0;
    foreach ($class as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td width="34">
                <?php echo $i; ?>
            </td>
            <td><?php echo $row['name']; ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

