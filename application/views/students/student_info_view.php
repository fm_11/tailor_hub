<form id="add" action="" method="post" enctype="multipart/form-data">
    <img src="<?php echo $web_address; ?>media/student/<?php echo $student_info_by_id[0]['photo']; ?>" height="200"
         width="200"><br>
    <label>Name</label>
    <input type="text" <?php if (isset($student_info_by_id)) { ?> value="<?php echo $student_info_by_id[0]['name']; ?>"
    <?php } ?> class="smallInput wide" name="name" required="1"/>


    <label>Process Code</label>
    <input
        type="text" <?php if (isset($student_info_by_id)) { ?> value="<?php echo $student_info_by_id[0]['process_code']; ?>"
    <?php } ?> class="smallInput wide" name="process_code" required="1"/>

    <label>Class</label>
    <select name="class_id" class="smallInput" required="required" onchange="subject_by_class()" id="class_id"
            disabled="disabled">
        <option value="">--Select--</option>
        <?php foreach ($class_list as $row) { ?>
            <option <?php if ($student_info_by_id[0]['class_id'] == $row['id']) { ?> selected="selected" <?php } ?>
                value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>

        <?php } ?>
    </select><br>

    <label>Section:</label>
    <select name="section_id" class="smallInput" required="required" disabled="disabled">
        <option value="">--Select--</option>
        <?php foreach ($section_list as $row) { ?>
            <option <?php if ($student_info_by_id[0]['section_id'] == $row['id']) { ?> selected="selected" <?php } ?>
                value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>

        <?php } ?>
    </select>
    <label>Group:</label><br>
    <select name="group" class="smallInput" required="required" disabled="disabled">
        <option value="">--Select--</option>
        <option <?php if ($student_info_by_id[0]['group'] == "N") { ?> selected="selected" <?php } ?> value="N">N/A
        </option>
        <option <?php if ($student_info_by_id[0]['group'] == "S") { ?> selected="selected" <?php } ?> value="S">
            Science
        </option>
        <option <?php if ($student_info_by_id[0]['group'] == "A") { ?> selected="selected" <?php } ?> value="A">Arts
        </option>
        <option <?php if ($student_info_by_id[0]['group'] == "C") { ?> selected="selected" <?php } ?> value="C">
            Commerce
        </option>
    </select><br>

    <label>Roll No:</label>
    <input <?php if (isset($student_info_by_id)) { ?> value="<?php echo $student_info_by_id[0]['roll_no']; ?>"
    <?php } ?> type="text" name="roll_no" class="smallInput wide" required="1" readonly>
    <label>Reg No:</label>
    <input <?php if (isset($student_info_by_id)) { ?> value="<?php echo $student_info_by_id[0]['reg_no']; ?>"
    <?php } ?> type="text" name="reg_no" class="smallInput wide" required="1" readonly>


    <label>Date of Birth</label>
    <input <?php if (isset($student_info_by_id)) { ?> value="<?php echo $student_info_by_id[0]['date_of_birth']; ?>"
    <?php } ?> type="text" name="date_of_birth" class="smallInput wide" required="1" readonly>

    <label>Gender:</label>
    <select name="gender" class="smallInput" required="required" disabled="disabled">
        <option value="">--Select--</option>
        <option <?php if ($student_info_by_id[0]['gender'] == "M") { ?> selected="selected" <?php } ?>  value="M">Male
        </option>
        <option <?php if ($student_info_by_id[0]['gender'] == "F") { ?> selected="selected" <?php } ?>  value="F">
            Female
        </option>
    </select><br>

    <label>Guardian Mobile</label>
    <input <?php if (isset($student_info_by_id)) { ?> value="<?php echo $student_info_by_id[0]['guardian_mobile']; ?>"
    <?php } ?> type="text" name="guardian_mobile" readonly class="smallInput wide" required="1">

</form>