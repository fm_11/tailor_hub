<style>
   .required_class{
     color: red;
   }
</style>
<div class="row">
            <div class="col-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <h5 class="mb-4"><?php echo $title; ?> <small style="color: red;">All Red marked are required</small></h5>

                            <form action="<?php echo base_url(); ?>tailor_customer_review/edit" method="post" enctype="multipart/form-data">
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                      <label for="inputEmail4">Name<span style="color:red;">*</span></label>
                                      <input type="text" class="form-control" id="name" required name="name" value="<?php echo $cr->name;  ?>" placeholder="Name">
                                        <input type="hidden" name="id"  class="smallInput wide" value="<?php echo $cr->id;  ?>">
                                   </div>
                                      <div class="form-group col-md-6">
                                        <label for="Timezone">Photo</label><span class="required_class">*</span>
                                        <br>
                                        <input type="hidden" name="old_image_path" value="<?php echo $cr->image_path; ?>"/>
                                        <img src="<?php echo base_url(); ?>media/customer_review/<?php echo $cr->image_path; ?>" style=" border:1px solid #666666;" width="150" height="150">
                                        <br><br>
                                        <input type="file" id="inputImage" name="txtPhoto"
                                            accept=".jpg,.jpeg,.png,.gif,.bmp,.tiff">
                                      </div>
                                  </div>
                                  <div class="form-row">
                                    <div class="form-group col-md-6">
                                      <label for="inputEmail4">Description<span style="color:red;">*</span></label>
                                      <textarea class="form-control" id="module_subject" name="description"><?php echo $cr->description; ?></textarea>
                                   </div>
                                  </div>

                                <button type="submit" class="btn btn-primary d-block mt-3">Submit</button>
                    </form>
            </div>
</div>
</div>
</div>
