



<div class="col-12 mb-4">
    <div class="card">
        <div class="card-body" style="overflow-x: auto;">
            <h5 class="card-title"><?php echo $title; ?></h5>
            <?php
             $code = $this->session->userdata('code');
              $lead_source_id = $this->session->userdata('lead_source_id');
              $country_id = $this->session->userdata('country_id');
              $name = $this->session->userdata('name');
             ?>
            <form class="form-inline" method="post" action="<?php echo base_url(); ?>reference_applicants/index">
              <div class="form-group col-md-2 mb-3">
               <label>Student ID</label>
                 <input type="text" class="form-control" value="<?php echo $code; ?>" name="code" id="code">
             </div>
             <div class="form-group col-md-3 mb-3">
              <label>Lead source</label>
              <select  class="form-control col-md-12" name="lead_source_id" >
                  <option value="">--Select--</option>
                  <?php
                  if (count($lead_source)) {
                      foreach ($lead_source as $list) {
                          ?>
                          <option
                              <?php if ($list['id'] == $lead_source_id) {
                              echo 'selected';
                          } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                          <?php
                      }
                  }
                  ?>
              </select>
            </div>
            <div class="form-group col-md-3 mb-3">
              <label for="contact">Country</label>
              <select  class="form-control col-md-12"  name="country_id" >
                  <option value="">--Select--</option>
                  <?php

                  if (count($countries)) {
                      foreach ($countries as $list) {
                          ?>
                          <option
                              <?php if ($list['id'] == $country_id) {
                              echo 'selected';
                          } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                          <?php
                      }
                  }
                  ?>
              </select>
            </div>
            <div class="form-group col-md-3 mb-3">

              <label>Name/Email/Contact No.</label>
              <input type="text" class="form-control" value="<?php echo $name; ?>" name="name" id="inlineFormInputName2">

            </div>
             <button type="submit" class="btn btn-sm btn-outline-primary mb-2">Search</button>
            </form>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Student ID</th>
                    <th scope="col">Student Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Mobile Number</th>
                    <th scope="col">Country</th>
                    <th scope="col">Registration Date</th>
                    <th scope="col">Lead Source</th>
                    <th scope="col">Admission Officer</th>

                </tr>
                </thead>
                <tbody>
                <?php
                $i = (int)$this->uri->segment(3);
                foreach ($applicants as $row):
                    $i++;
                    ?>
                    <tr>
                        <th scope="row"><?php echo $i; ?></th>
                        <td style="min-width: 90px;">
                          <a style="text-decoration:underline;"  href="<?php echo base_url(); ?>reference_applicants/view/<?php echo $row['id']; ?>">
                            <?php echo $row['applicant_code']; ?>
                          </a>
                        </td>
                        <td style="min-width: 150px;"><?php echo $row['first_name'] . ' ' .$row['last_name']; ?></td>
                        <td><?php echo $row['email']; ?></td>
                        <td><?php echo $row['country_code'].' '.$row['mobile']; ?></td>
                        <td><?php echo $row['country_id']; ?></td>
                        <td><?php echo $row['registration_date']; ?></td>
                        <td><?php echo $row['lead_source_id']; ?></td>
                        <td ><?php echo $row['admission_officer']; ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <?php echo $this->pagination->create_links(); ?>
        </div>
    </div>
</div>
