
<div class="row">
    <div class="col-12">
        <div class="card mb-4">
            <div class="card-body">
                <h5 class="mb-4"><?php echo $title; ?> <small style="color: red;">All Red marked are required</small></h5>
           <span style="float: right;color: red;margin-top: -43px;font-size: 16px;">File/Photo(3000 x 3000)px & Size(5MB) & Format Allow(jpg|png|jpeg|pdf|csv|doc|docx)</span>
                <form action="<?php echo base_url(); ?>reference_applicants/student_registration" method="post" enctype="multipart/form-data">
                  <div class="form-row">
                      <div class="form-group col-md-6">
                          <label for="studentFirstName">Given Name / First Name <span style="color:red;font-weight: bold;"> (As Passport)</span><span style="color:red;">*</span></label>
                          <input type="text" class="form-control" id="given_name"
                                 name="first_name" required>
                      </div>
                      <div class="form-group col-md-6">
                          <label for="studentLastName">Surename / Last Name</label>
                          <input type="text" class="form-control" id="last_name"
                                 name="last_name" >
                      </div>

                  </div>
                  <div class="form-row">
                      <div class="form-group col-md-6">
                          <label for="contact">Mobile Number <span style="color:red;">*</span></label>
                          <div class="row">
                            <div class="form-group col-md-4">
                              <select class="form-control select2-single"  name="country_code" required>
                                  <option value="">--Select--</option>
                                  <?php

                                  if (count($country_code)) {
                                      foreach ($country_code as $list) {
                                          ?>
                                          <option
                                              value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                          <?php
                                      }
                                  }
                                  ?>
                              </select>
                            </div>
                            <div class="form-group col-md-8" style="margin-left: -30px;">
                              <input type="text" class="form-control" id="mobile"
                                     name="mobile" required value="">
                            </div>
                          </div>

                      </div>
                      <div class="form-group col-md-6">
                          <label for="email">Email <span style="color:red;">*</span></label>
                          <input type="email" class="form-control" id="email"
                                 name="email" required value="">
                      </div>
                  </div>
                  <div class="form-row">
                      <div class="form-group col-md-4">
                          <label for="contact">Date Of Birth</label>
                          <div class='input-group date' id='collection_date'>
                              <input type='text' name="date_of_birth" class="form-control" />
                              <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                              </span>
                          </div>
                      </div>
                      <div class="form-group col-md-4">
                          <label for="email">Nationality <span style="color:red;">*</span></label>
                          <input type="text" class="form-control" id="nationality"
                                 name="nationality" required value="">
                      </div>
                      <div class="form-group col-md-4">
                        <label for="inputPassword4">Password <span style="color:red;">*</span></label>
                        <input type="password" class="form-control" id="password"
                               name="password"  placeholder="" required>
                      </div>
                  </div>
                  <div class="form-row">
                      <div class="form-group col-md-4">
                          <label for="contact">Passport Number <span style="color:red;">*</span></label>
                          <input type="text" class="form-control" id="passport_no"
                                 name="passport_no" required value="">
                      </div>
                      <div class="form-group col-md-4">
                          <label for="email">Issue Date</label>
                          <div class='input-group date' id='issue_date'>
                              <input type='text' name="issue_date" class="form-control" />
                              <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                              </span>
                          </div>
                      </div>

                      <div class="form-group col-md-4">
                          <label for="email">Expair Date</label>
                          <div class='input-group date' id='expair_date'>
                              <input type='text' name="expair_date" class="form-control" />
                              <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                              </span>
                          </div>
                      </div>
                  </div>
                  <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="email">Skype Id</label>
                        <input type="text" class="form-control" id="skype_id"
                               name="skype_id"  value="">
                    </div>


                      <div class="form-group col-md-4">
                          <label for="originOffice">Gender<span style="color:red;">*</span></label>
                          <select onchange="" class="form-control select2-single"  name="gender_id" required>
                              <option value="">--Select--</option>
                              <?php

                              if (count($genders)) {
                                  foreach ($genders as $list) {
                                      ?>
                                      <option
                                              value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                      <?php
                                  }
                              }
                              ?>
                          </select>
                      </div>


                      <div class="form-group col-md-4">
                          <label for="originOffice">Marital Status<span style="color:red;">*</span></label>
                          <select onchange="" class="form-control select2-single"  name="marital_status_id" required>
                              <option value="">--Select--</option>
                              <?php

                              if (count($marital_status)) {
                                  foreach ($marital_status as $list) {
                                      ?>
                                      <option
                                              value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                      <?php
                                  }
                              }
                              ?>
                          </select>
                      </div>

                  </div>

                  <div class="form-row">
                      <div class="form-group col-md-4">
                          <label for="contact">Lead Source<span style="color:red;">*</span></label>
                          <select onchange=""   class="form-control select2-single" name="lead_source_id">
                              <option value="">--Select--</option>
                              <?php

                              if (count($lead_source)) {
                                  foreach ($lead_source as $list) {
                                      ?>
                                      <option
                                              value="<?php echo $list['id']; ?>" ><?php echo $list['name']; ?></option>
                                      <?php
                                  }
                              }
                              ?>
                          </select>

                      </div>
                      <div class="form-group col-md-4">
                          <label for="contact">Origin Office<span style="color:red;">*</span></label>
                          <select  class="form-control select2-single" name="origin_office" required>
                              <option value="">--Select--</option>
                              <?php

                              if (count($origin_office)) {
                                  foreach ($origin_office as $list) {
                                      ?>
                                      <option
                                          value="<?php echo $list['id']; ?>" ><?php echo $list['name']; ?></option>
                                      <?php
                                  }
                              }
                              ?>
                          </select>

                      </div>

                      <div class="form-group col-md-4">
                          <label for="contact">Admission Officer<span style="color:red;">*</span></label>
                          <input type="text" class="form-control" id="admission_officer"
                                 name="admission_officer" required value="" readonly>
                      </div>
                  </div>
                  <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="is_valid_passport">Valid passport?</label>
                        <br>

                        <label class="radio-inline radio-right">
                            <input name="is_valid_passport" value="1" type="radio">Yes</label>
                        <label class="radio-inline radio-right">
                            <input name="is_valid_passport" checked="checked" value="0" type="radio">No</label>
                    </div>
                      <div class="form-group col-md-4">
                          <label for="originOffice">Month<span style="color:red;">*</span></label>
                          <select onchange="" class="form-control select2-single"  name="intake_month_id" required>
                              <option value="">--Select--</option>
                              <?php
                                if (count($intake_months)) {
                                    foreach ($intake_months as $list) {
                                        ?>
                                        <option
                                            value="<?php echo $list['id']; ?>" ><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                              ?>
                          </select>
                      </div>


                      <div class="form-group col-md-4">
                          <label for="originOffice">Year<span style="color:red;">*</span></label>
                          <select onchange="" class="form-control select2-single" name="intake_year_id" required>
                              <option value="">--Select--</option>
                              <?php
                                  if (count($intake_years)) {
                                      foreach ($intake_years as $list) {
                                          ?>
                                          <option
                                              value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                          <?php
                                      }
                                  }
                              ?>
                          </select>
                      </div>

                  </div>

                  <hr>
                  <h2>Address Details</h2>

                  <div class="form-row">
                      <div class="form-group col-md-3">
                          <label for="contact">Address</label>
                          <textarea name="cor_address" class="form-control"></textarea>
                      </div>
                      <div class="form-group col-md-3">
                          <label for="contact">City / Town <span style="color:red;">*</span></label>

                          <input type="text" class="form-control" id="cor_city"
                                 name="cor_city" required value="">
                      </div>

                      <div class="form-group col-md-3">
                          <label for="contact">Postal / Zip Code <span style="color:red;">*</span></label>
                          <input type="text" class="form-control" id="phone"
                                 name="zip_code" required value="">
                      </div>

                      <div class="form-group col-md-3">
                          <label for="contact">Country <span style="color:red;">*</span></label>
                          <select onchange="" class="form-control select2-single"  name="country_id" required>
                              <option value="">--Select--</option>
                              <?php

                              if (count($countries)) {
                                  foreach ($countries as $list) {
                                      ?>
                                      <option
                                              value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                      <?php
                                  }
                              }
                              ?>
                          </select>
                      </div>
                  </div>
                  <div class="form-row">
                      <div class="form-group col-md-6">
                          <label for="inputAddress">Upload Passport <span style="color:red;">*</span></label>
                          <input type="file" class="form-control"  id="passport_file" name="passport_file"  required>

                      </div>
                      <div class="form-group col-md-6">
                          <label for="inputAddress">Upload CV <span style="color:red;">*</span></label>
                          <input type="file" class="form-control"  id="cv_file" name="cv_file" required>

                      </div>

                  </div>
                  <hr>

                    <button type="submit" class="btn btn-primary d-block mt-3">Submit</button>

                </form>

                </div>

            </div>

        </div>
    </div>
</div>
