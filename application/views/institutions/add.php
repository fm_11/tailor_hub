<script type="text/javascript">
function getabout_media_type(media_type)
{
  if(media_type=='1')
  {
       document.getElementById('div_media_image').style.display = 'block';
       document.getElementById('div_media_video').style.display = 'none';
  }else{
    document.getElementById('div_media_video').style.display = 'block';
    document.getElementById('div_media_image').style.display = 'none';
  }


}
    function getCityByCountryId(city_id){
        //alert(city_id);
        if (window.XMLHttpRequest)
        {
            xmlhttp = new XMLHttpRequest();
        }
        else
        {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function()
        {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            {
                document.getElementById("city_id").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>institutions/getCityByCountryId?city_id=" + city_id, true);
        xmlhttp.send();
    }

</script>








<div class="row">
    <div class="col-12">
        <div class="card mb-4">
            <div class="card-body">
                <h5 class="mb-4"><?php echo $title; ?> <small style="color: red;">All Red marked are required</small></h5>
          
                <form action="<?php echo base_url(); ?>Institutions/add" method="post" enctype="multipart/form-data">
                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="inputEmail4">Country<span style="color:red;">*</span></label>
                            <select onchange="getCityByCountryId(this.value)" class="form-control select2-single" required name="country_id">
                                <option value="">--Select--</option>
                                <?php

                                if (count($countries)) {
                                    foreach ($countries as $list) {
                                        ?>
                                        <option
                                            value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="inputPassword4">City<span style="color:red;">*</span></label>
                            <select name="city_id" id="city_id" required class="form-control select2-single">
                                <option value="">Select</option>

                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Institution<span style="color:red;">*</span></label>
                            <input type="text" class="form-control" id="name"
                                   name="name" required value="">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="txtMobile">Establishment Year<span style="color:red;">*</span></label>
                            <input type="text" class="form-control" name="institution_estd_year" value="" required id="institution_estd_year">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="txtMobile">Institute Type<span style="color:red;">*</span></label>
                            <select class="form-control select2-single" name="institution_type_id" required>
                                <option value="">Select</option>
                                <?php

                                if (count($institution_types)) {
                                    foreach ($institution_types as $list) {
                                        ?>
                                        <option
                                                value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="txtMobile">Campus<span style="color:red;">*</span></label>
                            <input type="text" class="form-control" name="campus" value="" required id="campus">
                        </div>

                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputEmail4">Website<span style="color:red;">*</span></label>
                            <input type="text" class="form-control" id="website" required name="website" value="" placeholder="Name">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="inputPassword4">Financial Application fees</label>
                            <input type="text" class="form-control" id="application_fee"
                                   name="application_fee" value="">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="inputPassword4">Currency(Financial Application fees)</label>
                            <select class="form-control select2-single" name="financial_application_fees_currency">
                                <option value="">Select</option>
                                <?php

                                if (count($currencies)) {
                                    foreach ($currencies as $list) {
                                        ?>
                                        <option value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>

                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputEmail4">Institutional Benefits</label><br>
                            <textarea class="form-control" id="institutional_benefits" name="institutional_benefits"></textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputEmail4">Part Time Work Details (Mention hours permitted with estimated wages)</label><br>
                            <textarea class="form-control" id="part_time_work_details" name="part_time_work_details"></textarea>
                        </div>

                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <h3>Status</h3>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="is_active" id="exampleRadios1" value="1" checked>
                                <label class="form-check-label" for="exampleRadios1">
                                    Active
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="is_active" id="exampleRadios2" value="0">
                                <label class="form-check-label" for="exampleRadios2">
                                    Inactive
                                </label>
                            </div>
                        </div>
                    </div>
                    <h4>Key Contact Person</h4>
                    <hr>
                    <div class="form-row">
                      <div class="form-group col-md-4">
                          <label for="inputPassword4">Name</label>
                          <input type="text" class="form-control" id="international_contact_person"
                                 name="international_contact_person" value="">
                      </div>
                      <div class="form-group col-md-4">
                          <label for="inputPassword4">Email</label>
                          <input type="email" class="form-control" id="email"
                                 name="email" value="">
                      </div>
                      <div class="form-group col-md-4">
                          <label for="inputPassword4">Phone</label>
                          <input type="text" class="form-control" id="phone"
                                 name="phone" value="">
                      </div>
                    </div>
                    <h4>WEBSITE INFO.</h4>
                    <hr>

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="inputAddress">Upload Logo</label>
                            <input type="file" class="form-control"  id="institution_logo_path" name="institution_logo_path"
                                   accept=".jpg,.jpeg,.png,.gif,.bmp,.tiff">
                        </div>

                        <div class="form-group offset-2 col-md-4">
                            <label for="inputAddress">Upload Banner</label>
                            <input type="file" class="form-control"  id="banner_image" name="banner_image"
                                   accept=".jpg,.jpeg,.png,.gif,.bmp,.tiff">
                        </div>
                    </div>

                    <div class="form-row">

                        <div class="form-group col-md-4">
                            <label for="inputPassword4">Times Ranking</label>
                            <input type="text" class="form-control" id="times_ranking"
                                   name="times_ranking" value="">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputPassword4">International Ranking</label>
                            <input type="text" class="form-control" id="internationals"
                                   name="inter_ranking" value="">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputPassword4">International Student</label>
                            <input type="text" class="form-control" id="international"
                                   name="international" value="">
                        </div>
                    </div>

                    <h4>ABOUT DETAILS</h4>
                    <hr>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Media Type<span style="color:red;">*</span></label>
                            <select name="about_media_type" required class="form-control" onchange="getabout_media_type(this.value)">
                                <option value="">Select</option>
                                <option value="1">Image</option>
                                <option value="2">Video</option></select>

                            </select>
                        </div>


                          <div class="form-group col-md-6" id="div_media_image" style="display:none">
                              <label for="inputAddress">Upload Image</label>
                              <input type="file" class="form-control"  id="about_media_image" name="about_media_image"
                                     accept=".jpg,.jpeg,.png,.gif,.bmp,.tiff">

                          </div>
                          <div class="form-group col-md-6" id="div_media_video" style="display:none;">
                                   <label for="inputAddress">Video Link</label>
                                   <input type="text" class="form-control"  id="about_media_vedio" name="about_media_vedio">
                          </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Quote</label>
                            <textarea class="form-control" id="about_quote" name="about_quote"></textarea>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Location address</label>
                            <textarea class="form-control" id="location_address" name="location_address"></textarea>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="inputPassword4">Yearly cost of living</label>
                            <input type="text" class="form-control" id="financial_living_cost"
                                   name="financial_living_cost" value="">

                        </div>
                        <div class="form-group col-md-3">
                            <label for="inputPassword4">Currency (Yearly cost of living)</label>
                            <select class="form-control select2-single" name="yearly_cost_currency">
                                <option value="">Select</option>
                                <?php

                                if (count($currencies)) {
                                    foreach ($currencies as $list) {
                                        ?>
                                        <option value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="inputPassword4">Average Undergraduate Tuition Fees</label>
                            <input type="text" class="form-control" id="financial_tution_udergrade"
                                   name="financial_tution_udergrade" value="">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="inputPassword4">Currency (Average Undergraduate Tuition Fees)</label>
                            <select class="form-control select2-single" name="average_undergraduate_tuition_currency">
                                <option value="">Select</option>
                                <?php

                                if (count($currencies)) {
                                    foreach ($currencies as $list) {
                                        ?>
                                        <option value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>





                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="inputPassword4">Campus Accommodation Cost</label>
                            <input type="text" class="form-control" id="financial_living_cost"
                                   name="financial_living_cost" value="">

                        </div>
                        <div class="form-group col-md-3">
                            <label for="inputPassword4">Currency (Campus Accommodation Cost)</label>
                            <select class="form-control select2-single" name="campus_accommodation_currency">
                                <option value="">Select</option>
                                <?php

                                if (count($currencies)) {
                                    foreach ($currencies as $list) {
                                        ?>
                                        <option value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="inputPassword4">Average Postgraduate Tuition Fees</label>
                            <input type="text" class="form-control" id="financial_tution_udergrade"
                                   name="financial_tution_udergrade" value="">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="inputPassword4">Currency (Average Postgraduate Tuition Fees)</label>

                            <select class="form-control select2-single" name="average_postgraduate">
                                <option value="">Select</option>
                                <?php

                                if (count($currencies)) {
                                    foreach ($currencies as $list) {
                                        ?>
                                        <option value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>



                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Location Map</label>
                            <textarea class="form-control" id="location_map" name="location_map"></textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Scholarship</label>
                            <textarea class="form-control" id="scholarship" name="scholarship"></textarea>
                        </div>
                    </div>
                    <h4> ADDITIONAL INFORMATION</h4>
                    <hr>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                          <label for="inputPassword4">Application Method</label>
                          <textarea class="form-control" id="university_foundation_campus" name="application_method"></textarea>
                            <!-- <label for="inputPassword4">University Foundation Campus</label>
                            <textarea class="form-control" id="university_foundation_campus" name="university_foundation_campus"></textarea> -->
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Pathway Foundation Campus</label>
                            <select class="form-control select2-single" name="pathway_foundation_campus_institution_id">
                                <option value="">Select</option>
                                <?php

                                if (count($institutions)) {
                                    foreach ($institutions as $list) {
                                        ?>
                                        <option value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>

                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Pre Sessional English</label>
                            <textarea class="form-control" id="pre_sessional_english" name="pre_sessional_english"></textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Valid Agreement with NE</label>
                            <table>
                              <tr>

                                <th style="width:40%;">Name</th>
                                <th style="width:40%;">Percentage</th>
                                <th style="width:20%;text-align:right;">Active</th>
                              </tr>
                              <?php   (int)$row=0; foreach ($valid_agreement as $list): ?>
                                <tr>
                                <td>
                                  <span class="from-control"><?php echo $list['name']; ?></span>
                                  <input type="hidden"
                                         name="valid_agreement[<?php echo $row;?>][valid_agreement_id]" value="<?php echo $list['id']; ?>">
                                </td>
                                <td>
                                  <input type="text" class="form-control"
                                         name="valid_agreement[<?php echo $row; ?>][percentage]" value="">
                                </td>
                                <td style="text-align:right;">
                                  <input type="checkbox"
                                         name="valid_agreement[<?php echo $row; ?>][active]"  value="1" >
                                </td>
                              </tr>


                             <?php $row++; ?>
                              <?php endforeach; ?>

                            </table>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">

                        </div>

                    </div>
                    <button type="submit" class="btn btn-primary d-block mt-3">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
