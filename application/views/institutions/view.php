<style>
.txt-span {
  display: block;
  width: 100%;

  font-size: 1rem;
  line-height: 1.5;
  color:#495057;
  background-color:#fff;
  background-clip: padding-box;

  border-radius: .25rem;
  -webkit-transition: border-color 0.15s ease-in-out,-webkit-box-shadow 0.15s ease-in-out;
  transition: border-color 0.15s ease-in-out,-webkit-box-shadow 0.15s ease-in-out;
  transition: border-color 0.15s ease-in-out,box-shadow 0.15s ease-in-out;
  transition: border-color 0.15s ease-in-out,box-shadow 0.15s ease-in-out,-webkit-box-shadow 0.15s ease-in-out;
}
</style>


<div class="row">
    <div class="col-12">
        <div class="card mb-4">
            <div class="card-body">
                <h5 class="mb-4"><?php echo $title; ?> <small style="color: red;">All Red marked are required</small></h5>
                   <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="inputEmail4">Country<span style="color:red;">*</span></label>
                            <select disabled
                                    class="form-control select2-single" required name="country_id">
                                <option value="">--Select--</option>
                                <?php

                                if (count($countries)) {
                                    foreach ($countries as $list) {
                                        ?>
                                        <option
                                           <?php if ($list['id'] == $institution->country_id) {
                                            echo 'selected';
                                        } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="inputPassword4">City<span style="color:red;">*</span></label>
                            <select disabled name="city_id" id="city_id" required class="form-control select2-single">
                                <option value="">Select</option>
                                <?php
                                if (count($country_cities)) {
                                    foreach ($country_cities as $list) {
                                        ?>
                                        <option
                                            <?php if ($list['id'] == $institution->city_id) {
                                            echo 'selected';
                                        } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>

                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Institution<span style="color:red;">*</span></label>
                            <input type="text" class="form-control" disabled required id="name"
                                   name="name" value="<?php echo $institution->name;  ?>">

                        </div>

                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="txtMobile">Establishment Year<span style="color:red;">*</span></label>
                            <input type="text" disabled class="form-control" name="institution_estd_year" value="<?php echo $institution->institution_estd_year;  ?>" required id="institution_estd_year">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="txtMobile">Institute Type<span style="color:red;">*</span></label>
                            <select disabled class="form-control select2-single" required name="institution_type_id" required>
                                <option value="">Select</option>
                                <?php

                                if (count($institution_types)) {
                                    foreach ($institution_types as $list) {
                                        ?>
                                        <option
                                            <?php if ($list['id'] == $institution->institution_type_id) {
                                            echo 'selected';
                                        } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="txtMobile">Campus<span style="color:red;">*</span></label>
                            <input disabled type="text" class="form-control" name="campus" value="<?php echo $institution->campus;  ?>" required id="campus">
                        </div>

                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputEmail4">Website<span style="color:red;">*</span></label>
                            <input disabled type="text" class="form-control" id="website" required name="website" value="<?php echo $institution->website;  ?>" placeholder="Name">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="inputPassword4">Financial Application fees</label>
                            <input disabled type="text" class="form-control" id="application_fee"
                                   name="application_fee" value="<?php echo $institution->application_fee;  ?>">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="inputPassword4">Currency(Financial Application fees)</label>
                            <select disabled class="form-control select2-single" name="financial_application_fees_currency">
                                <option value="">Select</option>
                                <?php

                                if (count($currencies)) {
                                    foreach ($currencies as $list) {
                                        ?>
                                        <option <?php if ($list['id'] == $institution->financial_application_fees_currency) {
                                            echo 'selected';
                                        } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>

                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputEmail4">Institutional Benefits</label><br>
                            <textarea disabled class="form-control" id="institutional_benefits" name="institutional_benefits">
                                <?php echo $institution->institutional_benefits;  ?>
                            </textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputEmail4">Part Time Work Details (Mention hours permitted with estimated wages)</label><br>
                            <textarea disabled class="form-control" id="part_time_work_details" name="part_time_work_details">
                               <?php echo $institution->part_time_work_details;  ?>
                            </textarea>
                        </div>

                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <h3>Status</h3>
                              <?php  if($institution->is_active){echo "Active";}else{ echo "Inactive";}?>

                        </div>
                    </div>
                    <h4>Key Contact Person</h4>
                    <hr>
                    <div class="form-row">
                      <div class="form-group col-md-4">
                          <label for="inputPassword4">Name</label>
                          <input disabled type="text" class="form-control" id="international_contact_person"
                                 name="international_contact_person" value="<?php echo $institution->international_contact_person;  ?>">
                      </div>
                      <div class="form-group col-md-4">
                          <label for="inputPassword4">Email</label>
                          <input  disabled type="email" class="form-control" id="email"
                                 name="email" value="<?php echo $institution->email;  ?>">
                      </div>
                      <div class="form-group col-md-4">
                          <label for="inputPassword4">Phone</label>
                          <input disabled type="text" class="form-control" id="phone"
                                 name="phone" value="<?php echo $institution->phone;  ?>">
                      </div>
                    </div>
                    <h4>WEBSITE INFO.</h4>
                    <hr>

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="inputAddress">Upload Logo</label>
                                <br>
                          <?php if(isset($institution->institution_logo_path) && $institution->institution_logo_path!='0'){?>
                            <a target="_blank" href="<?php echo base_url(); ?>media/institute/<?php echo $institution->institution_logo_path; ?>">
                              <button type="button" class="btn btn-secondary btn-xs mb-1">Download</button>
                            </a>
                          <?php }?>
                        </div>

                        <div class="form-group offset-2 col-md-4">
                            <label for="inputAddress">Upload Banner</label>
                            <br>
                            <?php if(isset($institution->banner_image) && $institution->banner_image!='0'){?>
                            <a target="_blank" href="<?php echo base_url(); ?>media/institute/<?php echo $institution->banner_image; ?>">
                              <button type="button" class="btn btn-secondary btn-xs mb-1">Download</button>
                            </a>
                          <?php }?>
                        </div>
                    </div>

                    <div class="form-row">

                        <div class="form-group col-md-4">
                            <label for="inputPassword4">Times Ranking</label>
                            <input type="text" disabled class="form-control" id="times_ranking"
                                   name="times_ranking" value="<?php echo $institution->times_ranking;?>">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputPassword4">International Ranking</label>
                            <input type="text" disabled class="form-control" id="internationals"
                                   name="internationals" value="<?php echo $institution->inter_ranking;?>">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputPassword4">International Student</label>
                            <input type="text" disabled class="form-control" id="international"
                                   name="international" value="<?php echo $institution->international;?>">
                        </div>
                    </div>

                    <h4>ABOUT DETAILS</h4>
                    <hr>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Media Type<span style="color:red;">*</span></label>
                            <select disabled name="about_media_type" required class="form-control" onchange="getabout_media_type(this.value)">
                                <option value="">Select</option>
                                <option <?php if ($institution->about_media_type == 1) {
                                    echo 'selected';
                                }  ?> value="1">Image</option>
                                <option <?php if ($institution->about_media_type == 2) {
                                    echo 'selected';
                                }  ?> value="2">Video</option>
                            </select>

                            </select>
                        </div>

                          <div class="form-group col-md-6" id="div_media_image" style="display:  <?php if ($institution->about_media_type == 1) {
                                    echo 'block';
                                } else {
                                    echo 'none';
                                }?>">
                              <label for="inputAddress">Upload Image</label>
                            </br>
                            <a target="_blank" href="<?php echo base_url(); ?>media/institute/<?php echo $institution->about_media_image; ?>">
                              <button type="button" class="btn btn-secondary btn-xs mb-1">Download</button>
                            </a>
                          </div>
                          <div class="form-group col-md-6" id="div_media_video" style="display:  <?php if ($institution->about_media_type == 2) {
                                    echo 'block';
                                } else {
                                    echo 'none';
                                }?>">
                                   <label for="inputAddress">Video Link</label>
                                   <input type="text" disabled class="form-control"  id="about_media_vedio" name="about_media_vedio" value="<?php echo $institution->about_media_vedio; ?>">
                          </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Quote</label>
                            <textarea disabled class="form-control" id="about_quote" name="about_quote">
                                <?php echo $institution->about_quote;  ?>
                            </textarea>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Location address</label>
                            <textarea  disabled class="form-control" id="location_address" name="location_address">
                                <?php echo $institution->location_address;  ?>
                            </textarea>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="inputPassword4">Yearly cost of living</label>
                            <input disabled type="text" class="form-control" id="financial_living_cost"
                                   name="financial_living_cost" value="<?php echo $institution->financial_living_cost;  ?>">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="inputPassword4">Currency (Yearly cost of living)</label>
                            <select disabled class="form-control select2-single" name="yearly_cost_currency">
                                <option value="">Select</option>
                                <?php

                                if (count($currencies)) {
                                    foreach ($currencies as $list) {
                                        ?>
                                        <option <?php if ($list['id'] == $institution->yearly_cost_currency) {
                                            echo 'selected';
                                        } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="inputPassword4">Average Undergraduate Tuition Fees</label>
                            <input type="text" disabled  class="form-control" id="financial_tution_udergrade"
                                   name="financial_tution_udergrade" value="<?php echo $institution->financial_tution_udergrade;  ?>">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="inputPassword4">Currency (Average Undergraduate Tuition Fees)</label>
                            <select disabled class="form-control select2-single" name="average_undergraduate_tuition_currency">
                                <option value="">Select</option>
                                <?php

                                if (count($currencies)) {
                                    foreach ($currencies as $list) {
                                        ?>
                                        <option <?php if ($list['id'] == $institution->average_undergraduate_tuition_currency) {
                                            echo 'selected';
                                        } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-row">

                        <div class="form-group col-md-3">
                            <label for="inputPassword4">Average Undergraduate Tuition Fees</label>
                            <input type="text" disabled class="form-control" id="financial_tution_udergrade"
                                   name="financial_tution_udergrade" value="<?php echo $institution->financial_tution_udergrade;  ?>">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="inputPassword4">Currency (Campus Accommodation Cost)</label>
                            <select disabled class="form-control select2-single" name="campus_accommodation_currency">
                                <option value="">Select</option>
                                <?php

                                if (count($currencies)) {
                                    foreach ($currencies as $list) {
                                        ?>
                                        <option <?php if ($list['id'] == $institution->campus_accommodation_currency) {
                                            echo 'selected';
                                        } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="inputPassword4">Average Postgraduate Tuition Fees</label>
                            <input type="text" disabled class="form-control" id="financial_tution_postgrad"
                                   name="financial_tution_postgrad" value="<?php echo $institution->financial_tution_postgrad;  ?>">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="inputPassword4">Currency (Average Postgraduate Tuition Fees)</label>

                            <select disabled class="form-control select2-single" name="average_postgraduate">
                                <option value="">Select</option>
                                <?php

                                if (count($currencies)) {
                                    foreach ($currencies as $list) {
                                        ?>
                                        <option <?php if ($list['id'] == $institution->average_postgraduate) {
                                            echo 'selected';
                                        } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Location Map</label>
                            <textarea disabled class="form-control" id="location_map" name="location_map">
                                <?php echo $institution->location_map;  ?>
                            </textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Scholarship</label>
                            <textarea disabled id="scholarship" name="scholarship">
                                <?php echo $institution->scholarship;  ?>
                            </textarea>
                        </div>
                    </div>
                    <h4> ADDITIONAL INFORMATION</h4>
                    <hr>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Application Method</label>
                            <textarea disabled id="university_foundation_campus" name="university_foundation_campus">
                                <?php echo $institution->application_method;  ?>
                            </textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Pathway Foundation Campus<span style="color:red;">*</span></label>
                            <select  disabled class="form-control select2-single" name="pathway_foundation_campus_institution_id" required>
                                <option  value="">Select</option>
                                <?php

                                if (count($institutions)) {
                                    foreach ($institutions as $list) {
                                        ?>
                                        <option
                                            <?php if ($list['id'] == $institution->pathway_foundation_campus_institution_id) {
                                            echo 'selected';
                                        } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?>
                                        </option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>

                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Pre Sessional English</label>
                            <textarea disabled class="form-control" id="pre_sessional_english" name="pre_sessional_english">
                                <?php echo $institution->pre_sessional_english;  ?>
                            </textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Valid Agreement with NE</label>
                            <table>
                              <tr>

                                <th style="width:40%;">Name</th>
                                <th style="width:40%;">Percentage</th>
                                <th style="width:20%;text-align:right;">Active</th>
                              </tr>
                              <?php   (int)$row=0; foreach ($valid_agreement as $list): ?>
                                <tr>
                                <td>
                                  <span class="from-control"><?php echo $list['name']; ?></span>
                                </td>
                                <td>
                                  <?php echo $list['percentage']; ?>
                                </td>
                                <td style="text-align:right;">
                                  <?php if ($list['active']) {
                                  echo  'Yes';
                                }else{ echo "No";}?>
                                </td>
                                </tr>


                             <?php $row++; ?>
                              <?php endforeach; ?>

                            </table>
                        </div>

                    </div>

                    
            </div>
        </div>
    </div>
</div>
