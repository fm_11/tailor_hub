<div class="col-12 mb-4">
    <div class="card">
        <div class="card-body">
            <h5 class="card-title"><?php echo $title; ?></h5>
            <?php
             $code = $this->session->userdata('code');
              $lead_source_id = $this->session->userdata('lead_source_id');
              $country_id = $this->session->userdata('country_id');
              $name = $this->session->userdata('name');
             ?>
            <form class="form-inline" method="post" action="<?php echo base_url(); ?>institutions/index">
              <div class="form-group col-md-4 mb-3">

                <label>Institutions/Campus</label>
                <input type="text" class="form-control col-md-12" value="<?php echo $name; ?>" name="name" id="inlineFormInputName2">

              </div>
            <div class="form-group col-md-4 mb-3">
              <label for="contact">Country</label>
              <select  class="form-control col-md-12"  name="country_id" >
                  <option value="">--Select--</option>
                  <?php

                  if (count($countries)) {
                      foreach ($countries as $list) {
                          ?>
                          <option
                              <?php if ($list['id'] == $country_id) {
                              echo 'selected';
                          } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                          <?php
                      }
                  }
                  ?>
              </select>
            </div>

             <button type="submit" class="btn btn-sm btn-outline-primary mb-2">Search</button>
            </form>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Institution</th>
                    <th scope="col">Country</th>
                    <th scope="col">Campus</th>
                    <th scope="col">Status</th>
                    <th scope="col">Add Date</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $i = (int)$this->uri->segment(3);
                foreach ($Institutions as $row):
                    $i++;
                    ?>
                    <tr>
                        <th scope="row"><?php echo $i; ?></th>
                        <td>
                          <a style="text-decoration:underline;"   href="<?php echo base_url(); ?>Institutions/view/<?php echo $row['id']; ?>">
                            <?php echo $row['name']; ?>
                          </a>
                        </td>
                        <td><?php echo $row['country_name']; ?></td>
                        <td><?php echo $row['campus']; ?></td>
                        <td>
                            <?php
                                if($row['is_active']==1){
                                    echo "Active";
                                }else{
                                    echo "Inactive";
                                }
                            ?>
                        </td>
                        <!--<td><?php echo $row['created_at']; ?></td>-->
                        <td><?php echo date("d M Y",strtotime($row['created_at'])); ?></td>

                        <td>
                            <a href="<?php echo base_url(); ?>Institutions/edit/<?php echo $row['id']; ?>"
                               title="Edit">
                                <button type="button" class="btn btn-primary btn-xs mb-1">
                                    Edit
                                </button>
                            </a>

                            <a href="<?php echo base_url(); ?>Institutions/delete/<?php echo $row['id']; ?>"
                               onclick="return deleteConfirm()" title="Delete">
                                <button type="button" class="btn btn-primary btn-xs mb-1">
                                    Delete
                                </button>

                            </a>

                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <?php echo $this->pagination->create_links(); ?>
        </div>
    </div>
</div>
