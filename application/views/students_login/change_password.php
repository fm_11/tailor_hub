<script>
function myFunction() {
    var pass1 = document.getElementById("new_pass").value;
    var pass2 = document.getElementById("con_new_pass").value;
    var ok = true;
    if (pass1 != pass2) {
        alert("Password Doesn't Match !");
        document.getElementById("new_pass").style.borderColor = "#E34234";
        document.getElementById("con_new_pass").style.borderColor = "#E34234";
        ok = false;
    }
    return ok;
}
</script>

<div class="row">
            <div class="col-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <h5 class="mb-4"><?php echo $title; ?> <small style="color: red;">All Red marked are required</small></h5>

                            <form action="<?php echo base_url(); ?>students_login/change_password"  onsubmit="return myFunction()" method="post" enctype="multipart/form-data">
                              <div class="form-group">
                                <label for="name">Current Password <span style="color:red;">*</span></label>
                                <input type="password" class="form-control" name="current_pass" id="current_pass" required value="" placeholder="">
                             </div>

                             <div class="form-group">
                               <label for="short_name">New Password <span style="color:red;">*</span></label>
                               <input type="password" class="form-control" required  name="new_pass" id="new_pass" value="" placeholder="">
                            </div>

                            <div class="form-group">
                              <label for="allocated_days">Confrim New Password <span style="color:red;">*</span></label>
                              <input type="password" class="form-control" required name="con_new_pass" id="con_new_pass" value="" placeholder="">
                           </div>
                              <input class="smallInput" type="hidden" required="1" name="user_id" id="user_id" value="<?php echo $user_id;?>">
                                <button type="submit" class="btn btn-primary d-block mt-3">Change</button>
                    </form>
            </div>
</div>
</div>
</div>
