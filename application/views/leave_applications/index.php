<script>

</script>

<div class="col-12 mb-4">
    <div class="card">
        <div class="card-body">
            <h5 class="card-title"><?php echo $title; ?></h5>

            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Employee ID</th>
                        <th scope="col">Leave Type</th>
                        <th scope="col">From Date</th>
                        <th scope="col">To Date</th>
                        <th scope="col">Leave Days</th>
                        <th scope="col">Status</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                  <?php
                    $i = (int)$this->uri->segment(3);
                    foreach ($movements as $row):
                      $i++;
                      ?>
                    <tr>
                        <th scope="row"><?php echo $i; ?></th>
                        <td><?php echo $row['name']; ?></td>
                        <td><?php echo $row['code']; ?></td>
                        <td><?php echo $row['leave_type']; ?></td>
                        <td><?php echo $row['from_date']; ?></td>
                        <td><?php echo $row['to_date']; ?></td>
                        <td>
                            <a href="#" id="leave_details_all_date_section_<?php echo $i ?>"  onmouseover="get_leave_details_all_date(<?php echo $i ?>,<?php echo $row['id']; ?>)" data-toggle="tooltip" data-placement="top" title=""
                             data-original-title="Please Wait">
                              <?php echo $row['num_of_leave_days']; ?>
                            </a>
                        </td>
                        <td>
                                      <?php
                                        if($row['status'] == "A"){
                                          echo 'Accepted';
                                        }
                                        else if($row['status'] == "R"){
                                          echo 'Rejected';
                                        }
                                        else if($row['status'] == "P"){
                                          echo 'Pending';
                                        }else{
                                          echo '-';
                                        }
                                     ?>
                       </td>
                        <td>
                             <?php if($row['status'] == "P"){ ?>
                               
                               <a href="<?php echo base_url(); ?>leave_applications/delete/<?php echo $row['id']; ?>"
                                  onclick="return deleteConfirm()" title="Delete">
                                  <button type="button" class="btn btn-primary btn-xs mb-1">
                                    Delete
                                  </button>
                                </a>
                              <?php }else{
                                  echo '-';
                              } ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <?php echo $this->pagination->create_links(); ?>
        </div>
    </div>
</div>
