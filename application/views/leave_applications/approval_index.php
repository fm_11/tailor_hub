<div class="col-12 mb-4">
    <div class="card">
        <div class="card-body">
            <h5 class="card-title"><?php echo $title; ?></h5>

            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Employee ID</th>
                        <th scope="col">Leave Type</th>
                        <th scope="col">From Date</th>
                        <th scope="col">To Date</th>
                        <th scope="col">Leave Days</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                  <?php
                    $i = (int)$this->uri->segment(3);
                    foreach ($leave_infos as $row):
                      $i++;
                      ?>
                    <tr>
                        <th scope="row"><?php echo $i; ?></th>
                        <td><?php echo $row['name']; ?></td>
                        <td><?php echo $row['code']; ?></td>
                        <td><?php echo $row['leave_type']; ?></td>
                        <td><?php echo $row['from_date']; ?></td>
                        <td><?php echo $row['to_date']; ?></td>
                        <td><?php echo $row['num_of_leave_days']; ?></td>
                        <td>
                            <a href="<?php echo base_url(); ?>leave_applications/application_approve_reject/<?php echo $row['id']; ?>/A"
                              onclick="return confirm('Are you sure to approve ?')" title="Approve">
                               <button type="button" class="btn btn-primary btn-xs mb-1">
                                 Approve
                               </button>
                             </a>

                            <a href="<?php echo base_url(); ?>leave_applications/application_approve_reject/<?php echo $row['id']; ?>/R"
                               onclick="return confirm('Are you sure to reject ?')" title="Reject">
                               <button type="button" class="btn btn-danger btn-xs mb-1">
                                 Reject
                               </button>
                             </a>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <?php echo $this->pagination->create_links(); ?>
        </div>
    </div>
</div>
