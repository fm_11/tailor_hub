<h5 class="card-title">Leave Details</h5>
<table class="table">
    <thead>
        <tr>
            <th scope="col">Leave Type</th>
            <th scope="col">Allocated</th>
            <th scope="col">Used</th>
            <th scope="col">Remaining</th>
        </tr>
    </thead>
    <tbody>
      <?php
        foreach ($leave_details as $row):
          ?>
        <tr>
            <th><?php echo $row['short_name'] ?></th>
            <td><?php echo $row['allocated_days'] ?></td>
            <td><?php echo $row['total_used_leave'] ?></td>
            <td><?php echo $row['allocated_days'] - $row['total_used_leave']; ?></td>
        </tr>
      <?php endforeach; ?>
    </tbody>
</table>
