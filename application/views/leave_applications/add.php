<div class="row">
            <div class="col-lg-8 col-md-12 mb-4">
                    <div class="card mb-4">
                        <div class="card-body">
                            <h5 class="mb-4"><?php echo $title; ?></h5>

                            <form action="<?php echo base_url(); ?>leave_applications/add" method="post" enctype="multipart/form-data">
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                      <label for="inputName">Employee Name</label>
                                      <select class="form-control select2-single" id="leave_employee_id" name="employee_id" required>
                                          <option value="">--Select--</option>
                                          <?php
                                          foreach ($employees as $row):
                                          ?>
                                             <option value="<?php echo $row['id']; ?>"><?php echo $row['name'].' (' . $row['code'] .')'; ?></option>
                                          <?php endforeach; ?>
                                      </select>
                                   </div>
                                  </div>


                                <div class="form-row">

                                  <div class="form-group col-md-4">
                                    <label for="LeaveType">Leave Type</label>
                                    <select class="form-control select2-single"  name="leave_type_id" id="leave_type_id" required>
                                        <option value="">--Select--</option>
                                        <?php
                                        foreach ($leave_types as $row):
                                        ?>
                                           <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                  </div>

                                      <div class="form-group col-md-4">
                                        <label for="FromDate">From Date</label>
                                        <div class="input-group date">
                                           <input type="text" name="from_date" autocomplete="off" required class="form-control">
                                           <span class="input-group-text input-group-append input-group-addon">
                                               <i class="simple-icon-calendar"></i>
                                           </span>
                                       </div>
                                     </div>

                                     <div class="form-group col-md-4">
                                       <label for="ToDate">To Date</label>
                                       <div class="input-group date">
                                          <input type="text" name="to_date" autocomplete="off" required class="form-control">
                                          <span class="input-group-text input-group-append input-group-addon">
                                              <i class="simple-icon-calendar"></i>
                                          </span>
                                      </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="Location">Reason</label>

                                      <textarea class="form-control" name="reason" required id="reason"></textarea>
                                </div>
                                <button type="submit" class="btn btn-primary d-block mt-3">Submit</button>
                    </form>
            </div>
</div>
</div>


<div class="col-lg-4 col-md-12 mb-4">
                    <div class="card">
                        <div class="card-body" id="leave_details_body">
                            <h5 class="card-title">Leave Details</h5>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">Leave Type</th>
                                        <th scope="col">Allocated</th>
                                        <th scope="col">Used</th>
                                        <th scope="col">Remaining</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan="4">Please  select employee</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>



</div>
