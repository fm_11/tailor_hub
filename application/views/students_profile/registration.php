<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title><?php echo $title; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="<?php echo base_url(); ?>media/admin_panel/font/iconsmind-s/css/iconsminds.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>media/admin_panel/font/simple-line-icons/css/simple-line-icons.css" />

    <link rel="stylesheet" href="<?php echo base_url(); ?>media/admin_panel/css/vendor/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>media/admin_panel/css/vendor/bootstrap.rtl.only.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>media/admin_panel/css/vendor/bootstrap-float-label.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>media/admin_panel/css/main.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>media/admin_panel/font/iconsmind-s/css/iconsminds.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>media/admin_panel/font/simple-line-icons/css/simple-line-icons.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>media/admin_panel/css/vendor/select2.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>media/admin_panel/css/vendor/bootstrap-datepicker3.min.css" />

</head>
<style>
.select2-selection
{
padding: 1.2rem .75rem !important;
}
</style>
<body class="background show-spinner">
    <div class="fixed-background"></div>
    <main>
        <div class="container" style="overflow: auto;">
            <div class="row">
                <div class="col-12 col-md-10 mx-auto my-auto">
                    <div class="card " style="padding: 30px !important;">
                      <?php
                          $message = $this->session->userdata('message');
                          if ($message != '') {
                              ?>
                              <div class="alert alert-success rounded" role="alert">
                                              <?php
                                              echo $message;
                              $this->session->unset_userdata('message'); ?>
                                </div>
                          <?php
                          }
                          ?>

                          <?php
                          $exception = $this->session->userdata('exception');
                          if ($exception != '') {
                              ?>
                               <div class="alert alert-danger rounded" role="alert">
                                              <?php
                                              echo $exception;
                              $this->session->unset_userdata('exception'); ?>
                                   </div>
                          <?php
                          }
                          ?>


                  <h2 class="text-center" ><?php echo $title; ?> <small style="color: red;">All Red marked are required</small></h2><br></br>


                  <form action="<?php echo base_url(); ?>students_profile/registration" method="post" enctype="multipart/form-data" autocomplete="off">

                      <div class="form-row">
                          <div class="form-group col-md-6">
                              <label for="studentFirstName">Given Name / First Name <span style="color:red;">*</span></label>
                              <input type="text" class="form-control" id="given_name"
                                     name="first_name" required value="">
                          </div>
                          <div class="form-group col-md-6">
                              <label for="studentLastName">Surename / Last Name</label>
                              <input type="text" class="form-control" id="last_name"
                                     name="last_name"  value="">
                          </div>

                      </div>

                      <div class="form-row">
                          <div class="form-group col-md-4">
                              <label for="contact">Date Of Birth<span style="color:red;">*</span></label>
                              <div class='input-group date' id='collection_date'>
                                  <input type='text' name="date_of_birth" class="form-control" required />
                                  <span class="input-group-addon">
                                      <span class="glyphicon glyphicon-calendar"></span>
                                  </span>
                              </div>
                          </div>
                          <div class="form-group col-md-4">
                              <label for="email">Nationality <span style="color:red;">*</span></label>
                              <input type="text" class="form-control" id="email"
                                     name="nationality" required value="">
                          </div>
                          <div class="form-group col-md-4">
                            <label for="inputPassword4">Password <span style="color:red;">*</span></label>
                            <input type="password" class="form-control" id="password"
                                   name="password"  placeholder="" required>
                          </div>
                      </div>

                      <div class="form-row">
                          <div class="form-group col-md-4">
                              <label for="contact">Mobile Number <span style="color:red;">*</span></label>
                              <div class="row">
                                <div class="form-group col-md-6">
                                  <select class="form-control select2-single"  name="country_code" required >
                                      <option value="">--Select--</option>
                                      <?php

                                      if (count($country_code)) {
                                          foreach ($country_code as $list) {
                                              ?>
                                              <option
                                                  value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                              <?php
                                          }
                                      }
                                      ?>
                                  </select>
                                </div>
                                <div class="form-group col-md-6" style="margin-left: -30px;">
                                  <input type="number" class="form-control" id="mobile"
                                         name="mobile" required value="">
                                </div>
                              </div>

                          </div>
                          <div class="form-group col-md-4">
                              <label for="email">Email <span style="color:red;">*</span></label>
                              <input type="text" class="form-control" id="email"
                                     name="email" required value="">
                          </div>

                          <div class="form-group col-md-4">
                              <label for="email">Skype Id</label>
                              <input type="text" class="form-control" id="skype_id"
                                     name="skype_id"  value="">
                          </div>

                      </div>

                      <div class="form-row">
                          <div class="form-group col-md-4">
                              <label for="contact">Passport Number <span style="color:red;">*</span></label>
                              <input type="text" class="form-control" id="passport_no"
                                     name="passport_no" required value="">
                          </div>
                          <div class="form-group col-md-4">
                              <label for="email">Issue Date</label>
                              <div class='input-group date' id='issue_date'>
                                  <input type='text' name="issue_date" class="form-control" />
                                  <span class="input-group-addon">
                                      <span class="glyphicon glyphicon-calendar"></span>
                                  </span>
                              </div>
                          </div>

                          <div class="form-group col-md-4">
                              <label for="email">Expair Date</label>
                              <div class='input-group date' id='expair_date'>
                                  <input type='text' name="expair_date" class="form-control" />
                                  <span class="input-group-addon">
                                      <span class="glyphicon glyphicon-calendar"></span>
                                  </span>
                              </div>
                          </div>
                      </div>
                      <div class="form-row">

                          <div class="form-group col-md-4">
                              <label for="is_valid_passport">Valid passport?</label>
                              <br>

                              <label class="radio-inline radio-right">
                                  <input name="is_valid_passport" value="1" type="radio">Yes</label>
                              <label class="radio-inline radio-right">
                                  <input name="is_valid_passport" checked="checked" value="0" type="radio">No</label>
                          </div>

                          <div class="form-group col-md-4">
                              <label for="originOffice">Gender<span style="color:red;">*</span></label>
                              <select onchange="" class="form-control select2-single"  name="gender_id" required>
                                  <option value="">--Select--</option>
                                  <?php

                                  if (count($genders)) {
                                      foreach ($genders as $list) {
                                          ?>
                                          <option
                                                  value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                          <?php
                                      }
                                  }
                                  ?>
                              </select>
                          </div>


                          <div class="form-group col-md-4">
                              <label for="originOffice">Marital Status<span style="color:red;">*</span></label>
                              <select onchange="" class="form-control select2-single"  name="marital_status_id" required>
                                  <option value="">--Select--</option>
                                  <?php

                                  if (count($marital_status)) {
                                      foreach ($marital_status as $list) {
                                          ?>
                                          <option
                                                  value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                          <?php
                                      }
                                  }
                                  ?>
                              </select>
                          </div>

                      </div>

                      <div class="form-row">
                          <div class="form-group col-md-4">
                              <label for="contact">Lead Source<span style="color:red;">*</span></label>
                              <select onchange=""  class="form-control select2-single" name="lead_source_id" required >
                                  <option value="">--Select--</option>
                                  <?php

                                  if (count($lead_source)) {
                                      foreach ($lead_source as $list) {
                                          ?>
                                          <option
                                                  value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                          <?php
                                      }
                                  }
                                  ?>
                              </select>

                          </div>
                          <div class="form-group col-md-4">
                              <label for="originOffice">Month<span style="color:red;">*</span></label>
                              <select onchange="" class="form-control select2-single"  name="intake_month_id" required>
                                  <option value="">--Select--</option>
                                  <?php
                                    if (count($intake_months)) {
                                        foreach ($intake_months as $list) {
                                            ?>
                                            <option
                                                value="<?php echo $list['id']; ?>" ><?php echo $list['name']; ?></option>
                                            <?php
                                        }
                                    }
                                  ?>
                              </select>
                          </div>


                          <div class="form-group col-md-4">
                              <label for="originOffice">Year<span style="color:red;">*</span></label>
                              <select onchange="" class="form-control select2-single" name="intake_year_id" required>
                                  <option value="">--Select--</option>
                                  <?php
                                      if (count($intake_years)) {
                                          foreach ($intake_years as $list) {
                                              ?>
                                              <option
                                                  value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                              <?php
                                          }
                                      }
                                  ?>
                              </select>
                          </div>

                      </div>


                      <hr>
                      <h2 class="text-center">Address Details</h2><br></br>
<span style="color: red;margin-top: -43px;font-size: 12px;">File/Photo(3000 x 3000)px & Size(5MB) & Format Allow(jpg|png|jpeg|pdf|csv|doc|docx)</span><br></br>
                      <div class="form-row">
                          <div class="form-group col-md-3">
                              <label for="contact">Address</label>
                              <textarea name="cor_address" class="form-control"></textarea>
                          </div>
                          <div class="form-group col-md-3">
                              <label for="contact">City / Town <span style="color:red;">*</span></label>

                              <input type="text" class="form-control" id="cor_city"
                                     name="cor_city" required value="">
                          </div>

                          <div class="form-group col-md-3">
                              <label for="contact">Postal / Zip Code <span style="color:red;">*</span></label>
                              <input type="text" class="form-control" id="phone"
                                     name="zip_code" required value="">
                          </div>

                          <div class="form-group col-md-3">
                              <label for="contact">Country <span style="color:red;">*</span></label>
                              <select onchange="" class="form-control select2-single"  name="country_id" required>
                                  <option value="">--Select--</option>
                                  <?php

                                  if (count($countries)) {
                                      foreach ($countries as $list) {
                                          ?>
                                          <option
                                                  value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                          <?php
                                      }
                                  }
                                  ?>
                              </select>
                          </div>
                      </div>
                      <div class="form-row">
                          <div class="form-group col-md-6">
                              <label for="inputAddress">Upload Passport <span style="color:red;">*</span></label>
                              <input type="file" class="form-control"  id="passport_file" name="passport_file" required>

                          </div>
                          <div class="form-group col-md-6">
                              <label for="inputAddress">Upload CV <span style="color:red;">*</span></label>
                              <input type="file" class="form-control"  id="cv_file" name="cv_file" required>

                          </div>

                      </div>
                      <hr>
                       <div>
                           <button type="submit" class="btn btn-primary d-block mt-3 text-center" style="margin: auto;">Submit</button>
                       </div>


                  </form>
                        <!-- </div> -->
                    </div>
                </div>
            </div>
        </div>
    </main>
    <script src="<?php echo base_url(); ?>media/admin_panel/js/vendor/jquery-3.3.1.min.js"></script>
    <script src="<?php echo base_url(); ?>media/admin_panel/js/vendor/bootstrap.bundle.min.js"></script>
    <script src="<?php echo base_url(); ?>media/admin_panel/js/dore.script.js"></script>
    <script src="<?php echo base_url(); ?>media/admin_panel/js/scripts.js"></script>
    <script src="<?php echo base_url(); ?>media/admin_panel/js/vendor/select2.full.js"></script>
    <script src="<?php echo base_url(); ?>media/admin_panel/js/vendor/bootstrap-datepicker.js"></script>


</body>

</html>
