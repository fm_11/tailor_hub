<div class="row">
    <div class="col-12">
        <div class="card mb-4">
            <div class="card-body">
                <h5 class="mb-4"><?php echo $title; ?></h5>

                <form action="<?php echo base_url(); ?>applicants/edit_language" method="post" enctype="multipart/form-data">
                    <input type="hidden" class="form-control" id="id"
                           name="id"  value="<?php echo $edit_language->id;  ?>">
                       <input type="hidden" class="form-control" id="applicant_id"
                              name="applicant_id"  value="<?php echo $edit_language->applicant_id;  ?>">
                   <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="contact">English Language Exam</label>
                            <input type="text" class="form-control" id="is_ielts"
                                   name="is_ielts" required value="<?php echo $edit_language->is_ielts;?>">
                        </div>

                        <div class="form-group col-md-6">
                          <label for="contact">Listening</label>
                          <input type="text" class="form-control" id="ielts_listening"
                                 name="ielts_listening" required value="<?php echo $edit_language->ielts_listening;?>">
                        </div>

                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                          <label for="contact">Writing</label>
                          <input type="text" class="form-control" id="ielts_writing"
                                 name="ielts_writing" required value="<?php echo $edit_language->ielts_writing;?>">
                        </div>
                        <div class="form-group col-md-6">
                          <label for="contact">Reading</label>
                          <input type="text" class="form-control" id="ielts_reading"
                                 name="ielts_reading" required value="<?php echo $edit_language->ielts_reading;?>">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                          <label for="contact">Speaking</label>
                          <input type="text" class="form-control" id="ielts_speaking"
                                 name="ielts_speaking" required value="<?php echo $edit_language->ielts_speaking;?>">
                        </div>
                        <div class="form-group col-md-6">
                          <label for="contact">Overall</label>
                          <input type="text" class="form-control" id="ielts_overall_score"
                                 name="ielts_overall_score" required value="<?php echo $edit_language->ielts_overall_score;?>">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                          <label for="contact">Exam date</label>
                          <div class='input-group date' id='exam_date'>
                              <input type='text' name="exam_date" value="<?php echo $edit_language->exam_date; ?>" class="form-control" />
                              <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                              </span>
                          </div>
                        </div>
                        <div class="form-group col-md-6">
                          <input type="hidden" class="form-control"   name="certificate_file_name" value="<?php echo $edit_language->certificate_file;?>"/>
                          <label for="inputAddress">Upload Transcript</label>
                          <input type="file" class="form-control"  id="certificate_file" name="certificate_file"
                                 accept=".jpg,.jpeg,.png,.gif,.bmp,.tiff">
                         <?php if(isset($edit_language->certificate_file) && $edit_language->certificate_file!='0'){?>
                           <a target="_blank" href="<?php  echo base_url(); ?>media/applicant_edu/<?php echo $edit_language->certificate_file; ?>">
                             <button type="button" class="btn btn-secondary btn-xs mb-1">Download</button>
                           </a>
                         <?php }?>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary d-block mt-3">Update</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
