<div class="row">
    <div class="col-12">
        <div class="card mb-4">
            <div class="card-body">
                <h5 class="mb-4"><?php echo $title; ?></h5>

                <form action="<?php echo base_url(); ?>applicants/edit_education" method="post" enctype="multipart/form-data">
                    <input type="hidden" class="form-control" id="id"
                           name="id"  value="<?php echo $edit_education->id;  ?>">
                       <input type="hidden" class="form-control" id="applicant_id"
                              name="applicant_id"  value="<?php echo $edit_education->applicant_id;  ?>">

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="studentFirstName">Country of education</label>
                            <select onchange="" class="form-control select2-single" required name="country_id" required>
                                <option value="">--Select--</option>
                                <?php

                                if (count($countries)) {
                                    foreach ($countries as $list) {
                                        ?>
                                        <option
                                        <option <?php if ($list['id'] == $edit_education->country_id) {
                                            echo 'selected';
                                        } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="contact">Level of education</label>
                            <input type="text" class="form-control" id="qualification_1"
                                   name="qualification_1" required value="<?php echo $edit_education->qualification_1;  ?>">
                        </div>

                        <div class="form-group col-md-4">
                            <label for="contact">Result</label>
                            <input type="text" class="form-control" id="grade_1"
                                   name="grade_1" required value="<?php echo $edit_education->grade_1;  ?>">
                        </div>

                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="contact">Name of Institution</label>
                            <input type="text" class="form-control" id="institute_1"
                                   name="institute_1" required value="<?php echo $edit_education->institute_1; ?>">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="contact">Passing Year</label>
                            <input type="text" class="form-control" id="year_of_passing_1"
                                   name="year_of_passing_1" required value="<?php echo $edit_education->year_of_passing_1; ?>">
                        </div>


                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="contact">Attended Institution From</label>
                            <div class='input-group date' id='issue_date'>
                                <input type='text'  value="<?php echo $edit_education->from_date; ?>" name="from_date" class="form-control" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="contact">Attended Institution To</label>
                            <div class='input-group date' id='issue_date'>
                                <input type='text' value="<?php echo $edit_education->to_date; ?>" name="to_date" class="form-control" />
                                <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                            </div>
                        </div>

                        <div class="form-group col-md-3">
                            <input type="hidden" name="certificate_img_name" value="<?php echo $edit_education->certificate_img_path; ?>" />
                            <label for="inputAddress">Upload Certificate</label>
                            <input type="file" class="form-control"  id="certificate_img" name="certificate_img"
                                   accept=".jpg,.jpeg,.png,.gif,.bmp,.tiff">
                          <?php if(isset($edit_education->certificate_img_path) && $edit_education->certificate_img_path!='0'){?>
                             <a target="_blank" href="<?php  echo base_url(); ?>media/applicant_edu/<?php echo $edit_education->certificate_img_path; ?>">
                               <button type="button" class="btn btn-secondary btn-xs mb-1">Download</button>
                             </a>
                           <?php }?>
                        </div>

                        <div class="form-group col-md-3">
                          <input type="hidden" name="transcript_img_name" value="<?php echo $edit_education->transcript_img_path; ?>" />
                            <label for="inputAddress">Upload Transcript</label>
                            <input type="file" class="form-control"  id="transcript_img" name="transcript_img"
                                   accept=".jpg,.jpeg,.png,.gif,.bmp,.tiff">
                          <?php if(isset($edit_education->transcript_img_path) && $edit_education->transcript_img_path!='0'){?>
                           <a target="_blank" href="<?php  echo base_url(); ?>media/applicant_edu/<?php echo $edit_education->transcript_img_path; ?>">
                             <button type="button" class="btn btn-secondary btn-xs mb-1">Download</button>
                           </a>
                         <?php }?>
                        </div>

                    </div>
                    <button type="submit" class="btn btn-primary d-block mt-3">Update</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
