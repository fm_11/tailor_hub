
<script>
function actionForTillNow() {
  var x = document.getElementById("to_date_div");
   if (x.style.display === "none") {
     x.style.display = "block";
   } else {
     x.style.display = "none";
     document.getElementById("to_date_ex").value = "";
   }
}
</script>

<div class="row">
    <div class="col-12">
        <div class="card mb-4">
            <div class="card-body">
                <h5 class="mb-4"><?php echo $title; ?></h5>

                <form action="<?php echo base_url(); ?>applicants/edit_work_experience" method="post" enctype="multipart/form-data">
                    <input type="hidden" class="form-control" id="id"
                           name="id"  value="<?php echo $edit_work_experience->id;  ?>">
                       <input type="hidden" class="form-control" id="applicant_id"
                              name="applicant_id"  value="<?php echo $edit_work_experience->applicant_id;  ?>">

                    <div class="form-row">
                        <div class="form-group col-md-6">
                          <label for="contact">Name of Employer</label>
                          <input type="text" class="form-control" id="employer_name_1"
                                 name="employer_name_1" required value="<?php echo $edit_work_experience->employer_name_1;?>">
                        </div>

                        <div class="form-group col-md-6">
                          <label for="contact">Position</label>
                          <input type="text" class="form-control" id="position_1"
                                 name="position_1" required value="<?php echo $edit_work_experience->position_1;?>">
                        </div>

                    </div>

                    <div class="form-row">
                      <div class="form-group col-md-6">
                          <label for="studentFirstName">Country</label>
                          <select onchange="" class="form-control select2-single" required name="country_id" required>
                              <option value="">--Select--</option>
                              <?php

                              if (count($countries)) {
                                  foreach ($countries as $list) {
                                      ?>
                                      <option
                                      <option <?php if ($list['id'] == $edit_work_experience->country_id) {
                                          echo 'selected';
                                      } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                      <?php
                                  }
                              }
                              ?>
                          </select>
                      </div>
                        <div class="form-group col-md-6">
                          <input type="hidden" name="experience_letter_name" value="<?php echo $edit_work_experience->experience_letter; ?>">
                          <label for="inputAddress">Upload Work Experience</label>
                          <input type="file" class="form-control"  id="experience_letter" name="experience_letter">
                        <?php if(isset($edit_work_experience->experience_letter) && $edit_work_experience->experience_letter!='0'){?>
                          <a target="_blank" href="<?php  echo base_url(); ?>media/applicant_edu/<?php echo $edit_work_experience->experience_letter; ?>">
                            <button type="button" class="btn btn-secondary btn-xs mb-1">Download</button>
                          </a>
                        <?php }?>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-4">
                          <label for="contact">From</label>
                          <div class='input-group date' id='from'>
                              <input type='text' name="from" class="form-control" value="<?php echo $edit_work_experience->from;?>" />
                              <span class="input-group-addon">
                                 <span class="glyphicon glyphicon-calendar"></span>
                              </span>
                          </div>
                        </div>
                        <div class="form-group col-md-4" id="to_date_div">
                          <label for="contact">To</label>
                          <div class='input-group date' id='to'>
                              <input type='text' id="to_date_ex" name="to" class="form-control" value="<?php echo $edit_work_experience->to;?>"/>
                              <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                              </span>
                          </div>
                        </div>

                        <div class="form-group col-md-4">
                          <label for="tilnow">Till Now</label>
                            <input type="checkbox" onclick="actionForTillNow()" name="is_till_now" value="1">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary d-block mt-3">Update</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
