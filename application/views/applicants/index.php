<script type="text/javascript">

    function send_lead_email(){
            var leadList = [];
            $.each($("input[name='is_allow_email']:checked"), function(){
                leadList.push($(this).val());
            });
            if(leadList.length == 0){
              alert("Please select atleast one agents.");
              return false;
            }else{
              var arrayLength = leadList.length;
              var returnString = '';
              for (var i = 0; i < arrayLength; i++) {
                  console.log(leadList[i]);
                  if(i == 0){
                    returnString = (returnString + $("#email_" + leadList[i]).val());
                  }else{
                    returnString = (returnString + ', ' + $("#email_" + leadList[i]).val());
                  }
              }
              $("#recipientList").val(returnString);
              return true;
            }
    };
</script>



<div class="col-12 mb-4">
    <div class="card">
        <div class="card-body" style="overflow-x: auto;">
            <h5 class="card-title"><?php echo $title; ?></h5>
            <?php
             $code = $this->session->userdata('code');
              $lead_source_id = $this->session->userdata('lead_source_id');
              $country_id = $this->session->userdata('country_id');
              $name = $this->session->userdata('name');
              $admission_officer_id = $this->session->userdata('admission_officer');
             ?>
            <form class="form-inline" method="post" action="<?php echo base_url(); ?>applicants/index">
              <div class="form-group col-md-4 mb-4">
               <label>Student ID</label>
                 <input type="text" class="form-control col-md-12" value="<?php echo $code; ?>" name="code" id="code">
             </div>
             <div class="form-group col-md-4 mb-4">
              <label>Lead source</label>
              <select  class="form-control col-md-12" name="lead_source_id" >
                  <option value="">--Select--</option>
                  <?php
                  if (count($lead_source)) {
                      foreach ($lead_source as $list) {
                          ?>
                          <option
                              <?php if ($list['id'] == $lead_source_id) {
                              echo 'selected';
                          } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                          <?php
                      }
                  }
                  ?>
              </select>
            </div>
            <div class="form-group col-md-4 mb-4">
              <label for="contact">Country</label>
              <select  class="form-control col-md-12"  name="country_id" >
                  <option value="">--Select--</option>
                  <?php

                  if (count($countries)) {
                      foreach ($countries as $list) {
                          ?>
                          <option
                              <?php if ($list['id'] == $country_id) {
                              echo 'selected';
                          } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                          <?php
                      }
                  }
                  ?>
              </select>
            </div>

            <div class="form-group col-md-4 mb-4">

              <label>Name/Email/Contact No.</label>
              <input type="text" class="form-control col-md-12" value="<?php echo $name; ?>" name="name" id="inlineFormInputName2">

            </div>
            <div class="form-group col-md-4 mb-4">
              <label for="contact">Admission Officer</label>
              <select  class="form-control col-md-12"  name="admission_officer" >
                  <option value="">--Select--</option>
                  <?php

                  if (count($admission_officer)) {
                      foreach ($admission_officer as $list) {
                          ?>
                          <option
                              <?php if ($list['id'] == $admission_officer_id) {
                              echo 'selected';
                          } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                          <?php
                      }
                  }
                  ?>
              </select>
            </div>
             <button type="submit" class="btn btn-sm btn-outline-primary mb-2">Search</button>
            </form>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Student ID</th>
                    <th scope="col">Student Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Mobile Number</th>
                    <th scope="col">Country</th>
                    <th scope="col">Registration Date</th>

                    <th scope="col">Lead Source</th>
                    <th scope="col">Agent</th>
                      <th scope="col">Admission Officer</th>
                      <th scope="col">
                        <div class="btn-group mb-1">
                                <button class="btn btn-secondary btn-xs dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Action
                                </button>
                                <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 25px, 0px); top: 0px; left: -60px; will-change: transform;">
                                    <span class="dropdown-item" href="javascript:void">Select All
                                      <input type="checkbox" class="checkAll">
                                    </span>
                                    <span class="dropdown-item">
                                      <button type="button" onclick="send_lead_email();" id="send_lead_email" class="btn btn-secondary btn-xs mb-1" data-toggle="modal"
                                          data-target="#SendMailApplicantModalContent" data-whatever="">
                                          Send Email
                                      </button>
                                    </span>
                                </div>
                        </div>
                      </th>
                </tr>
                </thead>
                <tbody>
                <?php
                $i = (int)$this->uri->segment(3);
                foreach ($applicants as $row):
                    $i++;
                    ?>
                    <tr>
                        <th scope="row"><?php echo $i; ?></th>
                        <td style="min-width: 90px;">
                          <a style="text-decoration:underline;"   href="<?php echo base_url(); ?>applicants/view/<?php echo $row['id']; ?>">
                            <?php echo $row['applicant_code']; ?>
                          </a>
                        </td>

                        <td style="min-width: 150px;"><?php echo $row['first_name'] . ' ' .$row['last_name']; ?></td>
                        <td><?php echo $row['email']; ?></td>
                        <td><?php echo $row['country_code'].' '.$row['mobile']; ?></td>
                        <td><?php echo $row['country_id']; ?></td>
                        <td><?php echo $row['registration_date']; ?></td>
                        <td><?php echo $row['lead_source_id']; ?></td>
                          <td style="min-width: 130px;"><?php echo $row['agent_name']; ?></td>
                        <td ><?php echo $row['admission_officer']; ?></td>
                        <td>
                        <input type="checkbox" value="<?php echo $i; ?>" class="checkbox"  name="is_allow_email">
                        <input type="hidden" id="email_<?php echo $i; ?>" value="<?php echo $row['email']; ?>"/>
                      </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <?php echo $this->pagination->create_links(); ?>
        </div>
    </div>
</div>

                    <div class="modal fade" id="SendMailApplicantModalContent" tabindex="-1" role="dialog"
                                aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalContentLabel">Send New message</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form action="<?php echo base_url(); ?>applicants/applicants_email_send" method="post" enctype="multipart/form-data">
                                                <div class="form-group">
                                                    <label for="recipient-name"
                                                        class="col-form-label">Recipient<span style="color:red;"> *  </span></label>
                                                        <textarea style="height: 120px;" required class="form-control" name="recipientList" id="recipientList"></textarea>
                                                    <!-- <input type="text" class="form-control" name="recipientList" id="recipientList"> -->
                                                </div>
                                                <div class="form-group">
                                                    <label for="header-name"
                                                        class="col-form-label">Header<span style="color:red;"> *  </span></label>
                                                    <input type="text" class="form-control" required name="header" id="Header">
                                                </div>
                                                <div class="form-group">
                                                    <label for="message-text" class="col-form-label">Message<span style="color:red;"> *  </span></label>
                                                    <textarea class="form-control" required name="message" id="message-text"></textarea>
                                                </div>
                                                <button type="submit" class="btn btn-primary d-block mt-3">Send message</button>
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary"
                                                data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
