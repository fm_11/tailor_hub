
<div class="row">
    <div class="col-12">
        <div class="card mb-4">
            <div class="card-body">
                <h5 class="mb-4"><?php echo $title; ?></h5>

                <form action="<?php echo base_url(); ?>applicants/edit_reference" method="post" enctype="multipart/form-data">
                    <input type="hidden" class="form-control" id="id"
                           name="id"  value="<?php echo $edit_reference->id;  ?>">
                       <input type="hidden" class="form-control" id="applicant_id"
                              name="applicant_id"  value="<?php echo $edit_reference->applicant_id;  ?>">

                    <div class="form-row">
                        <div class="form-group col-md-6">
                          <label for="contact">Name</label>
                          <input type="text" class="form-control" id="ref1_name"
                                 name="ref1_name" required value="<?php echo $edit_reference->ref1_name;?>">
                        </div>

                        <div class="form-group col-md-6">
                          <label for="contact">Designation</label>
                          <input type="text" class="form-control" id="ref1_designation"
                                 name="ref1_designation" required value="<?php echo $edit_reference->ref1_designation;?>">
                        </div>

                    </div>

                    <div class="form-row">
                      <div class="form-group col-md-6">
                          <label for="studentFirstName">Country</label>
                          <select onchange="" class="form-control select2-single" required name="ref1_country_id" required>
                              <option value="">--Select--</option>
                              <?php

                              if (count($countries)) {
                                  foreach ($countries as $list) {
                                      ?>
                                      <option
                                      <option <?php if ($list['id'] == $edit_reference->ref1_country_id) {
                                          echo 'selected';
                                      } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                      <?php
                                  }
                              }
                              ?>
                          </select>
                      </div>
                        <div class="form-group col-md-6">

                          <input type="hidden" name="reference_letter_name" value="<?php echo $edit_reference->reference_letter; ?>">
                          <label for="inputAddress">Upload Reference Letter</label>
                          <input type="file" class="form-control"  id="reference_image" name="reference_image">
                         <?php if(isset($edit_reference->reference_letter) && $edit_reference->reference_letter!='0'){?>
                          <a target="_blank" href="<?php  echo base_url(); ?>media/applicant_edu/<?php echo $edit_reference->reference_letter; ?>">
                            <button type="button" class="btn btn-secondary btn-xs mb-1">Download</button>
                          </a>
                        <?php }?>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                          <label for="contact">Email</label>
                          <input type="text" class="form-control" id="ref1_email"
                                 name="ref1_email" required value="<?php echo $edit_reference->ref1_email; ?>">
                        </div>
                        <div class="form-group col-md-6" id="to_date_div">
                          <label for="contact">Phone</label>
                          <div class="row">
                            <div class="form-group col-md-4">
                              <select class="form-control select2-single"  name="country_code" required>
                                  <option value="">--Select--</option>
                                  <?php

                                  if (count($country_code)) {
                                      foreach ($country_code as $list) {
                                        ?>
                                        <option
                                            <?php if ($list['id'] == $edit_reference->country_code) {
                                            echo 'selected';
                                        } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                      }
                                  }
                                  ?>
                              </select>
                            </div>
                            <div class="form-group col-md-8" style="margin-left: -30px;">
                              <input type="text" class="form-control" id="ref1_phone"
                                     name="ref1_phone" required value="<?php echo $edit_reference->ref1_phone; ?>">
                            </div>
                          </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                          <label for="contact">Address</label>
                          <textarea class="form-control" name="ref1_address"><?php echo $edit_reference->ref1_address;?></textarea>
                        </div>
                        <div class="form-group col-md-6">
                          <label for="contact">Fax</label>
                          <input type="text" class="form-control" id="ref1_fax"
                                 name="ref1_fax" value="<?php echo $edit_reference->ref1_fax; ?>">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                          <label for="contact">City</label>
                          <input type="text" class="form-control" id="ref1_city"
                                 name="ref1_city" value="<?php echo $edit_reference->ref1_city; ?>">
                        </div>
                        <div class="form-group col-md-6">
                          <label for="contact">State</label>
                          <input type="text" class="form-control" id="ref1_state"
                                 name="ref1_state" value="<?php echo $edit_reference->ref1_state; ?>">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary d-block mt-3">Update</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
