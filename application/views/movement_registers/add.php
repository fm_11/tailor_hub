<div class="row">
            <div class="col-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <h5 class="mb-4"><?php echo $title; ?></h5>

                            <form action="<?php echo base_url(); ?>movement_registers/add" method="post" enctype="multipart/form-data">
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                      <label for="inputName">Employee Name</label>
                                      <select class="form-control select2-single"  name="employee_id" required>
                                          <option value="">--Select--</option>
                                          <?php
                                          foreach ($employees as $row):
                                          ?>
                                             <option value="<?php echo $row['id']; ?>"><?php echo $row['name'].' (' . $row['code'] .')'; ?></option>
                                          <?php endforeach; ?>
                                      </select>
                                   </div>
                                      <div class="form-group col-md-6">
                                        <label for="Purpose">Purpose</label>
                                        <input type="text" class="form-control" required id="purpose" name="purpose"
                                            value="">
                                      </div>
                                  </div>


                                <div class="form-row">
                                      <div class="form-group col-md-4">
                                        <label for="Date">Date</label>
                                        <div class="input-group date">
                                           <input type="text" name="date" required class="form-control">
                                           <span class="input-group-text input-group-append input-group-addon">
                                               <i class="simple-icon-calendar"></i>
                                           </span>
                                       </div>
                                     </div>

                                      <div class="form-group col-md-4">
                                        <label for="startTime">Out Time</label>
                                        <input type="time" class="form-control" required id="start_time" name="start_time"
                                            value="">
                                      </div>

                                      <div class="form-group col-md-4">
                                        <label for="endTime">In Time</label>
                                        <input type="time" class="form-control" placeholder="Not Required" id="end_time" name="end_time"
                                            value="">
                                      </div>
                                </div>


                                <div class="form-group">
                                    <label for="Location">Location</label>
                                    <input type="text" class="form-control" required id="location" name="location"
                                        value="">
                                </div>

                                <div class="form-group">
                                    <label for="Remarks">Remarks</label>
                                    <input type="text" class="form-control" id="remarks" name="remarks"
                                        value="">
                                </div>

                                <button type="submit" class="btn btn-primary d-block mt-3">Submit</button>
                    </form>
            </div>
</div>
</div>
</div>
