<div class="col-12 mb-4">
    <div class="card">
        <div class="card-body">
            <h5 class="card-title"><?php echo $title; ?></h5>

            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Employee ID</th>
                        <th scope="col">Date</th>
                        <th scope="col">Start Time</th>
                        <th scope="col">End Time</th>
                        <th scope="col">Purpose</th>
                        <th scope="col">Location</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                  <?php
                    $i = (int)$this->uri->segment(3);
                    foreach ($movements as $row):
                      $i++;
                      ?>
                    <tr>
                        <th scope="row"><?php echo $i; ?></th>
                        <td><?php echo $row['name']; ?></td>
                        <td><?php echo $row['employee_code']; ?></td>
                        <td><?php echo $row['date']; ?></td>
                        <td>
                        <?php echo date('h:i:s A', strtotime($row['start_time'])); ?>
                        </td>
                        <td>
                              <?php
                              if($row['end_time'] != ''){
                                echo date('h:i:s A', strtotime($row['end_time']));
                              }else{
                                echo '-';
                              }
                               ?>
                        </td>
                        <td><?php echo $row['purpose']; ?></td>
                        <td><?php echo $row['location']; ?></td>
                        <td>

                               <a href="<?php echo base_url(); ?>movement_registers/edit/<?php echo $row['id']; ?>"
                                  title="Edit">
                                  <button type="button" class="btn btn-primary btn-xs mb-1">
                                    Edit
                                  </button>
                                </a>

                               <a href="<?php echo base_url(); ?>movement_registers/delete/<?php echo $row['id']; ?>"
                                  onclick="return deleteConfirm()" title="Delete">
                                  <button type="button" class="btn btn-primary btn-xs mb-1">
                                    Delete
                                  </button>
                                </a>

                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <?php echo $this->pagination->create_links(); ?>
        </div>
    </div>
</div>
