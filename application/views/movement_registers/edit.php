<div class="row">
            <div class="col-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <h5 class="mb-4"><?php echo $title; ?></h5>

                            <form action="<?php echo base_url(); ?>movement_registers/edit" method="post" enctype="multipart/form-data">


                                <div class="form-row">
                                      <div class="form-group col-md-4">
                                        <label for="startTime">Out Time</label>
                                        <input type="time" class="form-control" readonly required id="start_time" name="start_time"
                                            value="<?php echo $movement_info[0]['start_time']; ?>">
                                            <input type="hidden" class="form-control" required id="id" name="id"
                                                value="<?php echo $movement_info[0]['id']; ?>">
                                      </div>

                                      <div class="form-group col-md-4">
                                        <label for="endTime">In Time</label>
                                        <input type="time" class="form-control" placeholder="Not Required" id="end_time" name="end_time"
                                            value="<?php echo $movement_info[0]['end_time']; ?>">
                                      </div>
                                </div>

                                <button type="submit" class="btn btn-primary d-block mt-3">update</button>
                    </form>
            </div>
</div>
</div>
</div>
