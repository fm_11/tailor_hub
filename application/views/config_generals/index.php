
<!--<div id="dashboard_wrapper" class="submit_form">
	<div>
		<div style="float:left;width:100%;height:auto; margin-top: 10px;">

			<ul class="tabs">
				<li class="tab1"><?php echo anchor('config_generals/index','View')?></li>
				<li class="tab2"><?php echo anchor('config_generals/general_configuration','General Config')?></li>
				<li class="tab9"><?php echo anchor('config_generals/operational_policy','Operational Policy')?></li>

			</ul>
		</div>
		<div style="margin-top: 10px; float:left;width:100%;height:auto;">

			<ul class="tabs">
			</ul>

			<div class="tab_container">
				<div id="tab1" class="tab_content">

					<div style="width:99%;height:auto;float:left;padding:5px;border:solid 0px red;">
						<?php if(!isset ($load_view)){$load_view = 'config_generals/view';} ?>
						<?php $this->load->view($load_view); ?>
						<?php unset($load_vew);?>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>-->


<div class="col-12 mb-4">
	  <div class="card">
          <div class="card-body">
              <h5 class="mb-4"><?php echo $title; ?></h5>
              <ul class="nav">
                  <li class="nav-item">
                      <a class="nav-link active" href="<?php echo base_url(); ?>config_generals/index">View</a>
                  </li>
                  <li class="nav-item">
                      <a class="nav-link" href="<?php echo base_url(); ?>config_generals/general_configuration">General Config</a>
                  </li>
                  <li class="nav-item">
                      <a class="nav-link" href="<?php echo base_url(); ?>config_generals/operational_policy">Operational Policy</a>
                  </li>
              </ul>

							<?php if(!isset ($load_view)){$load_view = 'config_generals/view';} ?>
							<?php $this->load->view($load_view); ?>
							<?php unset($load_vew);?>

          </div>
		</div>
</div>
