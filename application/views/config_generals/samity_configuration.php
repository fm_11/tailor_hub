<style>
	label {
	    display: block;
	    float: left;
	    height: 20px;
	    width: 35%;
	    font-weight: bold;
	}
	ol li {
		min-height: 20px!important;	
	}	
</style>
<?php
    $options = array('0'=>'No','1'=>'Yes');
    echo form_open('config_generals/samity_configuration');
    echo form_hidden('txt_id',isset($row->id)?$row->id:"");
?>
<?php //print_r($row);die;?>
<fieldset>
	<table class="addForm" border="0" cellspacing="0px" cellpadding="0px" width="100%">
			<tr>
				<td width="100%" colspan=2>
					<div class="formContainer" style="border:none;width:98%">
						<ol style="border:none;width:98%"> 					
							<li>
								<label for="txt_max_member">Maximum Member per Samity<span class="required_field_indicator">*</span></label>
								<div class="form_input_container">
								<?php 
									echo form_input(array('name'=>'txt_max_member','class'=>'input_textbox','maxlength'=>'50'),set_value('txt_max_member',isset($row->max_member)?$row->max_member:""));
									echo form_error('txt_max_member'); 
								?>
								</div>
							</li>	
						</ol>
					</div>
				</td>
			</tr>
			<tr>
				<td class="formBottomBar">
					<div class="buttons" style="margin:0px 0px 0px 20px;">
						<?php echo form_submit(array('name'=>'submit','id'=>'submit','class'=>'submit_buttons positive'),'Save');?>
						<?php echo form_button(array('name'=>'button','id'=>'button','value'=>'true','type'=>'reset','content'=>'Reset','class'=>'reset_buttons'));?>
						<?php echo form_button(array('name'=>'button','id'=>'button','value'=>'true','type'=>'reset','content'=>'Cancel','class'=>'cancel_buttons','onclick'=>"window.location.href='".site_url('config_generals/view')."'"));?>
					</div>
				</td>
				<td class="formBottomBar"></td>
		</tr>
	</table>
</fieldset>
