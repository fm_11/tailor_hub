<style>
	label {
	    display: block;
	    float: left;
	    height: 20px;
	    width: 35%;
	    font-weight: bold;
	}
	ol li {
		min-height: 20px!important;	
	}	
</style>
<script type="text/javascript">
	$(document).ready(function(){	
		//var savings_balance_used = '<?php if(isset($row->savings_balance_used_for_interest_calculation) ){ echo $row->savings_balance_used_for_interest_calculation; }?>';
		var savings_balance_used =$("#cbo_savings_balance_used_for_interest_calculation").val();
		//alert(savings_balance_used);
		if(savings_balance_used=='MINIMUM_BALANCE'){
			$('#txt_savings_minimum_balance_required_for_interest_calculation').attr('readonly',"");
		}
		else{
			$('#txt_savings_minimum_balance_required_for_interest_calculation').attr('readonly',"readonly");
		}
		//alert(skt_required);
		$("#cbo_savings_balance_used_for_interest_calculation").change(function(){
			var savings_balance_used= $("#cbo_savings_balance_used_for_interest_calculation").val();
			//alert(savings_balance_used);
			if(savings_balance_used == 'MINIMUM_BALANCE') {				
				$("#txt_savings_minimum_balance_required_for_interest_calculation").attr('readonly',"");
			}
			else{
				$("#txt_savings_minimum_balance_required_for_interest_calculation").val('');
				$("#txt_savings_minimum_balance_required_for_interest_calculation").attr('readonly',"readonly");
			}
		});	


//check Amount field interger / numeric value
$("#txt_minimum_saving_balance_to_allow_withdraw").keydown(function(event) {
		// Allow only backspace and delete
		if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 190 || event.keyCode == 110 || event.keyCode == 37 || event.keyCode == 38 || event.keyCode == 39 || event.keyCode == 40 ) {
						// let it happen, don't do anything
		}
		else {
			// Ensure that it is a number and stop the keypress
			if (event.keyCode < 48 || event.keyCode < 96 && event.keyCode > 57 || event.keyCode > 105 ) {
							event.preventDefault();
			}
		}
});
$("#txt_maximum_withdraw_rate_without_loan").keydown(function(event) {
		if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 190 || event.keyCode == 110 || event.keyCode == 37 || event.keyCode == 38 || event.keyCode == 39 || event.keyCode == 40 ) {
		}
		else {
			if (event.keyCode < 48 || event.keyCode < 96 && event.keyCode > 57 || event.keyCode > 105 ) {
							event.preventDefault();
			}
		}
});
$("#txt_maximum_withdraw_rate_with_loan").keydown(function(event) {

		if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 190 || event.keyCode == 110 || event.keyCode == 37 || event.keyCode == 38 || event.keyCode == 39 || event.keyCode == 40 ) {

		}
		else {

			if (event.keyCode < 48 || event.keyCode < 96 && event.keyCode > 57 || event.keyCode > 105 ) {
							event.preventDefault();
			}
		}
});

var _alertColor="#EB8F8C"; //red
var _noAlertColor=""; //nocolor

	// Min and max check
	$("#txt_maximum_withdraw_rate_without_loan").change(
	  function(){
		var tval = $("#txt_maximum_withdraw_rate_without_loan").val();	
	   // alert(tval);

			 if (tval!='') 
			   val=parseFloat(tval);
			 else
			   val=0;
			var min=1;
			var max=100;
			var msg="";
			
			if(min!='' && max !=''){
			  msg='Input value should be in range of '+min + ' to ' + max + '.' ;
			}
			else{
			  if(min!=''){msg='Input value should be greter than or equal to'+min +'.';}
			  
			  else{
				if(max!=''){msg='Input value should be less than or equal to '+ max +'.';}
			  }
			}
			
			if(min!=''){
			  if (min>val) {
				alert(msg);
				$('#txt_maximum_withdraw_rate_without_loan').val('');
				$('#txt_maximum_withdraw_rate_without_loan').css('background',_alertColor);
				$('#txt_maximum_withdraw_rate_without_loan').focus();
			  }
			  else { $('#txt_maximum_withdraw_rate_without_loan').css('background',_noAlertColor); }
			}
			
			if (max!=''){
			  if (val>max) {
				alert(msg);
				$('#txt_maximum_withdraw_rate_without_loan').val('');
				$('#txt_maximum_withdraw_rate_without_loan').css('background',_alertColor);
				$('#txt_maximum_withdraw_rate_without_loan').focus();
			  }
			  else { $('#txt_maximum_withdraw_rate_without_loan').css('background',_noAlertColor); }
			}
		
		});	

	$("#txt_maximum_withdraw_rate_with_loan").change(
	  function(){
		var tval = $("#txt_maximum_withdraw_rate_with_loan").val();	
	   // alert(tval);

			 if (tval!='') 
			   val=parseFloat(tval);
			 else
			   val=0;
			var min=1;
			var max=100;
			var msg="";
			
			if(min!='' && max !=''){
			  msg='Input value should be in range of '+min + ' to ' + max + '.' ;
			}
			else{
			  if(min!=''){msg='Input value should be greter than or equal to'+min +'.';}
			  
			  else{
				if(max!=''){msg='Input value should be less than or equal to '+ max +'.';}
			  }
			}
			
			if(min!=''){
			  if (min>val) {
				alert(msg);
				$('#txt_maximum_withdraw_rate_with_loan').val('');
				$('#txt_maximum_withdraw_rate_with_loan').css('background',_alertColor);
				$('#txt_maximum_withdraw_rate_with_loan').focus();
			  }
			  else { $('#txt_maximum_withdraw_rate_with_loan').css('background',_noAlertColor); }
			}
			
			if (max!=''){
			  if (val>max) {
				alert(msg);
				$('#txt_maximum_withdraw_rate_with_loan').val('');
				$('#txt_maximum_withdraw_rate_with_loan').css('background',_alertColor);
				$('#txt_maximum_withdraw_rate_with_loan').focus();
			  }
			  else { $('#txt_maximum_withdraw_rate_with_loan').css('background',_noAlertColor); }
			}
		
		});	
			
	});
</script>
<?php
    echo form_open('config_generals/savings_configuration');
    echo form_hidden('txt_id',isset($row->id)?$row->id:"");
?>
<fieldset>
	<table class="addForm" border="0" cellspacing="0px" cellpadding="0px" width="100%">
		<tr>
			<td width="100%" colspan=2>
				<div class="formContainer" style="border:none;width:98%">
					<ol style="border:none;width:98%"> 			
						<li>
							<label for="cbo_financial_year_start_month">Financial Year Start Month<span class="required_field_indicator">*</span></label>
							<div class="form_input_container">
							<?php 
								
								echo form_dropdown('cbo_financial_year_start_month',$financial_year_start_month,set_value('cbo_financial_year_start_month',isset($row->financial_year_start_month)?$row->financial_year_start_month:"",'id="cbo_financial_year_start_month"'));
								echo form_error('cbo_financial_year_start_month'); 
							?>
							</div>
						</li>
						<li>
							<label for="txt_minimum_saving_balance_to_allow_withdraw">Minimum Saving Balance to Allow Withdraw<span class="required_field_indicator">*</span></label>
							<div class="form_input_container">
							<?php 
								$attr = array('name'=>'txt_minimum_saving_balance_to_allow_withdraw','id'=>'txt_minimum_saving_balance_to_allow_withdraw');
								echo form_input($attr,set_value('txt_minimum_saving_balance_to_allow_withdraw',isset($row->minimum_saving_balance_to_allow_withdraw)?$row->minimum_saving_balance_to_allow_withdraw:""));
								echo form_error('txt_minimum_saving_balance_to_allow_withdraw'); 
							?>
							</div>
						</li>						
						<li>
							<label for="txt_maximum_withdraw_rate_without_loan">Maximum Withdraw Rate Without Loan<span class="required_field_indicator">*</span></label>
							<div class="form_input_container">
							<?php 
								$attr = array('name'=>'txt_maximum_withdraw_rate_without_loan','id'=>'txt_maximum_withdraw_rate_without_loan');
								echo form_input($attr,set_value('txt_maximum_withdraw_rate_without_loan',isset($row->maximum_withdraw_rate_without_loan)?$row->maximum_withdraw_rate_without_loan:""),"class='input_small'");
								echo form_error('txt_maximum_withdraw_rate_without_loan'); 
							?><strong>&nbsp;%</strong>
							</div>
						</li>						
						<li>
							<label for="txt_maximum_withdraw_rate_with_loan">Maximum Withdraw Rate With Loan<span class="required_field_indicator">*</span></label>
							<div class="form_input_container">
							<?php 
								$attr = array('name'=>'txt_maximum_withdraw_rate_with_loan','id'=>'txt_maximum_withdraw_rate_with_loan');
								echo form_input($attr,set_value('txt_maximum_withdraw_rate_with_loan',isset($row->maximum_withdraw_rate_with_loan)?$row->maximum_withdraw_rate_with_loan:""),"class='input_small'");
								echo form_error('txt_maximum_withdraw_rate_with_loan'); 
							?><strong>&nbsp;%</strong>
							</div>
						</li>						
						
						<li>
							<label for="cbo_savings_balance_used_for_interest_calculation">Saving Balance used for Interest Calculation<span class="required_field_indicator">*</span></label>
							<div class="form_input_container">
							<?php 
								echo form_dropdown('cbo_savings_balance_used_for_interest_calculation',$savings_balance_used,set_value('cbo_savings_balance_used_for_interest_calculation',isset($row->savings_balance_used_for_interest_calculation)?$row->savings_balance_used_for_interest_calculation:""),'id="cbo_savings_balance_used_for_interest_calculation"');
								echo form_error('cbo_savings_balance_used_for_interest_calculation'); 
							?>
							</div>
						</li>						
						<li>
							<label for="txt_savings_minimum_balance_required_for_interest_calculation">Minimum Balance required for Interest Calculation</label>
							<div class="form_input_container">
							<?php 
								$attr = array('name'=>'txt_savings_minimum_balance_required_for_interest_calculation','id'=>'txt_savings_minimum_balance_required_for_interest_calculation');
								echo form_input($attr,set_value('txt_savings_minimum_balance_required_for_interest_calculation',isset($row->savings_minimum_balance_required_for_interest_calculation)?$row->savings_minimum_balance_required_for_interest_calculation:""));
								echo form_error('txt_savings_minimum_balance_required_for_interest_calculation'); 
							?>
							</div>
						</li>						
						<li>
							<label for="txt_savings_minimum_account_duration_to_receive_interest">Minimum Account Duration to receive interest<span class="required_field_indicator">*</span></label>
							<div class="form_input_container">
							<?php 
								
								echo form_dropdown('cbo_savings_minimum_account_duration_to_receive_interest',$frequency_in_months,set_value('cbo_savings_minimum_account_duration_to_receive_interest',isset($row->savings_minimum_account_duration_to_receive_interest)?$row->savings_minimum_account_duration_to_receive_interest:""));
								echo form_error('cbo_savings_minimum_account_duration_to_receive_interest'); 
							?>
							</div>
						</li>
						<li>
							<label for="cbo_savings_is_inactive_member_eligible_to_receive_interest">Is Inactive member eligible to receive interest?<span class="required_field_indicator">*</span></label>
							<div class="form_input_container">
							<?php 
								echo form_dropdown('cbo_savings_is_inactive_member_eligible_to_receive_interest',$options,set_value('cbo_savings_is_inactive_member_eligible_to_receive_interest',isset($row->savings_is_inactive_member_eligible_to_receive_interest)?$row->savings_is_inactive_member_eligible_to_receive_interest:"",'id="cbo_savings_is_inactive_member_eligible_to_receive_interest"'));
								echo form_error('cbo_savings_is_inactive_member_eligible_to_receive_interest'); 
							?>
							</div>
						</li>
						<li>
							<label for="cbo_savings_frequency_of_interest_posting_to_accounts">Frequency of Interest Posting to Accounts<span class="required_field_indicator">*</span></label>
							<div class="form_input_container">
							<?php 
							// remove 0 month from array
							unset($frequency_in_months[0]);
								echo form_dropdown('cbo_savings_frequency_of_interest_posting_to_accounts',$frequency_in_months,set_value('cbo_savings_frequency_of_interest_posting_to_accounts',isset($row->savings_frequency_of_interest_posting_to_accounts)?$row->savings_frequency_of_interest_posting_to_accounts:"",'id="cbo_savings_frequency_of_interest_posting_to_accounts"'));
								echo form_error('cbo_savings_frequency_of_interest_posting_to_accounts');
							?>
							</div>
						</li>
						
						
						<li>
							<label for="cbo_savings_interest_calculation_closing_month">Interest Calculation Closing Month<span class="required_field_indicator">*</span></label>
							<div class="form_input_container">
							<?php 
								echo form_dropdown('cbo_savings_interest_calculation_closing_month',$month_list,set_value('cbo_savings_interest_calculation_closing_month',isset($row->savings_interest_calculation_closing_month)?$row->savings_interest_calculation_closing_month:""));
								echo form_error('cbo_savings_interest_calculation_closing_month');
							?>
							</div>
						</li>
						<li>
							<label for="cbo_savings_interest_disbursment_month">Interest Disbursement Month<span class="required_field_indicator">*</span></label>
							<div class="form_input_container">
							<?php 
								echo form_dropdown('cbo_savings_interest_disbursment_month',$month_list,set_value('cbo_savings_interest_disbursment_month',isset($row->savings_interest_disbursment_month)?$row->savings_interest_disbursment_month:""));
								echo form_error('cbo_savings_interest_disbursment_month');
							?>
							</div>
						</li>	
                                                <li>
							<label for="cbo_savings_interest_calculation_closing_month_include">Is Savings Closing Month Include During Interest Calculation<span class="required_field_indicator">*</span></label>
							<div class="form_input_container">
							<?php 
								echo form_dropdown('cbo_savings_interest_calculation_closing_month_include',array('No','Yes'),set_value('cbo_savings_closing_month_include_during_interest_calculation',isset($row->savings_closing_month_include_during_interest_calculation)?$row->savings_closing_month_include_during_interest_calculation:""));
								echo form_error('cbo_savings_interest_calculation_closing_month_include');
							?>
							</div>
						</li>
						 <li>
							<label for="cbo_non_cash_savings_withdraw_need_or_not">IS NON-CASH SAVINGS INTEREST WITHDRAW ALLOWED?<span class="required_field_indicator">*</span></label>
							<div class="form_input_container">
							<?php 
								echo form_dropdown('cbo_non_cash_savings_withdraw_need_or_not',array('No','Yes'),set_value('cbo_non_cash_savings_withdraw_need_or_not',isset($row->non_cash_savings_withdraw_need_or_not)?$row->non_cash_savings_withdraw_need_or_not:""));
								echo form_error('cbo_non_cash_savings_withdraw_need_or_not');
							?>
							</div>
						</li>
						 <li>
							<label for="cbo_is_non_cash_transaction_applicable">IS NON-CASH Transaction Applicable(for death/inactive member)?<span class="required_field_indicator">*</span></label>
							<div class="form_input_container">
							<?php 
								echo form_dropdown('cbo_is_non_cash_transaction_applicable',array('No','Yes'),set_value('cbo_is_non_cash_transaction_applicable',isset($row->is_non_cash_transaction_applicable)?$row->is_non_cash_transaction_applicable:""));
								echo form_error('cbo_is_non_cash_transaction_applicable');
							?>
							</div>
						</li>
                                                <li>
							<label for="is_interest_auto_calculation_allowed_during_savings_closing">IS Interest Auto Calculation Allowed During Savings Closing ?<span class="required_field_indicator">*</span></label>
							<div class="form_input_container">
							<?php 
								echo form_dropdown('cbo_is_interest_auto_calculation_allowed_during_savings_closing',array('No','Yes'),set_value('cbo_is_interest_auto_calculation_allowed_during_savings_closing',isset($row->is_interest_auto_calculation_allowed_during_savings_closing)?$row->is_interest_auto_calculation_allowed_during_savings_closing:""));
								echo form_error('cbo_is_interest_auto_calculation_allowed_during_savings_closing');
							?>
							</div>
						</li>
                                                
					</ol>
				</div>
			</td>
		</tr>
		<tr>
			<td class="formBottomBar">
				<div class="buttons" style="margin:0px 0px 0px 20px;">
					<?php echo form_submit(array('name'=>'submit','id'=>'submit','class'=>'submit_buttons positive'),'Save');?>
					<?php echo form_button(array('name'=>'button','id'=>'button','value'=>'true','type'=>'reset','content'=>'Reset','class'=>'reset_buttons'));?>
					<?php echo form_button(array('name'=>'button','id'=>'button','value'=>'true','type'=>'reset','content'=>'Cancel','class'=>'cancel_buttons','onclick'=>"window.location.href='".site_url('config_generals/view')."'"));?>
				</div>
			</td>
			<td class="formBottomBar"></td>
		</tr>
	</table>
</fieldset>
