<style>
	label {
	    display: block;
	    float: left;
	    height: 20px;
	    width: 35%;
	    font-weight: bold;
	}
	ol li {
		min-height: 20px!important;	
	}
        input[type='radio']{
            width:40px;
        }
</style>
<?php
    echo form_open('config_generals/transfer_configuration');
    echo form_hidden('txt_id',isset($row->id)?$row->id:"");
?>
<?php //print_r($row);die;?>
<fieldset>
	<table class="addForm" border="0" cellspacing="0px" cellpadding="0px" width="100%">
			<tr>
				<td width="100%" colspan=2>
					<div class="formContainer" style="border:none;width:98%">
						<ol style="border:none;width:98%"> 					
							<li>
								<label for="rdo_loan_status">Is transfer allowed for open loan member?<span class="required_field_indicator">*</span></label>
								<div class="form_input_container">								
								<div style="float:left;"><?php 
									$yes = array(
										'name'        => 'rdo_transferred_member_loan_status',
										'id'          => 'rdo_transferred_member_loan_status',
										'value'       => '1',
										'style'       => 'margin:0px 10px',
										'checked'     => (isset($row->transferred_member_loan_status)&&($row->transferred_member_loan_status == 1))?'checked':''                            
									);
									echo form_radio($yes) . "YES";
								?></div>
								<div style="float:left;"><?php
									$no = array(
										'name'        => 'rdo_transferred_member_loan_status',
										'id'          => 'rdo_transferred_member_loan_status',
										'value'       => '0',
										'style'       => 'margin:0px 10px',
										'checked'     => (isset($row->transferred_member_loan_status)&&($row->transferred_member_loan_status == 0))?'checked':''
									);
									echo form_radio($no) . "NO" ;
								?></div>
								<?php echo form_error('rdo_transferred_member_loan_status'); ?> 
								</div>
							</li>
							<li>
								<label for="rdo_branch_transfer">Can member transfer from one branch to another branch?<span class="required_field_indicator">*</span></label>
								<div class="form_input_container">								
								<div style="float:left;"><?php 
									$yes = array(
										'name'        => 'rdo_is_member_branch_transfer_allowed',
										'id'          => 'rdo_is_member_branch_transfer_allowed',
										'value'       => '1',
										'style'       => 'margin:0px 10px',
										'checked'     => (isset($row->is_member_branch_transfer_allowed)&&($row->is_member_branch_transfer_allowed == 1))?'checked':''                            
									);
									echo form_radio($yes) . "YES";
								?></div>
								<div style="float:left;"><?php
									$no = array(
										'name'        => 'rdo_is_member_branch_transfer_allowed',
										'id'          => 'rdo_is_member_branch_transfer_allowed',
										'value'       => '0',
										'style'       => 'margin:0px 10px',
										'checked'     => (isset($row->is_member_branch_transfer_allowed)&&($row->is_member_branch_transfer_allowed == 0))?'checked':''
									);
									echo form_radio($no) . "NO" ;
								?></div>
								<?php echo form_error('rdo_is_member_branch_transfer_allowed'); ?> 
								</div>
							</li>							
						</ol>
					</div>
				</td>
			</tr>
			<tr>
				<td class="formBottomBar">
					<div class="buttons" style="margin:0px 0px 0px 20px;">
						<?php echo form_submit(array('name'=>'submit','id'=>'submit','class'=>'submit_buttons positive'),'Save');?>
						<?php echo form_button(array('name'=>'button','id'=>'button','value'=>'true','type'=>'reset','content'=>'Reset','class'=>'reset_buttons'));?>
						<?php echo form_button(array('name'=>'button','id'=>'button','value'=>'true','type'=>'reset','content'=>'Cancel','class'=>'cancel_buttons','onclick'=>"window.location.href='".site_url('config_generals/view')."'"));?>
					</div>
				</td>
				<td class="formBottomBar"></td>
		</tr>
	</table>
</fieldset>
