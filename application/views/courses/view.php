<style>
.txt-span {
  display: block;
  width: 100%;

  font-size: 1rem;
  line-height: 1.5;
  color:#495057;
  background-color:#fff;
  background-clip: padding-box;

  border-radius: .25rem;
  -webkit-transition: border-color 0.15s ease-in-out,-webkit-box-shadow 0.15s ease-in-out;
  transition: border-color 0.15s ease-in-out,-webkit-box-shadow 0.15s ease-in-out;
  transition: border-color 0.15s ease-in-out,box-shadow 0.15s ease-in-out;
  transition: border-color 0.15s ease-in-out,box-shadow 0.15s ease-in-out,-webkit-box-shadow 0.15s ease-in-out;
}
</style>
<div class="row">
    <div class="col-12">
        <div class="card mb-4">
            <div class="card-body">
                <h5 class="mb-4"><?php echo $title; ?></h5>

                  <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="inputPassword4">Course Prefix</label>
                           <span class="txt-span">
                             <?php echo $courses[0]['course_prefix']; ?> </span>

                        </div>

                        <div class="form-group col-md-3">
                            <label for="inputPassword4">Course Title</label>
                            <span class="txt-span"> <?php echo $courses[0]['course_title'];  ?></span>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Course Level</label>
                              <span class="txt-span"> <?php echo $courses[0]['course_level'];  ?></span>

                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Country</label>
                            <span class="txt-span"> <?php echo $courses[0]['country_name'];  ?></span>

                        </div>

                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Institution</label>
                            <span class="txt-span"> <?php echo $courses[0]['institution_name'];  ?></span>

                        </div>

                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-8">
                            <label for="inputPassword4">Campus Location</label>
                            <span class="txt-span"> <?php echo $courses[0]['campus_location'];  ?></span>

                        </div>

                        <div class="form-group col-md-4">
                            <label for="inputPassword4">IELTS Score</label>
                            <span class="txt-span"> <?php echo $courses[0]['languages'];  ?></span>

                        </div>


                    </div>

                    <div class="form-row">

                        <div class="form-group col-md-3">
                            <label for="inputPassword4">Tuition Fees International</label>
                            <span class="txt-span"> <?php echo $courses[0]['course_fee'];  ?></span>

                        </div>
                        <div class="form-group col-md-3">
                            <label for="inputPassword4">Currency (Tuition Fees International)</label>
                          <span class="txt-span"> <?php echo $courses[0]['tuition_fees_currency'];  ?></span>

                        </div>
                        <div class="form-group col-md-3">
                            <label for="inputPassword4">Tuition Fees Home</label>
                            <span class="txt-span"> <?php echo $courses[0]['course_fee_home'];  ?></span>

                        </div>
                        <div class="form-group col-md-3">
                            <label for="inputPassword4">Currency (Tuition Fees Home)</label>
                           <span class="txt-span"> <?php echo $courses[0]['tuition_fees_home_currency'];  ?></span>

                        </div>

                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-2">
                            <label for="inputPassword4">Placement Year? </label>
                             <span class="txt-span"> <?php  if($courses[0]['is_placement_year']){echo "Yes";}else{ echo "No";}  ?></span>

                        </div>

                        <div class="form-group col-md-4">
                            <label for="inputPassword4">Course Duration</label>
                             <span class="txt-span"> <?php echo $courses[0]['course_duration'];  ?></span>

                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Course Category / Discipline</label>
                             <span class="txt-span"> <?php echo $courses[0]['course_category'];  ?></span>
                        </div>

                    </div>

                    <div class="form-group col-md-6">
                        <label for="inputPassword4">Related Courses </label>
                            <?php

                            if (count($related_courses)) {
                                foreach ($related_courses as $list) {
                        $is_selected = "";
                        foreach ($related_courses_id_list as $related_courses_id_list_row) {
                          if($related_courses_id_list_row['related_course_id'] == $list['id']){
                            $is_selected = "selected";
                            break;
                          }
                        }?>
                                   <?php if($is_selected){echo '<br>'.$list['course_title']; } ?>

                                    <?php
                                }
                            }
                            ?>

                    </div>
                    <div class="form-group col-md-6">
                      <label for="inputEmail4">university Link</label>
                       <span class="txt-span"> <?php echo $courses[0]['university_link'];  ?></span>

                    </div>

                    </div>
                    <div class="form-row">

                        <div class="form-group col-md-6">
                           <label for="inputEmail4">Module Subject</label><br>
                             <textarea disabled class="form-control" id="program_description" name="program_description"><?php echo $courses[0]['module_subject'];  ?></textarea>


                        </div>

                        <div class="form-group col-md-6">
                            <label for="inputEmail4">Entry Requirement</label><br>

                            <textarea  disabled class="form-control" id="entry_requirement"><?php echo $courses[0]['entry_requirement'];  ?> </textarea>
                        </div>

                    </div>

                    <div class="form-row">
                      <div class="form-group col-md-6">
                          <label for="inputPassword4">Course Intake </label>
                          <table>
                            <tr>

                              <th style="width:40%;">Name</th>
                              <th style="width:40%;">Starting Date</th>
                              <th style="width:20%;text-align:right;">Active</th>
                            </tr>
                            <?php   (int)$row=0; foreach ($intake_months as $list): ?>
                              <tr>
                              <td>
                                <span class="from-control"><?php echo $list['name']; ?></span>
                                <input type="hidden"
                                       name="intakes_list[<?php echo $row;?>][intake_month_id]" value="<?php echo $list['id']; ?>">
                              </td>
                              <td>
                                  <span class="from-control"><?php echo $list['starting_date']; ?></span>

                              </td>
                              <td style="text-align:right;">
                                <span> <?php if($list['active']){echo  'Yes'; }else{ echo "No";}?></span>
                              </td>
                            </tr>


                           <?php $row++; ?>
                            <?php endforeach; ?>

                          </table>

                      </div>

                        <div class="form-group col-md-6">
                            <label for="inputEmail4">Description</label><br>
                            <textarea disabled class="form-control" ><?php echo $courses[0]['program_description'];  ?></textarea>
                        </div>

                    </div>



            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        $('#datetimepicker2').datetimepicker({
            locale: 'ru'
        });
    });
</script>
