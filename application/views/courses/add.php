<script type="text/javascript">
    function getInstituteByCountryId(country_id){
        //alert(city_id);
        if (window.XMLHttpRequest)
        {
            xmlhttp = new XMLHttpRequest();
        }
        else
        {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function()
        {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            {
                document.getElementById("institution_id").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>courses/getInstituteByCountryId?country_id=" + country_id, true);
        xmlhttp.send();
    }

  //
	// function checkCourseIntake(){
	// 	if($('#intakes_list').val() != null){
	// 		if($('#related_course_list').val() != null){
	// 		   return true;
	// 	    }else{
	// 			alert("Please select minimum 1 Related Course");
	// 	        return false;
	// 		}
	// 	}
	// 	alert("Please select minimum 1 Course Intake");
	// 	return false;
	// }

</script>








<div class="row">
    <div class="col-12">
        <div class="card mb-4">
            <div class="card-body">
                <h5 class="mb-4"><?php echo $title; ?> <small style="color: red;">All Red marked are required</small></h5>

                <form action="<?php echo base_url(); ?>Courses/add" onsubmit="return checkCourseIntake()" method="post" enctype="multipart/form-data">
                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="inputPassword4">Course Prefix<span style="color:red;">*</span></label>
                            <select class="form-control select2-single" required name="course_prefix_id" required>
                                <option value="">Select</option>
                                <?php

                                if (count($course_prefix)) {
                                    foreach ($course_prefix as $list) {

                                        ?>
                                        <option
                                            value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>

                        <div class="form-group col-md-3">
                            <label for="inputPassword4">Course Title<span style="color:red;">*</span></label>
                            <input type="text" required class="form-control" id="course_title"
                                   name="course_title" value="">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Course Level<span style="color:red;">*</span></label>
                            <select class="form-control select2-single" required name="course_level_id" required>
                                <option value="">Select</option>
                                <?php

                                if (count($course_level)) {
                                    foreach ($course_level as $list) {

                                        ?>
                                        <option
                                            value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Country<span style="color:red;">*</span></label>
                            <select onchange="getInstituteByCountryId(this.value)" class="form-control select2-single" required name="country_id" required>
                                <option value="">--Select--</option>
                                <?php

                                if (count($countries)) {
                                    foreach ($countries as $list) {

                                        ?>
                                        <option
                                            value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Institution<span style="color:red;">*</span></label>
                            <select name="institution_id" id="institution_id" required class="form-control select2-single">
                                <option value="">Select</option>

                            </select>
                        </div>

                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-8">
                            <label for="inputPassword4">Campus Location</label>
                            <input type="text" class="form-control" id="campus_location"
                                   name="campus_location" value="">
                        </div>

                        <div class="form-group col-md-4">
                            <label for="inputPassword4">IELTS Score</label>
                            <input type="text" class="form-control" id="languages"
                                   name="languages" value="">
                        </div>


                    </div>

                    <div class="form-row">

                        <div class="form-group col-md-3">
                            <label for="inputPassword4">Tuition Fees International</label>
                            <input type="text" class="form-control" id="course_fee"
                                   name="course_fee" value="">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="inputPassword4">Currency (Tuition Fees International)</label>

                            <select class="form-control select2-single" name="tuition_fees_currency">
                                <option value="">Select</option>
                                <?php

                                if (count($currencies)) {
                                    foreach ($currencies as $list) {

                                        ?>
                                        <option value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="inputPassword4">Tuition Fees Home</label>
                            <input type="text" class="form-control" id="course_fee_home"
                                   name="course_fee_home" value="">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="inputPassword4">Currency (Tuition Fees Home)</label>

                            <select class="form-control select2-single" name="tuition_fees_home_currency">
                                <option value="">Select</option>
                                <?php

                                if (count($currencies)) {
                                    foreach ($currencies as $list) {

                                        ?>
                                        <option value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>

                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-2">
                            <label for="inputPassword4">Placement Year? </label>
                            <div class="checkbox">
                                <input type="checkbox" class="form-control" name="is_placement_year" id="is_placement_year" value="1">
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="inputPassword4">Course Duration</label>
                            <input type="text" class="form-control" id="course_duration"
                                   name="course_duration" value="">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Course Category / Discipline<span style="color:red;">*</span></label>
                            <select class="form-control select2-single" required name="course_category_id" required>
                                <option value="">Select</option>
                                <?php

                                if (count($disciplines)) {
                                    foreach ($disciplines as $list) {

                                        ?>
                                        <option
                                            value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>

                    </div>

                    <div class="form-row">


                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Related Courses<span style="color:red;">*</span> </label>
						    <select class="form-control" id="related_course_list" multiple name="related_course_list[]" required>
                                <?php

                                if (count($related_courses)) {
                                    foreach ($related_courses as $list) {

                                        ?>
                                        <option
                                            value="<?php echo $list['id']; ?>"><?php echo $list['course_title']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    <div class="form-group col-md-6">
                      <label for="inputEmail4">Link</label><br>
                      <input type="text" class="form-control" id="university_link" name="university_link" />
                    </div>

                    </div>
                    <div class="form-row">

                        <div class="form-group col-md-6">
                           <label for="inputEmail4">Module Subject</label><br>
                            <textarea class="form-control" id="module_subject" name="module_subject"></textarea>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="inputEmail4">Entry Requirement</label><br>
                            <textarea class="form-control" id="entry_requirement" name="entry_requirement"></textarea>
                        </div>

                    </div>

                    <div class="form-row">
                      <div class="form-group col-md-6">
                          <label for="inputPassword4">Course Intake </label>
                          <table>
                            <tr>

                              <th style="width:40%;">Name</th>
                              <th style="width:40%;">Starting Date</th>
                              <th style="width:20%;text-align:right;">Active</th>
                            </tr>
                            <?php   (int)$row=0; foreach ($intake_months as $list): ?>
                              <tr>
                              <td>
                                <span class="from-control"><?php echo $list['name']; ?></span>
                                <input type="hidden"
                                       name="intakes_list[<?php echo $row;?>][intake_month_id]" value="<?php echo $list['id']; ?>">
                              </td>
                              <td>
                                <input type="text" class="form-control"
                                       name="intakes_list[<?php echo $row; ?>][starting_date]" value="">
                              </td>
                              <td style="text-align:right;">
                                <input type="checkbox"
                                       name="intakes_list[<?php echo $row; ?>][active]"  value="1" >
                              </td>
                            </tr>


                           <?php $row++; ?>
                            <?php endforeach; ?>

                          </table>

                      </div>
                        <div class="form-group col-md-6">
                            <label for="inputEmail4">Description</label><br>
                            <textarea class="form-control" id="program_description" name="program_description"></textarea>
                        </div>

                    </div>


                    <button type="submit" class="btn btn-primary d-block mt-3">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        $('#datetimepicker2').datetimepicker({
            locale: 'ru'
        });
    });
</script>
