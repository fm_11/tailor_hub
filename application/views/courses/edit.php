<script type="text/javascript">
    function getCityByCountryId(institution_id){
        //alert(city_id);
        if (window.XMLHttpRequest)
        {
            xmlhttp = new XMLHttpRequest();
        }
        else
        {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function()
        {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            {
                document.getElementById("institution_id").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>institutions/getCityByCountryId?institution_id=" + institution_id, true);
        xmlhttp.send();
    }

	// function checkCourseIntake(){
	// 	if($('#intakes_list').val() != null){
	// 		if($('#related_course_list').val() != null){
	// 		   return true;
	// 	    }else{
	// 			alert("Please select minimum 1 Related Course");
	// 	        return false;
	// 		}
	// 	}
	// 	alert("Please select minimum 1 Course Intake");
	// 	return false;
	// }


</script>








<div class="row">
    <div class="col-12">
        <div class="card mb-4">
            <div class="card-body">
                <h5 class="mb-4"><?php echo $title; ?> <small style="color: red;">All Red marked are required</small></h5>

                <form action="<?php echo base_url(); ?>Courses/edit" onsubmit="return checkCourseIntake()" method="post" enctype="multipart/form-data">
                   <input type="hidden" value="<?php echo $courses->id;?>" />
                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="inputPassword4">Course Prefix<span style="color:red;">*</span></label>
                            <select class="form-control select2-single" required name="course_prefix_id" required>
                                <option value="">Select</option>
                                <?php

                                if (count($course_prefix)) {
                                    foreach ($course_prefix as $list) {

                                        ?>
                                        <option
                                            <?php if($list['id'] == $courses->course_prefix_id){ echo 'selected'; } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>

                        <div class="form-group col-md-3">
                            <label for="inputPassword4">Course Title<span style="color:red;">*</span></label>
                            <input type="text" class="form-control" id="course_title"
                                   name="course_title" required value="<?php echo $courses->course_title;  ?>">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Course Level<span style="color:red;">*</span></label>
                            <select class="form-control select2-single" required name="course_level_id" required>
                                <option value="">Select</option>
                                <?php

                                if (count($course_level)) {
                                    foreach ($course_level as $list) {

                                        ?>
                                        <option
                                            <?php if($list['id'] == $courses->course_level_id){ echo 'selected'; } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Country<span style="color:red;">*</span></label>
                            <select onchange="getCityByCountryId(this.value)" class="form-control select2-single" required name="country_id" required>
                                <option value="">--Select--</option>
                                <?php

                                if (count($countries)) {
                                    foreach ($countries as $list) {

                                        ?>
                                        <option
                                            <?php if($list['id'] == $courses->country_id){ echo 'selected'; } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Institution<span style="color:red;">*</span></label>
                            <select name="institution_id" id="institution_id" required class="form-control select2-single">
                                <option value="">Select</option>
									<?php

                                if (count($institutions)) {
                                    foreach ($institutions as $list) {

                                        ?>
                                        <option
                                            <?php if($list['id'] == $courses->institution_id){ echo 'selected'; } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>

                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="inputPassword4">Campus Location</label>
                            <input type="text" class="form-control" id="campus_location"
                                   name="campus_location" value="<?php echo $courses->campus_location;  ?>">
                        </div>

                        <div class="form-group col-md-2">
                            <label for="inputPassword4">IELTS Score</label>
                            <input type="text" class="form-control" id="languages"
                                   name="languages" value="<?php echo $courses->languages;  ?>">
                        </div>

                    </div>

                    <div class="form-row">

                        <div class="form-group col-md-3">
                            <label for="inputPassword4">Tuition Fees International</label>
                            <input type="text" class="form-control" id="course_fee"
                                   name="course_fee" value="<?php echo $courses->course_fee;  ?>">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="inputPassword4">Tuition Fees Home</label>
                            <input type="text" class="form-control" id="course_fee_home"
                                   name="course_fee_home" value="<?php echo $courses->course_fee_home;  ?>">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="inputPassword4">Currency (Tuition Fees International)</label>

                            <select class="form-control select2-single" name="tuition_fees_currency">
                                <option value="">Select</option>
                                <?php

                                if (count($currencies)) {
                                    foreach ($currencies as $list) {

                                        ?>
                                        <option <?php if($list['id'] == $courses->tuition_fees_currency){ echo 'selected'; } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="inputPassword4">Currency (Tuition Fees Home)</label>

                            <select class="form-control select2-single" name="tuition_fees_home_currency">
                                <option value="">Select</option>
                                <?php

                                if (count($currencies)) {
                                    foreach ($currencies as $list) {

                                        ?>
                                        <option <?php if($list['id'] == $courses->tuition_fees_home_currency){ echo 'selected'; } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>

                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-2">
                            <label for="inputPassword4">Placement Year? </label>
                            <div class="checkbox">
                                <input type="checkbox" name="is_placement_year" <?php if($courses->is_placement_year){ echo 'checked'; }  ?> id="is_placement_year" value="1">
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="inputPassword4">Course Duration</label>
                            <input type="text" class="form-control" id="course_duration"
                                   name="course_duration" value="<?php echo $courses->course_duration;  ?>">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Course Category / Discipline<span style="color:red;">*</span></label>
                            <select class="form-control select2-single" required name="course_category_id" required>
                                <option value="">Select</option>
                                <?php

                                if (count($disciplines)) {
                                    foreach ($disciplines as $list) {

                                        ?>
                                        <option
                                            <?php if($list['id'] == $courses->course_category_id){ echo 'selected'; } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>

                    </div>

                    <div class="form-row">
                        <!-- <div class="form-group col-md-6">
                            <label for="inputPassword4">Course Intake </label>
                            <select class="form-control" id="intakes_list" multiple name="intakes_list[]" required>
                                <?php

                                if (count($intake_months)) {
                                    foreach ($intake_months as $list) {
										$is_selected = "";
										foreach ($course_intake_month_list as $intake_month_row) {
											if($intake_month_row['intake_month_id'] == $list['id']){
												$is_selected = "selected";
												break;
											}
										}

                                        ?>
                                        <option <?php echo $is_selected; ?> value="<?php echo $list['id']; ?>">
										        <?php echo $list['name']; ?>
										</option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div> -->

                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Related Courses<span style="color:red;">*</span></label>
                            <select class="form-control" id="related_course_list" multiple name="related_course_list[]" required>
                                <?php

                                if (count($related_courses)) {
                                    foreach ($related_courses as $list) {

										$is_selected = "";
										foreach ($related_courses_id_list as $related_courses_id_list_row) {
											if($related_courses_id_list_row['related_course_id'] == $list['id']){
												$is_selected = "selected";
												break;
											}
										}

                                        ?>
                                        <option <?php echo $is_selected; ?> value="<?php echo $list['id']; ?>">
										        <?php echo $list['course_title']; ?>
										</option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                          <label for="inputEmail4">Link</label><br>
                          <input type="text" class="form-control" id="university_link" name="university_link" value="<?php echo $courses->university_link;?>" />
                        </div>
                    </div>
                    <div class="form-row">

                        <div class="form-group col-md-6">
                            <label for="inputEmail4">Module Subject</label><br>
                            <textarea class="form-control" id="module_subject" name="module_subject">
                                <?php echo $courses->module_subject;  ?>
                            </textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputEmail4">Entry Requirement</label><br>
                            <textarea class="form-control" id="entry_requirement" name="entry_requirement">
                                <?php echo $courses->entry_requirement;  ?>
                            </textarea>
                        </div>

                    </div>

                    <div class="form-row">
                      <div class="form-group col-md-6">
                          <label for="inputPassword4">Course Intake </label>
                          <table>
                            <tr>

                              <th style="width:40%;">Name</th>
                              <th style="width:40%;">Starting Date</th>
                              <th style="width:20%;text-align:right;">Active</th>
                            </tr>
                            <?php   (int)$row=0; foreach ($intake_months as $list): ?>
                              <tr>
                              <td>
                                <span class="from-control"><?php echo $list['name']; ?></span>
                                <input type="hidden"
                                       name="intakes_list[<?php echo $row;?>][intake_month_id]" value="<?php echo $list['id']; ?>">
                              </td>
                              <td>
                                <input type="text" class="form-control"
                                       name="intakes_list[<?php echo $row; ?>][starting_date]" value="<?php echo $list['starting_date']; ?>">
                              </td>
                              <td style="text-align:right;">
                                <input type="checkbox"
                                       name="intakes_list[<?php echo $row; ?>][active]" <?php if($list['active']){echo  'checked'; }?> value="1" >
                              </td>
                            </tr>


                           <?php $row++; ?>
                            <?php endforeach; ?>

                          </table>

                      </div>

                        <div class="form-group col-md-6">
                            <label for="inputEmail4">Description</label><br>
                            <textarea class="form-control" id="program_description" name="program_description">
                                <?php echo $courses->program_description;  ?>
                            </textarea>
                        </div>

                    </div>

						<input type="hidden" class="form-control" id="id"
                                   name="id" value="<?php echo $courses->id;  ?>">
                    <button type="submit" class="btn btn-primary d-block mt-3">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        $('#datetimepicker2').datetimepicker({
            locale: 'ru'
        });
    });
</script>
