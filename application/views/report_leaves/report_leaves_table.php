<?php
if(isset($excel)){
  header('Content-type: application/excel');
  $filename = 'Leave Report.xls';
  header('Content-Disposition: attachment; filename='.$filename);
}
?>
<div class="table-responsive">
<table class="table">
    <thead>
      <tr>
          <th  class="text-center" colspan="<?php echo 4 + (count($leave_types) * 3) ?>" scope="col">
            <h3>
               <?php echo $org_info['org_name']; ?>
            </h3>
            <p><?php echo $org_info['address']; ?></p>
            <p><?php echo $org_info['email']; ?></p>
            <p>Leave Report for <?php echo $year; ?></p>
            <p>Branch: <?php echo $org_info['branch_name']; ?></p>
          </th>
      </tr>
      <tr>
            <th scope="col" rowspan="2">#</th>
            <th scope="col" rowspan="2">Name</th>
            <th scope="col" rowspan="2">Employee ID</th>
            <th scope="col" rowspan="2">Designation</th>

            <?php
            foreach ($leave_types as $c_row):
            ?>
                <th colspan="3" class="text-center"><?php echo $c_row['short_name']; ?></th>
            <?php endforeach; ?>
      </tr>

      <tr>
        <?php
        foreach ($leave_types as $c_row):
        ?>
        <th scope="col">Allocated</th>
        <th scope="col">Used</th>
        <th scope="col">Remaining</th>
        <?php endforeach; ?>
      </tr>

    </thead>
    <tbody>

      <?php
      $i = 0;
      foreach ($rData as $row):
          $i++;
          ?>

        <tr>
            <th scope="row"><?php echo $i; ?></th>
            <td><?php echo $row['name']; ?></td>
            <td><?php echo $row['code']; ?></td>
            <td><?php echo $row['post_name']; ?></td>

            <?php
            foreach ($row['leave_data'] as $l_row):
            ?>
            <th scope="col"><?php echo $l_row['allocated_days']; ?></th>
            <th scope="col"><?php echo $l_row['total_used_leave']; ?></th>
            <th scope="col"><?php echo $l_row['allocated_days'] - $l_row['total_used_leave']; ?></th>
            <?php endforeach; ?>

        </tr>

      <?php endforeach; ?>

    </tbody>
</table>
</div>
