<script type="text/javascript">
    function deleteConfirm() {
        var result = confirm("Are you sure to delete?");
        if (result == true) {
            return true;
        }
        else {
            return false;
        }
    }
</script>
<?php $session_user = $this->session->userdata('user_info'); ?>
<h2>
    <?php
    if ($session_user[0]->is_integrate_with_website == 1) {
        ?>
        <a class="button_grey_round" style="margin-bottom: 5px;"
           href="<?php echo base_url(); ?>employee_infos/holiday_synchronize"><span>Holiday Synchronize</span></a>
    <?php
    } else {
        ?>
        <a class="button_grey_round" style="margin-bottom: 5px;"
           href="<?php echo base_url(); ?>timekeepings/holiday_add"><span>Add Holiday</span></a>
    <?php
    }
    ?>
</h2>

<table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <thead>
    <tr>
        <th width="50" scope="col">SL</th>
        <th width="200" scope="col">Holiday Type</th>
        <th width="200" scope="col">Date</th>
        <th width="200" scope="col">Reason</th>
        <th width="200" scope="col">Action</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = 0;
    foreach ($holidays as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td width="34">
                <?php echo $i; ?>
            </td>
            <td><?php echo $row['holiday_name']; ?></td>
            <td><?php echo $row['date']; ?></td>
            <td><?php echo $row['remarks']; ?></td>
            <?php
            if ($session_user[0]->is_integrate_with_website != 1) {
                ?>
                <td style="vertical-align:middle">
                    <a href="<?php echo base_url(); ?>timekeepings/holiday_edit/<?php echo $row['id']; ?>"
                       class="edit_icon" title="Edit"></a>
                    <a href="<?php echo base_url(); ?>timekeepings/holiday_delete/<?php echo $row['id']; ?>"
                       onclick="return deleteConfirm()" class="delete_icon" title="Delete"></a>
                </td>
            <?php
            }
            ?>
        </tr>
    <?php endforeach; ?>

    <tr class="footer">
        <td colspan="5" align="right">
            <!--  PAGINATION START  -->
            <div class="pagination">
                <?php echo $this->pagination->create_links(); ?>
            </div>
            <!--  PAGINATION END  -->
        </td>
    </tr>
    </tbody>
</table>