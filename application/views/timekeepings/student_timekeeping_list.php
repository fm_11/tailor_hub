<table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <thead>
    <tr>
        <th width="50" scope="col">SL</th>
        <th width="200" scope="col">Name</th>
        <th width="150" scope="col">Class</th>
        <th width="100" scope="col">Section</th>
        <th width="150" scope="col">Date</th>
        <th width="120" scope="col">Login Time</th>
        <th width="120" scope="col">Login Time</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = (int)$this->uri->segment(3);
    foreach ($timekeepings as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td width="34">
                <?php echo $i; ?>
            </td>
            <td><?php echo $row['student_name']; ?></td>
            <td><?php echo $row['class_name']; ?></td>
            <td><?php echo $row['section_name']; ?></td>
            <td><?php echo $row['login_status']; ?></td>
            <td><?php echo $row['date']; ?></td>
            <td><?php echo date('h:i:s A', strtotime($row['login_time'])); ?></td>
            <td><?php echo date('h:i:s A', strtotime($row['logout_time'])); ?></td>
        </tr>
    <?php endforeach; ?>

    <tr class="footer">
        <td colspan="7" align="right">
            <!--  PAGINATION START  -->
            <div class="pagination">
                <?php echo $this->pagination->create_links(); ?>
            </div>
            <!--  PAGINATION END  -->
        </td>
    </tr>
    </tbody>
</table>