<script>
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }


</script>

<form name="addForm" id="addForm" action="<?php echo base_url(); ?>timekeepings/getEmployeeAttendanceDetailReport" method="post">

    <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
        <tbody>
        <tr>        
            <td>
                <label>Month</label>
				<select class="smallInput" style="width:200px;" name="month" id="month" required="1">
					<option value="">-- Please Select --</option>
					<option value="1">January</option>
					<option value="2">February</option>
					<option value="3">March</option>
					<option value="4">April</option>
					<option value="5">May</option>
					<option value="6">June</option>
					<option value="7">July</option>
					<option value="8">August</option>
					<option value="9">September</option>
					<option value="10">October</option>
					<option value="11">November</option>
					<option value="12">December</option>
				</select>
               </td>
			</tr>
		<tr>
				 <td>
					<label>Year</label>
					<select class="smallInput" style="width:200px;" name="year" required="1">
						<option value="<?php echo date('Y') - 1; ?>"><?php echo date('Y') - 1; ?></option>
						<option value="<?php echo date('Y'); ?>" selected><?php echo date('Y'); ?></option>
						<option value="<?php echo date('Y') + 1; ?>"><?php echo date('Y') + 1; ?></option>
					</select>
				</td>
        </tr>
		
        <tr>
            <td colspan="5" align="right">
                <input type="button" onclick="printDiv('printableArea')" value="Print Result"/>           
                <input type="submit" class="submit" value="View Report">
            </td>
        </tr>
        </tbody>
    </table>




</form>
<br>


<div id="printableArea">
    <?php
    if (isset($adata)) {
        echo $report;
    }
    ?>
</div>
