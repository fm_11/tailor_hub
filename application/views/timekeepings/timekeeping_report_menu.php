<script type="text/javascript" src="<?php echo base_url(); ?>media/js/jquery-latest.min.js"></script>
<script>
    $(document).ready(function () {
        var str = location.href.toLowerCase();
        $(".sub-header li a").each(function () {
            if (str.indexOf(this.href.toLowerCase()) > -1) {
                $("a.current").removeClass("current");
                $(this).closest('a').addClass('current');
            }
        });
    })

</script>

<?php
$session_user = $this->session->userdata('user_info');
if ($session_user[0]->is_integrate_with_website == 1) {
    ?>
    <ul class="sub-header">
        <li><a href="<?php echo base_url(); ?>timekeepings/login_logout_report"><span>Login/Logout Report</span></a>
        </li>
        <li><a href="<?php echo base_url(); ?>timekeepings/employee_staff_report_absentee"><span>Employee/Staff Absentees Report</span></a>
        </li>
        <li><a href="<?php echo base_url(); ?>timekeepings/student_report_absentee"><span>Student Absentees Report</span></a>
        </li>
        <li><a href="<?php echo base_url(); ?>timekeepings/person_wise_login_logout_report"><span>Person Wise Login/Logout Report</span></a>
        </li>
    </ul>
<?php
} else {
    ?>
    <ul class="sub-header">
        <li><a href="<?php echo base_url(); ?>timekeepings/login_logout_report"><span>Login/Logout Report</span></a>
        </li>
        <li><a href="<?php echo base_url(); ?>timekeepings/employee_staff_report_absentee"><span>Employee/Staff Absentees Report</span></a></li>
        <li><a href="<?php echo base_url(); ?>timekeepings/person_wise_login_logout_report"><span>Person Wise Login/Logout Report</span></a></li>
		 <li><a href="<?php echo base_url(); ?>timekeepings/getEmployeeAttendanceDetailReport"><span>Employee Attendance Details Report</span></a></li>
    </ul>
<?php
}
?>


