
<table width="100%" border="1" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
            <thead>
            <tr>
                <td style="text-align:center;" colspan="<?php echo $number_of_days + 3; ?>" style="text-align:center">
                    <b style="font-size:15px;"><?php echo $HeaderInfo['school_name']; ?></b><br>
                    <b style="font-size:13px;">EIIN: <?php echo $HeaderInfo['eiin_number']; ?><br>
                      Employee Attendance Details Report <br>
					 month of  <?php echo date('F', mktime(0, 0, 0, $month, 10)). ', ' . $year; ?>
					 
					  </b>
					  				 
                </td>
            </tr>

            <tr>
			    <th width="20" scope="col">&nbsp;SL</th>
                <th width="250" scope="col">&nbsp;Name</th>
                <th width="150" scope="col">&nbsp;Index Num.</th>
                <?php
				$i = 1;
				while($i <= $number_of_days){
					if($i < 10){
					   $clm_date = '0'.$i;					
					}else{
					   $clm_date = $i;
					}
					echo '<th width="50">&nbsp;'.$clm_date.'</th>';
					$i++;
				}
				?>
            </tr>

            </thead>
            
			
			<tbody>
        <?php
        $j = 0;
        foreach ($adata as $row):		    			
            $j++;
            ?>
            <tr>
                <td  style="text-align: center;"  class="columnHeader" width="34">
                    <?php echo $j; ?>
                </td>
                <td align="center"><b><?php echo $row['name']; ?></b></td>
                <td align="center"><?php echo $row['employee_index_no']; ?></td> 
                <?php echo $row['data']; ?>				
            </tr>
        <?php endforeach; ?>
        </tbody>
			
        </table>