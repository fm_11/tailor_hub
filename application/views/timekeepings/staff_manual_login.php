<form name="addForm" action="<?php echo base_url(); ?>timekeepings/add_staff_timekeepings" method="post">
    <label>Date</label>
    <input type="text" class="smallInput" placeholder="YYYY-MM-DD" id="txtDate" value="<?php echo date('Y-m-d'); ?>" name="txtDate" required="1"/>

    <br>
    <input type="submit" class="submit" value="Process">
</form>

<script>
    $(function() {
        $( "#txtDate" ).datepicker({
            dateFormat: "yy-mm-dd"
        });
    });
</script>

<link rel="stylesheet" href="<?php echo base_url(); ?>media/calender/all.css">
<script src="<?php echo base_url(); ?>media/calender/jquery.js"></script>
<script src="<?php echo base_url(); ?>media/calender/core.js"></script>
<script src="<?php echo base_url(); ?>media/calender/widget.js"></script>
<script src="<?php echo base_url(); ?>media/calender/datepicker.js"></script>
