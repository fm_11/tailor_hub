<script>
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }

    $(function () {
        $("#txtDate").datepicker({
            dateFormat: "yy-mm-dd"
        });
    });

    function start() {
        selectMonth();
    }
    window.onload = start;

    function selectMonth() {
        document.my_form.get_month.options[new Date().getMonth()].selected = true;
        document.my_form.get_day.options[(new Date().getUTCDate()) - 1].selected = true;
    }
</script>

<link rel="stylesheet" href="<?php echo base_url(); ?>media/calender/all.css">
<script src="<?php echo base_url(); ?>media/calender/jquery.js"></script>
<script src="<?php echo base_url(); ?>media/calender/core.js"></script>
<script src="<?php echo base_url(); ?>media/calender/widget.js"></script>
<script src="<?php echo base_url(); ?>media/calender/datepicker.js"></script>


<form action="<?php echo base_url(); ?>timekeepings/employee_staff_report_absentee" id="my_form" name="my_form"
      method="post">
    &nbsp;&nbsp;Person Type: <select name="person_type" id="person_type" required="required">
        <?php
        $session_user = $this->session->userdata('user_info');
        if ($session_user[0]->is_integrate_with_website == 1) {
            ?>
            <option value="T">Employee</option>
            <option value="ST">Staff</option>
        <?php
        } else {
            ?>
            <option value="T">Employee</option>
            <option value="ST">Staff</option>
        <?php
        }
        ?>


    </select>
    &nbsp;&nbsp;Day: <select name="get_day" id="get_day" required="required">
        <option>1</option>
        <option>2</option>
        <option>3</option>
        <option>4</option>
        <option>5</option>
        <option>6</option>
        <option>7</option>
        <option>8</option>
        <option>9</option>
        <option>10</option>
        <option>11</option>
        <option>12</option>
        <option>13</option>
        <option>14</option>
        <option>15</option>
        <option>16</option>
        <option>17</option>
        <option>18</option>
        <option>19</option>
        <option>20</option>
        <option>21</option>
        <option>22</option>
        <option>23</option>
        <option>24</option>
        <option>25</option>
        <option>26</option>
        <option>27</option>
        <option>28</option>
        <option>29</option>
        <option>30</option>
        <option>31</option>
    </select>
    </select>
    &nbsp;&nbsp;Month: <select name="get_month" id="get_month">
        <option value='01'>January</option>
        <option value='02'>February</option>
        <option value='03'>March</option>
        <option value='04'>April</option>
        <option value='05'>May</option>
        <option value='06'>June</option>
        <option value='07'>July</option>
        <option value='08'>August</option>
        <option value='09'>September</option>
        <option value='10'>October</option>
        <option value='11'>November</option>
        <option value='12'>December</option>
    </select>
    &nbsp;&nbsp;Year: <select name="get_year" name="get_year">
        <?php
        for ($i = date("Y") - 10; $i <= date("Y"); $i++) {
            $sel = ($i == date('Y')) ? 'selected' : '';
            echo "<option value=" . $i . " " . $sel . ">" . date("Y", mktime(0, 0, 0, 0, 1, $i + 1)) . "</option>"; // change This Line
        }
        ?>
    </select>
    <button type="submit">View</button>
</form>
<br>
<?php
if (isset($absentee_info)) {
    ?>
    <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a">
        <tr>
            <th width="100%" style="text-align:right" scope="col">
                <input type="button" onclick="printDiv('printableArea')" value="Print Result"/>
            </th>
        </tr>
    </table>
<?php
}
?>

<div id="printableArea">
    <?php
    if (isset($absentee_info)) {
        ?>
        <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
            <thead>
            <tr>
                <td colspan="7" style="text-align:center">
                    <b style="font-size:15px;"><?php echo $HeaderInfo['school_name']; ?></b><br>
                    <b style="font-size:13px;">EIIN: <?php echo $HeaderInfo['eiin_number']; ?><br>
                        <?php
                        if ($person_type == 'T') {
                            if ($session_user[0]->is_integrate_with_website == 1) {
                                echo 'Employee';
                            } else {
                                echo 'Employee';
                            }
                        } else {
                            echo 'Staff';
                        }
                        ?> Absentees List Report of
                        for <?php echo date('d-M-Y', strtotime($date)); ?></b>

                </td>
            </tr>

            <tr>
                <th width="50" scope="col">SL</th>
                <th width="200" scope="col">Name</th>
                <th width="150" scope="col">Index Number</th>
                <th width="200" scope="col">Post</th>
            </tr>

            </thead>
            <tbody>
            <?php
            $i = 0;
            foreach ($absentee_info as $row):
                $i++;
                ?>
                <tr>
                    <td width="34">
                        <?php echo $i; ?>
                    </td>
                    <td><?php echo $row['name']; ?></td>
                    <td><?php echo $row['index_no']; ?></td>
                    <td><?php echo $row['post_name']; ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>

    <?php
    }
    ?>
</div>