<script>
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }

    $(function () {
        $("#txtDate").datepicker({
            dateFormat: "yy-mm-dd"
        });
    });

    function ClearComboValue(element_id) {
        var sel = document.getElementById(element_id);
        sel.selectedIndex = "";
    }

</script>

<link rel="stylesheet" href="<?php echo base_url(); ?>media/calender/all.css">
<script src="<?php echo base_url(); ?>media/calender/jquery.js"></script>
<script src="<?php echo base_url(); ?>media/calender/core.js"></script>
<script src="<?php echo base_url(); ?>media/calender/widget.js"></script>
<script src="<?php echo base_url(); ?>media/calender/datepicker.js"></script>


<form action="<?php echo base_url(); ?>timekeepings/person_wise_login_logout_report" id="my_form" name="my_form"
      method="post">
    &nbsp;&nbsp;<?php
    $session_user = $this->session->userdata('user_info');
    if ($session_user[0]->is_integrate_with_website == 1) {
        echo 'Employee';
    } else {
        echo 'Employee';
    } ?>: <select name="employee_id" id="employee_id" onchange="ClearComboValue('staff_id')" style="width: 200px;">
        <option value="">--Select--</option>
        <?php foreach ($employee as $row) { ?>
            <option
                value="<?php echo $row['id']; ?>"><?php echo $row['name'] . '(' . $row['employee_index_no'] . ')'; ?></option>

        <?php } ?>
    </select>

    &nbsp;OR&nbsp;&nbsp;Staff: <select name="staff_id" id="staff_id" onchange="ClearComboValue('employee_id')"
                                       style="width: 200px;">
        <option value="">--Select--</option>
        <?php foreach ($staff as $row) { ?>
            <option
                value="<?php echo $row['id']; ?>"><?php echo $row['name'] . '(' . $row['staff_index_no'] . ')'; ?></option>

        <?php } ?>
    </select>

    &nbsp;&nbsp;Month: <select name="get_month" id="get_month">
        <option value='01'>January</option>
        <option value='02'>February</option>
        <option value='03'>March</option>
        <option value='04'>April</option>
        <option value='05'>May</option>
        <option value='06'>June</option>
        <option value='07'>July</option>
        <option value='08'>August</option>
        <option value='09'>September</option>
        <option value='10'>October</option>
        <option value='11'>November</option>
        <option value='12'>December</option>
    </select>
    &nbsp;&nbsp;Year: <select name="get_year" name="get_year">
        <?php
        for ($i = date("Y") - 10; $i <= date("Y"); $i++) {
            $sel = ($i == date('Y')) ? 'selected' : '';
            echo "<option value=" . $i . " " . $sel . ">" . date("Y", mktime(0, 0, 0, 0, 1, $i + 1)) . "</option>"; // change This Line
        }
        ?>
    </select>
    <button type="submit">View</button>
</form>
<br>
<?php
if (isset($time_keeping_info)) {
    ?>
    <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a">
        <tr>
            <th width="100%" style="text-align:right" scope="col">
                <input type="button" onclick="printDiv('printableArea')" value="Print Result"/>
            </th>
        </tr>
    </table>
<?php
}
?>

<div id="printableArea">
    <?php
    if (isset($time_keeping_info)) {
        ?>
        <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
            <thead>
            <tr>
                <td colspan="7" style="text-align:center">
                    <b style="font-size:15px;"><?php echo $HeaderInfo['school_name']; ?></b><br>
                    <b style="font-size:13px;">EIIN: <?php echo $HeaderInfo['eiin_number']; ?><br>
                        <?php echo $title; ?> of
                        <?php echo date('F', mktime(0, 0, 0, $month, 10)) . ', ' . $year; ?></b>

                </td>
            </tr>
            <?php
            if ($person_type == 'S') {
                ?>
                <tr>
                    <th width="50" scope="col">SL</th>
                    <th width="200" scope="col">Name</th>
                    <th width="150" scope="col">Class</th>
                    <th width="100" scope="col">Section</th>
                    <th width="150" scope="col">Date</th>
                    <th width="120" scope="col">Login Time</th>
                    <th width="120" scope="col">Login Time</th>
                </tr>
            <?php
            } else {
                ?>
                <tr>
                    <th width="50" scope="col">SL</th>
                    <th width="200" scope="col">Name</th>
                    <th width="150" scope="col">Index Number</th>
                    <th width="200" scope="col">Post</th>
                    <th width="100" scope="col">Date</th>
                    <th width="100" scope="col">Login Time</th>
                    <th width="100" scope="col">Logout Time</th>
                </tr>
            <?php
            }
            ?>

            </thead>
            <tbody>
            <?php
            $i = 0;
            foreach ($time_keeping_info as $row):
                $i++;
                ?>
                <?php
                if ($person_type == 'S') {
                    ?>
                    <tr>
                        <td width="34">
                            <?php echo $i; ?>
                        </td>
                        <td><?php echo $row['student_name']; ?></td>
                        <td><?php echo $row['class_name']; ?></td>
                        <td><?php echo $row['section_name']; ?></td>
                        <td><?php echo $row['date']; ?></td>
                        <td><?php echo date('h:i:s A', strtotime($row['login_time'])); ?></td>
                        <td><?php echo date('h:i:s A', strtotime($row['logout_time'])); ?></td>
                    </tr>
                <?php
                } else {
                    ?>
                    <tr>
                        <td width="34">
                            <?php echo $i; ?>
                        </td>
                        <td><?php echo $row['name']; ?></td>
                        <td><?php echo $row['index_no']; ?></td>
                        <td><?php echo $row['post_name']; ?></td>
                        <td><?php echo $row['date']; ?></td>
                        <td><?php echo date('h:i:s A', strtotime($row['login_time'])); ?></td>
                        <td><?php echo date('h:i:s A', strtotime($row['logout_time'])); ?></td>
                    </tr>
                <?php
                }
                ?>
            <?php endforeach; ?>
            </tbody>
        </table>

    <?php
    }
    ?>
</div>