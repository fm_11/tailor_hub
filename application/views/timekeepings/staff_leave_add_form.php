<form name="addForm" action="<?php echo base_url(); ?>timekeepings/staff_leave_add" method="post">
    <label>Name</label>
    <select class="smallInput" name="staff_id" required="1">
        <option value="">-- Please Select --</option>
        <?php
        $i = 0;
        if (count($staff)) {
            foreach ($staff as $list) {
                $i++;
                ?>
                <option
                    value="<?php echo $list['id']; ?>"><?php echo $list['name'].'('. $list['staff_index_no'] .')'; ?></option>
            <?php
            }
        }
        ?>
    </select>

    <label>From Date</label>
    <input type="text" class="smallInput" placeholder="YYYY-MM-DD" name="txtFromDate" id="txtFromDate" required="1"/>

    <label>To Date</label>
    <input type="text" class="smallInput" placeholder="YYYY-MM-DD" name="txtToDate" id="txtToDate" required="1"/>
	
	<label>Synchronize Date</label>
    <input type="text" class="smallInput" placeholder="YYYY-MM-DD" name="syn_date" id="syn_date" required="1"/>

    <label>Reason</label>
    <input type="text" class="smallInput wide" name="txtReason" required="1"/>

    <br>
    <br>
    <input type="submit" class="submit" value="Submit">
    <input type="reset" class="submit" value="Reset">
</form>

<script>
    $(function() {
        $( "#txtFromDate,#syn_date,#txtToDate" ).datepicker({
            dateFormat: "yy-mm-dd"
        });       
    });
</script>

<link rel="stylesheet" href="<?php echo base_url(); ?>media/calender/all.css">
<script src="<?php echo base_url(); ?>media/calender/jquery.js"></script>
<script src="<?php echo base_url(); ?>media/calender/core.js"></script>
<script src="<?php echo base_url(); ?>media/calender/widget.js"></script>
<script src="<?php echo base_url(); ?>media/calender/datepicker.js"></script>
