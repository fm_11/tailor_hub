<form name="addForm" action="<?php echo base_url(); ?>timekeepings/add_student_timekeepings" method="post">
    <label>Date</label>
    <input type="text" class="smallInput" placeholder="YYYY-MM-DD" id="txtDate" value="<?php echo date('Y-m-d'); ?>" name="txtDate" required="1"/>


    <label>Class</label>
    <select name="class_id" class="smallInput" required="1" id="class_id">
        <option value="">--All--</option>
        <?php foreach ($class_list as $row) { ?>
            <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
        <?php } ?>
    </select>

    <label>Section</label>
    <select name="section_id" class="smallInput" required="1" id="section_id">
        <option value="">--All--</option>
        <?php foreach ($section_list as $row) { ?>
            <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
        <?php } ?>
    </select>

     <label>Group</label>
    <select name="group" required="1" class="smallInput">
        <option value="">--All--</option>
        <option value="N">N/A</option>
        <option value="S">Science</option>
        <option value="A">Arts</option>
        <option value="C">Commerce</option>
    </select>

    <br>
    <br>
    <input type="submit" class="submit" value="Process">
</form>

<script>
    $(function() {
        $( "#txtDate" ).datepicker({
            dateFormat: "yy-mm-dd"
        });
    });
</script>

<link rel="stylesheet" href="<?php echo base_url(); ?>media/calender/all.css">
<script src="<?php echo base_url(); ?>media/calender/jquery.js"></script>
<script src="<?php echo base_url(); ?>media/calender/core.js"></script>
<script src="<?php echo base_url(); ?>media/calender/widget.js"></script>
<script src="<?php echo base_url(); ?>media/calender/datepicker.js"></script>
