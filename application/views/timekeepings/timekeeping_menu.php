<script type="text/javascript" src="<?php echo base_url(); ?>media/js/jquery-latest.min.js"></script>
<script>
    $(document).ready(function () {
        var str = location.href.toLowerCase();
        $(".sub-header li a").each(function () {
            if (str.indexOf(this.href.toLowerCase()) > -1) {
                $("a.current").removeClass("current");
                $(this).closest('a').addClass('current');
            }
        });
    })

</script>


<?php
$session_user = $this->session->userdata('user_info');
if ($session_user[0]->is_integrate_with_website == 1) {
    ?>
    <ul class="sub-header">
        <li><a href="<?php echo base_url(); ?>timekeepings/retrieve_attendance_data"><span>Retrieve Data</span></a></li>
        <li><a href="<?php echo base_url(); ?>timekeepings/download_attendance_data"><span>Download Data</span></a></li>
        <li><a href="<?php echo base_url(); ?>timekeepings/manual_attendance_data"><span>Manual Attendance</span></a>
        </li>
        <li><a href="<?php echo base_url(); ?>timekeepings/employee_leave_list"><span>Employee Leave</span></a></li>
        <li><a href="<?php echo base_url(); ?>timekeepings/staff_leave_list"><span>Staff Leave</span></a></li>
        <li><a href="<?php echo base_url(); ?>timekeepings/student_leave_list"><span>Student Leave</span></a></li>
        <li><a href="<?php echo base_url(); ?>timekeepings/holiday_list"><span>Academic Holiday</span></a></li>
        <li><a href="<?php echo base_url(); ?>timekeepings/movement_register_list"><span>Movement Register</span></a>
        </li>
        <!--<li><a href="<?php echo base_url(); ?>timekeepings/employee_timekeeping_list"><span>Employee Timekeeping</span></a></li>
    <li><a href="<?php echo base_url(); ?>timekeepings/student_timekeeping_list"><span>Student Timekeeping</span></a></li>
    <li><a href="<?php echo base_url(); ?>timekeepings/staff_timekeeping_list"><span>Staff Timekeeping</span></a></li>-->
    </ul>
<?php
} else {
    ?>
    <ul class="sub-header">
        <li><a href="<?php echo base_url(); ?>timekeepings/retrieve_attendance_data"><span>Retrieve Data</span></a></li>
        <li><a href="<?php echo base_url(); ?>timekeepings/manual_attendance_data"><span>Manual Attendance</span></a>
        </li>
        <li><a href="<?php echo base_url(); ?>timekeepings/employee_leave_list"><span>Employee Leave</span></a></li>
        <li><a href="<?php echo base_url(); ?>timekeepings/staff_leave_list"><span>Staff Leave</span></a></li>
        <li><a href="<?php echo base_url(); ?>timekeepings/holiday_list"><span>Academic Holiday</span></a></li>
        <li><a href="<?php echo base_url(); ?>timekeepings/movement_register_list"><span>Movement Register</span></a>
        </li>
        <li><a href="<?php echo base_url(); ?>timekeepings/employee_timekeeping_list" title="Employee Timekeeping"><span>Employee Time..</span></a>
        </li>
        <li><a href="<?php echo base_url(); ?>timekeepings/staff_timekeeping_list" title="Staff Timekeeping"><span>Staff Time..</span></a>
        </li>
        <!-- <li><a href="<?php echo base_url(); ?>timekeepings/student_timekeeping_list"><span>Student Timekeeping</span></a></li>-->
    </ul>
<?php
}
?>



