<div class="col-12 mb-4">
        <div class="card">
                     <div class="card-body">
                            <h5 class="mb-4"><?php echo $title; ?></h5>

                            <?php
                            if(empty($todays_att_data)){
                              ?>
                            <form action="<?php echo base_url(); ?>timekeepings/self_manual_login" method="post" enctype="multipart/form-data">
                              <button type="submit" class="btn btn-success btn-lg mb-1">Login</button>
                            </form>
                              <?php
                            }else{
                            ?>
                            <form action="<?php echo base_url(); ?>timekeepings/self_manual_logout" method="post" enctype="multipart/form-data">
                                <button type="submit" class="btn btn-danger btn-lg mb-1">Logout</button>
                                <b>Today is Your Login Time:
                                  <?php echo date('h:i:s A', strtotime($todays_att_data[0]['login_time'])); ?>
                            </form>
                          <?php } ?>
                        </div>
       </div>
</div>
