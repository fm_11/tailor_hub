<script>
    $(function () {
        $("#txtDate").datepicker({
            dateFormat: "yy-mm-dd"
        });
    });

    function timeOut() {
        document.getElementById('in_div').style.display = 'none';
        document.getElementById('out_div').style.display = '';
    }
    function timeIn() {
        document.getElementById('in_div').style.display = '';
        document.getElementById('out_div').style.display = 'none';
    }
    function bothInOut() {
        document.getElementById('in_div').style.display = '';
        document.getElementById('out_div').style.display = '';
    }

    function getStudentByClassSectionAndGroup(class_id, section_id, group) {
        if (class_id == '' || section_id == '' || group == '') {
            return false;
        }
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        }
        else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("student_id").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>timekeepings/getStudentByClassSectionAndGroup?class_id=" + class_id + '&&section_id=' + section_id + '&&group=' + group, true);
        xmlhttp.send();
    }


    function person_wise_section(is_student_login) {
        var student_id = document.getElementById("student_id");
        var person_id = document.getElementById("person_id");
        if (is_student_login == 'Y') {
            document.getElementById('student_section').style.display = '';
            document.getElementById('employee_staff_section').style.display = 'none';

            student_id.setAttribute('required','required');
            person_id.removeAttribute('required');
        } else {
            document.getElementById('student_section').style.display = 'none';
            document.getElementById('employee_staff_section').style.display = '';

            person_id.setAttribute('required','required');
            student_id.removeAttribute('required');
        }
    }
</script>

<style type="text/css">
    .input-group {
    }

    .pull-center {
        margin-left: auto;
        margin-right: auto;
    }
</style>

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/timepicker/jquery-clockpicker.min.css">

<h2>Please Input All Correct Information</h2>
<form id="add" action="<?php echo base_url(); ?>timekeepings/manual_attendance_data" method="post"
      enctype="multipart/form-data">


    <?php $session_user = $this->session->userdata('user_info'); ?>
    <?php
    if ($session_user[0]->is_integrate_with_website == 1) {
        ?>
        <label>Is Student Login/Logout?</label>
        <select name="is_student_login" style="width: 130px;" id="is_student_login"
                onchange="person_wise_section(this.value)" required="1" class="smallInput">
            <option value="Y">Yes</option>
            <option value="N">No</option>
        </select>

        <span id="student_section">
    <label>Class</label>
    <select name="class_id"
            onchange="getStudentByClassSectionAndGroup(this.value,document.getElementById('section_id').value,document.getElementById('group').value)"
            class="smallInput" id="class_id">
        <option value="">-- Please Select --</option>
        <?php foreach ($class as $row) { ?>
            <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
        <?php } ?>
    </select>

    <label>Section</label>
    <select name="section_id"
            onchange="getStudentByClassSectionAndGroup(document.getElementById('class_id').value,this.value,document.getElementById('group').value)"
            class="smallInput" id="section_id">
        <option value="">-- Please Select --</option>
        <?php foreach ($section as $row) { ?>
            <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
        <?php } ?>
    </select>

    <label>Group</label>
    <select name="group" id="group"
            onchange="getStudentByClassSectionAndGroup(document.getElementById('class_id').value,document.getElementById('section_id').value,this.value)"
            class="smallInput">
        <option value="">-- Please Select --</option>
        <option value="N">N/A</option>
        <option value="S">Science</option>
        <option value="A">Arts</option>
        <option value="C">Commerce</option>
    </select>

    <label>Student Name</label>
    <select class="smallInput" name="student_id" id="student_id" required="1">
        <option value="">-- Please Select --</option>
    </select>
 </span>
    <?php
    }
    ?>

    <span id="employee_staff_section" <?php if ($session_user[0]->is_integrate_with_website != 0) { ?>style="display: none;" <?php } ?>>
    <label>
        <?php
        $session_user = $this->session->userdata('user_info');
        if ($session_user[0]->is_integrate_with_website == 1) {
            echo 'Employee';
        } else {
            echo 'Employee';
        } ?>/Staff</label>
    <select class="smallInput" name="person_id" id="person_id" <?php if ($session_user[0]->is_integrate_with_website == 0) { ?> required="1" <?php } ?>>
        <option value="">-- Please Select --</option>
        <?php
        if (count($employees)) {
            foreach ($employees as $list) {
                $i++;
                ?>
                <option
                    value="T-<?php echo $list['id']; ?>">
                    <?php if ($session_user[0]->is_integrate_with_website == 1) {
                        echo 'Employee';
                    } else {
                        echo 'Employee';
                    } ?>-<?php echo $list['name'] . '(' . $list['employee_index_no'] . ')'; ?></option>
            <?php
            }
        }
        ?>

        <?php
        if (count($staff)) {
            foreach ($staff as $list) {
                $i++;
                ?>
                <option
                    value="ST-<?php echo $list['id']; ?>">
                    Staff-<?php echo $list['name'] . '(' . $list['staff_index_no'] . ')'; ?></option>
            <?php
            }
        }
        ?>
    </select>
</span>

    <label>Date</label>
    <input type="text" class="smallInput wide" name="txtDate" id="txtDate" required="1"/>

    <label>Login Type</label>
    <input type="radio" name="login_type" onclick="timeIn()" id="in" value="in" required="1"/>In
    <input type="radio" name="login_type" onclick="timeOut()" id="out" value="out" required="1"/>Out
    <input type="radio" name="login_type" id="both" onclick="bothInOut()" checked value="both" required="1"/>Both

    <div id="in_div">
        <label>In Time</label>

        <div class="input-group clockpicker pull-center" data-placement="left" data-align="top" data-autoclose="true">
            <input type="text" name="inTime" class="smallInput wide" value="09:00">
				<span class="input-group-addon">
					<span class="glyphicon glyphicon-time"></span>
				</span>
        </div>
    </div>

    <div id="out_div">
        <label>Out Time</label>

        <div class="input-group clockpicker1 pull-center" data-placement="left" data-align="top" data-autoclose="true">
            <input type="text" name="outTime" class="smallInput wide" value="16:00">
				<span class="input-group-addon">
					<span class="glyphicon glyphicon-time"></span>
				</span>
        </div>
    </div>

    <br>
    <br>
    <input type="submit" class="submit" value="Submit">
    <input type="reset" class="submit" value="Reset">
</form>
<br/>


<div class="clear"></div><br/>

<link rel="stylesheet" href="<?php echo base_url(); ?>media/calender/all.css">
<script src="<?php echo base_url(); ?>media/calender/jquery.js"></script>
<script src="<?php echo base_url(); ?>media/calender/core.js"></script>
<script src="<?php echo base_url(); ?>media/calender/widget.js"></script>
<script src="<?php echo base_url(); ?>media/calender/datepicker.js"></script>

<!--Time Picker-->


<script type="text/javascript" src="<?php echo base_url(); ?>media/timepicker/jquery-clockpicker.min.js"></script>
<script type="text/javascript">
    $('.clockpicker,.clockpicker1').clockpicker({
        placement: 'bottom',
        align: 'right',
        autoclose: true,
        'default': '20:48'
    });
</script>

