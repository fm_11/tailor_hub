<hr>
<div class="table-responsive">
<form action="<?php echo base_url(); ?>manual_taxes/data_save" method="post" enctype="multipart/form-data">
<table class="table">
    <thead>
      <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Employee ID</th>
            <th scope="col">Designation</th>
            <th scope="col">Amount</th>
      </tr>
    </thead>
    <tbody>

      <?php
        $i = 0;
        foreach ($processData as $row):
      ?>

        <tr>
            <th scope="row"><?php echo $i + 1; ?></th>
            <td>
              <?php echo $row['name']; ?>
              <input type="hidden" class="form-control" id="employee_id_<?php echo $i; ?>" required name="employee_id_<?php echo $i; ?>" value="<?php echo $row['id']; ?>" placeholder="">
            </td>
            <td><?php echo $row['code']; ?></td>
            <td><?php echo $row['designation_name']; ?></td>
            <td>
              <input type="text" class="form-control" id="amount_<?php echo $i; ?>" required name="amount_<?php echo $i; ?>" value="<?php if($row['amount'] != ''){ echo $row['amount']; }else{ echo 0; } ?>" placeholder="">
            </td>
        </tr>

      <?php $i++; endforeach; ?>

      <tr>
          <td>

            <input type="hidden" class="form-control" id="finalcial_year_id" required name="finalcial_year_id" value="<?php echo $finalcial_year_id; ?>">
            <input type="hidden" class="form-control" id="total_row" required name="total_row" value="<?php echo $i; ?>">
            <input type="submit" value="Save Data" name="data_save" class="btn btn-primary d-block mt-3">

          </td>
      </tr>

    </tbody>
</table>
</form>
</div>
