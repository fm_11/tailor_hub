<div class="row">
            <div class="col-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <h5 class="mb-4"><?php echo $title; ?></h5>

                            <form action="<?php echo base_url(); ?>manual_taxes/index" method="post" enctype="multipart/form-data">


                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                          <label for="Branch">Finalcial Year</label>
                                          <select class="form-control select2-single"  name="finalcial_year_id">
                                              <option value="">--Select--</option>
                                              <?php
                                              foreach ($finalcial_years as $row):
                                              ?>
                                                 <option value="<?php echo $row['id']; ?>" <?php if(isset($finalcial_year_id)){ if($row['id'] == $finalcial_year_id){ echo 'selected'; } } ?>><?php echo $row['title']; ?></option>
                                              <?php endforeach; ?>
                                          </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="Branch">Branch</label>
                                        <select class="form-control select2-single"  name="branch_id">
                                            <option value="all">All</option>
                                            <?php
                                            foreach ($branches as $row):
                                            ?>
                                               <option value="<?php echo $row['id']; ?>" <?php if(isset($branch_id)){ if($row['id'] == $branch_id){ echo 'selected'; } } ?>><?php echo $row['name']; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="btn-group">
                                      <input type="submit" value="Process" name="process" class="btn btn-primary d-block mt-3">
                                </div>
                    </form>

                    <?php if(isset($processData)){ echo $process_table; } ?>

               </div>
          </div>
      </div>
</div>
