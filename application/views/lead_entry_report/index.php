
<script type="text/javascript">
    function printDiv() {
        var printContents = document.getElementById("printableArea").innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
    function getEmployeeByBranchId(branch_id){
        //alert(branch_id);
        if (window.XMLHttpRequest)
        {
            xmlhttp = new XMLHttpRequest();
        }
        else
        {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function()
        {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            {
                document.getElementById("user_id").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>lead_entry_report/getEmployeeByBrachId?branch_id=" + branch_id, true);
        xmlhttp.send();
    }
</script>
<div class="row">
            <div class="col-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <h5 class="mb-4"><?php echo $title; ?> <small style="color: red;">All Red marked are required</small></h5>

                            <form action="<?php echo base_url(); ?>lead_entry_report/index" method="post" enctype="multipart/form-data" autocomplete="off">
                              <div class="form-row">
                                <?php if($user_type=="hop"){?>
                                 <div class="form-group col-md-6">
                                     <label for="Branch">Branch <span style="color:red;">*</span></label>
                                     <select onchange="getEmployeeByBranchId(this.value)" class="form-control select2-single"  name="branch_id">
                                         <option value="">--Please Select--</option>
                                         <?php
                                         foreach ($branches as $row):
                                         ?>
                                            <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
                                         <?php endforeach; ?>
                                     </select>
                                 </div>
                                 <div class="form-group col-md-6">
                                     <label for="Branch">Employee <span style="color:red;">*</span></label>
                                     <select class="form-control select2-single" id="user_id"  name="user_id" required>
                                         <option value="">--Please Select--</option>
                                         <?php
                                         foreach ($employees as $row):
                                         ?>
                                            <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
                                         <?php endforeach; ?>
                                     </select>
                                 </div>
                               <?php } elseif ($user_type=="hoa") {?>
                                 <div class="form-group col-md-12">
                                     <label for="Branch">Employee <span style="color:red;">*</span></label>
                                     <select class="form-control select2-single" id="user_id"  name="user_id" required>
                                         <option value="">--Please Select--</option>
                                         <?php
                                         foreach ($employees as $row):
                                         ?>
                                            <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
                                         <?php endforeach; ?>
                                     </select>
                                 </div>
                               <?php  }else{?>
                                 <input type="hidden" name="user_id" value="<?php echo $user_ids;?>"/>
                               <?php } ?>
                              </div>
                             <div class="form-row">
                               <div class="form-group col-md-6">
                                     <label for="Branch">From Date<span style="color:red;">*</span></label>
                                     <div class="input-group date">
                                        <input type="text" name="from_date" required class="form-control">
                                        <span class="input-group-text input-group-append input-group-addon">
                                            <i class="simple-icon-calendar"></i>
                                        </span>
                                    </div>
                               </div>
                               <div class="form-group col-md-6">
                                     <label for="Branch">To Date<span style="color:red;">*</span></label>
                                     <div class="input-group date">
                                        <input type="text" name="to_date" required class="form-control">
                                        <span class="input-group-text input-group-append input-group-addon">
                                            <i class="simple-icon-calendar"></i>
                                        </span>
                                    </div>
                               </div>
                             </div>

                              <div class="btn-group">
                                    <input type="submit" value="Report View" name="view" class="btn btn-primary d-block mt-3">
                                    <input type="submit" name="excel" value="Download Excel" class="btn btn-outline-dark mt-3">
                                    <input   onclick="printDiv();" value="Print Report" class="btn btn-outline-dark mt-3">
                              </div>
                    </form>
                  <div id="printableArea">
                   <?php if(isset($rData)){ echo $report; } ?>
                   </div>
               </div>
          </div>
      </div>
</div>
