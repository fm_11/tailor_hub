<hr>
<style>
.my-custom-scrollbar {
/* position: relative;
height: 500px;
overflow: auto; */
}
.table-wrapper-scroll-y {
display: block;
}
</style>
<?php
if(isset($excel)){
  header('Content-type: application/excel');
  $filename = 'Lead Entry Report.xls';
  header('Content-Disposition: attachment; filename='.$filename);
}
?>
<div class="table-wrapper-scroll-y my-custom-scrollbar">
<table class="table table-responsive">
    <thead>
      <tr>
          <th  class="text-center" colspan="6" scope="col">
            <h3>
               <?php echo $org_info['org_name']; ?>
            </h3>
            <p><?php echo $org_info['address']; ?></p>
            <p><?php echo $org_info['email']; ?></p>
            <p>Student Registraion Report</p>
            <p>Branch: <?php echo $org_info['branch_name']; ?></p>
          </th>
      </tr>
      <tr>
            <th scope="col" class="text-left" colspan="2">Employee Name</th>
            <td scope="col" class="text-left" colspan="1"><?php echo $employee_info[0]['name']; ?></td>
     </tr>
     <tr>
           <th scope="col" class="text-left" colspan="2">Employee Code</th>
           <td scope="col" class="text-left" colspan="1"><?php echo $employee_info[0]['code']; ?></td>
    </tr>
    <tr>
          <th scope="col" class="text-left" colspan="2">Designation</th>
          <td scope="col" class="text-left" colspan="1"><?php echo $employee_info[0]['designation']; ?></td>
   </tr>
   <tr>
         <th scope="col" class="text-left" colspan="2">Report Period</th>
         <td scope="col" class="text-left" colspan="1"><?php echo $from_date; ?> To <?php echo $to_date; ?></td>
  </tr>
  <tr>
        <th scope="col" class="text-left" colspan="2">Total Day</th>
        <td scope="col" class="text-left" colspan="1"><?php echo $total_day; ?></td>
 </tr>
 <tr>
       <th scope="col" class="text-left" colspan="2">Report To</th>
       <td scope="col" class="text-left" colspan="1"><?php echo $report_to[0]['designation']; ?></td>
</tr>

      <tr>
            <th scope="col">#</th>
            <th scope="col">Source Name</th>
            <th scope="col">Quantity</th>

     </tr>
    </thead>
    <tbody>
      <?php
      $i = 0;
      foreach ($rData as $row):
          $i++;
          ?>
        <tr>
            <th scope="row"><?php echo $i; ?></th>
            <td><?php echo $row['name']; ?></td>
            <td><?php echo $row['quantity']; ?></td>
        </tr>
      <?php endforeach; ?>
      <tr>
            <th colspan="2"></th>
            <td colspan="1"></td>
     </tr>
     <tr>
           <td colspan="2"><?php echo $printed_by[0]['name']; ?></td>

            <td colspan="1"><?php echo $employee_info[0]['reporting_boss']; ?> </td>
    </tr>
    <tr>
          <th   colspan="2"><span style="border-top: 1px solid #3a2626;">Prepared By</span> </th>

          <th   colspan="1"><span style="border-top: 1px solid #3a2626;">Checked By</span></th>
   </tr>
   <tr>
         <th  colspan="2"><?php echo $printed_by[0]['designation_name']; ?> </th>

         <th  colspan="1"><?php echo $employee_info[0]['reporting_boss_designation']; ?> </th>
  </tr>

    </tbody>
</table>
</div>
