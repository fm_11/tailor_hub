
<script type="text/javascript">
    function printDiv() {
        var printContents = document.getElementById("printableArea").innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
    function getEmployeeByBranchId(branch_id){
        //alert(branch_id);
        if (window.XMLHttpRequest)
        {
            xmlhttp = new XMLHttpRequest();
        }
        else
        {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function()
        {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            {
                document.getElementById("user_id").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>lead_followup_report/getEmployeeByBrachId?branch_id=" + branch_id, true);
        xmlhttp.send();
    }

</script>


<div class="row">
            <div class="col-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <h5 class="mb-4"><?php echo $title; ?></h5>

                            <form action="<?php echo base_url(); ?>admission_report/index" method="post" enctype="multipart/form-data" autocomplete="off" required>
                                 <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="Branch">Branch <span style="color:red;">*</span></label>
                                        <select  class="form-control select2-single"  name="branch_id" required>
                                            <option value="">--Please Select--</option>
                                            <?php
                                            foreach ($branches as $row):
                                            ?>
                                               <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>


                                 </div>
                                <div class="form-row">
                                  <div class="form-group col-md-6">
                                        <label for="Branch">Year<span style="color:red;">*</span></label>
                                        <select class="form-control select2-single" id="intake_years"  name="intake_years" required>
                                            <option value="">--Please Select--</option>
                                            <?php
                                            foreach ($intake_years as $row):
                                            ?>
                                               <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                  </div>
                                  <div class="form-group col-md-6">
                                        <label for="Branch">Month<span style="color:red;">*</span></label>
                                        <select class="form-control select2-single" id="intake_months"  name="intake_months" required>
                                            <option value="">--Please Select--</option>
                                            <?php
                                            foreach ($intake_months as $row):
                                            ?>
                                               <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                  </div>
                                </div>
                              <div class="btn-group">
                                    <input type="submit" value="Report View" name="view" class="btn btn-primary d-block mt-3">
                                    <input type="submit" name="excel" value="Download Excel" class="btn btn-outline-dark mt-3">
                                    <input   onclick="printDiv();" value="Print Report" class="btn btn-outline-dark mt-3">
                              </div>
                            </form>
                            <div id="printableArea">
                             <?php if(isset($rData)){ echo $report; } ?>
                             </div>
                      </div>
                 </div>
          </div>
  </div>
