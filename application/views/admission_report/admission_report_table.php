<hr>
<style>
.my-custom-scrollbar {
/* position: relative;
height: 500px;
overflow: auto; */
}
.table-wrapper-scroll-y {
display: block;
}
</style>
<?php
if(isset($excel)){
  header('Content-type: application/excel');
  $filename = 'Admission Report.xls';
  header('Content-Disposition: attachment; filename='.$filename);
}
?>
<div class="table-wrapper-scroll-y my-custom-scrollbar">
<table class="table table-responsive">
    <thead>
      <tr>
          <th  class="text-center" colspan="8" scope="col">
            <h3>
               <?php echo $org_info['org_name']; ?>
            </h3>
            <p><?php echo $org_info['address']; ?></p>
            <p><?php echo $org_info['email']; ?></p>
            <p><?php echo $title; ?></p>
            <p>Branch: <?php echo $org_info['branch_name']; ?></p>
          </th>
      </tr>
      <tr>
            <th scope="col" class="text-left" colspan="2">Branch Name</th>
            <td scope="col" class="text-left" colspan="6"><?php echo $branch[0]['name']; ?></td>
     </tr>
   <tr>
         <th scope="col" class="text-left" colspan="2">Report Period</th>
         <td scope="col" class="text-left" colspan="6"><?php echo $intake; ?></td>
  </tr>
  <tr>
        <th scope="col" class="text-left" colspan="2">Total Day</th>
        <td scope="col" class="text-left" colspan="6"><?php echo $total_day; ?></td>
 </tr>
 <tr>
       <th scope="col" class="text-left" colspan="2">Report To</th>
       <td scope="col" class="text-left" colspan="6"><?php echo $report_to[0]['designation']; ?></td>
</tr>
      <tr>
            <th scope="col">#SL</th>
            <th scope="col">Employee</th>
            <th scope="col">Code</th>
            <th scope="col">Application Submitted</th>
            <th scope="col">Status Cancel</th>
            <th scope="col">Apply Visa</th>
            <th scope="col">Got Visa</th>
            <th scope="col">Success</th>
     </tr>
    </thead>
    <tbody>
      <?php
      $i = 0;
      $total_application_submitted=0;
      $total_application_cancel=0;
      $total_visa_application_submitted=0;
      $total_visa_received=0;
      foreach ($rData as $row):
          $i++;
          $success=0;
          $total_application_submitted=$total_application_submitted+$row['application_submitted'];
          $total_application_cancel=$total_application_cancel+$row['application_cancel'];
          $total_visa_application_submitted=$total_visa_application_submitted+$row['visa_application_submitted'];
          $total_visa_received=$total_visa_received+$row['visa_received'];
          if($row['visa_application_submitted']!='0')
          {
              $success=($row['visa_received']/$row['visa_application_submitted'])*100;
          }

          ?>
        <tr>
            <th scope="row"><?php echo $i; ?></th>
            <td><?php echo $row['name']; ?></td>
            <td><?php echo $row['code']; ?></td>
            <td><?php echo $row['application_submitted']; ?></td>
            <td><?php echo $row['application_cancel']; ?></td>
            <td><?php  echo $row['visa_application_submitted']; ?></td>
            <td><?php echo $row['visa_received']; ?></td>
            <td><?php  echo round($success,2); ?> %</td>
        </tr>
      <?php endforeach; ?>
      <tr>
            <th colspan="3" style="text-align:right;">Total : </th>
            <td colspan=""><?php  echo $total_application_submitted; ?></td>
            <td colspan=""><?php  echo $total_application_cancel; ?></td>
            <td colspan=""><?php  echo $total_visa_application_submitted; ?></td>
            <td colspan=""><?php  echo $total_visa_received; ?></td>

     </tr>
     <tr>
          <th colspan="8">Comments:</th>
    </tr>
     <tr>
          <th colspan="3">Branch Assistance Manager</th>
          <th colspan="2">Head Of Admission</th>
          <th colspan="3">Head Of Process</th>
    </tr>
    <tr style="height: 160px;">
         <th colspan="3"></th>
         <th colspan="2"></th>
         <th colspan="3"></th>
   </tr>
     <tr>
           <th colspan="5"><?php echo $printed_by[0]['name']; ?></th>
           <td colspan="3"></td>
    </tr>
    <tr>
          <th  class="text-left" colspan="5"><span style="border-top: 1px solid #3a2626;">Prepared By</span> </th>
          <th scope="col" class="text-left" colspan="3"><span style="border-top: 1px solid #3a2626;">Approved By</span></th>

   </tr>
   <tr>
         <th scope="col" class="text-left" colspan="5"><?php echo $printed_by[0]['designation_name']; ?> </th>
         <th scope="col" class="text-left" colspan="3"><?php echo $report_to[0]['designation']; ?></th>

  </tr>

    </tbody>
</table>
</div>
