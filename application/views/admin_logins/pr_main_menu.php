<?php
$controller = $this->uri->segment(1);
?>
<div class="main-menu">
		<div class="scroll">
			<ul class="list-unstyled">
				<li <?php if($controller == 'dashboard'){ ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>dashboard/<?php echo $this->session->userdata('module_short_name').'_dashboard'; ?>">
						<i class="iconsminds-shop-4"></i>
						<span>Dashboard</span>
					</a>
				</li>
				<li <?php if($controller == 'salary_types' || $controller == 'bonus_types' || $controller == 'other_allow_deduc_types'){ ?>class="active" <?php } ?>>
					<a href="#configuraton">
						<i class="iconsminds-digital-drawing"></i> Configuration
					</a>
				</li>
				<li <?php if($controller == 'manual_taxes' || $controller == 'other_allow_deduc_processes'){ ?>class="active" <?php } ?>>
					<a href="#process">
						<i class="iconsminds-pantone"></i> Process
					</a>
				</li>

				<li <?php if($controller == 'daily_attendances'){ ?>class="active" <?php } ?>>
					<a href="#report">
						<i class="iconsminds-library"></i> Report
					</a>
				</li>

			</ul>
		</div>
	</div>

	<div class="sub-menu">
		<div class="scroll">
			<ul class="list-unstyled" data-link="configuraton">

				<li>
					<a href="<?php echo base_url(); ?>salary_types/index">
						<i class="simple-icon-pie-chart"></i> <span class="d-inline-block">Salary Type</span>
					</a>
				</li>

				<li>
					<a href="<?php echo base_url(); ?>bonus_types/index">
						<i class="simple-icon-list"></i> <span class="d-inline-block">Bonus Type</span>
					</a>
				</li>

        <li>
          <a href="<?php echo base_url(); ?>designations/index">
            <i class="simple-icon-badge"></i> <span class="d-inline-block">Bonus Configuration</span>
          </a>
        </li>

				<li>
					<a href="<?php echo base_url(); ?>other_allow_deduc_types/index">
						<i class="simple-icon-grid"></i> <span class="d-inline-block">Other Allow/Decuc Type</span>
					</a>
				</li>

			</ul>
			<ul class="list-unstyled" data-link="process">
				<li>
					<a href="<?php echo base_url(); ?>manual_taxes/index">
						<i class="simple-icon-grid"></i> <span class="d-inline-block">Manual Tax</span>
					</a>
				</li>

				<li>
					<a href="<?php echo base_url(); ?>other_allow_deduc_processes/index">
						<i class="simple-icon-cursor"></i> <span class="d-inline-block">Other Allow/Decuc Process</span>
					</a>
				</li>

        <li>
					<a href="<?php echo base_url(); ?>salary_processes/index">
						<i class="simple-icon-shuffle"></i> <span class="d-inline-block">Salary Process</span>
					</a>
				</li>

			</ul>
			<ul class="list-unstyled" data-link="report">
				<li>
					<a href="<?php echo base_url(); ?>timekeepings/index">
						<i class="simple-icon-check mi-forms"></i> <span class="d-inline-block">Monthly Salary</span>
					</a>
				</li>

				<li>
					<a href="<?php echo base_url(); ?>movement_registers/index">
						<i class="simple-icon-check mi-forms"></i> <span class="d-inline-block">Pay Slip</span>
					</a>
				</li>

				<li>
					<a href="<?php echo base_url(); ?>leave_applications/index">
						<i class="simple-icon-check mi-forms"></i> <span class="d-inline-block">Bonus Report</span>
					</a>
				</li>

				<li>
					<a href="<?php echo base_url(); ?>leave_applications/approval_index">
						<i class="simple-icon-check mi-forms"></i> <span class="d-inline-block">Bank Statement</span>
					</a>
				</li>

			</ul>

		</div>
	</div>
