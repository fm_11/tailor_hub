<?php
$controller = $this->uri->segment(1);
?>
<div class="main-menu">
    <div class="scroll">
        <ul class="list-unstyled">
            <li <?php if ($controller == 'dashboard') { ?>class="active" <?php } ?>>
                <a href="<?php echo base_url(); ?>dashboard/<?php echo $this->session->userdata('module_short_name').'_dashboard'; ?>">
                    <i class="iconsminds-shop-4"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            <li <?php if (in_array($controller, array('courses','cities','agents','institutions','currencies','course_prefixes','intake_years','intake_months','institution_types','course_types','course_levels'))) { ?>class="active"<?php } ?>>
                <a href="#configuraton">
                    <i class="iconsminds-digital-drawing"></i> Configuration
                </a>
            </li>
            <li <?php if (in_array($controller, array('on_process_applicants','applications','leads','students_profile','applicants','application_status'))) { ?>class="active" <?php } ?>>
    					<a href="#process">
    						<i class="iconsminds-air-balloon-1"></i> Process
    					</a>
    				</li>
            <li <?php if (in_array($controller, array('on_process_applicants','applications','leads','students_profile','applicants','application_status'))) { ?>class="active" <?php } ?>>
    					<a href="#reports">
    						<i class="iconsminds-air-balloon-1"></i> Reports
    					</a>
    				</li>
        </ul>
    </div>
</div>

<div class="sub-menu">
    <div class="scroll">
        <ul class="list-unstyled" data-link="configuraton">

            <li>
                <a href="<?php echo base_url(); ?>currencies/index">
                    <i class="simple-icon-pie-chart"></i> <span class="d-inline-block">Currency</span>
                </a>
            </li>

            <li>
                <a href="<?php echo base_url(); ?>course_prefixes/index">
                    <i class="simple-icon-layers"></i> <span class="d-inline-block">Course Prefix</span>
                </a>
            </li>

            <li>
                <a href="<?php echo base_url(); ?>intake_years/index">
                    <i class="simple-icon-list"></i> <span class="d-inline-block">Intake Year</span>
                </a>
            </li>

            <li>
                <a href="<?php echo base_url(); ?>intake_months/index">
                    <i class="simple-icon-grid"></i> <span class="d-inline-block">Intake Month</span>
                </a>
            </li>

            <li>
                <a href="<?php echo base_url(); ?>institution_types/index">
                    <i class="simple-icon-badge"></i> <span class="d-inline-block">Institution Types</span>
                </a>
            </li>

            <li>
                <a href="<?php echo base_url(); ?>course_types/index">
                    <i class="simple-icon-calculator"></i> <span class="d-inline-block">Course Type</span>
                </a>
            </li>

            <li>
                <a href="<?php echo base_url(); ?>course_levels/index">
                    <i class="simple-icon-exclamation"></i> <span class="d-inline-block">Course Level</span>
                </a>
            </li>

		      	<li>
                <a href="<?php echo base_url(); ?>institutions/index">
                    <i class="simple-icon-exclamation"></i> <span class="d-inline-block">Institutions</span>
                </a>
            </li>

		      	<li>
                <a href="<?php echo base_url(); ?>courses/index">
                    <i class="simple-icon-exclamation"></i> <span class="d-inline-block">Course</span>
                </a>
            </li>

            <li>
                <a href="<?php echo base_url(); ?>cities/index">
                    <i class="simple-icon-exclamation"></i> <span class="d-inline-block">City</span>
                </a>
            </li>

            <li>
                <a href="<?php echo base_url(); ?>agents/index">
                    <i class="simple-icon-exclamation"></i> <span class="d-inline-block">Agent</span>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url(); ?>valid_agreements/index">
                    <i class="simple-icon-exclamation"></i> <span class="d-inline-block">Valid Agreements</span>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url(); ?>application_status_details/index">
                    <i class="simple-icon-exclamation"></i> <span class="d-inline-block">Application Status</span>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url(); ?>lead_sources/index">
                    <i class="simple-icon-exclamation"></i> <span class="d-inline-block">Lead Sources</span>
                </a>
            </li>

        </ul>

        <ul class="list-unstyled" data-link="process">
  				<li>
  					<a href="<?php echo base_url(); ?>leads/index">
  						<i class="simple-icon-grid"></i> <span class="d-inline-block">Leads</span>
  					</a>
  				</li>
  				<li>
  					<a href="<?php echo base_url(); ?>leads/my_desk">
  						<i class="simple-icon-grid"></i> <span class="d-inline-block">My Desk</span>
  					</a>
  				</li>
          <li>
  					<a href="<?php echo base_url(); ?>leads/deleted_leads">
  						<i class="simple-icon-grid"></i> <span class="d-inline-block">Deleted Leads</span>
  					</a>
  				</li>
          <li>
  					<a href="<?php echo base_url(); ?>applicants/index">
  						<i class="simple-icon-grid"></i> <span class="d-inline-block">Applicants</span>
  					</a>
  				</li>
          <li>
  					<a href="<?php echo base_url(); ?>applications/index">
  						<i class="simple-icon-grid"></i> <span class="d-inline-block">Pending Application</span>
  					</a>
  				</li>

          <li>
            <a href="<?php echo base_url(); ?>application_status/index">
              <i class="simple-icon-grid"></i> <span class="d-inline-block">Application List</span>
            </a>
          </li>

          <li>
            <a href="<?php echo base_url(); ?>on_process_applicants/index">
              <i class="simple-icon-grid"></i> <span class="d-inline-block">On-Process Applicants</span>
            </a>
          </li>


  			</ul>
        <ul class="list-unstyled" data-link="reports">
          <li>
            <a href="<?php echo base_url(); ?>agent_registration_report/index">
              <i class="simple-icon-grid"></i> <span class="d-inline-block">Agent Registration</span>
            </a>
          </li>
          <li>
            <a href="<?php echo base_url(); ?>student_registration_report/index">
              <i class="simple-icon-grid"></i> <span class="d-inline-block">Student Registration</span>
            </a>
          </li>
          <li>
            <a href="<?php echo base_url(); ?>lead_entry_report/index">
              <i class="simple-icon-grid"></i> <span class="d-inline-block">Lead Entry</span>
            </a>
          </li>
          <li>
            <a href="<?php echo base_url(); ?>visitors_report/index">
              <i class="simple-icon-grid"></i> <span class="d-inline-block">Visitors</span>
            </a>
          </li>
          <li>
            <a href="<?php echo base_url(); ?>lead_followup_report/index">
              <i class="simple-icon-grid"></i> <span class="d-inline-block">Lead Followup</span>
            </a>
          </li>
          <li>
            <a href="<?php echo base_url(); ?>admission_report/index">
              <i class="simple-icon-grid"></i> <span class="d-inline-block">Admission</span>
            </a>
          </li>
          <li>
            <a href="<?php echo base_url(); ?>process_report/index">
              <i class="simple-icon-grid"></i> <span class="d-inline-block">Process</span>
            </a>
          </li>
          <li>
            <a href="<?php echo base_url(); ?>intake_admission_report/index">
              <i class="simple-icon-grid"></i> <span class="d-inline-block">Admission (Individual)</span>
            </a>
          </li>
        </ul>
    </div>
</div>
