<?php
$controller = $this->uri->segment(1);
?>
<div class="main-menu">
		<div class="scroll">
			<ul class="list-unstyled">
				<li <?php if($controller == 'dashboard'){ ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>dashboard/<?php echo $this->session->userdata('module_short_name').'_dashboard'; ?>">
						<i class="iconsminds-shop-4"></i>
						<span>Dashboard</span>
					</a>
				</li>
				<li <?php if($controller == 'branches' || $controller == 'shifts' || $controller == 'designations' || $controller == 'leave_types' || $controller == 'org_infos' || $controller == 'departments' || $controller == 'holidays'){ ?>class="active" <?php } ?>>
					<a href="#configuraton">
						<i class="iconsminds-digital-drawing"></i> Configuration
					</a>
				</li>
				<li <?php if($controller == 'employees'){ ?>class="active" <?php } ?>>
					<a href="#employee">
						<i class="iconsminds-air-balloon-1"></i> Tailor
					</a>
				</li>
				<li <?php if($controller == 'tailor_order'){ ?>class="active" <?php } ?>>
					<a href="#order">
						<i class="iconsminds-air-balloon-1"></i> Order
					</a>
				</li>
				<li <?php if($controller == 'news'){ ?>class="active" <?php } ?>>
					<a href="#website">
						<i class="iconsminds-air-balloon-1"></i> Website
					</a>
				</li>
				<li <?php if($controller == 'users' || $controller == 'user_roles' || $controller == 'user_role_wise_privileges'){ ?>class="active" <?php } ?>>
					<a href="#user">
						<i class="iconsminds-bucket"></i> Users
					</a>
				</li>
			</ul>
		</div>
	</div>

	<div class="sub-menu">
		<div class="scroll">
			<ul class="list-unstyled" data-link="configuraton">


				<li>
					<a href="<?php echo base_url(); ?>divisions/index">
						<i class="simple-icon-layers"></i> <span class="d-inline-block">Division</span>
					</a>
				</li>

				<li>
					<a href="<?php echo base_url(); ?>areas/index">
						<i class="simple-icon-layers"></i> <span class="d-inline-block">Area</span>
					</a>
				</li>
				<li>
					<a href="<?php echo base_url(); ?>Shop_amenities/index">
						<i class="simple-icon-layers"></i> <span class="d-inline-block">Shop Amenities</span>
					</a>
				</li>
				<li>
					<a href="<?php echo base_url(); ?>items/index">
						<i class="simple-icon-layers"></i> <span class="d-inline-block">Items</span>
					</a>
				</li>
				<li>
					<a href="<?php echo base_url(); ?>Tailor_customer_review/index">
						<i class="simple-icon-layers"></i> <span class="d-inline-block">Customer Review</span>
					</a>
				</li>
				<li>
					<a href="<?php echo base_url(); ?>Tailor_milestone/index">
						<i class="simple-icon-layers"></i> <span class="d-inline-block">Milestone</span>
					</a>
				</li>
			</ul>
			<ul class="list-unstyled" data-link="employee">
				<li>
					<a href="<?php echo base_url(); ?>tailor_entry/index">
						<i class="simple-icon-grid"></i> <span class="d-inline-block">Tailo List</span>
					</a>
				</li>
				<li>
					<a href="<?php echo base_url(); ?>tailor_entry/add">
						<i class="simple-icon-user-follow"></i> <span class="d-inline-block">Tailor Add</span>
					</a>
				</li>

			</ul>
			<ul class="list-unstyled" data-link="order">
				<li>
					<a href="<?php echo base_url(); ?>tailor_order/index">
						<i class="simple-icon-calculator"></i> <span class="d-inline-block">All Orders</span>
					</a>
				</li>
				<li>
					<a href="<?php echo base_url(); ?>tailor_order/index_pending">
						<i class="simple-icon-grid"></i> <span class="d-inline-block">All Pending Orders</span>
					</a>
				</li>
				<li>
					<a href="<?php echo base_url(); ?>tailor_order/index_on_process">
						<i class="simple-icon-grid"></i> <span class="d-inline-block">All On Process Order</span>
					</a>
				</li>
				<li>
					<a href="<?php echo base_url(); ?>tailor_order/index_delivered">
						<i class="simple-icon-grid"></i> <span class="d-inline-block">All Delivered  Orders</span>
					</a>
				</li>
				<li>
					<a href="<?php echo base_url(); ?>tailor_order/index_reject">
						<i class="simple-icon-grid"></i> <span class="d-inline-block">All Rejected Orders</span>
					</a>
				</li>
			</ul>
			<ul class="list-unstyled" data-link="website">
				<li>
					<a href="<?php echo base_url(); ?>contact/index">
						<i class="simple-icon-grid"></i> <span class="d-inline-block">Contact</span>
					</a>
				</li>
				<li>
					<a href="<?php echo base_url(); ?>About_us/index">
						<i class="simple-icon-user-follow"></i> <span class="d-inline-block">About Us</span>
					</a>
				</li>
				<li>
					<a href="<?php echo base_url(); ?>news/index">
						<i class="simple-icon-grid"></i> <span class="d-inline-block">News</span>
					</a>
				</li>
			</ul>
			<ul class="list-unstyled" data-link="user">
				<li>
					<a href="<?php echo base_url(); ?>user_roles/index">
						<i class="simple-icon-calculator"></i> <span class="d-inline-block">User Roles</span>
					</a>
				</li>
				<li>
					<a href="<?php echo base_url(); ?>users/index">
						<i class="simple-icon-list"></i> <span class="d-inline-block">User List</span>
					</a>
				</li>
				<li>
					<a href="<?php echo base_url(); ?>users/add">
						<i class="simple-icon-user-follow"></i> <span class="d-inline-block">User Add</span>
					</a>
				</li>

			</ul>
		</div>
	</div>
