<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title><?php echo $title; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<?php echo base_url(); ?>media/admin_panel/favicon.png" type="image/png">
    <link rel="stylesheet" href="<?php echo base_url(); ?>media/admin_panel/font/iconsmind-s/css/iconsminds.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>media/admin_panel/font/simple-line-icons/css/simple-line-icons.css" />

    <link rel="stylesheet" href="<?php echo base_url(); ?>media/admin_panel/css/vendor/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>media/admin_panel/css/vendor/bootstrap.rtl.only.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>media/admin_panel/css/vendor/fullcalendar.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>media/admin_panel/css/vendor/dataTables.bootstrap4.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>media/admin_panel/css/vendor/datatables.responsive.bootstrap4.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>media/admin_panel/css/vendor/select2.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>media/admin_panel/css/vendor/perfect-scrollbar.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>media/admin_panel/css/vendor/owl.carousel.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>media/admin_panel/css/vendor/bootstrap-stars.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>media/admin_panel/css/vendor/nouislider.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>media/admin_panel/css/vendor/bootstrap-datepicker3.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>media/admin_panel/css/vendor/component-custom-switch.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>media/admin_panel/css/main.css" />
</head>

<body id="app-container" class="menu-default show-spinner">
    <nav class="navbar fixed-top">
        <div class="d-flex align-items-center navbar-left">
            <a href="#" class="menu-button d-none d-md-block">
                <svg class="main" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 9 17">
                    <rect x="0.48" y="0.5" width="7" height="1" />
                    <rect x="0.48" y="7.5" width="7" height="1" />
                    <rect x="0.48" y="15.5" width="7" height="1" />
                </svg>
                <svg class="sub" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 17">
                    <rect x="1.56" y="0.5" width="16" height="1" />
                    <rect x="1.56" y="7.5" width="16" height="1" />
                    <rect x="1.56" y="15.5" width="16" height="1" />
                </svg>
            </a>

            <a href="#" class="menu-button-mobile d-xs-block d-sm-block d-md-none">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26 17">
                    <rect x="0.5" y="0.5" width="25" height="1" />
                    <rect x="0.5" y="7.5" width="25" height="1" />
                    <rect x="0.5" y="15.5" width="25" height="1" />
                </svg>
            </a>

            <div class="search" data-search-path="Pages.Search.html?q=">
                <input placeholder="Search...">
                <span class="search-icon">
                    <i class="simple-icon-magnifier"></i>
                </span>
            </div>
        </div>



        <div class="navbar-right">
            <div class="header-icons d-inline-block align-middle">
              <div class="d-none d-md-inline-block align-text-bottom mr-3">
                                  <div class="custom-switch custom-switch-primary-inverse custom-switch-small pl-1" data-toggle="tooltip" data-placement="left" title="Dark Mode">
                                      <input class="custom-switch-input" id="switchDark" type="checkbox" checked>
                                      <label class="custom-switch-btn" for="switchDark"></label>
                                  </div>
                              </div>

                <div class="position-relative d-inline-block">
                    <button class="header-icon btn btn-empty" type="button" id="notificationButton"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="simple-icon-bell"></i>
                        <span class="count"><?php echo count($this->notification); ?></span>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right mt-3 scroll position-absolute"
                        id="notificationDropdown">

                        <?php

                        foreach ($this->notification as $row):
                            ?>
                        <div class="d-flex flex-row mb-3 pb-3 border-bottom">
                            <a href="<?php echo base_url() . $row['action_url']; ?>">
                                <img src="<?php echo base_url(); ?>media/employee/<?php echo $row['photo_location']; ?>" alt="Notification Image"
                                    class="img-thumbnail list-thumbnail xsmall border-0 rounded-circle" />
                            </a>
                            <div class="pl-3">
                                <a href="<?php echo base_url(); ?>admin_logins/readNotification/<?php echo $row['id']; ?>">
                                    <p class="font-weight-medium mb-1"><?php echo $row['message']; ?></p>
                                    <p class="text-muted mb-0 text-small"><?php echo $row['log_time']; ?></p>
                                </a>
                            </div>
                        </div>

                          <?php endforeach; ?>

                    </div>
                </div>

                <button class="header-icon btn btn-empty d-none d-sm-inline-block" type="button" id="fullScreenButton">
                    <i class="simple-icon-size-fullscreen"></i>
                    <i class="simple-icon-size-actual"></i>
                </button>

            </div>


            <div class="user d-inline-block">
                <button class="btn btn-empty p-0" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    <span class="name">
						          <?php
                        $session_user = $this->session->userdata('user_info');
                        echo $session_user[0]->name;
                        ?>
					            </span>
                    <span>
                      <img alt="Profile Picture" src="<?php echo base_url(); ?>media/employee/<?php $module_short_name=  $this->session->userdata('module_short_name');
                      echo $session_user[0]->photo_location; ?>"

                    </span>
                </button>

                <div class="dropdown-menu dropdown-menu-right mt-3">


                    <a class="dropdown-item" href="<?php echo base_url(); ?><?php $module_short_name=  $this->session->userdata('module_short_name');  ?>login/change_password/<?php echo $session_user[0]->id; ?>">Change Password</a>
                        <a class="dropdown-item" href="<?php echo base_url(); ?><?php $module_short_name=  $this->session->userdata('module_short_name'); ?>employees/myprofile/">My Profile</a>
                    <a class="dropdown-item" href="<?php echo base_url(); ?><?php $module_short_name=  $this->session->userdata('module_short_name'); ?>login/logout">Sign out</a>


                    <!-- <a class="dropdown-item" href="<?php echo base_url(); ?>login/change_password/<?php echo $session_user[0]->id; ?>">Change Password</a>
                    <a class="dropdown-item" href="<?php echo base_url(); ?>login/logout">Sign out</a>
                   -->
                </div>
            </div>
        </div>
    </nav>

    <div class="sidebar">
        <?php echo $main_menu; ?>
    </div>

    <main>
      <div class="container-fluid">
            <div class="row  ">
                <div class="col-12">
                  <div class="mb-2">
                        <h1><?php echo strtoupper($this->uri->segment(1)); ?></h1>
                        <?php
                           if (isset($is_show_button) && $is_show_button != "") {
                               if ($is_show_button == "add") {
                                   $btn_text = '<button type="button" class="btn btn-primary btn-lg top-right-button mr-1">ADD NEW</button>';
                               } else {
                                   $btn_text = '<button type="button" class="btn btn-light btn-lg top-right-button mr-1">BACK TO LIST</button>';
                               } ?>
                          <div class="text-zero top-right-button-container">
                              <a href="<?php echo base_url().$this->uri->segment(1).'/'.$is_show_button; ?>">
                              <?php echo $btn_text; ?>
                              </a>
                          </div>
                        <?php
                           } ?>
                        <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                            <ol class="breadcrumb pt-0">
                                <li class="breadcrumb-item">
                                    <a href="#"><?php echo strtoupper($this->uri->segment(2)); ?></a>
                                </li>

                                <li class="breadcrumb-item active" aria-current="page"><?php echo $title; ?></li>
                            </ol>
                        </nav>
                        <?php
                            $message = $this->session->userdata('message');
                            if ($message != '') {
                                ?>
                                <div class="alert alert-success rounded" role="alert">
                                                <?php
                                                echo $message;
                                $this->session->unset_userdata('message'); ?>
                                  </div>
                            <?php
                            }
                            ?>

                            <?php
                            $exception = $this->session->userdata('exception');
                            if ($exception != '') {
                                ?>
                                 <div class="alert alert-danger rounded" role="alert">
                                                <?php
                                                echo $exception;
                                $this->session->unset_userdata('exception'); ?>
                                     </div>
                            <?php
                            }
                            ?>

                    </div>
                    <div class="separator mb-5"></div>
                </div>
          </div>
          <?php
            echo $maincontent;
          ?>
      </div>
    </main>


    <script src="<?php echo base_url(); ?>media/admin_panel/js/vendor/jquery-3.3.1.min.js"></script>
    <script src="<?php echo base_url(); ?>media/admin_panel/js/vendor/bootstrap.bundle.min.js"></script>
    <script src="<?php echo base_url(); ?>media/admin_panel/js/vendor/Chart.bundle.min.js"></script>
    <script src="<?php echo base_url(); ?>media/admin_panel/js/vendor/chartjs-plugin-datalabels.js"></script>
    <script src="<?php echo base_url(); ?>media/admin_panel/js/vendor/moment.min.js"></script>
    <script src="<?php echo base_url(); ?>media/admin_panel/js/vendor/fullcalendar.min.js"></script>
    <script src="<?php echo base_url(); ?>media/admin_panel/js/vendor/datatables.min.js"></script>
    <script src="<?php echo base_url(); ?>media/admin_panel/js/vendor/perfect-scrollbar.min.js"></script>
    <script src="<?php echo base_url(); ?>media/admin_panel/js/vendor/owl.carousel.min.js"></script>
    <script src="<?php echo base_url(); ?>media/admin_panel/js/vendor/progressbar.min.js"></script>
    <script src="<?php echo base_url(); ?>media/admin_panel/js/vendor/jquery.barrating.min.js"></script>
    <script src="<?php echo base_url(); ?>media/admin_panel/js/vendor/select2.full.js"></script>
    <script src="<?php echo base_url(); ?>media/admin_panel/js/vendor/nouislider.min.js"></script>
    <script src="<?php echo base_url(); ?>media/admin_panel/js/vendor/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url(); ?>media/admin_panel/js/vendor/Sortable.js"></script>
    <script src="<?php echo base_url(); ?>media/admin_panel/js/vendor/mousetrap.min.js"></script>
	<script src="<?php echo base_url(); ?>media/admin_panel/js/vendor/ckeditor5-build-classic/ckeditor.js"></script>
    <script src="<?php echo base_url(); ?>media/admin_panel/js/dore.script.js"></script>
    <script src="<?php echo base_url(); ?>media/admin_panel/js/scripts.js"></script>


</body>

</html>
