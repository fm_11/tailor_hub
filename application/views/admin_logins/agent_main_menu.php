<?php
$controller = $this->uri->segment(1);
?>
<div class="main-menu">
		<div class="scroll">
			<ul class="list-unstyled">
				<li <?php if ($controller == 'agent_dashboard') { ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>agent_dashboard/index">
						<i class="iconsminds-shop-4"></i>
						<span>Agent Dashboard</span>
					</a>
				</li>

				<li <?php if ($controller == 'users' || $controller == 'user_roles' || $controller == 'user_role_wise_privileges') { ?>class="active" <?php } ?>>
					<a href="#Applicants">
						<i class="iconsminds-bucket"></i> Applicants
					</a>
				</li>
			</ul>
		</div>
	</div>

	<div class="sub-menu">
		<div class="scroll">

			<ul class="list-unstyled" data-link="Applicants">
				<li>
					<a href="<?php echo base_url(); ?>reference_applicants/index">
						<i class="simple-icon-calculator"></i> <span class="d-inline-block">Applicants</span>
					</a>
				</li>
				<li>
				<a href="<?php echo base_url(); ?>reference_applicants/student_registration">
					<i class="simple-icon-calculator"></i> <span class="d-inline-block">Registration</span>
				</a>
			</li>
			<li>
			<a href="<?php echo base_url(); ?>agent_login/contact">
				<i class="simple-icon-calculator"></i> <span class="d-inline-block">Contact</span>
			</a>
		</li>
			</ul>
		</div>
	</div>
