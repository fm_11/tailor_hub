<div class="row">
            <div class="col-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <h5 class="mb-4"><?php echo $title; ?></h5>

                            <form action="<?php echo base_url(); ?>org_infos/update_contact_info/" method="post" enctype="multipart/form-data">

                                <div class="form-group">
                                    <label for="inputEmail4">Company Name</label>
                                    <input type="text" class="form-control" id="txtOrgName" name="txtOrgName" value="<?php echo $contact_info->org_name;  ?>" placeholder="Company Name">
                                    <input type="hidden" name="id"  class="smallInput wide" value="<?php echo $contact_info->id;  ?>">
                                </div>


                                <div class="form-row">
                                  <div class="form-group col-md-6">
                                      <label for="txtMobile">Mobile</label>
                                      <input type="text" class="form-control" name="txtMobile" value="<?php echo $contact_info->mobile;  ?>" id="txtMobile">
                                  </div>
                                    <div class="form-group col-md-6">
                                        <label for="inputPassword4">Email</label>
                                        <input type="text" class="form-control" id="inputEmail"
                                            name="txtEmail" value="<?php echo $contact_info->email;  ?>">
                                    </div>
                                </div>


                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="txtMobile">Website Address</label>
                                        <input type="text" class="form-control" name="txtWebAddress" value="<?php echo $contact_info->web_address;  ?>" id="txtWebAddress">
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="txtMobile">Facebook Address</label>
                                        <input type="text" class="form-control" name="txtFacebookAddress" value="<?php echo $contact_info->facebook_address;  ?>" id="txtFacebookAddress">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputAddress">Address</label>
                                    <input type="text" class="form-control" id="inputAddress" name="txtAddress"
                                        value="<?php echo $contact_info->address;  ?>">

                                </div>

                                <button type="submit" class="btn btn-primary d-block mt-3">Update</button>
                    </form>
            </div>
</div>
</div>
</div>
