<?php
$controller = $this->uri->segment(1);
?>
<div class="main-menu">
		<div class="scroll">
			<ul class="list-unstyled">
				<li <?php if($controller == 'students_dashboard'){ ?>class="active" <?php } ?>>
					<a href="<?php echo base_url(); ?>students_dashboard/index">
						<i class="iconsminds-shop-4"></i>
						<span>Student Dashboard</span>
					</a>
				</li>

				<li <?php if($controller == 'users' || $controller == 'user_roles' || $controller == 'user_role_wise_privileges'){ ?>class="active" <?php } ?>>
					<a href="#Student">
						<i class="iconsminds-bucket"></i> Student
					</a>
				</li>

				</li>
			</ul>
		</div>
	</div>

	<div class="sub-menu">
		<div class="scroll">

			<ul class="list-unstyled" data-link="Student">
				<li>
					<a href="<?php echo base_url(); ?>students_dashboard/myprofile">
						<i class="simple-icon-calculator"></i> <span class="d-inline-block">My Profile</span>
					</a>
				</li>
				<li>
					<a href="<?php echo base_url(); ?>students_dashboard/contact">
						<i class="simple-icon-calculator"></i> <span class="d-inline-block">Contact</span>
					</a>
				</li>
				<li>
					<a href="<?php echo base_url(); ?>students_dashboard/agent_details">
						<i class="simple-icon-calculator"></i> <span class="d-inline-block">Agent</span>
					</a>
				</li>
				<li>
					<a href="<?php echo base_url(); ?>students_dashboard/applylist">
						<i class="simple-icon-calculator"></i> <span class="d-inline-block">Apply List</span>
					</a>
				</li>
				<li>
					<a href="<?php echo base_url(); ?>students_dashboard/apply">
						<i class="simple-icon-calculator"></i> <span class="d-inline-block">Apply</span>
					</a>
				</li>
			</ul>
		</div>
	</div>
