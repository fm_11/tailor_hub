<form name="updateForm" action="<?php echo base_url(); ?>admin_logins/timekeeping_config" method="post" enctype="multipart/form-data">
<table>
    <tr>
        <td valign="top"scope="col">
            <table ellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
                <tbody>
                <tr>
                    <th  style="text-align: left;">
                        Expected Login Time
                        <input type="text" name="expected_login_time"  class="smallInput wide" value="<?php echo $config_info->expected_login_time;  ?>">
                        <input type="hidden" name="id"  class="smallInput wide" value="<?php echo $config_info->id;  ?>">
                    </th>
                </tr>

                <tr>
                    <th  style="text-align: left;">
                        Expected Logout Time
                        <input type="text" name="expected_logout_time"  class="smallInput wide" value="<?php echo $config_info->expected_logout_time;  ?>">
                    </th>
                </tr>

                <tr>
                    <th>
                        <input type="submit" class="submit" value="Update">
                    </th>
                </tr>

                </tbody>
            </table>
        </td>
    </tr>
</table>
</form>
