<div class="row">
            <div class="col-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <h5 class="mb-4"><?php echo $title; ?> <small style="color: red;">All Red marked are required</small></h5>

                            <form action="<?php echo base_url(); ?>leave_types/edit" method="post" enctype="multipart/form-data">
                              <div class="form-group">
                                <label for="inputEmail4">Name<span style="color:red;">*</span></label>
                                <input type="text" class="form-control" id="name" required name="name" value="<?php echo $leave_type_info->name;  ?>" placeholder="">
                                  <input type="hidden" name="id"  class="smallInput wide" value="<?php echo $leave_type_info->id;  ?>">
                             </div>


                             <div class="form-group">
                               <label for="short_name">Short Name<span style="color:red;">*</span></label>
                               <input type="text" class="form-control" id="short_name" required name="short_name" value="<?php echo $leave_type_info->short_name;  ?>" placeholder="">
                             </div>

                             <div class="form-group">
                              <label for="allocated_days">Allocated Days<span style="color:red;">*</span></label>
                              <input type="text" class="form-control" id="allocated_days" required name="allocated_days" value="<?php echo $leave_type_info->allocated_days;  ?>" placeholder="">
                             </div>

                                <button type="submit" class="btn btn-primary d-block mt-3">Submit</button>
                    </form>
            </div>
</div>
</div>
</div>
