
<div class="col-12 mb-4">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title"><?php echo $title; ?></h5>

                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Name</th>
                                        <th scope="col">Short Name</th>
                                        <th scope="col">Allocated Days</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  <?php
                                  $i = 0;
                                  foreach ($leave_types as $row):
                                      $i++;
                                      ?>
                                    <tr>
                                        <th scope="row"><?php echo $i; ?></th>
                                        <td><?php echo $row['name']; ?></td>
                                        <td><?php echo $row['short_name']; ?></td>
                                        <td><?php echo $row['allocated_days']; ?></td>
                                        <td>

                                               <a href="<?php echo base_url(); ?>leave_types/edit/<?php echo $row['id']; ?>"
                                                  title="Edit">
                                                  <button type="button" class="btn btn-primary btn-xs mb-1">
                                                    Edit
                                                  </button>
                                                </a>

                                               <a href="<?php echo base_url(); ?>leave_types/delete/<?php echo $row['id']; ?>"
                                                  onclick="return deleteConfirm()" title="Delete">
                                                  <button type="button" class="btn btn-primary btn-xs mb-1">
                                                    Delete
                                                  </button>

                                                </a>

                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
