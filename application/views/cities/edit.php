
<div class="row">
    <div class="col-12">
        <div class="card mb-4">
            <div class="card-body">
                <h5 class="mb-4"><?php echo $title; ?> <small style="color: red;">All Red marked are required</small></h5>

                <form action="<?php echo base_url(); ?>cities/edit" method="post" enctype="multipart/form-data">

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputEmail4">Country<span style="color:red;">*</span></label>
                            <select class="form-control select2-single" required name="country_id">
                                <option value="">--Select--</option>
                                <?php

                                if (count($countries)) {
                                    foreach ($countries as $list) {

                                        ?>
                                        <option
                                            <?php if($list['id'] == $city->country_id){ echo 'selected'; } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">City<span style="color:red;">*</span></label>
                            <input type="text" class="form-control" id="name"
                                   name="name" required value="<?php echo $city->name;?>">
                            <input type="hidden" class="form-control" id="id"
                                   name="id" required value="<?php echo $city->id;?>">
                        </div>

                    </div>



                    <button type="submit" class="btn btn-primary d-block mt-3">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
