<hr>

<?php
if(isset($excel)){
  header('Content-type: application/excel');
  $filename = 'Movement Report.xls';
  header('Content-Disposition: attachment; filename='.$filename);
}
?>

<table class="table">
    <thead>
      <tr>
          <th  class="text-center" colspan="6" scope="col">
            <h3>
               <?php echo $org_info['org_name']; ?>
            </h3>
            <p><?php echo $org_info['address']; ?></p>
            <p><?php echo $org_info['email']; ?></p>
            <p>Movement Report date between<?php echo $from_date .' to '. $to_date; ?></p>

          </th>
      </tr>

      <tr>
            <th scope="col">#</th>
            <th scope="col">Date</th>
            <th scope="col">Start Time</th>
            <th scope="col">End Time</th>
            <th scope="col">Location</th>
            <th scope="col">Purpose</th>
      </tr>
    </thead>
    <tbody>

      <?php
      $i = 0;
      foreach ($rData as $row):
          $i++;
          ?>
        <tr>
            <th scope="row"><?php echo $i; ?></th>
            <td><?php echo $row['date']; ?></td>
            <td><?php echo date('h:i:s A', strtotime($row['start_time'])); ?></td>
            <td>
              <?php
                 if($row['end_time'] != ''){
                   echo date('h:i:s A', strtotime($row['end_time']));
                 }else{
                   echo '-';
                 }
             ?>
           </td>
            <td><?php echo $row['location']; ?></td>
            <td><?php echo $row['purpose']; ?></td>
        </tr>

      <?php endforeach; ?>
    </tbody>
</table>
