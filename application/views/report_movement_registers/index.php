<div class="row">
            <div class="col-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <h5 class="mb-4"><?php echo $title; ?></h5>

                            <form action="<?php echo base_url(); ?>report_movement_registers/index" method="post" enctype="multipart/form-data">
                                <div class="form-row">

                                  <div class="form-group col-md-4">
                                        <label for="Employee">Employee</label>
                                        <select class="form-control select2-single" id="leave_employee_id" name="employee_id" required>
                                            <option value="">--Select--</option>
                                            <?php
                                            foreach ($employees as $row):
                                            ?>
                                               <option value="<?php echo $row['id']; ?>"
                                                 <?php if(isset($employee_id)){if($employee_id == $row['id']){echo 'selected';}} ?>><?php echo $row['name'].' (' . $row['code'] .')'; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                  </div>

                                  <div class="form-group col-md-4">
                                        <label for="Branch">From Date</label>
                                        <div class="input-group date">
                                           <input type="text" name="from_date" value="<?php if(isset($from_date)){ echo $from_date; } ?>" required class="form-control">
                                           <span class="input-group-text input-group-append input-group-addon">
                                               <i class="simple-icon-calendar"></i>
                                           </span>
                                       </div>
                                  </div>

                                  <div class="form-group col-md-4">
                                        <label for="Branch">To Date</label>
                                        <div class="input-group date">
                                           <input type="text" value="<?php if(isset($to_date)){ echo $to_date; } ?>" name="to_date" required class="form-control">
                                           <span class="input-group-text input-group-append input-group-addon">
                                               <i class="simple-icon-calendar"></i>
                                           </span>
                                       </div>
                                  </div>
                                </div>

                              <div class="btn-group">
                                    <input type="submit" value="Report View" name="view" class="btn btn-primary d-block mt-3">
                                    <input type="submit" name="excel" value="Download Excel" class="btn btn-outline-dark mt-3">
                              </div>
                    </form>

                   <?php if(isset($rData)){ echo $report; } ?>
               </div>
          </div>
      </div>
</div>
