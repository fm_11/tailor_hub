<div class="row">
    <div class="col-12">
        <div class="card mb-4">
            <div class="card-body">
                <h5 class="mb-4"><?php echo $title; ?> <small style="color: red;">All Red marked are required</small></h5>

                <form action="<?php echo base_url(); ?>Intake_months/edit" method="post" enctype="multipart/form-data">
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="inputEmail4">Name<span style="color:red;">*</span></label>
                            <input type="text" class="form-control" id="name" required name="name" value="<?php echo $intake_months->name;  ?>" placeholder="Name">
                            <input type="hidden" name="id"  class="smallInput wide" value="<?php echo $intake_months->id;  ?>">
                        </div>
                        <!-- <div class="form-group col-md-6">
                            <label for="inputPassword4">Code</label>
                            <input type="text" class="form-control" id="code" required
                                   name="code" value="<?php echo $intake_months->code;  ?>">
                        </div> -->
                    </div>
                    <div class="form-row">
                      <div class="form-group col-md-6">
                          <label class="checkbox-inline"></label>
                          <input name="is_active" <?php if ($intake_months->is_active == '1') {
                                  echo 'checked';
                              } ?> type="checkbox" value="1">   Is Active?
                      </div>
                    </div>
                    <button type="submit" class="btn btn-primary d-block mt-3">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
