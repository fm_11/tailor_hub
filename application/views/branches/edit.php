<div class="row">
            <div class="col-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <h5 class="mb-4"><?php echo $title; ?> <small style="color: red;">All Red marked are required</small></h5>

                            <form action="<?php echo base_url(); ?>branches/edit" method="post" enctype="multipart/form-data">
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                      <label for="inputEmail4">Name<span style="color:red;">*</span></label>
                                      <input type="text" class="form-control" id="name" required name="name" value="<?php echo $branch_info->name;  ?>" placeholder="Branch Name">
                                        <input type="hidden" name="id"  class="smallInput wide" value="<?php echo $branch_info->id;  ?>">
                                   </div>
                                      <div class="form-group col-md-6">
                                          <label for="inputPassword4">Code<span style="color:red;">*</span></label>
                                          <input type="text" class="form-control" id="code" required
                                              name="code" value="<?php echo $branch_info->code;  ?>">
                                      </div>
                                  </div>

                              <div class="form-row">
                                  <div class="form-group col-md-6">
                                      <label for="txtMobile">Mobile<span style="color:red;">*</span></label>
                                      <input type="text" class="form-control" name="mobile" value="<?php echo $branch_info->mobile;  ?>" required id="mobile">
                                  </div>
                                    <div class="form-group col-md-6">
                                        <label for="inputPassword4">Email<span style="color:red;">*</span></label>
                                        <input type="text" class="form-control" id="email" required
                                            name="email" value="<?php echo $branch_info->email;  ?>">
                                    </div>
                                </div>


                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="Country">Country<span style="color:red;">*</span></label>
                                        <select class="form-control select2-single"  name="country_id" required>
                                            <option value="">--Select--</option>
                                            <?php
                                            foreach ($country as $row):
                                            ?>
                                               <option value="<?php echo $row['id']; ?>" <?php if($row['id'] == $branch_info->country_id){echo 'selected';} ?>><?php echo $row['name']; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>

                                    <div class="form-group col-md-6">
                                          <label for="Timezone">Timezone<span style="color:red;">*</span></label>
                                          <select class="form-control select2-single" name="time_zone_id" required>
                                              <option value="">--Select--</option>
                                              <?php
                                              foreach ($timezones as $row):
                                              ?>
                                                 <option value="<?php echo $row['id']; ?>" <?php if($row['id'] == $branch_info->time_zone_id){echo 'selected';} ?>><?php echo $row['name']; ?></option>
                                              <?php endforeach; ?>
                                          </select>
                                      </div>
                                </div>

                                <div class="form-row">
                                  <div class="form-group col-md-4">
                                    <label for="exTime">Branch Shift<span style="color:red;">*</span></label>
                                    <select class="form-control select2-single" name="shift_id" required>
                                        <option value="">--Select--</option>
                                        <?php
                                        foreach ($shifts as $row):
                                        ?>
                                           <option value="<?php echo $row['id']; ?>" <?php if($row['id'] == $branch_info->shift_id){echo 'selected';} ?>><?php echo $row['name']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                  </div>
                                  <div class="form-group col-md-8">
                                    <label for="inputAddress">Address<span style="color:red;">*</span></label>
                                    <input type="text" class="form-control" required id="address" name="address"
                                        value="<?php echo $branch_info->address;  ?>">
                                  </div>
                                </div>

                                <button type="submit" class="btn btn-primary d-block mt-3">Submit</button>
                    </form>
            </div>
</div>
</div>
</div>
