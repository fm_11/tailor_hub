<div class="row">
            <div class="col-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <h5 class="mb-4"><?php echo $title; ?> <small style="color: red;">All Red marked are required</small></h5>

                            <form action="<?php echo base_url(); ?>holidays/batch_add" method="post" class="needs-validation" novalidate enctype="multipart/form-data">

                                  <div class="form-row">
                                      <div class="form-group col-md-4">
                                          <label for="branches">Branch<span style="color:red;">*</span></label>
                                          <select class="form-control select2-single"  name="branch_id" required>
                                              <option value="">--Select--</option>
                                              <?php

                                              if (count($branches)) {
                                                  foreach ($branches as $list) {

                                                      ?>
                                                      <option
                                                          value="<?php echo $list['id']; ?>"  <?php if(isset($branch_id)){ if($branch_id == $list['id']){echo 'selected';} } ?>><?php echo $list['name']; ?></option>
                                                  <?php
                                                  }
                                              }
                                              ?>
                                          </select>
                                      </div>

                                      <div class="form-group col-md-4">
                                          <label for="branches">Month<span style="color:red;">*</span></label>
                                          <select class="form-control select2-single"  name="month" required>
                                              <option value="">--Select--</option>
                                              <?php
                                              if(isset($month)){
                                                $month = $month;
                                              }else{
                                                $month = date('m');
                                              }
                                    					$i = 1;
                                    					while ($i <= 12) {
                                    						$dateObj = DateTime::createFromFormat('!m', $i);
                                    						?>
                                    						<option value="<?php echo $i; ?>" <?php if(isset($month)){ if($month == $i){ echo 'selected'; }} ?>><?php echo $dateObj->format('F'); ?></option>
                                    						<?php
                                    						$i++;
                                    					}
                                    					?>
                                          </select>
                                      </div>

                                      <div class="form-group col-md-4">
                                          <label for="branches">Year<span style="color:red;">*</span></label>
                                          <select class="form-control select2-single"  name="year" required>
                                            <option value="<?php echo date('Y') - 1; ?>"><?php echo date('Y') - 1; ?></option>
                                            <option value="<?php echo date('Y'); ?>" selected><?php echo date('Y'); ?></option>
                                            <option value="<?php echo date('Y') + 1; ?>"><?php echo date('Y') + 1; ?></option>
                                          </select>
                                      </div>
                                  </div>


                                <button type="submit" class="btn btn-primary d-block mt-3">Process</button>
                    </form>

                    <?php
                      if (isset($holidays)) {
                          echo $process_table;
                      }
                    ?>
            </div>
       </div>
      </div>
</div>
