
<div class="col-12 mb-4">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">
                              <?php echo $title; ?>

                              <a href="<?php echo base_url(); ?>holidays/batch_add" title="Batch Add">
                                 <button type="button" class="btn btn-outline-secondary btn-xs mb-1">
                                   Batch Add
                                 </button>
                               </a>


                            </h5>

                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Branch</th>
                                        <th scope="col">Date</th>
                                        <th scope="col">Holiday Type</th>
                                        <th scope="col">Reason</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  <?php
                                  $i = (int)$this->uri->segment(3);
                                  foreach ($holidays as $row):
                                      $i++;
                                      ?>
                                    <tr>
                                        <th scope="row"><?php echo $i; ?></th>
                                        <td><?php echo $row['branch_name']; ?></td>
                                        <td><?php echo $row['date'] . ' (' . date('l', strtotime($row['date'])) . ')'; ?></td>
                                        <td><?php echo $row['holiday_name']; ?></td>
                                        <td><?php echo $row['remarks']; ?></td>
                                        <td>

                                               <a href="<?php echo base_url(); ?>holidays/edit/<?php echo $row['id']; ?>"
                                                  title="Edit">
                                                  <button type="button" class="btn btn-primary btn-xs mb-1">
                                                    Edit
                                                  </button>
                                                </a>

                                               <a href="<?php echo base_url(); ?>holidays/delete/<?php echo $row['id']; ?>"
                                                  onclick="return deleteConfirm()" title="Delete">
                                                  <button type="button" class="btn btn-primary btn-xs mb-1">
                                                    Delete
                                                  </button>

                                                </a>

                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                              <?php echo $this->pagination->create_links(); ?>
                        </div>
                    </div>
                </div>
