<br>

<form action="<?php echo base_url(); ?>holidays/batch_add_data_save" method="post" enctype="multipart/form-data">
<table class="table table-striped">
    <thead>
        <tr>
            <th scope="col"><input type="checkbox" onchange="checkAll(this)"></th>
            <th scope="col">Date</th>
            <th scope="col">Holiday Type</th>
        </tr>
    </thead>
    <tbody>
      <?php
              $i = 1;
              while($i <= $num_of_days){
              ?>

                  <tr>
                      <td>
                          <?php
                          $date = $year. '-'. sprintf("%02d", $month) . '-' . sprintf("%02d", $i);
                          ?>
                          <input type="checkbox" <?php if (in_array($date, array_column($holidays, 'date'))) {
                              echo 'checked';
                          } ?> name="is_allow_<?php echo $i; ?>">
                      </td>
                      <td>
                          <?php
                              echo $date . ' (' . date('l', strtotime($date)) . ')';
                          ?>
                          <input type="hidden" class="input-text-short" size="8"
                                 style="text-align: right; width:75px;"
                                 name="date_<?php echo $i; ?>" value="<?php echo $date; ?>"/>
                      </td>
                      <td>
                          <?php
                           $holiday_type_id = "";
                            if (in_array($date, array_column($holidays, 'date'))) {
                                 $key = array_search($date, array_column($holidays, 'date'));
                                 $holiday_type_id = $holidays[$key]['holiday_type'];
                            }else{
                                $holiday_type_id = 3;
                            }
                          ?>
                          <select class="form-control  col-md-6" name="txtHolidayType_<?php echo $i; ?>">
                              <option value="">-- Please Select --</option>
                              <?php
                              if (count($holiday_types)) {
                                  foreach ($holiday_types as $list) {
                                      ?>
                                      <option value="<?php echo $list['id']; ?>"
                                      <?php if($holiday_type_id != ""){if($list['id'] == $holiday_type_id) {
                                           echo 'selected="selected"';
                                        }} ?>><?php echo $list['name']; ?></option>
                                  <?php
                                  }
                              }
                              ?>
                          </select>
                      </td>

                  </tr>
              <?php  $i++; } ?>
    </tbody>
</table>
<input type="hidden" class="input-text-short" name="branch_id"
          value="<?php echo $branch_id; ?>"/>
<input type="hidden" class="input-text-short" name="year"
          value="<?php echo $year; ?>"/>
<input type="hidden" class="input-text-short" name="month"
          value="<?php echo $month; ?>"/>
<input type="hidden" class="input-text-short" name="num_of_days"
          value="<?php echo $num_of_days; ?>"/>
<button type="submit" class="btn btn-primary d-block mt-3">Save Data</button>
</form>
