<div class="row">
            <div class="col-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <h5 class="mb-4"><?php echo $title; ?> <small style="color: red;">All Red marked are required</small></h5>

                            <form action="<?php echo base_url(); ?>holidays/edit" method="post" enctype="multipart/form-data">

                                  <div class="form-group">
                                      <label for="txtBranch">Branch<span style="color:red;">*</span></label>
                                      <select class="form-control select2-single"  name="txtBranch" required>
                                          <option value="">--Select--</option>
                                          <?php

                                          if (count($branches)) {
                                              foreach ($branches as $list) {

                                                  ?>
                                                  <option
                                                      value="<?php echo $list['id']; ?>" <?php if($holiday_info[0]['branch_id'] == $list['id']){echo 'selected';} ?>><?php echo $list['name']; ?></option>
                                              <?php
                                              }
                                          }
                                          ?>
                                      </select>
                                  </div>

                                  <div class="form-group">
                                      <label for="txtHolidayType">Holiday Type<span style="color:red;">*</span></label>
                                      <select class="form-control select2-single"  name="txtHolidayType" required>
                                          <option value="">--Select--</option>
                                          <?php

                                          if (count($holiday_types)) {
                                              foreach ($holiday_types as $list) {

                                                  ?>
                                                  <option
                                                      value="<?php echo $list['id']; ?>" <?php if($holiday_info[0]['holiday_type'] == $list['id']){echo 'selected';} ?>><?php echo $list['name']; ?></option>
                                              <?php
                                              }
                                          }
                                          ?>
                                      </select>
                                  </div>

                                  <div class="form-group">
                                          <label for="Timezone">Date<span style="color:red;">*</span></label>
                                          <div class="input-group date">
                                             <input type="text" name="txtDate" value="<?php echo $holiday_info[0]['date']; ?>" required class="form-control">
                                             <span class="input-group-text input-group-append input-group-addon">
                                                 <i class="simple-icon-calendar"></i>
                                             </span>
                                         </div>
                                    </div>


                             <div class="form-group">
                               <label for="inputtxtRemarks">Remarks<span style="color:red;">*</span></label>
                               <input type="hidden" class="smallInput wide" value="<?php echo $holiday_info[0]['id']; ?>" name="id" id="id" required="1"/>
                               <textarea class="form-control" id="txtRemarks" required name="txtRemarks" ><?php echo $holiday_info[0]['remarks']; ?></textarea>
                            </div>

                                <button type="submit" class="btn btn-primary d-block mt-3">Update</button>
                    </form>
            </div>
</div>
</div>
</div>
