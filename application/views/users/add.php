<div class="row">
            <div class="col-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <h5 class="mb-4"><?php echo $title; ?></h5>

                            <form action="<?php echo base_url(); ?>users/add" method="post" enctype="multipart/form-data">

                              <div class="form-row">
                                  <div class="form-group col-md-6">
                                      <label for="Country">Employee</label>
                                      <select class="form-control select2-single"  name="employee_id" required>
                                          <option value="">--Select--</option>
                                          <?php
                                          foreach ($employees as $row):
                                          ?>
                                             <option value="<?php echo $row['id']; ?>"><?php echo $row['name'].' ('. $row['code'] . ')'; ?></option>
                                          <?php endforeach; ?>
                                      </select>
                                  </div>

                                  <div class="form-group col-md-6">
                                        <label for="Timezone">User Name</label>
                                        <input type="text" class="form-control" id="user_name" required name="user_name" value="" placeholder="User Name">
                                    </div>
                              </div>



                              <div class="form-row">
                                    <div class="form-group col-md-6">
                                      <label for="inputEmail4">Password</label>
                                      <input type="password" class="form-control" id="user_password" required name="user_password" value="">
                                   </div>
                                      <div class="form-group col-md-6">
                                          <label for="inputPassword4">Role</label>
                                          <select class="form-control select2-single"  name="role_id" required>
                                              <option value="">--Select--</option>
                                              <?php
                                              foreach ($roles as $row):
                                              ?>
                                                 <option value="<?php echo $row['id']; ?>"><?php echo $row['role_name']; ?></option>
                                              <?php endforeach; ?>
                                          </select>
                                      </div>
                              </div>


                              <div class="form-row">
                                <div class="form-group col-md-6">
                                  <label for="is_head_of_process">Is Head of Process ?</label>
                                  <select class="form-control select2-single"  name="is_head_of_process" required>
                                      <option value="0">No</option>
                                      <option value="1">Yes</option>
                                  </select>
                               </div>
                                   <div class="form-group col-md-6">
                                          <label for="is_head_of_admission">Is Head of Admission ?</label>
                                          <select class="form-control select2-single"  name="is_head_of_admission" required>
                                              <option value="0">No</option>
                                              <option value="1">Yes</option>
                                          </select>
                                      </div>
                              </div>

                                <button type="submit" class="btn btn-primary d-block mt-3">Submit</button>
                    </form>
            </div>
</div>
</div>
</div>
