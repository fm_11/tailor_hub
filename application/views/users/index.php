
<div class="col-12 mb-4">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title"><?php echo $title; ?></h5>

                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">User Name</th>
                                        <th scope="col">Employee</th>
                                        <th scope="col">Role</th>
                                        <th scope="col">Status</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  <?php
                                  $i = 0;
                                  foreach ($users as $row):
                                      $i++;
                                      ?>
                                    <tr>
                                        <th scope="row"><?php echo $i; ?></th>
                                        <td><?php echo $row['user_name']; ?></td>
                                        <td><?php echo $row['employee_name']. ' ('. $row['employee_code'] . ')'; ?></td>
                                        <td><?php echo $row['role_name'];?></td>
                                        <td>
                                          <?php
                                            if($row['is_active'] == 1){
                                               echo 'Active';
                                            }else{
                                              echo 'Inactive';
                                            }
                                          ?>
                                        </td>
                                        <td>

                                          <a href="<?php echo base_url(); ?>users/edit/<?php echo $row['id']; ?>"
                                            title="Edit">
                                             <button type="button" class="btn btn-primary btn-xs mb-1">
                                               Edit
                                             </button>
                                           </a>

                                               <a href="<?php echo base_url(); ?>users/delete/<?php echo $row['id']; ?>"
                                                  onclick="return deleteConfirm()" title="Delete">
                                                  <button type="button" class="btn btn-primary btn-xs mb-1">
                                                    Delete
                                                  </button>
                                                </a>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
