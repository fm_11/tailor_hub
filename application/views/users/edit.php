<div class="row">
            <div class="col-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <h5 class="mb-4"><?php echo $title; ?></h5>

                            <form action="<?php echo base_url(); ?>users/edit" method="post" enctype="multipart/form-data">

                              <div class="form-row">
                                  <div class="form-group col-md-6">
                                      <label for="Country">Employee</label>
                                      <select class="form-control select2-single" name="employee_id" required>
                                          <option value="">--Select--</option>
                                          <?php
                                          foreach ($employees as $row):
                                          ?>
                                             <option value="<?php echo $row['id']; ?>" <?php if ($user_info[0]->employee_id == $row['id']) {
                                              echo 'selected';
                                          } ?>><?php echo $row['name'].' ('. $row['code'] . ')'; ?></option>
                                          <?php endforeach; ?>
                                      </select>
                                  </div>

                                  <div class="form-group col-md-6">
                                        <label for="Timezone">User Name</label>
                                        <input type="text" class="form-control" id="user_name" required name="user_name" readonly value="<?php echo $user_info[0]->user_name; ?>" placeholder="User Name">
                                        <input type="hidden" class="form-control" id="id" required name="id"  value="<?php echo $user_info[0]->id; ?>">
                                    </div>
                              </div>



                              <div class="form-row">
                                <div class="form-group col-md-6">
                                  <label for="inputEmail4">Password</label>
                                  <input type="password" class="form-control" id="user_password" name="user_password" placeholder="Not Required" value="">
                               </div>
                                  <div class="form-group col-md-6">
                                          <label for="Inputrole">Role</label>
                                          <select class="form-control select2-single"  name="role_id" required>
                                              <option value="">--Select--</option>
                                              <?php
                                              foreach ($roles as $row):
                                              ?>
                                                 <option value="<?php echo $row['id']; ?>" <?php if ($user_info[0]->user_role == $row['id']) {
                                                  echo 'selected';
                                              } ?>><?php echo $row['role_name']; ?></option>
                                              <?php endforeach; ?>
                                          </select>
                                      </div>
                              </div>

                              <div class="form-row">
                                <div class="form-group col-md-6">
                                  <label for="is_head_of_process">Is Head of Process ?</label>
                                  <select class="form-control select2-single"  name="is_head_of_process" required>
                                      <option value="0" <?php if ($user_info[0]->is_head_of_process == '0') {
                                                  echo 'selected';
                                              } ?>>No</option>
                                      <option value="1" <?php if ($user_info[0]->is_head_of_process == '1') {
                                                  echo 'selected';
                                              } ?>>Yes</option>
                                  </select>
                               </div>
                                   <div class="form-group col-md-6">
                                          <label for="is_head_of_admission">Is Head of Admission ?</label>
                                          <select class="form-control select2-single"  name="is_head_of_admission" required>
                                              <option value="0" <?php if ($user_info[0]->is_head_of_admission == '0') {
                                                  echo 'selected';
                                              } ?>>No</option>
                                              <option value="1" <?php if ($user_info[0]->is_head_of_admission == '1') {
                                                  echo 'selected';
                                              } ?>>Yes</option>
                                          </select>
                                      </div>
                              </div>


                                <button type="submit" class="btn btn-primary d-block mt-3">Update</button>
                    </form>
            </div>
</div>
</div>
</div>
