<hr>
<style>
.my-custom-scrollbar {
position: relative;
height: 500px;
overflow: auto;
}
.table-wrapper-scroll-y {
display: block;
}
</style>
<?php
if(isset($excel)){
  header('Content-type: application/excel');
  $filename = 'Employee Details Report.xls';
  header('Content-Disposition: attachment; filename='.$filename);
}
?>
<div class="table-wrapper-scroll-y my-custom-scrollbar">
<table class="table table-responsive">
    <thead>
      <tr>
          <th  class="text-center" colspan="<?php echo ($number_of_days + 4); ?>" scope="col">
            <h3>
               <?php echo $org_info['org_name']; ?>
            </h3>
            <p><?php echo $org_info['address']; ?></p>
            <p><?php echo $org_info['email']; ?></p>
            <p>Attendance Report for month of <?php echo date('F', mktime(0, 0, 0, $month, 10)). ', ' . $year; ?></p>
            <p>Branch: <?php echo $org_info['branch_name']; ?></p>
          </th>
      </tr>
      <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Employee ID</th>
            <th scope="col">Designation</th>
            <?php
    			   	$i = 1;
      				while($i <= $number_of_days){
      					if($i < 10){
      					   $clm_date = '0'.$i;
      					}else{
      					   $clm_date = $i;
      					}
      					echo '<th scope="col">&nbsp;'.$clm_date.'</th>';
      					$i++;
      				}
    				?>
            <th scope="col">Total Days</th>
            <th scope="col">Working Days</th>
            <th scope="col">Present Days</th>
            <th scope="col">Absent Days</th>
            <th scope="col">Leave Days</th>
            <th scope="col">Total Holiday</th>
      </tr>
    </thead>
    <tbody>

      <?php
      $i = 0;
      foreach ($rData as $row):
          $i++;
          ?>
        <tr>
            <th scope="row"><?php echo $i; ?></th>
            <td><?php echo $row['name']; ?></td>
            <td><?php echo $row['code']; ?></td>
            <td><?php echo $row['post_name']; ?></td>
            <?php echo $row['data']; ?>
            <td><?php echo $row['total_days']; ?></td>
            <td><?php echo $row['total_working_days']; ?></td>
            <td><?php echo $row['total_present_days']; ?></td>
            <td><?php echo $row['total_absent_days']; ?></td>
            <td><?php echo $row['total_leave_days']; ?></td>
            <td><?php echo $row['total_holidays']; ?></td>
        </tr>

      <?php endforeach; ?>
    </tbody>
</table>
</div>
