<div class="row">
            <div class="col-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <h5 class="mb-4"><?php echo $title; ?></h5>

                            <form action="<?php echo base_url(); ?>attendance_details/index" method="post" enctype="multipart/form-data">
                                <div class="form-row">
                                  <div class="form-group col-md-4">
                                      <label for="Branch">Branch</label>
                                      <select class="form-control select2-single"  name="branch_id">
                                          <option value="all">--All--</option>
                                          <?php
                                          foreach ($branches as $row):
                                          ?>
                                             <option value="<?php echo $row['id']; ?>" <?php if(isset($branch_id)){ if($row['id'] == $branch_id){ echo 'selected'; } } ?>><?php echo $row['name']; ?></option>
                                          <?php endforeach; ?>
                                      </select>
                                  </div>

                                  <div class="form-group col-md-4">
                                        <label for="Branch">Month</label>
                                        <select  class="form-control select2-single" name="month" id="month" required="1">
                                  					<option value="">-- Select --</option>
                                            <?php
                                              $monthArray = range(1, 12);
                                              foreach ($monthArray as $this_month) {
                                                  // padding the month with extra zero
                                                  $monthPadding = str_pad($this_month, 2, "0", STR_PAD_LEFT);
                                                  // you can use whatever year you want
                                                  // you can use 'M' or 'F' as per your month formatting preference
                                                  $fdate = date("F", strtotime("2015-$monthPadding-01"));
                                                ?>
                                                <option value="<?php echo $this_month; ?>" <?php if(isset($month)){if($month == $this_month){ echo 'selected'; }} ?>><?php echo $fdate; ?></option>
                                                <?php
                                              }
                                              ?>
                                      </select>
                                  </div>

                                  <div class="form-group col-md-4">
                                        <label for="Branch">Year</label>
                                        <select  class="form-control select2-single" name="year" required="1">
                                           <?php
                                            // set start and end year range
                                            $yearArray = range(2018, 2030);
                                            foreach ($yearArray as $this_year) {
                                                // if you want to select a particular year
                                                if(isset($year)){
                                                  $selected = ($this_year == $year) ? 'selected' : '';
                                                }else{
                                                  $selected = ($this_year == date('Y')) ? 'selected' : '';
                                                }
                                                echo '<option '.$selected.' value="'.$this_year.'">'.$this_year.'</option>';
                                            }
                                            ?>
                                			 </select>
                                  </div>
                                </div>

                              <div class="btn-group">
                                    <input type="submit" value="Report View" name="view" class="btn btn-primary d-block mt-3">
                                    <input type="submit" name="excel" value="Download Excel" class="btn btn-outline-dark mt-3">
                              </div>
                    </form>

                   <?php if(isset($rData)){ echo $report; } ?>
               </div>
          </div>
      </div>
</div>
