<div class="row">
            <div class="col-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <h5 class="mb-4"><?php echo $title; ?> <small style="color: red;">All Red marked are required</small></h5>

                            <form action="<?php echo base_url(); ?>shifts/add" method="post" enctype="multipart/form-data">
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                      <label for="inputEmail4">Name<span style="color:red;">*</span></label>
                                      <input type="text" class="form-control" id="name" required name="name" value="" placeholder="Shift Name">
                                   </div>
                                      <div class="form-group col-md-6">
                                          <label for="short_name">Short Name<span style="color:red;">*</span></label>
                                          <input type="text" class="form-control" id="short_name" required
                                              name="short_name" value="">
                                      </div>
                                  </div>


                                <div class="form-row">
                                  <div class="form-group col-md-4">
                                    <label for="start_time">Office Start Time<span style="color:red;">*</span></label>
                                    <input type="time" class="form-control" required id="start_time" name="start_time"
                                        value="">
                                  </div>
                                  <div class="form-group col-md-4">
                                    <label for="end_time">Office End Time<span style="color:red;">*</span></label>
                                    <input type="time" class="form-control" required id="end_time" name="end_time"
                                        value="">
                                  </div>
                                  <div class="form-group col-md-4">
                                    <label for="exTime">Flexiable Time for Late<span style="color:red;">*</span></label>
                                    <input type="number" class="form-control" required id="flexible_time_in" name="flexible_time_in"
                                        value="">
                                  </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputremarks">Remarks<span style="color:red;">*</span></label>
                                    <input type="text" class="form-control" required id="remarks" name="remarks"
                                        value="">
                                </div>

                                <button type="submit" class="btn btn-primary d-block mt-3">Submit</button>
                    </form>
            </div>
</div>
</div>
</div>
