<div class="col-12 mb-4">
    <div class="card">
        <div class="card-body">
            <h5 class="card-title"><?php echo $title; ?></h5>

            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <!-- <th scope="col">Code</th> -->
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $i = (int)$this->uri->segment(3);
                foreach ($course_types as $row):
                    $i++;
                    ?>
                    <tr>
                        <th scope="row"><?php echo $i; ?></th>
                        <td><?php echo $row['name']; ?></td>
                        <td><?php echo $row['code']; ?></td>

                        <td>

                            <a href="<?php echo base_url(); ?>course_types/edit/<?php echo $row['id']; ?>"
                               title="Edit">
                                <button type="button" class="btn btn-primary btn-xs mb-1">
                                    Edit
                                </button>
                            </a>

                            <a href="<?php echo base_url(); ?>course_types/delete/<?php echo $row['id']; ?>"
                               onclick="return deleteConfirm()" title="Delete">
                                <button type="button" class="btn btn-primary btn-xs mb-1">
                                    Delete
                                </button>

                            </a>

                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <?php echo $this->pagination->create_links(); ?>
        </div>
    </div>
</div>
