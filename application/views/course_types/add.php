<div class="row">
    <div class="col-12">
        <div class="card mb-4">
            <div class="card-body">
                <h5 class="mb-4"><?php echo $title; ?> <small style="color: red;">All Red marked are required</small></h5>

                <form action="<?php echo base_url(); ?>Course_types/add" method="post" enctype="multipart/form-data">
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="inputEmail4">Name<span style="color:red;">*</span></label>
                            <input type="text" class="form-control" id="name" required name="name" value="" placeholder="Name">
                        </div>
                        <!-- <div class="form-group col-md-6">
                            <label for="inputPassword4">Code</label>
                            <input type="text" class="form-control" id="code"
                                   name="code" value="">
                        </div> -->
                    </div>

                    <button type="submit" class="btn btn-primary d-block mt-3">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
