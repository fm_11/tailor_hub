<form name="addForm" class="cmxform" id="commentForm" action="<?php echo base_url(); ?>tailor_milestone/update" method="post" enctype="multipart/form-data">
  <div class="form-row">
    <div class="form-group col-md-3">
      <label>First Number</label>
      <input type="hidden" name="id"  class="form-control" value="<?php echo $milestone->id;  ?>">
      <input type="text" autocomplete="off"  name="first_number"  class="form-control" value="<?php echo $milestone->first_number;  ?>">
    </div>

    <div class="form-group col-md-3">
      <label>First Label</label>
      <input type="text" autocomplete="off"  name="first_label"  class="form-control" value="<?php echo $milestone->first_label;  ?>">
    </div>

    <div class="form-group col-md-3">
    <label>Second Number</label>
    <input type="text" autocomplete="off"  name="second_number"  class="form-control" value="<?php echo $milestone->second_number;  ?>">
  </div>

  <div class="form-group col-md-3">
    <label>Second Label</label>
    <input type="text" autocomplete="off"  name="second_label"  class="form-control" value="<?php echo $milestone->second_label;  ?>">
  </div>
  </div>

  <div class="form-row">
    <div class="form-group col-md-3">
      <label>Third Number</label>
      <input type="text" autocomplete="off"  name="third_number"  class="form-control" value="<?php echo $milestone->third_number;  ?>">
    </div>

    <div class="form-group col-md-3">
      <label>Third Label</label>
      <input type="text" autocomplete="off"  name="third_label"  class="form-control" value="<?php echo $milestone->third_label;  ?>">
    </div>

    <div class="form-group col-md-3">
    <label>Fourth Number</label>
    <input type="text" autocomplete="off"  name="fourth_number"  class="form-control" value="<?php echo $milestone->fourth_number;  ?>">
  </div>

  <div class="form-group col-md-3">
    <label>Fourth Label</label>
    <input type="text" autocomplete="off"  name="fourth_label"  class="form-control" value="<?php echo $milestone->fourth_label;  ?>">
  </div>
  </div>



  <div class="float-right">
     <input class="btn btn-primary" type="submit" value="Update">
  </div>

</form>
