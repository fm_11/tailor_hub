

<div class="row">
    <div class="col-12">
        <div class="card mb-4">
            <div class="card-body">
                <h5 class="mb-4"><?php echo $title; ?> <small style="color: red;">All Red marked are required</small></h5>

                <form action="<?php echo base_url(); ?>leads/add" method="post" enctype="multipart/form-data">
                    <div class="form-row">
                       <div class="form-group col-md-6">
                            <label for="studentFirstName">Student First Name<span style="color:red;">*</span></label>
                            <input type="text" class="form-control" id="first_name"
                                   name="first_name" required value="">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="studentLastName">Student Last Name</label>
                            <input type="text" class="form-control" id="last_name"
                                   name="last_name"  value="">
                        </div>

                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="contact">Contact<span style="color:red;">*</span></label>
                          <div class="row">
                            <div class="form-group col-md-4">
                              <select class="form-control select2-single"  name="country_code" required>
                                  <option value="">--Select--</option>
                                  <?php

                                  if (count($country_code)) {
                                      foreach ($country_code as $list) {
                                          ?>
                                          <option
                                              value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                          <?php
                                      }
                                  }
                                  ?>
                              </select>
                            </div>
                            <div class="form-group col-md-8" style="margin-left: -30px;">
                              <input type="text" class="form-control" id="phone"
                                     name="phone" required value="">
                            </div>
                          </div>
                        </div>


                        <div class="form-group col-md-6">
                            <label for="email">Email</label>
                            <input type="text" class="form-control" id="email"
                                   name="email"  value="">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="country">Interested Country</label>
                            <select class="form-control select2-single"  name="interested_country">
                                <option value="">--Select--</option>
                                <?php

                                if (count($countries)) {
                                    foreach ($countries as $list) {
                                        ?>
                                        <option
                                            value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="purpose">Purpose</label>
                            <input type="text" class="form-control" id="purpose"
                                   name="purpose"  value="">
                        </div>

                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="courseLevel">Course Level</label>
                            <select class="form-control select2-single"  name="course_level_id">
                                <option value="">--Select--</option>
                                <?php

                                if (count($course_level)) {
                                    foreach ($course_level as $list) {
                                        ?>
                                        <option
                                            value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="subject">Subject</label>
                            <input type="text" class="form-control" id="course_subject"
                                   name="course_subject"  value="">
                        </div>

                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="originOffice">Origin Office<span style="color:red;">*</span></label>
                            <select class="form-control select2-single" required name="origin_office">
                                <option value="">--Select--</option>
                                <?php

                                if (count($origin_office)) {
                                    foreach ($origin_office as $list) {
                                        ?>
                                        <option
                                            value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="leadSource">Lead Source<span style="color:red;">*</span></label>
                            <select class="form-control select2-single" required name="lead_source_id">
                                <option value="">--Select--</option>
                                <?php

                                if (count($lead_source)) {
                                    foreach ($lead_source as $list) {
                                        ?>
                                        <option
                                            value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>

                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="collectionOfficer">Collection Officer<span style="color:red;">*</span></label>
                            <select class="form-control select2-single" required name="collection_officer">
                                <option value="">--Select--</option>
                                <?php

                                if (count($collection_officer)) {
                                    foreach ($collection_officer as $list) {
                                        ?>
                                        <option
                                            value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="collection_date">Collection Date<span style="color:red;">*</span></label>
                            <div class='input-group date' id='collection_date'>
                                <input type='text' name="collection_date" class="form-control" required/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>

                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="originOffice">Intake Month</label>
                            <select class="form-control select2-single"  name="intake_month" >
                                <option value="">--Select--</option>
                                <?php

                                if (count($intake_month)) {
                                    foreach ($intake_month as $list) {
                                        ?>
                                        <option
                                                value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="originOffice">Intake Year</label>
                            <select class="form-control select2-single"  name="intake_year" >
                                <option value="">--Select--</option>
                                <?php

                                if (count($intake_year)) {
                                    foreach ($intake_year as $list) {
                                        ?>
                                        <option
                                                value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>






                    <button type="submit" class="btn btn-primary d-block mt-3">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
