
<div class="row">
    <div class="col-12">
        <div class="card mb-4">
            <div class="card-body">
                <h5 class="mb-4"><?php echo $title; ?> <small style="color: red;">All Red marked are required</small></h5>

                <form action="<?php echo base_url(); ?>leads/concern_person_change" method="post" enctype="multipart/form-data">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputEmail4">New Concern Person <span style="color:red;">*</span></label>
                            <select class="form-control select2-single" required name="new_concern_person_id">
                                <option value="">--Select--</option>
                                <?php

                                if (count($collection_officer)) {
                                    foreach ($collection_officer as $list) {
                                        ?>
                                        <option
                                            value="<?php echo $list['id']; ?>"><?php echo $list['name'] . '(' . $list['code'] . ')'; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                            <input type="hidden" class="form-control" id="id"
                                   name="id" required value="<?php echo $id;?>">
                        </div>


                    </div>

                    <button type="submit" class="btn btn-primary d-block mt-3">Change</button>
                </form>
            </div>
        </div>
    </div>
</div>
