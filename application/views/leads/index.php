<script>
function get_lead_transfer_index(){
          var leadIdList = [];
          $.each($("input[name='is_allow']:checked"), function(){
              leadIdList.push($(this).val());
          });
          if(leadIdList.length == 0){
            alert("Please select atleast one lead.");
            return false;
          }else{
            var arrayLength = leadIdList.length;
            var returnString = '';
            for (var i = 0; i < arrayLength; i++) {
                console.log(leadIdList[i]);
                if(i == 0){
                  returnString = (returnString + $("#lead_" + leadIdList[i]).val());
                }else{
                  returnString = (returnString + ', ' + $("#lead_" + leadIdList[i]).val());
                }
            }
            //alert("My favourite sports are: " + returnString);
            $("#leadIdList").val(returnString);
            return true;
          }
};
function send_lead_email(){
        var leadList = [];
        $.each($("input[name='is_allow_email']:checked"), function(){
            leadList.push($(this).val());
        });
        if(leadList.length == 0){
          alert("Please select atleast one lead.");
          return false;
        }else{
          var arrayLength = leadList.length;
          var returnString = '';
          for (var i = 0; i < arrayLength; i++) {
              console.log(leadList[i]);
              if(i == 0){
                returnString = (returnString + $("#email_" + leadList[i]).val());
              }else{
                returnString = (returnString + ', ' + $("#email_" + leadList[i]).val());
              }
          }
          $("#recipientList").val(returnString);
          return true;
        }
};
</script>

<div class="col-12 mb-4">
    <div class="card">
        <div class="card-body" style="overflow-x: auto;">
            <h5 class="card-title"><?php echo $title; ?></h5>

            <?php
             $is_favourite = $this->session->userdata('is_favourite');
              $lead_source_id = $this->session->userdata('lead_source_id');
              $country_id = $this->session->userdata('country_id');
              $name = $this->session->userdata('name');
              $admission_officer_id = $this->session->userdata('admission_officer');
              $date=$this->session->userdata('date');
             //echo $is_favourite; die;
             ?>

            <form class="form-inline" method="post" action="<?php echo base_url(); ?>leads/<?php if ($this->uri->segment(2) != 'deleted_leads') {if ($user_id < 1) { echo "index";}else{echo "my_desk";}} ?>">
              <div class="form-group col-md-4 mb-4">
               <label>Is Favourite</label>
               <select class="select2-single col-md-12"  name="is_favourite">
                   <option value="all" <?php if ($is_favourite == 'all' || $is_favourite == '') {
                 echo 'selected';
             } ?>>--All--</option>
                   <option value="1" <?php if ($is_favourite == '1' && $is_favourite != '') {
                 echo 'selected';
             } ?>>Yes</option>
                   <option value="0" <?php if ($is_favourite == '0' && $is_favourite != '') {
                 echo 'selected';
             } ?>>No</option>
               </select>
             </div>
             <div class="form-group col-md-4 mb-4">
              <label>Lead source</label>
              <select  class="form-control col-md-12" name="lead_source_id" >
                  <option value="">--Select--</option>
                  <?php
                  if (count($lead_source)) {
                      foreach ($lead_source as $list) {
                          ?>
                          <option
                              <?php if ($list['id'] == $lead_source_id) {
                              echo 'selected';
                          } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                          <?php
                      }
                  }
                  ?>
              </select>
            </div>
            <div class="form-group col-md-4 mb-4">
              <label for="contact">Country</label>
              <select  class="form-control col-md-12"  name="country_id" >
                  <option value="">--Select--</option>
                  <?php

                  if (count($countries)) {
                      foreach ($countries as $list) {
                          ?>
                          <option
                              <?php if ($list['id'] == $country_id) {
                              echo 'selected';
                          } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                          <?php
                      }
                  }
                  ?>
              </select>
            </div>
            <div class="form-group col-md-4 mb-4">

              <label>Name/Email/Contact No.</label>
              <input type="text" class="form-control col-md-12" value="<?php echo $name; ?>" name="name" id="inlineFormInputName2">

            </div>
            <div class="form-group col-md-4 mb-4">
              <label for="contact">Followup Officer</label>
              <select  class="form-control  col-md-12"  name="admission_officer" >
                  <option value="">--Select--</option>
                  <?php

                  if (count($admission_officer)) {
                      foreach ($admission_officer as $list) {
                          ?>
                          <option
                              <?php if ($list['id'] == $admission_officer_id) {
                              echo 'selected';
                          } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                          <?php
                      }
                  }
                  ?>
              </select>
           </div>
           <div class="form-group col-md-3 mb-3">

             <label>Follow Up Date </label><br>
             <div class='input-group date' id='collection_date'>
                 <input type='text' name="date" class="form-control col-md-12" autocomplete="off" value="<?php echo $date; ?>"/>
                 <span class="input-group-addon">
                     <span class="glyphicon glyphicon-calendar"></span>
                 </span>
             </div>
             <!-- <input type="text" class="form-control col-md-12" value="<?php echo $name; ?>" name="name" id="inlineFormInputName2"> -->

           </div>
             <button type="submit" class="btn btn-sm btn-outline-primary mb-2">Search</button>
            </form>


            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col" style="width:50px !important;">#</th>
                    <th scope="col" style="width:10% !important;">Student Name</th>
                    <th scope="col" style="width:10% !important;">Contact</th>
                   <th scope="col" style="width:10% !important;">Email</th>
                    <th scope="col" style="width:10% !important;">Country</th>
                    <th scope="col" style="width:10% !important;">Course Level</th>
                    <th scope="col" style="width:10% !important;">Subject</th>
                    <th scope="col" style="width:10% !important;">Followup Officer</th>
                    <th scope="col" style="width:5% !important;">Last Follow Up Date</th>
                    <th scope="col"style="width:10% !important;">Content</th>
                    <th scope="col">Action</th>
                    <?php if ($this->uri->segment(2) != 'deleted_leads') {
                      if ($user_id < 1) { ?>
                    <th scope="col">
                      <div class="btn-group mb-1">
                              <button class="btn btn-secondary btn-xs dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  Action
                              </button>
                              <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 25px, 0px); top: 0px; left: -60px; will-change: transform;">
                                  <span class="dropdown-item" href="javascript:void">Select All
                                    <input type="checkbox" class="checkAll">
                                  </span>
                                  <span class="dropdown-item">
                                    <button type="button" onclick="get_lead_transfer_index();" id="lead_transfer_index" class="btn btn-secondary btn-xs mb-1" data-toggle="modal"
                                        data-target="#LeadModalContent" data-whatever="">
                                        Transfer to MyDesk
                                    </button>
                                  </span>
                              </div>
                      </div>
                    </th>
                  <?php }else{?>
                    <th scope="col">
                      <div class="btn-group mb-1">
                              <button class="btn btn-secondary btn-xs dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  Action
                              </button>
                              <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 25px, 0px); top: 0px; left: -60px; will-change: transform;">
                                  <span class="dropdown-item" href="javascript:void">Select All
                                    <input type="checkbox" class="checkAll">
                                  </span>
                                  <span class="dropdown-item">
                                    <button type="button" onclick="send_lead_email();" id="send_lead_email" class="btn btn-secondary btn-xs mb-1" data-toggle="modal"
                                        data-target="#SendMailLeadModalContent" data-whatever="">
                                        Send Email
                                    </button>
                                  </span>
                              </div>
                      </div>
                    </th>
                      <?php } ?>
                    <?php } ?>
                </tr>
                </thead>
                <tbody>
                <?php
                $i = (int)$this->uri->segment(3);
                foreach ($leads as $row):
                    $i++;
                    ?>
                    <tr>
                        <th scope="row"><?php echo $i; ?></th>

                        <td style="min-width: 100px !important;">
                          <a style="text-decoration:underline;"  href="<?php echo base_url(); ?>leads/view/<?php echo $row['id']; ?>">
                            <?php echo $row['first_name']; ?> <?php echo $row['last_name']; ?>
                          </a>
                        </td>
                        <td><?php echo $row['country_code'].' '.$row['phone']; ?></td>
                        <td><?php echo $row['email']; ?></td>
                        <td><?php echo $row['country_name']; ?></td>
                        <td><?php echo $row['course_level']; ?></td>
                        <td><?php echo $row['course_subject']; ?></td>
                        <td style="min-width: 100px !important;"><?php echo $row['followup_officer_name']; ?></td>
                        <td style="min-width: 100px !important;"><?php echo $row['followup_date']; ?></td>
                        <td><?php echo $row['content']; ?></td>
                        <td style="min-width: 340px !important;">

                      <?php if ($this->uri->segment(2) != 'deleted_leads') {
                        if ($user_id > 0) { ?>

                          <a href="<?php echo base_url(); ?>leads/add_to_favourite/<?php echo $row['id'].'/'.$row['is_favourite']; ?>"
                            title="Favourite">
                              <button type="button" class="btn <?php if ($row['is_favourite'] == 0) {
                            echo 'btn-warning';
                        } else {
                            echo 'btn-success';
                        } ?> btn-xs mb-1">
                                  <?php
                                    if ($row['is_favourite'] == 0) {
                                        echo 'Unfavourite';
                                    } else {
                                        echo 'Favourite';
                                    }
                                  ?>
                              </button>

                          </a>

                          <a href="<?php echo base_url(); ?>students_profile/lead_transfer/<?php echo $row['id']; ?>"
                             title="Edit">
                              <button type="button" class="btn btn-secondary btn-xs mb-1">
                                  Registration
                              </button>
                          </a>

                          <a href="<?php echo base_url(); ?>leads/concern_person_change/<?php echo $row['id']; ?>"
                             title="Edit">
                             <button type="button" class="btn btn-primary btn-xs mb-1">Transfer</button>
                          </a>
                            <a href="<?php echo base_url(); ?>leads/delete/<?php echo $row['id']; ?>"
                               onclick="return deleteConfirm()" title="Delete">
                                <button type="button" class="btn btn-primary btn-xs mb-1">
                                    Delete
                                </button>

                            </a>

                          <?php } else { ?>
                            <a href="<?php echo base_url(); ?>leads/transfer_to_mydesk/<?php echo $row['id']; ?>"
                               title="accept">
                                <button type="button" class="btn btn-primary btn-xs mb-1">
                                    Transfer to MyDesk
                                </button>
                            </a>
                          <?php }
                    } else { ?>
                      <a href="<?php echo base_url(); ?>leads/reactive_lead/<?php echo $row['id']; ?>"
                         title="Edit">
                          <button type="button" class="btn btn-primary btn-xs mb-1">
                              Reactive
                          </button>
                      </a>

                    <?php } ?>

                        </td>
                        <?php if ($this->uri->segment(2) != 'deleted_leads') {
                          if ($user_id < 1) { ?>
                      <td>
                        <input type="checkbox" value="<?php echo $i; ?>" class="checkbox"  name="is_allow">
                        <input type="hidden" id="lead_<?php echo $i; ?>" value="<?php echo $row['id']; ?>"/>
                      </td>
                    <?php }else{?>
                      <td>
                      <input type="checkbox" value="<?php echo $i; ?>" class="checkbox"  name="is_allow_email">
                      <input type="hidden" id="email_<?php echo $i; ?>" value="<?php echo $row['email']; ?>"/>
                    </td>
                        <?php } ?>
                      <?php } ?>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <?php echo $this->pagination->create_links(); ?>
        </div>
    </div>
</div>



<div class="modal fade" id="LeadModalContent" tabindex="-1" role="dialog"
                                aria-hidden="true">
              <div class="modal-dialog" role="document">
                  <div class="modal-content">

                      <div class="modal-body">
                          <form action="<?php echo base_url(); ?>leads/lead_transfer_index" method="post" enctype="multipart/form-data">
                              <div class="form-group">
                                  <input type="hidden" class="form-control" name="leadIdList" id="leadIdList">
                              </div>
                              <button type="submit" class="btn btn-primary d-block mt-3">Confirm</button>
                              <button type="button" class="btn btn-secondary"
                                  data-dismiss="modal" style="float: right;margin-top: -42px;">Cancel</button>
                          </form>
                      </div>

                  </div>
              </div>
          </div>





          <div class="modal fade" id="SendMailLeadModalContent" tabindex="-1" role="dialog"
                                          aria-hidden="true">
                                          <div class="modal-dialog" role="document">
                                              <div class="modal-content">
                                                  <div class="modal-header">
                                                      <h5 class="modal-title" id="exampleModalContentLabel">Send New message</h5>
                                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                          <span aria-hidden="true">&times;</span>
                                                      </button>
                                                  </div>
                                                  <div class="modal-body">
                                                      <form action="<?php echo base_url(); ?>leads/lead_email_send" method="post" enctype="multipart/form-data">
                                                          <div class="form-group">
                                                              <label for="recipient-name"
                                                                  class="col-form-label">Recipient:</label>
                                                              <input type="text" class="form-control" name="recipientList" id="recipientList">
                                                          </div>
                                                          <div class="form-group">
                                                              <label for="header-name"
                                                                  class="col-form-label">Header:</label>
                                                              <input type="text" class="form-control" name="header" id="Header">
                                                          </div>
                                                          <div class="form-group">
                                                              <label for="message-text" class="col-form-label">Message:</label>
                                                              <textarea class="form-control" name="message" id="message-text"></textarea>
                                                          </div>
                                                          <button type="submit" class="btn btn-primary d-block mt-3">Send message</button>
                                                      </form>
                                                  </div>
                                                  <div class="modal-footer">
                                                      <button type="button" class="btn btn-secondary"
                                                          data-dismiss="modal">Close</button>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
