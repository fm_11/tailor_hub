<div style="margin-bottom: 10px;">
  <a  href="<?php echo base_url(); ?>leads/edit/<?php echo $lead->id; ?>">
    <button type="button" class="btn btn-secondary btn-lg top-right-button mr-1">Edit</button>
  </a>
</div>
<div class="row">
    <div class="col-12">
        <div class="card mb-4">
            <div class="card-body">
                <h5 class="mb-4"><?php echo $title; ?></h5>


                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="studentFirstName">Student First Name</label>
                            <input type="text" disabled class="form-control" id="first_name"
                                   name="first_name" required value="<?php echo $lead->first_name;?>">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="studentLastName">Student Last Name</label>
                            <input disabled type="text" class="form-control" id="last_name"
                                   name="last_name"  value="<?php echo $lead->last_name;?>">
                        </div>

                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="contact">Contact</label>
                            <input disabled type="text" class="form-control" id="phone"
                                   name="phone" required value="<?php echo $lead->phone;?>">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="email">Email</label>
                            <input disabled type="text" class="form-control" id="email"
                                   name="email" required  value="<?php echo $lead->email;?>">
                        </div>

                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="country">Interested Country</label>
                            <select disabled class="form-control select2-single" required name="interested_country">
                                <option value="">--Select--</option>
                                <?php

                                if (count($countries)) {
                                    foreach ($countries as $list) {
                                        ?>
                                        <option
                                            <?php if ($list['id'] == $lead->interested_country) {
                                            echo 'selected';
                                        } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="purpose">Purpose</label>
                            <input disabled type="text" class="form-control" id="purpose"
                                   name="purpose"  value="<?php echo $lead->purpose;?>">
                        </div>

                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="courseLevel">Course Level</label>
                            <select disabled class="form-control select2-single"  name="course_level_id">
                                <option value="">--Select--</option>
                                <?php

                                if (count($course_level)) {
                                    foreach ($course_level as $list) {
                                        ?>
                                        <option
                                            <?php if ($list['id'] == $lead->course_level_id) {
                                            echo 'selected';
                                        } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="subject">Subject</label>
                            <input disabled type="text" class="form-control" id="course_subject"
                                   name="course_subject"  value="<?php echo $lead->course_subject;?>">

                        </div>

                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="originOffice">Origin Office</label>
                            <select disabled class="form-control select2-single" required name="origin_office">
                                <option value="">--Select--</option>
                                <?php

                                if (count($origin_office)) {
                                    foreach ($origin_office as $list) {
                                        ?>
                                        <option
                                            <?php if ($list['id'] == $lead->origin_office) {
                                            echo 'selected';
                                        } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="leadSource">Lead Source</label>
                            <select disabled class="form-control select2-single" required name="lead_source_id">
                                <option value="">--Select--</option>
                                <?php

                                if (count($lead_source)) {
                                    foreach ($lead_source as $list) {
                                        ?>
                                        <option
                                            <?php if ($list['id'] == $lead->lead_source_id) {
                                            echo 'selected';
                                        } ?>  value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>

                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="collectionOfficer">Collection Officer</label>
                            <select disabled class="form-control select2-single" required name="collection_officer">
                                <option value="">--Select--</option>
                                <?php

                                if (count($collection_officer)) {
                                    foreach ($collection_officer as $list) {
                                        ?>
                                        <option
                                            <?php if ($list['id'] == $lead->collection_officer) {
                                            echo 'selected';
                                        } ?>  value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="collection_date">Collection Date</label>
                            <div class='input-group date' id='collection_date'>
                                <input disabled type='text' required value="<?php echo $lead->collection_date; ?>" name="collection_date" class="form-control" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>

                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="purpose">University</label>
                            <input disabled type="text" class="form-control" value="<?php echo $lead->university; ?>" id="university"
                                   name="university">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="purpose">IELTS</label>
                            <input disabled type="text" class="form-control" value="<?php echo $lead->ielts; ?>" id="ielts"
                                   name="ielts">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label class="checkbox-inline"></label>
                            <input disabled name="status" <?php if ($lead->status == '1') {
                                    echo 'checked';
                                } ?> type="checkbox" value="1">Visited

                            <label class="checkbox-inline"></label>
                            <input disabled name="status" type="checkbox" <?php if ($lead->status == '2') {
                                    echo 'checked';
                                } ?> value="2">Assessment

                        </div>
                        <div class="form-group col-md-3">
                            <label for="originOffice">Intake Month</label>
                            <select disabled class="form-control select2-single"  name="intake_month">
                                <option value="">--Select--</option>
                                <?php

                                if (count($intake_month)) {
                                    foreach ($intake_month as $list) {
                                        ?>
                                        <option
                                            <?php if ($list['id'] == $lead->intake_month) {
                                            echo 'selected';
                                        } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="originOffice">Intake Year</label>
                            <select disabled class="form-control select2-single"  name="intake_year">
                                <option value="">--Select--</option>
                                <?php

                                if (count($intake_year)) {
                                    foreach ($intake_year as $list) {
                                        ?>
                                        <option
                                            <?php if ($list['id'] == $lead->intake_year) {
                                            echo 'selected';
                                        } ?> value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>


                    </div>

                <hr>
                <h5 class="card-title">Follow Up </h5>
                <div class="text-right">
                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myfollowup">Add FollowUp</button>
                </div>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Date</th>
                        <th scope="col">Follow Up Type</th>
                        <th scope="col">Remarks</th>
                        <th scope="col">Followup Officer</th>
                        <th scope="col">Next Followup Date</th>
                        <th scope="col">Document Received</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i = 0;
                    foreach ($lead_followup as $followup_row):
                        $i++;
                        ?>
                        <tr>
                            <th scope="row"><?php echo $i; ?></th>
                            <td><?php echo $followup_row['followup_date']; ?></td>
                            <td><?php echo $followup_row['followup_type_id']; ?></td>
                            <td><?php echo $followup_row['remarks']; ?></td>
                            <td><?php echo $followup_row['followup_officer_name']; ?></td>
                            <td><?php echo $followup_row['next_followup_date']; ?></td>
                            <td>
                                <?php
                                if ($followup_row['status']==1) {
                                    echo "Yes";
                                } else {
                                    echo "No";
                                }
                                ?>
                            </td>

                        </tr>

                    <?php endforeach; ?>
                    </tbody>

                </table>
                <div id="myfollowup" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Follow UP <small style="color: red;">All Red marked are required</small></h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>

                            </div>
                            <div class="modal-body">
                                <form action="<?php echo base_url(); ?>leads/followup_save/<?php echo $lead->id ?>" method="post" enctype="multipart/form-data" autocomplete="off">
                                  <input type="hidden" name="followup" value="view"/>
                                   <div class="form-group">

                                        <label for="contact">Follow Up Type<span style="color:red;">*</span></label>
                                       <select class="form-control select2-single" required name="followup_type_id">
                                           <option value="">--Select--</option>
                                           <?php

                                           if (count($followup_mode)) {
                                               foreach ($followup_mode as $list) {
                                                   ?>
                                                   <option
                                                        value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                                   <?php
                                               }
                                           }
                                           ?>
                                       </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="contact">Follow up Date<span style="color:red;">*</span></label>
                                        <div class='input-group date' id='followup_date'>
                                            <input type='text' name="followup_date" class="form-control" required/>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="contact">Remarks<span style="color:red;">*</span></label>
                                        <input type="text" class="form-control" id="remarks"
                                               name="remarks" required value="">
                                    </div>



                                    <div class="form-group">
                                        <label for="contact">Next Follow up Date</label>
                                        <div class='input-group date' id='next_followup_date'>
                                            <input type='text' name="next_followup_date" class="form-control" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="contact">Document Received<span style="color:red;">*</span></label>
                                            <select  class="form-control" name="status" id="sel1" required>
                                                <option value="">Select</option>
                                                <option value="1">Yes</option>
                                                <option value="2">No</option>
                                           </select>

                                    </div>

                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-danger d-block mt-3" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-success d-block mt-3">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
