<hr>
<style>
.my-custom-scrollbar {
/* position: relative;
height: 500px;
overflow: auto; */
}
.table-wrapper-scroll-y {
display: block;
}
</style>
<?php
if(isset($excel)){
  header('Content-type: application/excel');
  $filename = 'Student Registraion Report.xls';
  header('Content-Disposition: attachment; filename='.$filename);
}
?>
<div class="table-wrapper-scroll-y my-custom-scrollbar">
<table class="table table-responsive">
    <thead>
      <tr>
          <th  class="text-center" colspan="6" scope="col">
            <h3>
               <?php echo $org_info['org_name']; ?>
            </h3>
            <p><?php echo $org_info['address']; ?></p>
            <p><?php echo $org_info['email']; ?></p>
            <p>Student Registraion Report</p>
            <p>Branch: <?php echo $org_info['branch_name']; ?></p>
          </th>
      </tr>
      <tr>
            <th scope="col" class="text-left" colspan="2">Employee Name</th>
            <td scope="col" class="text-left" colspan="6"><?php echo $employee_info[0]['name']; ?></td>
     </tr>
     <tr>
           <th scope="col" class="text-left" colspan="2">Employee Code</th>
           <td scope="col" class="text-left" colspan="6"><?php echo $employee_info[0]['code']; ?></td>
    </tr>
    <tr>
          <th scope="col" class="text-left" colspan="2">Designation</th>
          <td scope="col" class="text-left" colspan="6"><?php echo $employee_info[0]['designation']; ?></td>
   </tr>
   <tr>
         <th scope="col" class="text-left" colspan="2">Report Period</th>
         <td scope="col" class="text-left" colspan="6"><?php echo $from_date; ?> To <?php echo $to_date; ?></td>
  </tr>
  <tr>
        <th scope="col" class="text-left" colspan="2">Total Day</th>
        <td scope="col" class="text-left" colspan="6"><?php echo $total_day; ?></td>
 </tr>
 <tr>
       <th scope="col" class="text-left" colspan="2">Report To</th>
       <td scope="col" class="text-left" colspan="6"><?php echo $report_to[0]['designation']; ?></td>
</tr>
<tr>
      <th scope="col" class="text-left" colspan="2">No of Student My Desk</th>
      <td scope="col" class="text-left" colspan="1"><?php echo $my_desk; ?></td>
      <th scope="col" class="text-left" colspan="2">No of Student Registration</th>
      <td scope="col" class="text-left" colspan="2"><?php echo $student_registration; ?></td>
</tr>
      <tr>
            <th scope="col">#</th>
            <th scope="col">Student Id</th>
            <th scope="col">Student Name</th>
            <th scope="col">Country</th>
            <th scope="col">Current Status</th>
            <th scope="col">Source</th>
            <th scope="col">Intake</th>
     </tr>
    </thead>
    <tbody>
      <?php
      $i = 0;
      foreach ($rData as $row):
          $i++;
          ?>
        <tr>
            <th scope="row"><?php echo $i; ?></th>
            <td><?php echo $row['applicant_code']; ?></td>
            <td><?php echo $row['NAME']; ?></td>
            <td><?php echo $row['country']; ?></td>
            <td><?php echo $row['STATUS']; ?></td>
            <td><?php echo $row['lead_source_name']; ?></td>
            <td><?php echo $row['MONTH']; ?> , <?php echo $row['YEAR']; ?></td>
        </tr>
      <?php endforeach; ?>
      <tr>
            <th colspan="3"></th>
            <td colspan="6"></td>
     </tr>
     <tr>
           <td colspan="3"><?php echo $printed_by[0]['name']; ?></td>
            <td colspan="2"></td>
            <td colspan="2"><?php echo $employee_info[0]['reporting_boss']; ?> </td>
    </tr>
    <tr>
          <th  class="text-left" colspan="3"><span style="border-top: 1px solid #3a2626;">Prepared By</span> </th>
          <th scope="col" class="text-left" colspan="2"><span style="border-top: 1px solid #3a2626;">Varified By</span></th>
          <th scope="col" class="text-left" colspan="2"><span style="border-top: 1px solid #3a2626;">Checked By</span></th>
   </tr>
   <tr>
         <th scope="col" class="text-left" colspan="3"><?php echo $printed_by[0]['designation_name']; ?> </th>
         <th scope="col" class="text-left" colspan="2">Head of Engagement</th>
         <th scope="col" class="text-left" colspan="2"><?php echo $employee_info[0]['reporting_boss_designation']; ?> </th>
  </tr>

    </tbody>
</table>
</div>
