<script type="text/javascript">

</script>


<div class="col-12 mb-4">
    <div class="card">
        <div class="card-body">
           <h5 class="card-title"><?php echo $title; ?></h5>

           <?php
            $name = $this->session->userdata('search__name');
            ?>
          <form class="form-inline" method="post" action="<?php echo base_url(); ?>ValidAgreements/index">
              <label class="sr-only" for="inlineFormInputName2">Name</label>
              <input type="text" class="form-control mb-2 mr-sm-2" value="<?php echo $name; ?>" name="name" id="inlineFormInputName2" placeholder="Name">
            <button type="submit" class="btn btn-sm btn-outline-primary mb-2">Search</button>
          </form>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Action</th>

                    </tr>
                </thead>
                <tbody>
                  <?php
                    $i = (int)$this->uri->segment(3);
                    foreach ($application_status as $row):
                      $i++;
                      ?>
                    <tr>
                        <th scope="row"><?php echo $i; ?></th>
                        <td><?php echo $row['name']; ?></td>
                        <td>
                               <a href="<?php echo base_url(); ?>application_status_details/edit/<?php echo $row['id']; ?>"
                                  title="Edit">
                                  <button type="button" class="btn btn-primary btn-xs mb-1">
                                    Edit
                                  </button>
                                </a>
                               <a href="<?php echo base_url(); ?>application_status_details/delete/<?php echo $row['id']; ?>"
                                  onclick="return deleteConfirm()" title="Delete">
                                  <button type="button" class="btn btn-primary btn-xs mb-1">
                                    Delete
                                  </button>
                                </a>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <?php echo $this->pagination->create_links(); ?>
        </div>
    </div>
</div>
