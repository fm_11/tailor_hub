<?php

class Tax extends CI_Model
{

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function get_finalcial_year()
    {
        $this->db->select('*');
        $this->db->from('tbl_finalcial_year');
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_employee_manual_tax_details($data)
    {
      $finalcial_year_id = $data['finalcial_year_id'];
      $branch_id = $data['branch_id'];
      $where = "";
      if($branch_id != 'all'){
        $where = " AND e.`branch_id` = $branch_id ";
      }
      $result = $this->db->query("SELECT e.`id`,e.`name`,e.`code`,p.`name` AS designation_name,t.`amount` FROM `tbl_employee` AS e
LEFT JOIN `tbl_employee_post` AS p ON p.`id` = e.`post`
LEFT JOIN `tbl_manual_taxes` AS t ON t.`employee_id` = e.`id` AND t.`finalcial_year_id` = $finalcial_year_id
WHERE e.`status` = 1 $where")->result_array();
      return $result;
    }
  }
