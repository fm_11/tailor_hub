<?php

class User_role_wise_privilege extends CI_Model
{
    public $title = '';
    public $content = '';
    public $date = '';

    public function __construct()
    {
        $this->load->database();
        parent::__construct();
    }

    /**
     * Generates a list of user wise privileges
     * @author  :   Amlan Chowdhury
     * @uses    :   To Generate a list of user wise privileges
     * @access  :   public
     * @param   :   int $offset, int $limit
     * @return  :   array
     */
    public function get_list($offset, $limit)
    {
        $query = $this->db->get('user_role_wise_privileges', $offset, $limit);
        return $query->result();
    }

    public function get_role_name_by_id($role_id)
    {
        return $this->db->query("SELECT `role_name` FROM `user_roles` WHERE `id` = '$role_id'")->row()->role_name;
    }

    /**
     * Counts number of rows of  user wise privileges table
     * @author  :   Amlan Chowdhury
     * @uses    :   To count number of rows of  user wise privileges table
     * @access  :   public
     * @return  :   int
     */
    public function row_count()
    {
        return $this->db->count_all_results('user_role_wise_privileges');
    }

    /**
     * Adds data to user role wise privileges
     * @author  :   Amlan Chowdhury
     * @uses    :   To add data to user role wise privileges
     * @access  :   public
     * @param   :   array $data
     * @return  :   boolean
     */
    public function add($data)
    {
        if (isset($data['resources'])) {
            //echo '-------'.$data['role_id'].'=======';echo '<pre>';print_r($data);echo '</pre>';//die('model die');

            $this->db->trans_start();
            $this->db->delete('user_role_wise_privileges', array('role_id' => $data['role_id']));
            //$this->insert_rows('user_role_wise_privileges', $data['column_names'],$data['column_rows']);
            $this->db->insert_batch('user_role_wise_privileges', $data['resources']);
            $this->db->trans_complete();

            return $this->db->trans_status();
        } else {
            $this->db->delete('user_role_wise_privileges', array('role_id' => $data['role_id']));
            return true;
        }
    }

    /**
     * Updates data of user role wise privileges
     * @author  :   Amlan Chowdhury
     * @uses    :   To update data of user roles wise privileges
     * @access  :   public
     * @param   :   array $data
     * @return  :   boolean
     */
    public function edit($data)
    {
        return $this->db->update('user_role_wise_privileges', $data, array('id' => $data['id']));
    }

    /**
     * Reads data of specific user role wise privilege
     * @author  :   Amlan Chowdhury
     * @uses    :   To  read data of specific user role wise privilege
     * @access  :   public
     * @param   :   int $user_role_wise_privileges_id
     * @return  :   boolean
     */
    public function read($user_role_wise_privileges_id)
    {
        return $this->db->get_where('user_role_wise_privileges', array('id' => $user_role_wise_privileges_id))->result();
    }

    /**
     * Gets data of user role wise privilege by role id
     * @author  :   Amlan Chowdhury
     * @uses    :   To  get data of user role wise privilege by role id
     * @access  :   public
     * @param   :   int $role_id
     * @return  :   array
     */
    public function get_by_role_id($role_id)
    {
        $query = $this->db->get_where('user_role_wise_privileges', array('role_id' => $role_id));
        return $query->result_array();
    }

    /**
     * Gets data of user role wise privilege by role id,controller name and action
     * @author  :   Amlan Chowdhury
     * @uses    :   To  get data of user role wise privilege by role id, controller name and action
     * @access  :   public
     * @param   :   int $role_id, string $controller, string $action
     * @return  :   array
     */
    public function get_by_role_id_controller_action($role_id, $controller, $action)
    {
        $query = $this->db->get_where('user_role_wise_privileges', array('role_id' => $role_id, 'controller' => $controller, 'action' => $action));
        return $query->result_array();
    }

    /**
     * Gets data of user role wise privilege by controller name and action
     * @author  :   Amlan Chowdhury
     * @uses    :   To  get data of user role wise privilege by controller name and action
     * @access  :   public
     * @param   :   string $controller, string $action
     * @return  :   array
     */
    public function get_by_controller_action($controller, $action)
    {
        $query = $this->db->get_where('user_role_wise_privileges', array('controller' => $controller, 'action' => $action));
        return $query->result_array();
    }

    /**
     * Gets data of user role wise privilege by controller name ,action and role id
     * @author  :   Amlan Chowdhury
     * @uses    :   To  get data of user role wise privilege by controller name, action and role id
     * @access  :   public
     * @param   :   string $controller, string $action, int $role_id
     * @return  :   array
     */
    public function get_by_controller_action_role($controller, $action, $role_id)
    {
        $query = $this->db->get_where('user_role_wise_privileges', array('controller' => $controller, 'action' => $action, 'role_id' => $role_id));
        return $query->result_array();
    }

    /**
     * Gets data of user role wise privileged resourses by controller role id
     * @author  :   Amlan Chowdhury
     * @uses    :   To  get data of user role wise privileged resourses by role id
     * @access  :   public
     * @param   :   int $role_id
     * @return  :   array
     */
    public function get_privileged_resources($role_id)
    {
        $query = $this->db->get_where('user_role_wise_privileges', array('role_id' => $role_id));
        {
            $amlan_field_officers_reports_ccdas = array();
            $results = $query->result_array();
            foreach ($results as $result) {
                if ($result['controller'] == 'amlan_field_officers_reports') {
                    $result['id'] += 1000;
                    $result['controller'] = 'amlan_field_officers_reports_ccdas';
                    array_push($amlan_field_officers_reports_ccdas, $result);
                }
            }
            foreach ($amlan_field_officers_reports_ccdas as $amlan_field_officers_reports_ccda) {
                array_push($results, $amlan_field_officers_reports_ccda);
            }
            return $results;
        }
        return $query->result_array();
    }

    /**
     * Deletes data of specific user role wise privileges
     * @author  :   Amlan Chowdhury
     * @uses    :   To  delete data of specific user role wise privileges
     * @access  :   public
     * @param   :   int $user_role_wise_privileges_id
     * @return  :   boolean
     */
    public function delete($user_role_wise_privileges_id)
    {
        return $this->db->delete('user_role_wise_privileges', array('id' => $user_role_wise_privileges_id));
    }

    /**
     * Deletes data of user role wise privileges by role id ,controller name and action
     * @author  :   Amlan Chowdhury
     * @uses    :   To  delete data of user role wise privileges by role id ,controller name and action
     * @access  :   public
     * @param   :   int $role_id,string $controller, string $action
     * @return  :   boolean
     */
    public function delete_by_role_id_controller_action($role_id, $controller, $action)
    {
        return $this->db->delete('user_role_wise_privileges', array('role_id' => $role_id, 'controller' => $controller, 'action' => $action));
    }

    /**
     * Deletes data of specific user role wise privileges by role id
     * @author  :   Amlan Chowdhury
     * @uses    :   To  delete data of specific user role wise privileges by role id
     * @access  :   public
     * @param   :   int $role_id
     * @return  :   boolean
     */
    public function delete_by_role_id($role_id)
    {
        return $this->db->delete('user_role_wise_privileges', array('role_id' => $role_id));
    }

    /**
     * Checks permission of user role wise privileges
     * @author  :   Amlan Chowdhury
     * @uses    :   To  check permission of user role wise privileges
     * @access  :   public
     * @param   :   int $role_id, string $controller,string $action
     * @return  :   boolean
     */
    public function check_permission($role_id, $controller, $action)
    {
        $controller = strtolower($controller);
        $action = strtolower($action);

        if ($this->is_globally_allowed_action($controller, $action)) { //if action is globally allowed, access is granted
            return true;
        } else {
            $check = $this->db->query("SELECT `id` FROM `user_role_wise_privileges` WHERE `controller` = '$controller' AND `action` = '$action' AND `role_id` = '$role_id'")->result_array();
            return !empty($check) ? true : false;
        }
    }

    /**
     * Checks if the action is globally dis-allowed or not
     * @author  :   Anis Alamgir
     * @uses    :   To check if the action is globally dis-allowed or not
     * @access  :   public
     * @param   :   string $controller,string $action
     * @return  :   boolean
     * @wiki    : http://203.188.255.195/tracker/projects/microfin360/wiki/Manage_User_Role#Only-For-Super-Admin
     */
    public function is_globally_disallowed_action($controller, $action)
    {
        return isset($actions[$controller][$action]) ? true : false;
    }

    /**
     * Checks if the action is globally allowed or not
     * @author  :   Amlan Chowdhury
     * @uses    :   To check if the action is globally allowed or not
     * @access  :   public
     * @param   :   string $controller,string $action
     * @return  :   boolean
     */
    public function is_globally_allowed_action($controller, $action)
    {
        $actions = array();
        $actions['login']['index'] = 1;
        $actions['admin_logins']['readNotification'] = 1;
		$actions['admin_logins']['set_module'] = 1;
        $actions['leave_applications']['varifyOneTimeUrl'] = 1;
        $actions['login']['forgot_password'] = 1;
        $actions['login']['change_password'] = 1;
        $actions['login']['denied'] = 1;
        $actions['login']['authentication'] = 1;
        $actions['login']['logout'] = 1;
        $actions['agent_login']['index'] = 1;
        $actions['agent_login']['authentication'] = 1;
        $actions['agent_login']['logout'] = 1;
        $actions['agent_login']['myprofile'] = 1;
        $actions['agent_login']['contact'] = 1;
        $actions['agent_login']['denied'] = 1;
        $actions['agent_login']['change_password'] = 1;
        $actions['agent_dashboard']['index'] = 1;
        $actions['agent_dashboard']['agent_index'] = 1;
        $actions['reference_applicants']['index'] = 1;
        $actions['reference_applicants']['view'] = 1;
        $actions['reference_applicants']['student_registration'] = 1;
        $actions['reference_applicants']['application'] = 1;
        $actions['reference_applicants']['getCourseByCourseLevelId'] = 1;
        $actions['reference_applicants']['getInstituteByCountryId'] = 1;
        $actions['reference_applicants']['reference'] = 1;
        $actions['reference_applicants']['delete_reference'] = 1;
        $actions['reference_applicants']['edit_reference'] = 1;
        $actions['reference_applicants']['work_experience'] = 1;
        $actions['reference_applicants']['edit_work_experience'] = 1;
        $actions['reference_applicants']['delete_work_experience'] = 1;
        $actions['reference_applicants']['delete_language'] = 1;
        $actions['reference_applicants']['edit_language'] = 1;
        $actions['reference_applicants']['language_save'] = 1;
        $actions['reference_applicants']['delete_education'] = 1;
        $actions['reference_applicants']['edit_education'] = 1;
        $actions['reference_applicants']['education_save'] = 1;
        $actions['reference_applicants']['edit'] = 1;
        $actions['reference_applicants']['view_status'] = 1;

        $actions['students_login']['index'] = 1;
        $actions['students_login']['authentication'] = 1;
        $actions['students_login']['logout'] = 1;
        $actions['students_login']['denied'] = 1;
        $actions['students_dashboard']['index'] = 1;
        $actions['students_dashboard']['myprofile'] = 1;
        $actions['students_dashboard']['contact'] = 1;
        $actions['students_dashboard']['agent_details'] = 1;
        $actions['students_dashboard']['apply'] = 1;
        $actions['students_dashboard']['applylist'] = 1;
        $actions['students_dashboard']['getInstituteByCountryId'] = 1;
        $actions['students_dashboard']['getCourseByCourseLevelId'] = 1;
        $actions['students_login']['change_password'] = 1;
        $actions['students_profile']['registration'] = 1;
        $actions['students_dashboard']['view_status'] = 1;
        $actions['students_dashboard']['edit'] = 1;
        $actions['students_dashboard']['education_save'] = 1;
        $actions['students_dashboard']['edit_education'] = 1;
        $actions['students_dashboard']['delete_education'] = 1;
        $actions['students_dashboard']['language_save'] = 1;
        $actions['students_dashboard']['edit_language'] = 1;
        $actions['students_dashboard']['delete_language'] = 1;
        $actions['students_dashboard']['delete_work_experience'] = 1;
        $actions['students_dashboard']['edit_work_experience'] = 1;
        $actions['students_dashboard']['work_experience'] = 1;
        $actions['students_dashboard']['edit_reference'] = 1;
        $actions['students_dashboard']['delete_reference'] = 1;
        $actions['students_dashboard']['reference'] = 1;
        $actions['homes']['index'] = 1;
        $actions['homes']['tailor_area'] = 1;
        $actions['homes']['contact'] = 1;
        $actions['homes']['about'] = 1;
        $actions['homes']['tailor_list'] = 1;
        $actions['homes']['tailor_profile'] = 1;
        $actions['homes']['order_submit'] = 1;
        $actions['homes']['blog_details'] = 1;
        $actions['homes']['blog'] = 1;
        return isset($actions[$controller][$action]);
    }


    public function get_all_resources_array()
    {
        define("VIEW", "View");
        define("ADD", "Add");
        define("EDIT", "Edit");
        define("DELETE", "Delete");
        //initializing
        $group_id = 0;
        $tmp = array();
        //Organization management group


        $group_name = 'Dashboard';
        $subgroup_name = 'Dashboard';

        $entity_name = 'Dashboard';
        $controller = 'dashboard';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('index','hrm_dashboard','crm_dashboard', 'todays_present_employee','todays_absent_employee'));

        $entity_name = 'Dashboard Employee List and Email Send';
        $controller = 'employees';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('dashboard_email_send','dashboard_employee_list'));


        $group_name = 'Configuration';
        $subgroup_name = 'Configuration';

        $entity_name = 'Organization Information';
        $controller = 'org_infos';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'index');

        $entity_name = 'Branch Information';
        $controller = 'branches';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

        $entity_name = 'Department Information';
        $controller = 'departments';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

        $entity_name = 'Designation Information';
        $controller = 'designations';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

        $entity_name = 'Leave Type';
        $controller = 'leave_types';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');
        //crm_dashboard
        $entity_name = 'Currency';
        $controller = 'currencies';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

        $entity_name = 'Course Prefix';
        $controller = 'course_prefixes';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

        $entity_name = 'Intake Year';
        $controller = 'intake_years';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW,array('index','view'));
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

        $entity_name = 'Intake Month';
        $controller = 'intake_months';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('index','view'));
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

        $entity_name = 'Institution Types';
        $controller = 'institution_types';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('index','view'));
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

        $entity_name = 'Course Types';
        $controller = 'course_types';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('index','view'));
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

        $entity_name = 'Course Levels';
        $controller = 'course_levels';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('index','view'));
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

        $entity_name = 'Institutions';
        $controller = 'institutions';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('index','view'));
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, array('add','getCityByCountryId'));
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

        $entity_name = 'Courses';
        $controller = 'courses';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('index','view'));
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, array('add','getInstituteByCountryId'));
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

        $entity_name = 'City';
        $controller = 'cities';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('index','view'));
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

        $entity_name = 'Agents';
        $controller = 'agents';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('view','updateMsgStatusAgentStatus','agents_email_send','index'));
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

        $entity_name = 'Valid Agreement Information';
        $controller = 'valid_agreements';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

        $entity_name = 'Application Status Information';
        $controller = 'application_status_details';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

        $entity_name = 'Lead Sources Information';
        $controller = 'lead_sources';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');

        ///end
        $group_name = 'User Management';
        $subgroup_name = 'User Management';

        $entity_name = 'User Role';
        $controller = 'user_roles';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

        $entity_name = 'User Information';
        $controller = 'users';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');



        $group_name = 'Process';
        $subgroup_name = 'Process';

        $entity_name = 'Employee Information';
        $controller = 'employees';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

        $entity_name = 'Manual Login Logout';
        $controller = 'timekeepings';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, array('index','self_manual_login','self_manual_logout'));

        $entity_name = 'Employee Movement Register';
        $controller = 'movement_registers';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

        $entity_name = 'Employee Leave Application';
        $controller = 'leave_applications';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, array('add','get_leave_days_without_holiday','get_leave_details','get_leave_details_all_date'));
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

        $entity_name = 'Employee Leave Approval';
        $controller = 'leave_applications';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('approval_index','application_approve_reject'));


        $entity_name = 'Employee Notice';
        $controller = 'notices';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');


        $entity_name = 'Leads';
        $controller = 'leads';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('index','lead_email_send','transfer_to_mydesk','lead_transfer_index','view'));
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');



        $entity_name = 'My Desk';
        $controller = 'leads';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('my_desk','lead_email_send','followup_save','add_to_favourite','concern_person_change','view'));
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, array('delete'));

        $entity_name = 'Deleted Leads';
        $controller = 'leads';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('deleted_leads','reactive_lead'));

        $entity_name = 'Applicants';
        $controller = 'applicants';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('index','my_file_upload','generateApplicationsId','getInstituteByCountryId','getCourseByCourseLevelId','view_status','view'));
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, array('add','education_save','language_save','work_experience','reference','application'));
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, array('edit','edit_education','edit_language','edit_work_experience','edit_reference'));
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, array('delete','delete_education','delete_language','delete_work_experience','delete_reference'));


        $entity_name = 'Applicantion List';
        $controller = 'applications';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('assign_process_officer','index'));

        $entity_name = 'Applicantion Status List';
        $controller = 'application_status';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('index','change_status','status_changes_email_send','update_interview_info','my_file_upload','view_status','view'));
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, array('delete'));

        $entity_name = 'On-Process Applicants';
        $controller = 'on_process_applicants';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('index','view','applicants_email_send'));
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, array('delete'));

        $entity_name = 'Registration';
        $controller = 'students_profile';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'lead_transfer');


        $group_name = 'Report';
        $subgroup_name = 'Report';

        $entity_name = 'Daily Attendance Report';
        $controller = 'daily_attendances';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');

        $entity_name = 'Attendance Deatils Report';
        $controller = 'attendance_details';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');

        $entity_name = 'Absentees Report';
        $controller = 'report_absentees';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');

        $entity_name = 'Leave Report';
        $controller = 'report_leaves';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');

        $entity_name = 'My Job Card';
        $controller = 'my_attendance_details';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');

        $entity_name = 'Movement Register Report';
        $controller = 'report_movement_registers';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');

        $entity_name = 'Agent Registraion Report';
        $controller = 'agent_registration_report';
              $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('index','getEmployeeByBrachId'));

        $entity_name = 'Student Registraion Report';
        $controller = 'student_registration_report';
                $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('index','getEmployeeByBrachId'));

        $entity_name = 'Lead Entry Report';
        $controller = 'lead_entry_report';
              $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('index','getEmployeeByBrachId'));

        $entity_name = 'Visitors Report';
        $controller = 'visitors_report';
                $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('index','getEmployeeByBrachId'));

        $entity_name = 'Lead Followup Report';
        $controller = 'lead_followup_report';
              $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('index','getEmployeeByBrachId'));
        $entity_name = 'Admission Report';
        $controller = 'admission_report';
              $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('index','getEmployeeByBrachId'));

        $entity_name = 'Process Report';
        $controller = 'process_report';
                $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('index','getEmployeeByBrachId'));
        $entity_name = 'Admission Report(Individual)';
        $controller = 'intake_admission_report';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('index','getEmployeeByBrachId'));


        return $tmp;
    }

    public function create_resource(&$tmp, $group_name, $subgroup_name, $entity_name, $controller, $action_title, $actions = null)
    {
        if (is_null($actions)) {
            $tmp[$group_name][$subgroup_name][$entity_name][$controller][$action_title][0]['name'] = strtolower($action_title);
        } elseif (is_string($actions)) {
            $tmp[$group_name][$subgroup_name][$entity_name][$controller][$action_title][0]['name'] = $actions;
        } elseif (is_array($actions)) {
            foreach ($actions as $key => $row) {
                $tmp[$group_name][$subgroup_name][$entity_name][$controller][$action_title][$key]['name'] = $row;
            }
        }

        return $tmp;
    }
}
