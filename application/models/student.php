<?php

class Student extends CI_Model
{

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function get_all_student_list($limit, $offset, $value = ''){
        $this->db->select('s.*,c.name as class_name,sc.name as section_name');
        $this->db->from('tbl_student as s');
        $this->db->join('tbl_class AS c', 's.class_id=c.id','left');
        $this->db->join('tbl_section AS sc', 's.section_id=sc.id','left');
        if (isset($value) && !empty($value)  && $value['class_id'] != '') {
            $this->db->where('s.class_id', $value['class_id']);
        }
        if (isset($value) && !empty($value) && $value['section_id'] != '') {
            $this->db->where('s.section_id', $value['section_id']);
        }
        if (isset($value) && !empty($value) && $value['group'] != '') {
            $this->db->where('s.group', $value['group']);
        }
        if (isset($value) && !empty($value) && $value['roll'] != '') {
            $this->db->where('s.roll_no', $value['roll']);
        }

        if (isset($value) && !empty($value) && $value['gender'] != '') {
            $this->db->where('s.gender', $value['gender']);
        }

        if (isset($value) && !empty($value) && $value['student_status'] != '') {
            $this->db->where('s.status', $value['student_status']);
        }

        $this->db->where('s.is_deleted', 0);
        $this->db->order_by("s.roll_no", "asc");
        if (isset($limit) && $limit > 0)
            $this->db->limit($limit, $offset);
        $query = $this->db->get();
        return $query->result_array();
    }


    function get_student_info_by_student_id($id){
        $this->db->select('s.*,c.name as class_name,sc.name as section_name');
        $this->db->from('tbl_student as s');
        $this->db->join('tbl_class AS c', 's.class_id=c.id','left');
        $this->db->join('tbl_section AS sc', 's.section_id=sc.id','left');
        $this->db->where('s.id', $id);
        $query = $this->db->get();
        return $query->result_array();
    }
	
	 public function routine_upload($data){
		 return $this->db->insert('tbl_student_class_routine', $data);
	 }

    public function add_upload_result($data)
    {
        return $this->db->insert('tbl_student_uploaded_result', $data);
    }

    public function update_uploaded_result_status($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update('tbl_student_uploaded_result', $data);
    }

    public function update_exam_status($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update('tbl_exam', $data);
    }


    function get_uploaded_result_info_by_result_id($id)
    {
        return $this->db->where('id', $id)->get('tbl_student_uploaded_result')->result_array();
    }

    function get_uploaded_syllabus_info_by_syllabus_id($id)
    {
        return $this->db->where('id', $id)->get('tbl_syllabus')->result_array();
    }
	
	 function get_uploaded_routine_info_by_routine_id($id)
    {
        return $this->db->where('id', $id)->get('tbl_student_class_routine')->result_array();
    }


    function delete_uploaded_result_info_by_result_id($id)
    {
        return $this->db->delete('tbl_student_uploaded_result', array('id' => $id));
    }


    function delete_uploaded_syllabus_info_by_syllabus_id($id)
    {
        return $this->db->delete('tbl_syllabus', array('id' => $id));
    }


	function delete_uploaded_routine_info_by_routine_id($id)
    {
        return $this->db->delete('tbl_student_class_routine', array('id' => $id));
    }


    function getStudentTimeKeepingInformation($limit, $offset, $value = ''){
        $this->db->select('s.*,c.name as class_name,sc.name as section_name,sl.login_status,sl.date');
        $this->db->from('tbl_student as s');
        $this->db->join('tbl_student_logins AS sl', 's.id=sl.student_id','left');
        $this->db->join('tbl_class AS c', 's.class_id=c.id','left');
        $this->db->join('tbl_section AS sc', 's.section_id=sc.id','left');
        if (isset($value) && !empty($value)  && $value['class_id'] != '') {
            $this->db->where('s.class_id', $value['class_id']);
        }
        if (isset($value) && !empty($value) && $value['section_id'] != '') {
            $this->db->where('s.section_id', $value['section_id']);
        }
        if (isset($value) && !empty($value) && $value['group'] != '') {
            $this->db->where('s.group', $value['group']);
        }
        if (isset($value) && !empty($value) && $value['date'] != '') {
            $this->db->where('sl.date', $value['date']);
        }
        $this->db->order_by("sl.date", "desc");
        if (isset($limit) && $limit > 0)
            $this->db->limit($limit, $offset);
        $query = $this->db->get();
        return $query->result_array();
    }


    function get_all_applied_student($limit, $offset, $value = ''){
        $this->db->select('a.*,c.name as class_name');
        $this->db->from('tbl_admission as a');
        $this->db->join('tbl_class AS c', 'a.class_id=c.id','left');
        if (isset($value) && !empty($value)  && $value['class_id'] != '') {
            $this->db->where('a.class_id', $value['class_id']);
        }
        if (isset($value) && !empty($value)  && $value['pin'] != '') {
            $this->db->where('a.pin', $value['pin']);
        }
        if (isset($value) && !empty($value) && $value['year'] != '') {
            $this->db->where('a.year', $value['year']);
        }
        $this->db->order_by("a.date_of_application", "desc");
        if (isset($limit) && $limit > 0)
            $this->db->limit($limit, $offset);
        $query = $this->db->get();
        return $query->result_array();
    }
}

?>