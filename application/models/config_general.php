<?php
class Config_general extends CI_Model
{

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    /**
     * Adds data to config general
     * @author  :   Md. Saiful Islam Feroz
     * @uses    :   get list
     * @access  :   public
     * @param   :   string purpose
     * @return  :   array
     */
    function get_list($purpose = '')
    {
        if ($purpose) {
            return $this->db->get_where('tbl_config_general', array('purpose' => $purpose))->result();
        } else {
            return $this->db->get('tbl_config_general')->result();
        }
    }

    /**
     * Adds data to config general
     * @author  :   Matin
     * @uses    :   To add data to config general
     * @access  :   public
     * @param   :   array $data
     * @return  :   boolean
     */
    function add($data)
    {
        foreach ($data as $field_name => $value) {
            $this->db->update('tbl_config_general', array('default_value' => $value), array('db_field_name' => $field_name));
        }
        return $this->db->insert('config_general', $data);
    }

    /**
     * Updates data of config general
     * @author  :   Matin
     * @uses    :   To update data to config general
     * @access  :   public
     * @param   :   array $data
     * @return  :   boolean
     */
    function edit($data)
    {
        $this->db->trans_start();
        foreach ($data as $field_name => $value) {
            $this->db->update('tbl_config_general', array('default_value' => $value), array('db_field_name' => $field_name));
            if ($field_name == "default_support_user_password") {
                $this->db->update('users', array('password' => sha1($value), 'last_password_changed' => date('Y-m-d H:i:s')), array('is_support_user' => '1'));
            }
        }
        $this->db->trans_complete();
        return $this->db->trans_status();
        //return $this->db->update('config_general', $data);
    }

    /**
     * Reads data of config general
     * @author  :   Matin
     * @uses    :   To  read data of config general
     * @access  :   public
     * @param   :   void
     * @return  :   array
     */
    function read($purpose = NULL)
    {
        return $this->get_general_configurations($purpose);
    }

    function get_general_configurations($purpose='')
    {
        if(!$purpose||$purpose==''||  is_bool($purpose)){
            $query=$this->db->get('tbl_config_general')->result();
        }else{
            $query=$this->db->get_where('tbl_config_general',array('purpose'=>$purpose))->result();
        }
        $data=array();
        foreach ($query as $value) {
            $data[$value->db_field_name]=$value->default_value;
        }
        //echo $this->db->last_query();
        $data=  $this->arrayToObject($data);
        //echo "<pre>";print_r($data);die;
        return $data;
    }

    function array_to_obj($array, &$obj)
        {
            foreach ($array as $key => $value)
            {
              if (is_array($value))
              {
              $obj->$key = new stdClass();
              array_to_obj($value, $obj->$key);
              }
              else
              {
                $obj->$key = $value;
              }
            }
          return $obj;
        }

    function arrayToObject($array)
       {
        $object= new stdClass();
        return $this->array_to_obj($array,$object);
       }

    /**
     * Reads data of config general
     * @author  :   Matin
     * @uses    :   To  read data of config general
     * @access  :   public
     * @param   :   void
     * @return  :   array
     */
    function read_array($purpose = NULL)
    {
        if (!$purpose) {
            $query = $this->db->get('tbl_config_general')->result();
        } else {
            $query = $this->db->get_where('tbl_config_general', array('purpose' => $purpose))->result();
        }
        $data = array();
        foreach ($query as $value) {
            $data[$value->db_field_name] = $value->default_value;
        }
        // $data=  $this->arrayToObject($data);
        // echo "<pre>";print_r($data);echo "</pre>";die;
        return $data;
//		$query=$this->db->get('config_general');
//		return $query->row_array();
    }

    /**
     * Gets maximum member no of config general
     * @author  :   Matin
     * @uses    :   To  get maximum member no of config general
     * @access  :   public
     * @param   :   void
     * @return  :   array
     */
    function maximum_member_no()
    {
        $this->db->select("max_member");
        $query = $this->db->get_where('tbl_config_general');
        return $query->row_array();
    }

    function get_collection_sheet_configuration($report_name)
    {
        return $this->db->get_where('config_collectionsheets', array('report_name' => $report_name))->result();
    }

    function update_collectionsheet_configuration($id, $default_value)
    {
        $data['default_value'] = $default_value;
        return $this->db->update('config_collectionsheets', $data, array('id' => $id));
    }

    function read_by_purpose($purpose)
    {
        return $this->db->get_where('tbl_config_general', array('purpose' => $purpose))->result();
    }

    function read_by_purpose_and_db_field_name($purpose,$db_field_name)
    {
        return $this->db->get_where('tbl_config_general', array('purpose' => $purpose,'db_field_name' => $db_field_name))->result_array();
    }

    function get_daily_collection_configuration($report_name)
    {
        return $this->db->get_where('config_collectionsheets', array('report_name' => $report_name))->result();
    }

    function update_daily_collection_configuration($id, $default_value)
    {
        $data['default_value'] = $default_value;
        return $this->db->update('config_collectionsheets', $data, array('id' => $id));
    }
}
