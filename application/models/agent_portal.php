<?php

class Agent_portal extends CI_Model
{
    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->library(array('session'));
    }

    public function getUserIpAddr()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            //ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            //ip pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    public function get_agents_portal_info($limit, $offset, $value = '')
    {
        $this->db->select('company_details.*,c.name as contact_person_id,i.name as allocate_branch_id,e.name as engagement_officer_id');
        $this->db->from('company_details');

        $this->db->join('tbl_employee AS c', 'company_details.contact_person_id=c.id', 'left');
        $this->db->join('tbl_branch AS i', 'company_details.allocate_branch_id=i.id', 'left');
        $this->db->join('tbl_employee AS e', 'company_details.engagement_officer_id=e.id', 'left');

        $this->db->order_by("id", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_agents_portal_by_unit_id($id)
    {
        return $this->db->where('id', $id)->get('company_details')->row();
    }


    public function get_agents_info($limit, $offset, $value = '')
    {
        $this->db->select('agents.*,e.name as admission_officer_name');
        $this->db->from('agents');
        $this->db->join('tbl_employee AS e', 'agents.admission_officer=e.id', 'left');
        if (isset($value['code']) && $value['code'] != '') {
             $this->db->where('agents.code', $value['code']);
         }
        if (isset($value['name']) && $value['name'] != '') {
              $where_con="(agents.company_name LIKE '%".$value['name']."%' OR agents.personal_name LIKE '%".$value['name']."%' OR agents.phone LIKE '%".$value['name']."%' OR agents.email LIKE '%".$value['name']."%')";
              $this->db->where($where_con);
        }
        $this->db->order_by("id", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_agents_info_by_unit_id($id)
    {
        return $this->db->where('id', $id)->get('agents')->row();
    }
   public function get_admission_officer_details($agent_id)
    {
      $result=$this->db->query("SELECT e.`name`,e.`email`,e.`mobile`,b.`name` AS branch_name,p.`name` AS post,e.photo_location,e.country_code FROM `agents` AS a
                        INNER JOIN `tbl_employee` AS e ON a.`admission_officer`=e.`id`
                        LEFT JOIN `tbl_branch` AS b ON e.`branch_id`=b.`id`
                        LEFT JOIN `tbl_employee_post` AS p ON e.post=p.`id`
                        WHERE a.`id`=$agent_id")->result_array();
                        return $result;
    }
}
