<?php

class Tailor extends CI_Model
{

    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->library(array('session'));
    }



    function get_all_tailor_list($limit, $offset, $value = '')
    {
        $this->db->select('t.*,d.name as division_name,a.name as area_name');
        $this->db->from('tbl_tailor_entry as t');
        $this->db->join('tbl_division AS d', 't.division_id=d.id');
        $this->db->join('tbl_area AS a', 't.area_id=a.id');

        if (isset($value['tailor_id']) && !empty($value) && $value['tailor_id'] != '') {
            $this->db->where('t.id', $value['tailor_id']);
        }
        if (isset($value) && !empty($value) && isset($value['name']) && $value['name'] != '') {
            $this->db->like('t.name', $value['name']);
            $this->db->or_like('t.code', $value['name']);
        }
        $this->db->order_by("t.id", "asc");
        if (isset($limit) && $limit > 0)
            $this->db->limit($limit, $offset);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function get_tailor_info_by_id($id)
    {
        return $this->db->where('id', $id)->get('tbl_tailor_entry')->row();
    }
    public function update_tailor_info($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update('tbl_tailor_entry', $data);
    }

    public function add_tailor_info($data)
    {
        return $this->db->insert('tbl_tailor_entry', $data);
    }

    function delete_tailor_info_by_tailor_id($id)
    {
        return $this->db->delete('tbl_tailor_entry', array('id' => $id));
    }
    public function get_all_divisions()
    {
        $this->db->select('*');
        $this->db->from('tbl_division');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_division_info($id)
    {
        return $this->db->where('id', $id)->get('tbl_division')->row();
    }
    public function get_all_divisions_list()
    {
                  $result=$this->db->query("SELECT * FROM `tbl_division` where status=1")->result_array();
                   return $result;
    }
    public function get_all_news_list()
    {
            $result=$this->db->query("SELECT * FROM `tbl_news` where status=1")->result_array();
             return $result;
    }
    public function get_website_contact_info()
    {
        return $this->db->get('tbl_contact')->result_array();
    }
    public function get_all_area_list_by_division_id($division_id)
    {
                  $result=$this->db->query("SELECT a.*,d.`name` AS division_name FROM `tbl_area` AS a
                                          INNER JOIN `tbl_division` AS d ON a.`division_id`=d.`id`
                                           WHERE a.`division_id`='$division_id'")->result_array();
                   return $result;
    }
    public function get_all_tailor_list_by_area_id($area_id)
    {
                  $result=$this->db->query(" SELECT t.`name`,t.`image_path`,t.`id`,a.`name` AS area_name,d.`name` AS division_name,a.`id` AS area_id,d.id as division_id FROM `tbl_tailor_entry` AS t
                                             INNER JOIN `tbl_area` AS a ON t.area_id=a.`id`
                                             INNER JOIN `tbl_division` AS d ON t.division_id=d.`id`
                                           WHERE a.`id`='$area_id'")->result_array();
                   return $result;
    }
    public function get_tailor_info_by_tailor_id($tailor_id)
    {
                  $result=$this->db->query("  SELECT t.*,a.`name` AS area_name,d.`name` AS division_name,c.`name` AS currency_name FROM `tbl_tailor_entry` AS t
                                           INNER JOIN `tbl_area` AS a ON t.area_id=a.`id`
                                           INNER JOIN `tbl_division` AS d ON t.division_id=d.`id`
                                           LEFT JOIN `cc_currencies` AS c ON t.`cc_currency_id`=c.`id`
                                           WHERE t.`id`='$tailor_id'")->row();
                   return $result;
    }
    public function get_tailor_shop_amenities_details_by_tailor_id($tailor_id)
    {
                  $result=$this->db->query("SELECT name,font_awosome FROM `tbl_shop_amenities_details`AS d
                                            INNER JOIN `tbl_shop_amenities` AS s ON d.`shop_amenities_id`=s.`id`
                                            WHERE d.`tailor_id`='$tailor_id'")->result_array();
                   return $result;
    }
    public function get_all_tailor_list_by_tailor_id($tailor_id)
    {
                  $result=$this->db->query("SELECT t.`name`,t.`image_path`,t.`id`,a.`name` AS area_name,d.`name` AS division_name,a.`id` AS area_id,d.id AS division_id FROM `tbl_tailor_entry` AS t
                                             INNER JOIN `tbl_area` AS a ON t.area_id=a.`id`
                                             INNER JOIN `tbl_division` AS d ON t.division_id=d.`id`
                                             WHERE t.`area_id` =(SELECT `area_id` FROM `tbl_tailor_entry` WHERE id='$tailor_id') AND t.`id`!='$tailor_id'")->result_array();
                   return $result;
    }
    public function get_tailor_item_details_by_tailor_id($tailor_id)
    {
                  $result=$this->db->query("SELECT i.*,d.price,d.tailor_id FROM `tbl_tailor_item_details` AS d
                                            INNER JOIN `tbl_tailor_item` AS i ON d.`item_id`=i.`id`
                                            WHERE d.`tailor_id`='$tailor_id'")->result_array();
                   return $result;
    }
    public function get_all_tailor_order()
    {
                  $result=$this->db->query("SELECT * FROM `tbl_tailor_order` AS o
                                            INNER JOIN `tbl_tailor_entry` AS t ON o.`tailor_id`=t.`id`")->result_array();
                   return $result;
    }
    public function get_tailor_order_details_by_orderid($order_id)
    {
                  $result=$this->db->query("SELECT d.*,t.`name` AS tailor_name,o.`booking_date`,o.`booking_time`,o.`customer_email`,o.`customer_name`,o.`remarks`,o.`customer_phone`,o.status,o.transaction_id
,o.`shipping_address` ,o.`total_cost` AS grand_total ,i.`measurement_level_1`,i.`measurement_level_2`,i.`measurement_level_3`,i.`measurement_level_4`,i.`measurement_level_5`,i.`measurement_level_6`
                            FROM `tbl_tailor_order` AS o
                            INNER JOIN `tbl_tailor_order_details` AS d ON o.`id`=d.`tailor_order_id`
                            INNER JOIN `tbl_tailor_entry` AS t ON o.`tailor_id`=t.`id`
                            INNER JOIN tbl_tailor_item as i on d.item_id=i.id
                            WHERE o.`id`='$order_id'")->result_array();
                   return $result;
    }
    function get_all_tailor_order_list($limit, $offset, $value = '')
    {

        $this->db->select('o.*,t.`name` AS tailor_name');
        $this->db->from('tbl_tailor_order AS o');
        $this->db->join('tbl_tailor_entry AS t', 'o.tailor_id=t.id');
        //
        if (isset($value['tailor_id']) && !empty($value) && $value['tailor_id'] != '') {
            $this->db->where('t.id', $value['tailor_id']);
        }
        if (isset($value['order_status']) && !empty($value) && $value['order_status'] != '') {
          if($value['order_status'] != 'all')
          {
              $this->db->where('o.status', $value['order_status']);
          }

        }
        if (isset($value) && !empty($value) && isset($value['customer_name']) && $value['customer_name'] != '') {
            $this->db->like('t.name', $value['customer_name']);
            $this->db->or_like('o.customer_name', $value['customer_name']);
            $this->db->or_like('o.customer_phone', $value['customer_name']);
        }

       $this->db->order_by('o.booking_date', 'desc');
        if (isset($limit) && $limit > 0)
            $this->db->limit($limit, $offset);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function get_tailor_milestone()
    {
                  $result=$this->db->query("SELECT * FROM `tbl_tailor_milestone` AS o")->result_array();
                   return $result;
    }
    public function get_all_tailor_customer_review()
    {
                  $result=$this->db->query("SELECT * FROM `tbl_tailor_customer_review` AS o")->result_array();
                   return $result;
    }
    public function get_blog_details_info($id)
    {
        return $this->db->where('id', $id)->get('tbl_news')->row();
    }
    public function get_contact_info()
    {
        return $this->db->get('tbl_contact')->row();
    }
    public function update_tailor_order_info($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update('tbl_tailor_order', $data);
    }
    function get_tailor_order_list_by_transaction_id($transaction_id)
    {

        $this->db->select('o.*,t.`name` AS tailor_name');
        $this->db->from('tbl_tailor_order AS o');
        $this->db->join('tbl_tailor_entry AS t', 'o.tailor_id=t.id');
        $this->db->like('o.transaction_id', $transaction_id);
        $this->db->order_by('o.booking_date', 'desc');
        $query = $this->db->get();
        return $query->result_array();
    }
    function get_tailor_item_list_by_random()
    {

        $this->db->select('name,`image_path_1`,`image_path_2`,`image_path_3`,`image_path_4`,`image_path_5`');
        $this->db->from('tbl_tailor_item');
        $this->db->order_by('rand()');
         $this->db->limit(5);
        $query = $this->db->get();
        $data=  $query->result_array();
        $image_array = array();
        $i=0;
        foreach ($data as  $value) {
          // $image_array['name'][$i]=$value['name'];
           $image_array[$i]=$value['image_path_1'];
           $i++;
           if($value['image_path_2']!='')
           {
             $image_array[$i]=$value['image_path_2'];
             $i++;
           }
           if($value['image_path_3']!='')
           {
             $image_array[$i]=$value['image_path_3'];
             $i++;
           }
           if($value['image_path_4']!='')
           {
             $image_array[$i]=$value['image_path_4'];
             $i++;
           }
           if($value['image_path_5']!='')
           {
             $image_array[$i]=$value['image_path_5'];
             $i++;
           }
        }
        return $image_array;
    }
    public function get_all_area_list_by_area_name($area_name)
    {
                  $result=$this->db->query("SELECT a.*,d.`name` AS division_name FROM `tbl_area` AS a
                                          INNER JOIN `tbl_division` AS d ON a.`division_id`=d.`id`
                                           WHERE a.`name` LIKE '%$area_name%' OR d.`name` LIKE '%$area_name%'")->result_array();
                   return $result;
    }
}
