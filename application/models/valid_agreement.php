<?php

class Valid_agreement extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->library(array('session'));
    }

    function get_all_valid_agreement($limit, $offset, $value = '')
    {
        $this->db->select('*');
        $this->db->from('tbl_valid_agreement');
        if (isset($value) && !empty($value) &&  $value['name'] != '') {
            $this->db->like('name', $value['name']);
        }
        $this->db->order_by("name", "asc");
        if (isset($limit) && $limit > 0)
            $this->db->limit($limit, $offset);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function update_valid_agreement_info($data)
    {
        $this->db->where('id', $data['id']);
      return  $this->db->update('tbl_valid_agreement', $data);
    }

    public function add_valid_agreement_info($data)
    {
        return $this->db->insert('tbl_valid_agreement', $data);
    }
    public function get_valid_agreement_by_id($id)
    {
        return $this->db->where('id', $id)->get('tbl_valid_agreement')->row();
      //  return $this->db->where('id', $id)->get('tbl_school')->result_array();
    }
    public function checkifexist_valid_agreement_by_name($name)
    {
      $count = $this->db->query("SELECT id FROM tbl_valid_agreement where name = '$name'")->num_rows();

      if((int)$count==0)
      {
        return 0;
      }else{
        return 1;
      }

    }
    public function checkifexist_update_valid_agreement_by_name($name,$id)
    {
      $count = $this->db->query("SELECT id FROM tbl_valid_agreement where name = '$name' and id !='$id'")->num_rows();
      if((int)$count==0)
      {
        return 0;
      }else{
        return 1;
      }
    }
    function delete__valid_agreement_info_by_id($id)
    {
        return $this->db->delete('tbl_valid_agreement', array('id' => $id));
    }

}

?>
