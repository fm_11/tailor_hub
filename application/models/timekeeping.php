<?php

class Timekeeping extends CI_Model
{

    function __construct()
    {
        // Call the Model constructor
        $this->load->model(array('Employee'));
        parent::__construct();
    }

    function check_date_branch_holiday($date, $branch_id)
    {
        $sql = "SELECT h.*,ht.`name`,ht.`short_name` FROM `tbl_holiday` AS h
INNER JOIN `tbl_holiday_type` AS ht ON ht.`id` = h.`holiday_type`
				WHERE h.`branch_id` = '$branch_id' AND h.`date` = '$date'";
        $result_info = $this->db->query($sql);
        return $result_info->row();
    }

    function get_employee_movement_details($data){
      $employee_id = $data['employee_id'];
      $from_date = $data['from_date'];
      $to_date = $data['to_date'];
      $move_info = $this->db->query("SELECT * FROM `tbl_employee_movement_registers` WHERE
`employee_id` = '$employee_id' AND `date` BETWEEN '$from_date' AND '$to_date'")->result_array();
      return $move_info;
    }

    function get_employee_leave_info_by_date_employee_id($date, $employee_id)
  {
      $leave_info = $this->db->query("SELECT l.*,lt.short_name as leave_type_name FROM `tbl_employee_leave_days` AS l
        INNER JOIN `tbl_leave_types` AS lt ON lt.`id` = l.`leave_type_id`
        WHERE l.`employee_id` = '$employee_id' AND  l.`date` = '$date'
        ")->result_array();
      return $leave_info;
  }

    function get_login_info_by_employee_id($date, $employee_id)
    {
        $rows = $this->db->query("SELECT * FROM `tbl_logins` WHERE `employee_id` = '$employee_id' AND `date`= '$date'")->result_array();
        return $rows;
    }

    public function update_employee_leave_info($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update('tbl_employee_leave_applications', $data);
        return true;
    }

    function get_employee_report_absentee($date, $branch_id){

       $leave_info = $this->get_employee_leave_info($date);
       $login_info = $this->get_employee_login_info($date);

       $where = "";
       if($branch_id != ''){
         $where = " AND `tbl_employee`.`branch_id` = '$branch_id' ";
       }
       $sql = " SELECT id,branch_id FROM `tbl_employee` WHERE `tbl_employee`.`status` = '1' $where ORDER BY `tbl_employee`.id";

       $employees_info = $this->db->query($sql)->result_array();

       $result_information = array();
       $s = 0;
       foreach ($employees_info as $row) {
           $employee_id = $row['id'];
           $branch_id = $row['branch_id'];

           $check_holiday = $this->check_date_branch_holiday($date, $branch_id);

           $employee_info = $this->get_employee_information($employee_id);

           if (empty($leave_info[$employee_id]) && empty($login_info[$employee_id]) && empty($check_holiday)) {
               $result_information[$s]['name'] = $employee_info[0]['name'];
               $result_information[$s]['code'] = $employee_info[0]['code'];
               $result_information[$s]['post_name'] = $employee_info[0]['post_name'];
               $result_information[$s]['branch_name'] = $employee_info[0]['branch_name'];
               $result_information[$s]['section_name'] = $employee_info[0]['section_name'];
               $s++;
           }
       }

       //echo "<pre>";
      // print_r($result_information);
      // die;
       return $result_information;

    }

    function get_employee_information($employee_id)
    {
        $cond = array();
        $cond['employee_id'] = $employee_id;
        $result_info = $this->Employee->get_all_employees(0, 0, $cond);
        return $result_info;
    }

    function get_employee_login_info($date)
    {
        $rows = $this->db->query("SELECT `id`,`employee_id`,`date` FROM `tbl_logins` WHERE `date`= '$date'")->result_array();
        $login_info = array();
        foreach ($rows as $row) {
            $login_info[$row['employee_id']]['id'] = $row['id'];
            $login_info[$row['employee_id']]['employee_id'] = $row['employee_id'];
            $login_info[$row['employee_id']]['date'] = $row['date'];
        }
        return $login_info;
    }

    function get_employee_leave_info($date)
   {
       $rows = $this->db->query("SELECT * FROM `tbl_employee_leave_applications` AS t WHERE  t.`from_date` <= '$date' AND t.`to_date` >= '$date'")->result_array();
       $leave_info = array();
       foreach ($rows as $row) {
           $leave_info[$row['employee_id']]['id'] = $row['id'];
           $leave_info[$row['employee_id']]['employee_id'] = $row['employee_id'];
           $leave_info[$row['employee_id']]['from_date'] = $row['from_date'];
           $leave_info[$row['employee_id']]['to_date'] = $row['to_date'];
       }
       return $leave_info;
   }


    function get_leave_types(){
        return $this->db->query("SELECT * FROM `tbl_leave_types` ORDER BY id")->result_array();
      }

      function get_employee_leave_data($data){
          $year = $data['year'];
          $from_date = $data['from_date'];
          $to_date = $data['to_date'];
          $branch_id = $data['branch_id'];

          $where = "";
          if($branch_id != '' && $branch_id != 'all'){
             $where = " AND e.branch_id = '$branch_id' ";
          }
          $sql = "SELECT e.id,e.branch_id,e.name,e.code,p.name AS post_name FROM `tbl_employee` as e
          LEFT JOIN `tbl_employee_post` AS p on p.id  = e.post
          WHERE e.`status` = '1' $where ORDER BY e.id";
          $employees_info = $this->db->query($sql)->result_array();
          $leave_types = $this->get_leave_types();

          $result_information = array();
          $s = 0;
          foreach ($employees_info as $row) {
              $employee_id = $row['id'];
              $leave_data = array();
              $i = 0;
              foreach ($leave_types as $type_row):
                $leave_type_id = $type_row['id'];
                $leave_data[$i]['short_name'] = $type_row['short_name'];
                $leave_data[$i]['allocated_days'] = $type_row['allocated_days'];

                $total_used_leave = $this->db->query("SELECT SUM(`num_of_leave_days`) AS total_num_of_leave_days FROM `tbl_employee_leave_applications` WHERE
                 `from_date` >= '$from_date' AND  `to_date` <= '$to_date' AND `status` = 'A' AND `employee_id` = $employee_id AND `leave_type_id` = $leave_type_id")->result_array();
                if($total_used_leave[0]['total_num_of_leave_days'] == ''){
                  $leave_data[$i]['total_used_leave'] = 0;
                }else{
                  $leave_data[$i]['total_used_leave'] = $total_used_leave[0]['total_num_of_leave_days'];
                }

                $result_information[$s]['name'] = $row['name'];
                $result_information[$s]['code'] = $row['code'];
                $result_information[$s]['post_name'] = $row['post_name'];
                $result_information[$s]['leave_data'] = $leave_data;
                $i++;
              endforeach;
              $s++;
          }


        return $result_information;
      }

  	function get_employee_attendance_details($data)
      {
          $month = $data['month'];
          $year = $data['year'];
          $number_of_days = $data['number_of_days'];

          $where = "";
          if (isset($data['employee_id']) && !empty($data) && $data['employee_id'] != '') {
              $employee_id = $data['employee_id'];
              $where = " AND e.id = '$employee_id' ";
          }else{
            $branch_id = $data['branch_id'];
            if($branch_id != '' && $branch_id != 'all'){
               $where = " AND e.branch_id = '$branch_id' ";
            }
          }

          $sql = "SELECT e.id,e.branch_id,e.name,e.code,p.name AS post_name FROM `tbl_employee` as e
          LEFT JOIN `tbl_employee_post` AS p on p.id  = e.post
          WHERE e.`status` = '1' $where ORDER BY e.id";

          $employees_info = $this->db->query($sql)->result_array();



          $result_information = array();
          $s = 0;
          foreach ($employees_info as $row) {

              $total_days = $number_of_days;
              $total_working_days = 0;
              $total_present_days = 0;
              $total_absent_days = 0;
              $total_leave_days = 0;
              $total_holidays = 0;

              $employee_id  = $row['id'];
              $employee_branch_id = $row['branch_id'];
              $employee_info = $this->get_employee_information($employee_id);

              $status = "";
              $status_array = array();
              $i = 1;
              while ($i <= $number_of_days) {
                  if ($i < 10) {
                      $date = $year . '-' . $month . '-0' . $i;
                  } else {
                      $date = $year . '-' . $month . '-' . $i;
                  }

                  $login_info = $this->get_login_info_by_employee_id($date, $employee_id);
                  if (!empty($login_info)) {
                      $total_present_days = $total_present_days + 1;
                      $t =  "";
                      if($login_info[0]['login_time'] != ''){
                        $t .= date('h:i:s A', strtotime($login_info[0]['login_time']))."<br>";
                        $status_array[$i]['login_time'] = date('h:i:s A', strtotime($login_info[0]['login_time']));
                      }else{
                        $t .= "-<br>";
                        $status_array[$i]['login_time'] = "-";
                      }

                      if($login_info[0]['logout_time'] != ''){
                        $t .= date('h:i:s A', strtotime($login_info[0]['logout_time']));
                        $status_array[$i]['logout_time'] = date('h:i:s A', strtotime($login_info[0]['logout_time']));
                      }else{
                        $t .= "-";
                        $status_array[$i]['logout_time'] = "-";
                      }

                      $status_array[$i]['status'] = "P";
                      $status_array[$i]['date'] = $date;

                      $status .= "<td>" . $t . "</td>";
                  } else {
                      $check_holiday = $this->check_date_branch_holiday($date, $employee_branch_id);
                      if (!empty($check_holiday)) {
                          $total_holidays = $total_holidays + 1;
                          $status_array[$i]['status'] = $check_holiday->short_name;
                          $status_array[$i]['login_time'] = "-";
                          $status_array[$i]['logout_time'] = "-";
                          $status_array[$i]['date'] = $date;
                          $status .= "<td>". $check_holiday->short_name ."</td>";
                      } else {
                          $leave_info = $this->get_employee_leave_info_by_date_employee_id($date, $employee_id);
                          if (!empty($leave_info)) {
                              $total_leave_days = $total_leave_days + 1;
                              $status_array[$i]['status'] = $leave_info[0]['leave_type_name'];
                              $status_array[$i]['login_time'] = "-";
                              $status_array[$i]['logout_time'] = "-";
                              $status_array[$i]['date'] = $date;
                              $status .= "<td>". $leave_info[0]['leave_type_name'] ."</td>";
                          } else {
                              $total_absent_days = $total_absent_days + 1;
                              $status_array[$i]['status'] = "A";
                              $status_array[$i]['login_time'] = "-";
                              $status_array[$i]['logout_time'] = "-";
                              $status_array[$i]['date'] = $date;
                              $status .= "<td>A</td>";
                          }
                      }
                  }
                  $i++;
              }
              //echo $status;
              //die;
              $total_working_days = $total_present_days + $total_absent_days + $total_leave_days;


              $result_information[$s]['total_days'] = $total_days;
              $result_information[$s]['total_working_days'] = $total_working_days;
              $result_information[$s]['total_present_days'] = $total_present_days;
              $result_information[$s]['total_absent_days'] = $total_absent_days;
              $result_information[$s]['total_leave_days'] = $total_leave_days;
              $result_information[$s]['total_holidays'] = $total_holidays;

              $result_information[$s]['name'] = $employee_info[0]['name'];
              $result_information[$s]['code'] = $employee_info[0]['code'];
              $result_information[$s]['post_name'] = $employee_info[0]['post_name'];
              $result_information[$s]['data'] = $status;
              $result_information[$s]['data_array'] = $status_array;
              $s++;
          }
        // echo "<pre>";
        //   print_r($result_information);
        //  echo "</pre>";
        //  die;
          return $result_information;
      }



    function get_all_employee_timekeeping_list($limit, $offset, $value = '')
    {
        $this->db->select('tbl_logins.*,e.name,e.code,tp.name as post_name');
        $this->db->from('tbl_logins');
        $this->db->join('tbl_employee AS e', 'tbl_logins.employee_id=e.id');
        $this->db->join('tbl_employee_post AS tp', 'e.post=tp.id');

        if (isset($value) && !empty($value['date']) && $value['date'] != '') {
            $this->db->where('tbl_logins.date', $value['date']);
        }

        if (isset($value) && !empty($value['branch_id']) && $value['branch_id'] != '') {
            $this->db->where('tbl_logins.branch_id', $value['branch_id']);
        }

        $this->db->order_by("tp.shorting_order", "asc");
        if (isset($limit) && $limit > 0)
            $this->db->limit($limit, $offset);
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_all_student_timekeeping_list($limit, $offset, $value = '')
    {
        $this->db->select('tbl_logins.*,s.name as student_name,c.name as class_name,sc.name as section_name');
        $this->db->from('tbl_logins');
        $this->db->join('tbl_student AS s', 'tbl_logins.person_id=s.id');
        $this->db->join('tbl_class AS c', 's.class_id=c.id', 'left');
        $this->db->join('tbl_section AS sc', 's.section_id=sc.id', 'left');
        if (isset($value) && !empty($value['date'])) {
            $this->db->where('tbl_logins.date', $value['date']);
        }
        if (isset($value) && !empty($value['person_id']) && $value['person_id'] != '') {
            $this->db->where('tbl_logins.person_id', $value['person_id']);
        }
        $this->db->where('tbl_logins.person_type', $value['person_type']);
        $this->db->order_by("tbl_logins.id", "desc");
        if (isset($limit) && $limit > 0)
            $this->db->limit($limit, $offset);
        $query = $this->db->get();
        return $query->result_array();
    }


    function get_all_movement_register_list($limit, $offset, $value = '')
    {
        $this->db->select('m.*,e.name,e.code as employee_code');
        $this->db->from('tbl_employee_movement_registers AS m');
        $this->db->join('tbl_employee AS e', 'm.employee_id=e.id');
        if (isset($value) && !empty($value['name']) && $value['name'] != '') {
            $this->db->like('e.name', $value['name']);
        }

        if (isset($value) && !empty($value['date']) && $value['date'] != '') {
            $this->db->where('m.date', $value['date']);
        }

        if (isset($value) && !empty($value['employee_id']) && $value['employee_id'] != '') {
            $this->db->where('m.employee_id', $value['employee_id']);
        }

        $this->db->order_by("m.date", "desc");
        if (isset($limit) && $limit > 0)
            $this->db->limit($limit, $offset);
        $query = $this->db->get();
        return $query->result_array();
    }



    function get_all_employee_leave_list($limit, $offset, $value = '')
    {
        $this->db->select('tbl_employee_leave_applications.*,e.name,e.code,lt.name as leave_type');
        $this->db->from('tbl_employee_leave_applications');
        $this->db->join('tbl_employee AS e', 'tbl_employee_leave_applications.employee_id=e.id');
        $this->db->join('tbl_leave_types AS lt', 'tbl_employee_leave_applications.leave_type_id=lt.id');

        if (isset($value) && !empty($value['employee_id']) && $value['employee_id'] != '') {
            $this->db->where('tbl_employee_leave_applications.employee_id', $value['employee_id']);
        }

        if (isset($value) && !empty($value['approver_employee_id']) && $value['approver_employee_id'] != '') {
            $this->db->where('tbl_employee_leave_applications.reporting_boss_id', $value['approver_employee_id']);
        }

        if (isset($value) && !empty($value['status']) && $value['status'] != '') {
            $this->db->where('tbl_employee_leave_applications.status', $value['status']);
        }

        $this->db->order_by("tbl_employee_leave_applications.id", "desc");
        if (isset($limit) && $limit > 0)
            $this->db->limit($limit, $offset);
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_all_staff_leave_list($limit, $offset, $value = '')
    {
        $this->db->select('tbl_staff_leave_applications.*,s.name');
        $this->db->from('tbl_staff_leave_applications');
        $this->db->join('tbl_staff_info AS s', 'tbl_staff_leave_applications.staff_id=s.id');
        $this->db->where('tbl_staff_leave_applications.is_deleted', '0');
        $this->db->order_by("tbl_staff_leave_applications.id", "desc");
        if (isset($limit) && $limit > 0)
            $this->db->limit($limit, $offset);
        $query = $this->db->get();
        return $query->result_array();
    }


    function get_all_staff_timekeeping_list($limit, $offset, $value = '')
    {
        $this->db->select('tbl_logins.*,s.name as name,s.staff_index_no as index_no,tp.name as post_name');
        $this->db->from('tbl_logins');
        $this->db->join('tbl_staff_info AS s', 'tbl_logins.person_id=s.id');
        $this->db->join('tbl_employee_post AS tp', 's.post=tp.id');
        if (isset($value) && !empty($value['date'])) {
            $this->db->where('tbl_logins.date', $value['date']);
        }

        if (isset($value) && !empty($value['person_id']) && $value['person_id'] != '') {
            $this->db->where('tbl_logins.person_id', $value['person_id']);
        }

        if (isset($value) && !empty($value['form_date']) && $value['form_date'] != '') {
            $this->db->where('tbl_logins.date >=', $value['form_date']);
            $this->db->where('tbl_logins.date <=', $value['to_date']);
        }

        $this->db->where('tbl_logins.person_type', $value['person_type']);
        $this->db->order_by("tbl_logins.date", "desc");
        $this->db->order_by("tbl_logins.person_id", "asc");

        if (isset($limit) && $limit > 0)
            $this->db->limit($limit, $offset);
        $query = $this->db->get();
        return $query->result_array();
    }


    function get_all_holiday_list($limit, $offset, $value = '')
    {
        $this->db->select('h.*,ht.name as holiday_name,b.name as branch_name');
        $this->db->from('tbl_holiday as h');
        $this->db->join('tbl_holiday_type AS ht', 'h.holiday_type=ht.id');
        $this->db->join('tbl_branch AS b', 'h.branch_id=b.id');
        if (isset($value) && !empty($value) && $value['from_date'] != '' && $value['to_date'] != '') {
            $from_date = $value['from_date'];
            $to_date = $value['to_date'];
            $this->db->where("h.date between '$from_date' and '$to_date' ");
        }
        $this->db->order_by("h.branch_id", "asc");
        $this->db->order_by("h.date", "asc");
        if (isset($limit) && $limit > 0)
            $this->db->limit($limit, $offset);
        $query = $this->db->get();
        return $query->result_array();
    }


    function get_employee_staff_report_absentee($date, $person_type)
    {
        if ($person_type == 'T') {
            $leave_info = $this->get_employee_leave_info($date);
            $sql = " SELECT id AS person_id
				 FROM `tbl_employee` WHERE `tbl_employee`.`status` = '1' ORDER BY `tbl_employee`.id";
        } else {
            $leave_info = $this->get_staff_leave_info($date);
            $sql = " SELECT id AS person_id
				 FROM `tbl_staff_info`";
        }
        $persons_info = $this->db->query($sql)->result_array();
        $persons_table = array();
        foreach ($persons_info as $info) {
            $persons_table[] = $info['person_id'];
        }
        //echo "<br /><br /><pre>";print_r($persons_table); echo "</pre>"; die;

        $login_info = $this->get_login_info($date, $person_type);

        $result_information = array();
        $s = 0;
        foreach ($persons_table as $person_id) {
            $person_info = $this->get_persons_information($person_id, $person_type);
            if (empty($leave_info[$person_id]) && empty($login_info[$person_id])) {
                $result_information[$s]['name'] = $person_info[0]['name'];
                if ($person_type == 'T') {
                    $result_information[$s]['index_no'] = $person_info[0]['employee_index_no'];
                } else {
                    $result_information[$s]['index_no'] = $person_info[0]['staff_index_no'];
                }
                $result_information[$s]['post_name'] = $person_info[0]['post_name'];
                $s++;
            }
        }
//        echo "<pre>";
//        print_r($result_information);
//        echo "</pre>";
//        die;
        return $result_information;
    }




    function get_staff_leave_info($date)
    {
        $rows = $this->db->query("SELECT * FROM `tbl_staff_leave_applications` AS t WHERE t.`is_deleted` = 0 AND t.`date_from` <= '$date' AND t.`date_to` >= '$date'")->result_array();
        $leave_info = array();
        foreach ($rows as $row) {
            $leave_info[$row['staff_id']]['id'] = $row['id'];
            $leave_info[$row['staff_id']]['staff_id'] = $row['staff_id'];
            $leave_info[$row['staff_id']]['date_from'] = $row['date_from'];
            $leave_info[$row['staff_id']]['date_to'] = $row['date_to'];
        }
        return $leave_info;
    }






    function check_holiday($date)
    {
        $sql = "SELECT *
				FROM `tbl_holiday`
				WHERE `date` = '$date'";
        $result_info = $this->db->query($sql);
        return $result_info->result();
    }


    function get_student_report_absentee($date,$class_id,$section_id,$group)
    {
        $leave_info = $this->get_student_leave_info($date,$class_id,$section_id,$group);
        $sql = "SELECT s.id FROM `tbl_student` AS s WHERE s.`status` = '1' AND s.`class_id` = '$class_id' AND s.`section_id` = '$section_id' AND s.`group` = '$group'  ORDER BY s.roll_no";

        $students_info = $this->db->query($sql)->result_array();
        $students_table = array();
        foreach ($students_info as $info) {
            $students_table[] = $info['id'];
        }
        //echo "<br /><br /><pre>";print_r($students_table); echo "</pre>"; die;
        $person_type = 'S';
        $login_info = $this->get_login_info($date, $person_type);

        $result_information = array();
        $s = 0;
        foreach ($students_table as $student_id) {
            $person_info = $this->get_persons_information($student_id, $person_type);
            if (empty($leave_info[$student_id]) && empty($login_info[$student_id])) {
                $result_information[$s]['name'] = $person_info[0]['name'];
                $result_information[$s]['roll_no'] = $person_info[0]['roll_no'];
                $result_information[$s]['reg_no'] = $person_info[0]['reg_no'];
                $result_information[$s]['guardian_mobile'] = $person_info[0]['guardian_mobile'];
                $s++;
            }
        }
//        echo "<pre>";
//        print_r($result_information);
//        echo "</pre>";
//        die;
        return $result_information;
    }


    function get_student_leave_info($date,$class_id,$section_id,$group)
    {
        $rows = $this->db->query("SELECT * FROM `tbl_student_leave_applications` AS s WHERE s.`class_id` = '$class_id' AND s.`section_id` = '$section_id' AND s.`group` = '$group' AND s.`is_deleted` = 0 AND  s.`date_from` <= '$date' AND s.`date_to` >= '$date'")->result_array();
        $leave_info = array();
        foreach ($rows as $row) {
            $leave_info[$row['student_id']]['id'] = $row['id'];
            $leave_info[$row['student_id']]['student_id'] = $row['student_id'];
            $leave_info[$row['student_id']]['date_from'] = $row['date_from'];
            $leave_info[$row['student_id']]['date_to'] = $row['date_to'];
        }
        return $leave_info;
    }


}

?>
