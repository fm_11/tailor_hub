<?php

class Applications_Status_Details extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->library(array('session'));
    }

    function get_all_application_status_details($limit, $offset, $value = '')
    {
        $this->db->select('*');
        $this->db->from('tbl_application_status');
        if (isset($value) && !empty($value) &&  $value['name'] != '') {
            $this->db->like('name', $value['name']);
        }
        $this->db->order_by("name", "asc");
        if (isset($limit) && $limit > 0)
            $this->db->limit($limit, $offset);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function update_application_status_details_info($data)
    {
        $this->db->where('id', $data['id']);
      return  $this->db->update('tbl_application_status', $data);
    }

    public function add_application_status_details_info($data)
    {
        return $this->db->insert('tbl_application_status', $data);
    }
    public function get_application_status_details_by_id($id)
    {
        return $this->db->where('id', $id)->get('tbl_application_status')->row();
      //  return $this->db->where('id', $id)->get('tbl_school')->result_array();
    }
    public function checkifexist_application_status_details_by_name($name)
    {
      $count = $this->db->query("SELECT id FROM tbl_application_status where name = '$name'")->num_rows();

      if((int)$count==0)
      {
        return 0;
      }else{
        return 1;
      }

    }
    public function checkifexist_update_application_status_details_by_name($name,$id)
    {
      $count = $this->db->query("SELECT id FROM tbl_application_status where name = '$name' and id !='$id'")->num_rows();
      if((int)$count==0)
      {
        return 0;
      }else{
        return 1;
      }
    }
    function delete_application_status_details_info_by_id($id)
    {
        return $this->db->delete('tbl_application_status', array('id' => $id));
    }

}

?>
