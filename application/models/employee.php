<?php

class Employee extends CI_Model
{

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function get_all_employees($limit, $offset, $value = '')
    {
        $this->db->select('tbl_employee.*,tp.name as post_name,ts.name as section_name, b.name as branch_name, b.id as branch_id, tz.value as time_zone');
        $this->db->from('tbl_employee');
        $this->db->join('tbl_branch AS b', 'tbl_employee.branch_id=b.id');
        $this->db->join('tbl_employee_post AS tp', 'tbl_employee.post=tp.id');
        $this->db->join('tbl_employee_section AS ts', 'tbl_employee.section=ts.id');
        $this->db->join('tbl_timezone AS tz', 'b.time_zone_id=tz.id');
        if (isset($value['status']) && !empty($value) && $value['status'] != '') {
            $this->db->where('tbl_employee.status', $value['status']);
        }
        if (isset($value['employee_id']) && !empty($value) && $value['employee_id'] != '') {
            $this->db->where('tbl_employee.id', $value['employee_id']);
        }

        if (isset($value) && !empty($value) && isset($value['name']) && $value['name'] != '') {
            $this->db->like('tbl_employee.name', $value['name']);
            $this->db->or_like('tbl_employee.code', $value['name']);
        }

        $this->db->order_by("tbl_employee.id", "asc");
        if (isset($limit) && $limit > 0)
            $this->db->limit($limit, $offset);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function update_employee_status($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update('tbl_employee', $data);
    }


    public function update_employee_info($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update('tbl_employee', $data);
    }

    public function add_employee_info($data)
    {
        return $this->db->insert('tbl_employee', $data);
    }

    function get_employee_photo_info_by_employee_id($id)
    {
        $this->db->select('tbl_employee.*,tp.name as post_name,ts.name as section_name,b.`name` AS branch_name,s.`name` AS shift_name,e.`name` AS reporting_boss,e.code as repo_code');
        $this->db->from('tbl_employee');
        $this->db->join('tbl_employee_post AS tp', 'tbl_employee.post=tp.id');
        $this->db->join('tbl_employee_section AS ts', 'tbl_employee.section=ts.id');
        $this->db->join('tbl_branch AS b', 'tbl_employee.branch_id=b.id','left');
        $this->db->join('tbl_employee AS e', 'tbl_employee.reporting_boss_id=e.id','left');
        $this->db->join('tbl_shift AS s', 'tbl_employee.shift_id=s.id','left');
        $this->db->where('tbl_employee.id', $id);
        $query = $this->db->get();
        return $query->result_array();

    }

    function get_staff_photo_info_by_staff_id($id)
    {
        return $this->db->where('id', $id)->get('tbl_staff_info')->result_array();
    }

    function delete_employee_info_by_employee_id($id)
    {
        return $this->db->delete('tbl_employee', array('id' => $id));
    }

    function getEmployeeName($employee_id){
      return $this->db->query("SELECT e.`name`,e.`code`,p.`name` as designation_name,b.`name` as branch_name,e.branch_id
        FROM `tbl_employee` as e
        LEFT JOIN `tbl_employee_post` as p ON p.`id` = e.`post`
        LEFT JOIN `tbl_branch` as b ON b.`id` = e.`branch_id`
        WHERE e.id = $employee_id")->result_array();
    }

    function delete_staff_info_by_staff_id($id)
    {
        return $this->db->delete('tbl_staff_info', array('id' => $id));
    }

    function update_staff_info($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update('tbl_staff_info', $data);
    }

}

?>
