
<?php
class Email_process extends CI_Model
{

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function send_email($receivers,$header,$message){
        $this->load->config('email');
        $this->load->library('email');
        $this->load->library('encrypt');

        $from = $this->config->item('smtp_user');
        $to = $receivers;
        $subject = $header;
        $message = $message;

        $this->email->set_newline("\r\n");
        $this->email->from($from);
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($message);

        if ($this->email->send()) {
            return true;
        } else {
            show_error($this->email->print_debugger());
        }
    }


}
