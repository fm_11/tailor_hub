<?php

class Admin_login extends CI_Model
{
    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->library(array('session'));
    }

    public function get_contact()
    {
        return $this->db->get('tbl_contact')->row();
    }

    public function update_contact($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update('tbl_contact', $data);
    }

    public function get_milestone_info()
    {
        return $this->db->get('tbl_tailor_milestone')->row();
    }

    public function update_milestone_info($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update('tbl_tailor_milestone', $data);
    }

    public function get_customer_review($id)
    {
        return $this->db->where('id', $id)->get('tbl_tailor_customer_review')->row();
    }

    //Divisions

    public function get_division_info($id)
    {
        return $this->db->where('id', $id)->get('tbl_division')->row();
    }

    public function get_shop_info($id)
    {
        return $this->db->where('id', $id)->get('tbl_shop_amenities')->row();
    }

    public function get_about_us_info($id)
    {
        return $this->db->where('id', $id)->get('tbl_about_us')->row();
    }

    public function get_contacts_info($id)
    {
        return $this->db->where('id', $id)->get('tbl_contact')->row();
    }

    public function get_news_info($id)
    {
        return $this->db->where('id', $id)->get('tbl_news')->row();
    }

    public function get_area_info($id)
    {
        return $this->db->where('id', $id)->get('tbl_area')->row();
    }

    public function get_area_infos($id)
    {
        return $this->db->where('id', $id)->get('tbl_area')->result_array();
    }

    public function get_all_divisions($limit, $offset, $value = '')
    {
        $this->db->select('*');
        $this->db->from('tbl_division');

        $this->db->order_by("id", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getUserIpAddr()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            //ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            //ip pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    public function get_application_list($limit, $offset, $value = '')
    {
        $this->db->select('ua.*,cor.course_title as course_name,aas.name as current_status_name,ap.applicant_code,
        c.name as country_name,i.name as institute_name,ap.first_name, ap.last_name,cim.name as intake_months,
        ciy.name as intake_years,cls.name as lead_source,te.name as admission_officer,ap.id as applicantid,tble.name as process_officer');
        $this->db->from('university_application as ua');
        $this->db->join('applicants AS ap', 'ap.id=ua.applicant_id');

        $this->db->join('cc_countries AS c', 'c.id=ua.country_id');
        $this->db->join('institutions AS i', 'i.id=ua.institution_id');
        $this->db->join('courses AS cor', 'cor.id=ua.subject_id', 'left');
        $this->db->join('tbl_application_status AS aas', 'aas.id=ua.current_status', 'left');
        $this->db->join('cc_intake_months AS cim', 'ua.intake_month=cim.id', 'left');
        $this->db->join('cc_intake_years AS ciy', 'ua.intake_year=ciy.id', 'left');
        $this->db->join('cc_lead_sources AS cls', 'ap.lead_source_id=cls.id', 'left');
        $this->db->join('tbl_user AS tu', 'ap.admission_officer_id=tu.id', 'left');
        $this->db->join('tbl_employee AS te', 'tu.employee_id=te.id', 'left');
        $this->db->join('tbl_user AS tblu', 'ua.process_officer_id=tblu.id', 'left');
        $this->db->join('tbl_employee AS tble', 'tblu.employee_id=tble.id', 'left');
        if (isset($value['application_status']) && $value['application_status'] != '') {
            $this->db->where('ua.application_status', $value['application_status']);
        }

        if (isset($value['process_officer_id']) && $value['process_officer_id'] != '') {
            $this->db->where('ua.process_officer_id', $value['process_officer_id']);
        }
        if (isset($value['admission_officer']) && $value['admission_officer'] != '') {
            $this->db->where('ap.admission_officer_id', $value['admission_officer']);
        }
        // if (isset($value) && !empty($value) && isset($value['applicant_name']) && $value['applicant_name'] != '') {
        //     $this->db->like('ap.first_name', $value['applicant_name']);
        //     $this->db->or_like('ap.last_name', $value['applicant_name']);
        //     $this->db->or_like('ap.applicant_code', $value['applicant_name']);
        // }

           if (isset($value['country_id']) && $value['country_id'] != '') {
                $this->db->where('c.id', $value['country_id']);
            }
            if (isset($value['institutions_id']) && $value['institutions_id'] != '') {
                $this->db->where('i.id', $value['institutions_id']);
            }

            if (isset($value['courses_id']) && $value['courses_id'] != '') {
                $this->db->where('cor.id', $value['courses_id']);
            }
            if (isset($value['intake_year_id']) && $value['intake_year_id'] != '') {
                $this->db->where('ua.intake_year', $value['intake_year_id']);
            }
            if (isset($value['intake_month_id']) && $value['intake_month_id'] != '') {
                $this->db->where('ua.intake_month', $value['intake_month_id']);
            }
            if (isset($value['application_status_id']) && $value['application_status_id'] != '') {
                 $this->db->where('aas.id', $value['application_status_id']);
             }
            if (isset($value['name']) && $value['name'] != '') {
                  $where_con="(ap.first_name LIKE '%".$value['name']."%' OR ap.last_name LIKE '%".$value['name']."%' OR ap.applicant_code LIKE '%".$value['name']."%' OR ap.phone LIKE '%".$value['name']."%' OR ap.email LIKE '%".$value['name']."%')";
                  $this->db->where($where_con);
            }
        $this->db->order_by("ua.id", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }


    public function getReportHeaderAddress($branch_id)
    {
        $org_info =  $this->db->get('tbl_contact_info')->result_array();
        if ($branch_id != null &&  $branch_id != '' && $branch_id != 'all') {
            $branch_info = $this->db->where('id', $branch_id)->get('tbl_branch')->result_array();
        }

        //echo '<pre>';
        //print_r($branch_info);
        //die;

        $data = array();
        $data['org_name'] = $org_info[0]['org_name'];
        if (isset($branch_info)) {
            $data['address'] = $branch_info[0]['address'];
            $data['email'] = $branch_info[0]['email'];
            $data['branch_name'] = $branch_info[0]['name'];
        } else {
            $data['address'] = $org_info[0]['address'];
            $data['email'] = $org_info[0]['email'];
            $data['branch_name'] = "All Branch";
        }
        return $data;
    }


    public function get_all_notices($limit, $offset, $value = '')
    {
        $this->db->select('tbl_notice.*,e.name');
        $this->db->from('tbl_notice');
        $this->db->join('tbl_employee AS e', 'e.id=tbl_notice.employee_id');
        $this->db->order_by("tbl_notice.id", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function update_notification_status($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update('tbl_notification', $data);
        return true;
    }


    public function get_notification($employee_id)
    {
        return $this->db->query("SELECT n.*,e.`photo_location` FROM `tbl_notification` AS n
LEFT JOIN `tbl_employee` AS e ON e.`id` = n.`from_employee_id`
WHERE n.`to_employee_id` = '$employee_id' AND n.`it_seen` = 0
")->result_array();
    }

    public function get_branch_info()
    {
        return $this->db->query("SELECT b.*, c.`name` AS country_name, t.`name` AS timezone,
        s.`name` AS shift_name FROM `tbl_branch` AS b
INNER JOIN `cc_countries` AS c ON c.`id` = b.`country_id`
INNER JOIN `tbl_timezone` AS t ON t.`id` = b.`time_zone_id`
LEFT JOIN `tbl_shift` AS s ON s.`id` = b.`shift_id`
")->result_array();
    }

    public function get_all_branch_list()
    {
        return $this->db->get('tbl_branch')->result_array();
    }



    public function get_course_prefix_info($limit, $offset, $value = '')
    {
        $this->db->select('*');
        $this->db->from('cc_course_prefix');

        $this->db->order_by("id", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }


    public function get_course_prefix_info_by_unit_id($id)
    {
        return $this->db->where('id', $id)->get('cc_course_prefix')->row();
    }

    public function get_currencies_info($limit, $offset, $value = '')
    {
        $this->db->select('*');
        $this->db->from('cc_currencies');

        $this->db->order_by("id", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_currencies_info_by_unit_id($id)
    {
        return $this->db->where('id', $id)->get('cc_currencies')->row();
    }

    public function get_intake_year_info($limit, $offset, $value = '')
    {
        $this->db->select('*');
        $this->db->from('cc_intake_years');

        $this->db->order_by("id", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_intake_year_info_by_unit_id($id)
    {
        return $this->db->where('id', $id)->get('cc_intake_years')->row();
    }

    public function get_intake_months_info($limit, $offset, $value = '')
    {
        $this->db->select('*');
        $this->db->from('cc_intake_months');

        $this->db->order_by("id", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_intake_months_info_by_unit_id($id)
    {
        return $this->db->where('id', $id)->get('cc_intake_months')->row();
    }

    public function get_institution_types_info($limit, $offset, $value = '')
    {
        $this->db->select('*');
        $this->db->from('cc_institution_type');

        $this->db->order_by("id", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_institution_types_info_by_unit_id($id)
    {
        return $this->db->where('id', $id)->get('cc_institution_type')->row();
    }

    public function get_course_types_info($limit, $offset, $value = '')
    {
        $this->db->select('*');
        $this->db->from('cc_course_type');

        $this->db->order_by("id", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_course_types_info_by_unit_id($id)
    {
        return $this->db->where('id', $id)->get('cc_course_type')->row();
    }

    public function get_course_levels_info($limit, $offset, $value = '')
    {
        $this->db->select('*');
        $this->db->from('cc_course_level');

        $this->db->order_by("id", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_course_levels_info_by_unit_id($id)
    {
        return $this->db->where('id', $id)->get('cc_course_level')->row();
    }

    public function get_Institutions_info($limit, $offset, $value = '')
    {
        $this->db->select('institutions.*,c.name as country_name');
        $this->db->from('institutions');
        $this->db->join('cc_countries AS c', 'institutions.country_id=c.id');
        $this->db->order_by("institutions.id", "desc");
        if (isset($value['country_id']) && $value['country_id'] != '') {
            $this->db->where('institutions.country_id', $value['country_id']);
        }
        if (isset($value['name']) && $value['name'] != '') {
              $where_con="(institutions.name LIKE '%".$value['name']."%' OR institutions.campus LIKE '%".$value['name']."%')";
              $this->db->where($where_con);
        }

        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_institution_info_by_unit_id($id)
    {
        return $this->db->where('id', $id)->get('institutions')->row();
    }

    public function get_courses_info($limit, $offset, $value = '')
    {
        $this->db->select('courses.*,c.name as country_name,i.name as institution_name');
        $this->db->from('courses');
        $this->db->join('cc_countries AS c', 'courses.country_id=c.id');
        $this->db->join('institutions AS i', 'courses.institution_id=i.id');
        $this->db->order_by("courses.id", "desc");

        if (isset($value['country_id']) && $value['country_id'] != '') {
            $this->db->where('courses.country_id', $value['country_id']);
        }
        if (isset($value['institutions_id']) && $value['institutions_id'] != '') {
            $this->db->where('courses.institution_id', $value['institutions_id']);
        }
        if (isset($value['name']) && $value['name'] != '') {
              $where_con="(courses.course_title LIKE '%".$value['name']."%')";
              $this->db->where($where_con);
        }
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_courses_info_by_unit_id($id)
    {
        return $this->db->where('id', $id)->get('courses')->row();
    }

    public function get_cities_info($limit, $offset, $value = '')
    {
        $this->db->select('country_cities.*,c.name as country_name');
        $this->db->from('country_cities');
        $this->db->join('cc_countries AS c', 'country_cities.country_id=c.id');
        if (isset($value['country_id']) && $value['country_id'] != '') {
            $this->db->where('country_cities.country_id', $value['country_id']);
        }
        if (isset($value['name']) && $value['name'] != '') {
              $where_con="(country_cities.name LIKE '%".$value['name']."%')";
              $this->db->where($where_con);
        }
        $this->db->order_by("country_cities.id", "desc");
        $this->db->order_by("id", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_cities_info_by_unit_id($id)
    {
        return $this->db->where('id', $id)->get('country_cities')->row();
    }

    public function get_leads_info($limit, $offset, $value = '')
    {
      $leadIds = array();
      if (isset($value['date']) && $value['date'] != '') {
        $date=$value['date'];
        $lead=$this->db->query("SELECT p.lead_id FROM leads_followup AS p
                           JOIN
                           (
                               SELECT lead_id, MAX(followup_date) AS max_dt
                               FROM leads_followup
                               GROUP BY lead_id
                           ) t
                           ON p.lead_id= t.lead_id AND p.followup_date = t.max_dt
                           WHERE p.followup_date='$date' GROUP BY p.lead_id,p.followup_date")->result_array();
                          $length=count($lead);
                           for ($i=0; $i <$length ; $i++) {
                             array_push($leadIds,$lead[$i]["lead_id"]);
                           }
      }


        $this->db->select('leads.*,c.name as country_name,l.name as lead_source_id,em.name as followup_officer_name,e.name as collection_officer,
      (SELECT te.`name`  FROM `leads_followup` AS lf INNER JOIN `tbl_user` AS tu ON lf.`followup_officer`=tu.`id`
      INNER JOIN `tbl_employee` AS te ON tu.`employee_id`=te.`id` WHERE lead_id=leads.id ORDER BY lf.id DESC LIMIT 1) AS followup_officer,
(SELECT cfm.name  FROM `leads_followup` AS lf INNER JOIN `cc_followup_mode` AS cfm ON lf.`followup_type_id`=cfm.`id`
WHERE lead_id=leads.id ORDER BY lf.id DESC LIMIT 1) AS content,
(SELECT lf.`followup_date`  FROM `leads_followup` AS lf WHERE lead_id=leads.id ORDER BY lf.id DESC LIMIT 1) AS followup_date,
        o.name as collection_venue,cl.name as course_level',FALSE);

        $this->db->from('leads');
        $this->db->join('cc_countries AS c', 'leads.interested_country=c.id', 'left');
        $this->db->join('cc_lead_sources AS l', 'leads.lead_source_id=l.id', 'left');
        $this->db->join('tbl_employee AS e', 'leads.collection_officer=e.id', 'left');
        $this->db->join('tbl_user AS tu', 'leads.current_officer=tu.id', 'left');
        $this->db->join('tbl_employee AS em', 'tu.employee_id=em.id', 'left');
        $this->db->join('cc_collection_venue AS o', 'leads.collection_venue=o.id', 'left');
        $this->db->join('cc_course_level AS cl', 'leads.course_level_id=cl.id', 'left');


        if (isset($value['country_id']) && $value['country_id'] != '') {
            $this->db->where('c.id', $value['country_id']);
        }
        if (isset($value['lead_source_id']) && $value['lead_source_id'] != '') {
            $this->db->where('l.id', $value['lead_source_id']);
        }
        if (isset($value['admission_officer']) && $value['admission_officer'] != '') {
            $this->db->where('leads.current_officer', $value['admission_officer']);
        }
        if (isset($value['current_officer']) && $value['current_officer'] != '') {
            $this->db->where('leads.current_officer', $value['current_officer']);
        }
        if (isset($value['name']) && $value['name'] != '') {
              $where_con="(leads.first_name LIKE '%".$value['name']."%' OR leads.last_name LIKE '%".$value['name']."%' OR leads.phone LIKE '%".$value['name']."%' OR leads.email LIKE '%".$value['name']."%')";
              $this->db->where($where_con);
        }

        if (isset($value['is_favourite']) && $value['is_favourite'] != 'all' && $value['is_favourite'] != '') {
            $this->db->where('leads.is_favourite', $value['is_favourite']);
        }

        if (isset($value['origin_office']) && $value['origin_office'] != '') {
            $this->db->where('leads.origin_office', $value['origin_office']);
        }


        if (isset($value['is_deleted'])) {
            $this->db->where('leads.is_deleted', $value['is_deleted']);
        }
        $this->db->where('leads.is_registered', '0');
        if (isset($value['date']) && $value['date'] != '') {
          if(count($leadIds)==0)
          {
             array_push($leadIds,0);
          }
          $this->db->where_in('leads.id',$leadIds);
        }
        $this->db->order_by("leads.id", "desc");

        $this->db->order_by("id", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }


    public function get_applicants_info($limit, $offset, $value = '')
    {
        $this->db->select('applicants.*,l.name as lead_source_id,c.name as country_id,e.name as admission_officer,ag.company_name as agent_name');
        $this->db->from('applicants');
        $this->db->order_by("id", "desc");
        $this->db->join('cc_countries AS c', 'applicants.country_id=c.id', 'left');
        $this->db->join('tbl_user AS u', 'applicants.admission_officer_id=u.id','left');
        $this->db->join('tbl_employee AS e', 'u.employee_id=e.id', 'left');
        //$this->db->join('tbl_employee_post AS y', 'applicants.position_1=y.id','left');
        $this->db->join('cc_lead_sources AS l', 'applicants.lead_source_id=l.id', 'left');
        $this->db->join('cc_marital_status AS m', 'applicants.marital_status_id=m.id', 'left');
        $this->db->join('cc_genders AS g', 'applicants.gender_id=g.id', 'left');
        $this->db->join('agents AS ag', 'applicants.agent_id=ag.id', 'left');


        if (isset($value['admission_officer_id']) && $value['admission_officer_id'] != '') {
            $this->db->where('applicants.admission_officer_id', $value['admission_officer_id']);
        }
        if (isset($value['admission_officer']) && $value['admission_officer'] != '') {
            $this->db->where('applicants.admission_officer_id', $value['admission_officer']);
        }
        if (isset($value['origin_office']) && $value['origin_office'] != '') {
            $this->db->where('applicants.origin_office', $value['origin_office']);
        }
        if (isset($value['country_id']) && $value['country_id'] != '') {
            $this->db->where('c.id', $value['country_id']);
        }
        if (isset($value['lead_source_id']) && $value['lead_source_id'] != '') {
            $this->db->where('l.id', $value['lead_source_id']);
        }
        if (isset($value['code']) && $value['code'] != '') {
            $this->db->where('applicants.applicant_code', $value['code']);
        }
        if (isset($value['name']) && $value['name'] != '') {
              $where_con="(applicants.first_name LIKE '%".$value['name']."%' OR applicants.last_name LIKE '%".$value['name']."%' OR applicants.mobile LIKE '%".$value['name']."%' OR applicants.email LIKE '%".$value['name']."%')";
              $this->db->where($where_con);
        }
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_leads_info_by_unit_id($id)
    {
        return $this->db->where('id', $id)->get('leads')->row();
    }

    public function get_applicants_info_by_unit_id($id)
    {
        return $this->db->where('id', $id)->get('applicants')->result_array();
    }

    public function get_edit_education_by_unit_id($id)
    {
        return $this->db->where('id', $id)->get('applicant_education')->row();
    }

    public function get_org_info()
    {
        return $this->db->get('tbl_contact_info')->result_array();
    }


    public function get_branch_info_by_unit_id($id)
    {
        return $this->db->where('id', $id)->get('tbl_branch')->row();
    }


    public function get_all_country()
    {
        return $this->db->get('cc_countries')->result_array();
    }

    public function get_all_shift()
    {
        return $this->db->get('tbl_shift')->result_array();
    }

    public function get_all_timezone()
    {
        return $this->db->get('tbl_timezone')->result_array();
    }

    public function get_timekeeping_config_info()
    {
        return $this->db->get('tbl_timekeeping_config')->row();
    }

    public function timekeeping_config_update($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update('tbl_timekeeping_config', $data);
    }


    public function get_history_info()
    {
        return $this->db->get('tbl_history_info')->row();
    }

    public function get_rules_and_regulation_info()
    {
        return $this->db->get('tbl_rules_and_regulation')->row();
    }

    public function get_history_info_by_id($id)
    {
        return $this->db->where('id', $id)->get('tbl_history_info')->row();
    }

    public function get_rules_and_regulation_by_id($id)
    {
        return $this->db->where('id', $id)->get('tbl_rules_and_regulation')->row();
    }

    public function history_info_update($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update('tbl_history_info', $data);
    }

    public function rules_and_regulation_info_update($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update('tbl_rules_and_regulation', $data);
    }

    public function add_contact_info_update($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update('tbl_admission_contact', $data);
    }

    public function add_process_info_update($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update('tbl_admission_process', $data);
    }

    public function get_about_info()
    {
        return $this->db->get('tbl_about_info')->row();
    }


    public function get_contact_info()
    {
        return $this->db->get('tbl_contact_info')->row();
    }

    public function get_home_page_news_info($limit, $offset, $value = '')
    {
        $this->db->select('hpn.*');
        $this->db->from('tbl_home_page_news AS hpn');
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_home_page_breaking_news_info()
    {
        return $this->db->get('tbl_home_page_breaking_news')->row();
    }

    public function get_about_info_by_id($id)
    {
        return $this->db->where('id', $id)->get('tbl_about_info')->row();
    }

    public function get_home_page_news_info_by_id($id)
    {
        return $this->db->where('id', $id)->get('tbl_home_page_news')->row();
    }

    public function get_home_page_breaking_news_info_by_id($id)
    {
        return $this->db->where('id', $id)->get('tbl_home_page_breaking_news')->row();
    }

    public function about_info_update($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update('tbl_about_info', $data);
    }

    public function home_page_news_info_update($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update('tbl_home_page_news', $data);
    }

    public function home_page_breaking_news_info_update($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update('tbl_home_page_breaking_news', $data);
    }

    public function get_all_unit()
    {
        return $this->db->get('tbl_unit')->result_array();
    }

    public function add_unit($data)
    {
        return $this->db->insert('tbl_unit', $data);
    }

    public function delete_unit_by_unit_id($id)
    {
        return $this->db->delete('tbl_unit', array('id' => $id));
    }

    public function get_unit_info_by_unit_id($id)
    {
        return $this->db->where('id', $id)->get('tbl_unit')->row();
    }

    public function update_unit_info($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update('tbl_unit', $data);
    }

    public function get_all_notice($limit, $offset, $value = '')
    {
        $this->db->select('n.*');
        $this->db->from('tbl_notice AS n');
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $this->db->order_by('n.id', 'desc');
        $query = $this->db->get();
        return $query->result_array();
    }


    public function get_all_document($limit, $offset, $value = '')
    {
        $this->db->select('d.*');
        $this->db->from('tbl_document AS d');
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $this->db->order_by('d.id', 'desc');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_contact_message($limit, $offset, $value = '')
    {
        $this->db->select('c.*');
        $this->db->from('tbl_contact AS c');
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $this->db->order_by('c.id', 'desc');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function add_document($data)
    {
        return $this->db->insert('tbl_document', $data);
    }

    public function add_notice($data)
    {
        return $this->db->insert('tbl_notice', $data);
    }

    public function add_admission_form($data)
    {
        return $this->db->insert('tbl_admission_form', $data);
    }



    public function get_document_info_by_id($id)
    {
        return $this->db->where('id', $id)->get('tbl_document')->result_array();
    }


    public function get_notice_info_by_id($id)
    {
        return $this->db->where('id', $id)->get('tbl_notice')->result_array();
    }


    public function delete_document_info_by_id($id)
    {
        return $this->db->delete('tbl_document', array('id' => $id));
    }



    public function delete_notice_info_by_id($id)
    {
        return $this->db->delete('tbl_notice', array('id' => $id));
    }



    public function update_contact_info($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update('tbl_contact_info', $data);
    }

    public function getAllDigitalContent($limit, $offset, $value = '')
    {
        $this->db->select('dc.*');
        $this->db->from('tbl_digital_contents as dc');
        $this->db->order_by("dc.id", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }
    //////Rakibul////////
    public function get_reference_applicants_info($limit, $offset, $value = '')
    {
        $this->db->select('applicants.*,l.name as lead_source_id,c.name as country_id,e.name as admission_officer');
        $this->db->from('applicants');
        $this->db->order_by("id", "desc");
        $this->db->join('cc_countries AS c', 'applicants.country_id=c.id', 'left');
        $this->db->join('tbl_user AS u', 'applicants.admission_officer_id=u.id','left');
        $this->db->join('tbl_employee AS e', 'u.employee_id=e.id', 'left');
        //$this->db->join('tbl_employee_post AS y', 'applicants.position_1=y.id','left');
        $this->db->join('cc_lead_sources AS l', 'applicants.lead_source_id=l.id', 'left');
        $this->db->join('cc_marital_status AS m', 'applicants.marital_status_id=m.id', 'left');
        $this->db->join('cc_genders AS g', 'applicants.gender_id=g.id', 'left');


        if (isset($value['admission_officer_id']) && $value['admission_officer_id'] != '') {
            $this->db->where('applicants.admission_officer_id', $value['admission_officer_id']);
        }

        if (isset($value['origin_office']) && $value['origin_office'] != '') {
            $this->db->where('applicants.origin_office', $value['origin_office']);
        }
        if (isset($value['agent_id']) && $value['agent_id'] != '') {
            $this->db->where('applicants.agent_id', $value['agent_id']);
        }
        if (isset($value['country_id']) && $value['country_id'] != '') {
            $this->db->where('c.id', $value['country_id']);
        }
        if (isset($value['lead_source_id']) && $value['lead_source_id'] != '') {
            $this->db->where('l.id', $value['lead_source_id']);
        }
        if (isset($value['code']) && $value['code'] != '') {
            $this->db->where('applicants.applicant_code', $value['code']);
        }
        if (isset($value['name']) && $value['name'] != '') {
              $where_con="(applicants.first_name LIKE '%".$value['name']."%' OR applicants.last_name LIKE '%".$value['name']."%' OR applicants.mobile LIKE '%".$value['name']."%' OR applicants.email LIKE '%".$value['name']."%')";
              $this->db->where($where_con);
        }
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }
    // public function get_reference_applicants_info($limit, $offset, $value = '')
    // {
    //   $this->db->select('applicants.*,l.name as lead_source_id,c.name as country_id,e.name as admission_officer');
    //   $this->db->from('applicants');
    //   $this->db->order_by("id", "desc");
    //   $this->db->join('cc_countries AS c', 'applicants.country_id=c.id', 'left');
    //   $this->db->join('tbl_user AS u', 'applicants.admission_officer_id=u.id','left');
    //   $this->db->join('tbl_employee AS e', 'u.employee_id=e.id', 'left');
    //   $this->db->join('cc_lead_sources AS l', 'applicants.lead_source_id=l.id', 'left');
    //   $this->db->join('cc_marital_status AS m', 'applicants.marital_status_id=m.id', 'left');
    //   $this->db->join('cc_genders AS g', 'applicants.gender_id=g.id', 'left');
    //
    //     if (isset($value['agent_id']) && $value['agent_id'] != '') {
    //         $this->db->where('applicants.agent_id', $value['agent_id']);
    //     }
    //
    //     if (isset($limit) && $limit > 0) {
    //         $this->db->limit($limit, $offset);
    //     }
    //     $query = $this->db->get();
    //     return $query->result_array();
    // }
    //Rakib
    public function my_file_remove($old_file, $new_file, $location)
    {
        if (!empty($new_file)) {
            if (!empty($old_file) && $old_file !='0') {
                $filedel = PUBPATH.$location.$old_file;

                if (unlink($filedel)) {
                    return "ok"; //ok
                } else {
                    return "ise";//internal server error
                }
            }
            return "nf";
        }
        return "nf";//not found
    }
    //Rakib
    public function checkIf_exist_applicants_password($applicant_id)
    {
        $result= $this->db->query("SELECT PASSWORD FROM applicants
                                WHERE id = '$applicant_id'")->result_array();
      //  print_r($result[0]['PASSWORD']);
        //die();

        if (!empty($result[0]['PASSWORD'])) {
            return 1;
        } else {
            return 0;
        }
    }
    //Rakib
    public function get_all_application_status_details($limit, $offset, $value = '')
    {
        $this->db->select('*');
        $this->db->from('tbl_application_status');
        if (isset($value) && !empty($value) &&  $value['name'] != '') {
            $this->db->like('name', $value['name']);
        }
        $this->db->order_by("name", "asc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }
    //Rakib
    public function get_edit_language_by_applicant_id($id)
    {
        return $this->db->where('id', $id)->get('applicant_english_language')->row();
    }
    //Rakib
    public function get_edit_work_experience_by_applicant_id($id)
    {
        return $this->db->where('id', $id)->get('applicant_work_experience')->row();
    }
    //Rakib
    public function get_edit_reference_by_applicant_id($id)
    {
        return $this->db->where('id', $id)->get('applicant_reference')->row();
    }
    public function check_information_update_by_applicant_id($applicant_id)
    {
        $reference_count = $this->db->query("SELECT id FROM applicant_reference WHERE applicant_id = '$applicant_id'")->num_rows();
      //  $work_experience_count = $this->db->query("SELECT id FROM applicant_work_experience WHERE applicant_id = '$applicant_id'")->num_rows();
      //  $language_count = $this->db->query("SELECT id FROM applicant_english_language WHERE applicant_id = '$applicant_id'")->num_rows();
        $education_count = $this->db->query("SELECT id FROM applicant_education WHERE applicant_id = '$applicant_id'")->num_rows();

        if ((int)$reference_count>0  && (int)$education_count>0) {
            return 1;
        } else {
            return 0;
        }
    }
    //Rakib
   public function Get_course_view($id)
    {

  $query=$this->db->query("SELECT tc.name AS country_name,i.name AS institution_name,d.name AS course_category,cl.name AS course_level,
                          cp.name AS course_prefix,cc.name AS tuition_fees_currency,cc1.name AS tuition_fees_home_currency,c.* FROM `courses` AS c
                          LEFT JOIN `cc_countries` AS tc ON c.country_id =tc.Id
                          LEFT JOIN institutions AS i ON c.institution_id=i.id
                          LEFT JOIN disciplines AS d ON c.course_category_id=d.id
                          LEFT JOIN cc_course_level AS cl ON c.course_level_id=cl.id
                          LEFT JOIN cc_course_prefix AS cp ON c.`course_prefix_id`=cp.id
                          LEFT JOIN cc_currencies AS cc ON c.`tuition_fees_currency`=cc.id
                          LEFT JOIN cc_currencies AS cc1 ON c.`tuition_fees_home_currency`=cc1.id
                    WHERE c.id='$id'")->result_array();
                    return $query;
    }
//Rakibul 10 April 2020
    public function get_user_type($user_id)
    {
      $result= $this->db->query("SELECT u.`is_head_of_admission`,u.`is_head_of_process`,p.`name` AS designation FROM tbl_user AS u
                              LEFT JOIN `tbl_employee` AS e ON u.`employee_id`=e.`id`
                              LEFT JOIN `tbl_employee_post` AS p ON e.`post`=p.`id`
                              WHERE u.id = '$user_id'")->result_array();

          if ($result[0]['is_head_of_process']) {
             ///head_of_process
              return "hop";
          } elseif($result[0]['is_head_of_admission']) {
             //head_of_admission
              return "hoa";
          }  elseif($result[0]['designation']=='Admission Officer') {
               //head_of_admission
                return "ao";
            }
          else{
            //admission_officer
            return "e";
          }

    }
    public function get_user_common_info($user_id)
    {
    $result = $this->db->query("SELECT p.`name` AS designation,e.`name`,e.`code`,e.`branch_id`,em.`name` AS reporting_boss,p1.`name`  AS reporting_boss_designation FROM  `tbl_employee` AS e
                                LEFT JOIN `tbl_employee_post` AS p ON e.`post`=p.`id`
                                INNER JOIN `tbl_user` AS u ON e.id=u.`employee_id`
                                LEFT JOIN `tbl_employee` AS em ON e.`reporting_boss_id`=em.`id`
                                LEFT JOIN `tbl_employee_post` AS p1 ON em.`post`=p1.`id`
                                WHERE u.id='$user_id'")->result_array();
      return $result;
    }
    public function get_user_head_of_admission_designation($user_id)
    {
    $result = $this->db->query("SELECT p.`name` as designation,e.`name` FROM tbl_user AS u
                            INNER JOIN `tbl_employee` AS e ON u.`employee_id`=e.`id`
                            LEFT JOIN `tbl_employee_post` AS p ON e.`post`=p.`id`
                            LEFT JOIN `tbl_branch` AS b ON e.`branch_id`=b.`id`
                            WHERE e.`branch_id`=(SELECT `branch_id` FROM `tbl_employee` AS e
                            INNER JOIN `tbl_user` AS u ON e.`id`=u.`employee_id`  WHERE u.id='$user_id')
                            AND u.`is_head_of_admission`=1")->result_array();
      return $result;
    }
    public function get_user_head_of_process_designation()
    {
      $result = $this->db->query("SELECT p.`name` as designation,e.`name` FROM tbl_user AS u
                              INNER JOIN `tbl_employee` AS e ON u.`employee_id`=e.`id`
                              LEFT JOIN `tbl_employee_post` AS p ON e.`post`=p.`id`
                              WHERE  u.`is_head_of_process`=1")->result_array();
      return $result;
    }
    public function get_lead_followup_report_data($user_id)
    {
      $result = $this->db->query("SELECT c.`name`,COUNT(c.`name`) AS my_desk,
                              (SELECT GetLeadCount(l.`current_officer`,l.`lead_source_id`,'fo')) leads_followup,
                              (SELECT GetLeadCount(l.`current_officer`,l.`lead_source_id`,'d') ) is_deleted,
                              (SELECT GetLeadCount(l.`current_officer`,l.`lead_source_id`,'f')) is_favourite
                              FROM leads AS l
                              INNER JOIN `cc_lead_sources` AS c ON l.`lead_source_id`=c.`id`
                              WHERE  l.`is_registered`=0 AND l.`current_officer`='$user_id'
                              GROUP BY c.`name`")->result_array();

      return $result;
    }
    public function get_employee_list_by_employeeid($employee_id)
    {
    $result = $this->db->query("SELECT u.`id`,e.name FROM `tbl_employee` AS e
                        INNER JOIN tbl_user AS u ON e.id=u.`employee_id`
                        WHERE e.branch_id =(SELECT branch_id FROM tbl_employee WHERE id='$employee_id')
                        ORDER BY e.name")->result_array();
      return $result;
    }
    public function get_intake_years()
    {
    $result = $this->db->query("SELECT id,name,weight FROM `cc_intake_years` WHERE `is_active`=1 ORDER BY NAME DESC")->result_array();
      return $result;
    }
    public function get_intake_months()
    {
    $result = $this->db->query("SELECT id,name,weight FROM `cc_intake_months` WHERE `is_active`=1")->result_array();
      return $result;
    }
    public function get_intake_years_by_id($id)
    {
    $result = $this->db->query("SELECT id,name,weight FROM `cc_intake_years` WHERE `is_active`=1 and id='$id' ORDER BY NAME DESC")->result_array();
      return $result;
    }
    public function get_intake_months_by_id($id)
    {
    $result = $this->db->query("SELECT id,name,weight FROM `cc_intake_months` WHERE `is_active`=1 and id='$id'")->result_array();
      return $result;
    }
    public function get_admission_report_data($branch,$month,$year)
    {
      $result = $this->db->query("SELECT e.`name`,e.`code`,
                            (SELECT GetApplicationStatusCount(u.`id`,'ac',$month,$year)) AS application_cancel,
                            (SELECT GetApplicationStatusCount(u.`id`,'vas',$month,$year)) AS visa_application_submitted,
                            (SELECT GetApplicationStatusCount(u.`id`,'vReceived',$month,$year)) AS visa_received,
                            (SELECT GetApplicationStatusCount(u.`id`,'as',$month,$year)) AS application_submitted
                             FROM `tbl_employee` AS e
                            INNER JOIN `tbl_user` AS u ON e.`id`=u.`employee_id`
                            INNER JOIN `tbl_employee_post` AS p ON e.`post`=p.`id`
                            WHERE p.`name`='Admission Officer' AND e.`branch_id`='$branch'
                            AND u.`is_active`=1 ORDER BY e.`name`")->result_array();

      return $result;
    }
    public function get_student_applied_university_process_report_data($admssion_officer_id,$f_date,$t_date)
    {
      $result = $this->db->query("SELECT CONCAT(a.`first_name`,' ',a.`last_name`) AS s_name,
                                a.`applicant_code`,u.`application_code` AS u_code ,
                                c.name AS country,i.name AS institutions,m.name AS intake_months,
                                y.name AS intake_years,s.`name` AS current_status
                                FROM `applicants` AS a
                                INNER JOIN  university_application AS u ON a.`id`=u.`applicant_id`
                                INNER JOIN `institutions` AS i ON u.`institution_id`=i.id
                                INNER JOIN `cc_countries` AS c ON u.`country_id`=c.id
                                LEFT JOIN `cc_intake_months` AS m ON u.`intake_month`=m.id
                                LEFT JOIN `cc_intake_years` AS Y ON u.`intake_year`=y.id
                                LEFT JOIN tbl_application_status AS s ON u.`current_status`=s.`id`
                                WHERE a.`admission_officer_id`='$admssion_officer_id' AND CAST(u.`proces_date` AS DATE )
                                BETWEEN CAST('$f_date' AS DATE) AND CAST('$t_date' AS DATE)
                                ORDER BY a.first_name,y.name,m.weight")->result_array();

      return $result;
    }
    public function get_student_status_process_report_data($admssion_officer_id,$f_date,$t_date)
    {
      $result = $this->db->query("SELECT
GetApplicationStatusCountDateRange(id,'as',CAST('$f_date' AS DATE) ,CAST('$t_date' AS DATE)) AS application_submitted,
GetApplicationStatusCountDateRange(id,'uolr',CAST('$f_date' AS DATE) ,CAST('$t_date' AS DATE)) AS u_letter_received,
GetApplicationStatusCountDateRange(id,'colr',CAST('$f_date' AS DATE) ,CAST('$t_date' AS DATE)) AS c_letter_received,
GetApplicationStatusCountDateRange(id,'vas',CAST('$f_date' AS DATE) ,CAST('$t_date' AS DATE)) AS visa_application_submitted,
GetApplicationStatusCountDateRange(id,'vReceived',CAST('$f_date' AS DATE) ,CAST('$t_date' AS DATE)) AS visa_received,
GetApplicationStatusCountDateRange(id,'vRejected',CAST('$f_date' AS DATE) ,CAST('$t_date' AS DATE)) AS visa_rejected
 FROM `tbl_user` WHERE id='$admssion_officer_id'")->result_array();

      return $result;
    }

    public function get_intake_admission_report_data($admssion_officer_id,$year,$month)
    {
      $result = $this->db->query("SELECT CONCAT(a.`first_name`,' ',a.`last_name`) AS s_name,
                                a.`applicant_code`,u.`application_code` AS u_code ,
                                c.name AS country,i.name AS institutions,m.name AS intake_months,
                                y.name AS intake_years,s.`name` AS current_status,ls.`name` AS lead_sources
                                FROM `applicants` AS a
                                INNER JOIN  university_application AS u ON a.`id`=u.`applicant_id`
                                INNER JOIN `institutions` AS i ON u.`institution_id`=i.id
                                INNER JOIN `cc_countries` AS c ON u.`country_id`=c.id
                                LEFT JOIN `cc_intake_months` AS m ON u.`intake_month`=m.id
                                LEFT JOIN `cc_intake_years` AS Y ON u.`intake_year`=y.id
                                LEFT JOIN tbl_application_status AS s ON u.`current_status`=s.`id`
                                LEFT JOIN `cc_lead_sources` AS ls ON a.`lead_source_id`=ls.`id`
                                WHERE a.`admission_officer_id`='$admssion_officer_id'
                                AND u.`intake_month`=$month AND u.`intake_year`=$year
                                ORDER BY a.first_name,y.name,m.weight")->result_array();

      return $result;
    }
    public function get_intake_admission_status_report_data($admssion_officer_id,$year,$month)
    {
      $result = $this->db->query("SELECT
        GetApplicationStatusCount(id,'as',$month,$year) AS application_submitted,
        GetApplicationStatusCount(id,'reg',$month,$year) AS registration,
        GetApplicationStatusCount(id,'vis',$month,$year) AS visited,
        GetApplicationStatusCount(id,'ac',$month,$year) AS application_cancel,
        GetApplicationStatusCount(id,'vas',$month,$year) AS visa_application_submitted,
        GetApplicationStatusCount(id,'vReceived',$month,$year) AS visa_received,
        GetApplicationStatusCount(id,'vRejected',$month,$year) AS visa_rejected
         FROM `tbl_user` WHERE id='$admssion_officer_id'")->result_array();

      return $result;
    }
    public function check_ifexist_email_applicant($email)
    {
      $email_count = $this->db->query("SELECT id FROM `applicants` WHERE email='$email'")->num_rows();
      if($email_count>0)
      {
        return 1;
      }else{
        return 0;
      }
    }
    public function check_ifexist_update_email_applicant($email,$id)
    {
      $email_count = $this->db->query("SELECT id FROM `applicants` WHERE email='$email' and id!='$id'")->num_rows();
      if($email_count>0)
      {
        return 1;
      }else{
        return 0;
      }
    }
    public function get_university_applicant_details($applicationid)
    {
      $result=$this->db->query("SELECT i.`name` AS institution_name,CONCAT(a.`first_name`,' ',a.`last_name`,'(',a.`applicant_code`,')') AS applicant_name,
                          co.course_title,m.`name` AS intake_month,u.sop_img_path,u.others_img_path,y.`name` AS intake_year,u.`application_code`,a.admission_officer_id
                          FROM university_application AS u
                          LEFT JOIN `institutions` AS i ON u.`institution_id`=i.`id`
                          LEFT JOIN `applicants` AS a ON u.`applicant_id`=a.`id`
                          LEFT JOIN courses AS co ON u.`subject_id`=co.`id`
                          LEFT JOIN `cc_intake_months` AS m ON u.`intake_month`=m.`id`
                          LEFT JOIN `cc_intake_years` AS Y ON u.`intake_year`=y.`id`
                          WHERE u.id='$applicationid'")->result_array();
        return $result;
    }
   public function get_university_application_status($applicationid,$process_officer_id)
    {
    $user_type=  $this->get_user_type($process_officer_id);
    // if($user_type=="hoa")
    // {
      $result=$this->db->query("SELECT
                            (SELECT `name` FROM tbl_application_status WHERE id=h.old_status_id) AS old_status ,
                            (SELECT `name` FROM tbl_application_status WHERE id=h.new_status_id) AS new_status ,u.application_code,
                            date_time,te.name  as process_officer,h.remarks,file_location,i.`name` AS institution_name,CONCAT(a.`first_name`,' ',a.`last_name`,'(',a.`applicant_code`,')') AS applicant_name,
                            co.course_title,m.`name` AS intake_month,y.`name` AS intake_year
                            FROM tbl_application_status_history AS h
                            LEFT JOIN university_application AS u ON h.`application_id`=u.`id`
                            LEFT JOIN `institutions` AS i ON u.`institution_id`=i.`id`
                            LEFT JOIN `applicants` AS a ON u.`applicant_id`=a.`id`
                            LEFT JOIN tbl_user as tu on u.process_officer_id=tu.id
                            LEFT JOIN tbl_employee as te on tu.employee_id=te.id
                            LEFT JOIN courses AS co ON u.`subject_id`=co.`id`
                            LEFT JOIN `cc_intake_months` AS m ON u.`intake_month`=m.`id`
                            LEFT JOIN `cc_intake_years` AS Y ON u.`intake_year`=y.`id`
                            WHERE h.application_id='$applicationid'
                            ORDER BY h.id desc")->result_array();
                            return $result;
    // }else{
    //   $result=$this->db->query("SELECT
    //                         (SELECT `name` FROM tbl_application_status WHERE id=h.old_status_id) AS old_status ,
    //                         (SELECT `name` FROM tbl_application_status WHERE id=h.new_status_id) AS new_status ,u.application_code,
    //                         date_time,te.name  as process_officer,h.remarks,file_location,i.`name` AS institution_name,CONCAT(a.`first_name`,' ',a.`last_name`,'(',a.`applicant_code`,')') AS applicant_name,
    //                         co.course_title,m.`name` AS intake_month,y.`name` AS intake_year
    //                         FROM tbl_application_status_history AS h
    //                         LEFT JOIN university_application AS u ON h.`application_id`=u.`id`
    //                         LEFT JOIN `institutions` AS i ON u.`institution_id`=i.`id`
    //                         LEFT JOIN `applicants` AS a ON u.`applicant_id`=a.`id`
    //                         LEFT JOIN tbl_user as tu on u.process_officer_id=tu.id
    //                         LEFT JOIN tbl_employee as te on tu.employee_id=te.id
    //                         LEFT JOIN courses AS co ON u.`subject_id`=co.`id`
    //                         LEFT JOIN `cc_intake_months` AS m ON u.`intake_month`=m.`id`
    //                         LEFT JOIN `cc_intake_years` AS Y ON u.`intake_year`=y.`id`
    //                         WHERE h.application_id='$applicationid' and u.process_officer_id='$process_officer_id'
    //                         ORDER BY h.id desc")->result_array();
    //                         return $result;
    // }

    }
    public function update_agent_status($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update('agents', $data);
    }
    public function check_ifexist_email_agent($email)
    {
      $email_count = $this->db->query("SELECT id FROM `agents` WHERE email='$email'")->num_rows();
      if($email_count>0)
      {
        return 1;
      }else{
        return 0;
      }
    }
    public function check_ifexist_update_email_agent($email,$id)
    {
      $email_count = $this->db->query("SELECT id FROM `agents` WHERE email='$email' and id!='$id'")->num_rows();
      if($email_count>0)
      {
        return 1;
      }else{
        return 0;
      }
    }
    public function check_ifexist_phone_leads($phone)
    {
      $phone_count = $this->db->query("SELECT id FROM `leads` WHERE phone='$phone'")->num_rows();
      if($phone_count>0)
      {
        return 1;
      }else{
        return 0;
      }
    }
    public function check_ifexist_update_phone_leads($phone,$id)
    {
      $phone_count = $this->db->query("SELECT id FROM `leads` WHERE phone='$phone' and id!='$id'")->num_rows();
      if($phone_count>0)
      {
        return 1;
      }else{
        return 0;
      }
    }
    public function check_ifexist_phone_applicant($phone)
    {
      $phone_count = $this->db->query("SELECT id FROM `applicants` WHERE mobile='$phone'")->num_rows();
      if($phone_count>0)
      {
        return 1;
      }else{
        return 0;
      }
    }
    public function check_ifexist_update_phone_applicant($phone,$id)
    {
      $phone_count = $this->db->query("SELECT id FROM `applicants` WHERE mobile='$phone' and id!='$id'")->num_rows();
      if($phone_count>0)
      {
        return 1;
      }else{
        return 0;
      }
    }
    public function get_university_by_applicant_id($applicantid)
     {
       $result=$this->db->query("SELECT un.application_code,un.id,un.sop_img_path,un.others_img_path, i.name university_name,(CASE WHEN ccp.name IS NULL THEN c.`course_title` ELSE CONCAT(ccp.name,' ',c.course_title) END) AS course_title,cl.name course_level,m.name cc_month,yy.name cc_year,cc.name cc_currency,
                                        tc.name AS country,un.payment_date,te.name  as process_officer,tas.name as current_status,un.subject_id,un.institution_id FROM university_application AS un
                                       LEFT JOIN institutions AS i ON un.institution_id=i.id
                                       LEFT JOIN courses AS c ON un.subject_id =c.id
                                       LEFT JOIN cc_course_level AS cl ON un.course_level_id=cl.id
                                       LEFT JOIN cc_intake_months AS m ON un.intake_month =m.id
                                       LEFT JOIN cc_intake_years AS yy ON un.intake_year=yy.id
                                       LEFT JOIN cc_currencies AS cc ON un.currency_id=cc.id
                                       LEFT JOIN cc_countries AS tc ON un.country_id =tc.id
                                       LEFT JOIN tbl_user as u on un.process_officer_id=u.id
                                       LEFT JOIN tbl_employee as te on u.employee_id=te.id
                                       LEFT JOIN `tbl_application_status` AS tas ON un.current_status=tas.id
                                       LEFT JOIN `cc_course_prefix` AS ccp ON c.`course_prefix_id`=ccp.`id`
                                       WHERE un.applicant_id='$applicantid'")->result_array();
                             return $result;
     }

     public function get_university_info_by_applicant_id($limit, $offset, $value = '')
      {
        $this->db->select('un.application_code,un.id,un.sop_img_path,un.others_img_path, i.name university_name,c.course_title,cl.name course_level,m.name cc_month,yy.name cc_year,cc.name cc_currency,
                                         tc.name AS country,un.payment_date,te.name  as process_officer,tas.name as current_status ');
        $this->db->from('university_application as un');
        $this->db->join('institutions AS i', 'un.institution_id=i.id', 'left');
        $this->db->join('courses AS c', 'un.subject_id =c.id', 'left');
        $this->db->join('cc_course_level AS cl', 'un.course_level_id=cl.id', 'left');
        $this->db->join('cc_intake_months AS m', 'un.intake_month =m.id', 'left');
        $this->db->join('cc_intake_years AS yy', 'un.intake_year=yy.id', 'left');
        $this->db->join('cc_currencies AS cc', 'un.currency_id=cc.id', 'left');
        $this->db->join('cc_countries AS tc', 'un.country_id =tc.id', 'left');
        $this->db->join('tbl_user as u', 'un.process_officer_id=u.id', 'left');
        $this->db->join('tbl_employee as te', 'u.employee_id=te.id', 'left');
        $this->db->join('tbl_application_status AS tas', 'un.current_status=tas.id', 'left');

        if (isset($value['applicantid']) && $value['applicantid'] != '') {
            $this->db->where('un.applicant_id', $value['applicantid']);
        }
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
      }

     public function get_admission_officer_details_by_applicant($applicant_id)
     {
        $result=$this->db->query("SELECT e.`name`,e.`email`,e.`mobile`,b.`name` AS branch_name,p.`name` AS post,e.photo_location,e.country_code FROM `applicants` AS a
                        INNER JOIN tbl_user AS u ON a.`admission_officer_id`=u.id
                        INNER JOIN `tbl_employee` AS e ON u.`employee_id`=e.`id`
                        LEFT JOIN `tbl_branch` AS b ON e.`branch_id`=b.`id`
                        LEFT JOIN `tbl_employee_post` AS p ON e.post=p.`id`
                        WHERE a.`id`=$applicant_id")->result_array();
                        return $result;
     }
     public function get_agent_details_by_applicant($applicant_id)
     {
        $result=$this->db->query("SELECT ag.`branch_name`,ag.`email`,ag.`company_name`,ag.`personal_name`,ag.`phone`,ag.`address`,ag.logo,ag.country_code
                        FROM `applicants` AS a
                        INNER JOIN `agents` AS ag ON   a.`agent_id`=ag.`id`
                        WHERE a.`id`=$applicant_id")->result_array();
                        return $result;
     }
     public function get_branch_by_agent_id($agent_id)
     {
       $result=$this->db->query("SELECT b.`name`,b.`id` FROM `agents` AS a
                                INNER JOIN `tbl_employee` AS e ON a.admission_officer=e.`id`
                                INNER JOIN `tbl_branch` AS b ON e.`branch_id`=b.id
                                WHERE a.`id`=$agent_id")->result_array();
                    return $result;
     }
     public function get_branch_by_admission_officer_id($officer_id)
     {
       $result=$this->db->query("SELECT b.`name`,b.`id` FROM `agents` AS a
                                INNER JOIN `tbl_employee` AS e ON a.admission_officer=e.`id`
                                INNER JOIN `tbl_branch` AS b ON e.`branch_id`=b.`id`
                                WHERE u.`id`=$officer_id")->result_array();
                    return $result;
     }

     public function get_admission_officer_list($process_officer_id)
     {
       $user_type=  $this->get_user_type($process_officer_id);
       if($user_type=='hop')
       {
         $result=$this->db->query("SELECT u.`id`,e.`name` FROM `tbl_user` AS u
                  INNER JOIN `tbl_employee` AS e ON u.`employee_id`=e.`id`
                  ORDER BY e.`name`")->result_array();
                   return $result;
       }elseif ($user_type=='hoa') {
         $result=$this->db->query("SELECT u.`id`,e.`name` FROM `tbl_user` AS u
          INNER JOIN `tbl_employee` AS e ON u.`employee_id`=e.`id`
          WHERE e.`branch_id`=(SELECT e.`branch_id` FROM `tbl_user` AS u
                               INNER JOIN `tbl_employee` AS e ON u.`employee_id`=e.`id` WHERE u.`id`=$process_officer_id)
                               ORDER BY e.`name`")->result_array();
                   return $result;
       }else{
         $result=$this->db->query("SELECT u.`id`,e.`name` FROM `tbl_user` AS u
                  INNER JOIN `tbl_employee` AS e ON u.`employee_id`=e.`id`
                  WHERE u.`id`=$process_officer_id  ORDER BY e.`name`")->result_array();
                   return $result;
       }

     }
     public function get_admission_officer_list_for_my_desk($process_officer_id)
     {
         $result=$this->db->query("SELECT u.`id`,e.`name` FROM `tbl_user` AS u
          INNER JOIN `tbl_employee` AS e ON u.`employee_id`=e.`id`
          WHERE e.`branch_id`=(SELECT e.`branch_id` FROM `tbl_user` AS u
                               INNER JOIN `tbl_employee` AS e ON u.`employee_id`=e.`id` WHERE u.`id`=$process_officer_id)
                               ORDER BY e.`name`")->result_array();
                   return $result;

     }
     function get_all_lead_sources_info($limit, $offset, $value = '')
     {
         $this->db->select('*');
         $this->db->from('cc_lead_sources');
         if (isset($value) && !empty($value) &&  $value['name'] != '') {
             $this->db->like('name', $value['name']);
         }
         $this->db->order_by("name", "asc");
         if (isset($limit) && $limit > 0)
             $this->db->limit($limit, $offset);
         $query = $this->db->get();
         return $query->result_array();
     }

     public function update_lead_sources_info($data)
     {
         $this->db->where('id', $data['id']);
       return  $this->db->update('cc_lead_sources', $data);
     }

     public function add_lead_sources_info($data)
     {
         return $this->db->insert('cc_lead_sources', $data);
     }
     public function get_lead_sources_by_id($id)
     {
         return $this->db->where('id', $id)->get('cc_lead_sources')->row();
       //  return $this->db->where('id', $id)->get('tbl_school')->result_array();
     }
     public function checkifexist_lead_sources_by_name($name)
     {
       $count = $this->db->query("SELECT id FROM cc_lead_sources where name = '$name'")->num_rows();

       if((int)$count==0)
       {
         return 0;
       }else{
         return 1;
       }

     }
     public function checkifexist_update_lead_sources_by_name($name,$id)
     {
       $count = $this->db->query("SELECT id FROM cc_lead_sources where name = '$name' and id !='$id'")->num_rows();
       if((int)$count==0)
       {
         return 0;
       }else{
         return 1;
       }
     }
     public function get_country_code()
     {
       $result=$this->db->query("SELECT * FROM `country_code`")->result_array();
        return $result;
     }
     public function get_admission_officer_info($admission_officer_id)
     {
        $result=$this->db->query("SELECT e.`name`,e.`email`,e.`mobile`,e.`code`,b.`name` AS branch_name,p.`name` AS post_name FROM `tbl_user` AS u
                                  INNER JOIN `tbl_employee` AS e ON u.`employee_id`=e.`id`
                                  LEFT JOIN `tbl_employee_post` AS p ON e.`post`=p.`id`
                                  LEFT JOIN `tbl_branch` AS b  ON e.`branch_id`=b.`id`
                                  WHERE  u.`id`=$admission_officer_id")->result_array();
            return $result;
     }
     public function get_subject_list()
     {
       $result=$this->db->query("SELECT c.`id`,(CASE WHEN p.name IS NULL THEN c.`course_title` ELSE CONCAT(p.name,' ',c.course_title) END) AS name FROM `courses` AS c
                            INNER JOIN `cc_course_prefix` AS p ON c.`course_prefix_id`=p.`id`")->result_array();
                            return $result;
     }
     public function get_educaton_list_by_applicant_id($id)
     {
       $result=$this->db->query("SELECT a.*,c.name as country_name ,(CASE WHEN p.name IS NULL THEN cc.`course_title` ELSE CONCAT(p.name,' ',cc.course_title) END) AS SUBJECT
         FROM applicant_education as a
         left join cc_countries as c on c.id = a.country_id
         left join `courses` AS cc on a.course_id=cc.id
         LEFT JOIN `cc_course_prefix` AS p ON cc.`course_prefix_id`=p.`id`
         WHERE applicant_id = '$id'")->result_array();
         return $result;
     }
     public function get_tailor_items_info($limit, $offset, $value = '')
     {
         $this->db->select('*');
         $this->db->from('tbl_tailor_item');
         $this->db->order_by("id", "desc");


         if (isset($value['name']) && $value['name'] != '') {
               $where_con="name LIKE '%".$value['name']."%'";
               $this->db->where($where_con);
         }
         if (isset($limit) && $limit > 0) {
             $this->db->limit($limit, $offset);
         }
         $query = $this->db->get();
         return $query->result_array();
     }
     public function get_tailor_item_by_id($id)
     {
         return $this->db->where('id', $id)->get('tbl_tailor_item')->row();
     }
     public function check_ifexist_tailor_item($item_name)
     {
       $email_count = $this->db->query("SELECT id FROM `tbl_tailor_item` WHERE name='$item_name'")->num_rows();
       if($email_count>0)
       {
         return 1;
       }else{
         return 0;
       }
     }
     public function check_ifexist_update_tailor_item($item_name,$id)
     {
       $email_count = $this->db->query("SELECT id FROM `tbl_tailor_item` WHERE name='$item_name' and id!='$id'")->num_rows();
       if($email_count>0)
       {
         return 1;
       }else{
         return 0;
       }
     }
     public function check_ifexist_area($name,$division_id)
     {
       $email_count = $this->db->query("SELECT id FROM `tbl_area` WHERE name='$item_name' and division_id='$division_id'")->num_rows();
       if($email_count>0)
       {
         return 1;
       }else{
         return 0;
       }
     }
     public function check_ifexist_update_area($item_name,$division_id,$id)
     {
       $email_count = $this->db->query("SELECT id FROM `tbl_area` WHERE name='$item_name' and division_id='$division_id' and id!='$id'")->num_rows();
       if($email_count>0)
       {
         return 1;
       }else{
         return 0;
       }
     }
     public function check_ifexist_division($item_name)
     {
       $email_count = $this->db->query("SELECT id FROM `tbl_division` WHERE name='$item_name'")->num_rows();
       if($email_count>0)
       {
         return 1;
       }else{
         return 0;
       }
     }
     public function check_ifexist_update_division($item_name,$id)
     {
       $email_count = $this->db->query("SELECT id FROM `tbl_division` WHERE name='$item_name' and id!='$id'")->num_rows();
       if($email_count>0)
       {
         return 1;
       }else{
         return 0;
       }
     }

}
