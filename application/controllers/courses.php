<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Courses extends CI_Controller
{
    public $notification = array();

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $employee_id = $user_info[0]->employee_id;
        //echo $employee_id; die;
        $this->notification = $this->Admin_login->get_notification($employee_id);
        $this->db->query('SET SESSION sql_mode = ""');
    }

    public function index()
    {
      $cond = array();
      if($_POST){
        $sdata['name'] = $this->input->post('name', true);
        $cond['name'] = $this->input->post('name', true);
        $sdata['country_id'] = $this->input->post('country_id', true);
        $cond['country_id'] = $this->input->post('country_id', true);
        $sdata['institutions_id'] = $this->input->post('institutions_id', true);
        $cond['institutions_id'] = $this->input->post('institutions_id', true);
        $this->session->set_userdata($sdata);

      }else{
        $cond['name'] = $this->session->userdata('name');
        $cond['country_id'] = $this->session->userdata('country_id');
        $cond['institutions_id'] = $this->session->userdata('institutions_id');

      }
        $data = array();
        $data['title'] = 'Courses';
        $data['heading_msg'] = "Courses Information";
        $this->load->library('pagination');
        $config['base_url'] = site_url('courses/index/');
        $config['per_page'] = 10;
        $config['total_rows'] = count($this->Admin_login->get_courses_info(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['courses'] = $this->Admin_login->get_courses_info(10, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['is_show_button'] = "add";
        $data['countries'] = $this->db->query("SELECT * FROM cc_countries")->result_array();
        $data['institutions'] = $this->db->query("SELECT * FROM institutions")->result_array();
        $data['branches'] = $this->db->query("SELECT * FROM tbl_branch")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('courses/index', $data, true);
        $this->load->view('admin_logins/index', $data);
    }

    function add()
    {
        if($_POST){

            $data = array();
            $data['course_prefix_id'] = $this->input->post('course_prefix_id', true);
            $data['course_title'] = $this->input->post('course_title', true);
            $data['course_level_id'] = $this->input->post('course_level_id', true);
            $data['country_id'] = $this->input->post('country_id', true);
            $data['institution_id'] = $this->input->post('institution_id', true);
            $data['campus_location'] = $this->input->post('campus_location', true);
            $data['languages'] = $this->input->post('languages', true);
            $data['course_fee'] = $this->input->post('course_fee', true);
            $data['tuition_fees_currency'] = $this->input->post('tuition_fees_currency', true);
            $data['tuition_fees_home_currency'] = $this->input->post('tuition_fees_home_currency', true);
            $data['course_fee_home'] = $this->input->post('course_fee_home', true);
            $data['is_placement_year'] = $this->input->post('is_placement_year', true);
            $data['course_duration'] = $this->input->post('course_duration', true);
            $data['course_category_id'] = $this->input->post('course_category_id', true);
            $data['module_subject'] = $this->input->post('module_subject', true);
            $data['entry_requirement'] = $this->input->post('entry_requirement', true);
            $data['program_description'] = $this->input->post('program_description', true);
            $data['university_link'] = $this->input->post('university_link', true);

            $this->db->insert('courses', $data);
			$course_id = $this->db->insert_id();


			//intake course insert
			$intakes_list['intake_details'] = $this->input->post('intakes_list', true);
      foreach ($intakes_list['intake_details'] as $key) {
        $intakes_list_array = array();
        if(!empty($key['active'])){
          $intakes_list_array['course_id']=$course_id;
          $intakes_list_array['intake_month_id']=(int)$key['intake_month_id'];
          $intakes_list_array['starting_date']=$key['starting_date'];
          $this->db->insert('course_intake_months', $intakes_list_array);
        }

      }



			// $k = 0;
			// $intakes_list_array = array();
			// while($k < count($intakes_list)){
      //   if(!empty($key['active'])){
      //     {
      //       $intakes_list_array[$k]['course_id'] = $course_id;
      //       $intakes_list_array[$k]['intake_month_id'] = $intakes_list[$k];
      //       $intakes_list_array[$k]['starting_date'] = $this->input->post('starting_date', true);
      //     }
      //
			// 	$k++;
			// }
			// $this->db->insert_batch('course_intake_months', $intakes_list_array);


			//related course
			$related_course_list = $this->input->post('related_course_list', true);
			$k = 0;
			$related_course_list_array = array();
			while($k < count($related_course_list)){
				$related_course_list_array[$k]['course_id'] = $course_id;
				$related_course_list_array[$k]['related_course_id'] = $related_course_list[$k];
				$k++;
			}
			$this->db->insert_batch('related_courses', $related_course_list_array);


            $sdata['message'] = "You are Successfully Added Courses  Info !";
            $this->session->set_userdata($sdata);
            redirect("courses/index");
        }else{
            $data = array();
            $data['title'] = 'Add Courses Information';
            $data['heading_msg'] = "Add Courses Information";
            $data['is_show_button'] = "index";
            $data['intake_months'] = $this->db->query("SELECT * FROM cc_intake_months")->result_array();
            $data['disciplines'] = $this->db->query("SELECT * FROM disciplines")->result_array();
            $data['countries'] = $this->db->query("SELECT * FROM cc_countries")->result_array();
            $data['currencies'] = $this->db->query("SELECT * FROM cc_currencies")->result_array();
            $data['course_prefix'] = $this->db->query("SELECT * FROM cc_course_prefix")->result_array();
            $data['course_level'] = $this->db->query("SELECT * FROM cc_course_level")->result_array();
			$cond = array();
			$data['related_courses'] = $this->Admin_login->get_courses_info(0, 0, $cond);
            $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
            $data['maincontent'] = $this->load->view('courses/add', $data, true);
            $this->load->view('admin_logins/index', $data);
        }
    }

    function getInstituteByCountryId()
    {
        $country_id = $this->input->get('country_id', true);
        $data = array();
        $data['institute_info'] = $this->db->query("SELECT * FROM institutions WHERE `country_id`  = '$country_id'")->result_array();
        $this->load->view('courses/institute_list', $data);
    }


    function edit($id=null)
    {
        if($_POST){
            $data = array();
			$data['id'] = $this->input->post('id', true);
            $data['course_prefix_id'] = $this->input->post('course_prefix_id', true);
            $data['course_title'] = $this->input->post('course_title', true);
            $data['course_level_id'] = $this->input->post('course_level_id', true);
            $data['country_id'] = $this->input->post('country_id', true);
            $data['institution_id'] = $this->input->post('institution_id', true);
            $data['campus_location'] = $this->input->post('campus_location', true);
            $data['languages'] = $this->input->post('languages', true);
            $data['course_fee'] = $this->input->post('course_fee', true);
            $data['course_fee_home'] = $this->input->post('course_fee_home', true);
            $data['is_placement_year'] = $this->input->post('is_placement_year', true);
            $data['course_duration'] = $this->input->post('course_duration', true);
            $data['course_category_id'] = $this->input->post('course_category_id', true);
            $data['module_subject'] = $this->input->post('module_subject', true);
            $data['entry_requirement'] = $this->input->post('entry_requirement', true);
            $data['program_description'] = $this->input->post('program_description', true);
            $data['university_link'] = $this->input->post('university_link', true);
            $data['tuition_fees_home_currency']=$this->input->post('tuition_fees_home_currency',true);
            $data['tuition_fees_currency']=$this->input->post('tuition_fees_currency',true);

            // print_r($this->input->post('university_link', true));
            // die();
            $this->db->where('id', $data['id']);
            $this->db->update('courses', $data);


			//intake course insert
			$this->db->delete('course_intake_months', array('course_id' => $data['id']));
      $intakes_list['intake_details'] = $this->input->post('intakes_list', true);
      foreach ($intakes_list['intake_details'] as $key) {
        $intakes_list_array = array();
        if(!empty($key['active'])){
          $intakes_list_array['course_id']=$data['id'];
          $intakes_list_array['intake_month_id']=(int)$key['intake_month_id'];
          $intakes_list_array['starting_date']=$key['starting_date'];
          $this->db->insert('course_intake_months', $intakes_list_array);
        }
      }
			// $intakes_list = $this->input->post('intakes_list', true);
			// $k = 0;
			// $intakes_list_array = array();
			// while($k < count($intakes_list)){
			// 	$intakes_list_array[$k]['course_id'] = $data['id'];
			// 	$intakes_list_array[$k]['intake_month_id'] = $intakes_list[$k];
			// 	$intakes_list_array[$k]['starting_date'] = $this->input->post('starting_date', true);
			// 	$k++;
			// }
			// $this->db->insert_batch('course_intake_months', $intakes_list_array);


			//related course
            $this->db->delete('related_courses', array('course_id' => $data['id']));
			$related_course_list = $this->input->post('related_course_list', true);
			$k = 0;
			$related_course_list_array = array();
			while($k < count($related_course_list)){
				$related_course_list_array[$k]['course_id'] = $data['id'];
				$related_course_list_array[$k]['related_course_id'] = $related_course_list[$k];
				$k++;
			}
			$this->db->insert_batch('related_courses', $related_course_list_array);


            $sdata['message'] = "You are Successfully Updated Courses Info !";
            $this->session->set_userdata($sdata);
            redirect("courses/index");
        }else{
            $data = array();
            $data['title'] = 'Update Courses Information';
            $data['heading_msg'] = "Update Courses Information";
            $data['is_show_button'] = "index";
            $data['intake_months'] = $this->db->query("  SELECT m.`id`,m.`name`,  1 active,d.`starting_date` FROM cc_intake_months AS m
                              INNER JOIN course_intake_months AS d ON m.id =d.`intake_month_id`
                              WHERE d.`course_id`='$id'
                              UNION ALL
                              SELECT m.`id`,m.`name`,  0 active,'' dstarting_date FROM cc_intake_months AS m
                              WHERE m.`id` NOT IN(
                              SELECT m.`id`FROM cc_intake_months AS m
                              INNER JOIN course_intake_months AS d ON m.id =d.`intake_month_id`
                              WHERE d.`course_id`='$id')")->result_array();
            $data['disciplines'] = $this->db->query("SELECT * FROM disciplines")->result_array();
            $data['countries'] = $this->db->query("SELECT * FROM cc_countries")->result_array();
            $data['course_prefix'] = $this->db->query("SELECT * FROM cc_course_prefix")->result_array();
            $data['course_level'] = $this->db->query("SELECT * FROM cc_course_level")->result_array();
            $data['courses'] = $this->Admin_login->get_courses_info_by_unit_id($id);
			$country_id = $data['courses']->country_id;
			$data['course_intake_month_list'] = $this->db->query("SELECT * FROM course_intake_months WHERE course_id = '$id'")->result_array();

			$cond = array();
			$data['related_courses'] = $this->Admin_login->get_courses_info(0, 0, $cond);
			$data['related_courses_id_list'] = $this->db->query("SELECT * FROM related_courses WHERE `course_id`  = '$id'")->result_array();
            $data['currencies'] = $this->db->query("SELECT * FROM cc_currencies")->result_array();
			$data['institutions'] = $this->db->query("SELECT * FROM institutions WHERE `country_id`  = '$country_id'")->result_array();
            $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
            $data['maincontent'] = $this->load->view('courses/edit', $data, true);
            $this->load->view('admin_logins/index', $data);
        }
    }

    function delete($id)
    {
        $this->db->delete('related_courses', array('course_id' => $id));
        $this->db->delete('courses', array('id' => $id));
        $sdata['message'] = "Courses Information Deleted Successfully !";
        $this->session->set_userdata($sdata);
        redirect("courses/index");
    }


      function view($id=null)
      {

              $data = array();
              $data['title'] = 'View Courses Information';
              $data['heading_msg'] = "View Courses Information";
              $data['is_show_button'] = "index";
              $data['intake_months'] = $this->db->query("  SELECT m.`id`,m.`name`,  1 active,d.`starting_date` FROM cc_intake_months AS m
                                INNER JOIN course_intake_months AS d ON m.id =d.`intake_month_id`
                                WHERE d.`course_id`='$id'
                                UNION ALL
                                SELECT m.`id`,m.`name`,  0 active,'' dstarting_date FROM cc_intake_months AS m
                                WHERE m.`id` NOT IN(
                                SELECT m.`id`FROM cc_intake_months AS m
                                INNER JOIN course_intake_months AS d ON m.id =d.`intake_month_id`
                                WHERE d.`course_id`='$id')")->result_array();

              $data['courses'] = $this->Admin_login->Get_course_view($id);


        			$data['course_intake_month_list'] = $this->db->query("SELECT * FROM course_intake_months WHERE course_id = '$id'")->result_array();

        			$cond = array();
        			$data['related_courses'] = $this->Admin_login->get_courses_info(0, 0, $cond);
        			$data['related_courses_id_list'] = $this->db->query("SELECT * FROM related_courses WHERE `course_id`  = '$id'")->result_array();

              $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
              $data['maincontent'] = $this->load->view('courses/view', $data, true);
              $this->load->view('admin_logins/index', $data);

      }

}
