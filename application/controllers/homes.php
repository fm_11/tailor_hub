<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Homes extends CI_Controller
{
public $notification = array();
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login','Tailor'));
        $this->load->library('session');
        $this->db->query('SET SESSION sql_mode = ""');
    }

    public function index()
    {
      $data = array();
      $data['divisions'] = $this->Tailor->get_all_divisions_list();
      $data['news'] = $this->Tailor->get_all_news_list();
      $data['milestones'] = $this->Tailor->get_tailor_milestone();

      $data['customer_review']=$this->Tailor->get_all_tailor_customer_review();
      $data['contact_info']=$this->Tailor->get_contact_info();
      $data['design_lifestyle']=$this->Tailor->get_tailor_item_list_by_random();
      //  echo '<pre>';
      // print_r($data['design_lifestyle']);
      // die;
      $data['main_menu'] = $this->load->view('homes/menu', '', true);
      $data['index_header'] = $this->load->view('homes/index_header', '', true);
      $data['mobile_menu'] = $this->load->view('homes/mobile_menu', '', true);
      $data['index_sidebar'] = $this->load->view('homes/index_sidebar', '', true);
      $data['index_slider'] = $this->load->view('homes/index_slider', $data, true);
      $data['index_footer'] = $this->load->view('homes/index_footer', $data, true);

      $this->load->view('homes/index', $data);
    }
    public function tailor_area($id)
    {
      $data = array();
        if ($_POST) {
            $area_name = $_POST['search'];
            $data['area_list'] = $this->Tailor->get_all_area_list_by_area_name($area_name);
        }else{
          $data['area_list'] = $this->Tailor->get_all_area_list_by_division_id($id);
        }
      $data['divisions'] = $this->Tailor->get_all_divisions_list();

      $data['contact_info']=$this->Tailor->get_contact_info();
      $data['main_menu'] = $this->load->view('homes/menu', '', true);
      $data['index_header'] = $this->load->view('homes/index_header', $data, true);
      $data['mobile_menu'] = $this->load->view('homes/mobile_menu', '', true);
      $data['index_sidebar'] = $this->load->view('homes/index_sidebar', '', true);
      $data['index_footer'] = $this->load->view('homes/index_footer', $data, true);

      $this->load->view('homes/tailor_area', $data);
    }
    public function contact()
    {
      $data = array();

      $data['contacts'] = $this->Tailor->get_website_contact_info();
      $data['contact_info']=$this->Tailor->get_contact_info();
      $data['main_menu'] = $this->load->view('homes/menu', '', true);
      $data['index_header'] = $this->load->view('homes/index_header', '', true);
      $data['mobile_menu'] = $this->load->view('homes/mobile_menu', '', true);
      $data['index_sidebar'] = $this->load->view('homes/index_sidebar', '', true);
      $data['index_slider'] = $this->load->view('homes/index_slider', $data, true);
      $data['index_footer'] = $this->load->view('homes/index_footer', $data, true);

      $this->load->view('homes/contact', $data);
    }
    public function about()
    {
      $data = array();
      $data['contacts'] = $this->Tailor->get_website_contact_info();
      $data['contact_info']=$this->Tailor->get_contact_info();
      $data['main_menu'] = $this->load->view('homes/menu', '', true);
      $data['index_header'] = $this->load->view('homes/index_header', '', true);
      $data['mobile_menu'] = $this->load->view('homes/mobile_menu', '', true);
      $data['index_sidebar'] = $this->load->view('homes/index_sidebar', '', true);
      $data['index_slider'] = $this->load->view('homes/index_slider', $data, true);
      $data['index_footer'] = $this->load->view('homes/index_footer', $data, true);
      $this->load->view('homes/content', $data);
    }
    public function tailor_list($id)
    {
      $data = array();
      $data['contact_info']=$this->Tailor->get_contact_info();
      $data['divisions'] = $this->Tailor->get_all_divisions_list();
      $data['tailor_list'] = $this->Tailor->get_all_tailor_list_by_area_id($id);
      $data['main_menu'] = $this->load->view('homes/menu', '', true);
      $data['index_header'] = $this->load->view('homes/index_header', $data, true);
      $data['mobile_menu'] = $this->load->view('homes/mobile_menu', '', true);
      $data['index_sidebar'] = $this->load->view('homes/index_sidebar', '', true);
      $data['index_footer'] = $this->load->view('homes/index_footer', $data, true);
      $this->load->view('homes/tailor_list', $data);
    }
    public function tailor_order($id)
    {
      $data = array();
      $data['tailor_order'] = $this->Tailor->get_all_tailor_order($id);
      $data['contact_info']=$this->Tailor->get_contact_info();
      $data['main_menu'] = $this->load->view('homes/menu', '', true);
      $data['index_header'] = $this->load->view('homes/index_header', $data, true);
      $data['mobile_menu'] = $this->load->view('homes/mobile_menu', '', true);
      $data['index_sidebar'] = $this->load->view('homes/index_sidebar', '', true);
      $data['index_footer'] = $this->load->view('homes/index_footer', $data, true);
      $this->load->view('homes/tailor_order', $data);
    }
    public function tailor_order_details($id)
    {
      $data = array();
      $data['tailor_order'] = $this->Tailor->get_all_tailor_order($id);
      $data['tailor_order_details'] = $this->Tailor->get_all_tailor_order_details($id);
      $data['contact_info']=$this->Tailor->get_contact_info();
      $data['main_menu'] = $this->load->view('homes/menu', '', true);
      $data['index_header'] = $this->load->view('homes/index_header', $data, true);
      $data['mobile_menu'] = $this->load->view('homes/mobile_menu', '', true);
      $data['index_sidebar'] = $this->load->view('homes/index_sidebar', '', true);
      $data['index_footer'] = $this->load->view('homes/index_footer', $data, true);
      $this->load->view('homes/tailor_order_details', $data);
    }

    public function blog_details($id)
    {
      $data = array();
       $data['blog_details'] = $this->Tailor->get_blog_details_info($id);
      // $data['tailor_order_details'] = $this->Tailor->get_all_tailor_order_details($id);
      // $data['main_menu'] = $this->load->view('homes/index_menu', '', true);
      // $data['index_header'] = $this->load->view('homes/index_header', $data, true);
      // $data['mobile_menu'] = $this->load->view('homes/mobile_menu', '', true);
      // $data['index_sidebar'] = $this->load->view('homes/index_sidebar', '', true);
      // $data['index_footer'] = $this->load->view('homes/index_footer', $data, true);
        $data['contact_info']=$this->Tailor->get_contact_info();
      $this->load->view('homes/blog-detail', $data);
    }
    public function blog()
    {
      $data = array();
      // $data['tailor_order'] = $this->Tailor->get_all_tailor_order($id);
      // $data['tailor_order_details'] = $this->Tailor->get_all_tailor_order_details($id);
      // $data['main_menu'] = $this->load->view('homes/index_menu', '', true);
      // $data['index_header'] = $this->load->view('homes/index_header', $data, true);
      // $data['mobile_menu'] = $this->load->view('homes/mobile_menu', '', true);
      // $data['index_sidebar'] = $this->load->view('homes/index_sidebar', '', true);
      // $data['index_footer'] = $this->load->view('homes/index_footer', $data, true);
        $data['contact_info']=$this->Tailor->get_contact_info();
      $this->load->view('homes/blog', $data);
    }
    public function tailor_profile($id)
    {
      $data = array();
      $data['divisions'] = $this->Tailor->get_all_divisions_list();
      $data['tailor_info'] = $this->Tailor->get_tailor_info_by_tailor_id($id);
      $data['shop_amenities'] = $this->Tailor->get_tailor_shop_amenities_details_by_tailor_id($id);
      $data['tailor_list_without_this_tailor'] = $this->Tailor->get_all_tailor_list_by_tailor_id($id);
      $data['item_details'] = $this->Tailor->get_tailor_item_details_by_tailor_id($id);
      // print_r($data['tailor_info']); echo die;

      // print_r($data['tailor_list_without_this_tailor']);
      // die;
      $data['contact_info']=$this->Tailor->get_contact_info();
      $data['main_menu'] = $this->load->view('homes/menu', '', true);
      $data['index_header'] = $this->load->view('homes/index_header', $data, true);
      $data['mobile_menu'] = $this->load->view('homes/mobile_menu', '', true);
      $data['index_sidebar'] = $this->load->view('homes/index_sidebar', '', true);
      $data['index_footer'] = $this->load->view('homes/index_footer', $data, true);
      $this->load->view('homes/tailor_profile', $data);
    }
    public function track_order_record()
    {
        $data = array();
        if($_POST){
            $transaction_id = $this->input->post('order_track', true);

            $data['order_track_list']=$this->Tailor->get_tailor_order_list_by_transaction_id($transaction_id);
        }else{
          $data['order_track_list']=$this->Tailor->get_tailor_order_list_by_transaction_id('nodata');
        }
      $data['contact_info']=$this->Tailor->get_contact_info();
      $data['divisions'] = $this->Tailor->get_all_divisions_list();

      $data['main_menu'] = $this->load->view('homes/menu', '', true);
      $data['index_header'] = $this->load->view('homes/index_header', $data, true);
      $data['mobile_menu'] = $this->load->view('homes/mobile_menu', '', true);
      $data['index_sidebar'] = $this->load->view('homes/index_sidebar', '', true);
      $data['index_footer'] = $this->load->view('homes/index_footer', $data, true);
      $this->load->view('homes/track_order_record', $data);
    }

		public function my_file_upload($filename, $type)
		{
		   $ext = explode('.',$_FILES[$filename]['name']);
		   $new_file_name = $type . '.' .end($ext);

			// print_r(rand());
			// die();
			$this->load->library('upload');

			$config = array(
					'upload_path' => "media/website/order_image/",
					'allowed_types' => "jpg|png|jpeg|JPEG|JPG|PNG|pdf|csv|doc|docx",
					'max_size' => "99999999999",
					'max_height' => "3000",
					'max_width' => "3000",
					'file_name' => $new_file_name
				);

			$this->upload->initialize($config);
			//  $xx =$this->upload->do_upload($filename);


			if (!$this->upload->do_upload($filename)) {
				return '0';
			} else {
				return $new_file_name;
			}
		}


    public function order_submit()
    {
        if ($_POST) {
          // echo '<pre>';
           //print_r($_FILES);
          // die;

          $this->load->library('upload');
          $config['upload_path'] = 'media/website/images/area/';
          $config['allowed_types'] = 'jpg|png|JPEG|jpeg';
          $config['max_size'] = '6000';
          $config['max_width'] = '4000';
          $config['max_height'] = '4000';
          $this->upload->initialize($config);

          $data = array();
          $data['customer_name'] = $this->input->post('txt_customer_name', true);
          $data['customer_phone'] = $this->input->post('txt_customer_phone', true);
          $data['customer_email'] = $this->input->post('txt_customer_email', true);
          $data['shipping_address'] = $this->input->post('txt_remarks', true);
          $data['remarks'] = $this->input->post('txt_customer_address', true);
          $data['booking_date'] =date('Y-m-d');

          $data['booking_time'] = date("H:i:s");
          $data['total_cost'] = $this->input->post('txt_total_amount', true);
          $data['total_quantity'] = $this->input->post('division_id', true);
          $data['tailor_id'] = $this->input->post('txt_tailor_id', true);
          $Transaction_Td= $this->generateTransaction_Td();
          $data['transaction_id'] =$Transaction_Td;
          $this->db->insert('tbl_tailor_order', $data);

          $tailor_order_id = $this->db->insert_id();

          $item_details_list['item_details'] = $this->input->post('item_details', true);
	    	  $loop = 0;
          $index=0;
          $final_array =array();
          foreach ($item_details_list['item_details'] as $key) {
            $list_array = array();
            if(!empty($key['colorCheckbox'])){
              $list_array['tailor_order_id']=$tailor_order_id;
              $list_array['quantity']=(double)$key['quantity'];
              $list_array['item_cost']=(double)$key['price'];
              $list_array['total_cost']=(double)$key['total_price'];
              if(isset($key['measurement_level_1']))
              {
                $list_array['measurement_value_1']=$key['measurement_level_1'];
              }else {
                $list_array['measurement_value_1']='';
              }
              if(isset($key['measurement_level_2']))
              {
                $list_array['measurement_value_2']=$key['measurement_level_2'];
              }else {
                $list_array['measurement_value_2']='';
              }
              if(isset($key['measurement_level_3']))
              {
                $list_array['measurement_value_3']=$key['measurement_level_3'];
              }else {
                $list_array['measurement_value_3']='';
              }
              if(isset($key['measurement_level_4']))
              {
                $list_array['measurement_value_4']=$key['measurement_level_4'];
              }else {
                $list_array['measurement_value_4']='';
              }
              if(isset($key['measurement_level_5']))
              {
                $list_array['measurement_value_5']=$key['measurement_level_5'];
              }else {
                $list_array['measurement_value_5']='';
              }
              if(isset($key['measurement_level_6']))
              {
                $list_array['measurement_value_6']=$key['measurement_level_6'];
              }else {
                $list_array['measurement_value_6']='';
              }

              $list_array['item_id']=$key['item_id'];
              $list_array['neck_pattern']=$key['neck_attern'];
              $list_array['fabric']=$key['fabric'];
              if(isset($key['choose_image']))
              {
               $list_array['item_image_path']=$key['choose_image'];
             }else{
               $list_array['item_image_path']='';
             }


              $photo_client_design = "";
			      $photo_client_embroidery="";

        		if (isset($_FILES['client_design_' . $loop]) && $_FILES['client_design_' . $loop]['name'] != '') {
        			$image_name = 'client_design_' . $loop;
        			$photo_client_design = $this->my_file_upload($image_name, $Transaction_Td.'_'.$image_name);

        			if ($photo_client_design=='0') {
        				$sdata['exception'] = "Your Design file doesn't upload." . $this->upload->display_errors().' and max height: 500px ,max width: 500px.';
        				$this->session->set_userdata($sdata);
                $this->db->delete('tbl_tailor_order', array('id' => $tailor_order_id));
        				redirect("homes/tailor_profile/".$this->input->post('txt_tailor_id', true));
        			}
        		}

    		  	if (isset($_FILES['client_embroidery_' . $loop]) && $_FILES['client_embroidery_' . $loop]['name'] != '') {
    				        $image_name = 'client_embroidery_' . $loop;
                    $photo_client_embroidery = $this->my_file_upload($image_name,  $Transaction_Td.'_'.$image_name);

                   if ($photo_client_embroidery == '0') {
                        $sdata['exception'] = "Embroidery file doesn't upload." . $this->upload->display_errors().' and max height: 500px ,max width: 500px.';
                        $this->session->set_userdata($sdata);
                        $this->db->delete('tbl_tailor_order', array('id' => $tailor_order_id));
                        redirect("homes/tailor_profile/".$this->input->post('txt_tailor_id', true));
                    }
            }


              $list_array['client_design']=$photo_client_design;
              $list_array['embroidery']=$photo_client_embroidery;
              $final_array[$index]=$list_array;
              $index++;
              //$this->db->insert('tbl_tailor_order_details', $list_array);
            }
		     	$loop++;
          }
          // echo '<pre>';
          //  print_r($final_array);
          // die;

          $this->db->insert_batch('tbl_tailor_order_details', $final_array);
          $sdata['message'] = "Your order has been successfully submitted. We will contact you very soon!";
          $this->session->set_userdata($sdata);
         redirect("homes/tailor_profile/".$this->input->post('txt_tailor_id', true));
        }
    }
    function generateTransaction_Td(){
      $date = new DateTime();
      $code = $date->getTimestamp();
      $total_collection = count($this->db->query("SELECT id FROM tbl_tailor_order")->result_array());
      $code .= $total_collection;
      $code .= $this->generateRandomString(5);
      return $code;
    }
    function generateRandomString($length = 2) {
      $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < $length; $i++) {
      $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return $randomString;
    }
}
