<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Currencies extends CI_Controller
{
    public $notification = array();

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $employee_id = $user_info[0]->employee_id;
        //echo $employee_id; die;
        $this->notification = $this->Admin_login->get_notification($employee_id);

    }
    public function index()
    {

    $data = array();
    $data['title'] = 'Currencies';
    $data['heading_msg'] = "Currencies Information";
    $cond = array();
    $this->load->library('pagination');
    $config['base_url'] = site_url('currencies/index/');
    $config['per_page'] = 10;
    $config['total_rows'] = count($this->Admin_login->get_currencies_info(0, 0, $cond));
    $this->pagination->initialize($config);
    $data['currencies'] = $this->Admin_login->get_currencies_info(10, (int)$this->uri->segment(3), $cond);
    $data['counter'] = (int)$this->uri->segment(3);
    $data['is_show_button'] = "add";
    $data['branches'] = $this->db->query("SELECT * FROM tbl_branch")->result_array();
    $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
    $data['maincontent'] = $this->load->view('currencies/index', $data, true);
    $this->load->view('admin_logins/index', $data);
    }

    function add()
    {
        if($_POST){
            $data = array();
            $data['name'] = $this->input->post('name', true);
            $data['code'] = $this->input->post('code', true);


            $this->db->insert('cc_currencies', $data);
            $sdata['message'] = "You are Successfully Added Course Prefix Info !";
            $this->session->set_userdata($sdata);
            redirect("currencies/index");
        }else{
            $data = array();
            $data['title'] = 'Add Currencies Information';
            $data['heading_msg'] = "Add Currencies Information";
            $data['is_show_button'] = "index";
            $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
            $data['maincontent'] = $this->load->view('currencies/add', $data, true);
            $this->load->view('admin_logins/index', $data);
        }
    }

    function edit($id=null)
    {
        if($_POST){
            $data = array();
            $data['id'] = $this->input->post('id', true);
            $data['name'] = $this->input->post('name', true);
            $data['code'] = $this->input->post('code', true);


            $this->db->where('id', $data['id']);
            $this->db->update('cc_currencies', $data);
            $sdata['message'] = "You are Successfully Updated Course prefix Info !";
            $this->session->set_userdata($sdata);
            redirect("currencies/index");
        }else{
            $data = array();
            $data['title'] = 'Update Currencies Information';
            $data['heading_msg'] = "Update Currencies Information";
            $data['is_show_button'] = "index";
            $data['currencies'] = $this->Admin_login->get_currencies_info_by_unit_id($id);
            $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
            $data['maincontent'] = $this->load->view('currencies/edit', $data, true);
            $this->load->view('admin_logins/index', $data);
        }
    }

    function delete($id)
    {
        $this->db->delete('cc_currencies', array('id' => $id));
        $sdata['message'] = "Currencies Information Deleted Successfully !";
        $this->session->set_userdata($sdata);
        redirect("currencies/index");
    }

}
