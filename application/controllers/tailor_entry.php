<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tailor_entry extends CI_Controller
{
  public $notification = array();
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login','Email_process','Config_general','Tailor'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->db->query('SET SESSION sql_mode = ""');
        //set timezone
        date_default_timezone_set($user_info[0]->time_zone);

        $employee_id =  $user_info[0]->employee_id;
        //echo $employee_id; die;
        $this->notification = $this->Admin_login->get_notification($employee_id);
    }

    public function index()
    {
      //  echo CI_VERSION; die;
        $cond = array();
        if($_POST){
          $name = $this->input->post('name', true);
          $sdata['search_employee_name'] = $name;
          $this->session->set_userdata($sdata);
          $cond['name'] = $name;
        }else{
          $name = $this->session->userdata('search_employee_name');
          $cond['name'] = $name;
        }
        $data = array();
        $data['title'] = 'Tailor Information';
        $data['heading_msg'] = "Tailor Information";

        $this->load->library('pagination');
        $config['base_url'] = site_url('tailor_entry/index/');
        $config['per_page'] = 10;
        $config['total_rows'] = count($this->Tailor->get_all_tailor_list(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['tailors'] = $this->Tailor->get_all_tailor_list(10, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['is_show_button'] = "add";
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('tailor_entry/index', $data, true);
        $this->load->view('admin_logins/index', $data);
    }

    function add()
    {
      //  $is_employee_id_auto = $this->Config_general->read_by_purpose_and_db_field_name('general','employee_id_will_be_auto')[0]['default_value'];
        if ($_POST) {
        //  echo '<pre>';
        //  print_r($_POST);
          //die;
          $photo_name1 = "";$photo_name2="";$photo_name3="";$photo_name4="";$photo_name5="";$photo_name6="";
          $file1 = $_FILES["txtPhoto1"]['name'];
          $file2 = $_FILES["txtPhoto2"]['name'];
          $file3 = $_FILES["txtPhoto3"]['name'];
          $file4 = $_FILES["txtPhoto4"]['name'];
          $file5 = $_FILES["txtPhoto5"]['name'];
          $file6 = $_FILES["txtPhoto6"]['name'];
          if($file1!='')
          {
            $photo_name1=$this->my_file_upload("txtPhoto1","image_1_");
            if($photo_name1=='0')
            {
              $sdata['exception'] = "Image 1  doesn't upload." . $this->upload->display_errors().' and max height: 3000px ,max width: 3000px';
              $this->session->set_userdata($sdata);
              redirect('tailor_entry/add');
            }
          }
          if($file2!='')
          {
            $photo_name2=$this->my_file_upload("txtPhoto2","image_2_");
            if($photo_name2=='0')
            {
              $sdata['exception'] = "Image 12 doesn't upload." . $this->upload->display_errors().' and max height: 3000px ,max width: 3000px';
              $this->session->set_userdata($sdata);
              redirect('tailor_entry/add');
            }
          }

          if($file3!='')
          {
            $photo_name3=$this->my_file_upload("txtPhoto3","image_3_");
            if($photo_name3=='0')
            {
              $sdata['exception'] = "Image 3 doesn't upload." . $this->upload->display_errors().' and max height: 3000px ,max width: 3000px';
              $this->session->set_userdata($sdata);
              redirect('tailor_entry/add');
            }
          }

          if($file4!='')
          {
            $photo_name4=$this->my_file_upload("txtPhoto4","image_4_");
            if($photo_name4=='0')
            {
              $sdata['exception'] = "Image 4 doesn't upload." . $this->upload->display_errors().' and max height: 3000px ,max width: 3000px';
              $this->session->set_userdata($sdata);
              redirect('tailor_entry/add');
            }
          }
          if($file5!='')
          {
            $photo_name5=$this->my_file_upload("txtPhoto4","image_5_");
            if($photo_name5=='0')
            {
              $sdata['exception'] = "Image 5 doesn't upload." . $this->upload->display_errors().' and max height: 3000px ,max width: 3000px';
              $this->session->set_userdata($sdata);
              redirect('tailor_entry/add');
            }
          }
          if($file6!='')
          {
            $photo_name6=$this->my_file_upload("txtPhoto5","image_6_");
            if($photo_name6=='0')
            {
              $sdata['exception'] = "Image 6 doesn't upload." . $this->upload->display_errors().' and max height: 3000px ,max width: 3000px';
              $this->session->set_userdata($sdata);
              redirect('tailor_entry/add');
            }
          }
            $this->load->library('upload');
            $config['upload_path'] = 'media/tailor/';
            $config['allowed_types'] = 'jpg|png|JPEG|jpeg';
            $config['max_size'] = '6000';
            $config['max_width'] = '4000';
            $config['max_height'] = '4000';
            $this->upload->initialize($config);
            $extension = strtolower(substr(strrchr($_FILES['txtPhoto']['name'], '.'), 1));

            foreach ($_FILES as $field => $file) {
                if ($file['error'] == 0) {
                    $new_photo_name = "Tailor_" . time() . "_" . date('Y-m-d') . "." . $extension;
                    if (move_uploaded_file($_FILES["txtPhoto"]["tmp_name"], "media/tailor/" . $new_photo_name)) {
                        $data = array();
                        $data['name'] = $this->input->post('txtName', true);
                        $data['area_id'] = $this->input->post('area_id', true);
                        $data['division_id'] = $this->input->post('division_id', true);
                        $data['location'] = $this->input->post('location', true);
                        $data['rating'] = $this->input->post('rating', true);
                        $data['shop_amenities_id'] = $this->input->post('shop_amenities_id', true);
                        $data['tailor_cancel_policy'] = $this->input->post('tailor_cancel_policy', true);
                        $data['property_highlights'] = $this->input->post('property_highlights', true);
                        $data['establishment_year'] = $this->input->post('establishment_year', true);
                        $data['shop_opening_time'] = $this->input->post('shop_opening_time', true);
                        $data['gender'] = $this->input->post('txtGender', true);
                        $data['shop_closing_time'] = $this->input->post('shop_closing_time', true);
                        $data['payment_method'] = $this->input->post('payment_method', true);
                        $data['cc_currency_id'] = $this->input->post('cc_currency_id', true);
                        $data['image_path'] = $new_photo_name;
                        $data['status'] = 1;
                        $data['code'] = $this->generateTailorId($data['shop_opening_time']);
                        $data['about'] = $this->input->post('about', true);
                        $data['contact_person_name'] = $this->input->post('contact_person_name', true);
                        $data['contact_person_phone'] = $this->input->post('contact_person_phone', true);
                        $data['contact_person_email'] = $this->input->post('contact_person_email', true);
                        $data['contact_person_adrres'] = $this->input->post('contact_person_adrres', true);
                        $data['remarks'] = $this->input->post('remarks', true);
                        $data['slider_image_1'] = $photo_name1;
                        $data['slider_image_2'] = $photo_name2;
                        $data['slider_image_3'] = $photo_name3;
                        $data['slider_image_4'] = $photo_name4;
                        $data['slider_image_5'] = $photo_name5;
                        $data['slider_image_6'] = $photo_name6;
                         // echo '<pre>';
                         // print_r($_POST);
                         //  die;

                        if($this->Tailor->add_tailor_info($data)){
                        $tailor_id = $this->db->insert_id();
                        $shop_amenities_list['shop_amenities'] = $this->input->post('shop_amenities', true);
                        foreach ($shop_amenities_list['shop_amenities'] as $key) {
                          $amenities_list_array = array();
                          if(!empty($key['active'])){
                            $amenities_list_array['tailor_id']=$tailor_id;
                            $amenities_list_array['shop_amenities_id']=(int)$key['id'];
                            $this->db->insert('tbl_shop_amenities_details', $amenities_list_array);
                          }
                        }
                        $item_list['item_list'] = $this->input->post('items', true);
                        foreach ($item_list['item_list'] as $key) {
                          $item_list_array = array();
                          if(!empty($key['active'])){
                            $item_list_array['tailor_id']=$tailor_id;
                            $item_list_array['item_id']=(int)$key['id'];
                              $items_list_array['price']=$key['price'];
                            $this->db->insert('tbl_tailor_item_details', $item_list_array);
                          }
                        }
                            $sdata['message'] = "You are Successfully Tailor Information Added !";
                            $this->session->set_userdata($sdata);
                            redirect("tailor_entry/add");
                        }else{
                            $sdata['exception'] = "Sorry Tailor Doesn't Added !";
                            $this->session->set_userdata($sdata);
                            redirect("tailor_entry/add");
                        }
                    } else {
                        $sdata['exception'] = "Sorry Tailor Doesn't Added !" . $this->upload->display_errors();
                        $this->session->set_userdata($sdata);
                        redirect("tailor_entry/add");
                    }
                } else {
                    $sdata['exception'] = "Sorry Photo Does't Upload !";
                    $this->session->set_userdata($sdata);
                    redirect("tailor_entry/add");
                }
            }
        } else {
            $data = array();
            $data['title'] = 'Add New Tailor Information';
            $data['heading_msg'] = "Add New Tailor Information";
            $data['is_show_button'] = "index";
            $data['areas'] = $this->db->query("SELECT * FROM tbl_area where id=0")->result_array();
            $data['divisions'] = $this->db->query("SELECT * FROM tbl_division")->result_array();
            $data['currencies'] = $this->db->query("SELECT * FROM cc_currencies")->result_array();
            $data['shop_amenities'] = $this->db->query("SELECT * FROM tbl_shop_amenities")->result_array();
            $data['items'] = $this->db->query("SELECT id,name FROM tbl_tailor_item")->result_array();
            $data['is_employee_id_auto'] = 1;
            $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
            $data['maincontent'] = $this->load->view('tailor_entry/add', $data, true);
            $this->load->view('admin_logins/index', $data);
        }
    }


    function generateTailorId($shop_opening_time){
          $code="";
          $parts = explode('-', date("Y-m-d"));
          $code .= $parts[1]. substr($parts[0], -2);
          $total_tailor= count($this->db->query("SELECT id FROM tbl_tailor_entry")->result_array());
          return $code.str_pad(($total_tailor + 1), 4, '0', STR_PAD_LEFT);
    }

    function edit($id = null)
    {
        if ($_POST) {
            $new_photo_name=$this->input->post('old_image_path', true);
            $file = $_FILES["txtPhoto"]['name'];
            if ($file != '') {
                $old_file = $new_photo_name;
                if (!empty($old_file)) {
                    $filedel = PUBPATH . 'media/tailor/' . $old_file;
                    unlink($filedel);
                }
                $this->load->library('upload');
                $config['upload_path'] = 'media/tailor/';
                $config['allowed_types'] = 'jpg|png|JPEG|jpeg';
                $config['max_size'] = '6000';
                $config['max_width'] = '4000';
                $config['max_height'] = '4000';
                $this->upload->initialize($config);
                $extension = strtolower(substr(strrchr($_FILES['txtPhoto']['name'], '.'), 1));
                $new_photo_name = "Tailor_" . time() . "_" . date('Y-m-d') . "." . $extension;
                if (move_uploaded_file($_FILES["txtPhoto"]["tmp_name"], "media/tailor/" . $new_photo_name))
                {

                } else {
                    $sdata['exception'] = "Sorry Photo Does't Upload !";
                    $this->session->set_userdata($sdata);
                    redirect("tailor_entry/edit/".$this->input->post('name', true));
                }
              }

                      $data = array();

                      $photo_name1 = "";$photo_name2="";$photo_name3="";$photo_name4="";$photo_name5="";$photo_name6="";
                      $file1 = $_FILES["txtPhoto1"]['name'];
                      $file2 = $_FILES["txtPhoto2"]['name'];
                      $file3 = $_FILES["txtPhoto3"]['name'];
                      $file4 = $_FILES["txtPhoto4"]['name'];
                      $file5 = $_FILES["txtPhoto5"]['name'];
                      $file6 = $_FILES["txtPhoto6"]['name'];
                      if($file1!='')
                      {
                        $photo_name1=$this->my_file_upload("txtPhoto1","image_1_");
                        if($photo_name1=='0')
                        {
                          $sdata['exception'] = "Image 1 doesn't upload." . $this->upload->display_errors().' and max height: 3000px ,max width: 3000px';
                          $this->session->set_userdata($sdata);
                          redirect('tailor_entry/edit/'.$id);
                        }
                        $data['slider_image_1'] = $photo_name1;
                        $msg =$this->Admin_login->my_file_remove($_POST['oldtxtPhoto1'], $photo_name1, 'media/website/images/');

                      }
                      if($file2!='')
                      {
                        $photo_name2=$this->my_file_upload("txtPhoto2","image_2_");
                        if($photo_name2=='0')
                        {
                          $sdata['exception'] = "Image 2 doesn't upload." . $this->upload->display_errors().' and max height: 3000px ,max width: 3000px';
                          $this->session->set_userdata($sdata);
                          redirect('tailor_entry/edit/'.$id);
                        }
                        $data['slider_image_2'] = $photo_name2;
                        $msg =$this->Admin_login->my_file_remove($_POST['oldtxtPhoto2'], $photo_name2, 'media/website/images/');
                      }

                      if($file3!='')
                      {
                        $photo_name3=$this->my_file_upload("txtPhoto3","image_3_");
                        if($photo_name3=='0')
                        {
                          $sdata['exception'] = "Image 3 doesn't upload." . $this->upload->display_errors().' and max height: 3000px ,max width: 3000px';
                          $this->session->set_userdata($sdata);
                          redirect('tailor_entry/edit/'.$id);
                        }
                        $data['slider_image_3'] = $photo_name3;
                        $msg =$this->Admin_login->my_file_remove($_POST['oldtxtPhoto3'], $photo_name3, 'media/website/images/');
                      }

                      if($file4!='')
                      {
                        $photo_name4=$this->my_file_upload("txtPhoto4","image_4_");
                        if($photo_name4=='0')
                        {
                          $sdata['exception'] = "Image 4 doesn't upload." . $this->upload->display_errors().' and max height: 3000px ,max width: 3000px';
                          $this->session->set_userdata($sdata);
                            redirect('tailor_entry/edit/'.$id);
                        }
                        $data['slider_image_4'] = $photo_name4;
                        $msg =$this->Admin_login->my_file_remove($_POST['oldtxtPhoto4'], $photo_name4, 'media/website/images/');
                      }
                      if($file5!='')
                      {
                        $photo_name5=$this->my_file_upload("txtPhoto5","image_5_");
                        if($photo_name4=='0')
                        {
                          $sdata['exception'] = "Image 5 doesn't upload." . $this->upload->display_errors().' and max height: 3000px ,max width: 3000px';
                          $this->session->set_userdata($sdata);
                            redirect('tailor_entry/edit/'.$id);
                        }
                        $data['slider_image_5'] = $photo_name5;
                        $msg =$this->Admin_login->my_file_remove($_POST['oldtxtPhoto5'], $photo_name5, 'media/website/images/');
                      }
                      if($file6!='')
                      {
                        $photo_name6=$this->my_file_upload("txtPhoto6","image_6_");
                        if($photo_name5=='0')
                        {
                          $sdata['exception'] = "Image 6 doesn't upload." . $this->upload->display_errors().' and max height: 3000px ,max width: 3000px';
                          $this->session->set_userdata($sdata);
                            redirect('tailor_entry/edit/'.$id);
                        }
                        $data['slider_image_6'] = $photo_name6;
                        $msg =$this->Admin_login->my_file_remove($_POST['oldtxtPhoto6'], $photo_name5, 'media/website/images/');
                      }

                      $data['id'] = $this->input->post('id', true);
                      $data['name'] = $this->input->post('name', true);
                      $data['area_id'] = $this->input->post('area_id', true);
                      $data['division_id'] = $this->input->post('division_id', true);
                      $data['location'] = $this->input->post('location', true);
                      $data['rating'] = $this->input->post('rating', true);
                      $data['shop_amenities_id'] = $this->input->post('shop_amenities_id', true);
                      $data['tailor_cancel_policy'] = $this->input->post('tailor_cancel_policy', true);
                      $data['property_highlights'] = $this->input->post('property_highlights', true);
                      $data['establishment_year'] = $this->input->post('establishment_year', true);
                      $data['shop_opening_time'] = $this->input->post('shop_opening_time', true);
                      $data['gender'] = $this->input->post('txtGender', true);
                      $data['shop_closing_time'] = $this->input->post('shop_closing_time', true);
                      $data['payment_method'] = $this->input->post('payment_method', true);
                      $data['cc_currency_id'] = $this->input->post('cc_currency_id', true);
                      $data['image_path'] = $new_photo_name;
                      $data['code'] = $this->input->post('code', true);
                      $data['about'] = $this->input->post('about', true);
                      $data['contact_person_name'] = $this->input->post('contact_person_name', true);
                      $data['contact_person_phone'] = $this->input->post('contact_person_phone', true);
                      $data['contact_person_email'] = $this->input->post('contact_person_email', true);
                      $data['contact_person_adrres'] = $this->input->post('contact_person_adrres', true);
                      // print_r($data);
                      // die;
                      $this->db->where('id', $data['id']);
                      $this->db->update('tbl_tailor_entry', $data);

                      $this->db->delete('tbl_shop_amenities_details', array('tailor_id' => $data['id']));
                      $tailor_id = $data['id'];
                      $shop_amenities_list['shop_amenities'] = $this->input->post('shop_amenities', true);
                      foreach ($shop_amenities_list['shop_amenities'] as $key) {
                        $amenities_list_array = array();
                        if(!empty($key['active'])){
                          $amenities_list_array['tailor_id']=$tailor_id;
                          $amenities_list_array['shop_amenities_id']=(int)$key['id'];
                          $this->db->insert('tbl_shop_amenities_details', $amenities_list_array);
                        }
                      }
                      $this->db->delete('tbl_tailor_item_details', array('tailor_id' => $data['id']));
                      $item_list['item_list'] = $this->input->post('items', true);
                      foreach ($item_list['item_list'] as $key) {
                        $items_list_array = array();
                        if(!empty($key['active'])){
                          $items_list_array['tailor_id']=$tailor_id;
                          $items_list_array['item_id']=(int)$key['id'];
                          $items_list_array['price']=$key['price'];
                          $this->db->insert('tbl_tailor_item_details', $items_list_array);
                        }
                      }
                      $sdata['message'] = "You are Successfully Tailor Information Updated ! ";
                      $this->session->set_userdata($sdata);
                      redirect("tailor_entry/index");

        } else {
            $data = array();
            $data['title'] = 'Update Tailor Information';
            $data['heading_msg'] = "Update Tailor Information";
            $data['is_show_button'] = "index";
            $data['tailor_info'] = $this->Tailor->get_tailor_info_by_id($id);
            $division_id=$data['tailor_info']->division_id;

            $data['areas'] = $this->db->query("SELECT * FROM tbl_area where division_id=$division_id")->result_array();
            $data['divisions'] = $this->db->query("SELECT * FROM tbl_division")->result_array();
            $data['currencies'] = $this->db->query("SELECT * FROM cc_currencies")->result_array();
            $data['shop_amenities'] = $this->db->query("SELECT d.`id`,d.`name`,  1 active FROM `tbl_shop_amenities_details` AS s
                              INNER JOIN tbl_shop_amenities AS d ON s.`shop_amenities_id` =d.id
                              WHERE s.`tailor_id`=$id
                              UNION ALL
                              SELECT m.`id`,m.`name`,  0 active FROM tbl_shop_amenities AS m
                              WHERE m.`id` NOT IN(
                              SELECT s.`shop_amenities_id` FROM `tbl_shop_amenities_details` AS s
                              INNER JOIN tbl_shop_amenities AS d ON s.`shop_amenities_id` =d.id
                              WHERE s.`tailor_id`=$id)")->result_array();
            $data['items'] = $this->db->query("SELECT d.`id`,d.`name`,  1 active,s.`price` FROM `tbl_tailor_item_details` AS s
                              INNER JOIN tbl_tailor_item AS d ON s.`item_id` =d.id
                              WHERE s.`tailor_id`=$id
                              UNION ALL
                              SELECT m.`id`,m.`name`,  0 active,NULL price FROM tbl_tailor_item AS m
                              WHERE m.`id` NOT IN(
                              SELECT s.`item_id` FROM `tbl_tailor_item_details` AS s
                              INNER JOIN tbl_tailor_item AS d ON s.`item_id` =d.id
                              WHERE s.`tailor_id`=$id)")->result_array();
            $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
            $data['maincontent'] = $this->load->view('tailor_entry/edit', $data, true);
            $this->load->view('admin_logins/index', $data);
        }
    }

    function delete($id)
    {
        $employee_photo_info = $this->Employee->get_employee_photo_info_by_employee_id($id);
        $file = $employee_photo_info[0]['photo_location'];
        if (!empty($file)) {
            $filedel = PUBPATH . 'media/employee/' . $file;
            if (unlink($filedel)) {
                $this->Employee->delete_employee_info_by_employee_id($id);
            } else {
                $this->Employee->delete_employee_info_by_employee_id($id);
            }
        } else {
            $this->Employee->delete_employee_info_by_employee_id($id);
        }

        $sdata['message'] = "Employee Information Deleted Successfully !";
        $this->session->set_userdata($sdata);
        redirect("employees/index");
    }

    function updateMsgStatusTailorStatus()
    {
        $status = $this->input->get('status', true);
        $id = $this->input->get('id', true);
        $data = array();
        $data['id'] = $this->input->get('id', true);
        if ($status == 1) {
            $data['status'] = 0;
        } else {
            $data['status'] = 1;
        }
        $this->Tailor->update_tailor_info($data);
        if ($status == 0) {
            echo '<a class="approve_icon" title="Active" href="#" onclick="msgStatusUpdate(' . $id . ',1)"><button type="button" class="btn btn-primary btn-xs mb-1">Active</button></a>';
        } else {
            echo '<a class="reject_icon" title="Inactive" href="#" onclick="msgStatusUpdate(' . $id . ',0)"><button type="button" class="btn btn-primary btn-xs mb-1">Inactive</button></a>';
        }
    }
    function myprofile()
    {
            $user_info = $this->session->userdata('user_info');
            $employee_id =  $user_info[0]->employee_id;
            $data = array();
            $data['title'] = 'MY Profile';
            $data['heading_msg'] = "MY Profile";
            //$data['is_show_button'] = "index";
            $data['employees'] = $this->db->query("SELECT id,name,code FROM tbl_employee")->result_array();
            $data['employee_info'] = $this->Employee->get_employee_photo_info_by_employee_id($employee_id);
            $data['branches'] = $this->db->query("SELECT * FROM tbl_branch")->result_array();
            $data['sections'] = $this->db->query("SELECT * FROM tbl_employee_section")->result_array();
            $data['post'] = $this->db->query("SELECT * FROM tbl_employee_post")->result_array();
            $data['shifts'] = $this->Admin_login->get_all_shift();
            $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
            $data['maincontent'] = $this->load->view('employee_infos/myprofile', $data, true);
            $this->load->view('admin_logins/index', $data);

    }
    public function getAreaByDivisionId()
    {
        $division_id = $this->input->get('division_id', true);

        $data = array();
        $data['areas'] = $this->db->query("SELECT e.`id`,e.name FROM `tbl_area` AS e
                             WHERE e.division_id ='$division_id'
                            ORDER BY e.name")->result_array();
        $this->load->view('tailor_entry/area_list', $data);
    }

    public function my_file_upload($filename, $type)
    {
       $ext = explode('.',$_FILES[$filename]['name']);
       $new_file_name = $type.time().'.'.end($ext);

        // print_r(rand());
        // die();
        $this->load->library('upload');

        $config = array(
                'upload_path' => "media/website/images/",
                'allowed_types' => "jpg|png|jpeg|JPEG|JPG|PNG|pdf|csv|doc|docx",
                'max_size' => "99999999999",
                'max_height' => "3000",
                'max_width' => "3000",
                'file_name' => $new_file_name
            );

        $this->upload->initialize($config);
        if (!$this->upload->do_upload($filename)) {
            return '0';
        } else {
            return $new_file_name;
        }
    }

}
