<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class News extends CI_Controller
{
public $notification = array();
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $employee_id =  $user_info[0]->employee_id;
        $this->notification = $this->Admin_login->get_notification($employee_id);
        $this->db->query('SET SESSION sql_mode = ""');
    }

    public function index()
    {
      $data = array();
      $data['title'] = 'News';
      $data['heading_msg'] = "News Info";
      $data['news'] = $this->db->query("SELECT * FROM tbl_news")->result_array();
      $data['is_show_button'] = "add";
      $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
      $data['maincontent'] = $this->load->view('news/index', $data, true);
      $this->load->view('admin_logins/index', $data);
    }


    function add()
    {
      if($_POST){
        $data = array();

        $this->load->library('upload');
        $config['upload_path'] = 'media/website/images/news/';
        $config['allowed_types'] = 'jpg|png|JPEG|jpeg';
        $config['max_size'] = '6000';
        $config['max_width'] = '4000';
        $config['max_height'] = '4000';
        $this->upload->initialize($config);
        $extension = strtolower(substr(strrchr($_FILES['txtPhoto']['name'], '.'), 1));
        $new_photo_name = "news_" . time() . "_" . date('Y-m-d') . "." . $extension;
        if (move_uploaded_file($_FILES["txtPhoto"]["tmp_name"], "media/website/images/news/" . $new_photo_name))
        {

        } else {
            $sdata['exception'] = "Sorry Photo Does't Upload !";
            $this->session->set_userdata($sdata);
            redirect("news/add");
        }
        $data['title'] = $this->input->post('title', true);
        $data['date'] = $this->input->post('date', true);
        $data['image_path'] = $new_photo_name;
        $data['description'] = $this->input->post('description', true);
        $data['status'] = $this->input->post('status', true);
        $this->db->insert('tbl_news', $data);
        $sdata['message'] = "You Successfully Added Contact Info !";
        $this->session->set_userdata($sdata);
        redirect("news/index");
      }else{
        $data = array();
        $data['title'] = 'Add News Info';
        $data['heading_msg'] = "Add News Info";
        $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('news/add', $data, true);
        $this->load->view('admin_logins/index', $data);
      }
  }

  function edit($id=null)
  {
    if($_POST){
      $data = array();
      $new_photo_name=$this->input->post('old_image_path', true);
       $file = $_FILES["txtPhoto"]['name'];
       if ($file != '') {
           $old_file = $new_photo_name;
           if (!empty($old_file)) {
               $filedel = PUBPATH . 'media/website/images/news/' . $old_file;
               unlink($filedel);
           }
           $this->load->library('upload');
           $config['upload_path'] = 'media/website/images/';
           $config['allowed_types'] = 'jpg|png|JPEG|jpeg';
           $config['max_size'] = '6000';
           $config['max_width'] = '4000';
           $config['max_height'] = '4000';
           $this->upload->initialize($config);
           $extension = strtolower(substr(strrchr($_FILES['txtPhoto']['name'], '.'), 1));
           $new_photo_name = "news_" . time() . "_" . date('Y-m-d') . "." . $extension;
           if (move_uploaded_file($_FILES["txtPhoto"]["tmp_name"], "media/website/images/news/" . $new_photo_name))
           {

           } else {
               $sdata['exception'] = "Sorry Photo Does't Upload !";
               $this->session->set_userdata($sdata);
               redirect("news/edit/".$this->input->post('id', true));
           }
         }
      $data['id'] = $this->input->post('id', true);
      $data['title'] = $this->input->post('title', true);
      $data['date'] = $this->input->post('date', true);
      $data['image_path'] = $new_photo_name;
      $data['description'] = $this->input->post('description', true);
      
      $this->db->where('id', $data['id']);
      $this->db->update('tbl_news', $data);
      $sdata['message'] = "You Successfully News Info !";
      $this->session->set_userdata($sdata);
      redirect("news/index");
    }else{
      $data = array();
      $data['title'] = 'News Info';
      $data['heading_msg'] = "Update News Info";
      $data['is_show_button'] = "index";
      $data['news'] = $this->Admin_login->get_news_info($id);
      $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
      $data['maincontent'] = $this->load->view('news/edit', $data, true);
      $this->load->view('admin_logins/index', $data);
    }
}

  function delete($id)
  {
      $this->db->delete('tbl_news', array('id' => $id));
      $sdata['message'] = "News Deleted Successfully !";
      $this->session->set_userdata($sdata);
      redirect("news/index");
  }

  function updateMsgStatusNewsStatus()
  {
      $status = $this->input->get('status', true);
      $id = $this->input->get('id', true);
      $data = array();
      $data['id'] = $this->input->get('id', true);
      if ($status == 1) {
          $data['status'] = 0;
      } else {
          $data['status'] = 1;
      }
      $this->db->where('id', $data['id']);
      $this->db->update('tbl_news', $data);
      if ($status == 0) {
          echo '<a class="approve_icon" title="Active" href="#" onclick="msgStatusUpdate(' . $id . ',1)"><button type="button" class="btn btn-primary btn-xs mb-1">Active</button></a>';
      } else {
          echo '<a class="reject_icon" title="Inactive" href="#" onclick="msgStatusUpdate(' . $id . ',0)"><button type="button" class="btn btn-primary btn-xs mb-1">Inactive</button></a>';
      }
  }

}
