<?php

class User_roles extends CI_Controller
{
public $notification = array();
    function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model(array('Admin_login','User_role', 'User_role_wise_privilege'));
        $this->load->library(array('session'));
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $employee_id =  $user_info[0]->employee_id;
        //echo $employee_id; die;
        $this->notification = $this->Admin_login->get_notification($employee_id);
    }

    function index()
    {
        $data = array();
        $data['heading_msg'] = $data['title'] = 'User role';
        $data['user_roles'] = $this->User_role->get_list();
        $data['is_show_button'] = "add";
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('user_roles/index', $data, true);
        $this->load->view('admin_logins/index', $data);
    }

    function add()
    {
        if ($_POST) {
            if ($this->User_role->add($this->input->post())) {
                $sdata['message'] = "Role Information Added";
                $this->session->set_userdata($sdata);
                redirect("user_roles/index");
            } else {
                $sdata['message'] = "Role Information could not add";
                $this->session->set_userdata($sdata);
                redirect("user_roles/add");
            }
        }
        $data = array();
        $data['heading_msg'] = $data['title'] = 'User role';
        $data['is_show_button'] = "index";
        $data['action'] = 'add';
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('user_roles/save', $data, true);
        $this->load->view('admin_logins/index', $data);
    }

    function edit($id)
    {
        if ($_POST) {
            if ($this->User_role->edit($this->input->post(),$id)) {
                $sdata['message'] = "Information Successfully updated.";
                $this->session->set_userdata($sdata);
                redirect("user_roles/index");
            } else {
                $sdata['message'] = "Information could not add";
                $this->session->set_userdata($sdata);
                redirect("user_roles/index");
            }
        }
        $data = array();
        $data['heading_msg'] = $data['title'] = 'User role';
        $data['action'] = 'edit/' . $id;
        $data['row'] = $this->User_role->read($id);
        $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('user_roles/save', $data, true);
        $this->load->view('admin_logins/index', $data);
    }

    public function delete($id)
    {
        $this->User_role->delete($id);
        $sdata['message'] = "Information Deleted!";
        $this->session->set_userdata($sdata);
        redirect("user_roles/index");
    }


}
