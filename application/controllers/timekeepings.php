<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Timekeepings extends CI_Controller
{
    public $notification = array();
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login','Timekeeping', 'common/insert_model', 'common/custom_methods_model',));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }

        //set timezone
        date_default_timezone_set($user_info[0]->time_zone);
        $employee_id =  $user_info[0]->employee_id;
        //echo $employee_id; die;
        $this->notification = $this->Admin_login->get_notification($employee_id);
    }

    public function index()
    {
      //  date_default_timezone_set("Asia/Hong_Kong");
      //  echo date('Y-m-d H:i:s'); die;

        $data = array();

        $session_user = $this->session->userdata('user_info');
        $employee_id = $session_user[0]->employee_id;
        $employee_info = $this->db->query("SELECT branch_id FROM tbl_employee WHERE id = '$employee_id'")->result_array();
        $branch_id =  $employee_info[0]['branch_id'];
        $branch_info = $this->db->query("SELECT b.id,b.name,t.value as time_zone FROM tbl_branch as b
                       INNER JOIN tbl_timezone as t ON t.id = b.time_zone_id
                       WHERE b.id = '$branch_id'")->result_array();
        $time_zone = $branch_info[0]['time_zone'];
        date_default_timezone_set($time_zone);
        $date = date('Y-m-d');
        $data['todays_att_data'] = $this->db->query("SELECT * FROM tbl_logins WHERE employee_id = '$employee_id' AND date = '$date'")->result_array();
        $data['title'] = 'Manual Attenndance';
        $data['heading_msg'] = "Manual Attenndance";
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('timekeepings/manual_attendance', $data, true);
        $this->load->view('admin_logins/index', $data);
    }


    function self_manual_login(){
      $session_user = $this->session->userdata('user_info');
      $employee_id = $session_user[0]->employee_id;
      $employee_info = $this->db->query("SELECT e.branch_id,e.shift_id,s.start_time,s.end_time  FROM tbl_employee AS e
        LEFT JOIN tbl_shift as s ON s.id = e.shift_id
         WHERE e.id = '$employee_id'")->result_array();
      $branch_id =  $employee_info[0]['branch_id'];
      $shift_id =  $employee_info[0]['shift_id'];

      if($shift_id == 0 || $shift_id == ''){
          $branch_info = $this->db->query("SELECT b.id,b.shift_id,s.start_time,s.end_time FROM tbl_branch as b
                       INNER JOIN tbl_shift as s ON s.id = b.shift_id
                       WHERE b.id = '$branch_id'")->result_array();
          $expected_login_time = $branch_info[0]['start_time'];
          $expected_logout_time = $branch_info[0]['end_time'];
          $shift_id =  $branch_info[0]['shift_id'];
      }else{
          $expected_login_time = $employee_info[0]['start_time'];
          $expected_logout_time = $employee_info[0]['end_time'];
      }



      $date = date('Y-m-d');
      $login_data = array();
      $login_data['employee_id'] = $employee_id;
      $login_data['shift_id'] = $shift_id;
      $login_data['branch_id'] = $branch_id;
      $login_data['login_time'] = date('H:i:s');
      $login_data['logout_time'] = null;
      $login_data['expected_login_time'] = $expected_login_time;
      $login_data['expected_logout_time'] = $expected_logout_time;
      $login_data['date'] = $date;
      $login_data['is_manual_login'] = '1';
      //echo '<pre>';
      //print_r($login_data);
      //die;
      $this->insert_model->add("tbl_logins", $login_data);
      $sdata['message'] = 'Login Successfully';
      $this->session->set_userdata($sdata);
      redirect("timekeepings/index");
    }

    function self_manual_logout(){
      $session_user = $this->session->userdata('user_info');
      $employee_id = $session_user[0]->employee_id;
      $date = date('Y-m-d');
      $todays_att_data = $this->db->query("SELECT id FROM tbl_logins WHERE employee_id = '$employee_id' AND date = '$date'")->result_array();
      if(empty($todays_att_data)){
        $sdata['exception'] = 'Login Information not found !';
        $this->session->set_userdata($sdata);
        redirect("timekeepings/index");
      }
      $login_data = array();
      $login_data['id'] = $todays_att_data[0]['id'];
      $login_data['logout_time'] = date('H:i:s');
      //echo '<pre>';
      //print_r($login_data);
      //die;
      $this->db->where('id', $login_data['id']);
      $this->db->update('tbl_logins', $login_data);
      $sdata['message'] = 'Logout Successfully';
      $this->session->set_userdata($sdata);
      redirect("timekeepings/index");
    }



}
