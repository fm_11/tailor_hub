<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Contact extends CI_Controller
{
public $notification = array();
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $employee_id =  $user_info[0]->employee_id;
        //echo $employee_id; die;
        $this->notification = $this->Admin_login->get_notification($employee_id);
    }

    public function index()
    {
      $data = array();
      $data['title'] = 'Contact Information';
      $data['heading_msg'] = "Contact Information";
      $data['contact_info'] = $this->Admin_login->get_contact();
      //echo '<pre>';
      //print_r($data['contact_info']); die;
      $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
      $data['maincontent'] = $this->load->view('contact/index', $data, true);
      $this->load->view('admin_logins/index', $data);
    }


    function update_contact_info()
    {
            $data = array();
            $data['id'] = $this->input->post('id', true);
            $data['address'] = $this->input->post('address', true);
            $data['email'] = $this->input->post('email', true);
            $data['phone'] = $this->input->post('phone', true);
            $data['open_day'] = $this->input->post('open_day', true);
            $data['open_time'] = $this->input->post('open_time', true);
            $data['closed_day'] = $this->input->post('closed_day', true);
            $data['closed_time'] = $this->input->post('closed_time', true);
          	if (isset($_FILES['banner']) && $_FILES['banner']['name'] != '')
            {
              $image_name = 'banner';
              $photo = $this->my_file_upload($image_name,$image_name);

              if ($photo=='0') {
                $sdata['exception'] = "Your banner file doesn't upload." . $this->upload->display_errors().' and max height: 930px ,max width: 1920px.';
                $this->session->set_userdata($sdata);
                redirect("contact/index");
              }
                  $data['banner_path'] = $photo;
            }
            $this->Admin_login->update_contact($data);
            $sdata['message'] = "You Successfully Updated Contact Info ! ";
            $this->session->set_userdata($sdata);
            redirect("contact/index");
    }
    public function my_file_upload($filename, $type)
    {
       $ext = explode('.',$_FILES[$filename]['name']);
       $new_file_name = 'home_'.$type .time(). '.' .end($ext);

      // print_r(rand());
      // die();
      $this->load->library('upload');

      $config = array(
          'upload_path' => "media/website/banner_image/",
          'allowed_types' => "jpg|png|jpeg|JPEG|JPG|PNG",
          'max_size' => "99999999999",
          'max_height' => "930",
          'max_width' => "1920",
          'file_name' => $new_file_name
        );

      $this->upload->initialize($config);
      //  $xx =$this->upload->do_upload($filename);


      if (!$this->upload->do_upload($filename)) {
        return '0';
      } else {
        return $new_file_name;
      }
    }

}
