<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Intake_Admission_Report extends CI_Controller
{
public $notification = array();
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login','Employee'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        //set timezone
        date_default_timezone_set($user_info[0]->time_zone);
        $employee_id =  $user_info[0]->employee_id;
        //echo $employee_id; die;
        $this->notification = $this->Admin_login->get_notification($employee_id);
    }

    public function index()
    {
      $data = array();
      $user_info = $this->session->userdata('user_info');
      $employee_id =  $user_info[0]->employee_id;
      $user_id =$user_info[0]->id;
      if($_POST){
        $userid = $this->input->post('user_id', true);
        $intake_years = $this->input->post('intake_years', true);
        $intake_months = $this->input->post('intake_months', true);
        $intake_years_details = $this->Admin_login->get_intake_years_by_id($intake_years);
        $intake_months_details = $this->Admin_login->get_intake_months_by_id($intake_months);
        $data['intake']=$intake_months_details[0]['name'].'-'.$intake_years_details[0]['name'];
        $data['total_day'] =cal_days_in_month(CAL_GREGORIAN, $intake_months_details[0]['weight'], $intake_years_details[0]['name']);

        $data['employee_info']=$this->Admin_login->get_user_common_info($userid);
        $data['title'] = 'Admission Report(Individual)';
        $data['heading_msg'] = "Admission Report(Individual)";
        $data['report_to']=$this->Admin_login->get_user_head_of_admission_designation($userid);
        $data['approved_by']=$this->Admin_login->get_user_head_of_process_designation($userid);
        $data['printed_by']=$this->Employee->getEmployeeName($employee_id);
        $data['rData'] = $this->Admin_login->get_intake_admission_report_data($userid,$intake_years,$intake_months);
        $data['status_Data'] = $this->Admin_login->get_intake_admission_status_report_data($userid,$intake_years,$intake_months);

        if(empty($data['rData']) && empty($data['status_Data'])){
          $sdata['exception'] = "Data not found !";
          $this->session->set_userdata($sdata);
          redirect("intake_admission_report/index");
        }
        $data['org_info'] = $this->Admin_login->getReportHeaderAddress($data['employee_info'][0]['branch_id']);
        $excel = $this->input->post('excel', true);
      //  echo $excel; die;
        if(isset($excel) && $excel != ''){
          $data['excel'] = 1;
          $this->load->view('intake_admission_report/intake_admission_report_table', $data);
          //die;
        }else{
          $data['report'] = $this->load->view('intake_admission_report/intake_admission_report_table', $data, true);
        }
      }

      if(!isset($excel) || $excel == ''){
        $data['title'] = 'Admission Report(Individual)';
        $data['heading_msg'] = "Admission Report(Individual)";
        $user_type=$this->Admin_login->get_user_type($user_id);
        if($user_type=="hop")
        {
            $data['branches'] = $this->Admin_login->get_all_branch_list();
            //empty load
           $data['employees'] = $this->Admin_login->get_employee_list_by_employeeid(0);
        }elseif($user_type=="hoa")
        {
          $data['employees'] = $this->Admin_login->get_employee_list_by_employeeid($employee_id);
        }

        $data['user_ids']=$user_id;
        $data['user_type'] =$user_type;
        $data['intake_years'] =$this->Admin_login->get_intake_years();
        $data['intake_months'] =$this->Admin_login->get_intake_months();
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('intake_admission_report/index', $data, true);
        $this->load->view('admin_logins/index', $data);
      }
    }
    public function getEmployeeByBrachId()
    {
        $branch_id = $this->input->get('branch_id', true);
        // print_r($branch_id);
        // $data = array();
        $data['employees'] = $this->db->query("SELECT u.`id`,e.name FROM `tbl_employee` AS e
                            INNER JOIN tbl_user AS u ON e.id=u.`employee_id` WHERE e.branch_id ='$branch_id'
                            ORDER BY e.name")->result_array();
        $this->load->view('intake_admission_report/employee_list', $data);
    }


}
