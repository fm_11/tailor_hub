<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Students_profile extends CI_Controller
{
    public $notification = array();

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login','Email_process','Employee'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (!empty($user_info)) {
          $employee_id = $user_info[0]->employee_id;
          $this->notification = $this->Admin_login->get_notification($employee_id);
        }
$this->db->query('SET SESSION sql_mode = ""');
    }

    public function lead_transfer($lead_id = null)
    {
        if ($_POST) {
//            echo '<pre>';
//            print_r($_POST);
//            die;
          if ($this->Admin_login->check_ifexist_email_applicant($this->input->post('email', true))) {
              $sdata['exception'] = "This email already exist.";
              $this->session->set_userdata($sdata);
              redirect("students_profile/lead_transfer/".$this->input->post('lead_id', true));
          }
          if ($this->Admin_login->check_ifexist_phone_applicant($this->input->post('mobile', true))) {
              $sdata['exception'] = "This mobile number already exist.";
              $this->session->set_userdata($sdata);
              redirect("students_profile/lead_transfer/".$this->input->post('lead_id', true));
          }
            $data = array();
            if (isset($_FILES['passport_file']) && $_FILES['passport_file']['name'] != '') {
                $passport_file_name = $this->my_file_upload('passport_file', 'passport');
                if ($passport_file_name=='0') {
                    $sdata['exception'] = "Passport doesn't upload." . $this->upload->display_errors()." and max height: 3000px ,max width: 3000px";
                    $this->session->set_userdata($sdata);
                    redirect("students_profile/lead_transfer/".$this->input->post('lead_id', true));
                }
                $data['passport_file_location'] = $passport_file_name;
            }
            if (isset($_FILES['cv_file']) && $_FILES['cv_file']['name'] != '') {
                $cv_file_name = $this->my_file_upload('cv_file', 'cv');
                if ($cv_file_name=='0') {
                    $sdata['exception'] = "CV doesn't upload." . $this->upload->display_errors()." and max height: 3000px ,max width: 3000px";
                    $this->session->set_userdata($sdata);
                    redirect("students_profile/lead_transfer/".$this->input->post('lead_id', true));
                }
                $data['cv_file_location'] = $cv_file_name;
            }

            if ($this->input->post('password', true)=='') {
                $sdata['exception'] = "Password is required field.";
                $this->session->set_userdata($sdata);
                redirect("students_profile/lead_transfer/".$this->input->post('lead_id', true));
            }
            $data['lead_id'] = $this->input->post('lead_id', true);
            $data['first_name'] = $this->input->post('first_name', true);
            $data['last_name'] = $this->input->post('last_name', true);
            $data['date_of_birth'] = $this->input->post('date_of_birth', true);
            $data['nationality'] = $this->input->post('nationality', true);
            $data['mobile'] = $this->input->post('mobile', true);
            $data['email'] = $this->input->post('email', true);
            $data['skype_id'] = $this->input->post('skype_id', true);
            $data['passport_no'] = $this->input->post('passport_no', true);
            $data['issue_date'] = $this->input->post('issue_date', true);
            $data['expair_date'] = $this->input->post('expair_date', true);
            $data['is_valid_passport'] = $this->input->post('is_valid_passport', true);
            $data['gender_id'] = $this->input->post('gender_id', true);
            $data['marital_status_id'] = $this->input->post('marital_status_id', true);
            $data['cor_address'] = $this->input->post('cor_address', true);
            $data['cor_city'] = $this->input->post('cor_city', true);
            $data['zip_code'] = $this->input->post('zip_code', true);
            $data['country_id'] = $this->input->post('country_id', true);
            $data['lead_source_id'] = $this->input->post('lead_source_id', true);
            $data['origin_office'] = $this->input->post('origin_office', true);
            $data['intake_month_id'] = $this->input->post('intake_month_id', true);
            $data['intake_year_id'] = $this->input->post('intake_year_id', true);
            $data['applicant_code'] = $this->generateApplicantsId($data['origin_office']);
            $data['registration_date'] = date('Y-m-d');
            if ($this->input->post('password', true) != '') {
              $data['password'] = md5($this->input->post('password', true));
            }
            $user_info = $this->session->userdata('user_info');
            $data['admission_officer_id'] = $user_info[0]->id;
            $data['country_code'] = $this->input->post('country_code', true);

           $mail=$this->send_email_for_applicant($data['admission_officer_id'],($data['first_name'].' '.$data['last_name']),($this->input->post('password', true)),$data['email']);

            $this->db->insert('applicants', $data);

           $status_mess=" and successfully email send.";
           if(!$mail)
           {
               $status_mess=" and email cannot send. ";
           }
            $data = array();
            $data['id'] = $this->input->post('lead_id', true);
            $data['is_registered'] = 1;
            $this->db->where('id', $data['id']);
            $this->db->update('leads', $data);

            $sdata['message'] = "You are successfully transfer to admission department".$status_mess;
            $this->session->set_userdata($sdata);
            redirect("leads/my_desk");
        } else {
         $user_info = $this->session->userdata('user_info');
         $user_type=$this->Admin_login->get_user_type($user_info[0]->id);

         if($user_type!='ao' && $user_type!='hoa' && $user_type!='hop')
         {
           $sdata['exception'] = "You have no permission";
           $this->session->set_userdata($sdata);
           redirect("leads/my_desk");
         }

            $data = array();
            $data['title'] = 'Student Profile';
            $data['heading_msg'] = "Student Profile";
            //$data['is_show_button'] = "index";
            $data['marital_status'] = $this->db->query("SELECT * FROM cc_marital_status")->result_array();
            $data['genders'] = $this->db->query("SELECT * FROM cc_genders")->result_array();
            $data['countries'] = $this->db->query("SELECT * FROM cc_countries")->result_array();
            $data['origin_office'] = $this->db->query("SELECT * FROM tbl_branch")->result_array();
            $data['lead_source'] = $this->db->query("SELECT * FROM cc_lead_sources")->result_array();
            $data['country_cities'] = $this->db->query("SELECT * FROM country_cities")->result_array();
            $data['course_level'] = $this->db->query("SELECT * FROM cc_course_level")->result_array();
            $data['intake_years'] =$this->Admin_login->get_intake_years();
            $data['intake_months'] =$this->Admin_login->get_intake_months();
            $data['country_code'] =$this->Admin_login->get_country_code();
            $data['lead_info'] = $this->db->query("SELECT * FROM leads WHERE `id`  = '$lead_id'")->result_array();
            $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
            $data['maincontent'] = $this->load->view('students_profile/add', $data, true);
            $this->load->view('admin_logins/index', $data);
        }
    }

    public function generateApplicantsId($branch_id)
    {
        $branches = $this->db->query("SELECT * FROM tbl_branch WHERE id = '$branch_id'")->result_array();
        $code = "ST-";
        $code .= $branches[0]['code'];
        $total_applicants = count($this->db->query("SELECT id FROM applicants")->result_array());
        return $code.str_pad(($total_applicants + 1), 3, '0', STR_PAD_LEFT);
    }
    function send_email_for_applicant($admission_officer_id,$applicant_name,$passord,$applicant_email){

       $info =$this->Admin_login->get_admission_officer_info($admission_officer_id);
         //reciver
        $receivers = $applicant_email;
         //subject
        $header = "Congratulation";
       //message
       $message = "";

       $message=$message.'Your account is successfully registered. Press sign in button to sing in your account. <br>';
       $message=$message.'Your User Id: <b>'.$applicant_email.'</b><br>';
       $message=$message.'Your Password: <b>'.$passord.'</b><br>';




        //   print_r($receivers);
        // print_r($header);
        //   print_r($message);
        //           die();
        $email_data = array();
        $session_user = $this->session->userdata('user_info');

        if(empty($session_user))
        {
          $email_data['sender_name'] = $info[0]['name'] . '(' . $info[0]['code'] . ')';
          $email_data['sender_designation_name'] = $info[0]['post_name'];
          $email_data['sender_branch_name'] = $info[0]['branch_name'];
        }else{
          print_r($info);
          die();
          $employee_id = $session_user[0]->employee_id;
          $employee_info = $this->Employee->getEmployeeName($employee_id);
          $email_data['sender_name'] = $employee_info[0]['name'] . '(' . $employee_info[0]['code'] . ')';
          $email_data['sender_designation_name'] = $employee_info[0]['designation_name'];
          $email_data['sender_branch_name'] = $employee_info[0]['branch_name'];
        }

         $email_data['receiver_name'] = $applicant_name;
        // $email_data['admission_name'] = $info[0]['name'] ;
        // $email_data['admission_email'] = $info[0]['email'];
        // $email_data['admission_mobile'] = $info[0]['mobile'];
        $email_data['file_link'] = "http://achi365.com/spate-i/students_login";

        $email_data['title'] = $header;
        $email_data['details'] = $message;
        $email_data['sign_in'] = 'Sign In';
        $email_message = $this->load->view('admin_logins/email_template2',$email_data,true);
    // print_r($this->load->view('admin_logins/email_template2',$email_data,true));
    // die();

        if($this->Email_process->send_email($receivers,$header,$email_message)){
           return 1;
        }else{
         return 0;
        }

    }

    public function my_file_upload($filename, $type)
    {
         $ext = explode('.',$_FILES[$filename]['name']);
         $new_file_name = time().'_'. $type . '.' .end($ext);

        //  echo $new_file_name;
        // print_r($filename);
        //  die;
        $this->load->library('upload');

        $config = array(
              'upload_path' => "media/applicant_edu/",
              'allowed_types' => "jpg|png|jpeg|JPEG|JPG|PNG|pdf|csv|doc|docx",
              'max_size' => "99999999999",
              'max_height' => "3000",
              'max_width' => "3000",
              'file_name' => $new_file_name
          );

        $this->upload->initialize($config);

        if (!$this->upload->do_upload($filename)) {
            return '0';
        } else {
            return $new_file_name;
        }
    }
    public function registration()
    {
        if ($_POST) {
//            echo '<pre>';
//            print_r($_POST);
//            die;
            $data = array();
            if ($this->Admin_login->check_ifexist_email_applicant($this->input->post('email', true))) {
                $sdata['exception'] = "This email already exist.";
                $this->session->set_userdata($sdata);
                redirect("students_profile/registration/");
            }
            if ($this->Admin_login->check_ifexist_phone_applicant($this->input->post('mobile', true))) {
                $sdata['exception'] = "This mobile number already exist.";
                $this->session->set_userdata($sdata);
                redirect("students_profile/registration/");
            }
            $passport_file_name='0';
            if (isset($_FILES['passport_file']) && $_FILES['passport_file']['name'] != '') {
                $passport_file_name = $this->my_file_upload('passport_file', 'passport');
                if ($passport_file_name=='0') {
                    $sdata['exception'] = "Passport doesn't upload." . $this->upload->display_errors().' and max height: 1500px ,max width: 1500px';
                    $this->session->set_userdata($sdata);
                    redirect("students_profile/registration/");
                }

            }
            $cv_file_name='0';
            if (isset($_FILES['cv_file']) && $_FILES['cv_file']['name'] != '') {
                $cv_file_name = $this->my_file_upload('cv_file', 'cv');
                if ($cv_file_name=='0') {
                    $sdata['exception'] = "CV doesn't upload." . $this->upload->display_errors().' and max height: 1500px ,max width: 1500px';
                    $this->session->set_userdata($sdata);
                    redirect("students_profile/registration/");
                }

            }

            if ($this->input->post('password', true)=='') {
                $sdata['exception'] = "Password is required field.";
                $this->session->set_userdata($sdata);
                redirect("students_profile/registration/");
            }
            $data['passport_file_location'] = $passport_file_name;
            $data['cv_file_location'] = $cv_file_name;
            $data['first_name'] = $this->input->post('first_name', true);
            $data['last_name'] = $this->input->post('last_name', true);
            $data['date_of_birth'] = $this->input->post('date_of_birth', true);
            $data['nationality'] = $this->input->post('nationality', true);
            $data['mobile'] = $this->input->post('mobile', true);
            $data['email'] = $this->input->post('email', true);
            $data['skype_id'] = $this->input->post('skype_id', true);
            $data['passport_no'] = $this->input->post('passport_no', true);
            $data['issue_date'] = $this->input->post('issue_date', true);
            $data['expair_date'] = $this->input->post('expair_date', true);
            $data['is_valid_passport'] = $this->input->post('is_valid_passport', true);
            $data['gender_id'] = $this->input->post('gender_id', true);
            $data['marital_status_id'] = $this->input->post('marital_status_id', true);
            $data['cor_address'] = $this->input->post('cor_address', true);
            $data['cor_city'] = $this->input->post('cor_city', true);
            $data['zip_code'] = $this->input->post('zip_code', true);
            $data['country_id'] = $this->input->post('country_id', true);
            $data['lead_source_id'] = $this->input->post('lead_source_id', true);

            $data['origin_office'] = 1;
            $data['intake_month_id'] = $this->input->post('intake_month_id', true);
            $data['intake_year_id'] = $this->input->post('intake_year_id', true);
            $data['applicant_code'] = $this->generateApplicantsId($data['origin_office']);
            $data['registration_date'] = date('Y-m-d');
            if ($this->input->post('password', true) != '') {
              $data['password'] = md5($this->input->post('password', true));
            }
            $admssion_officer=$this->db->query("SELECT u.id FROM tbl_user AS u
                            INNER JOIN `tbl_employee` AS e ON u.`employee_id`=e.`id`
                            LEFT JOIN `tbl_branch` AS b ON e.`branch_id`=b.`id`
                            WHERE e.`branch_id`=1 AND u.`is_head_of_admission`=1")->result_array();

          $data['admission_officer_id'] = $admssion_officer[0]['id'];
          $data['country_code'] = $this->input->post('country_code', true);

          $mail=$this->send_email_for_applicant($data['admission_officer_id'],($data['first_name'].' '.$data['last_name']),($this->input->post('password', true)),$data['email']);
         $this->db->insert('applicants', $data);
          $status_mess=" and successfully email send.";
          if(!$mail)
          {
              $status_mess=" and email cannot send. ";
          }
          $sdata['message'] = "You are successfully Registraion".$status_mess;
          $this->session->set_userdata($sdata);
            redirect("students_profile/registration");
        } else {


            $data = array();
            $data['title'] = 'Student Registraion';
            $data['heading_msg'] = "Student Registraion";
            //$data['is_show_button'] = "index";
            $data['marital_status'] = $this->db->query("SELECT * FROM cc_marital_status")->result_array();
            $data['genders'] = $this->db->query("SELECT * FROM cc_genders")->result_array();
            $data['countries'] = $this->db->query("SELECT * FROM cc_countries")->result_array();
            $data['origin_office'] = $this->db->query("SELECT * FROM tbl_branch")->result_array();
            $data['lead_source'] = $this->db->query("SELECT * FROM cc_lead_sources where id='3'")->result_array();
           //print_r($data['lead_source']);
           //die();

            $data['country_cities'] = $this->db->query("SELECT * FROM country_cities")->result_array();
            $data['course_level'] = $this->db->query("SELECT * FROM cc_course_level")->result_array();
            $data['intake_years'] =$this->Admin_login->get_intake_years();
            $data['intake_months'] =$this->Admin_login->get_intake_months();
            $data['country_code'] =$this->Admin_login->get_country_code();
          //  $data['lead_info'] = $this->db->query("SELECT * FROM leads WHERE `id`  = '$lead_id'")->result_array();
            //$data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
            $this->load->view('students_profile/registration', $data);
            //$this->load->view('admin_logins/index', $data);
        }
    }

}
