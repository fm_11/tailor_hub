<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Application_Status_Details extends CI_Controller
{
   public $notification = array();
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login','Applications_Status_Details'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $employee_id =  $user_info[0]->employee_id;
        //echo $employee_id; die;
        $this->notification = $this->Admin_login->get_notification($employee_id);
        $this->db->query('SET SESSION sql_mode = ""');
    }

    public function index()
    {
      $cond = array();
      if($_POST){
        $name = $this->input->post('name', true);
        $sdata['search_name'] = $name;
        $this->session->set_userdata($sdata);
        $cond['name'] = $name;
      }else{
        $name = $this->session->userdata('search_name');
        $cond['name'] = $name;
      }
      $data = array();
      $data['title'] = 'Application Status Information';
      $data['heading_msg'] = "Application Status Information";

      $this->load->library('pagination');
      $config['base_url'] = site_url('application_status_details/index/');
      $config['per_page'] = 10;
      $config['total_rows'] = count($this->Applications_Status_Details->get_all_application_status_details(0, 0, $cond));
      $this->pagination->initialize($config);
      $data['application_status'] = $this->Applications_Status_Details->get_all_application_status_details(10, (int)$this->uri->segment(3), $cond);
      $data['counter'] = (int)$this->uri->segment(3);
      $data['is_show_button'] = "add";
      $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
      $data['maincontent'] = $this->load->view('application_status_details/index', $data, true);
      $this->load->view('admin_logins/index', $data);
    }


    function add()
    {
      if($_POST){
        $data = array();
        $data['name'] = $this->input->post('name', true);
        $count =$this->Applications_Status_Details->checkifexist_application_status_details_by_name($this->input->post('name', true));
        if($count)
        {
          $sdata['exception'] = "'".$this->input->post('name', true)."' name already exist !";
          $this->session->set_userdata($sdata);
          redirect("application_status_details/add");
        }
        if($this->Applications_Status_Details->add_application_status_details_info($data)){
            $sdata['message'] = "Data Successfully Saved!";
            $this->session->set_userdata($sdata);
            redirect("application_status_details/index");
        }else{
            $sdata['exception'] = "Sorry Application Status Data Doesn't Added !";
            $this->session->set_userdata($sdata);
            redirect("application_status_details/add");
        }
        redirect("application_status_details/index");
      }else{
        $data = array();
        $data['title'] = 'Add Application Status Information';
        $data['heading_msg'] = "Add Application Status Information";
        $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('application_status_details/add', $data, true);
        $this->load->view('admin_logins/index', $data);
      }
  }

  function edit($id=null)
  {
    if($_POST){
      $data = array();
      $data['id'] = $this->input->post('id', true);
      $data['name'] = $this->input->post('name', true);
      $count =$this->Applications_Status_Details->checkifexist_update_application_status_details_by_name($this->input->post('name', true),$this->input->post('id', true));
      if($count)
      {
        $sdata['exception'] = "'".$this->input->post('name', true)."' name already exist !";
        $this->session->set_userdata($sdata);
        redirect("application_status_details/index");
      }
      if($this->Applications_Status_Details->update_application_status_details_info($data)){
          $sdata['message'] = "Data Successfully Updated!";
          $this->session->set_userdata($sdata);
          redirect("application_status_details/index");
      }else{
          $sdata['exception'] = "Sorry Application Status Information Doesn't Updated !";
          $this->session->set_userdata($sdata);
          redirect("application_status_details/index");
      }
    }else{
      $data = array();
      $data['title'] = 'Update Application Status Information';
      $data['heading_msg'] = "Update Application Status Information";
      $data['is_show_button'] = "index";
      $data['application_status'] = $this->Applications_Status_Details->get_application_status_details_by_id($id);
      $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
      $data['maincontent'] = $this->load->view('application_status_details/edit', $data, true);
      $this->load->view('admin_logins/index', $data);
    }
}

  function delete($id)
  {
    if($this->Applications_Status_Details->delete_application_status_details_info_by_id($id)){
        $sdata['message'] = "Data Successfully Deleted!";
        $this->session->set_userdata($sdata);
        redirect("application_status_details/index");
    }else{
        $sdata['exception'] = "Sorry Application Status Information Doesn't Deleted !";
        $this->session->set_userdata($sdata);
        redirect("application_status_details/index");
    }
  }

}
