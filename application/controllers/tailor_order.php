<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tailor_order extends CI_Controller
{
  public $notification = array();
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login','Tailor'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->db->query('SET SESSION sql_mode = ""');
        //set timezone
        date_default_timezone_set($user_info[0]->time_zone);

        $employee_id =  $user_info[0]->employee_id;
        //echo $employee_id; die;
        $this->notification = $this->Admin_login->get_notification($employee_id);
    }

    public function index()
    {
      //  echo CI_VERSION; die;
        $cond = array();
        if($_POST){
          $name = $this->input->post('customer_name', true);
          $order_status = $this->input->post('order_status', true);
          $sdata['customer_name'] = $name;
          $sdata['order_status']=$order_status;

          $this->session->set_userdata($sdata);
          $cond['customer_name'] = $name;
          $cond['order_status'] = $order_status;
        }else{
          $name = $this->session->userdata('customer_name');
          $cond['customer_name'] = $name;
          $cond['order_status'] = $this->session->userdata('order_status');;
        }
        $data = array();
        $data['title'] = 'All Order';
        $data['heading_msg'] = "All Order";

        $this->load->library('pagination');
        $config['base_url'] = site_url('tailor_order/index/');
        $config['per_page'] = 10;
        $config['total_rows'] = count($this->Tailor->get_all_tailor_order_list(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['orders'] = $this->Tailor->get_all_tailor_order_list(10, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('tailor_order/index', $data, true);
        $this->load->view('admin_logins/index', $data);
    }
    function view($id=null)
    {

          $data = array();
          $data['title'] = 'Order Details';
          $data['heading_msg'] = "Order Details";
          $data['is_show_button'] = "index";
          $data['order_details'] = $this->Tailor->get_tailor_order_details_by_orderid($id);
          $data['report'] = $this->load->view('tailor_order/order_details_print', $data, true);
          // print_r($data['order_details'][0]['customer_name']);
          // die;
          $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
          $data['maincontent'] = $this->load->view('tailor_order/view', $data, true);
          $this->load->view('admin_logins/index', $data);

    }
    public function index_pending()
    {
      //  echo CI_VERSION; die;
        $cond = array();
        if($_POST){
          $name = $this->input->post('customer_name', true);
        //  $order_status = $this->input->post('order_status', true);
          $sdata['customer_name'] = $name;
          //$sdata['order_status']=$order_status;

          $this->session->set_userdata($sdata);
          $cond['customer_name'] = $name;
        //  $cond['order_status'] = $order_status;
        }else{
          $name = $this->session->userdata('customer_name');
          $cond['customer_name'] = $name;
        //  $cond['order_status'] = $this->session->userdata('order_status');;
        }
        $cond['order_status'] = 'P';
        $data = array();
        $data['title'] = 'All Pending Order';
        $data['heading_msg'] = "All Pending Order";

        $this->load->library('pagination');
        $config['base_url'] = site_url('tailor_order/index_pending/');
        $config['per_page'] = 10;
        $config['total_rows'] = count($this->Tailor->get_all_tailor_order_list(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['orders'] = $this->Tailor->get_all_tailor_order_list(10, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('tailor_order/index_pending', $data, true);
        $this->load->view('admin_logins/index', $data);
    }
    public function index_reject()
    {
      //  echo CI_VERSION; die;
        $cond = array();
        if($_POST){
          $name = $this->input->post('customer_name', true);
        //  $order_status = $this->input->post('order_status', true);
          $sdata['customer_name'] = $name;
          //$sdata['order_status']=$order_status;

          $this->session->set_userdata($sdata);
          $cond['customer_name'] = $name;
        //  $cond['order_status'] = $order_status;
        }else{
          $name = $this->session->userdata('customer_name');
          $cond['customer_name'] = $name;
        //  $cond['order_status'] = $this->session->userdata('order_status');;
        }
        $cond['order_status'] = 'R';
        $data = array();
        $data['title'] = 'All Rejected Order';
        $data['heading_msg'] = "All Rejected Order";

        $this->load->library('pagination');
        $config['base_url'] = site_url('tailor_order/index_reject/');
        $config['per_page'] = 10;
        $config['total_rows'] = count($this->Tailor->get_all_tailor_order_list(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['orders'] = $this->Tailor->get_all_tailor_order_list(10, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('tailor_order/index_reject', $data, true);
        $this->load->view('admin_logins/index', $data);
    }

    public function index_delivered()
    {
      //  echo CI_VERSION; die;
        $cond = array();
        if($_POST){
          $name = $this->input->post('customer_name', true);
        //  $order_status = $this->input->post('order_status', true);
          $sdata['customer_name'] = $name;
          //$sdata['order_status']=$order_status;

          $this->session->set_userdata($sdata);
          $cond['customer_name'] = $name;
        //  $cond['order_status'] = $order_status;
        }else{
          $name = $this->session->userdata('customer_name');
          $cond['customer_name'] = $name;
        //  $cond['order_status'] = $this->session->userdata('order_status');;
        }
        $cond['order_status'] = 'D';
        $data = array();
        $data['title'] = 'All Delivered Order';
        $data['heading_msg'] = "All Delivered Order";

        $this->load->library('pagination');
        $config['base_url'] = site_url('tailor_order/index_delivered/');
        $config['per_page'] = 10;
        $config['total_rows'] = count($this->Tailor->get_all_tailor_order_list(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['orders'] = $this->Tailor->get_all_tailor_order_list(10, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('tailor_order/index_delivered', $data, true);
        $this->load->view('admin_logins/index', $data);
    }
    public function index_on_process()
    {
      //  echo CI_VERSION; die;
        $cond = array();
        if($_POST){
          $name = $this->input->post('customer_name', true);
        //  $order_status = $this->input->post('order_status', true);
          $sdata['customer_name'] = $name;
          //$sdata['order_status']=$order_status;

          $this->session->set_userdata($sdata);
          $cond['customer_name'] = $name;
        //  $cond['order_status'] = $order_status;
        }else{
          $name = $this->session->userdata('customer_name');
          $cond['customer_name'] = $name;
        //  $cond['order_status'] = $this->session->userdata('order_status');;
        }
        $cond['order_status'] = 'O';
        $data = array();
        $data['title'] = 'All On Process Order';
        $data['heading_msg'] = "All On Process Order";

        $this->load->library('pagination');
        $config['base_url'] = site_url('tailor_order/index_on_process/');
        $config['per_page'] = 10;
        $config['total_rows'] = count($this->Tailor->get_all_tailor_order_list(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['orders'] = $this->Tailor->get_all_tailor_order_list(10, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('tailor_order/index_on_process', $data, true);
        $this->load->view('admin_logins/index', $data);
    }

        function updateMsgOrderStatus()
        {
            $status = $this->input->get('status', true);
            $id = $this->input->get('id', true);
            $data = array();
            $data['id'] = $this->input->get('id', true);

            $data['status'] = $status;

            $this->Tailor->update_tailor_order_info($data);

        }

  }

?>
