<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cities extends CI_Controller
{
    public $notification = array();

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $employee_id = $user_info[0]->employee_id;
        //echo $employee_id; die;
        $this->notification = $this->Admin_login->get_notification($employee_id);
        $this->db->query('SET SESSION sql_mode = ""');
    }

    public function index()
    {
      $cond = array();
      if($_POST){
        $sdata['name'] = $this->input->post('name', true);
        $cond['name'] = $this->input->post('name', true);
        $sdata['country_id'] = $this->input->post('country_id', true);
        $cond['country_id'] = $this->input->post('country_id', true);
        $this->session->set_userdata($sdata);

      }else{
        $cond['name'] = $this->session->userdata('name');
        $cond['country_id'] = $this->session->userdata('country_id');
      }
        $data = array();
        $data['title'] = 'City';
        $data['heading_msg'] = "City Information";

        $this->load->library('pagination');
        $config['base_url'] = site_url('cities/index/');
        $config['per_page'] = 10;
        $config['total_rows'] = count($this->Admin_login->get_cities_info(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['cities'] = $this->Admin_login->get_cities_info(10, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['is_show_button'] = "add";
        $data['countries'] = $this->db->query("SELECT * FROM cc_countries")->result_array();
        $data['branches'] = $this->db->query("SELECT * FROM tbl_branch")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('cities/index', $data, true);
        $this->load->view('admin_logins/index', $data);
    }

    function add()
    {
        if($_POST){
            $data = array();
            $data['country_id'] = $this->input->post('country_id', true);
            $data['name'] = $this->input->post('name', true);


            $this->db->insert('country_cities', $data);
            $sdata['message'] = "You are Successfully Added City!";
            $this->session->set_userdata($sdata);
            redirect("cities/index");
        }else{
            $data = array();
            $data['title'] = 'City';
            $data['heading_msg'] = "Cities Information";
            $data['is_show_button'] = "index";
            $data['countries'] = $this->db->query("SELECT * FROM cc_countries")->result_array();

            $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
            $data['maincontent'] = $this->load->view('cities/add', $data, true);
            $this->load->view('admin_logins/index', $data);
        }
    }

    function edit($id=null)
    {
        if($_POST){
            $data = array();
            $data['id'] = $this->input->post('id', true);
            $data['country_id'] = $this->input->post('country_id', true);
            $data['name'] = $this->input->post('name', true);


            $this->db->where('id', $data['id']);
            $this->db->update('country_cities', $data);
            $sdata['message'] = "You are Successfully Updated City !";
            $this->session->set_userdata($sdata);
            redirect("cities/index");
        }else{
            $data = array();
            $data['title'] = 'Update City';
            $data['heading_msg'] = "Update City";
            $data['is_show_button'] = "index";
            $data['countries'] = $this->db->query("SELECT * FROM cc_countries")->result_array();
            $data['city'] = $this->Admin_login->get_cities_info_by_unit_id($id);
            $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
            $data['maincontent'] = $this->load->view('cities/edit', $data, true);
            $this->load->view('admin_logins/index', $data);
        }
    }

    function delete($id)
    {
        $this->db->delete('country_cities', array('id' => $id));
        $sdata['message'] = "City Deleted Successfully !";
        $this->session->set_userdata($sdata);
        redirect("cities/index");
    }

}
