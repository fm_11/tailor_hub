<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Org_infos extends CI_Controller
{
public $notification = array();
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $employee_id =  $user_info[0]->employee_id;
        //echo $employee_id; die;
        $this->notification = $this->Admin_login->get_notification($employee_id);
    }

    public function index()
    {
      $data = array();
      $data['title'] = 'Organization Information';
      $data['heading_msg'] = "Organization Information";
      $data['contact_info'] = $this->Admin_login->get_contact_info();
      //echo '<pre>';
      //print_r($data['contact_info']); die;
      $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
      $data['maincontent'] = $this->load->view('admin_logins/contact_info', $data, true);
      $this->load->view('admin_logins/index', $data);
    }


    function update_contact_info()
    {
        $file_name = $_FILES['txtPhoto']['name'];
        if($file_name != ''){
            $this->load->library('upload');
            $config['upload_path'] = 'media/images/';
            $config['allowed_types'] = 'gif|jpg|png|JPEG|JPG|jpeg';
            $config['max_size'] = '5000';
            $config['max_width'] = '5000';
            $config['max_height'] = '5000';
            $this->upload->initialize($config);
            $extension = strtolower(substr(strrchr($_FILES['txtPhoto']['name'], '.'), 1));
            foreach ($_FILES as $field => $file) {
                if ($file['error'] == 0) {
                    $new_photo_name = "Contact_" . time() . "_" . date('Y-m-d') . "." . $extension;
                    if (move_uploaded_file($_FILES["txtPhoto"]["tmp_name"], "media/images/" . $new_photo_name)) {
                        $data = array();
                        $data['id'] = $this->input->post('id', true);
                        $data['org_name'] = $this->input->post('txtOrgName', true);
                        $data['address'] = $this->input->post('txtAddress', true);
                        $data['email'] = $this->input->post('txtEmail', true);
                        $data['mobile'] = $this->input->post('txtMobile', true);
                        $data['facebook_address'] = $this->input->post('txtFacebookAddress', true);
                        $data['web_address'] = $this->input->post('txtWebAddress', true);
                        $data['picture'] = $new_photo_name;
                        $this->Admin_login->update_contact_info($data);
                        $sdata['message'] = "You are Successfully Updated Organization Info ! ";
                        $this->session->set_userdata($sdata);
                        redirect("org_infos/index");
                    } else {
                        $sdata['exception'] = "Sorry Organization Info. Doesn't Updated !" . $this->upload->display_errors();
                        $this->session->set_userdata($sdata);
                        redirect("org_infos/index");
                    }
                } else {
                    $sdata['exception'] = "Sorry Organization Info. Doesn't Updated  !" . $this->upload->display_errors();
                    $this->session->set_userdata($sdata);
                    redirect("org_infos/index");
                }
            }
        }else{
            $data = array();
            $data['id'] = $this->input->post('id', true);
            $data['org_name'] = $this->input->post('txtOrgName', true);
            $data['address'] = $this->input->post('txtAddress', true);
            $data['email'] = $this->input->post('txtEmail', true);
            $data['mobile'] = $this->input->post('txtMobile', true);
            $data['facebook_address'] = $this->input->post('txtFacebookAddress', true);
            $data['web_address'] = $this->input->post('txtWebAddress', true);
            $this->Admin_login->update_contact_info($data);
            $sdata['message'] = "You are Successfully Updated Organization Info ! ";
            $this->session->set_userdata($sdata);
            redirect("org_infos/index");
        }
    }

}
