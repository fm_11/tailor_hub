<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Manual_taxes extends CI_Controller
{
public $notification = array();
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login','Tax','Branch'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        //set timezone
        date_default_timezone_set($user_info[0]->time_zone);
        $employee_id =  $user_info[0]->employee_id;
        //echo $employee_id; die;
        $this->notification = $this->Admin_login->get_notification($employee_id);
    }

    public function index()
    {
      $data = array();

      if($_POST){
        //echo '<pre>';
        //print_r($_POST);
        //die;
        $data['finalcial_year_id'] = $this->input->post("finalcial_year_id");
        $data['branch_id'] = $this->input->post("branch_id");

        $pdata = array();
        $pdata['finalcial_year_id'] = $data['finalcial_year_id'];
        $pdata['branch_id'] = $data['branch_id'];
        $data['processData'] = $this->Tax->get_employee_manual_tax_details($pdata);
        //echo '<pre>';
        //print_r($data['rData']);
        //die;
        if(empty($data['processData'])){
          $sdata['exception'] = "Data not found !";
          $this->session->set_userdata($sdata);
          redirect("manual_taxes/index");
        }

        $data['process_table'] = $this->load->view('manual_taxes/process_table', $data, true);

      }

      $data['title'] = 'Employee Manual Tax';
      $data['heading_msg'] = "Employee Manual Tax";
      $data['finalcial_years'] = $this->Tax->get_finalcial_year();
      $data['branches'] = $this->Admin_login->get_all_branch_list();
      $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
      $data['maincontent'] = $this->load->view('manual_taxes/index', $data, true);
      $this->load->view('admin_logins/index', $data);
    }


    public function data_save()
    {
      $finalcial_year_id = $this->input->post("finalcial_year_id");
      $total_row = $this->input->post("total_row");
      $i = 0;
      $data = array();
      while ($i < $total_row) {
        $s_data = array();
        $s_data['finalcial_year_id'] = $finalcial_year_id;
        $s_data['amount'] = $this->input->post("amount_" . $i);
        $s_data['employee_id'] = $this->input->post("employee_id_" . $i);
        $data[$i] = $s_data;
        $i++;
      }

      $this->db->insert_batch('tbl_manual_taxes', $data);
      $sdata['message'] = "You are Successfully Tax Added !";
      $this->session->set_userdata($sdata);
      redirect("manual_taxes/index");
      // echo '<pre>';
      // print_r($data);
      // die;
    }



}
