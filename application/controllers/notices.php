<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Notices extends CI_Controller
{
  public $notification = array();
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login','Employee','Email_process'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }

        //set timezone
        date_default_timezone_set($user_info[0]->time_zone);

        $employee_id =  $user_info[0]->employee_id;
        //echo $employee_id; die;
        $this->notification = $this->Admin_login->get_notification($employee_id);
    }

    public function index()
    {
      //  echo CI_VERSION; die;
        $data = array();
        $data['title'] = 'Notice List';
        $data['heading_msg'] = "Notice List";
        $cond = array();
        $this->load->library('pagination');
        $config['base_url'] = site_url('notices/index/');
        $config['per_page'] = 10;
        $config['total_rows'] = count($this->Admin_login->get_all_notices(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['notices'] = $this->Admin_login->get_all_notices(10, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['is_show_button'] = "add";
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('notices/index', $data, true);
        $this->load->view('admin_logins/index', $data);
    }

    function add()
    {
        if ($_POST) {
          //email
          $header = "A new notice is coming";
          $email_title = $this->input->post('title', true);
          $email_description = $this->input->post('description', true) ;

          $email_employee_info = $this->db->query("SELECT email FROM `tbl_employee` WHERE status = '1'")->result_array();
          $receivers = implode(', ', array_column($email_employee_info, 'email'));
          $file_location = "";
          //echo $receivers; die;

            $file = $_FILES["txtFile"]['name'];
            $session_user = $this->session->userdata('user_info');
            $employee_id = $session_user[0]->employee_id;
            if ($file != '') {
              $this->load->library('upload');
              $config['upload_path'] = 'media/notice/';
              $config['allowed_types'] = 'pdf|csv|doc|docx|jpg|png|JPEG|jpeg';
              $config['max_size'] = '6000';
              $config['max_width'] = '4000';
              $config['max_height'] = '4000';
              $this->upload->initialize($config);
              $extension = strtolower(substr(strrchr($_FILES['txtFile']['name'], '.'), 1));

              foreach ($_FILES as $field => $file) {
                  if ($file['error'] == 0) {
                      $new_file_name = "notice_" . time() . "_" . date('Y-m-d') . "." . $extension;
                      if (move_uploaded_file($_FILES["txtFile"]["tmp_name"], "media/notice/" . $new_file_name)) {
                             $file_location = $new_file_name;
                      }
                      else {
                          $sdata['exception'] = "Sorry Notice Doesn't Added !" . $this->upload->display_errors();
                          $this->session->set_userdata($sdata);
                          redirect("notices/add");
                      }
                  } else {
                      $sdata['exception'] = "Sorry Notice Does't Add !";
                      $this->session->set_userdata($sdata);
                      redirect("notices/add");
                  }
              }
            }

              $data = array();
              $data['employee_id'] = $employee_id;
              $data['title'] = $this->input->post('title', true);
              $data['description'] = $this->input->post('description', true);
              $data['date_time'] = date('Y-m-d H:i:s');
              $data['file_location'] = $file_location;



              //email
              $mail_send = false;
              $email_data = array();
              $email_data['receiver_name'] = "All";
              $employee_info = $this->Employee->getEmployeeName($employee_id);
              $email_data['sender_name'] = $employee_info[0]['name'] . '(' . $employee_info[0]['code'] . ')';
              $email_data['sender_designation_name'] = $employee_info[0]['designation_name'];
              $email_data['sender_branch_name'] = $employee_info[0]['branch_name'];
              $email_data['title'] = $email_title;
              $email_data['details'] = $email_description;
              if($file_location != ''){
                $email_data['file_link'] = base_url() . "media/notice/". $file_location;
              }
              $email_message = $this->load->view('admin_logins/email_template',$email_data,true);
              //  echo $email_message; die;
              if($receivers != ''){
                if($this->Email_process->send_email($receivers,$header,$email_message)){
                  $mail_send = true;
                }
              }


              $this->db->insert('tbl_notice', $data);
              $sdata['message'] = "You are Successfully Notice Added ! ";
              $this->session->set_userdata($sdata);
              redirect("notices/index");
        } else {
            $data = array();
            $data['title'] = 'Add New Notice';
            $data['heading_msg'] = "Add New Notice";
            $data['is_show_button'] = "index";
            $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
            $data['maincontent'] = $this->load->view('notices/add', $data, true);
            $this->load->view('admin_logins/index', $data);
        }
    }







    function edit($id = null)
    {
        if ($_POST) {
              $file = $_FILES["txtFile"]['name'];
              $session_user = $this->session->userdata('user_info');
              $employee_id = $session_user[0]->employee_id;
              if ($file != '') {
                $this->load->library('upload');
                $config['upload_path'] = 'media/notice/';
                $config['allowed_types'] = 'pdf|csv|doc|docx|jpg|png|JPEG|jpeg';
                $config['max_size'] = '6000';
                $config['max_width'] = '4000';
                $config['max_height'] = '4000';
                $this->upload->initialize($config);
                $extension = strtolower(substr(strrchr($_FILES['txtFile']['name'], '.'), 1));

                foreach ($_FILES as $field => $file) {
                    if ($file['error'] == 0) {
                        $new_photo_name = "notice_" . time() . "_" . date('Y-m-d') . "." . $extension;
                        if (move_uploaded_file($_FILES["txtFile"]["tmp_name"], "media/notice/" . $new_photo_name)) {
                            $data = array();
                            $data['id'] = $this->input->post('id', true);
                            $data['employee_id'] = $employee_id;
                            $data['title'] = $this->input->post('title', true);
                            $data['description'] = $this->input->post('description', true);
                            $data['date_time'] = date('Y-m-d H:i:s');

                            $data['file_location'] = $new_photo_name;

                            $this->db->where('id', $data['id']);
                            $this->db->update('tbl_notice', $data);
                            $sdata['message'] = "You are Successfully Notice Updated !";
                            $this->session->set_userdata($sdata);
                            redirect("notices/add");
                        } else {
                            $sdata['exception'] = "Sorry Notice Doesn't Added !" . $this->upload->display_errors();
                            $this->session->set_userdata($sdata);
                            redirect("notices/add");
                        }
                    } else {
                        $sdata['exception'] = "Sorry Notice Does't Add !";
                        $this->session->set_userdata($sdata);
                        redirect("notices/add");
                    }
                }
              }else{
                $data = array();
                $data['id'] = $this->input->post('id', true);
                $data['employee_id'] = $employee_id;
                $data['title'] = $this->input->post('title', true);
                $data['description'] = $this->input->post('description', true);
                $data['date_time'] = date('Y-m-d H:i:s');
                $this->db->where('id', $data['id']);
                $this->db->update('tbl_notice', $data);
                $sdata['message'] = "You are Successfully Notice Updated !";
                $this->session->set_userdata($sdata);
                redirect("notices/add");
              }
        }  else {
            $data = array();
            $data['title'] = 'Update Notice Information';
            $data['heading_msg'] = "Update Notice Information";
            $data['is_show_button'] = "index";
            $data['notice_info'] = $this->db->query("SELECT * FROM tbl_notice WHERE id ='$id'")->result_array();
            $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
            $data['maincontent'] = $this->load->view('notices/edit', $data, true);
            $this->load->view('admin_logins/index', $data);
        }
    }

    function delete($id)
    {
        $notice_info = $this->db->query("SELECT * FROM tbl_notice WHERE id ='$id'")->result_array();
        $file = $notice_info[0]['file_location'];
        if (!empty($file)) {
            $filedel = PUBPATH . 'media/notice/' . $file;
            if (unlink($filedel)) {
                $this->db->delete('tbl_notice', array('id' => $id));
            } else {
                $this->db->delete('tbl_notice', array('id' => $id));
            }
        } else {
            $this->db->delete('tbl_notice', array('id' => $id));
        }

        $sdata['message'] = "Notice Deleted Successfully !";
        $this->session->set_userdata($sdata);
        redirect("notices/index");
    }


}
