<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Students_Login extends CI_Controller {

    function __construct() {

        parent::__construct();
        $data = array();
        $this->load->helper(array('form', 'url'));
        $this->load->model(array('Admin_login','common/insert_model', 'common/edit_model', 'common/custom_methods_model'));
        $this->load->library(array('session'));

        $user_info = $this->session->userdata('user_info');
        if(!empty($user_info)){
          $employee_id =  $user_info[0]->employee_id;
          //echo $employee_id; die;
          $this->notification = $this->Admin_login->get_notification($employee_id);
        }
$this->db->query('SET SESSION sql_mode = ""');
    }

    public function index() {
        $data = array();
        $this->load->view('students_login/index', $data);
    }

    public function authentication() {

        $user_name = $this->input->post("user_name");
        $password = $this->input->post("password");
		//echo $user_name.'/'.$password; die;
        $password = md5($password);
        $is_authenticated = $this->custom_methods_model->num_of_data("applicants", "WHERE email='$user_name' AND PASSWORD='$password'");
        // echo $is_authenticated;
        // die();
        if ($is_authenticated > 0) {
            $user_info = $this->custom_methods_model->select_row_by_condition("applicants", "email='$user_name' AND PASSWORD='$password'");

          //  $employee_id = $user_info[0]->employee_id;
          //  $employee_info = $this->custom_methods_model->select_row_by_condition("tbl_employee", "id='$employee_id'");
            if($user_info[0]->photo_path == ''){
               $user_info[0]->photo_location = 'profile-pic.png';
            }else{
               $user_info[0]->photo_location = $user_info[0]->photo_path;
            }
            $user_info[0]->name =  $user_info[0]->first_name.' '.$user_info[0]->last_name;
            $user_info[0]->employee_id =  $user_info[0]->id;
            $user_info[0]->id =  $user_info[0]->id;


           //set time_zone
           // $branch_id =   $employee_info[0]->branch_id;
           // $branch_info = $this->db->query("SELECT t.value as time_zone FROM tbl_branch as b
           //                INNER JOIN tbl_timezone as t ON t.id = b.time_zone_id
           //                WHERE b.id = '$branch_id'")->result_array();
           // $user_info[0]->time_zone =  $branch_info[0]['time_zone'];

            $this->session->set_userdata('user_info', $user_info);

            //for module select
            $arraydata = array(
               'module_id'  => 10,
               'main_menu_file'     => "students_main_menu",
               'module_short_name'     => "students"
             );
            $this->session->set_userdata($arraydata);
            // print_r($user_info);
            // die;
            redirect("students_dashboard/index");
        } else {
            $sdata['exception'] = "User Name or Password did not match!";
            $this->session->set_userdata($sdata);
            redirect("students_login/index");
        }
    }

	function getSchoolData($postdata, $api)
    {
        $opts = array('http' =>
            array(
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => $postdata
            )
        );
        $context = stream_context_create($opts);
        $result = file_get_contents("$api", false, $context);
        return json_decode($result);

    }

    public function logout() {
        $this->session->unset_userdata('user_info');
        $sdata['message'] = "You are logged out";
        $this->session->set_userdata($sdata);
        redirect("students_login/index");
    }

    public function denied() {
        $data['title'] = 'Access denied';
        $data['heading_msg'] = 'Access denied';
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('students_login/denied', '', true);
        $this->load->view('admin_logins/index', $data);
    }


	public function change_password($user_id = null){
		if($_POST){
		   $user_id = $this->input->post("user_id");
		   $password = $this->input->post("current_pass");
           $password = md5($password);
		   $is_authenticated = $this->custom_methods_model->num_of_data("applicants", "WHERE id='$user_id' AND PASSWORD='$password'");
		   if ($is_authenticated > 0) {
               $data = array();
			   $data['PASSWORD'] =   md5($this->input->post("new_pass"));
			   $data['id'] = $user_id;
			   $this->db->where('id', $data['id']);
               $this->db->update('applicants', $data);
			   $sdata['message'] = "You are Successfully Password Changes.";
               $this->session->set_userdata($sdata);
               redirect("students_login/change_password/".$user_id);
			}else {
				$sdata['exception'] = "Sorry Current Password Doesn't Match !";
				$this->session->set_userdata($sdata);
				redirect("students_login/change_password/".$user_id);
			}
		}else{
			      $data['title'] = 'Change Password';
            $data['heading_msg'] = "Change Password";
            $data['user_id'] = $user_id;
            $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
            $data['maincontent'] = $this->load->view('students_login/change_password', $data, true);
            $this->load->view('admin_logins/index', $data);
		}
	}

}
