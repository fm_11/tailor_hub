<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Other_allow_deduc_types extends CI_Controller
{
public $notification = array();
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login','Salary_type'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $employee_id =  $user_info[0]->employee_id;
        //echo $employee_id; die;
        $this->notification = $this->Admin_login->get_notification($employee_id);
    }

    public function index()
    {
      $data = array();
      $data['title'] = 'Other Allowance/Deducation';
      $data['heading_msg'] = "Other Allowance/Deducation";
      $data['is_show_button'] = "add";
      $data['other_allow_deduc_types'] = $this->Salary_type->get_all_other_allow_deduc_type();
      $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
      $data['maincontent'] = $this->load->view('other_allow_deduc_types/index', $data, true);
      $this->load->view('admin_logins/index', $data);
    }


    function add()
    {
      if($_POST){
        $data = array();
        $data['name'] = $this->input->post('name', true);
        $data['type'] = $this->input->post('type', true);
        $data['rounding_type'] = $this->input->post('rounding_type', true);
        $data['report_order'] = $this->input->post('report_order', true);
        $this->db->insert('tbl_other_allow_deduc_types', $data);
        $sdata['message'] = "You are Successfully Added Data";
        $this->session->set_userdata($sdata);
        redirect("other_allow_deduc_types/index");
      }else{
        $data = array();
        $data['title'] = 'Other Allowance/Deducation';
        $data['heading_msg'] = "Add Other Allowance/Deducation";
        $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('other_allow_deduc_types/add', $data, true);
        $this->load->view('admin_logins/index', $data);
      }
  }

  function edit($id=null)
  {
    if($_POST){
      $data = array();
      $data['id'] = $this->input->post('id', true);
      $data['name'] = $this->input->post('name', true);
      $data['type'] = $this->input->post('type', true);
      $data['rounding_type'] = $this->input->post('rounding_type', true);
      $data['report_order'] = $this->input->post('report_order', true);
      $this->db->where('id', $data['id']);
      $this->db->update('tbl_other_allow_deduc_types', $data);
      $sdata['message'] = "You are Successfully Updated Data";
      $this->session->set_userdata($sdata);
      redirect("other_allow_deduc_types/index");
    }else{
      $data = array();
      $data['title'] = 'Other Allowance/Deducation';
      $data['heading_msg'] = "Update Other Allowance/Deducation";
      $data['is_show_button'] = "index";
      $data['salary_type_info'] = $this->Salary_type->get_other_allow_deduc_type_by_id($id);
      $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
      $data['maincontent'] = $this->load->view('other_allow_deduc_types/edit', $data, true);
      $this->load->view('admin_logins/index', $data);
    }
}

  function delete($id)
  {
      $this->db->delete('tbl_other_allow_deduc_types', array('id' => $id));
      $sdata['message'] = "Data Deleted Successfully !";
      $this->session->set_userdata($sdata);
      redirect("other_allow_deduc_types/index");
  }

}
