<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Employees extends CI_Controller
{
  public $notification = array();
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login','Employee','Timekeeping','Email_process','Config_general'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
$this->db->query('SET SESSION sql_mode = ""');
        //set timezone
        date_default_timezone_set($user_info[0]->time_zone);

        $employee_id =  $user_info[0]->employee_id;
        //echo $employee_id; die;
        $this->notification = $this->Admin_login->get_notification($employee_id);
    }

    public function index()
    {
      //  echo CI_VERSION; die;
        $cond = array();
        if($_POST){
          $name = $this->input->post('name', true);
          $sdata['search_employee_name'] = $name;
          $this->session->set_userdata($sdata);
          $cond['name'] = $name;
        }else{
          $name = $this->session->userdata('search_employee_name');
          $cond['name'] = $name;
        }
        $data = array();
        $data['title'] = 'Employee Information';
        $data['heading_msg'] = "Employee Information";

        $this->load->library('pagination');
        $config['base_url'] = site_url('employees/index/');
        $config['per_page'] = 10;
        $config['total_rows'] = count($this->Employee->get_all_employees(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['employees'] = $this->Employee->get_all_employees(10, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['is_show_button'] = "add";
        $data['branches'] = $this->db->query("SELECT * FROM tbl_branch")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('employee_infos/employee_info_list', $data, true);
        $this->load->view('admin_logins/index', $data);
    }

    public function dashboard_employee_list(){
        $data = array();
        $data['title'] = 'Employee Information';
        $data['heading_msg'] = "Employee Information";
        $cond = array();
        $cond['status'] = 1;
        $process_employees = array();
        $i = 0;
        $employees = $this->Employee->get_all_employees(0, 0, $cond);

        foreach ($employees as $row):
            $process_employees[$i]['id'] = $row['id'];
            $process_employees[$i]['name'] = $row['name'];
            $process_employees[$i]['branch_name'] = $row['branch_name'];
            $process_employees[$i]['code'] = $row['code'];
            $process_employees[$i]['post_name'] = $row['post_name'];
            $process_employees[$i]['mobile'] = $row['mobile'];
            $process_employees[$i]['email'] = $row['email'];


            $time_zone =  $row['time_zone'];
            //set timezone for employee
            date_default_timezone_set($time_zone);
            $process_employees[$i]['current_time'] = date('H:i:s');

            $todays_status = "";

            $branch_id =  $row['branch_id'];
            $date = date('Y-m-d');

            //check holiday
            $is_holiday = $this->Timekeeping->check_date_branch_holiday($date, $branch_id);
            if(!empty($is_holiday)){
              $todays_status .= $is_holiday->short_name;
            }



            //login data check
            $is_present = $this->Timekeeping->get_login_info_by_employee_id($date, $row['id']);
            if(!empty($is_present)){
              $todays_status .= 'Present ('. date('h:i:s A', strtotime($is_present[0]['login_time'])).')';
            }else{
              //chekc leave
              $is_leave = $this->Timekeeping->get_employee_leave_info_by_date_employee_id($date, $row['id']);
              if(!empty($is_leave)){
                $todays_status .= 'On Leave ('.$is_leave[0]['leave_type_name'].')';
              }
            }

            if(empty($is_present) && empty($is_leave) && empty($is_holiday)){
              $todays_status .= "Absent";
            }

            $process_employees[$i]['todays_status'] = $todays_status;

            $i++;
        endforeach;

          //set timezone for default
        $user_info = $this->session->userdata('user_info');
        date_default_timezone_set($user_info[0]->time_zone);

        $data['employees'] =  $process_employees;
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('employee_infos/dashboard_employee_list', $data, true);
        $this->load->view('admin_logins/index', $data);
    }

    function dashboard_email_send(){
      if($_POST){
        $receivers = $this->input->post('recipientList', true);
        $message = $this->input->post('message', true);
        $header = $this->input->post('header', true);

        $session_user = $this->session->userdata('user_info');
        $employee_id = $session_user[0]->employee_id;

        $email_data = array();
        $email_data['receiver_name'] = "All";
        $employee_info = $this->Employee->getEmployeeName($employee_id);
        $email_data['sender_name'] = $employee_info[0]['name'] . '(' . $employee_info[0]['code'] . ')';
        $email_data['sender_designation_name'] = $employee_info[0]['designation_name'];
        $email_data['sender_branch_name'] = $employee_info[0]['branch_name'];
        $email_data['title'] = $header;
        $email_data['details'] = $message;

        $email_message = $this->load->view('admin_logins/email_template2',$email_data,true);

        if($this->Email_process->send_email($receivers,$header,$email_message)){
          $sdata['message'] = "You are Successfully Message Send.";
          $this->session->set_userdata($sdata);
          redirect("employees/dashboard_employee_list");
        }else{
          $sdata['exception'] = "Sorry Message Doesn't Send !";
          $this->session->set_userdata($sdata);
          redirect("employees/dashboard_employee_list");
        }
      }
    }

    function add()
    {
        $is_employee_id_auto = $this->Config_general->read_by_purpose_and_db_field_name('general','employee_id_will_be_auto')[0]['default_value'];
        if ($_POST) {
        //  echo '<pre>';
        //  print_r($_POST);
          //die;
            $this->load->library('upload');
            $config['upload_path'] = 'media/employee/';
            $config['allowed_types'] = 'jpg|png|JPEG|jpeg';
            $config['max_size'] = '6000';
            $config['max_width'] = '4000';
            $config['max_height'] = '4000';
            $this->upload->initialize($config);
            $extension = strtolower(substr(strrchr($_FILES['txtPhoto']['name'], '.'), 1));

            foreach ($_FILES as $field => $file) {
                if ($file['error'] == 0) {
                    $new_photo_name = "Employee_" . time() . "_" . date('Y-m-d') . "." . $extension;
                    if (move_uploaded_file($_FILES["txtPhoto"]["tmp_name"], "media/employee/" . $new_photo_name)) {
                        $data = array();
                        $data['name'] = $this->input->post('txtName', true);
                        $data['post'] = $this->input->post('txtPost', true);
                        $data['section'] = $this->input->post('txtSection', true);
                        $data['national_id'] = $this->input->post('txtNID', true);
                        $data['date_of_birth'] = $this->input->post('txtDOB', true);
                        $data['joining_date'] = $this->input->post('txtDateOfJoining', true);
                        $data['confirmation_date'] = $this->input->post('txtDateOfConfirmation', true);
                        $data['mobile'] = $this->input->post('txtMobile', true);
                        $data['branch_id'] = $this->input->post('txtBranch', true);
                        $data['email'] = $this->input->post('txtEmail', true);
                        $data['gender'] = $this->input->post('txtGender', true);
                        $data['address'] = $this->input->post('txtAddress', true);
                        $data['process_code'] = $this->input->post('txtProcessCode', true);
                        if($this->input->post('txtReportingBoss', true) != ''){
            							$data['reporting_boss_id'] = $this->input->post('txtReportingBoss', true);
            						}
                        $data['is_permanent'] = $this->input->post('txtIsPermanent', true);
                        $data['leave_auto_approve'] = $this->input->post('leave_auto_approve', true);
                        $data['shift_id'] = $this->input->post('shift_id', true);
                        $data['photo_location'] = $new_photo_name;
                        $data['status'] = 1;

                        if($is_employee_id_auto == '1'){
                          $data['code'] = $this->generateEmployeeId($data['branch_id'],$data['joining_date']);
                        }else{
                          $data['code'] = $this->input->post('txtEmployeeId', true);
                        }



                        if($this->Employee->add_employee_info($data)){
                            $sdata['message'] = "You are Successfully Employee Information Added !";
                            $this->session->set_userdata($sdata);
                            redirect("employees/add");
                        }else{
                            $sdata['exception'] = "Sorry Employee Doesn't Added !";
                            $this->session->set_userdata($sdata);
                            redirect("employees/add");
                        }
                    } else {
                        $sdata['exception'] = "Sorry Employee Doesn't Added !" . $this->upload->display_errors();
                        $this->session->set_userdata($sdata);
                        redirect("employees/add");
                    }
                } else {
                    $sdata['exception'] = "Sorry Photo Does't Upload !";
                    $this->session->set_userdata($sdata);
                    redirect("employees/add");
                }
            }
        } else {
            $data = array();
            $data['title'] = 'Add New Employee Information';
            $data['heading_msg'] = "Add New Employee Information";
            $data['is_show_button'] = "index";
            $data['branches'] = $this->db->query("SELECT * FROM tbl_branch")->result_array();
            $data['employees'] = $this->db->query("SELECT id,name,code FROM tbl_employee")->result_array();
            $data['sections'] = $this->db->query("SELECT * FROM tbl_employee_section")->result_array();
            $data['post'] = $this->db->query("SELECT * FROM tbl_employee_post")->result_array();
            $data['shifts'] = $this->Admin_login->get_all_shift();
            $data['is_employee_id_auto'] = $is_employee_id_auto;
            //echo $data['is_employee_id_auto']; die;
            $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
            $data['maincontent'] = $this->load->view('employee_infos/employee_info_add_form', $data, true);
            $this->load->view('admin_logins/index', $data);
        }
    }


    function generateEmployeeId($branch_id, $date_of_join){
          $branches = $this->db->query("SELECT * FROM tbl_branch WHERE id = '$branch_id'")->result_array();
          $code = "";
          $code .= $branches[0]['code'];
          $parts = explode('-', $date_of_join);
          $code .= $parts[1]. substr($parts[0], -2);
          $total_employee = count($this->db->query("SELECT id FROM tbl_employee")->result_array());
          return $code.str_pad(($total_employee + 1), 3, '0', STR_PAD_LEFT);
    }

    function edit($id = null)
    {
        if ($_POST) {
            $file = $_FILES["txtPhoto"]['name'];
            if ($file != '') {
                $employee_photo_info = $this->Employee->get_employee_photo_info_by_employee_id($this->input->post('id', true));
                $old_file = $employee_photo_info[0]['photo_location'];
                if (!empty($old_file)) {
                    $filedel = PUBPATH . 'media/employee/' . $old_file;
                    unlink($filedel);
                }
                $this->load->library('upload');
                $config['upload_path'] = 'media/employee/';
                $config['allowed_types'] = 'jpg|png|JPEG|jpeg';
                $config['max_size'] = '6000';
                $config['max_width'] = '4000';
                $config['max_height'] = '4000';
                $this->upload->initialize($config);
                $extension = strtolower(substr(strrchr($_FILES['txtPhoto']['name'], '.'), 1));
                foreach ($_FILES as $field => $file) {
                    if ($file['error'] == 0) {
                        $new_photo_name = "Employee_" . time() . "_" . date('Y-m-d') . "." . $extension;
                        if (move_uploaded_file($_FILES["txtPhoto"]["tmp_name"], "media/employee/" . $new_photo_name)) {
                            $data = array();
                            $data['id'] = $this->input->post('id', true);
                            $data['name'] = $this->input->post('txtName', true);
                            $data['post'] = $this->input->post('txtPost', true);
                            $data['section'] = $this->input->post('txtSection', true);
                            $data['national_id'] = $this->input->post('txtNID', true);
                            $data['date_of_birth'] = $this->input->post('txtDOB', true);
                            $data['joining_date'] = $this->input->post('txtDateOfJoining', true);
                            $data['confirmation_date'] = $this->input->post('txtDateOfConfirmation', true);
                            $data['mobile'] = $this->input->post('txtMobile', true);
                            $data['branch_id'] = $this->input->post('txtBranch', true);
                            $data['email'] = $this->input->post('txtEmail', true);
                            $data['gender'] = $this->input->post('txtGender', true);
                            $data['address'] = $this->input->post('txtAddress', true);
                            $data['process_code'] = $this->input->post('txtProcessCode', true);

                            if($this->input->post('txtReportingBoss', true) != ''){
                							$data['reporting_boss_id'] = $this->input->post('txtReportingBoss', true);
                						}
                            $data['is_permanent'] = $this->input->post('txtIsPermanent', true);
                            $data['leave_auto_approve'] = $this->input->post('leave_auto_approve', true);
                            $data['shift_id'] = $this->input->post('shift_id', true);
                            $data['photo_location'] = $new_photo_name;
                            $this->Employee->update_employee_status($data);
                            $sdata['message'] = "You are Successfully Employee Information Updated ! ";
                            $this->session->set_userdata($sdata);
                            redirect("employees/index");
                        } else {
                            $sdata['exception'] = "Sorry Employee Doesn't Updated !" . $this->upload->display_errors();
                            $this->session->set_userdata($sdata);
                            redirect("employees/index");
                        }
                    } else {
                        $sdata['exception'] = "Sorry Photo Does't Upload !";
                        $this->session->set_userdata($sdata);
                        redirect("employees/index");
                    }
                }
            } else {
                $data = array();
                $data['id'] = $this->input->post('id', true);
                $data['name'] = $this->input->post('txtName', true);
                $data['post'] = $this->input->post('txtPost', true);
                $data['section'] = $this->input->post('txtSection', true);
                $data['national_id'] = $this->input->post('txtNID', true);
                $data['date_of_birth'] = $this->input->post('txtDOB', true);
                $data['joining_date'] = $this->input->post('txtDateOfJoining', true);
                $data['confirmation_date'] = $this->input->post('txtDateOfConfirmation', true);
                $data['mobile'] = $this->input->post('txtMobile', true);
                $data['branch_id'] = $this->input->post('txtBranch', true);
                $data['email'] = $this->input->post('txtEmail', true);
                $data['gender'] = $this->input->post('txtGender', true);
                $data['address'] = $this->input->post('txtAddress', true);
                $data['process_code'] = $this->input->post('txtProcessCode', true);
                if($this->input->post('txtReportingBoss', true) != ''){
                  $data['reporting_boss_id'] = $this->input->post('txtReportingBoss', true);
                }
                $data['is_permanent'] = $this->input->post('txtIsPermanent', true);
                $data['leave_auto_approve'] = $this->input->post('leave_auto_approve', true);
                $data['shift_id'] = $this->input->post('shift_id', true);
                $this->Employee->update_employee_status($data);
                $sdata['message'] = "You are Successfully Employee Information Updated ! ";
                $this->session->set_userdata($sdata);
                redirect("employees/index");
            }
        } else {
            $data = array();
            $data['title'] = 'Update Employee Information';
            $data['heading_msg'] = "Update Employee Information";
            $data['is_show_button'] = "index";
            $data['employees'] = $this->db->query("SELECT id,name,code FROM tbl_employee")->result_array();
            $data['employee_info'] = $this->Employee->get_employee_photo_info_by_employee_id($id);
            $data['branches'] = $this->db->query("SELECT * FROM tbl_branch")->result_array();
            $data['sections'] = $this->db->query("SELECT * FROM tbl_employee_section")->result_array();
            $data['post'] = $this->db->query("SELECT * FROM tbl_employee_post")->result_array();
            $data['shifts'] = $this->Admin_login->get_all_shift();
            $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
            $data['maincontent'] = $this->load->view('employee_infos/employee_info_edit_form', $data, true);
            $this->load->view('admin_logins/index', $data);
        }
    }

    function delete($id)
    {
        $employee_photo_info = $this->Employee->get_employee_photo_info_by_employee_id($id);
        $file = $employee_photo_info[0]['photo_location'];
        if (!empty($file)) {
            $filedel = PUBPATH . 'media/employee/' . $file;
            if (unlink($filedel)) {
                $this->Employee->delete_employee_info_by_employee_id($id);
            } else {
                $this->Employee->delete_employee_info_by_employee_id($id);
            }
        } else {
            $this->Employee->delete_employee_info_by_employee_id($id);
        }

        $sdata['message'] = "Employee Information Deleted Successfully !";
        $this->session->set_userdata($sdata);
        redirect("employees/index");
    }

    function updateMsgStatusEmployeeStatus()
    {
        $status = $this->input->get('status', true);
        $id = $this->input->get('id', true);
        $data = array();
        $data['id'] = $this->input->get('id', true);
        if ($status == 1) {
            $data['status'] = 0;
        } else {
            $data['status'] = 1;
        }
        $this->Employee->update_employee_status($data);
        if ($status == 0) {
            echo '<a class="approve_icon" title="Active" href="#" onclick="msgStatusUpdate(' . $id . ',1)"><button type="button" class="btn btn-primary btn-xs mb-1">Active</button></a>';
        } else {
            echo '<a class="reject_icon" title="Inactive" href="#" onclick="msgStatusUpdate(' . $id . ',0)"><button type="button" class="btn btn-primary btn-xs mb-1">Inactive</button></a>';
        }
    }
    function myprofile()
    {
            $user_info = $this->session->userdata('user_info');
            $employee_id =  $user_info[0]->employee_id;
            $data = array();
            $data['title'] = 'MY Profile';
            $data['heading_msg'] = "MY Profile";
            //$data['is_show_button'] = "index";
            $data['employees'] = $this->db->query("SELECT id,name,code FROM tbl_employee")->result_array();
            $data['employee_info'] = $this->Employee->get_employee_photo_info_by_employee_id($employee_id);
            $data['branches'] = $this->db->query("SELECT * FROM tbl_branch")->result_array();
            $data['sections'] = $this->db->query("SELECT * FROM tbl_employee_section")->result_array();
            $data['post'] = $this->db->query("SELECT * FROM tbl_employee_post")->result_array();
            $data['shifts'] = $this->Admin_login->get_all_shift();
            $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
            $data['maincontent'] = $this->load->view('employee_infos/myprofile', $data, true);
            $this->load->view('admin_logins/index', $data);

    }


}
