<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Holidays extends CI_Controller
{
    public $notification = array();
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login','Timekeeping', 'common/insert_model', 'common/custom_methods_model',));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }

        //set timezone
        date_default_timezone_set($user_info[0]->time_zone);

        $employee_id =  $user_info[0]->employee_id;
        //echo $employee_id; die;
        $this->notification = $this->Admin_login->get_notification($employee_id);
    }

    public function index()
    {
        $data = array();
        $data['title'] = 'Holiday';
        $data['heading_msg'] = 'Holiday';
        $this->load->library('pagination');
        $config['base_url'] = site_url('holidays/index/');
        $config['per_page'] = 10;
        $cond = array();
        $config['total_rows'] = count($this->Timekeeping->get_all_holiday_list(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['holidays'] = $this->Timekeeping->get_all_holiday_list(10, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['is_show_button'] = "add";
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('holidays/index', $data, true);
        $this->load->view('admin_logins/index', $data);
    }


    function add()
    {
      if ($_POST) {
            $date = $this->input->post("txtDate");
            $branch_id = $this->input->post("txtBranch");
            $is_already_holiday = $this->db->query("SELECT id FROM tbl_holiday WHERE date = '$date' AND branch_id = '$branch_id'")->result_array();
            if (!empty($is_already_holiday)) {
                $sdata['exception'] = "Already added this date for holiday !";
                $this->session->set_userdata($sdata);
                redirect("holidays/add");
            }
            $data = array();
            $data['holiday_type'] = $this->input->post("txtHolidayType");
            $data['branch_id'] = $this->input->post("txtBranch");
            $data['date'] = $date;
            $data['remarks'] = $this->input->post("txtRemarks");
            $this->db->insert('tbl_holiday', $data);
            $sdata['message'] = "Holiday Successfully Added.";
            $this->session->set_userdata($sdata);
            redirect("holidays/add");
        } else {
            $data = array();
            $data['title'] = 'Holiday Add';
            $date = date('Y-m-d');
            $data['date'] = $date;
            $data['is_show_button'] = "index";
            $data['heading_msg'] = "Holiday Add";
            $data['holiday_types'] = $this->db->query("SELECT * FROM `tbl_holiday_type`")->result_array();
            $data['branches'] = $this->Admin_login->get_all_branch_list();
            $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
            $data['maincontent'] = $this->load->view('holidays/add', $data, true);
            $this->load->view('admin_logins/index', $data);
        }
    }

    function batch_add(){
        $data = array();
        $data['holiday_types'] = $this->db->query("SELECT * FROM `tbl_holiday_type`")->result_array();
        if($_POST){
            $data['month'] = $this->input->post("month");
            $data['year'] = $this->input->post("year");
            $data['branch_id'] = $this->input->post("branch_id");
            $month = $data['month'];
            $year = $data['year'];
            $branch_id = $data['branch_id'];
            $data['num_of_days'] = cal_days_in_month(CAL_GREGORIAN,$data['month'],$data['year']);
           // echo $data['num_of_days']; die;
            $data['holidays'] = $this->db->query("SELECT * FROM `tbl_holiday` WHERE
                            branch_id = '$branch_id' AND  MONTH(`date`) = '$month' AND YEAR(`date`) = '$year'")->result_array();
            //echo '<pre>';
            //print_r($data['holidays']);
           // die;
            $data['process_table'] = $this->load->view('holidays/batch_add_form_table', $data, true);
        }
        $data['title'] = 'Holiday';
        $date = date('Y-m-d');
        $data['is_show_button'] = "index";
        $data['heading_msg'] = "Holiday Add";
        $data['branches'] = $this->Admin_login->get_all_branch_list();
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('holidays/batch_add', $data, true);
        $this->load->view('admin_logins/index', $data);
    }

    function batch_add_data_save(){
        if($_POST){
            //echo '<pre>';
           // print_r($_POST);
           // die;
            $year = $this->input->post("year");
            $month = $this->input->post("month");
            $num_of_days = $this->input->post("num_of_days");
            $branch_id = $this->input->post("branch_id");

            $this->db->query("DELETE FROM `tbl_holiday` WHERE branch_id = '$branch_id' AND  MONTH(`date`) = '$month' AND YEAR(`date`) = '$year'");

           //echo '<pre>';
          // print_r($tt);
           //die;

            $i = 1;
            while ($i <= $num_of_days) {
                if ($this->input->post("is_allow_" . $i)) {
                     $data = array();
                     $data['holiday_type'] = $this->input->post("txtHolidayType_" . $i);
                     $data['branch_id'] = $branch_id;
                     $data['date'] = $this->input->post("date_" . $i);
                     $data['remarks'] = "Batch Holiday Add";
                     $this->db->insert('tbl_holiday', $data);
                }
                $i++;
            }
            $sdata['message'] = "Holiday Added Successfully.";
            $this->session->set_userdata($sdata);
            redirect("holidays/batch_add");
            }
    }

    function edit($id=null)
    {
      if ($_POST) {
          $data = array();
          $data['id'] = $this->input->post('id', true);
          $data['holiday_type'] = $this->input->post("txtHolidayType");
          $data['branch_id'] = $this->input->post("txtBranch");
          $data['date'] = $this->input->post("txtDate");
          $data['remarks'] = $this->input->post("txtRemarks");
          $this->db->where('id', $data['id']);
          $this->db->update('tbl_holiday', $data);
          $sdata['message'] = "You are Successfully Holiday Updated ! ";
          $this->session->set_userdata($sdata);
          redirect("holidays/index");
      } else {
          $data = array();
          $data['title'] = 'Update Holiday';
          $data['heading_msg'] = "Update Holiday";
          $data['is_show_button'] = "index";
          $data['branches'] = $this->Admin_login->get_all_branch_list();
          $data['holiday_types'] = $this->db->query("SELECT * FROM `tbl_holiday_type`")->result_array();
          $data['holiday_info'] = $this->db->query("SELECT * FROM tbl_holiday WHERE id='$id'")->result_array();
          $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
          $data['maincontent'] = $this->load->view('holidays/edit', $data, true);
          $this->load->view('admin_logins/index', $data);
      }
    }

    function delete($id)
   {
       $this->db->delete('tbl_holiday', array('id' => $id));
       $sdata['message'] = "Holiday Deleted Successfully !";
       $this->session->set_userdata($sdata);
       redirect("holidays/index");
   }

}
