<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Shifts extends CI_Controller
{
public $notification = array();
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (!empty($user_info)) {
          $employee_id =  $user_info[0]->employee_id;
          //echo $employee_id; die;
          $this->notification = $this->Admin_login->get_notification($employee_id);
        }
    }

    public function index()
    {
      $data = array();
      $data['title'] = 'Shift Information';
      $data['heading_msg'] = "Shift Information";
      $data['shifts'] = $this->db->query("SELECT * FROM tbl_shift")->result_array();
      $data['is_show_button'] = "add";
      $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
      $data['maincontent'] = $this->load->view('shifts/index', $data, true);
      $this->load->view('admin_logins/index', $data);
    }


    function add()
    {
      if($_POST){
        $data = array();
        $data['name'] = $this->input->post('name', true);
        $data['short_name'] = $this->input->post('short_name', true);
        $data['start_time'] = $this->input->post('start_time', true);
        $data['end_time'] = $this->input->post('end_time', true);
        $data['flexible_time_in'] = $this->input->post('flexible_time_in', true);
        $data['remarks'] = $this->input->post('remarks', true);
        $this->db->insert('tbl_shift', $data);
        $sdata['message'] = "You are Successfully Added Shift Info !";
        $this->session->set_userdata($sdata);
        redirect("shifts/index");
      }else{
        $data = array();
        $data['title'] = 'Shift Information';
        $data['heading_msg'] = "Add Shift Information";
        $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('shifts/add', $data, true);
        $this->load->view('admin_logins/index', $data);
      }
  }

  function edit($id=null)
  {
    if($_POST){
      $data = array();
      $data['id'] = $this->input->post('id', true);
      $data['name'] = $this->input->post('name', true);
      $data['short_name'] = $this->input->post('short_name', true);
      $data['start_time'] = $this->input->post('start_time', true);
      $data['end_time'] = $this->input->post('end_time', true);
      $data['flexible_time_in'] = $this->input->post('flexible_time_in', true);
      $data['remarks'] = $this->input->post('remarks', true);
      $this->db->where('id', $data['id']);
      $this->db->update('tbl_shift', $data);
      $sdata['message'] = "You are Successfully Updated Shift Info !";
      $this->session->set_userdata($sdata);
      redirect("shifts/index");
    }else{
      $data = array();
      $data['title'] = 'Shift Information';
      $data['heading_msg'] = "Update Shift Information";
      $data['is_show_button'] = "index";
      $data['shift_info'] = $this->db->query("SELECT * FROM tbl_shift WHERE id = '$id'")->row();
      $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
      $data['maincontent'] = $this->load->view('shifts/edit', $data, true);
      $this->load->view('admin_logins/index', $data);
    }
}

  function delete($id)
  {
      $this->db->delete('tbl_shift', array('id' => $id));
      $sdata['message'] = "Shift Information Deleted Successfully !";
      $this->session->set_userdata($sdata);
      redirect("shifts/index");
  }

}
