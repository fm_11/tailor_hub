<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Report_absentees extends CI_Controller
{
public $notification = array();
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login','Timekeeping','Branch'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        //set timezone
        date_default_timezone_set($user_info[0]->time_zone);
        $employee_id =  $user_info[0]->employee_id;
        //echo $employee_id; die;
        $this->notification = $this->Admin_login->get_notification($employee_id);
    }

    public function index()
    {
      $data = array();
      $data['title'] = 'Employee Absentee Report';
      $data['heading_msg'] = "Employee Absentee Report";
      if($_POST){
        $date = $this->input->post('date', true);
        $branch_id = $this->input->post('branch_id', true);

        $data['absentee_info'] = $this->Timekeeping->get_employee_report_absentee($date, $branch_id);
        //echo '<pre>';
        //print_r($data['rData']); die;
        $data['date'] = $date;
        $data['branch_id'] = $branch_id;

        if(empty($data['absentee_info'])){
          $sdata['exception'] = "Data not found !";
          $this->session->set_userdata($sdata);
          redirect("report_absentees/index");
        }
    

        $data['org_info'] = $this->Admin_login->getReportHeaderAddress($branch_id);

        $excel = $this->input->post('excel', true);
        //  echo $excel; die;
        if(isset($excel) && $excel != ''){
          $data['excel'] = 1;
          $this->load->view('report_absentees/report_absentees_table', $data);
          //die;
        }else{
          $data['report'] = $this->load->view('report_absentees/report_absentees_table', $data, true);
        }

      }
      if(!isset($excel) || $excel == ''){

        $data['branches'] = $this->db->query("SELECT * FROM tbl_branch")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('report_absentees/index', $data, true);
        $this->load->view('admin_logins/index', $data);
      }
    }



}
