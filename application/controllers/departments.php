<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Departments extends CI_Controller
{
public $notification = array();
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $employee_id =  $user_info[0]->employee_id;
        //echo $employee_id; die;
        $this->notification = $this->Admin_login->get_notification($employee_id);
    }

    public function index()
    {
      $data = array();
      $data['title'] = 'Department Information';
      $data['heading_msg'] = "Department Information";
      $data['departments'] = $this->db->query("SELECT * FROM tbl_employee_section")->result_array();
      $data['is_show_button'] = "add";
      $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
      $data['maincontent'] = $this->load->view('departments/index', $data, true);
      $this->load->view('admin_logins/index', $data);
    }


    function add()
    {
      if($_POST){
        $data = array();
        $data['name'] = $this->input->post('name', true);
        $this->db->insert('tbl_employee_section', $data);
        $sdata['message'] = "You are Successfully Added Department Info !";
        $this->session->set_userdata($sdata);
        redirect("departments/index");
      }else{
        $data = array();
        $data['title'] = 'Add Department Information';
        $data['heading_msg'] = "Add Department Information";
        $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('departments/add', $data, true);
        $this->load->view('admin_logins/index', $data);
      }
  }

  function edit($id=null)
  {
    if($_POST){
      $data = array();
      $data['id'] = $this->input->post('id', true);
      $data['name'] = $this->input->post('name', true);
      $this->db->where('id', $data['id']);
      $this->db->update('tbl_employee_section', $data);
      $sdata['message'] = "You are Successfully Updated Department Info !";
      $this->session->set_userdata($sdata);
      redirect("departments/index");
    }else{
      $data = array();
      $data['title'] = 'Update Department Information';
      $data['heading_msg'] = "Update Department Information";
      $data['is_show_button'] = "index";
      $data['department'] = $this->db->query("SELECT * FROM tbl_employee_section WHERE id = '$id'")->row();
      $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
      $data['maincontent'] = $this->load->view('departments/edit', $data, true);
      $this->load->view('admin_logins/index', $data);
    }
}

  function delete($id)
  {
      $this->db->delete('tbl_employee_section', array('id' => $id));
      $sdata['message'] = "Department Information Deleted Successfully !";
      $this->session->set_userdata($sdata);
      redirect("departments/index");
  }

}
