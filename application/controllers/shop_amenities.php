<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Shop_amenities extends CI_Controller
{
public $notification = array();
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $employee_id =  $user_info[0]->employee_id;
        //echo $employee_id; die;
        $this->notification = $this->Admin_login->get_notification($employee_id);
        $this->db->query('SET SESSION sql_mode = ""');
    }

    public function index()
    {
      $data = array();
      $data['title'] = 'Shop Amenities Information';
      $data['heading_msg'] = "Shop Amenities Information";
      $data['shop'] = $this->db->query("SELECT * FROM tbl_shop_amenities")->result_array();
      $data['is_show_button'] = "add";
      $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
      $data['maincontent'] = $this->load->view('shop_amenities/index', $data, true);
      $this->load->view('admin_logins/index', $data);
    }


    function add()
    {
      if($_POST){
        $data = array();
        $data['name'] = $this->input->post('name', true);
        $this->db->insert('tbl_shop_amenities', $data);
        $sdata['message'] = "You Successfully Added Shop Amenities Info !";
        $this->session->set_userdata($sdata);
        redirect("shop_amenities/index");
      }else{
        $data = array();
        $data['title'] = 'Add Shop Amenities Information';
        $data['heading_msg'] = "Add Shop Amenities Information";
        $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('shop_amenities/add', $data, true);
        $this->load->view('admin_logins/index', $data);
      }
  }

  function edit($id=null)
  {
    if($_POST){
      $data = array();
      $data['id'] = $this->input->post('id', true);
      $data['name'] = $this->input->post('name', true);
      $this->db->where('id', $data['id']);
      $this->db->update('tbl_shop_amenities', $data);
      $sdata['message'] = "You Successfully Updated Shop Amenities Info !";
      $this->session->set_userdata($sdata);
      redirect("shop_amenities/index");
    }else{
      $data = array();
      $data['title'] = 'Update Shop Amenities Information';
      $data['heading_msg'] = "Update Shop Amenities Information";
      $data['is_show_button'] = "index";
      $data['shop'] = $this->Admin_login->get_shop_info($id);
      $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
      $data['maincontent'] = $this->load->view('shop_amenities/edit', $data, true);
      $this->load->view('admin_logins/index', $data);
    }
}

  function delete($id)
  {
      $this->db->delete('tbl_shop_amenities', array('id' => $id));
      $sdata['message'] = "Shop Amenities Information Deleted Successfully !";
      $this->session->set_userdata($sdata);
      redirect("shop_amenities/index");
  }

}
