<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Areas extends CI_Controller
{
public $notification = array();
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $employee_id =  $user_info[0]->employee_id;
        //echo $employee_id; die;
        $this->notification = $this->Admin_login->get_notification($employee_id);
        $this->db->query('SET SESSION sql_mode = ""');
    }

    public function index()
    {
      $data = array();
      $data['title'] = 'Area Information';
      $data['heading_msg'] = "Area Information";
      $data['areas'] = $this->db->query("SELECT a.*,d.name as d_name FROM tbl_area as a LEFT JOIN tbl_division as d on a.division_id = d.id")->result_array();
      $data['is_show_button'] = "add";
      $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
      $data['maincontent'] = $this->load->view('areas/index', $data, true);
      $this->load->view('admin_logins/index', $data);
    }


    function add()
    {
      if($_POST){

        $data = array();

        if($this->Admin_login->check_ifexist_area($this->input->post('name', true),$this->input->post('division_id', true)))
        {
          $sdata['exception'] = "This area name '".$this->input->post('name', true)."' already exists.";
          $this->session->set_userdata($sdata);
          redirect("areas/add");
        }
        $this->load->library('upload');
        $config['upload_path'] = 'media/website/images/area/';
        $config['allowed_types'] = 'jpg|png|JPEG|jpeg';
        $config['max_size'] = '6000';
        $config['max_width'] = '4000';
        $config['max_height'] = '4000';
        $this->upload->initialize($config);
        $extension = strtolower(substr(strrchr($_FILES['txtPhoto']['name'], '.'), 1));
        $new_photo_name = "area_" . time() . "_" . date('Y-m-d') . "." . $extension;
        if (move_uploaded_file($_FILES["txtPhoto"]["tmp_name"], "media/website/images/area/" . $new_photo_name))
        {

        } else {
            $sdata['exception'] = "Sorry Photo Does't Upload !";
            $this->session->set_userdata($sdata);
            redirect("areas/add");
        }

        $data['name'] = $this->input->post('name', true);
        $data['division_id'] = $this->input->post('division_id', true);
        $data['image_path'] = $new_photo_name;

        $this->db->insert('tbl_area', $data);
        $sdata['message'] = "You Successfully Added Area Info !";
        $this->session->set_userdata($sdata);
        redirect("areas/index");
      }else{
        $data = array();
        $data['title'] = 'Add Area Information';
        $data['heading_msg'] = "Add Area Information";
        $data['is_show_button'] = "index";
        $data['divisions'] = $this->db->query("SELECT * FROM tbl_division")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('areas/add', $data, true);
        $this->load->view('admin_logins/index', $data);
      }
  }

  function edit($id=null)
  {
    if($_POST){
      $data = array();

      if($this->Admin_login->check_ifexist_update_area($this->input->post('name', true),$this->input->post('division_id', true),$this->input->post('id', true)))
      {
        $sdata['exception'] = "This area name '".$this->input->post('name', true)."' already exists.";
        $this->session->set_userdata($sdata);
        redirect("areas/add");
      }
       $new_photo_name=$this->input->post('old_image_path', true);
        $file = $_FILES["txtPhoto"]['name'];
        if ($file != '') {
            $old_file = $new_photo_name;
            if (!empty($old_file)) {
                $filedel = PUBPATH . 'media/website/images/' . $old_file;
                unlink($filedel);
            }
            $this->load->library('upload');
            $config['upload_path'] = 'media/tailor/area/';
            $config['allowed_types'] = 'jpg|png|JPEG|jpeg';
            $config['max_size'] = '6000';
            $config['max_width'] = '4000';
            $config['max_height'] = '4000';
            $this->upload->initialize($config);
            $extension = strtolower(substr(strrchr($_FILES['txtPhoto']['name'], '.'), 1));
            $new_photo_name = "area_" . time() . "_" . date('Y-m-d') . "." . $extension;
            if (move_uploaded_file($_FILES["txtPhoto"]["tmp_name"], "media/website/images/area/" . $new_photo_name))
            {

            } else {
                $sdata['exception'] = "Sorry Photo Does't Upload !";
                $this->session->set_userdata($sdata);
                redirect("areas/edit/".$this->input->post('id', true));
            }
          }

      $data['id'] = $this->input->post('id', true);
      $data['name'] = $this->input->post('name', true);
      $data['division_id'] = $this->input->post('division_id', true);
      $data['image_path'] = $new_photo_name;
      $this->db->where('id', $data['id']);
      $this->db->update('tbl_area', $data);
      $sdata['message'] = "You Successfully Updated Area Info !";
      $this->session->set_userdata($sdata);
      redirect("areas/index");
    }else{
      $data = array();
      $data['title'] = 'Update Area Information';
      $data['heading_msg'] = "Update Area Information";
      $data['is_show_button'] = "index";
      $data['divisions'] = $this->db->query("SELECT * FROM tbl_division")->result_array();
      $data['area_info'] = $this->Admin_login->get_area_info($id);
      $data['area_infos'] = $this->Admin_login->get_area_infos($id);
      $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
      $data['maincontent'] = $this->load->view('areas/edit', $data, true);
      $this->load->view('admin_logins/index', $data);
    }
}

  function delete($id)
  {
      $this->db->delete('tbl_area', array('id' => $id));
      $sdata['message'] = "Area Information Deleted Successfully !";
      $this->session->set_userdata($sdata);
      redirect("areas/index");
  }


}
