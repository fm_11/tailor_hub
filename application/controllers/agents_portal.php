<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Agents_portal extends CI_Controller
{
    public $notification = array();

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login'));
        $this->load->model(array('Agent_portal'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $employee_id = $user_info[0]->employee_id;
        //echo $employee_id; die;
        $this->notification = $this->Admin_login->get_notification($employee_id);
$this->db->query('SET SESSION sql_mode = ""');
    }

    public function index()
    {

        $data = array();
        $data['title'] = 'Agent_portal';
        $data['heading_msg'] = "Company Information";
        $cond = array();
        $this->load->library('pagination');
        $config['base_url'] = site_url('agents_portal/index/');
        $config['per_page'] = 10;
        $config['total_rows'] = count($this->Agent_portal->get_agents_portal_info(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['agents_portal'] = $this->Agent_portal->get_agents_portal_info(10, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['is_show_button'] = "add";
        $data['branches'] = $this->db->query("SELECT * FROM tbl_branch")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('agents_portal/index', $data, true);
        $this->load->view('admin_logins/index', $data);
    }

    function add(){
        if($_POST){
            //Logo upload
            if($_FILES["logo_image_path"]['name'] != ''){
                $logo_name = 'ins_logo_'.time().'-'.$_FILES["logo_image_path"]['name'];

                $config = array(
                    'upload_path' => "media/agent_portal/",
                    'allowed_types' => "gif|jpg|png|jpeg|JPEG|JPG|PNG|GIF",
                    'overwrite' => TRUE,
                    'max_size' => "99999999999",
                    'max_height' => "800",
                    'max_width' => "1500",
                    'file_name' => $logo_name
                );
                $this->load->library('upload', $config);

                if(!$this->upload->do_upload('logo_image_path'))
                {
                    $sdata['exception'] = "Logo doesn't upload." . $this->upload->display_errors();
                    $this->session->set_userdata($sdata);
                    redirect("agents_portal/index");
                }
            }
            $data = array();

            if($_FILES["logo_image_path"]['name'] != ''){
                $data['logo_image_path'] = $logo_name;
            }
            $data['company_name'] = $this->input->post('company_name',true);
            $data['street_address'] = $this->input->post('street_address',true);
            $data['city_id'] = $this->input->post('city_id',true);
            $data['country_id'] = $this->input->post('country_id',true);
            $data['postal_code'] = $this->input->post('postal_code',true);
            $data['phone'] = $this->input->post('phone',true);
            $data['mobile'] = $this->input->post('mobile',true);
            $data['website'] = $this->input->post('website',true);
            $data['skype'] = $this->input->post('skype',true);
            $data['comission'] = $this->input->post('comission',true);
            $data['principal_name'] = $this->input->post('principal_name',true);
            $data['principal_position'] = $this->input->post('principal_position',true);
            $data['principal_phone'] = $this->input->post('principal_phone',true);
            $data['email'] = $this->input->post('email',true);
            $data['contact_person_id'] = $this->input->post('contact_person_id',true);
            $data['allocate_branch_id'] = $this->input->post('allocate_branch_id',true);
            $data['engagement_officer_id'] = $this->input->post('engagement_officer_id',true);
            $data['account_name'] = $this->input->post('account_name',true);
            $data['account_number'] = $this->input->post('account_number',true);
            $data['bank_name'] = $this->input->post('bank_name',true);
            $data['branch_name'] = $this->input->post('branch_name',true);
            $data['status'] = $this->input->post('status',true);
            $data['date'] = $this->input->post('date',true);



            $this->db->insert('company_details', $data);
            $sdata['message'] = "You are Successfully Added Company Details !";
            $this->session->set_userdata($sdata);
            redirect("agents_portal/index");
        }else{
            $data = array();
            $data['title'] = 'Add Company Details';
            $data['heading_msg'] = "Add Company Details";
            $data['is_show_button'] = "index";
            $data['employee'] = $this->db->query("SELECT * FROM tbl_employee")->result_array();
            $data['branch'] = $this->db->query("SELECT * FROM tbl_branch")->result_array();
            $data['countries'] = $this->db->query("SELECT * FROM cc_countries")->result_array();
            $data['country_cities'] = $this->db->query("SELECT * FROM country_cities")->result_array();

            $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
            $data['maincontent'] = $this->load->view('agents_portal/add', $data, true);
            $this->load->view('admin_logins/index', $data);
        }
    }

    function getCityByCountryId()
    {
        $city_id = $this->input->get('city_id', true);
        $data = array();
        $data['city_info'] = $this->db->query("SELECT * FROM country_cities WHERE `country_id`  = '$city_id'")->result_array();
        $this->load->view('agents_portal/city_list', $data);
    }

    function edit($id=null)
    {
        if($_POST){

            if($_FILES["logo_image_path"]['name'] != ''){
                //echo '<pre>';
                //print_r($_FILES["institution_logo_path"]);
                //die;
                $logo_name = 'ins_logo_'.time().'-'.$_FILES["logo_image_path"]['name'];

                $config = array(
                    'upload_path' => "media/agent_portal/",
                    'allowed_types' => "gif|jpg|png|jpeg|JPEG|JPG|PNG|GIF",
                    'overwrite' => TRUE,
                    'max_size' => "99999999999",
                    'max_height' => "800",
                    'max_width' => "1500",
                    'file_name' => $logo_name
                );
                $this->load->library('upload', $config);

                if(!$this->upload->do_upload('logo_image_path'))
                {
                    $sdata['exception'] = "Logo doesn't upload." . $this->upload->display_errors();
                    $this->session->set_userdata($sdata);
                    redirect("agents_portal/index");
                }
            }



            //End Logo Upload

            $data = array();

            $data['id'] = $this->input->post('id',true);
            if($_FILES["logo_image_path"]['name'] != ''){
                $data['logo_image_path'] = $logo_name;
            }
            $data['company_name'] = $this->input->post('company_name',true);
            $data['street_address'] = $this->input->post('street_address',true);
            $data['city_id'] = $this->input->post('city_id',true);
            $data['country_id'] = $this->input->post('country_id',true);
            $data['postal_code'] = $this->input->post('postal_code',true);
            $data['phone'] = $this->input->post('phone',true);
            $data['mobile'] = $this->input->post('mobile',true);
            $data['website'] = $this->input->post('website',true);
            $data['skype'] = $this->input->post('skype',true);
            $data['comission'] = $this->input->post('comission',true);
            $data['principal_name'] = $this->input->post('principal_name',true);
            $data['principal_position'] = $this->input->post('principal_position',true);
            $data['principal_phone'] = $this->input->post('principal_phone',true);
            $data['email'] = $this->input->post('email',true);
            $data['contact_person_id'] = $this->input->post('contact_person_id',true);
            $data['allocate_branch_id'] = $this->input->post('allocate_branch_id',true);
            $data['engagement_officer_id'] = $this->input->post('engagement_officer_id',true);
            $data['account_name'] = $this->input->post('account_name',true);
            $data['account_number'] = $this->input->post('account_number',true);
            $data['bank_name'] = $this->input->post('bank_name',true);
            $data['branch_name'] = $this->input->post('branch_name',true);
            $data['status'] = $this->input->post('status',true);
            $data['date'] = $this->input->post('date',true);


            $this->db->where('id', $data['id']);
            $this->db->update('company_details', $data);
            $sdata['message'] = "You are Successfully Updated Company Details Info !";
            $this->session->set_userdata($sdata);
            redirect("agents_portal/index");
        }else {

            $data = array();
            $data['title'] = 'Update Company Details';
            $data['heading_msg'] = "Update Agent_portal Information";
            $data['is_show_button'] = "index";
            $data['employee'] = $this->db->query("SELECT * FROM tbl_employee")->result_array();
            $data['branch'] = $this->db->query("SELECT * FROM tbl_branch")->result_array();
            $data['countries'] = $this->db->query("SELECT * FROM cc_countries")->result_array();
            $data['country_cities'] = $this->db->query("SELECT * FROM country_cities")->result_array();

            $data['agents_portal'] = $this->Agent_portal->get_agents_portal_by_unit_id($id);
            $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
            $data['maincontent'] = $this->load->view('agents_portal/edit', $data, true);
            $this->load->view('admin_logins/index', $data);
        }

    }

    function delete($id)
    {
        $this->db->delete('company_details', array('id' => $id));
        $sdata['message'] = "Agent Portal Deleted Successfully !";
        $this->session->set_userdata($sdata);
        redirect("agents_portal/index");
    }
}
