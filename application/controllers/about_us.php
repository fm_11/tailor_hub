<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class About_us extends CI_Controller
{
public $notification = array();
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $employee_id =  $user_info[0]->employee_id;
        $this->notification = $this->Admin_login->get_notification($employee_id);
        $this->db->query('SET SESSION sql_mode = ""');
    }

    public function index()
    {
      $data = array();
      $data['title'] = 'About Us';
      $data['heading_msg'] = "About us";
      $data['about'] = $this->db->query("SELECT * FROM tbl_about_us")->result_array();
      $data['is_show_button'] = "add";
      $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
      $data['maincontent'] = $this->load->view('about_us/index', $data, true);
      $this->load->view('admin_logins/index', $data);
    }


    function add()
    {
      if($_POST){
        $data = array();
        $data['about_us'] = $this->input->post('about_us', true);
        $this->db->insert('tbl_about_us', $data);
        $sdata['message'] = "You Successfully Added About Us Info !";
        $this->session->set_userdata($sdata);
        redirect("about_us/index");
      }else{
        $data = array();
        $data['title'] = 'Add About Us';
        $data['heading_msg'] = "Add About Us Description";
        $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('about_us/add', $data, true);
        $this->load->view('admin_logins/index', $data);
      }
  }

  function edit($id=null)
  {
    if($_POST){
      $data = array();
      $data['id'] = $this->input->post('id', true);
      $data['about_us'] = $this->input->post('about_us', true);
      $this->db->where('id', $data['id']);
      $this->db->update('tbl_about_us', $data);
      $sdata['message'] = "You Successfully Updated About Us Info !";
      $this->session->set_userdata($sdata);
      redirect("about_us/index");
    }else{
      $data = array();
      $data['title'] = 'Update About Us Description';
      $data['heading_msg'] = "Update About Us Description";
      $data['is_show_button'] = "index";
      $data['about'] = $this->Admin_login->get_about_us_info($id);
      $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
      $data['maincontent'] = $this->load->view('about_us/edit', $data, true);
      $this->load->view('admin_logins/index', $data);
    }
}

  function delete($id)
  {
      $this->db->delete('tbl_about_us', array('id' => $id));
      $sdata['message'] = "About Us Description Deleted Successfully !";
      $this->session->set_userdata($sdata);
      redirect("about_us/index");
  }

}
