<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Movement_registers extends CI_Controller
{
    public $notification = array();
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login','Timekeeping', 'common/insert_model', 'common/custom_methods_model',));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }

        //set timezone
        date_default_timezone_set($user_info[0]->time_zone);
        
        $employee_id =  $user_info[0]->employee_id;
        //echo $employee_id; die;
        $this->notification = $this->Admin_login->get_notification($employee_id);
    }

    public function index()
    {
        $session_user = $this->session->userdata('user_info');
        $user_role =  $session_user[0]->user_role;
        //echo '<pre>';
        //print_r($session_user);
        //die;
        $employee_id = $session_user[0]->employee_id;
        $data = array();
        $data['title'] = 'Employee Movement Register';
        $data['heading_msg'] = 'Employee Movement Register';
        $this->load->library('pagination');
        $config['base_url'] = site_url('movement_registers/index/');
        $config['per_page'] = 10;
        $cond = array();
        if($user_role != 1){
          $cond['employee_id'] = $employee_id;
        }else{
          $cond['employee_id'] ="";
        }
        $config['total_rows'] = count($this->Timekeeping->get_all_movement_register_list(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['movements'] = $this->Timekeeping->get_all_movement_register_list(10, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['is_show_button'] = "add";
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('movement_registers/index', $data, true);
        $this->load->view('admin_logins/index', $data);
    }


    function add()
    {
        $session_user = $this->session->userdata('user_info');
        $user_role =  $session_user[0]->user_role;
        $employee_id =  $session_user[0]->employee_id;
        if ($_POST) {
            $data = array();
            $data['employee_id'] = $this->input->post('employee_id');
            if($user_role != 1 && $employee_id != $data['employee_id']){
              $sdata['exception'] = "Employee  not allowed !";
              $this->session->set_userdata($sdata);
              redirect("movement_registers/add");
            }
            $data['date'] = $this->input->post('date');
            $data['start_time'] = $this->input->post('start_time');
            $data['end_time'] = $this->input->post('end_time');
            $data['location'] = $this->input->post('location');
            $data['purpose'] = $this->input->post('purpose');
            $data['remarks'] = $this->input->post('remarks');
            $this->db->insert('tbl_employee_movement_registers', $data);
            $sdata['message'] = "You are Successfully Movement Register Added !";
            $this->session->set_userdata($sdata);
            redirect("movement_registers/add");
        }

        if($user_role != 1){
          $data['employees'] = $this->db->query("SELECT id,name,code FROM `tbl_employee` WHERE id = '$employee_id'")->result_array();
        }else{
          $data['employees'] = $this->db->query("SELECT id,name,code FROM `tbl_employee` ORDER BY id")->result_array();
        }
        $data['title'] = 'Add Movement Register';
        $data['heading_msg'] = "Add Movement Register";
        $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('movement_registers/add', $data, true);
        $this->load->view('admin_logins/index', $data);
    }

    function edit($id=null)
    {
        if ($_POST) {
            $data = array();
            $data['id'] = $this->input->post('id');
            $data['end_time'] = $this->input->post('end_time');
            $this->db->where('id', $data['id']);
            $this->db->update('tbl_employee_movement_registers', $data);
            $sdata['message'] = "You are Successfully Movement Register Updated !";
            $this->session->set_userdata($sdata);
            redirect("movement_registers/index");
        }
        $data = array();
        $data['movement_info'] = $this->db->query("SELECT * FROM `tbl_employee_movement_registers` WHERE id = '$id'")->result_array();
        $data['title'] = 'Update Movement Register';
        $data['heading_msg'] = "Update Movement Register";
        $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('movement_registers/edit', $data, true);
        $this->load->view('admin_logins/index', $data);
    }

    function delete($id)
   {
       $this->db->delete('tbl_employee_movement_registers', array('id' => $id));
       $sdata['message'] = "You are Successfully Movement Register Data Deleted !";
       $this->session->set_userdata($sdata);
       redirect("movement_registers/index");
   }



}
