<?php

class User_role_wise_privileges extends CI_Controller
{
  public $notification = array();
    function __construct()
    {
        parent::__construct();

        $this->load->helper(array('form', 'url'));
        $this->load->model(array('Admin_login','User_role_wise_privilege'));
        $this->load->library(array('session'));
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $employee_id =  $user_info[0]->employee_id;
        //echo $employee_id; die;
        $this->notification = $this->Admin_login->get_notification($employee_id);
    }


    function index($role_id)
    {
        if($role_id == 1){
            $sdata['exception'] = "Super admin has been permited for all features.";
            $this->session->set_userdata($sdata);
            redirect("user_roles/index");
        }

        if($role_id == ''){
            $sdata['exception'] = "Please select a role.";
            $this->session->set_userdata($sdata);
            redirect("user_roles/index");
        }
        $data = array();
        $data['heading_msg'] = $data['title'] = 'Role wise privileges [' . $this->User_role_wise_privilege->get_role_name_by_id($role_id) . ' ]';
        $data['role_privilege_resources'] = $this->User_role_wise_privilege->get_privileged_resources($role_id);
        $data['user_resources_array'] = $this->User_role_wise_privilege->get_all_resources_array();
        $data['role_id'] = $role_id;
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('user_role_wise_privileges/index', $data, true);
        $this->load->view('admin_logins/index', $data);
    }

    function add()
    {
        $this->_prepare_validation();
        if ($_POST) {
            $data = $this->_get_posted_data();
            if ($this->form_validation->run() === TRUE) {
                if ($this->User_role_wise_privilege->add($data)) {
                    $sdata['message'] = "Information Added";
                    $this->session->set_userdata($sdata);
                    redirect("user_roles/index");
                }
            }
        }
    }

    function _prepare_validation()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('role_id', 'Role anme', 'trim|required|xss_clean|is_natural_no_zero');
        $this->form_validation->set_rules('data[]', 'Action', 'xss_clean');
    }

    function _get_posted_data()
    {
        $data = array();
        $user_resources = $this->User_role_wise_privilege->get_all_resources_array();
        $i = 0;
        foreach ($user_resources as $rows1) {
            foreach ($rows1 as $rows2) {
                $entity = 0;
                foreach ($rows2 as $key3 => $rows3) {
                    foreach ($rows3 as $key4 => $rows4) {
                        foreach ($rows4 as $key5 => $rows5) {
                            foreach ($rows5 as $rows6) {
                                if (isset($_POST['data'][$entity]["$key4"]["$key5"])) {
                                    $data['resources'][$i]['controller'] = $key4;
                                    $data['resources'][$i]['action'] = $rows6['name'];
                                    $data['resources'][$i]['role_id'] = $_POST['role_id'];
                                }
                                $i++;
                            }
                        }
                    }
                    $entity++;
                }
            }
        }
        $data['role_id'] = $_POST['role_id'];
        return $data;
    }
}
