<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Reference_Applicants extends CI_Controller
{
    public $notification = array();

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login','Employee','Email_process'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->db->query('SET SESSION sql_mode = ""');
        $employee_id = $user_info[0]->employee_id;
        //echo $employee_id; die;
        $this->notification = $this->Admin_login->get_notification(0);
    }

    public function index()
    {
      $cond = array();
      if($_POST){
        $sdata['name'] = $this->input->post('name', true);
        $cond['name'] = $this->input->post('name', true);
        $sdata['country_id'] = $this->input->post('country_id', true);
        $cond['country_id'] = $this->input->post('country_id', true);
        $sdata['lead_source_id'] = $this->input->post('lead_source_id', true);
        $cond['lead_source_id'] = $this->input->post('lead_source_id', true);
        $sdata['code'] = $this->input->post('code', true);
        $cond['code'] = $this->input->post('code', true);
        $this->session->set_userdata($sdata);

      }else{

        $cond['name'] = $this->session->userdata('name');
        $cond['country_id'] = $this->session->userdata('country_id');
        $cond['lead_source_id'] = $this->session->userdata('lead_source_id');
        $cond['code'] = $this->session->userdata('code');
      }

        $data = array();
        $data['title'] = 'Reference Applicants';
        $data['heading_msg'] = "Reference Applicants";

        $user_info = $this->session->userdata('user_info');
        $employee_id = $user_info[0]->employee_id;



        $cond['agent_id'] = $user_info[0]->id;

        $this->load->library('pagination');
        $config['base_url'] = site_url('reference_applicants/index/');
        $config['per_page'] = 10;
        $config['total_rows'] = count($this->Admin_login->get_reference_applicants_info(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['applicants'] = $this->Admin_login->get_reference_applicants_info(10, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);

        //$data['is_show_button'] = "add";
        $data['countries'] = $this->db->query("SELECT * FROM cc_countries")->result_array();
        $data['lead_source'] = $this->db->query("SELECT * FROM cc_lead_sources")->result_array();
        $data['branches'] = $this->db->query("SELECT * FROM leads")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('reference_applicants/index', $data, true);
        $this->load->view('admin_logins/index', $data);
    }
    public function edit($id=null)
    {
        if ($_POST) {
            $data = array();
            if ($this->Admin_login->check_ifexist_update_phone_applicant($this->input->post('mobile', true),$this->input->post('id', true))) {
                $sdata['exception'] = "This mobile number already exist.";
                $this->session->set_userdata($sdata);
                redirect("applicants/edit/".$this->input->post('id', true));
            }
            if ($this->Admin_login->check_ifexist_update_email_applicant($this->input->post('email', true),$this->input->post('id', true))) {
                $sdata['exception'] = "This email already exist.";
                $this->session->set_userdata($sdata);
                redirect("applicants/edit/".$this->input->post('id', true));
            }
            if (isset($_FILES['passport_file']) && $_FILES['passport_file']['name'] != '') {
                $passport_file_name = $this->my_file_upload('passport_file', 'passport');
                $data['passport_file_location'] = $passport_file_name;
                if ($passport_file_name=='0') {
                    $sdata['exception'] = "Passport doesn't upload." . $this->upload->display_errors();
                    $this->session->set_userdata($sdata);
                    redirect("reference_applicants/edit/".$this->input->post('id', true));
                }
                $old_passport_file = $this->input->post('passport_file_name', true);
                $msg =$this->Admin_login->my_file_remove($old_passport_file, $passport_file_name, 'media/applicant_edu/');
            }
            if (isset($_FILES['cv_file']) && $_FILES['cv_file']['name'] != '') {
                $cv_file_name = $this->my_file_upload('cv_file', 'cv');
                $data['cv_file_location'] = $cv_file_name;
                if ($cv_file_name=='0') {
                    $sdata['exception'] = "CV doesn't upload." . $this->upload->display_errors();
                    $this->session->set_userdata($sdata);
                    redirect("reference_applicants/edit/".$this->input->post('id', true));
                }
                $old_cv_file = $this->input->post('cv_file_name', true);
                $msg =$this->Admin_login->my_file_remove($old_cv_file, $cv_file_name, 'media/applicant_edu/');
            }


            $data['id'] = $this->input->post('id', true);
            $data['first_name'] = $this->input->post('first_name', true);
            $data['last_name'] = $this->input->post('last_name', true);
            $data['date_of_birth'] = $this->input->post('date_of_birth', true);
            $data['nationality'] = $this->input->post('nationality', true);
            $data['mobile'] = $this->input->post('mobile', true);
            $data['email'] = $this->input->post('email', true);
            $data['skype_id'] = $this->input->post('skype_id', true);
            $data['passport_no'] = $this->input->post('passport_no', true);
            $data['issue_date'] = $this->input->post('issue_date', true);
            $data['expair_date'] = $this->input->post('expair_date', true);
            $data['is_valid_passport'] = $this->input->post('is_valid_passport', true);
            $data['gender_id'] = $this->input->post('gender_id', true);
            $data['marital_status_id'] = $this->input->post('marital_status_id', true);
            $data['cor_address'] = $this->input->post('cor_address', true);
            $data['cor_city'] = $this->input->post('cor_city', true);
            $data['zip_code'] = $this->input->post('zip_code', true);
            $data['country_id'] = $this->input->post('country_id', true);
            $data['lead_source_id'] = $this->input->post('lead_source_id', true);
            $data['origin_office'] = $this->input->post('origin_office', true);
            $data['agent_id']=$this->input->post('agent_id', true);
            $data['intake_month_id'] = $this->input->post('intake_month_id', true);
            $data['intake_year_id'] = $this->input->post('intake_year_id', true);
            if ($this->input->post('password', true) != '') {
                if ($this->input->post('password', true) != 'password') {
                    $data['password'] = md5($this->input->post('password', true));
                }
            }
            $data['country_code'] = $this->input->post('country_code', true);
            $this->db->where('id', $data['id']);
            $this->db->update('applicants', $data);
            $sdata['message'] = "You are Successfully Updated Applicants data";
            $this->session->set_userdata($sdata);
            redirect("reference_applicants/index");
        } else {
            $data = array();
            $data['title'] = 'Update Applicants';
            $data['heading_msg'] = "Update Applicants";
            $data['is_show_button'] = "index";
            $data['marital_status'] = $this->db->query("SELECT * FROM cc_marital_status")->result_array();
            $data['genders'] = $this->db->query("SELECT * FROM cc_genders")->result_array();
            $data['tbl_employee'] = $this->db->query("SELECT u.id ,e.name FROM tbl_employee as e inner join tbl_user as u on e.id=u.employee_id")->result_array();
            $data['countries'] = $this->db->query("SELECT * FROM cc_countries")->result_array();
            $data['country_cities'] = $this->db->query("SELECT * FROM country_cities")->result_array();
            $data['course_level'] = $this->db->query("SELECT * FROM cc_course_level")->result_array();
            $data['lead_source'] = $this->db->query("SELECT * FROM cc_lead_sources where name='Agent'")->result_array();
            $data['employee_post'] = $this->db->query("SELECT * FROM tbl_employee_post")->result_array();
            $data['intake_years'] =$this->Admin_login->get_intake_years();
            $data['intake_months'] =$this->Admin_login->get_intake_months();
            $data['country_code'] =$this->Admin_login->get_country_code();
            $data['applicant_info'] = $this->Admin_login->get_applicants_info_by_unit_id($id);
              $data['applicant_education'] = $this->Admin_login->get_educaton_list_by_applicant_id($id);
            $data['applicant_language'] = $this->db->query("SELECT * FROM applicant_english_language WHERE applicant_id = '$id'")->result_array();
            $data['applicant_experience'] = $this->db->query("SELECT e.*,c.name as country_name FROM applicant_work_experience AS e
                                            LEFT JOIN cc_countries as c ON c.id = e.country_id
                                            WHERE e.applicant_id = '$id'")->result_array();

            //$data['origin_office'] = $this->db->query("SELECT * FROM tbl_branch")->result_array();
            $data['origin_office'] = $this->Admin_login->get_branch_by_agent_id($data['applicant_info'][0]['agent_id']);

            $data['applicant_reference'] = $this->db->query("SELECT a.*,c.name as country_name FROM applicant_reference as a
                                         LEFT JOIN cc_countries as c ON c.id = a.ref1_country_id
                                         WHERE applicant_id = '$id'")->result_array();
           $data['courses'] = $this->Admin_login->get_subject_list();
            //echo '<pre>';
            //print_r($data['applicant_info']);
            //die;
            $data['agent_id'] = $this->db->query("SELECT personal_name as name,id,code FROM agents")->result_array();
            $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
            $data['maincontent'] = $this->load->view('reference_applicants/edit', $data, true);
            $this->load->view('admin_logins/index', $data);
        }
    }
    public function view($id)
    {
        $data = array();
        $data['title'] = 'View Applicants';
        $data['heading_msg'] = "View Applicants";
        $data['is_show_button'] = "index";
        $data['marital_status'] = $this->db->query("SELECT * FROM cc_marital_status")->result_array();
        $data['genders'] = $this->db->query("SELECT * FROM cc_genders")->result_array();
        $data['tbl_employee'] = $this->db->query("SELECT u.id ,e.name FROM tbl_employee as e inner join tbl_user as u on e.id=u.employee_id")->result_array();
        $data['countries'] = $this->db->query("SELECT * FROM cc_countries")->result_array();
        $data['country_cities'] = $this->db->query("SELECT * FROM country_cities")->result_array();
        $data['course_level'] = $this->db->query("SELECT * FROM cc_course_level")->result_array();
        $data['lead_source'] = $this->db->query("SELECT * FROM cc_lead_sources")->result_array();
        $data['employee_post'] = $this->db->query("SELECT * FROM tbl_employee_post")->result_array();

        $data['applicant_info'] = $this->Admin_login->get_applicants_info_by_unit_id($id);
          $data['applicant_education'] = $this->Admin_login->get_educaton_list_by_applicant_id($id);
        $data['applicant_language'] = $this->db->query("SELECT * FROM applicant_english_language WHERE applicant_id = '$id'")->result_array();
        $data['applicant_experience'] = $this->db->query("SELECT e.*,c.name as country_name FROM applicant_work_experience AS e
                                    LEFT JOIN cc_countries as c ON c.id = e.country_id
                                    WHERE e.applicant_id = '$id'")->result_array();

        $data['origin_office'] = $this->db->query("SELECT * FROM tbl_branch")->result_array();
        $data['university_application'] =$this->Admin_login->get_university_by_applicant_id($id);

        $data['application_status']=$this->db->query("SELECT
                              (SELECT `name` FROM tbl_application_status WHERE id=h.old_status_id) AS old_status ,
                              (SELECT `name` FROM tbl_application_status WHERE id=h.new_status_id) AS new_status ,
                              date_time,remarks,file_location
                              FROM tbl_application_status_history AS h WHERE h.application_id='$id'
                              ORDER BY date_time")->result_array();

        $data['applicant_reference'] = $this->db->query("SELECT r.*,c.name AS country_name FROM applicant_reference AS r LEFT JOIN cc_countries AS c ON r.ref1_country_id =c.id WHERE r.applicant_id = '$id'")->result_array();
        $data['check_information']=$this->Admin_login->check_information_update_by_applicant_id($id);
        //print_r($data['applicant_info']);
        //die;
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('reference_applicants/view', $data, true);
        $this->load->view('admin_logins/index', $data);
    }

    public function student_registration()
    {
        if ($_POST) {
            $user_info = $this->session->userdata('user_info');

            $data = array();
            if ($this->Admin_login->check_ifexist_email_applicant($this->input->post('email', true))) {
                $sdata['exception'] = "This email already exist.";
                $this->session->set_userdata($sdata);
                redirect("reference_applicants/student_registration/");
            }
            if ($this->Admin_login->check_ifexist_phone_applicant($this->input->post('mobile', true))) {
                $sdata['exception'] = "This mobile number already exist.";
                $this->session->set_userdata($sdata);
                redirect("reference_applicants/student_registration/");
            }
            if ($this->input->post('password', true)=='') {
                $sdata['exception'] = "Password is required field.";
                $this->session->set_userdata($sdata);
                redirect("reference_applicants/student_registration/");
            }
            $passport_file_name='0';
            if (isset($_FILES['passport_file']) && $_FILES['passport_file']['name'] != '') {
                $passport_file_name = $this->my_file_upload('passport_file', 'passport');

                if ($passport_file_name=='0') {
                    $sdata['exception'] = "Passport doesn't upload." . $this->upload->display_errors();
                    $this->session->set_userdata($sdata);
                    redirect("reference_applicants/student_registration/");
                }
            }
            $cv_file_name='0';
            if (isset($_FILES['cv_file']) && $_FILES['cv_file']['name'] != '') {
                $cv_file_name = $this->my_file_upload('cv_file', 'cv');

                if ($cv_file_name=='0') {
                    $sdata['exception'] = "CV doesn't upload." . $this->upload->display_errors();
                    $this->session->set_userdata($sdata);
                    redirect("reference_applicants/student_registration/");
                }
            }
            $data['passport_file_location'] = $passport_file_name;
            $data['cv_file_location'] = $cv_file_name;
            $data['agent_id'] = $user_info[0]->id;
            $data['first_name'] = $this->input->post('first_name', true);
            $data['last_name'] = $this->input->post('last_name', true);
            $data['date_of_birth'] = $this->input->post('date_of_birth', true);
            $data['nationality'] = $this->input->post('nationality', true);
            $data['mobile'] = $this->input->post('mobile', true);
            $data['email'] = $this->input->post('email', true);
            $data['skype_id'] = $this->input->post('skype_id', true);
            $data['passport_no'] = $this->input->post('passport_no', true);
            $data['issue_date'] = $this->input->post('issue_date', true);
            $data['expair_date'] = $this->input->post('expair_date', true);
            $data['is_valid_passport'] = $this->input->post('is_valid_passport', true);
            $data['gender_id'] = $this->input->post('gender_id', true);
            $data['marital_status_id'] = $this->input->post('marital_status_id', true);
            $data['cor_address'] = $this->input->post('cor_address', true);
            $data['cor_city'] = $this->input->post('cor_city', true);
            $data['zip_code'] = $this->input->post('zip_code', true);
            $data['country_id'] = $this->input->post('country_id', true);
            $data['lead_source_id'] = $this->input->post('lead_source_id', true);
            $data['origin_office'] = $this->input->post('origin_office', true);
            $data['intake_month_id'] = $this->input->post('intake_month_id', true);
            $data['intake_year_id'] = $this->input->post('intake_year_id', true);
            $data['registration_date'] = date('Y-m-d');
            if ($this->input->post('password', true) != '') {
              $data['password'] = md5($this->input->post('password', true));
            }
            $data['country_code'] = $this->input->post('country_code', true);
            $data['applicant_code'] = $this->generateApplicantsId($data['origin_office']);

            $user_info = $this->session->userdata('user_info');
            $admission_office_id=$user_info[0]->admission_officer;
            $admission_office = $this->db->query("SELECT * FROM tbl_user WHERE employee_id = '$admission_office_id'")->result_array();
            $data['admission_officer_id'] = $admission_office[0]['id'];

            $this->db->insert('applicants', $data);
            $mail=$this->send_email_for_applicant($data['admission_officer_id'],($data['first_name'].' '.$data['last_name']),($this->input->post('password', true)),$data['email']);
           $this->db->insert('applicants', $data);
            $status_mess=" and successfully email send.";
            if(!$mail)
            {
                $status_mess=" and email cannot send. ";
            }
            $sdata['message'] = "Successfully Data Saved ".$status_mess;
            $this->session->set_userdata($sdata);
            redirect("reference_applicants/index");
        } else {
          $user_info = $this->session->userdata('user_info');
          $agent_id=$user_info[0]->id;
          // print_r($admission_office_id);
          // die();
            $data = array();
            $data['title'] = 'Student Registration ';
            $data['heading_msg'] = "Student Registration";
            //$data['is_show_button'] = "index";
            $data['marital_status'] = $this->db->query("SELECT * FROM cc_marital_status")->result_array();
            $data['genders'] = $this->db->query("SELECT * FROM cc_genders")->result_array();
            $data['countries'] = $this->db->query("SELECT * FROM cc_countries")->result_array();
            $data['origin_office'] = $this->Admin_login->get_branch_by_agent_id($agent_id);
            $data['lead_source'] = $this->db->query("SELECT * FROM cc_lead_sources where name='Agent'")->result_array();
            $data['country_cities'] = $this->db->query("SELECT * FROM country_cities")->result_array();
            $data['course_level'] = $this->db->query("SELECT * FROM cc_course_level")->result_array();
            $data['intake_years'] =$this->Admin_login->get_intake_years();
            $data['intake_months'] =$this->Admin_login->get_intake_months();
            $data['country_code'] =$this->Admin_login->get_country_code();
            $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
            $data['maincontent'] = $this->load->view('reference_applicants/student_registration', $data, true);
            $this->load->view('admin_logins/index', $data);
        }
    }
    public function generateApplicantsId($branch_id)
    {
      $branches = $this->db->query("SELECT * FROM tbl_branch WHERE id = '$branch_id'")->result_array();
      $code = "ST-";
      $code .= $branches[0]['code'];
      $total_applicants = count($this->db->query("SELECT id FROM applicants")->result_array());
      return $code.str_pad(($total_applicants + 1), 3, '0', STR_PAD_LEFT);
    }
    function send_email_for_applicant($admission_officer_id,$applicant_name,$passord,$applicant_email){

       $info =$this->Admin_login->get_admission_officer_info($admission_officer_id);
         //reciver
        $receivers = $applicant_email;
         //subject
        $header = "Congratulation";
       //message
       $message = "";
       $message=$message.'Your account is successfully registered. Press sign in button to sing in your account. <br>';
       $message=$message.'Your User Id: <b>'.$applicant_email.'</b><br>';
       $message=$message.'Your Password: <b>'.$passord.'</b><br>';
        $email_data = array();
        $session_user = $this->session->userdata('user_info');

        if(empty($session_user))
        {
          $email_data['sender_name'] = $info[0]['name'] . '(' . $info[0]['code'] . ')';
          $email_data['sender_designation_name'] = $info[0]['post_name'];
          $email_data['sender_branch_name'] = $info[0]['branch_name'];
        }else{
          print_r($info);
          die();
          $employee_id = $session_user[0]->employee_id;
          $employee_info = $this->Employee->getEmployeeName($employee_id);
          $email_data['sender_name'] = $employee_info[0]['name'] . '(' . $employee_info[0]['code'] . ')';
          $email_data['sender_designation_name'] = $employee_info[0]['designation_name'];
          $email_data['sender_branch_name'] = $employee_info[0]['branch_name'];
        }

         $email_data['receiver_name'] = $applicant_name;
        // $email_data['admission_name'] = $info[0]['name'] ;
        // $email_data['admission_email'] = $info[0]['email'];
        // $email_data['admission_mobile'] = $info[0]['mobile'];
        $email_data['file_link'] = "http://achi365.com/spate-i/students_login";

        $email_data['title'] = $header;
        $email_data['details'] = $message;
        $email_data['sign_in'] = 'Sign In';
        $email_message = $this->load->view('admin_logins/email_template2',$email_data,true);
    // print_r($this->load->view('admin_logins/email_template2',$email_data,true));
    // die();

        if($this->Email_process->send_email($receivers,$header,$email_message)){
           return 1;
        }else{
         return 0;
        }

    }
    //University Application
    public function application($id)
    {
        if ($_POST) {
            $data = array();
            $application_code =$this->generateApplicationsId($this->input->post('country_id', true));
            if($application_code=="0")
            {
              $sdata['exception'] = "Country code missing.Please insert country code.";
              $this->session->set_userdata($sdata);
              redirect("reference_applicants/view/".$id);
            }
            $sop_img_path='0';
            if (isset($_FILES['sop_path']) && $_FILES['sop_path']['name'] != '') {
                $sop_img_path = $this->my_file_upload('sop_path', 'sop_path');

                if ($sop_img_path=='0') {
                    $sdata['exception'] = "Statement of Purpose doesn't upload." . $this->upload->display_errors();
                    $this->session->set_userdata($sdata);
                    redirect("reference_applicants/view/".$id);
                }
            }
           $others_img_path='0';
            if (isset($_FILES['others_file_path']) && $_FILES['others_file_path']['name'] != '') {
                $others_img_path = $this->my_file_upload('others_file_path', 'others_file');

                if ($others_img_path=='0') {
                    $sdata['exception'] = "Others file doesn't upload." . $this->upload->display_errors();
                    $this->session->set_userdata($sdata);
                    redirect("reference_applicants/view/".$id);
                }
            }
            $data['sop_img_path'] = $sop_img_path;
            $data['others_img_path'] = $others_img_path;
            $data['application_code']=$application_code;
            $data['applicant_id'] = $id;
            $data['country_id'] = $this->input->post('country_id', true);
            $data['institution_id'] = $this->input->post('institution_id', true);
            $data['course_level_id'] = $this->input->post('course_level_id', true);
            $data['subject_id'] = $this->input->post('subject_id', true);
            $data['intake_month'] = $this->input->post('intake_month', true);
            $data['intake_year'] = $this->input->post('intake_year', true);
            $data['application_fees'] = $this->input->post('application_fees', true);
            $data['proces_date'] = date('Y-m-d H:i:s');
            if ($this->input->post('currency_id', true) != '') {
                $data['currency_id'] = $this->input->post('currency_id', true);
            }


            if ($this->input->post('payment_date', true) != '') {
                $data['payment_date'] = $this->input->post('payment_date', true);
            }
            $user_info = $this->session->userdata('user_info');
            $admission_office_id=$user_info[0]->admission_officer;
            $admission_office = $this->db->query("SELECT * FROM tbl_user WHERE employee_id = '$admission_office_id'")->result_array();
            $data['added_by'] = $admission_office[0]['id'];
            $data['process_officer_id'] = $admission_office[0]['id'];
            $data['created_at'] = date('Y-m-d H:i:s');

            $this->db->insert('university_application', $data);
            $sdata['message'] = "Data Successfully Added.";
            $this->session->set_userdata($sdata);

            redirect("reference_applicants/view/".$id);
        } else {
          $information_count =$this->Admin_login->check_information_update_by_applicant_id($id);
          if ($information_count) {
            $data = array();
            $data['title'] = 'Add University Application';
            $data['heading_msg'] = "Add University Application";
            $data['is_show_button'] = "index";
            $data['applicant_info'] = $this->Admin_login->get_applicants_info_by_unit_id($id);
            $data['countries'] = $this->db->query("SELECT * FROM cc_countries")->result_array();
            $data['intake_years'] =$this->Admin_login->get_intake_years();
            $data['intake_months'] =$this->Admin_login->get_intake_months();
            $data['institutions'] = $this->db->query("SELECT * FROM institutions")->result_array();
            $data['courses'] = $this->db->query("SELECT * FROM courses")->result_array();
            $data['course_level'] = $this->db->query("SELECT * FROM cc_course_level")->result_array();
            $data['currencies'] = $this->db->query("SELECT * FROM cc_currencies")->result_array();
            $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
            $data['maincontent'] = $this->load->view('reference_applicants/application', $data, true);
            $this->load->view('admin_logins/index', $data);

        } else {
            $sdata['exception'] = "Please added student Information first.";
            $this->session->set_userdata($sdata);
            redirect("reference_applicants/index");
        }
        }
    }
    public function generateApplicationsId($country_id)
    {
        $country = $this->db->query("SELECT * FROM cc_countries WHERE id = '$country_id'")->result_array();
        if(empty($country[0]['code']))
        {
          return "0";
        }
        $code = "AP-";
        $code .= $country[0]['code'];
        $total_applications = count($this->db->query("SELECT id FROM university_application")->result_array());
        return $code.str_pad(($total_applications + 1), 3, '0', STR_PAD_LEFT);
    }
    public function education_save($id)
    {
        //  echo '<pre>';
        //  print_r($_FILES);
        // die;
        $certificate_img = "0";
        $transcript_img = "0";
        if (isset($_FILES['certificate_img']) && $_FILES['certificate_img']['name'] != '') {
            $certificate_img = $this->my_file_upload('certificate_img', 'certificate');
            //echo $certificate_img;
            //die;
        }

        if (isset($_FILES['transcript_img']) && $_FILES['transcript_img']['name'] != '') {
            $transcript_img = $this->my_file_upload('transcript_img', 'transcript');
            //  echo $transcript_img;
            //die;
        }


        $data = array();
        $data['certificate_img_path'] = $certificate_img;
        $data['transcript_img_path'] = $transcript_img;
        $data['applicant_id'] = $id;
        $data['country_id'] = $this->input->post('country_id', true);
        $data['institute_1'] = $this->input->post('institute_1', true);
        if($this->input->post('from_date', true)!='')
          {
              $data['from_date'] = $this->input->post('from_date', true);
          }
          if($this->input->post('to_date', true)!='')
          {
              $data['to_date'] = $this->input->post('to_date', true);
          }
        $data['year_of_passing_1'] = $this->input->post('year_of_passing_1', true);
        $data['qualification_1'] = $this->input->post('qualification_1', true);
        $data['grade_1'] = $this->input->post('grade_1', true);
        $data['course_id'] = $this->input->post('course_id', true);
        $this->db->insert('applicant_education', $data);
        $sdata['message'] = "Data Successfully Added.";
        $this->session->set_userdata($sdata);

        redirect("reference_applicants/view/".$id);
    }
    public function my_file_upload($filename, $type)
    {
      //  $new_file_name = time().'_'.$type.'_'.$_FILES[$filename]['name'];
        $ext = explode('.',$_FILES[$filename]['name']);
        $new_file_name = time().'_'. $type . '.' .end($ext);

        //  echo $new_file_name;
        // print_r($filename);
        //  die;
        $this->load->library('upload');

        $config = array(
                    'upload_path' => "media/applicant_edu/",
                    'allowed_types' => "jpg|png|jpeg|JPEG|JPG|PNG|pdf|csv|doc|docx",
                    'max_size' => "99999999999",
                    'max_height' => "3000",
                    'max_width' => "3000",
                    'file_name' => $new_file_name
                );

        $this->upload->initialize($config);

        if (!$this->upload->do_upload($filename)) {
            return '0';
        } else {
            return $new_file_name;
        }
    }
    public function edit_education($id=null)
    {
        if ($_POST) {
            $applicant_id = $this->input->post('applicant_id', true);
            $data = array();


            if (isset($_FILES['certificate_img']) && $_FILES['certificate_img']['name'] != '') {
                $certificate_img = $this->my_file_upload('certificate_img', 'certificate');
                if ($certificate_img=='0') {
                    $sdata['exception'] = "Certificate file doesn't upload." . $this->upload->display_errors();
                    $this->session->set_userdata($sdata);
                    redirect("reference_applicants/edit/".$applicant_id);
                }
                $old_certificate_file = $this->input->post('certificate_img_name', true);
                $msg =$this->Admin_login->my_file_remove($old_certificate_file, $certificate_img, 'media/applicant_edu/');
                $data['certificate_img_path'] = $certificate_img;
                //echo $certificate_img;
              //die;
            }

            if (isset($_FILES['transcript_img']) && $_FILES['transcript_img']['name'] != '') {
                $transcript_img = $this->my_file_upload('transcript_img', 'transcript');

                if ($transcript_img=='0') {
                    $sdata['exception'] = "Transcript file doesn't upload." . $this->upload->display_errors();
                    $this->session->set_userdata($sdata);
                    redirect("reference_applicants/edit/".$applicant_id);
                }
                $old_transcript_file = $this->input->post('transcript_img_name', true);
                $msg =$this->Admin_login->my_file_remove($old_transcript_file, $transcript_img, 'media/applicant_edu/');
                $data['transcript_img_path'] = $transcript_img;
            }

            $data['id'] = $this->input->post('id', true);
            $data['country_id'] = $this->input->post('country_id', true);
            $data['applicant_id'] = $applicant_id;
            $data['institute_1'] = $this->input->post('institute_1', true);
            $data['year_of_passing_1'] = $this->input->post('year_of_passing_1', true);
            $data['qualification_1'] = $this->input->post('qualification_1', true);
            $data['grade_1'] = $this->input->post('grade_1', true);
            if($this->input->post('from_date', true)!='')
            {
                $data['from_date'] = $this->input->post('from_date', true);
            }
            if($this->input->post('to_date', true)!='')
            {
                $data['to_date'] = $this->input->post('to_date', true);
            }
            $data['course_id'] = $this->input->post('course_id', true);
            $this->db->where('id', $data['id']);
            $this->db->update('applicant_education', $data);
            $sdata['message'] = "You are Successfully Education Information !";
            $this->session->set_userdata($sdata);
            redirect("reference_applicants/view/".$applicant_id);
        } else {
            $data = array();
            $data['title'] = 'Student Education Information';
            $data['heading_msg'] = "Student Education Information";
            $data['is_show_button'] = "index";
             $data['courses'] = $this->Admin_login->get_subject_list();
            $data['countries'] = $this->db->query("SELECT * FROM cc_countries")->result_array();
            $data['edit_education'] = $this->Admin_login->get_edit_education_by_unit_id($id);
            $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
            $data['maincontent'] = $this->load->view('reference_applicants/edit_education', $data, true);
            $this->load->view('admin_logins/index', $data);
        }
    }
    public function delete_education($id)
    {
        $data = $this->db->where('id', $id)->get('applicant_education')->row();
        $applicant_id=$data->applicant_id;
        $certificate_file =$data->certificate_img_path;
        $transcript_file =$data->transcript_img_path;

        if ($this->db->delete('applicant_education', array('id' => $id))) {
            $this->Admin_login->my_file_remove($certificate_file, 'test', 'media/applicant_edu/');
            $this->Admin_login->my_file_remove($transcript_file, 'test', 'media/applicant_edu/');
            $sdata['message'] = "Successfully Data Deleted";
            $this->session->set_userdata($sdata);
            redirect("reference_applicants/view/".$applicant_id);
        } else {
            $sdata['exception'] = "Education Not Deleted";
            $this->session->set_userdata($sdata);
            redirect("reference_applicants/view/".$applicant_id);
        }
    }

    public function language_save($id)
    {
        $data = array();
        $data['certificate_file']="0";
        if (isset($_FILES['certificate_file']) && $_FILES['certificate_file']['name'] != '') {
            $certificate_img = $this->my_file_upload('certificate_file', 'lan_certificate');
            $data['certificate_file'] = $certificate_img;
            if ($certificate_img=='0') {
                $sdata['exception'] = "Transcript Image doesn't upload." . $this->upload->display_errors();
                $this->session->set_userdata($sdata);
                redirect("reference_applicants/edit/".$id);
            }
        }

        $data['applicant_id'] = $id;
        $data['is_ielts'] = $this->input->post('is_ielts', true);
        $data['ielts_listening'] = $this->input->post('ielts_listening', true);
        $data['ielts_writing'] = $this->input->post('ielts_writing', true);
        $data['ielts_reading'] = $this->input->post('ielts_reading', true);
        $data['ielts_speaking'] = $this->input->post('ielts_speaking', true);
        $data['ielts_overall_score'] = $this->input->post('ielts_overall_score', true);
        $data['exam_date'] = $this->input->post('exam_date', true);

        $this->db->insert('applicant_english_language', $data);
        $sdata['message'] = "Data Successfully Added.";
        $this->session->set_userdata($sdata);

        redirect("reference_applicants/view/".$id);
    }
    public function edit_language($id=null)
    {
        if ($_POST) {
            $applicant_id = $this->input->post('applicant_id', true);
            $data = array();
            if (isset($_FILES['certificate_file']) && $_FILES['certificate_file']['name'] != '') {
                $certificate_img = $this->my_file_upload('certificate_file', 'lan_certificate');
                if ($certificate_img=='0') {
                    $sdata['exception'] = "Transcript Image doesn't upload." . $this->upload->display_errors();
                    $this->session->set_userdata($sdata);
                    redirect("reference_applicants/edit/".$applicant_id);
                }
                $old_certificate_file = $this->input->post('certificate_file_name', true);
                $msg =$this->Admin_login->my_file_remove($old_certificate_file, $certificate_img, 'media/applicant_edu/');
                $data['certificate_file'] = $certificate_img;
            }

            $data['id'] = $this->input->post('id', true);
            $data['applicant_id'] = $applicant_id;
            $data['is_ielts'] = $this->input->post('is_ielts', true);
            $data['ielts_listening'] = $this->input->post('ielts_listening', true);
            $data['ielts_writing'] = $this->input->post('ielts_writing', true);
            $data['ielts_reading'] = $this->input->post('ielts_reading', true);
            $data['ielts_speaking'] = $this->input->post('ielts_speaking', true);
            $data['ielts_overall_score'] = $this->input->post('ielts_overall_score', true);
            $data['exam_date'] = $this->input->post('exam_date', true);
            $this->db->where('id', $data['id']);
            $this->db->update('applicant_english_language', $data);
            $sdata['message'] = "You are Successfully Upadted Language Information !";
            $this->session->set_userdata($sdata);
            redirect("reference_applicants/view/".$applicant_id);
        } else {
            $data = array();
            $data['title'] = 'Student Language Information';
            $data['heading_msg'] = "Student Language Information";
            $data['is_show_button'] = "index";
            $data['edit_language'] = $this->Admin_login->get_edit_language_by_applicant_id($id);
            $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
            $data['maincontent'] = $this->load->view('reference_applicants/edit_language', $data, true);
            $this->load->view('admin_logins/index', $data);
        }
    }
    public function delete_language($id)
    {
        $data = $this->db->where('id', $id)->get('applicant_english_language')->row();
        $applicant_id=$data->applicant_id;
        $certificate_file =$data->certificate_file;
        if ($this->db->delete('applicant_english_language', array('id' => $id))) {
            $this->Admin_login->my_file_remove($certificate_file, 'test', 'media/applicant_edu/');
            $sdata['message'] = "Successfully Data Deleted";
            $this->session->set_userdata($sdata);
            redirect("reference_applicants/view/".$applicant_id);
        } else {
            $sdata['exception'] = "Language Not Deleted";
            $this->session->set_userdata($sdata);
            redirect("reference_applicants/view/".$applicant_id);
        }
    }

    public function delete_work_experience($id)
    {
        $data = $this->db->where('id', $id)->get('applicant_work_experience')->row();
        $applicant_id=$data->applicant_id;
        $experience_letter =$data->experience_letter;
        if ($this->db->delete('applicant_work_experience', array('id' => $id))) {
            $this->Admin_login->my_file_remove($experience_letter, 'test', 'media/applicant_edu/');
            $sdata['message'] = "Successfully Data Deleted";
            $this->session->set_userdata($sdata);
            redirect("reference_applicants/view/".$applicant_id);
        } else {
            $sdata['exception'] = "Work Experience Not Deleted";
            $this->session->set_userdata($sdata);
            redirect("reference_applicants/view/".$applicant_id);
        }
    }
    public function edit_work_experience($id=null)
    {
        if ($_POST) {
            $applicant_id = $this->input->post('applicant_id', true);
            $data = array();
            if (isset($_FILES['experience_letter']) && $_FILES['experience_letter']['name'] != '') {
                $experience_letter = $this->my_file_upload('experience_letter', 'ecp_letter');
                $data['experience_letter'] = $experience_letter;
                if ($experience_letter=='0') {
                    $sdata['exception'] = "Experience Letter doesn't upload." . $this->upload->display_errors();
                    $this->session->set_userdata($sdata);
                    redirect("reference_applicants/edit/".$applicant_id);
                }
                $old_experience_letter_file = $this->input->post('experience_letter_name', true);
                $msg =$this->Admin_login->my_file_remove($old_experience_letter_file, $experience_letter, 'media/applicant_edu/');
            }

            $data['id'] = $this->input->post('id', true);
            $data['applicant_id'] = $applicant_id;
            $data['is_till_now'] =  $this->input->post('is_till_now', true);
            $data['employer_name_1'] = $this->input->post('employer_name_1', true);
            $data['position_1'] = $this->input->post('position_1', true);
            $data['country_id'] = $this->input->post('country_id', true);
            $data['from'] = $this->input->post('from', true);
            $data['to'] = $this->input->post('to', true);
            $this->db->where('id', $data['id']);
            $this->db->update('applicant_work_experience', $data);
            $sdata['message'] = "You are Successfully Upadted Work Experience Information !";
            $this->session->set_userdata($sdata);
            redirect("reference_applicants/view/".$applicant_id);
        } else {
            $data = array();
            $data['title'] = 'Student Work Experience Information';
            $data['heading_msg'] = "Student Work Experience Information";
            $data['is_show_button'] = "index";
            $data['countries'] = $this->db->query("SELECT * FROM cc_countries")->result_array();
            $data['edit_work_experience'] = $this->Admin_login->get_edit_work_experience_by_applicant_id($id);
            $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
            $data['maincontent'] = $this->load->view('reference_applicants/edit_work_experience', $data, true);
            $this->load->view('admin_logins/index', $data);
        }
    }

    public function work_experience($id)
    {
        $data = array();
        $data['experience_letter'] ="0";
        if (isset($_FILES['experience_letter']) && $_FILES['experience_letter']['name'] != '') {
            $experience_letter = $this->my_file_upload('experience_letter', 'ecp_letter');
            $data['experience_letter'] = $experience_letter;
            //echo $certificate_img;
          //die;
        }

        $data['applicant_id'] = $id;
        $data['is_till_now'] =  $this->input->post('is_till_now', true);
        $data['employer_name_1'] = $this->input->post('employer_name_1', true);
        $data['position_1'] = $this->input->post('position_1', true);
        $data['country_id'] = $this->input->post('country_id', true);
        $data['from'] = $this->input->post('from', true);
        $data['to'] = $this->input->post('to', true);
        $this->db->insert('applicant_work_experience', $data);
        $sdata['message'] = "Data Successfully Added.";
        $this->session->set_userdata($sdata);

        redirect("reference_applicants/view/".$id);
    }


    public function edit_reference($id=null)
    {
        if ($_POST) {
            $applicant_id = $this->input->post('applicant_id', true);
            $reference_letter='0';
            $data = array();
            if (isset($_FILES['reference_image']) && $_FILES['reference_image']['name'] != '') {
                $reference_letter = $this->my_file_upload('reference_image', 'reference_letter');

                if ($reference_letter=='0') {
                    $sdata['exception'] = "Reference Letter doesn't upload." . $this->upload->display_errors();
                    $this->session->set_userdata($sdata);
                    redirect("reference_applicants/edit/".$applicant_id);
                }
                $old_reference_letter_file = $this->input->post('reference_letter_name', true);
                $msg =$this->Admin_login->my_file_remove($old_reference_letter_file, $reference_letter, 'media/applicant_edu/');
            }
            $data['reference_letter'] = $reference_letter;
            $data['id'] = $this->input->post('id', true);
            $data['applicant_id'] = $applicant_id;
            $data['ref1_name'] = $this->input->post('ref1_name', true);
            $data['ref1_designation'] = $this->input->post('ref1_designation', true);
            $data['ref1_email'] = $this->input->post('ref1_email', true);
            $data['ref1_phone'] = $this->input->post('ref1_phone', true);
            $data['ref1_fax'] = $this->input->post('ref1_fax', true);
            $data['ref1_address'] = $this->input->post('ref1_address', true);
            $data['ref1_city'] = $this->input->post('ref1_city', true);
            $data['ref1_state'] = $this->input->post('ref1_state', true);
            $data['ref1_country_id'] = $this->input->post('ref1_country_id', true);
            $data['country_code'] = $this->input->post('country_code', true);
            $this->db->where('id', $data['id']);
            $this->db->update('applicant_reference', $data);
            $sdata['message'] = "You are Successfully Upadted Reference Information !";
            $this->session->set_userdata($sdata);
            redirect("reference_applicants/view/".$applicant_id);
        } else {
            $data = array();
            $data['title'] = 'Student Reference Information';
            $data['heading_msg'] = "Student Reference Information";
            $data['is_show_button'] = "index";
            $data['countries'] = $this->db->query("SELECT * FROM cc_countries")->result_array();
            $data['country_code'] =$this->Admin_login->get_country_code();
            $data['edit_reference'] = $this->Admin_login->get_edit_reference_by_applicant_id($id);
            $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
            $data['maincontent'] = $this->load->view('reference_applicants/edit_reference', $data, true);
            $this->load->view('admin_logins/index', $data);
        }
    }
    public function delete_reference($id)
    {
        $data = $this->db->where('id', $id)->get('applicant_reference')->row();
        $applicant_id=$data->applicant_id;
        $reference_letter =$data->reference_letter;
        if ($this->db->delete('applicant_reference', array('id' => $id))) {
            $this->Admin_login->my_file_remove($reference_letter, 'test', 'media/applicant_edu/');
            $sdata['message'] = "Successfully Data Deleted";
            $this->session->set_userdata($sdata);
            redirect("reference_applicants/view/".$applicant_id);
        } else {
            $sdata['exception'] = "Work Experience Not Deleted";
            $this->session->set_userdata($sdata);
            redirect("reference_applicants/view/".$applicant_id);
        }
    }
    public function reference($id)
    {
        $data = array();
        if (isset($_FILES['reference_image']) && $_FILES['reference_image']['name'] != '') {
            $reference_letter = $this->my_file_upload('reference_image', 'reference_letter');
            $data['reference_letter'] = $reference_letter;
            //echo $certificate_img;
          //die;
        }
        $data['applicant_id'] = $id;
        $data['ref1_name'] = $this->input->post('ref1_name', true);
        $data['ref1_designation'] = $this->input->post('ref1_designation', true);
        $data['ref1_email'] = $this->input->post('ref1_email', true);
        $data['ref1_phone'] = $this->input->post('ref1_phone', true);
        $data['ref1_fax'] = $this->input->post('ref1_fax', true);
        $data['ref1_address'] = $this->input->post('ref1_address', true);
        $data['ref1_city'] = $this->input->post('ref1_city', true);
        $data['ref1_state'] = $this->input->post('ref1_state', true);
        $data['ref1_country_id'] = $this->input->post('ref1_country_id', true);
        $data['country_code'] = $this->input->post('country_code', true);

        $this->db->insert('applicant_reference', $data);
        $sdata['message'] = "Data Successfully Added.";
        $this->session->set_userdata($sdata);

        redirect("reference_applicants/view/".$id);
    }

    public function getInstituteByCountryId()
    {
        $country_id = $this->input->get('country_id', true);

        $data = array();
        $data['institute_info'] = $this->db->query("SELECT * FROM institutions WHERE `country_id`  = '$country_id'")->result_array();
        $this->load->view('reference_applicants/institution_list', $data);
    }


    public function getCourseByCourseLevelId()
    {
        $course_level_id = $this->input->get('course_level_id', true);
        $country_id = $this->input->get('country_id', true);
        $institution_id = $this->input->get('institution_id', true);
        $data = array();
        $query="";
        if(!empty($course_level_id))
        {
          $query=" AND course_level_id=$course_level_id";
        }
        if(!empty($country_id))
        {
          $query=$query." AND country_id=$country_id";
        }
        if(!empty($institution_id))
        {
          $query=$query." AND institution_id=$institution_id";
        }
        $data['subj_info'] = $this->db->query("SELECT * FROM courses WHERE 1=1 $query")->result_array();
        $this->load->view('reference_applicants/subject_list', $data);
    }
    public function view_status($id=null)
    {
        if ($_POST) {

            $sdata['message'] = "You are Successfully Change Application Status".$status_mess;
            $this->session->set_userdata($sdata);
            redirect("reference_applicants/view_status/".$id);
        } else {
            $data['id'] = $id;
            $data['title'] = 'View Application Status';
            $data['heading_msg'] = "View Application Status";
            $data['is_show_button'] = "index";

            $data['application_status_view']=$this->db->query("SELECT
                                  (SELECT `name` FROM tbl_application_status WHERE id=h.old_status_id) AS old_status ,
                                  (SELECT `name` FROM tbl_application_status WHERE id=h.new_status_id) AS new_status ,u.application_code,
                                  date_time,te.name  as process_officer,h.remarks,file_location,i.`name` AS institution_name,CONCAT(a.`first_name`,' ',a.`last_name`,'(',a.`applicant_code`,')') AS applicant_name
                                  FROM tbl_application_status_history AS h
                                  LEFT JOIN university_application AS u ON h.`application_id`=u.`id`
                                  LEFT JOIN `institutions` AS i ON u.`institution_id`=i.`id`
                                  LEFT JOIN `applicants` AS a ON u.`applicant_id`=a.`id`
                                  LEFT JOIN tbl_user as tu on u.process_officer_id=tu.id
                                  LEFT JOIN tbl_employee as te on tu.employee_id=te.id
                                  WHERE h.application_id='$id'
                                  ORDER BY h.id desc")->result_array();
            $data['application_status'] = $this->db->query("SELECT * FROM tbl_application_status")->result_array();
            $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
            $data['maincontent'] = $this->load->view('reference_applicants/view_status', $data, true);
            $this->load->view('admin_logins/index', $data);
        }
    }
}
