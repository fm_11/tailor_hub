<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class On_process_applicants extends CI_Controller
{
    public $notification = array();

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login','Email_process','Employee'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->db->query('SET SESSION sql_mode = ""');
        $employee_id = $user_info[0]->employee_id;
        //echo $employee_id; die;
        $this->notification = $this->Admin_login->get_notification($employee_id);
    }

    public function index()
    {
      $cond = array();
      if($_POST){
        $sdata['name'] = $this->input->post('name', true);
        $cond['name'] = $this->input->post('name', true);
        $sdata['country_id'] = $this->input->post('country_id', true);
        $cond['country_id'] = $this->input->post('country_id', true);
        $sdata['lead_source_id'] = $this->input->post('lead_source_id', true);
        $cond['lead_source_id'] = $this->input->post('lead_source_id', true);
        $sdata['code'] = $this->input->post('code', true);
        $cond['code'] = $this->input->post('code', true);
        $this->session->set_userdata($sdata);
      }else{
        $cond['name'] = $this->session->userdata('name');
        $cond['country_id'] = $this->session->userdata('country_id');
        $cond['lead_source_id'] = $this->session->userdata('lead_source_id');
        $cond['code'] = $this->session->userdata('code');
      }

        $data = array();
        $data['title'] = 'On-Process Applicants';
        $data['heading_msg'] = "On-Process Applicants";

        $user_info = $this->session->userdata('user_info');
        $is_head_of_admission = $user_info[0]->is_head_of_admission;
        $is_head_of_process = $user_info[0]->is_head_of_process;
        $employee_id = $user_info[0]->employee_id;
        if ($is_head_of_process != '1') {
            if ($is_head_of_admission == '1') {
                $head_of_admission_info = $this->db->query("SELECT id,branch_id FROM tbl_employee where id = '$employee_id'")->result_array();
                if (!empty($head_of_admission_info)) {
                    $cond['origin_office'] = $head_of_admission_info[0]['branch_id'];
                } else {
                    $cond['admission_officer_id'] = $user_info[0]->id;
                }
            } else {
                $cond['admission_officer_id'] = $user_info[0]->id;
            }
        }


        $this->load->library('pagination');
        $config['base_url'] = site_url('on_process_applicants/index/');
        $config['per_page'] = 10;
        $config['total_rows'] = count($this->Admin_login->get_applicants_info(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['applicants'] = $this->Admin_login->get_applicants_info(10, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['countries'] = $this->db->query("SELECT * FROM cc_countries")->result_array();
        $data['lead_source'] = $this->db->query("SELECT * FROM cc_lead_sources")->result_array();
        $data['branches'] = $this->db->query("SELECT * FROM leads")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('on_process_applicants/index', $data, true);
        $this->load->view('admin_logins/index', $data);
    }

    public function view($id)
    {
        $data = array();
        $data['title'] = 'View Application University';
        $data['heading_msg'] = "View Application University";
        $data['is_show_button'] = "index";
        $data['university_application'] =$this->Admin_login->get_university_by_applicant_id($id);
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('on_process_applicants/view', $data, true);
        $this->load->view('admin_logins/index', $data);
    }
    function applicants_email_send(){
        if($_POST){
          $receivers = $this->input->post('recipientList', true);
          $message = $this->input->post('message', true);
          $header = $this->input->post('header', true);

          $session_user = $this->session->userdata('user_info');
          $employee_id = $session_user[0]->employee_id;

          $email_data = array();
          $email_data['receiver_name'] = "All";
          $employee_info = $this->Employee->getEmployeeName($employee_id);
          $email_data['sender_name'] = $employee_info[0]['name'] . '(' . $employee_info[0]['code'] . ')';
          $email_data['sender_designation_name'] = $employee_info[0]['designation_name'];
          $email_data['sender_branch_name'] = $employee_info[0]['branch_name'];
          $email_data['title'] = $header;
          $email_data['details'] = $message;
          $email_data['file_link'] = "http://achi365.com/spate-i/students_login";
          $email_message = $this->load->view('admin_logins/email_template2',$email_data,true);

          if($this->Email_process->send_email($receivers,$header,$email_message)){
            $sdata['message'] = "You are Successfully Message Send.";
            $this->session->set_userdata($sdata);
           redirect("applicants/index");
          }else{
            $sdata['exception'] = "Sorry Message Doesn't Send !";
            $this->session->set_userdata($sdata);
           redirect("applicants/index");
          }
        }
      }
}
