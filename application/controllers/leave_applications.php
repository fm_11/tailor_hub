<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Leave_applications extends CI_Controller
{
    public $notification = array();
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Email_process','Admin_login','Timekeeping', 'common/insert_model', 'common/custom_methods_model',));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (!empty($user_info)) {
          //set timezone
            date_default_timezone_set($user_info[0]->time_zone);
            $employee_id =  $user_info[0]->employee_id;
            //echo $employee_id; die;
            $this->notification = $this->Admin_login->get_notification($employee_id);
        }
        $this->db->query('SET SESSION sql_mode = ""');
    }


    function varifyOneTimeUrl($token,$status){
         $token_info = $this->db->query("SELECT * FROM `tbl_verify_leave_from_email`
         WHERE `token` = '$token' AND `is_already_use` = '0'")->result_array();
         if(!empty($token_info)){
            $id = $token_info[0]['leave_id'];
            //start process
            if($status != 'A' && $status != "R"){
              echo "This type of status not allowed for leave !";
              die;
            }
             //echo $id.'/'.$status;
             $leave_info = $this->db->query("SELECT l.*,lt.name as leave_type_name FROM `tbl_employee_leave_applications` as l
               INNER JOIN `tbl_leave_types` as lt ON lt.id = l.leave_type_id
                WHERE l.id = '$id'")->result_array();
             if(!empty($leave_info)){


               $reporting_boss_id = $leave_info[0]['reporting_boss_id'];
               $login_employee_id =  $reporting_boss_id;
               $employee_id = $leave_info[0]['employee_id'];

               //echo $reporting_boss_id.'/'.$login_employee_id; die;

               if($login_employee_id == $reporting_boss_id){

                 $leave_data = array();
                 $leave_data['id'] = $id;
                 $leave_data['status'] = $status;
                 $leave_data['updated_on'] = date('Y-m-d H:i:s');
                 $leave_data['updated_by'] = $login_employee_id;

                 if($this->Timekeeping->update_employee_leave_info($leave_data)){

                   //update tbl_verify_leave_from_email
                   $verify_data = array();
                   $verify_data['token'] = $token;
                   $verify_data['is_already_use'] = 1;
                   $verify_data['updated_time'] =  date('Y-m-d H:i:s');
                   $this->db->where('token', $verify_data['token']);
                   $this->db->update('tbl_verify_leave_from_email', $verify_data);

                   //notification set
                   $notifications = array();
                   $notifications['from_employee_id'] = $reporting_boss_id;
                   $notifications['to_employee_id'] = $employee_id;
                   if($status == "A"){
                     $notifications['message'] = "Your leave application has been accepted.";
                   }else{
                     $notifications['message'] = "Your leave application has been rejected.";
                   }
                   $notifications['action_url'] = "leave_applications/index";
                   $notifications['reference_id'] = $id;
                   $notifications['notification_type'] = "LeaveApplication";
                   $notifications['log_time'] = date('Y-m-d H:i:s');
                   $this->db->insert('tbl_notification', $notifications);


                   //email send
                   $header = "Leave Application " . ($status == 'A' ? "Accepted" : 'Rejected');;
                   $email_message = $notifications['message'] . '<br>';
                   $email_message .= ("From Date: " . $leave_info[0]['from_date'] . '<br>');
                   $email_message .= ("To Date: " . $leave_info[0]['to_date'] . '<br>');
                   $email_message .= ("Leave Type: " . $leave_info[0]['leave_type_name'] . '<br>');

                   $employee_info = $this->db->query("SELECT id,email FROM `tbl_employee` WHERE id = '$employee_id'")->result_array();
                   $receivers = $employee_info[0]['email'];
                   //echo $receivers;die;
                  $mail_send = false;
                   if($receivers != ''){
                     if($this->Email_process->send_email($receivers,$header,$email_message)){
                       $mail_send = true;
                     }
                   }

                   echo "You are successfully leave application " . ($status == 'A' ? "Accepted" : 'Rejected');
                   die;

                 }else {
                   echo "Data updated failed. Please contact to vendor !";
                   die;
                 }
               }else{
                 echo "You're not allowed for this !";
                 die;
               }
             }else{
               echo "Employee leave information not found !";
               die;
             }
            //end process
         }else{
           echo 'Sorry ! It has been completed already. Thank you';
           die;
         }
    }

    public function index()
    {

        $session_user = $this->session->userdata('user_info');
        $user_role =  $session_user[0]->user_role;
        //echo '<pre>';
        //print_r($session_user);
        //die;
        $employee_id = $session_user[0]->employee_id;
        $data = array();
        $data['title'] = 'Employee Leave Application';
        $data['heading_msg'] = 'Employee Leave Application';
        $this->load->library('pagination');
        $config['base_url'] = site_url('leave_applications/index/');
        $config['per_page'] = 10;
        $cond = array();
        if($user_role != 1){
          $cond['employee_id'] = $employee_id;
        }else{
          $cond['employee_id'] ="";
        }
        $config['total_rows'] = count($this->Timekeeping->get_all_employee_leave_list(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['movements'] = $this->Timekeeping->get_all_employee_leave_list(10, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['is_show_button'] = "add";
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('leave_applications/index', $data, true);
        $this->load->view('admin_logins/index', $data);
    }



    public function approval_index()
    {
        $session_user = $this->session->userdata('user_info');
        //echo '<pre>';
        //print_r($session_user);
        //die;
        $employee_id = $session_user[0]->employee_id;
        $data = array();
        $data['title'] = 'Employee Leave Application Approval';
        $data['heading_msg'] = 'Employee Leave Application Approval';
        $this->load->library('pagination');
        $config['base_url'] = site_url('leave_applications/approval_index/');
        $config['per_page'] = 10;
        $cond = array();
        $cond['approver_employee_id'] = $employee_id;
        $cond['status'] = "P";
        $config['total_rows'] = count($this->Timekeeping->get_all_employee_leave_list(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['leave_infos'] = $this->Timekeeping->get_all_employee_leave_list(10, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('leave_applications/approval_index', $data, true);
        $this->load->view('admin_logins/index', $data);
    }

    public function application_approve_reject($id,$status)
    {
      if($status != 'A' && $status != "R"){
        $sdata['exception'] = "This type of status not allowed for leave !";
        $this->session->set_userdata($sdata);
        redirect("leave_applications/approval_index");
      }
       //echo $id.'/'.$status;
       $leave_info = $this->db->query("SELECT l.*,lt.name as leave_type_name FROM `tbl_employee_leave_applications` as l
         INNER JOIN `tbl_leave_types` as lt ON lt.id = l.leave_type_id
          WHERE l.id = '$id'")->result_array();
       if(!empty($leave_info)){
         $session_user = $this->session->userdata('user_info');
         $user_role =  $session_user[0]->user_role;
         $login_employee_id =  $session_user[0]->employee_id;

         $reporting_boss_id = $leave_info[0]['reporting_boss_id'];
         $employee_id = $leave_info[0]['employee_id'];

         //echo $reporting_boss_id.'/'.$login_employee_id; die;

         if($login_employee_id == $reporting_boss_id){

           $leave_data = array();
           $leave_data['id'] = $id;
           $leave_data['status'] = $status;
           $leave_data['updated_on'] = date('Y-m-d H:i:s');
           $leave_data['updated_by'] = $login_employee_id;

           if($this->Timekeeping->update_employee_leave_info($leave_data)){
             //notification set
             $notifications = array();
             $notifications['from_employee_id'] = $reporting_boss_id;
             $notifications['to_employee_id'] = $employee_id;
             if($status == "A"){
               $notifications['message'] = "Your leave application has been accepted.";
             }else{
               $notifications['message'] = "Your leave application has been rejected.";
             }
             $notifications['action_url'] = "leave_applications/index";
             $notifications['reference_id'] = $id;
             $notifications['notification_type'] = "LeaveApplication";
             $notifications['log_time'] = date('Y-m-d H:i:s');
             $this->db->insert('tbl_notification', $notifications);


             //email send
             $header = "Leave Application " . ($status == 'A' ? "Accepted" : 'Rejected');;
             $email_message = $notifications['message'] . '<br>';
             $email_message .= ("From Date: " . $leave_info[0]['from_date'] . '<br>');
             $email_message .= ("To Date: " . $leave_info[0]['to_date'] . '<br>');
             $email_message .= ("Leave Type: " . $leave_info[0]['leave_type_name'] . '<br>');

             $employee_info = $this->db->query("SELECT id,email FROM `tbl_employee` WHERE id = '$employee_id'")->result_array();
             $receivers = $employee_info[0]['email'];
             //echo $receivers;die;
            $mail_send = false;
            //  $email_message = $this->load->view('admin_logins/email_template2',$email_data,true);
            //  print_r($this->load->view('admin_logins/email_template2',$email_data,true));
              die;
             if($receivers != ''){
               if($this->Email_process->send_email($receivers,$header,$email_message)){
                 $mail_send = true;
               }
             }

             $sdata['message'] = "You are successfully leave application " . ($status == 'A' ? "Accepted" : 'Rejected');
             $this->session->set_userdata($sdata);
             redirect("leave_applications/approval_index");

           }else {
             $sdata['exception'] = "Data updated failed. Please contact to vendor !";
             $this->session->set_userdata($sdata);
             redirect("leave_applications/approval_index");
           }
         }else{
           $sdata['exception'] = "You're not allowed for this !";
           $this->session->set_userdata($sdata);
           redirect("leave_applications/approval_index");
         }
       }else{
         $sdata['exception'] = "Employee leave information not found !";
         $this->session->set_userdata($sdata);
         redirect("leave_applications/approval_index");
       }
    }


    function add()
    {
      //phpinfo();
      //die;
        $session_user = $this->session->userdata('user_info');
        $user_role =  $session_user[0]->user_role;
        $employee_id =  $session_user[0]->employee_id;
        if ($_POST) {
            $data = array();
            $data['employee_id'] = $this->input->post('employee_id');
            if($user_role != 1 && $employee_id != $data['employee_id']){
              $sdata['exception'] = "Employee  not allowed !";
              $this->session->set_userdata($sdata);
              redirect("leave_applications/add");
            }

            $applier_employee_id = $data['employee_id'];
            $from_date = $this->input->post('from_date');
            $to_date = $this->input->post('to_date');
            $already_applied = $this->db->query("SELECT id FROM `tbl_employee_leave_days` WHERE `employee_id` = '$applier_employee_id'
                              AND `date` BETWEEN '$from_date' AND '$to_date'")->result_array();
            if(!empty($already_applied)){
              $sdata['exception'] = "You have already applied to this date range!";
              $this->session->set_userdata($sdata);
              redirect("leave_applications/add");
            }

            $employee_info = $this->db->query("SELECT id,name,code,reporting_boss_id,leave_auto_approve,branch_id FROM `tbl_employee` WHERE id = '$applier_employee_id'")->result_array();
            if(!empty($employee_info)){
               if(($employee_info[0]['reporting_boss_id'] > 0 && $employee_info[0]['reporting_boss_id'] != '') || $employee_info[0]['leave_auto_approve'] == 1){

                //leave days calculation
                 $leave_days = $this->get_leave_days_without_holiday($this->input->post('from_date'),$this->input->post('to_date'),$employee_info[0]['branch_id']);
                 if(empty($leave_days)){
                   $sdata['exception'] = "Donn't get any working days to this date range!";
                   $this->session->set_userdata($sdata);
                   redirect("leave_applications/add");
                 }
                 $data['branch_id'] = $employee_info[0]['branch_id'];
                 $data['leave_type_id'] = $this->input->post('leave_type_id');
                 $data['from_date'] = $this->input->post('from_date');
                 $data['to_date'] = $this->input->post('to_date');
                 $data['num_of_leave_days'] = count($leave_days);
                 $data['reason'] = $this->input->post('reason');
                 $data['reporting_boss_id'] = $employee_info[0]['reporting_boss_id'];
                 $data['added_on'] = date('Y-m-d H:i:s');
                 $data['added_by'] = $employee_id;
                 if($employee_info[0]['leave_auto_approve'] == 1){
                   $data['status'] = 'A';
                 }else{
                   $data['status'] = 'P';
                 }

                 if($this->db->insert('tbl_employee_leave_applications', $data)){
                    $leave_id = $this->db->insert_id();
                    //save leave days
                    $process_leave_days = array();
                    $leave_count = 0;
                    foreach ($leave_days as $row):
                      $process_leave_days[$leave_count]['date'] = $row['date'];
                      $process_leave_days[$leave_count]['leave_id'] = $leave_id;
                      $process_leave_days[$leave_count]['employee_id'] = $data['employee_id'];
                      $process_leave_days[$leave_count]['leave_type_id'] = $data['leave_type_id'];
                      $leave_count++;
                    endforeach;


                    $this->db->insert_batch('tbl_employee_leave_days', $process_leave_days);
                    //save leave days


                   $session_leave_msg = "You are successfully leave application but mail send failed !";

                   if($employee_info[0]['leave_auto_approve'] != 1){
                     $notifications = array();
                     $notifications['from_employee_id'] = $data['employee_id'];
                     $notifications['to_employee_id'] = $employee_info[0]['reporting_boss_id'];
                     $notifications['message'] = "A leave application request came from " . $employee_info[0]['name'] .' (' . $employee_info[0]['code'] . ')';
                     $notifications['action_url'] = "leave_applications/approval_index";
                     $notifications['reference_id'] = $leave_id;
                     $notifications['notification_type'] = "LeaveApplication";
                     $notifications['log_time'] = date('Y-m-d H:i:s');
                     $this->db->insert('tbl_notification', $notifications);


                     //url create for leave accept and reject by email
                     $bytes = openssl_random_pseudo_bytes(16, $cstrong);
                     $hex   = bin2hex($bytes);
                     // Add the hash to your verification script URL
                     $url = base_url() . "leave_applications/varifyOneTimeUrl/" . $hex;
                     $approve_link = "<a href='" . $url . '/A' . "'>Approve |</a> ";
                     $reject_link = " <a href='" . $url . '/R' . "'>Reject</a>";

                     $email_verify = array();
                     $email_verify['leave_id'] = $leave_id;
                     $email_verify['token'] = $hex;
                     $email_verify['is_already_use'] = 0;
                     $email_verify['added_time'] = date('Y-m-d H:i:s');
                     $email_verify['added_by'] = $employee_id;
                     $this->db->insert('tbl_verify_leave_from_email', $email_verify);
                     //url create end

                     //email send
                     $header = "Leave Application -> " . $employee_info[0]['name'] .' (' . $employee_info[0]['code'] . ')';
                     $email_message = $notifications['message'] . '<br>';
                     $email_message .= ("From Date: " . $this->input->post('from_date')) . '<br>';
                     $email_message .= ("To Date: " . $this->input->post('to_date')) . '<br>';
                     $email_message .= ("Reason: " . $this->input->post('reason')) . '<br><br>';
                     $email_message .= ($approve_link . $reject_link);

                     $reporting_boss_id = $employee_info[0]['reporting_boss_id'];
                     $reporting_boss_info = $this->db->query("SELECT id,email FROM `tbl_employee` WHERE id = '$reporting_boss_id'")->result_array();
                     $receivers = $reporting_boss_info[0]['email'];
                     //echo $receivers;die;

                     if($receivers != ''){
                       if($this->Email_process->send_email($receivers,$header,$email_message)){
                         $session_leave_msg = "You are successfully leave application and mail sended !";
                       }
                     }
                   }

                   $sdata['message'] = $session_leave_msg;
                   $this->session->set_userdata($sdata);
                   redirect("leave_applications/add");
                 }else{
                   $sdata['exception'] = "You leave application could not save !";
                   $this->session->set_userdata($sdata);
                   redirect("leave_applications/add");
                 }
               }else{
                 $sdata['exception'] = "You leave application could not save. Because reporting boss not found !";
                 $this->session->set_userdata($sdata);
                 redirect("leave_applications/add");
               }
            }else{
              $sdata['exception'] = "You leave application could not save !";
              $this->session->set_userdata($sdata);
              redirect("leave_applications/add");
            }
        }

        if($user_role != 1){
          $data['employees'] = $this->db->query("SELECT id,name,code FROM `tbl_employee` WHERE id = '$employee_id'")->result_array();
        }else{
          $data['employees'] = $this->db->query("SELECT id,name,code FROM `tbl_employee` ORDER BY id")->result_array();
        }
        $data['leave_types'] = $this->db->query("SELECT * FROM `tbl_leave_types` ORDER BY id")->result_array();
        $data['title'] = 'Leave Application';
        $data['heading_msg'] = "Leave Application";
        $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('leave_applications/add', $data, true);
        $this->load->view('admin_logins/index', $data);
    }

    function get_leave_days_without_holiday($from_date,$to_date,$branch_id){
          $begin = new DateTime($from_date);
          $end   = new DateTime($to_date);

          //holiday check
          $is_holiday_allow = $this->db->query("SELECT id FROM `tbl_holiday` WHERE
           `date` != '$from_date' AND `date` != '$to_date' AND
           (`date` BETWEEN '$from_date' AND  '$to_date') AND `branch_id` = $branch_id")->result_array();

           $holiday_list = array();
           if(empty($is_holiday_allow)){//holiday applicable because holiday not found between two date
             $holiday_list = $this->db->query("SELECT `id`,`date` FROM `tbl_holiday` WHERE
              (`date` BETWEEN '$from_date' AND  '$to_date') AND `branch_id` = $branch_id")->result_array();
           }

          //echo '<pre>';
          //print_r($holiday_list);
         // die;
          $leave_days = array();
          $leave_count = 0;
          for($l = $begin; $l <= $end; $l->modify('+1 day')){
              $is_holiday = 0;

              if(!empty($holiday_list)){
                if(in_array($l->format("Y-m-d"), array_column($holiday_list, 'date'))){
                   $is_holiday = 1;
                }
              }

              if($is_holiday == 0){
                $leave_days[$leave_count]['date'] = $l->format("Y-m-d");
              }

              $leave_count++;
          }
          return $leave_days;
    }

    function get_leave_details(){
      $employee_id = $this->input->post('employee_id');
      $leave_types = $this->db->query("SELECT * FROM `tbl_leave_types` ORDER BY id")->result_array();

      $data = array();
      $i = 0;
      foreach ($leave_types as $row):
        $leave_type_id = $row['id'];
        $data[$i]['short_name'] = $row['short_name'];
        $data[$i]['allocated_days'] = $row['allocated_days'];
        $from_date = '2019-07-01';
        $to_date = '2020-06-30';
        $total_used_leave = $this->db->query("SELECT SUM(`num_of_leave_days`) AS total_num_of_leave_days FROM `tbl_employee_leave_applications` WHERE
         `from_date` >= '$from_date' AND  `to_date` <= '$to_date' AND `status` = 'A' AND `employee_id` = $employee_id AND `leave_type_id` = $leave_type_id")->result_array();
        if($total_used_leave[0]['total_num_of_leave_days'] == ''){
          $data[$i]['total_used_leave'] = 0;
        }else{
          $data[$i]['total_used_leave'] = $total_used_leave[0]['total_num_of_leave_days'];
        }

        $i++;
      endforeach;
      $leave_data = array();
      $leave_data['leave_details'] = $data;
      $this->load->view('leave_applications/leave_details', $leave_data);
    }

    function get_leave_details_all_date(){
      $leave_id = $this->input->post('leave_id');
      $leave_days = $this->db->query("SELECT `date` FROM `tbl_employee_leave_days` WHERE `leave_id` = $leave_id ORDER BY `date`")->result_array();

      $data = "";
      $i = 0;
      foreach ($leave_days as $row):
           if($i != 0){
             $data .= ', ' . $row['date'];
           }else{
             $data .= $row['date'];
           }
           $i++;
      endforeach;
      echo $data;

    }


    function edit($id=null)
    {
        if ($_POST) {
            $data = array();
            $data['id'] = $this->input->post('id');
            $data['end_time'] = $this->input->post('end_time');
            $this->db->where('id', $data['id']);
            $this->db->update('tbl_employee_movement_registers', $data);
            $sdata['message'] = "You are Successfully Movement Register Updated !";
            $this->session->set_userdata($sdata);
            redirect("movement_registers/index");
        }
        $data = array();
        $data['movement_info'] = $this->db->query("SELECT * FROM `tbl_employee_movement_registers` WHERE id = '$id'")->result_array();
        $data['title'] = 'Update Movement Register';
        $data['heading_msg'] = "Update Movement Register";
        $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('movement_registers/edit', $data, true);
        $this->load->view('admin_logins/index', $data);
    }

    function delete($id)
   {
       $leave_info = $this->db->query("SELECT * FROM `tbl_employee_leave_applications` WHERE id = '$id'")->result_array();
       if(empty($leave_info) || $leave_info[0]['status'] != "P"){
         $sdata['exception'] = "This application cannot be deleted !";
         $this->session->set_userdata($sdata);
         redirect("leave_applications/index");
       }
       $this->db->delete('tbl_notification', array('reference_id' => $id, 'notification_type' => 'LeaveApplication'));
       $this->db->delete('tbl_employee_leave_days', array('leave_id' => $id));
       $this->db->delete('tbl_employee_leave_applications', array('id' => $id));
       $sdata['message'] = "You are Successfully Leave Application Deleted !";
       $this->session->set_userdata($sdata);
       redirect("leave_applications/index");
   }



}
