<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Designations extends CI_Controller
{
public $notification = array();
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $employee_id =  $user_info[0]->employee_id;
        //echo $employee_id; die;
        $this->notification = $this->Admin_login->get_notification($employee_id);
    }

    public function index()
    {
      $data = array();
      $data['title'] = 'Designation Information';
      $data['heading_msg'] = "Designation Information";
      $data['designations'] = $this->db->query("SELECT * FROM tbl_employee_post")->result_array();
      $data['is_show_button'] = "add";
      $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
      $data['maincontent'] = $this->load->view('designations/index', $data, true);
      $this->load->view('admin_logins/index', $data);
    }


    function add()
    {
      if($_POST){
        $data = array();
        $data['name'] = $this->input->post('name', true);
        $data['num_of_post'] = $this->input->post('num_of_post', true);
        $data['shorting_order'] = $this->input->post('shorting_order', true);
        $this->db->insert('tbl_employee_post', $data);
        $sdata['message'] = "You are Successfully Added Designation Info !";
        $this->session->set_userdata($sdata);
        redirect("designations/index");
      }else{
        $data = array();
        $data['title'] = 'Add Designation Information';
        $data['heading_msg'] = "Add Designation Information";
        $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('designations/add', $data, true);
        $this->load->view('admin_logins/index', $data);
      }
  }

  function edit($id=null)
  {
    if($_POST){
      $data = array();
      $data['id'] = $this->input->post('id', true);
      $data['name'] = $this->input->post('name', true);
      $data['num_of_post'] = $this->input->post('num_of_post', true);
      $data['shorting_order'] = $this->input->post('shorting_order', true);
      $this->db->where('id', $data['id']);
      $this->db->update('tbl_employee_post', $data);
      $sdata['message'] = "You are Successfully Updated Designation Info !";
      $this->session->set_userdata($sdata);
      redirect("designations/index");
    }else{
      $data = array();
      $data['title'] = 'Update Designation Information';
      $data['heading_msg'] = "Update Designation Information";
      $data['is_show_button'] = "index";
      $data['designation'] = $this->db->query("SELECT * FROM tbl_employee_post WHERE id = '$id'")->row();
      $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
      $data['maincontent'] = $this->load->view('designations/edit', $data, true);
      $this->load->view('admin_logins/index', $data);
    }
}

  function delete($id)
  {
      $this->db->delete('tbl_employee_post', array('id' => $id));
      $sdata['message'] = "Designation Information Deleted Successfully !";
      $this->session->set_userdata($sdata);
      redirect("designations/index");
  }

}
