<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Institutions extends CI_Controller
{
    public $notification = array();

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $employee_id = $user_info[0]->employee_id;
        //echo $employee_id; die;
        $this->notification = $this->Admin_login->get_notification($employee_id);
        $this->db->query('SET SESSION sql_mode = ""');
    }

    public function index()
    {
      $cond = array();
      if($_POST){
        $sdata['name'] = $this->input->post('name', true);
        $cond['name'] = $this->input->post('name', true);
        $sdata['country_id'] = $this->input->post('country_id', true);
        $cond['country_id'] = $this->input->post('country_id', true);
        $this->session->set_userdata($sdata);

      }else{

        $cond['name'] = $this->session->userdata('name');
        $cond['country_id'] = $this->session->userdata('country_id');
      }

        $data = array();
        $data['title'] = 'Institution';
        $data['heading_msg'] = "Institution Information";

        $this->load->library('pagination');
        $config['base_url'] = site_url('institutions/index/');
        $config['per_page'] = 10;
        $config['total_rows'] = count($this->Admin_login->get_Institutions_info(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['Institutions'] = $this->Admin_login->get_Institutions_info(10, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['is_show_button'] = "add";
        $data['countries'] = $this->db->query("SELECT * FROM cc_countries")->result_array();
        $data['branches'] = $this->db->query("SELECT * FROM tbl_branch")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('institutions/index', $data, true);
        $this->load->view('admin_logins/index', $data);
    }

    public function add()
    {
        if ($_POST) {
            $data = array();
            //Logo upload
            if ($_FILES["institution_logo_path"]['name'] != '') {
              $ext = explode('.',$_FILES["institution_logo_path"]['name']);
              $logo_name = 'ins_logo_'.time().'.' .end($ext);
                $config = array(
                    'upload_path' => "media/institute/",
                    'allowed_types' => "jpg|png|jpeg|JPEG|JPG|PNG",
                    'overwrite' => true,
                    'max_size' => "99999999999",
                    'max_height' => "3000",
                    'max_width' => "3000",
                    'file_name' => $logo_name
                );
                $this->load->library('upload', $config);

                if (!$this->upload->do_upload('institution_logo_path')) {
                    $sdata['exception'] = "Logo doesn't upload." . $this->upload->display_errors()." and max height: 3000px ,max width: 3000px";
                    $this->session->set_userdata($sdata);
                    redirect("institutions/add");
                }
            }



            //End Logo Upload

            //Banner upload
            if ($_FILES["banner_image"]['name'] != '') {
                $ext = explode('.',$_FILES["banner_image"]['name']);
                $banner_name = 'ins_banner_'.time().'.' .end($ext);
                $config = array(
                    'upload_path' => "media/institute/",
                    'allowed_types' => "jpg|png|jpeg|JPEG|JPG|PNG",
                    'overwrite' => true,
                    'max_size' => "99999999999",
                    'max_height' => "3000",
                    'max_width' => "3000",
                    'file_name' => $banner_name
                );
                $this->load->library('upload', $config);

                if (!$this->upload->do_upload('banner_image')) {
                    $sdata['exception'] = "Banner Image doesn't upload." . $this->upload->display_errors()." and max height: 3000px ,max width: 3000px";
                    $this->session->set_userdata($sdata);
                    redirect("institutions/add");
                }
            }

            //media imagearc
            $media_type=$this->input->post('about_media_type', true);
            if ((int)$media_type==1) {
                if ($_FILES["about_media_image"]['name'] != '') {
                    $ext = explode('.',$_FILES["about_media_image"]['name']);
                    $about_media_image = 'about_media_'.time().'.' .end($ext);
                    $config = array(
                     'upload_path' => "media/institute/",
                     'allowed_types' => "jpg|png|jpeg|JPEG|JPG|PNG",
                     'overwrite' => true,
                     'max_size' => "99999999999",
                     'max_height' => "3000",
                     'max_width' => "3000",
                     'file_name' => $about_media_image
                 );
                    $this->load->library('upload', $config);

                    if (!$this->upload->do_upload('about_media_image')) {
                        $sdata['exception'] = "Media Image doesn't upload." . $this->upload->display_errors()." and max height: 3000px ,max width: 3000px";
                        $this->session->set_userdata($sdata);
                        redirect("institutions/index");
                    }
                    $data['about_media_image']=$about_media_image;
                    $data['about_media_vedio']='';
                }
            } else {
                $data['about_media_vedio']=$this->input->post('about_media_vedio', true);
                $data['about_media_image']='0';
            }

            //End Banner Upload


            if ($_FILES["institution_logo_path"]['name'] != '') {
                $data['institution_logo_path'] = $logo_name;
            }
            if ($_FILES["banner_image"]['name'] != '') {
                $data['banner_image'] = $banner_name;
            }
            $data['country_id'] = $this->input->post('country_id', true);
            $data['city_id'] = $this->input->post('city_id', true);
            $data['name'] = $this->input->post('name', true);
            $data['institution_estd_year'] = $this->input->post('institution_estd_year', true);
            $data['institution_type_id'] = $this->input->post('institution_type_id', true);
            $data['campus'] = $this->input->post('campus', true);
            $data['website'] = $this->input->post('website', true);
            $data['application_fee'] = $this->input->post('application_fee', true);
            $data['institutional_benefits'] = $this->input->post('institutional_benefits', true);
            $data['part_time_work_details'] = $this->input->post('part_time_work_details', true);
            $data['is_active'] = $this->input->post('is_active', true);
            $data['times_ranking'] = $this->input->post('times_ranking', true);
            $data['international'] = $this->input->post('international', true);
            //$data['satisfaction'] = $this->input->post('satisfaction', true);
            $data['about_media_type'] = $this->input->post('about_media_type', true);
            $data['about_quote'] = $this->input->post('about_quote', true);
            $data['location_address'] = $this->input->post('location_address', true);
            $data['financial_living_cost'] = $this->input->post('financial_living_cost', true);
            $data['yearly_cost_currency'] = $this->input->post('yearly_cost_currency', true);
            $data['average_undergraduate_tuition_currency'] = $this->input->post('average_undergraduate_tuition_currency', true);
            $data['average_postgraduate'] = $this->input->post('yearly_cost_currency', true);
            $data['campus_accommodation_currency'] = $this->input->post('campus_accommodation_currency', true);
            $data['financial_application_fees_currency'] = $this->input->post('financial_application_fees_currency', true);

            $data['financial_campus_accomodation'] = $this->input->post('financial_campus_accomodation', true);
            $data['financial_tution_udergrade'] = $this->input->post('financial_tution_udergrade', true);
            $data['financial_tution_postgrad'] = $this->input->post('financial_tution_postgrad', true);
            $data['location_map'] = $this->input->post('location_map', true);
            $data['scholarship'] = $this->input->post('scholarship', true);
            $data['university_foundation_campus'] = $this->input->post('university_foundation_campus', true);
            $data['pathway_foundation_campus_institution_id'] = $this->input->post('pathway_foundation_campus_institution_id', true);
            $data['pre_sessional_english'] = $this->input->post('pre_sessional_english', true);
            $data['ne_valid_agreement'] = $this->input->post('ne_valid_agreement', true);
            $data['application_method'] = $this->input->post('application_method', true);
            $data['inter_ranking'] = $this->input->post('inter_ranking', true);
            $data['phone'] = $this->input->post('phone', true);
            $data['email'] = $this->input->post('email', true);
            $data['international_contact_person'] = $this->input->post('international_contact_person', true);
            $this->db->insert('institutions', $data);
            $institutionId=  $this->db->insert_id();
            $query['valid']  = $this->input->post('valid_agreement', true);
            foreach ($query['valid'] as $key) {
                $valid_agree = array();
                if (!empty($key['active'])) {
                    $valid_agree['institution_id']=$institutionId;
                    $valid_agree['valid_agreement_id']=(int)$key['valid_agreement_id'];
                    $valid_agree['percentage']=$key['percentage'];

                    $this->db->insert('tbl_valid_agreement_details', $valid_agree);
                    //echo $key['percentage'].'mmm'.$key['valid_agreement_id'].'pp'.$key['active'].'<br>';
                }
            }
            $sdata['message'] = "You are Successfully Added Institutions Info !";
            $this->session->set_userdata($sdata);
            redirect("institutions/index");
        } else {
            $data = array();
            $data['title'] = 'Add Institutions Information';
            $data['heading_msg'] = "Add Institutions Information";
            $data['is_show_button'] = "index";
            $data['countries'] = $this->db->query("SELECT * FROM cc_countries")->result_array();
            $data['country_cities'] = $this->db->query("SELECT * FROM country_cities")->result_array();
            $data['institution_types'] = $this->db->query("SELECT * FROM cc_institution_type")->result_array();
            $data['institutions'] = $this->db->query("SELECT * FROM institutions")->result_array();
            $data['currencies'] = $this->db->query("SELECT * FROM cc_currencies")->result_array();
            $data['valid_agreement']=$this->db->query("SELECT * FROM  tbl_valid_agreement")->result_array();
            $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
            $data['maincontent'] = $this->load->view('institutions/add', $data, true);
            $this->load->view('admin_logins/index', $data);
        }
    }


    public function getCityByCountryId()
    {
        $city_id = $this->input->get('city_id', true);
        $data = array();
        $data['city_info'] = $this->db->query("SELECT * FROM country_cities WHERE `country_id`  = '$city_id'")->result_array();
        $this->load->view('institutions/city_list', $data);
    }

    public function edit($id=null)
    {
        if ($_POST) {
            $data = array();

            if ($_FILES["institution_logo_path"]['name'] != '') {
                //echo '<pre>';
                //print_r($_FILES["institution_logo_path"]);
                //die;
                $ext = explode('.',$_FILES["institution_logo_path"]['name']);
                $logo_name = 'ins_logo_'.time().'.' .end($ext);
                $config = array(
                    'upload_path' => "media/institute/",
                    'allowed_types' => "jpg|png|jpeg|JPEG|JPG|PNG",
                    'overwrite' => true,
                    'max_size' => "99999999999",
                    'max_height' => "3000",
                    'max_width' => "3000",
                    'file_name' => $logo_name
                );
                $this->load->library('upload', $config);

                if (!$this->upload->do_upload('institution_logo_path')) {
                    $sdata['exception'] = "Logo doesn't upload." . $this->upload->display_errors()." and max height: 3000px ,max width: 3000px";
                    $this->session->set_userdata($sdata);
                    redirect("institutions/index");
                }
            }
            //End Logo Upload

            //Banner upload
            if ($_FILES["banner_image"]['name'] != '') {
                $ext = explode('.',$_FILES["banner_image"]['name']);
                $banner_name = 'ins_banner_'.time().'.' .end($ext);
                $config = array(
                    'upload_path' => "media/institute/",
                    'allowed_types' => "jpg|png|jpeg|JPEG|JPG|PNG",
                    'overwrite' => true,
                    'max_size' => "99999999999",
                    'max_height' => "3000",
                    'max_width' => "3000",
                    'file_name' => $banner_name
                );
                $this->load->library('upload', $config);

                if (!$this->upload->do_upload('banner_image')) {
                    $sdata['exception'] = "Banner Image doesn't upload." . $this->upload->display_errors()." and max height: 3000px ,max width: 3000px";
                    $this->session->set_userdata($sdata);
                    redirect("institutions/index");
                }
            }
            //media image is_uploaded_file
            $media_type=$this->input->post('about_media_type', true);

            if (empty($media_type)) {
                $data['about_media_image']='';
                $data['about_media_vedio']='';
                $old_file = $this->input->post('about_media_image_file_name', true);
                if (!empty($old_file)) {
                    $filedel = PUBPATH . 'media/institute/' . $old_file;
                    unlink($filedel);
                }
            } else {
                if ((int)$media_type==1) {
                    if ($_FILES["about_media_image"]['name'] != '') {
                        $old_file = $this->input->post('about_media_image_file_name', true);
                        if (!empty($old_file)) {
                            $filedel = PUBPATH . 'media/institute/' . $old_file;
                            unlink($filedel);
                        }
                        $ext = explode('.',$_FILES["about_media_image"]['name']);
                        $about_media_image = 'about_media_'.time().'.' .end($ext);
                        $config = array(
                          'upload_path' => "media/institute/",
                          'allowed_types' => "gif|jpg|png|jpeg|JPEG|JPG|PNG|GIF",
                          'overwrite' => true,
                          'max_size' => "99999999999",
                          'max_height' => "3000",
                          'max_width' => "3000",
                          'file_name' => $about_media_image
                      );
                        $this->load->library('upload', $config);

                        if (!$this->upload->do_upload('about_media_image')) {
                            $sdata['exception'] = "Media Image doesn't upload." . $this->upload->display_errors()." and max height: 3000px ,max width: 3000px";
                            $this->session->set_userdata($sdata);
                            redirect("institutions/index");
                        }
                        $data['about_media_image']=$about_media_image;
                        $data['about_media_vedio']='';
                    }
                } else {
                    $data['about_media_vedio']=$this->input->post('about_media_vedio', true);
                    $data['about_media_image']='';
                    $old_file = $this->input->post('about_media_image_file_name', true);
                    if (!empty($old_file)) {
                        $filedel = PUBPATH . 'media/institute/' . $old_file;
                        unlink($filedel);
                    }
                }
            }



            //End Banner Upload


            if ($_FILES["institution_logo_path"]['name'] != '') {
                $data['institution_logo_path'] = $logo_name;
            }
            if ($_FILES["banner_image"]['name'] != '') {
                $data['banner_image'] = $banner_name;
            }
            $data['id']=$this->input->post('id', true);
            $data['country_id'] = $this->input->post('country_id', true);
            $data['city_id'] = $this->input->post('city_id', true);
            $data['name'] = $this->input->post('name', true);
            $data['institution_estd_year'] = $this->input->post('institution_estd_year', true);
            $data['institution_type_id'] = $this->input->post('institution_type_id', true);
            $data['campus'] = $this->input->post('campus', true);
            $data['website'] = $this->input->post('website', true);
            $data['application_fee'] = $this->input->post('application_fee', true);
            $data['institutional_benefits'] = $this->input->post('institutional_benefits', true);
            $data['part_time_work_details'] = $this->input->post('part_time_work_details', true);
            $data['is_active'] = $this->input->post('is_active', true);
            $data['times_ranking'] = $this->input->post('times_ranking', true);
            $data['international'] = $this->input->post('international', true);
            //$data['satisfaction'] = $this->input->post('satisfaction', true);
            $data['about_media_type'] = $this->input->post('about_media_type', true);
            $data['about_quote'] = $this->input->post('about_quote', true);
            $data['location_address'] = $this->input->post('location_address', true);
            $data['financial_living_cost'] = $this->input->post('financial_living_cost', true);
            $data['yearly_cost_currency'] = $this->input->post('yearly_cost_currency', true);
            $data['average_undergraduate_tuition_currency'] = $this->input->post('average_undergraduate_tuition_currency', true);
            $data['average_postgraduate'] = $this->input->post('yearly_cost_currency', true);
            $data['campus_accommodation_currency'] = $this->input->post('campus_accommodation_currency', true);
            $data['financial_application_fees_currency'] = $this->input->post('financial_application_fees_currency', true);

            $data['financial_campus_accomodation'] = $this->input->post('financial_campus_accomodation', true);
            $data['financial_tution_udergrade'] = $this->input->post('financial_tution_udergrade', true);
            $data['financial_tution_postgrad'] = $this->input->post('financial_tution_postgrad', true);
            $data['location_map'] = $this->input->post('location_map', true);
            $data['scholarship'] = $this->input->post('scholarship', true);
            $data['university_foundation_campus'] = $this->input->post('university_foundation_campus', true);
            $data['pathway_foundation_campus_institution_id'] = $this->input->post('pathway_foundation_campus_institution_id', true);
            $data['pre_sessional_english'] = $this->input->post('pre_sessional_english', true);
            $data['ne_valid_agreement'] = $this->input->post('ne_valid_agreement', true);
            $data['application_method'] = $this->input->post('application_method', true);
            $data['inter_ranking'] = $this->input->post('inter_ranking', true);
            $data['phone'] = $this->input->post('phone', true);
            $data['email'] = $this->input->post('email', true);
            $data['international_contact_person'] = $this->input->post('international_contact_person', true);

            $id=$data['id'];

            $this->db->where('id', $data['id']);
            $this->db->update('institutions', $data);
            $query['valid']  = $this->input->post('valid_agreement', true);
            $this->db->delete('tbl_valid_agreement_details', array('institution_id' => $id));
            foreach ($query['valid'] as $key) {
                $valid_agree = array();
                if (!empty($key['active'])) {
                    $valid_agree['institution_id']=$id;
                    $valid_agree['valid_agreement_id']=(int)$key['valid_agreement_id'];
                    $valid_agree['percentage']=$key['percentage'];

                    $this->db->insert('tbl_valid_agreement_details', $valid_agree);
                    //echo $key['percentage'].'mmm'.$key['valid_agreement_id'].'pp'.$key['active'].'<br>';
                }
            }
            //  die();

            $sdata['message'] = "You are Successfully Updated Institutions Info !";
            $this->session->set_userdata($sdata);
            redirect("institutions/index");
        } else {
            $data = array();
            $data['title'] = 'Update Institutions Information';
            $data['heading_msg'] = "Update Institutions Information";
            $data['is_show_button'] = "index";
            $data['countries'] = $this->db->query("SELECT * FROM cc_countries")->result_array();
            $data['currencies'] = $this->db->query("SELECT * FROM cc_currencies")->result_array();

            $data['institution_types'] = $this->db->query("SELECT * FROM cc_institution_type")->result_array();
            $data['institutions'] = $this->db->query("SELECT * FROM institutions")->result_array();
            $data['institution'] = $this->Admin_login->get_institution_info_by_unit_id($id);
            //$data['valid_agreement_details']=$this->db->query("SELECT a.id,d.percentage FROM  tbl_valid_agreement  AS a
            //    LEFT JOIN tbl_valid_agreement_details  AS d ON a.id =d.valid_agreement_id
            //  WHERE d.institution_id='$id'")->result_array();
            //$data['valid_agreement']=$this->db->query("SELECT * FROM  tbl_valid_agreement")->result_array();



            $data['valid_agreement']  =$this->db->query("SELECT  a.id,a.name,1 active,d.percentage FROM tbl_valid_agreement AS a
                                    INNER JOIN tbl_valid_agreement_details AS d ON a.id =d.valid_agreement_id
                                    WHERE d.institution_id='$id'
                                    UNION ALL
                                    SELECT a.id,a.name,0 active,'' percentage FROM tbl_valid_agreement AS a
                                    WHERE a.id NOT IN(
                                    SELECT  a.id FROM tbl_valid_agreement AS a
                                    INNER JOIN tbl_valid_agreement_details AS d ON a.id =d.valid_agreement_id
                                    WHERE d.institution_id='$id')")->result_array();
            $city_id = $data['institution']->city_id;
            $data['country_cities'] = $this->db->query("SELECT * FROM country_cities WHERE id = '$city_id'")->result_array();
            //echo $city_id;

            //  die;
            $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
            $data['maincontent'] = $this->load->view('institutions/edit', $data, true);
            $this->load->view('admin_logins/index', $data);
        }
    }

    public function delete($id)
    {
        $this->db->delete('institutions', array('id' => $id));
        $this->db->delete('tbl_valid_agreement_details', array('institution_id' => $id));
        $sdata['message'] = "institutions Information Deleted Successfully !";
        $this->session->set_userdata($sdata);
        redirect("institutions/index");
    }
    public function view($id=null)
    {
            $data = array();
            $data['title'] = 'View Institutions Information';
            $data['heading_msg'] = "View Institutions Information";
            $data['is_show_button'] = "index";
            $data['countries'] = $this->db->query("SELECT * FROM cc_countries")->result_array();
            $data['currencies'] = $this->db->query("SELECT * FROM cc_currencies")->result_array();

            $data['institution_types'] = $this->db->query("SELECT * FROM cc_institution_type")->result_array();
            $data['institutions'] = $this->db->query("SELECT * FROM institutions")->result_array();
            $data['institution'] = $this->Admin_login->get_institution_info_by_unit_id($id);

            $data['valid_agreement']  =$this->db->query("SELECT  a.id,a.name,1 active,d.percentage FROM tbl_valid_agreement AS a
                                    INNER JOIN tbl_valid_agreement_details AS d ON a.id =d.valid_agreement_id
                                    WHERE d.institution_id='$id'
                                    UNION ALL
                                    SELECT a.id,a.name,0 active,'' percentage FROM tbl_valid_agreement AS a
                                    WHERE a.id NOT IN(
                                    SELECT  a.id FROM tbl_valid_agreement AS a
                                    INNER JOIN tbl_valid_agreement_details AS d ON a.id =d.valid_agreement_id
                                    WHERE d.institution_id='$id')")->result_array();
            $city_id = $data['institution']->city_id;
            $data['country_cities'] = $this->db->query("SELECT * FROM country_cities WHERE id = '$city_id'")->result_array();

            $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
            $data['maincontent'] = $this->load->view('institutions/view', $data, true);
            $this->load->view('admin_logins/index', $data);

    }

}
