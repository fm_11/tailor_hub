<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Process_Report extends CI_Controller
{
public $notification = array();
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login','Employee'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        //set timezone
        date_default_timezone_set($user_info[0]->time_zone);
        $employee_id =  $user_info[0]->employee_id;
        //echo $employee_id; die;
        $this->notification = $this->Admin_login->get_notification($employee_id);
    }

    public function index()
    {
      $data = array();
      $user_info = $this->session->userdata('user_info');
      $employee_id =  $user_info[0]->employee_id;
      $user_id =$user_info[0]->id;
      if($_POST){
        $userid = $this->input->post('user_id', true);
        $from_date = $this->input->post('from_date', true);
        $to_date = $this->input->post('to_date', true);
        $f_date = strtotime($from_date);
        $t_date = strtotime($to_date);
        $datediff =  $t_date-$f_date;
        $data['from_date']=$from_date;
        $data['to_date']=$to_date;
        $data['employee_info']=$this->Admin_login->get_user_common_info($userid);
        $data['total_day'] =round($datediff / (60 * 60 * 24))+1;
        $data['title'] = 'Process Report';
        $data['heading_msg'] = "Process Report";
        $data['report_to']=$this->Admin_login->get_user_head_of_admission_designation($userid);
        $data['approved_by']=$this->Admin_login->get_user_head_of_process_designation($userid);
        $data['printed_by']=$this->Employee->getEmployeeName($employee_id);
        $data['rData'] = $this->Admin_login->get_student_applied_university_process_report_data($userid,$from_date,$to_date);
        $data['status_Data'] = $this->Admin_login->get_student_status_process_report_data($userid,$from_date,$to_date);
        if(empty($data['rData']) && empty($data['status_Data'])){
          $sdata['exception'] = "Data not found !";
          $this->session->set_userdata($sdata);
          redirect("process_report/index");
        }
        $data['org_info'] = $this->Admin_login->getReportHeaderAddress($data['employee_info'][0]['branch_id']);
        $excel = $this->input->post('excel', true);
      //  echo $excel; die;
        if(isset($excel) && $excel != ''){
          $data['excel'] = 1;
          $this->load->view('process_report/process_report_table', $data);
          //die;
        }else{
          $data['report'] = $this->load->view('process_report/process_report_table', $data, true);
        }
      }

      if(!isset($excel) || $excel == ''){
        $data['title'] = 'Process Report';
        $data['heading_msg'] = "Process Report";
        $user_type=$this->Admin_login->get_user_type($user_id);
        if($user_type=="hop")
        {
            $data['branches'] = $this->Admin_login->get_all_branch_list();
            //empty load
            $data['employees'] = $this->Admin_login->get_employee_list_by_employeeid(0);
        }elseif($user_type=="hoa")
        {
          $data['employees'] = $this->Admin_login->get_employee_list_by_employeeid($employee_id);
        }

        $data['user_ids']=$user_id;
        $data['user_type'] =$user_type;
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('process_report/index', $data, true);
        $this->load->view('admin_logins/index', $data);
      }
    }
    public function getEmployeeByBrachId()
    {
        $branch_id = $this->input->get('branch_id', true);

        $data = array();
        $data['employees'] = $this->db->query("SELECT u.`id`,e.name FROM `tbl_employee` AS e
                            INNER JOIN tbl_user AS u ON e.id=u.`employee_id` WHERE e.branch_id ='$branch_id'
                            ORDER BY e.name")->result_array();
        $this->load->view('lead_followup_report/employee_list', $data);
    }


}
