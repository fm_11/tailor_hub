<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Agent_Dashboard extends CI_Controller
{
    public $notification = array();
    public function __construct()
    {
        parent::__construct();
        $data = array();
        $this->load->helper(array('form', 'url'));
        $this->load->model(array('Timekeeping','Employee','Admin_login', 'common/insert_model', 'common/edit_model', 'common/custom_methods_model'));
        $this->load->library(array('session'));

        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("agent_login/index");
        }

        //set timezone
        //date_default_timezone_set($user_info[0]->time_zone);

       $employee_id =  $user_info[0]->employee_id;
        //echo $employee_id; die;
        $this->db->query('SET SESSION sql_mode = ""');
        $this->notification = $this->Admin_login->get_notification(0);
    }


    public function index()
    {

        $module_id = $this->session->userdata('module_id');
        if ($module_id == '') {
            $sdata['exception'] = "You do not have any module.";
            $this->session->set_userdata($sdata);
            redirect("agent_login/index");
        }

        $module_short_name = $this->session->userdata('module_short_name');
        $data = array();
        $date = date('Y-m-d');
        $session_user = $this->session->userdata('user_info');
      //  $employee_id =  $session_user[0]->employee_id;
        $user_id =  $session_user[0]->id;

        $data['title'] = 'Dashboard';
        $data['heading_msg'] = 'Dashboard';
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('agent_dashboard/index', $data, true);
        $this->load->view('admin_logins/index', $data);
      //  redirect("agent_dashboard/agent_dashboard");
    }




    public function todays_present_employee()
    {
        $data = array();
        $date = date('Y-m-d');
        $para = array();
        $para['date'] = $date;
        $data['present_data'] = $this->Timekeeping->get_all_employee_timekeeping_list(0, 0, $para);
        $data['title'] = 'Todays Present Employee (' . $date . ')';
        $data['heading_msg'] = 'Todays Present Employee (' . $date . ')';
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('dashboard/todays_present_employee', $data, true);
        $this->load->view('admin_logins/index', $data);
    }

    public function todays_absent_employee()
    {
        $data = array();
        $date = date('Y-m-d');
        $branch_id = '';
        $data['absentee_info'] =  $this->Timekeeping->get_employee_report_absentee($date, $branch_id);
        //  echo '<pre>';
        //  print_r($data['absentee_info']);
        //  die;
        $data['title'] = 'Todays Absent Employee (' . $date . ')';
        $data['heading_msg'] = 'Todays Absent Employee (' . $date . ')';
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('dashboard/todays_absent_employee', $data, true);
        $this->load->view('admin_logins/index', $data);
    }
}
