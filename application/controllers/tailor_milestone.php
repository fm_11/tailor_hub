<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Tailor_milestone extends CI_Controller
{
    public function __construct()
    {
      parent::__construct();
      $this->load->database();
      $this->load->model(array('Admin_login','Email_process','Config_general','Tailor'));
      $this->load->library('session');
      $user_info = $this->session->userdata('user_info');
      if (empty($user_info)) {
          $sdata = array();
          $sdata['message'] = "Please Login Vaild User !";
          $this->session->set_userdata($sdata);
          redirect("login/index");
      }
      $this->db->query('SET SESSION sql_mode = ""');
      //set timezone
      date_default_timezone_set($user_info[0]->time_zone);

      $employee_id =  $user_info[0]->employee_id;
      $this->notification = $this->Admin_login->get_notification($employee_id);
    }

    public function index()
    {
        $data = array();
        $data['title'] = 'Milestone';
        $data['heading_msg'] = 'Milestone';
        $data['milestone'] = $this->Admin_login->get_milestone_info();
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('tailor_milestone/index', $data, true);
        $this->load->view('admin_logins/index', $data);
    }

    public function update()
    {
        if($_POST){
          $data = array();
          $data['id'] = $this->input->post('id', true);
          $data['first_number'] = $this->input->post('first_number', true);
          $data['first_label'] = $this->input->post('first_label', true);
          $data['second_number'] = $this->input->post('second_number', true);
          $data['second_label'] = $this->input->post('second_label', true);
          $data['third_number'] = $this->input->post('third_number', true);
          $data['third_label'] = $this->input->post('third_label', true);
          $data['fourth_number'] = $this->input->post('fourth_number', true);
          $data['fourth_label'] = $this->input->post('fourth_label', true);
          $this->Admin_login->update_milestone_info($data);
          $sdata['message'] = 'Successfully Added Milestone Information';
          $this->session->set_userdata($sdata);
          redirect("tailor_milestone/index");
        }
    }
}
