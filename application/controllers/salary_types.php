<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Salary_types extends CI_Controller
{
public $notification = array();
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login','Salary_type'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $employee_id =  $user_info[0]->employee_id;
        //echo $employee_id; die;
        $this->notification = $this->Admin_login->get_notification($employee_id);
    }

    public function index()
    {
      $data = array();
      $data['title'] = 'Salary Type';
      $data['heading_msg'] = "Salary Type";
      $data['is_show_button'] = "add";
      $data['salary_types'] = $this->Salary_type->get_all_salary_type();
      $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
      $data['maincontent'] = $this->load->view('salary_types/index', $data, true);
      $this->load->view('admin_logins/index', $data);
    }


    function add()
    {
      if($_POST){
        $data = array();
        $data['name'] = $this->input->post('name', true);
        $data['short_name'] = $this->input->post('short_name', true);
        $data['type'] = $this->input->post('type', true);
        $data['percentage_of_gross'] = $this->input->post('percentage_of_gross', true);
        $data['rounding_type'] = $this->input->post('rounding_type', true);
        $data['report_order'] = $this->input->post('report_order', true);

        $this->db->insert('tbl_salary_types', $data);
        $sdata['message'] = "You are Successfully Added Saalry Type";
        $this->session->set_userdata($sdata);
        redirect("salary_types/index");
      }else{
        $data = array();
        $data['title'] = 'Salary Type';
        $data['heading_msg'] = "Add Salary Type";
        $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('salary_types/add', $data, true);
        $this->load->view('admin_logins/index', $data);
      }
  }

  function edit($id=null)
  {
    if($_POST){
      $data = array();
      $data['id'] = $this->input->post('id', true);
      $data['name'] = $this->input->post('name', true);
      $data['short_name'] = $this->input->post('short_name', true);
      $data['type'] = $this->input->post('type', true);
      $data['percentage_of_gross'] = $this->input->post('percentage_of_gross', true);
      $data['rounding_type'] = $this->input->post('rounding_type', true);
      $data['report_order'] = $this->input->post('report_order', true);
      $this->db->where('id', $data['id']);
      $this->db->update('tbl_salary_types', $data);
      $sdata['message'] = "You are Successfully Updated Salary Type";
      $this->session->set_userdata($sdata);
      redirect("salary_types/index");
    }else{
      $data = array();
      $data['title'] = 'Salary Type';
      $data['heading_msg'] = "Update Salary Type";
      $data['is_show_button'] = "index";
      $data['salary_type_info'] = $this->Salary_type->get_salary_type_by_id($id);
      $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
      $data['maincontent'] = $this->load->view('salary_types/edit', $data, true);
      $this->load->view('admin_logins/index', $data);
    }
}

  function delete($id)
  {
      $this->db->delete('tbl_salary_types', array('id' => $id));
      $sdata['message'] = "Salary Type Deleted Successfully !";
      $this->session->set_userdata($sdata);
      redirect("salary_types/index");
  }

}
