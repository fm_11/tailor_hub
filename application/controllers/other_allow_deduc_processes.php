<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Other_allow_deduc_processes extends CI_Controller
{
public $notification = array();
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login','Salary_type'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $employee_id =  $user_info[0]->employee_id;
        //echo $employee_id; die;
        $this->notification = $this->Admin_login->get_notification($employee_id);
    }

    public function index()
    {
      $data = array();
      $data['title'] = 'Other Allowance/Deducation Process';
      $data['heading_msg'] = "Other Allowance/Deducation Process";
      $data['is_show_button'] = "add";
      $data['other_allow_deduc_types'] = $this->Salary_type->get_all_other_allow_deduc_type();
      $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
      $data['maincontent'] = $this->load->view('other_allow_deduc_types/index', $data, true);
      $this->load->view('admin_logins/index', $data);
    }
}