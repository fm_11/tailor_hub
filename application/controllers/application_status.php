<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Application_status extends CI_Controller
{
    public $notification = array();

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login','Email_process','Employee'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $employee_id = $user_info[0]->employee_id;
        //echo $employee_id; die;
        $this->notification = $this->Admin_login->get_notification($employee_id);
        $this->db->query('SET SESSION sql_mode = ""');
    }


    public function index()
    {
        $cond = array();
        if($_POST){
          $sdata['name'] = $this->input->post('name', true);
          $cond['name'] = $this->input->post('name', true);
          $sdata['country_id'] = $this->input->post('country_id', true);
          $cond['country_id'] = $this->input->post('country_id', true);
          $sdata['institutions_id'] = $this->input->post('institutions_id', true);
          $cond['institutions_id'] = $this->input->post('institutions_id', true);
          $sdata['courses_id'] = $this->input->post('courses_id', true);
          $cond['courses_id'] = $this->input->post('courses_id', true);
          $sdata['application_status_id'] = $this->input->post('application_status_id', true);
          $cond['application_status_id'] = $this->input->post('application_status_id', true);
          $sdata['intake_year_id'] = $this->input->post('intake_year_id', true);
          $cond['intake_year_id'] = $this->input->post('intake_year_id', true);
          $sdata['intake_month_id'] = $this->input->post('intake_month_id', true);
          $cond['intake_month_id'] = $this->input->post('intake_month_id', true);
          $sdata['admission_officer'] = $this->input->post('admission_officer', true);
          $cond['admission_officer'] = $this->input->post('admission_officer', true);
          $this->session->set_userdata($sdata);

        }else{

          $cond['name'] = $this->session->userdata('name');
          $cond['country_id'] = $this->session->userdata('country_id');
          $cond['institutions_id'] = $this->session->userdata('institutions_id');
          $cond['courses_id'] = $this->session->userdata('courses_id');
          $cond['application_status_id'] = $this->session->userdata('application_status_id');
          $cond['intake_year_id'] = $this->session->userdata('intake_year_id');
          $cond['intake_month_id'] = $this->session->userdata('intake_month_id');
          $cond['admission_officer'] = $this->input->post('admission_officer', true);
        }
        $cond['application_status'] = "A";
        $data = array();
        $data['title'] = 'Applicantion List';
        $data['heading_msg'] = "Applicantion List";

        $user_info = $this->session->userdata('user_info');
        $user_id = $user_info[0]->id;
        $is_head_of_process = $user_info[0]->is_head_of_process;


        if ($is_head_of_process == '0') {
            $cond['process_officer_id'] = $user_info[0]->id;
        }

        $this->load->library('pagination');
        $config['base_url'] = site_url('application_status/index/');
        $config['per_page'] = 10;
        $config['total_rows'] = count($this->Admin_login->get_application_list(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['applications'] = $this->Admin_login->get_application_list(10, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['admission_officer'] = $this->Admin_login->get_admission_officer_list($user_info[0]->id);
        $data['countries'] = $this->db->query("SELECT * FROM cc_countries")->result_array();
        $data['application_status'] = $this->db->query("SELECT * FROM tbl_application_status WHERE id != '1'")->result_array();
        $data['institutions'] = $this->db->query("SELECT * FROM institutions")->result_array();
        $data['courses'] = $this->db->query("SELECT * FROM courses")->result_array();
        $data['intake_year'] =$this->Admin_login->get_intake_years();
        $data['intake_month'] =$this->Admin_login->get_intake_months();
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('application_status/index', $data, true);
        $this->load->view('admin_logins/index', $data);
    }

    public function change_status($id=null)
    {
        if ($_POST) {
            $id = $this->input->post('id', true);
            $application_info = $this->db->query("SELECT id,current_status FROM university_application WHERE id = '$id'")->result_array();
            $user_info = $this->session->userdata('user_info');
            $user_id = $user_info[0]->id;


            $staus_file = "0";
            if (isset($_FILES['file_path']) && $_FILES['file_path']['name'] != '') {
                $staus_file = $this->my_file_upload('file_path', 'application_file');
                if ($staus_file=='0') {
                    $sdata['exception'] = "File doesn't upload." . $this->upload->display_errors().' and max height: 3000px ,max width: 3000px';
                    $this->session->set_userdata($sdata);
                    redirect("application_status/view_status/".$id);
                }
            }
            $application_id=$application_info[0]['id'];
            $new_status_id=$this->input->post('current_status_id', true);
            $count = $this->db->query("SELECT id FROM tbl_application_status_history
              WHERE application_id = '$application_id' and new_status_id='$new_status_id'")->num_rows();
            if($count>0)
            {
              $sdata['exception'] = "This Application Status Already Exits.";
              $this->session->set_userdata($sdata);
              redirect("application_status/change_status/".$id);
            }

            $application_status =$this->status_changes_email_send($id,$this->input->post('current_status_id', true),$staus_file,$this->input->post('remarks', true));
            $history_data = array();
            $history_data['application_id'] = $application_id;
            $history_data['old_status_id'] = $application_info[0]['current_status'];
            $history_data['new_status_id'] = $new_status_id;
            $history_data['remarks'] = $this->input->post('remarks', true);
            $history_data['user_id'] = $user_id;
            $history_data['file_location'] = $staus_file;
            $history_data['date_time'] = date('Y-m-d H:i:s');
            $this->db->insert('tbl_application_status_history', $history_data);

            $data = array();
            $data['id'] =  $id;
            $data['current_status'] = $this->input->post('current_status_id', true);
            $this->db->where('id', $data['id']);
            $this->db->update('university_application', $data);
            $status_mess=" and successfully email send.";
            if(!$application_status)
            {
                $status_mess=" and email cannot send.";
            }
            $sdata['message'] = "You are Successfully Change Application Status".$status_mess;
            $this->session->set_userdata($sdata);
            redirect("application_status/index");
        } else {
            $data['id'] = $id;
            $data['title'] = 'Change Application Status';
            $data['heading_msg'] = "Change Application Status";
            $data['is_show_button'] = "index";
            $data['application_status'] = $this->db->query("SELECT * FROM tbl_application_status WHERE id != '1'")->result_array();
            $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
            $data['maincontent'] = $this->load->view('application_status/application_status_change', $data, true);
            $this->load->view('admin_logins/index', $data);
        }
    }

    function status_changes_email_send($university_application_id,$status_id,$staus_file,$remarks){


     $info =$this->db->query(" SELECT i.`name` AS institution_name,CONCAT(a.`first_name`,' ',a.`last_name`) AS applicant_name,a.`applicant_code`
                             ,u.`application_code`,u.id,ag.`code` AS agent_code,ag.`personal_name` AS agent_name,ag.`email` AS agent_email,
                             a.`email` AS applicant_email,em.`email` AS admission_officer_email,em.`name` AS admission_officer_name,
                             co.`course_title`,m.`name` AS intake_month,y.`name` AS intake_year,em.`mobile` AS admission_officer_phone
                             FROM university_application AS u
                             LEFT JOIN `institutions` AS i ON u.`institution_id`=i.`id`
                             LEFT JOIN `applicants` AS a ON u.`applicant_id`=a.`id`
                             LEFT JOIN `agents` AS ag ON a.`agent_id`=ag.`id`
                             LEFT JOIN `tbl_user` AS  tu ON a.`admission_officer_id`=tu.`id`
                             LEFT JOIN `tbl_employee` AS em ON tu.`employee_id`=em.`id`
                             LEFT JOIN `courses` AS co ON u.`subject_id`=co.`id`
                             LEFT JOIN `cc_intake_months` AS m ON u.`intake_month`=m.`id`
                             LEFT JOIN `cc_intake_years` AS Y ON u.`intake_year` = y.`id`
                             WHERE u.`id`='$university_application_id'")->result_array();




        $application_status= $this->db->query("SELECT name FROM tbl_application_status WHERE id = '$status_id'")->result_array();
       //reciver
        $receivers = "";
         if(!empty($info[0]['admission_officer_email']))
         {
           $receivers=$info[0]['admission_officer_email'];
         }
         if(!empty($info[0]['applicant_email']))
         {
           $receivers=$receivers.','.$info[0]['applicant_email'];
         }
         if(!empty($info[0]['agent_email']))
         {
           $receivers=$receivers.','.$info[0]['agent_email'];
         }
         //subject
        $header = "[";
        if(!empty($info[0]['applicant_code']))
        {
          $header=$header.$info[0]['applicant_code'].']';
        }
        if(!empty($info[0]['agent_code']))
        {
          $header=$header.' ['.$info[0]['agent_code'].']';
        }
        if(!empty($info[0]['application_code']))
        {
          $header=$header.' ['.$info[0]['application_code'].']';
        }
        $header=$header.' - '.$application_status[0]['name'];
      //message
       $message = "";
       // if(!empty($info[0]['applicant_name']))
       // {
       //   $message=$info[0]['applicant_name'];
       // }
       // if(!empty($info[0]['agent_name']))
       // {
       //   $message=$message.' , '.$info[0]['agent_name'];
       // }
       // if(!empty($info[0]['admission_officer_name']))
       // {
       //   $message=$message.' , '.$info[0]['admission_officer_name'].']</br>';
       // }

       $message=$message.'<b>Student Id: </b>'.$info[0]['applicant_code'].'<br>';
      $message=$message.'<b>Application Id: </b>'.$info[0]['application_code'].'<br>';
       $message=$message.'<b>University Name: </b>'.$info[0]['institution_name'].'<br>';
       $message=$message.'<b>Subject : </b>'.$info[0]['course_title'].'<br>';
       $message=$message.'<b>Intake : </b>'.$info[0]['intake_month'].' , '.$info[0]['intake_year'].'<br>';
       $message=$message.'<b>Status : </b>'.$application_status[0]['name'].'<br>';


        $session_user = $this->session->userdata('user_info');
        $employee_id = $session_user[0]->employee_id;
        //   print_r($receivers);
        // print_r($header);
        //   print_r($message);
        //           die();
        $email_data = array();
        $email_data['receiver_name'] = $info[0]['applicant_name'];
        $employee_info = $this->Employee->getEmployeeName($employee_id);
        $email_data['sender_name'] = $employee_info[0]['name'] . '(' . $employee_info[0]['code'] . ')';
        $email_data['sender_designation_name'] = $employee_info[0]['designation_name'];
        $email_data['sender_branch_name'] = $employee_info[0]['branch_name'];
        $email_data['admission_name'] = $info[0]['admission_officer_name'];
        $email_data['admission_email'] = $info[0]['admission_officer_email'];
        $email_data['admission_mobile'] = $info[0]['admission_officer_phone'];
        $email_data['staus_file'] = $staus_file;
        $email_data['file_link'] = "http://achi365.com/spate-i/students_login";
// print_r($header);
// die;
        $email_data['title'] = $header;
        $email_data['details'] = $message;
        $email_data['status_submit'] = $remarks;//'Your '.$application_status[0]['name'].' to '.$info[0]['institution_name'].' at '.$info[0]['course_title'].' for '.$info[0]['intake_month'].' , '.$info[0]['intake_year'].' Intake.</br>';
        $email_message = $this->load->view('admin_logins/email_template2',$email_data,true);
    // print_r($this->load->view('admin_logins/email_template2',$email_data,true));
    // die();

        if($this->Email_process->send_email($receivers,$header,$email_message)){
           return 1;
        }else{
         return 0;
        }

    }
    public function update_interview_info($id=null)
    {
        if ($_POST) {
            $data = array();
            $data['id'] =  $this->input->post('id', true);
            $data['university_interview_date'] = $this->input->post('university_interview_date', true);
            $data['visa_appointment_date'] = $this->input->post('visa_appointment_date', true);
            $this->db->where('id', $data['id']);
            $this->db->update('university_application', $data);
            $sdata['message'] = "You are Successfully Update Interview Information";
            $this->session->set_userdata($sdata);
            redirect("application_status/index");
        } else {
            $data = array();
            $data['id'] = $id;
            $data['title'] = 'Update Interview Information';
            $data['heading_msg'] = "Update Interview Information";
            $data['is_show_button'] = "index";
            $data['application_info'] = $this->db->query("SELECT * FROM university_application WHERE id = '$id'")->result_array();
            $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
            $data['maincontent'] = $this->load->view('application_status/update_interview_info', $data, true);
            $this->load->view('admin_logins/index', $data);
        }
    }

    public function my_file_upload($filename, $type)
    {
      $ext = explode('.',$_FILES[$filename]['name']);
     $new_file_name = time().'_'. $type . '.' .end($ext);
        //  echo $new_file_name;
        // print_r($filename);
        //  die;
        $this->load->library('upload');

        $config = array(
                'upload_path' => "media/applicantion_document/",
                'allowed_types' => "jpg|png|jpeg|JPEG|JPG|PNG|pdf|csv|doc|docx",
                'max_size' => "99999999999",
                'max_height' => "3000",
                'max_width' => "3000",
                'file_name' => $new_file_name
            );

        $this->upload->initialize($config);

        if (!$this->upload->do_upload($filename)) {
            return '0';
        } else {
            return $new_file_name;
        }
    }

    public function view_status($id=null)
    {
      $user_info = $this->session->userdata('user_info');
      $user_id = $user_info[0]->id;
        if ($_POST) {
            $id = $this->input->post('id', true);
            $application_info = $this->db->query("SELECT id,current_status FROM university_application WHERE id = '$id'")->result_array();

            $staus_file = "0";
            if (isset($_FILES['file_path']) && $_FILES['file_path']['name'] != '') {
                $staus_file = $this->my_file_upload('file_path', 'application_file');
                if ($staus_file=='0') {
                    $sdata['exception'] = "File doesn't upload." . $this->upload->display_errors().' and max height: 3000px ,max width: 3000px';
                    $this->session->set_userdata($sdata);
                    redirect("application_status/view_status/".$id);
                }
            }
            $application_id=$application_info[0]['id'];
            $new_status_id=$this->input->post('current_status_id', true);
            $count = $this->db->query("SELECT id FROM tbl_application_status_history
              WHERE application_id = '$application_id' and new_status_id='$new_status_id'")->num_rows();
            if($count>0)
            {
              $sdata['exception'] = "This Application Status Already Exits.";
              $this->session->set_userdata($sdata);
              redirect("application_status/view_status/".$id);
            }

            $application_status =$this->status_changes_email_send($id,$this->input->post('current_status_id', true),$staus_file,$this->input->post('remarks', true));
            $history_data = array();
            $history_data['application_id'] = $application_info[0]['id'];
            $history_data['old_status_id'] = $application_info[0]['current_status'];
            $history_data['new_status_id'] = $this->input->post('current_status_id', true);
            $history_data['remarks'] = $this->input->post('remarks', true);
            $history_data['user_id'] = $user_id;
            $history_data['file_location'] = $staus_file;
            $history_data['date_time'] = date('Y-m-d H:i:s');
            $this->db->insert('tbl_application_status_history', $history_data);

            $data = array();
            $data['id'] =  $id;
            $data['current_status'] = $this->input->post('current_status_id', true);
            $this->db->where('id', $data['id']);
            $this->db->update('university_application', $data);
            $status_mess=" and successfully email send.";
            if(!$application_status)
            {
                $status_mess=" and email cannot send.";
            }
            $sdata['message'] = "You are Successfully Change Application Status".$status_mess;
            $this->session->set_userdata($sdata);
            redirect("application_status/view_status/".$id);
        } else {
            $data['id'] = $id;
            $data['title'] = 'View Application Status';
            $data['heading_msg'] = "View Application Status";
            $data['is_show_button'] = "index";
            $data['applicants']=$this->Admin_login->get_university_applicant_details($id);

            $data['application_status_view']=$this->Admin_login->get_university_application_status($id,$user_id);
            $data['application_status'] = $this->db->query("SELECT * FROM tbl_application_status")->result_array();
            $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
            $data['maincontent'] = $this->load->view('application_status/view_status', $data, true);
            $this->load->view('admin_logins/index', $data);
        }
    }

    public function view($id)
    {
        $data = array();
        $data['title'] = 'View Applicants';
        $data['heading_msg'] = "View Applicants";
        $data['is_show_button'] = "index";
        $data['marital_status'] = $this->db->query("SELECT * FROM cc_marital_status")->result_array();
        $data['genders'] = $this->db->query("SELECT * FROM cc_genders")->result_array();
        $data['tbl_employee'] = $this->db->query("SELECT u.id ,e.name FROM tbl_employee as e inner join tbl_user as u on e.id=u.employee_id")->result_array();
        $data['countries'] = $this->db->query("SELECT * FROM cc_countries")->result_array();
        $data['country_cities'] = $this->db->query("SELECT * FROM country_cities")->result_array();
        $data['course_level'] = $this->db->query("SELECT * FROM cc_course_level")->result_array();
        $data['lead_source'] = $this->db->query("SELECT * FROM cc_lead_sources")->result_array();
        $data['employee_post'] = $this->db->query("SELECT * FROM tbl_employee_post")->result_array();

        $data['applicant_info'] = $this->Admin_login->get_applicants_info_by_unit_id($id);
        $data['applicant_education'] = $this->Admin_login->get_educaton_list_by_applicant_id($id);
        $data['applicant_language'] = $this->db->query("SELECT * FROM applicant_english_language WHERE applicant_id = '$id'")->result_array();
        $data['applicant_experience'] = $this->db->query("SELECT e.*,c.name as country_name FROM applicant_work_experience AS e
                                  LEFT JOIN cc_countries as c ON c.id = e.country_id
                                  WHERE e.applicant_id = '$id'")->result_array();

        $data['origin_office'] = $this->db->query("SELECT * FROM tbl_branch")->result_array();
        $data['university_application'] =$this->Admin_login->get_university_by_applicant_id($id);

        $data['application_status']=$this->db->query("SELECT
                          (SELECT `name` FROM tbl_application_status WHERE id=h.old_status_id) AS old_status ,
                          (SELECT `name` FROM tbl_application_status WHERE id=h.new_status_id) AS new_status ,
                          date_time,remarks,file_location
                          FROM tbl_application_status_history AS h WHERE h.application_id='$id'
                          ORDER BY date_time")->result_array();

        $data['applicant_reference'] = $this->db->query("SELECT r.*,c.name AS country_name FROM applicant_reference AS r LEFT JOIN cc_countries AS c ON r.ref1_country_id =c.id WHERE r.applicant_id = '$id'")->result_array();
        $data['check_information']=$this->Admin_login->check_information_update_by_applicant_id($id);
        //echo '<pre>';
        //print_r($data['applicant_info']);
        //die;
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('application_status/view', $data, true);
        $this->load->view('admin_logins/index', $data);
    }
}
