<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Applications extends CI_Controller
{
    public $notification = array();

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login','common/insert_model', 'common/edit_model', 'common/custom_methods_model'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $employee_id = $user_info[0]->employee_id;
        //echo $employee_id; die;
        $this->notification = $this->Admin_login->get_notification($employee_id);
        $this->db->query('SET SESSION sql_mode = ""');
    }



    public function index()
    {
        $data = array();
        $data['title'] = 'Applicantion List';
        $data['heading_msg'] = "Applicantion List";
        $cond = array();
        $cond['application_status'] = "P";
        $this->load->library('pagination');
        $config['base_url'] = site_url('applications/index/');
        $config['per_page'] = 10;
        $config['total_rows'] = count($this->Admin_login->get_application_list(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['applications'] = $this->Admin_login->get_application_list(10, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        //$data['is_show_button'] = "add";
        $data['branches'] = $this->db->query("SELECT * FROM leads")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('applications/index', $data, true);
        $this->load->view('admin_logins/index', $data);
    }

    public function assign_process_officer($id=null)
    {
        $data = array();
        if ($_POST) {
            $process_officer_id = $this->input->post('process_officer_id', true);
            $user_info_data = $this->custom_methods_model->select_row_by_condition("tbl_user", "employee_id='$process_officer_id'");
            if (empty($user_info_data)) {
                $sdata['exception'] = "No user was found for this employee";
                $this->session->set_userdata($sdata);
                redirect("applications/index");
            }
            $new_process_officer_user_id = $user_info_data[0]->id;

            $data = array();
            $data['id'] =  $this->input->post('id', true);
            $data['process_officer_id'] = $new_process_officer_user_id;
            $data['application_status'] = 'A';
            $this->db->where('id', $data['id']);
            $this->db->update('university_application', $data);

            $sdata['message'] = "You are Successfully Assign Application";
            $this->session->set_userdata($sdata);
            redirect("applications/index");
        }
        $data['id'] = $id;
        $data['title'] = 'Assign Concern Person';
        $data['heading_msg'] = "Assign Concern Person";
        $data['collection_officer'] = $this->db->query("SELECT id,name,code FROM tbl_employee WHERE `status` = '1'")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('applications/assign_process_officer', $data, true);
        $this->load->view('admin_logins/index', $data);
    }
}
