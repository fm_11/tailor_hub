<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Agent_Login extends CI_Controller {

    function __construct() {

        parent::__construct();
        $data = array();
        $this->load->helper(array('form', 'url'));
        $this->load->model(array('Admin_login','common/insert_model', 'common/edit_model', 'common/custom_methods_model','Agent_portal'));
        $this->load->library(array('session'));

        $user_info = $this->session->userdata('user_info');
        if(!empty($user_info)){
          $employee_id =  $user_info[0]->employee_id;
          //echo $employee_id; die;
          $this->notification = $this->Admin_login->get_notification(0);
        }
       $this->db->query('SET SESSION sql_mode = ""');
    }

    public function index() {

        $data = array();
        $this->load->view('agent_login/index', $data);
    }

    public function authentication() {

        $user_name = $this->input->post("user_name");
        $password = $this->input->post("password");
		//echo $user_name.'/'.$password; die;
        $password = md5($password);
        $is_authenticated = $this->custom_methods_model->num_of_data("agents", "WHERE email='$user_name' AND PASSWORD='$password'");
        if ($is_authenticated > 0) {
            $user_info = $this->custom_methods_model->select_row_by_condition("agents", "email='$user_name' AND PASSWORD='$password'");
        if($user_info[0]->status){
          //  $employee_info = $this->custom_methods_model->select_row_by_condition("tbl_employee", "id='$employee_id'");
            if($user_info[0]->logo == '' || $user_info[0]->logo == '0'){
             $user_info[0]->photo_location = 'profile-pic.png';
             }else{
                $user_info[0]->photo_location = $user_info[0]->logo;
            }
            // print_r($user_info[0]->photo_location);
            // die();
            $user_info[0]->name =  $user_info[0]->personal_name;
            $user_info[0]->is_agent =  '1';
            $user_info[0]->employee_id=$user_info[0]->id;

           //set time_zone
           // $branch_id =   $employee_info[0]->branch_id;
           // $branch_info = $this->db->query("SELECT t.value as time_zone FROM tbl_branch as b
           //                INNER JOIN tbl_timezone as t ON t.id = b.time_zone_id
           //                WHERE b.id = '$branch_id'")->result_array();
           // $user_info[0]->time_zone =  $branch_info[0]['time_zone'];

            $this->session->set_userdata('user_info', $user_info);

            //for module select
            $arraydata = array(
               'module_id'  => 9,
               'main_menu_file'     => "agent_main_menu",
               'module_short_name'     => "agent"
             );
            $this->session->set_userdata($arraydata);

            redirect("agent_dashboard/index");
          }else{
            $sdata['exception'] = "Your account has been deactivated.!";
            $this->session->set_userdata($sdata);
            redirect("agent_login/index");
          }
        } else {
            $sdata['exception'] = "User Name or Password did not match!";
            $this->session->set_userdata($sdata);
            redirect("agent_login/index");
        }
    }

	function getSchoolData($postdata, $api)
    {
        $opts = array('http' =>
            array(
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => $postdata
            )
        );
        $context = stream_context_create($opts);
        $result = file_get_contents("$api", false, $context);
        return json_decode($result);

    }

    public function logout() {
        $this->session->unset_userdata('user_info');
        $sdata['message'] = "You are logged out";
        $this->session->set_userdata($sdata);
        redirect("agent_login/index");
    }

    public function denied() {
        $data['title'] = 'Access denied';
        $data['heading_msg'] = 'Access denied';
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('agent_dashboard/denied', '', true);
        $this->load->view('admin_logins/index', $data);
    }


	public function change_password($user_id = null){
		if($_POST){
		   $user_id = $this->input->post("user_id");
		   $password = $this->input->post("current_pass");
           $password = md5($password);
		   $is_authenticated = $this->custom_methods_model->num_of_data("agents", "WHERE id='$user_id' AND PASSWORD='$password'");
		   if ($is_authenticated > 0) {
               $data = array();
			   $data['PASSWORD'] =   md5($this->input->post("new_pass"));
			   $data['id'] = $user_id;
			   $this->db->where('id', $data['id']);
               $this->db->update('agents', $data);
			   $sdata['message'] = "You are Successfully Password Changes.";
               $this->session->set_userdata($sdata);
               redirect("agent_login/change_password/".$user_id);
			}else {
				$sdata['exception'] = "Sorry Current Password Doesn't Match !";
				$this->session->set_userdata($sdata);
				redirect("agent_login/change_password/".$user_id);
			}
		}else{
			      $data['title'] = 'Change Password';
            $data['heading_msg'] = "Change Password";
            $data['user_id'] = $user_id;
            $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
            $data['maincontent'] = $this->load->view('agent_login/change_password', $data, true);
            $this->load->view('admin_logins/index', $data);
		}
	}
  public function myprofile()
  {
      $session_user = $this->session->userdata('user_info');
      $id =  $session_user[0]->id;
      $data = array();
      $data['title'] = 'My Profile';
      $data['heading_msg'] = "My Profile";
      $data['is_show_button'] = "index";
      $data['employees'] = $this->db->query("SELECT * FROM tbl_employee")->result_array();
      $data['agents'] = $this->Agent_portal->get_agents_info_by_unit_id($id);
      $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
      $data['maincontent'] = $this->load->view('agent_login/myprofile', $data, true);
      $this->load->view('admin_logins/index', $data);
  }
  public function contact()
  {
      $session_user = $this->session->userdata('user_info');
      $id =  $session_user[0]->id;
      $data = array();
      $data['title'] = 'Admission Officer Details';
      $data['heading_msg'] = "Admission Officer Details";
      $data['is_show_button'] = "index";
      $data['admission_officer'] = $this->Agent_portal->get_admission_officer_details($id);
      $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
      $data['maincontent'] = $this->load->view('agent_login/contact', $data, true);
      $this->load->view('admin_logins/index', $data);
  }
}
