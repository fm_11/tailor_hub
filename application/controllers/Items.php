<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Items extends CI_Controller
{
    public $notification = array();

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login','Employee','Email_process','common/insert_model', 'common/edit_model', 'common/custom_methods_model'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $employee_id = $user_info[0]->employee_id;
        //echo $employee_id; die;
        $this->notification = $this->Admin_login->get_notification($employee_id);
        $this->db->query('SET SESSION sql_mode = ""');
    }

    public function index()
    {
      $cond = array();
      if($_POST){
        $sdata['name'] = $this->input->post('name', true);
        $cond['name'] = $this->input->post('name', true);
        $this->session->set_userdata($sdata);

      }else{

        $cond['name'] = $this->session->userdata('name');

      }

        $data = array();
        $data['title'] = 'Item';
        $data['heading_msg'] = "Item";
        $data['is_show_button'] = "add";
        $this->load->library('pagination');
        $config['base_url'] = site_url('items/index/');
        $config['per_page'] = 10;
        $config['total_rows'] = count($this->Admin_login->get_tailor_items_info(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['items'] = $this->Admin_login->get_tailor_items_info(10, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('items/index', $data, true);
        $this->load->view('admin_logins/index', $data);
    }
    public function add()
    {
        if ($_POST) {
          $photo_name1 = "";$photo_name2="";$photo_name3="";$photo_name4="";$photo_name5="";
          $file1 = $_FILES["txtPhoto1"]['name'];
          $file2 = $_FILES["txtPhoto2"]['name'];
          $file3 = $_FILES["txtPhoto3"]['name'];
          $file4 = $_FILES["txtPhoto4"]['name'];
          $file5 = $_FILES["txtPhoto5"]['name'];
          if ($this->Admin_login->check_ifexist_tailor_item($this->input->post('name', true))) {
              $sdata['exception'] = "This item name already exist.";
              $this->session->set_userdata($sdata);
              redirect("items/add/");
          }
          if($file1!='')
          {
            $photo_name1=$this->my_file_upload("txtPhoto1","image_1_");
            if($photo_name1=='0')
            {
              $sdata['exception'] = "Image 1  doesn't upload." . $this->upload->display_errors().' and max height: 3000px ,max width: 3000px';
              $this->session->set_userdata($sdata);
              redirect('items/add');
            }
          }
          if($file2!='')
          {
            $photo_name2=$this->my_file_upload("txtPhoto2","image_2_");
            if($photo_name2=='0')
            {
              $sdata['exception'] = "Image 12 doesn't upload." . $this->upload->display_errors().' and max height: 3000px ,max width: 3000px';
              $this->session->set_userdata($sdata);
              redirect('items/add');
            }
          }

          if($file3!='')
          {
            $photo_name3=$this->my_file_upload("txtPhoto3","image_3_");
            if($photo_name3=='0')
            {
              $sdata['exception'] = "Image 3 doesn't upload." . $this->upload->display_errors().' and max height: 3000px ,max width: 3000px';
              $this->session->set_userdata($sdata);
              redirect('items/add');
            }
          }

          if($file4!='')
          {
            $photo_name4=$this->my_file_upload("txtPhoto4","image_4_");
            if($photo_name4=='0')
            {
              $sdata['exception'] = "Image 4 doesn't upload." . $this->upload->display_errors().' and max height: 3000px ,max width: 3000px';
              $this->session->set_userdata($sdata);
              redirect('items/add');
            }
          }
          if($file5!='')
          {
            $photo_name5=$this->my_file_upload("txtPhoto5","image_5_");
            if($photo_name5=='0')
            {
              $sdata['exception'] = "Image 5 doesn't upload." . $this->upload->display_errors().' and max height: 3000px ,max width: 3000px';
              $this->session->set_userdata($sdata);
              redirect('items/add');
            }
          }
             $data = array();
            $data['name'] = $_POST['name'];
            $data['measurement_level_1'] = $_POST['measurement_level_1'];
            $data['measurement_level_2'] = $_POST['measurement_level_2'];
            $data['measurement_level_3'] = $_POST['measurement_level_3'];
            $data['measurement_level_4'] = $_POST['measurement_level_4'];
            $data['measurement_level_5'] = $_POST['measurement_level_5'];
            $data['measurement_level_6'] = $_POST['measurement_level_6'];
            $data['image_path_1'] = $photo_name1;
            $data['image_path_2'] = $photo_name2;
            $data['image_path_3'] = $photo_name3;
            $data['image_path_4'] = $photo_name4;
            $data['image_path_5'] = $photo_name5;
            $this->db->insert('tbl_tailor_item', $data);
            $sdata['message'] = "You are Successfully Added Item Info !";


            $this->session->set_userdata($sdata);
            redirect('items/add');
        }
        $data = array();
        $data['title'] = 'Add Item';
        $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('items/add', $data, true);
        $this->load->view('admin_logins/index', $data);
    }


    public function edit($id=null)
    {
        if ($_POST) {
            $data = array();

            $photo_name1 = "";$photo_name2="";$photo_name3="";$photo_name4="";$photo_name5="";
            $file1 = $_FILES["txtPhoto1"]['name'];
            $file2 = $_FILES["txtPhoto2"]['name'];
            $file3 = $_FILES["txtPhoto3"]['name'];
            $file4 = $_FILES["txtPhoto4"]['name'];
            $file5 = $_FILES["txtPhoto5"]['name'];

            if ($this->Admin_login->check_ifexist_update_tailor_item($this->input->post('name', true),$id)) {
                $sdata['exception'] = "This item name already exist.";
                $this->session->set_userdata($sdata);
                redirect("items/edit/".$id);
            }
            if($file1!='')
            {
              $photo_name1=$this->my_file_upload("txtPhoto1","image_1_");
              if($photo_name1=='0')
              {
                $sdata['exception'] = "Image 1 doesn't upload." . $this->upload->display_errors().' and max height: 3000px ,max width: 3000px';
                $this->session->set_userdata($sdata);
                redirect('items/edit/'.$id);
              }
              $data['image_path_1'] = $photo_name1;
              $msg =$this->Admin_login->my_file_remove($_POST['oldtxtPhoto1'], $photo_name1, 'media/tailor_item/');

            }
            if($file2!='')
            {
              $photo_name2=$this->my_file_upload("txtPhoto2","image_2_");
              if($photo_name2=='0')
              {
                $sdata['exception'] = "Image 2 doesn't upload." . $this->upload->display_errors().' and max height: 3000px ,max width: 3000px';
                $this->session->set_userdata($sdata);
                redirect('items/edit/'.$id);
              }
              $data['image_path_2'] = $photo_name2;
              $msg =$this->Admin_login->my_file_remove($_POST['oldtxtPhoto2'], $photo_name2, 'media/tailor_item/');
            }

            if($file3!='')
            {
              $photo_name3=$this->my_file_upload("txtPhoto3","image_3_");
              if($photo_name3=='0')
              {
                $sdata['exception'] = "Image 3 doesn't upload." . $this->upload->display_errors().' and max height: 3000px ,max width: 3000px';
                $this->session->set_userdata($sdata);
                redirect('items/edit/'.$id);
              }
              $data['image_path_3'] = $photo_name3;
              $msg =$this->Admin_login->my_file_remove($_POST['oldtxtPhoto3'], $photo_name3, 'media/tailor_item/');
            }

            if($file4!='')
            {
              $photo_name4=$this->my_file_upload("txtPhoto4","image_4_");
              if($photo_name4=='0')
              {
                $sdata['exception'] = "Image 4 doesn't upload." . $this->upload->display_errors().' and max height: 3000px ,max width: 3000px';
                $this->session->set_userdata($sdata);
                  redirect('items/edit/'.$id);
              }
              $data['image_path_4'] = $photo_name4;
              $msg =$this->Admin_login->my_file_remove($_POST['oldtxtPhoto4'], $photo_name4, 'media/tailor_item/');
            }
            if($file5!='')
            {
              $photo_name5=$this->my_file_upload("txtPhoto5","image_5_");
              if($photo_name4=='0')
              {
                $sdata['exception'] = "Image 5 doesn't upload." . $this->upload->display_errors().' and max height: 3000px ,max width: 3000px';
                $this->session->set_userdata($sdata);
                  redirect('items/edit/'.$id);
              }
              $data['image_path_5'] = $photo_name5;
              $msg =$this->Admin_login->my_file_remove($_POST['oldtxtPhoto5'], $photo_name5, 'media/tailor_item/');
            }


            $data['id'] = $id;
            $data['name'] = $this->input->post('name', true);
            $data['measurement_level_1'] = $_POST['measurement_level_1'];
            $data['measurement_level_2'] = $_POST['measurement_level_2'];
            $data['measurement_level_3'] = $_POST['measurement_level_3'];
            $data['measurement_level_4'] = $_POST['measurement_level_4'];
            $data['measurement_level_5'] = $_POST['measurement_level_5'];
            $data['measurement_level_6'] = $_POST['measurement_level_6'];
            $this->db->where('id', $data['id']);
            $this->db->update('tbl_tailor_item', $data);
            $sdata['message'] = "You are Successfully Updated Items data";
            $this->session->set_userdata($sdata);
            redirect("items/index");
        } else {
            $data = array();
            $data['title'] = 'Update Items';
            $data['heading_msg'] = "Update Items";
            $data['is_show_button'] = "index";
            $data['items'] = $this->Admin_login->get_tailor_item_by_id($id);
            $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
            $data['maincontent'] = $this->load->view('items/edit', $data, true);
            $this->load->view('admin_logins/index', $data);
        }
    }


    public function my_file_upload($filename, $type)
    {
       $ext = explode('.',$_FILES[$filename]['name']);
       $new_file_name = $type.time().'.'.end($ext);

        // print_r(rand());
        // die();
        $this->load->library('upload');

        $config = array(
                'upload_path' => "media/tailor_item/",
                'allowed_types' => "jpg|png|jpeg|JPEG|JPG|PNG|pdf|csv|doc|docx",
                'max_size' => "99999999999",
                'max_height' => "3000",
                'max_width' => "3000",
                'file_name' => $new_file_name
            );

        $this->upload->initialize($config);
        if (!$this->upload->do_upload($filename)) {
            return '0';
        } else {
            return $new_file_name;
        }
    }


    public function delete($id)
    {
        $data = $this->db->where('id', $id)->get('tbl_tailor_item')->row();

        $image_1 =$data->image_path_1;
        $image_2 =$data->image_path_2;
        $image_3 =$data->image_path_3;
        $image_4 =$data->image_path_4;
        $image_5 =$data->image_path_5;

        if ($this->db->delete('tbl_tailor_item', array('id' => $id))) {
            $this->Admin_login->my_file_remove($image_1, 'test', 'media/tailor_item/');
            $this->Admin_login->my_file_remove($image_2, 'test', 'media/tailor_item/');
            $this->Admin_login->my_file_remove($image_3, 'test', 'media/tailor_item/');
            $this->Admin_login->my_file_remove($image_4, 'test', 'media/tailor_item/');
            $this->Admin_login->my_file_remove($image_5, 'test', 'media/tailor_item/');

            $sdata['message'] = "Successfully Data Deleted";
            $this->session->set_userdata($sdata);
            redirect("items/index");
        } else {
            $sdata['exception'] = "Items Not Deleted";
            $this->session->set_userdata($sdata);
            redirect("items/index");
        }
    }
}
