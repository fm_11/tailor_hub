<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Leads extends CI_Controller
{
    public $notification = array();

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login','common/custom_methods_model','Email_process','Employee'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $employee_id = $user_info[0]->employee_id;
        //echo $employee_id; die;
        $this->notification = $this->Admin_login->get_notification($employee_id);
        $this->db->query('SET SESSION sql_mode = ""');
    }

    public function index()
    {

      $cond = array();
      if($_POST){
        $sdata['name'] = $this->input->post('name', true);
        $cond['name'] = $this->input->post('name', true);
        $sdata['country_id'] = $this->input->post('country_id', true);
        $cond['country_id'] = $this->input->post('country_id', true);
        $sdata['lead_source_id'] = $this->input->post('lead_source_id', true);
        $cond['lead_source_id'] = $this->input->post('lead_source_id', true);
        $sdata['is_favourite'] = $this->input->post('is_favourite', true);
        $cond['is_favourite'] = $this->input->post('is_favourite', true);
        $sdata['date'] = $this->input->post('date', true);
        $cond['date'] = $this->input->post('date', true);
        $this->session->set_userdata($sdata);

      }else{

        $cond['name'] = $this->session->userdata('name');
        $cond['country_id'] = $this->session->userdata('country_id');
        $cond['lead_source_id'] = $this->session->userdata('lead_source_id');
        $cond['is_favourite'] = $this->session->userdata('is_favourite');
        $cond['date'] = $this->session->userdata('date');
      }
        $data = array();
        $data['title'] = 'Leads';
        $data['heading_msg'] = "Leads Information";
        $data['user_id'] = 0;

        $cond['current_officer'] = '0';
        $cond['is_deleted'] = 0;
        $this->load->library('pagination');
        $config['base_url'] = site_url('leads/index/');
        $config['per_page'] = 10;
        $config['total_rows'] = count($this->Admin_login->get_leads_info(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['leads'] = $this->Admin_login->get_leads_info(10, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['is_show_button'] = "add";
        $data['countries'] = $this->db->query("SELECT * FROM cc_countries")->result_array();
        $data['lead_source'] = $this->db->query("SELECT * FROM cc_lead_sources")->result_array();
        $data['branches'] = $this->db->query("SELECT * FROM leads")->result_array();

        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('leads/index', $data, true);
        $this->load->view('admin_logins/index', $data);
    }

    public function deleted_leads()
    {
        $data = array();
        $data['title'] = 'Deleted Leads';
        $data['heading_msg'] = "Deleted Leads Information";
        $cond = array();
        $cond['is_deleted'] = 1;
        $this->load->library('pagination');
        $config['base_url'] = site_url('leads/deleted_leads/');
        $config['per_page'] = 10;
        $config['total_rows'] = count($this->Admin_login->get_leads_info(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['leads'] = $this->Admin_login->get_leads_info(10, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['is_show_button'] = "add";
        $data['branches'] = $this->db->query("SELECT * FROM leads")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('leads/index', $data, true);
        $this->load->view('admin_logins/index', $data);
    }

    public function my_desk()
    {
      $cond = array();
      if($_POST){
        $sdata['name'] = $this->input->post('name', true);
        $cond['name'] = $this->input->post('name', true);
        $sdata['country_id'] = $this->input->post('country_id', true);
        $cond['country_id'] = $this->input->post('country_id', true);
        $sdata['lead_source_id'] = $this->input->post('lead_source_id', true);
        $cond['lead_source_id'] = $this->input->post('lead_source_id', true);
        $sdata['admission_officer'] = $this->input->post('admission_officer', true);
        $cond['admission_officer'] = $this->input->post('admission_officer', true);
        $sdata['date'] = $this->input->post('date', true);
        $cond['date'] = $this->input->post('date', true);
        $is_favourite = $this->input->post('is_favourite', true);
       if ($is_favourite != 'all') {
           $sdata['is_favourite'] = $is_favourite;
           $this->session->set_userdata($sdata);
           $cond['is_favourite'] = $is_favourite;
       } else {
           $sdata['is_favourite'] = "";
           $this->session->set_userdata($sdata);
           $cond['is_favourite'] = "";
           $cond['date'] = $this->session->userdata('date');
       }

        // $sdata['is_favourite'] = $this->input->post('is_favourite', true);
        // $cond['is_favourite'] = $this->input->post('is_favourite', true);
        $this->session->set_userdata($sdata);

      }else{

        $cond['name'] = $this->session->userdata('name');
        $cond['country_id'] = $this->session->userdata('country_id');
        $cond['lead_source_id'] = $this->session->userdata('lead_source_id');
        $is_favourite = $this->session->userdata('is_favourite');
        $cond['is_favourite'] = $is_favourite;
        $cond['admission_officer'] = $this->input->post('admission_officer', true);
      //  $cond['is_favourite'] = $this->session->userdata('is_favourite');
      }
        $data = array();
        $data['title'] = 'Leads -> MyDesk';
        $data['heading_msg'] = "Leads -> MyDesk";
        $user_info = $this->session->userdata('user_info');
        $is_head_of_admission = $user_info[0]->is_head_of_admission;
        $employee_id =  $user_info[0]->employee_id;
        $user_id = $user_info[0]->id;
        $data['user_id'] = $user_id;

        if ($is_head_of_admission == '1') {
            $head_of_admission_info = $this->db->query("SELECT id,branch_id FROM tbl_employee where id = '$employee_id'")->result_array();
            if (!empty($head_of_admission_info)) {
                $cond['origin_office'] = $head_of_admission_info[0]['branch_id'];
            } else {
                $cond['current_officer'] = $user_id;
            }
        } else {
            $cond['current_officer'] = $user_id;
        }


        $cond['is_deleted'] = 0;
        // if ($_POST) {
        //     $is_favourite = $this->input->post('is_favourite', true);
        //     if ($is_favourite != 'all') {
        //         $sdata['is_favourite'] = $is_favourite;
        //         $this->session->set_userdata($sdata);
        //         $cond['is_favourite'] = $is_favourite;
        //     } else {
        //         $sdata['is_favourite'] = "";
        //         $this->session->set_userdata($sdata);
        //         $cond['is_favourite'] = "";
        //     }
        // } else {
        //     $is_favourite = $this->session->userdata('is_favourite');
        //     $cond['is_favourite'] = $is_favourite;
        // }
        //echo   $cond['is_favourite'];
        //  die;
        $this->load->library('pagination');
        $config['base_url'] = site_url('leads/my_desk/');
        $config['per_page'] = 10;
        $config['total_rows'] = count($this->Admin_login->get_leads_info(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['leads'] = $this->Admin_login->get_leads_info(10, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['countries'] = $this->db->query("SELECT * FROM cc_countries")->result_array();
        $data['lead_source'] = $this->db->query("SELECT * FROM cc_lead_sources")->result_array();
        $data['branches'] = $this->db->query("SELECT * FROM leads")->result_array();
        $data['admission_officer'] = $this->Admin_login->get_admission_officer_list_for_my_desk($user_info[0]->id);
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('leads/index', $data, true);
        $this->load->view('admin_logins/index', $data);
    }

    public function add()
    {
        if ($_POST) {
          if ($this->Admin_login->check_ifexist_phone_leads($this->input->post('phone', true))) {
              $sdata['exception'] = "This contact already exist.";
              $this->session->set_userdata($sdata);
              redirect("leads/add/");
          }
            $data = array();
            $data['id'] = $this->input->post('id', true);
            $data['first_name'] = $this->input->post('first_name', true);
            $data['last_name'] = $this->input->post('last_name', true);
            $data['phone'] = $this->input->post('phone', true);
            $data['email'] = $this->input->post('email', true);
            if($this->input->post('interested_country', true)!='')
            {
              $data['interested_country'] = $this->input->post('interested_country', true);
            }
            $data['purpose'] = $this->input->post('purpose', true);
            $data['course_level_id'] = $this->input->post('course_level_id', true);
            $data['course_subject'] = $this->input->post('course_subject', true);
            $data['origin_office'] = $this->input->post('origin_office', true);
            $data['lead_source_id'] = $this->input->post('lead_source_id', true);
            $data['collection_officer'] = $this->input->post('collection_officer', true);
            $data['collection_venue'] = $this->input->post('collection_venue', true);
            $data['intake_month'] = $this->input->post('intake_month', true);
            $data['intake_year'] = $this->input->post('intake_year', true);
            $data['collection_date'] = $this->input->post('collection_date', true);
            $data['country_code'] = $this->input->post('country_code', true);
            $this->db->insert('leads', $data);
            $sdata['message'] = "You are Successfully Added Leads!";
            $this->session->set_userdata($sdata);
            redirect("leads/index");
        } else {
            $data = array();
            $data['title'] = 'Leads Information';
            $data['heading_msg'] = "Leads Information";
            $data['is_show_button'] = "index";
            $data['countries'] = $this->db->query("SELECT * FROM cc_countries")->result_array();
            $data['course_level'] = $this->db->query("SELECT * FROM cc_course_level")->result_array();
            $data['origin_office'] = $this->db->query("SELECT * FROM tbl_branch")->result_array();
            $data['lead_source'] = $this->db->query("SELECT * FROM cc_lead_sources")->result_array();
            $data['collection_venue'] = $this->db->query("SELECT * FROM cc_collection_venue")->result_array();
            $data['collection_officer'] = $this->db->query("SELECT * FROM tbl_employee")->result_array();
            $data['intake_year'] =$this->Admin_login->get_intake_years();
            $data['intake_month'] =$this->Admin_login->get_intake_months();
            $data['country_code'] =$this->Admin_login->get_country_code();
            $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
            $data['maincontent'] = $this->load->view('leads/add', $data, true);
            $this->load->view('admin_logins/index', $data);
        }
    }

    public function edit($id=null)
    {
        $lead_info = $this->Admin_login->get_leads_info_by_unit_id($id);
        //echo $lead_info->collection_officer;
        //die;
        if (!empty($lead_info) && $lead_info->current_officer <= 0) {
            $sdata['exception'] = "Please get to your desk first";
            $this->session->set_userdata($sdata);
            redirect("leads/index");
        }
        if ($_POST) {
          if ($this->Admin_login->check_ifexist_update_phone_leads($this->input->post('phone', true),$this->input->post('id', true))) {
              $sdata['exception'] = "This contact already exist.";
              $this->session->set_userdata($sdata);
              redirect("leads/edit/".$this->input->post('id', true));
          }
            $data = array();
            $data['id'] = $this->input->post('id', true);
            $data['first_name'] = $this->input->post('first_name', true);
            $data['last_name'] = $this->input->post('last_name', true);
            $data['phone'] = $this->input->post('phone', true);
            $data['email'] = $this->input->post('email', true);
            if($this->input->post('interested_country', true)!='')
            {
              $data['interested_country'] = $this->input->post('interested_country', true);
            }

            $data['purpose'] = $this->input->post('purpose', true);
            $data['course_level_id'] = $this->input->post('course_level_id', true);
            $data['course_subject'] = $this->input->post('course_subject', true);
            $data['origin_office'] = $this->input->post('origin_office', true);
            $data['lead_source_id'] = $this->input->post('lead_source_id', true);
            $data['collection_officer'] = $this->input->post('collection_officer', true);
            $data['collection_venue'] = $this->input->post('collection_venue', true);
            $data['intake_year'] = $this->input->post('intake_year', true);
            $data['intake_month'] = $this->input->post('intake_month', true);
            $data['university'] = $this->input->post('university', true);
            $data['ielts'] = $this->input->post('ielts', true);
            $data['status'] = $this->input->post('status', true);
            $data['collection_date'] = $this->input->post('collection_date', true);
            $data['country_code'] = $this->input->post('country_code', true);

            $this->db->where('id', $data['id']);
            $this->db->update('leads', $data);
            $sdata['message'] = "You are Successfully Updated leads !";
            $this->session->set_userdata($sdata);
            redirect("leads/my_desk");
        } else {
            $data = array();
            $data['title'] = 'Update Leads';
            $data['heading_msg'] = "Update Leads";
            $data['is_show_button'] = "index";
            $data['countries'] = $this->db->query("SELECT * FROM cc_countries")->result_array();
            $data['followup_mode'] = $this->db->query("SELECT * FROM cc_followup_mode")->result_array();
            $data['course_level'] = $this->db->query("SELECT * FROM cc_course_level")->result_array();
            $data['origin_office'] = $this->db->query("SELECT * FROM tbl_branch")->result_array();
            $data['lead_source'] = $this->db->query("SELECT * FROM cc_lead_sources")->result_array();
            $data['collection_venue'] = $this->db->query("SELECT * FROM cc_collection_venue")->result_array();
            $data['collection_officer'] = $this->db->query("SELECT * FROM tbl_employee")->result_array();
            $data['intake_year'] =$this->Admin_login->get_intake_years();
            $data['intake_month'] =$this->Admin_login->get_intake_months();
            $data['country_code'] =$this->Admin_login->get_country_code();
            $data['lead'] = $this->Admin_login->get_leads_info_by_unit_id($id);
            $data['lead_followup'] = $this->db->query("SELECT e.*,c.name as followup_type_id,emp.name as followup_officer_name FROM leads_followup AS e
                                            LEFT JOIN cc_followup_mode as c ON c.id = e.followup_type_id
                                            LEFT JOIN tbl_user as u ON u.id = e.followup_officer
                                            LEFT JOIN tbl_employee as emp ON emp.id = u.employee_id
                                            WHERE e.lead_id = '$id'")->result_array();

            //$data['lead_followup'] = $this->db->query("SELECT * FROM leads_followup WHERE lead_id = '$id'")->result_array();

            $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
            $data['maincontent'] = $this->load->view('leads/edit', $data, true);
            $this->load->view('admin_logins/index', $data);
        }
    }
    public function view($id=null)
    {
        if ($id <= 0) {
            $sdata['exception'] = "Invalid!";
            $this->session->set_userdata($sdata);
            redirect("leads/index");
        }
        $data = array();
        $data['title'] = 'View Leads';
        $data['heading_msg'] = "View Leads";
        $data['is_show_button'] = "index";
        $data['countries'] = $this->db->query("SELECT * FROM cc_countries")->result_array();
        $data['followup_mode'] = $this->db->query("SELECT * FROM cc_followup_mode")->result_array();
        $data['course_level'] = $this->db->query("SELECT * FROM cc_course_level")->result_array();
        $data['origin_office'] = $this->db->query("SELECT * FROM tbl_branch")->result_array();
        $data['lead_source'] = $this->db->query("SELECT * FROM cc_lead_sources")->result_array();
        $data['collection_venue'] = $this->db->query("SELECT * FROM cc_collection_venue")->result_array();
        $data['collection_officer'] = $this->db->query("SELECT * FROM tbl_employee")->result_array();
        $data['intake_year'] =$this->Admin_login->get_intake_years();
        $data['intake_month'] =$this->Admin_login->get_intake_months();
        $data['lead'] = $this->Admin_login->get_leads_info_by_unit_id($id);
        $data['lead_followup'] = $this->db->query("SELECT e.*,c.name as followup_type_id,emp.name as followup_officer_name FROM leads_followup AS e
                                        LEFT JOIN cc_followup_mode as c ON c.id = e.followup_type_id
                                        LEFT JOIN tbl_user as u ON u.id = e.followup_officer
                                        LEFT JOIN tbl_employee as emp ON emp.id = u.employee_id
                                        WHERE e.lead_id = '$id'")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('leads/view', $data, true);
        $this->load->view('admin_logins/index', $data);
    }
    function lead_email_send(){
      if($_POST){
        $receivers = $this->input->post('recipientList', true);
        $message = $this->input->post('message', true);
        $header = $this->input->post('header', true);

        $session_user = $this->session->userdata('user_info');
        $employee_id = $session_user[0]->employee_id;

        $email_data = array();
        $email_data['receiver_name'] = "All";
        $employee_info = $this->Employee->getEmployeeName($employee_id);
        $email_data['sender_name'] = $employee_info[0]['name'] . '(' . $employee_info[0]['code'] . ')';
        $email_data['sender_designation_name'] = $employee_info[0]['designation_name'];
        $email_data['sender_branch_name'] = $employee_info[0]['branch_name'];
        $email_data['title'] = $header;
        $email_data['details'] = $message;
        $email_data['file_link'] = "http://achi365.com/spate-i/students_login";
        $email_message = $this->load->view('admin_logins/email_template2',$email_data,true);

        if($this->Email_process->send_email($receivers,$header,$email_message)){
          $sdata['message'] = "You are Successfully Message Send.";
          $this->session->set_userdata($sdata);
         redirect("leads/my_desk");
        }else{
          $sdata['exception'] = "Sorry Message Doesn't Send !";
          $this->session->set_userdata($sdata);
          redirect("leads/my_desk");
        }
      }
    }
    public function transfer_to_mydesk($id)
    {
        $user_info = $this->session->userdata('user_info');
        $user_id = $user_info[0]->id;
        $data = array();
        $data['id'] = $id;
        $data['current_officer'] = $user_id;
        $this->db->where('id', $data['id']);
        $this->db->update('leads', $data);
        $sdata['message'] = "You are Successfully Transfer leads";
        $this->session->set_userdata($sdata);
        redirect("leads/index");
    }
    function lead_transfer_index(){
      if($_POST){
        $string_id = $this->input->post('leadIdList', true);
        $lead_id_list =  explode(",",$string_id);
        if(!empty($string_id))
        {
          $user_info = $this->session->userdata('user_info');
          $user_id = $user_info[0]->id;
           foreach ($lead_id_list as $value)
           {
                $data = array();
                $data['id'] = $value;
                $data['current_officer'] = $user_id;
                $this->db->where('id', $data['id']);
                $this->db->update('leads', $data);
            }
            $sdata['message'] = "You are Successfully Transfer leads";
            $this->session->set_userdata($sdata);
            redirect("leads/index");
        }else{
          $sdata['exception'] = "Sorry Leads Cannot Transfer !";
          $this->session->set_userdata($sdata);
          redirect("leads/index");
        }

      }
    }

    public function concern_person_change($id=null)
    {
        $data = array();
        if ($_POST) {
            $lead = $this->Admin_login->get_leads_info_by_unit_id($this->input->post('id', true));
            $user_info = $this->session->userdata('user_info');

            $history = array();
            $history['lead_id'] = $this->input->post('id', true);
            $history['old_officer'] = $lead->current_officer;
            $new_concern_person_id = $this->input->post('new_concern_person_id', true);
            $user_info_data = $this->custom_methods_model->select_row_by_condition("tbl_user", "employee_id='$new_concern_person_id'");
            if (empty($user_info_data)) {
                $sdata['exception'] = "No user was found for this employee";
                $this->session->set_userdata($sdata);
                redirect("leads/my_desk");
            }
            $new_concern_person_user_id = $user_info_data[0]->id;

            $history['new_officer'] = $new_concern_person_user_id;
            $history['created_at'] = date('Y-m-d H:i:s');
            $history['created_by'] = $user_info[0]->id;
            $this->db->insert('leads_assign_history', $history);

            $data = array();
            $data['id'] =  $this->input->post('id', true);
            $data['current_officer'] = $new_concern_person_user_id;
            $this->db->where('id', $data['id']);
            $this->db->update('leads', $data);

            $sdata['message'] = "You are Successfully Transfer leads";
            $this->session->set_userdata($sdata);
            redirect("leads/my_desk");
        }
        $data['id'] = $id;
        $data['title'] = 'Concern Person Change';
        $data['heading_msg'] = "Concern Person Change";
        $data['collection_officer'] = $this->db->query("SELECT id,name,code FROM tbl_employee WHERE `status` = '1'")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('leads/concern_person_change', $data, true);
        $this->load->view('admin_logins/index', $data);
    }

    public function delete($id)
    {
        $data = array();
        $data['id'] = $id;
        $data['is_deleted'] = 1;
        $this->db->where('id', $data['id']);
        $this->db->update('leads', $data);
        $sdata['message'] = "You are Successfully delete lead";
        $this->session->set_userdata($sdata);
        redirect("leads/my_desk");
    }

    public function reactive_lead($id)
    {
        $data = array();
        $data['id'] = $id;
        $data['is_deleted'] = 0;
        $this->db->where('id', $data['id']);
        $this->db->update('leads', $data);
        $sdata['message'] = "You are Successfully activate lead";
        $this->session->set_userdata($sdata);
        redirect("leads/deleted_leads");
    }

    public function add_to_favourite($id, $is_favourite)
    {
        $data = array();
        $data['id'] = $id;
        if ($is_favourite == 0) {
            $data['is_favourite'] = 1;
        } else {
            $data['is_favourite'] = 0;
        }
        $this->db->where('id', $data['id']);
        $this->db->update('leads', $data);
        $sdata['message'] = "You are successfully changed data status";
        $this->session->set_userdata($sdata);
        redirect("leads/my_desk");
    }


    public function followup_save($id)
    {
        $user_info = $this->session->userdata('user_info');
        $user_id = $user_info[0]->id;
        $data = array();
        $data['lead_id'] = $id;
        $data['followup_type_id'] = $this->input->post('followup_type_id', true);
        $data['followup_date'] = $this->input->post('followup_date', true);
        $data['remarks'] = $this->input->post('remarks', true);
        if($this->input->post('next_followup_date', true)!='')
        {
          $data['next_followup_date'] = $this->input->post('next_followup_date', true);
        }

        $data['status'] = $this->input->post('status', true);
        $data['followup_officer'] = $user_id;

        $this->db->insert('leads_followup', $data);
        $sdata['message'] = "Data Successfully Added.";
        $this->session->set_userdata($sdata);
       $followup=$this->input->post('followup', true);
       if($followup=='view')
       {
           redirect("leads/view/".$id);
       }else{
           redirect("leads/edit/".$id);
       }

    }
}
