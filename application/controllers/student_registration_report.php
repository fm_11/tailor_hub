
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Student_Registration_Report extends CI_Controller
{
public $notification = array();
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login','Timekeeping','Branch'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        //set timezone
        date_default_timezone_set($user_info[0]->time_zone);
        $employee_id =  $user_info[0]->employee_id;
        //echo $employee_id; die;
        $this->notification = $this->Admin_login->get_notification($employee_id);
    }

    public function index()
    {
      $data = array();
      $user_info = $this->session->userdata('user_info');
      $employee_id =  $user_info[0]->employee_id;
      $user_id =$user_info[0]->id;

      if($_POST){
        $userid = $this->input->post('user_id', true);
        $from_date = $this->input->post('from_date', true);
        $to_date = $this->input->post('to_date', true);
        $f_date = strtotime($from_date);
        $t_date = strtotime($to_date);
        $datediff =  $t_date-$f_date;
        $data['from_date']=$from_date;
        $data['to_date']=$to_date;
        $data['employee_info']=$this->Admin_login->get_user_common_info($userid);
        $data['total_day'] =round($datediff / (60 * 60 * 24))+1;

        $data['my_desk']=$this->db->query("SELECT id FROM `leads` WHERE is_deleted=0 AND  is_registered=0 AND current_officer='$userid'")->num_rows();
        $data['student_registration']=$this->db->query("SELECT id FROM `leads` WHERE is_deleted=0 AND  is_registered=1 AND current_officer='$userid'")->num_rows();
        $data['title'] = 'Student Registration Report';
        $data['heading_msg'] = "Student Registration Report";
        $data['report_to']=$this->Admin_login->get_user_head_of_admission_designation($userid);
        $data['printed_by']=$this->Employee->getEmployeeName($employee_id);
        $data['rData'] = $this->db->query("SELECT `applicant_code`,CONCAT(a.`first_name`,' ',a.`last_name`) AS NAME, c.`name` AS country,
                                s.`name` AS STATUS,cs.`name` AS lead_source_name,m.`name` AS MONTH,y.`name` AS YEAR
                                FROM `applicants` AS a
                                LEFT JOIN `cc_countries` AS c ON a.`country_id`=c.`id`
                                LEFT JOIN `tbl_application_status` AS s ON a.`applicant_status_id`=s.`id`
                                LEFT JOIN `cc_lead_sources` AS cs ON a.`lead_source_id`=cs.`id`
                                LEFT JOIN `cc_intake_months` AS m ON a.`intake_month_id`=m.`id`
                                LEFT JOIN `cc_intake_years` AS Y ON a.`intake_year_id`=y.`id`
                                WHERE a.`admission_officer_id`='$userid' order by  a.applicant_code")->result_array();


        if(empty($data['rData'])){
          $sdata['exception'] = "Data not found !";
          $this->session->set_userdata($sdata);
          redirect("student_registration_report/index");
        }

        $data['org_info'] = $this->Admin_login->getReportHeaderAddress($data['employee_info'][0]['branch_id']);
        $excel = $this->input->post('excel', true);
      //  echo $excel; die;
        if(isset($excel) && $excel != ''){
          $data['excel'] = 1;
          $this->load->view('student_registration_report/student_registration_report_table', $data);
          //die;
        }else{
          $data['report'] = $this->load->view('student_registration_report/student_registration_report_table', $data, true);
        }
      }

      if(!isset($excel) || $excel == ''){
        $data['title'] = 'Student Registraion Report';
        $data['heading_msg'] = "Student Registrstion Report";
        $user_type=$this->Admin_login->get_user_type($user_id);
        if($user_type=="hop")
        {
            $data['branches'] = $this->Admin_login->get_all_branch_list();
            //empty load
            $data['employees'] = $this->Admin_login->get_employee_list_by_employeeid(0);
        }elseif($user_type=="hoa")
        {
          $data['employees'] = $this->Admin_login->get_employee_list_by_employeeid($employee_id);
        }

        $data['user_ids']=$user_id;
        $data['user_type'] =$user_type;
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('student_registration_report/index', $data, true);
        $this->load->view('admin_logins/index', $data);
      }
    }
    public function getEmployeeByBrachId()
    {
        $branch_id = $this->input->get('branch_id', true);

        $data = array();
        $data['employees'] = $this->db->query("SELECT u.`id`,e.name FROM `tbl_employee` AS e
                            INNER JOIN tbl_user AS u ON e.id=u.`employee_id` WHERE e.branch_id ='$branch_id'
                            ORDER BY e.name")->result_array();
        $this->load->view('student_registration_report/employee_list', $data);
    }



}
