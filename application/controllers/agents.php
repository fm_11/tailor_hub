<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Agents extends CI_Controller
{
    public $notification = array();

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login','Employee','Agent_portal','Email_process'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $employee_id = $user_info[0]->employee_id;
        //echo $employee_id; die;
        $this->notification = $this->Admin_login->get_notification($employee_id);
        $this->db->query('SET SESSION sql_mode = ""');
    }

    public function index()
    {
      $cond = array();
      if($_POST){
        $sdata['name'] = $this->input->post('name', true);
        $cond['name'] = $this->input->post('name', true);
        $sdata['code'] = $this->input->post('code', true);
        $cond['code'] = $this->input->post('code', true);
        $this->session->set_userdata($sdata);

      }else{

        $cond['name'] = $this->session->userdata('name');
        $cond['code'] = $this->session->userdata('code');
      }
        $data = array();
        $data['title'] = 'Agents';
        $data['heading_msg'] = "Agents Information";

        $this->load->library('pagination');
        $config['base_url'] = site_url('agents/index/');
        $config['per_page'] = 10;
        $config['total_rows'] = count($this->Agent_portal->get_agents_info(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['agents'] = $this->Agent_portal->get_agents_info(10, (int)$this->uri->segment(3), $cond);
        //  echo '<pre>';
        //  print_r($data['agents']);
        //  die;
        $data['counter'] = (int)$this->uri->segment(3);
        $data['is_show_button'] = "add";
        //  $data['agentses'] = $this->db->query("SELECT * FROM agents")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('agents/index', $data, true);
        $this->load->view('admin_logins/index', $data);
    }

    public function add()
    {
        if ($_POST) {

          if ($this->Admin_login->check_ifexist_email_agent($this->input->post('email', true))) {
              $sdata['exception'] = "This email already exist.";
              $this->session->set_userdata($sdata);
              redirect("agents/add/");
          }
            //Logo upload
            $logo_name='0';
            if ($_FILES["logo"]['name'] != '') {
                $ext = explode('.',$_FILES["logo"]['name']);
                $logo_name = 'ins_logo_'.time(). '.' .end($ext);
                $config = array(
                    'upload_path' => "media/agent/",
                    'allowed_types' => "jpg|png|jpeg|JPEG|JPG|PNG",
                    'overwrite' => true,
                    'max_size' => "99999999999",
                    'max_height' => "3000",
                    'max_width' => "3000",
                    'file_name' => $logo_name
                );
                $this->load->library('upload', $config);

                if (!$this->upload->do_upload('logo')) {
                    $sdata['exception'] = "Logo doesn't upload." . $this->upload->display_errors();
                    $this->session->set_userdata($sdata);
                    redirect("agents/index");
                }
            }
            $data = array();
            $data['logo'] = $logo_name;
            $data['company_name'] = $this->input->post('company_name', true);
            $data['personal_name'] = $this->input->post('personal_name', true);
            $data['address'] = $this->input->post('address', true);
            $data['phone'] = $this->input->post('phone', true);
            $data['email'] = $this->input->post('email', true);
            $data['admission_officer'] = $this->input->post('admission_officer', true);
            $data['marketing_officer'] = $this->input->post('marketing_officer', true);
            $data['bank_name'] = $this->input->post('bank_name', true);
            $data['account_number'] = $this->input->post('account_number', true);
            $data['account_name'] = $this->input->post('account_name', true);
            $data['branch_name'] = $this->input->post('branch_name', true);
            $data['password'] = md5($this->input->post('password', true));
            $data['status'] = $this->input->post('status', true);
            $date = new DateTime();
            $data['process_date']=date("Y-m-d H:i:s");

            $admission_officer = $this->input->post('admission_officer', true);
            $admission_officer_info = $this->db->query("SELECT id,branch_id FROM tbl_employee WHERE id = '$admission_officer'")->result_array();
            $finalcode = "";
            if (!empty($admission_officer_info)) {
                $branch_id = $admission_officer_info[0]['branch_id'];
                $branches = $this->db->query("SELECT * FROM tbl_branch WHERE id = '$branch_id'")->result_array();
                $code = "AG-";
                $code .= $branches[0]['code'];
                $total_agent = count($this->db->query("SELECT id FROM agents")->result_array());
                $finalcode = $code.str_pad(($total_agent + 1), 3, '0', STR_PAD_LEFT);
            }
            $data['code'] = $finalcode;
            $data['country_code'] = $this->input->post('country_code', true);
            $this->db->insert('agents', $data);
            $sdata['message'] = "You are Successfully Added Course Level Info !";
            $this->session->set_userdata($sdata);
            redirect("agents/index");
        } else {
            $data = array();
            $data['title'] = 'Agents Information';
            $data['heading_msg'] = "Agents Information";
            $data['is_show_button'] = "index";
            $data['employees'] = $this->db->query("SELECT * FROM tbl_employee")->result_array();
            $data['country_code'] =$this->Admin_login->get_country_code();
            $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
            $data['maincontent'] = $this->load->view('agents/add', $data, true);
            $this->load->view('admin_logins/index', $data);
        }
    }

    public function edit($id=null)
    {
        if ($_POST) {
          if ($this->Admin_login->check_ifexist_update_email_agent($this->input->post('email', true),$this->input->post('id', true))) {
              $sdata['exception'] = "This email already exist.";
              $this->session->set_userdata($sdata);
              redirect("agents/edit/".$this->input->post('id', true));
          }
            //echo '<pre>';
            // print_r($_POST);
            // die;
            //Logo upload
            $logo_name='0';
            if ($_FILES["logo"]['name'] != '') {
              $ext = explode('.',$_FILES["logo"]['name']);
              $logo_name = 'ins_logo_'.time(). '.' .end($ext);
              $config = array(
                  'upload_path' => "media/agent/",
                  'allowed_types' => "jpg|png|jpeg|JPEG|JPG|PNG",
                  'overwrite' => true,
                  'max_size' => "99999999999",
                  'max_height' => "3000",
                  'max_width' => "3000",
                  'file_name' => $logo_name
              );
                $this->load->library('upload', $config);

                if (!$this->upload->do_upload('logo')) {
                    $sdata['exception'] = "Logo doesn't upload." . $this->upload->display_errors()." and max height: 3000px ,max width: 3000px";
                    $this->session->set_userdata($sdata);
                    redirect("agents/index");
                }
            }
            $data = array();
            if ($_FILES["logo"]['name'] != '') {
                $data['logo'] = $logo_name;
            }
            $data['id'] = $this->input->post('id', true);
            $data['company_name'] = $this->input->post('company_name', true);
            $data['personal_name'] = $this->input->post('personal_name', true);
            $data['address'] = $this->input->post('address', true);
            $data['phone'] = $this->input->post('phone', true);
            $data['email'] = $this->input->post('email', true);
            $data['admission_officer'] = $this->input->post('admission_officer', true);
            $data['marketing_officer'] = $this->input->post('marketing_officer', true);
            $data['bank_name'] = $this->input->post('bank_name', true);
            $data['account_number'] = $this->input->post('account_number', true);
            $data['account_name'] = $this->input->post('account_name', true);
            $data['branch_name'] = $this->input->post('branch_name', true);
            $data['status'] = $this->input->post('status', true);
            if ($this->input->post('password', true) != '') {
                $data['password'] = md5($this->input->post('password', true));
            }
           $data['country_code'] = $this->input->post('country_code', true);

            $this->db->where('id', $data['id']);
            $this->db->update('agents', $data);
            $sdata['message'] = "You are Successfully Updated Agents Info !";
            $this->session->set_userdata($sdata);
            redirect("agents/index");
        } else {
            $data = array();
            $data['title'] = 'Update Agents Information';
            $data['heading_msg'] = "Update Agents Information";
            $data['is_show_button'] = "index";
            $data['employees'] = $this->db->query("SELECT * FROM tbl_employee")->result_array();
            $data['agents'] = $this->Agent_portal->get_agents_info_by_unit_id($id);
            $data['country_code'] =$this->Admin_login->get_country_code();
            $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
            $data['maincontent'] = $this->load->view('agents/edit', $data, true);
            $this->load->view('admin_logins/index', $data);
        }
    }

    public function delete($id)
    {
        $this->db->delete('agents', array('id' => $id));
        $sdata['message'] = "Agent Deleted Successfully !";
        $this->session->set_userdata($sdata);
        redirect("agents/index");
    }
    public function view($id=null)
    {

        $data = array();
        $data['title'] = 'View Agents Information';
        $data['heading_msg'] = "View Agents Information";
        $data['is_show_button'] = "index";
        $data['employees'] = $this->db->query("SELECT * FROM tbl_employee")->result_array();
        $data['agents'] = $this->Agent_portal->get_agents_info_by_unit_id($id);
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('agents/view', $data, true);
        $this->load->view('admin_logins/index', $data);

    }
    function updateMsgStatusAgentStatus()
    {
        $status = $this->input->get('status', true);
        $id = $this->input->get('id', true);
        $data = array();
        $data['id'] = $this->input->get('id', true);
        if ($status == 1) {
            $data['status'] = 0;
        } else {
            $data['status'] = 1;
        }
        $this->Admin_login->update_agent_status($data);
        if ($status == 0) {
            echo '<a class="approve_icon" title="Active" href="#" onclick="msgStatusUpdate(' . $id . ',1)"><button type="button" class="btn btn-primary btn-xs mb-1">Active</button></a>';
        } else {
            echo '<a class="reject_icon" title="Inactive" href="#" onclick="msgStatusUpdate(' . $id . ',0)"><button type="button" class="btn btn-primary btn-xs mb-1">Inactive</button></a>';
        }
    }

function agents_email_send(){
    if($_POST){
      $receivers = $this->input->post('recipientList', true);
      $message = $this->input->post('message', true);
      $header = $this->input->post('header', true);

      $session_user = $this->session->userdata('user_info');
      $employee_id = $session_user[0]->employee_id;

      $email_data = array();
      $email_data['receiver_name'] = "All";
      $employee_info = $this->Employee->getEmployeeName($employee_id);
      $email_data['sender_name'] = $employee_info[0]['name'] . '(' . $employee_info[0]['code'] . ')';
      $email_data['sender_designation_name'] = $employee_info[0]['designation_name'];
      $email_data['sender_branch_name'] = $employee_info[0]['branch_name'];
      $email_data['title'] = $header;
      $email_data['details'] = $message;
      $email_data['file_link'] = "http://achi365.com/spate-i/agent_login";
      $email_message = $this->load->view('admin_logins/email_template2',$email_data,true);

      if($this->Email_process->send_email($receivers,$header,$email_message)){
        $sdata['message'] = "You are Successfully Message Send.";
        $this->session->set_userdata($sdata);
       redirect("agents/index");
      }else{
        $sdata['exception'] = "Sorry Message Doesn't Send !";
        $this->session->set_userdata($sdata);
       redirect("agents/index");
      }
    }
  }
}
