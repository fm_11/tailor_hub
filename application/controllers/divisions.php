<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Divisions extends CI_Controller
{
public $notification = array();
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $employee_id =  $user_info[0]->employee_id;
        //echo $employee_id; die;
        $this->notification = $this->Admin_login->get_notification($employee_id);
        $this->db->query('SET SESSION sql_mode = ""');
    }

    public function index()
    {
      $data = array();
      $data['title'] = 'Division Information';
      $data['heading_msg'] = "Division Information";
      $data['divisions'] = $this->db->query("SELECT * FROM tbl_division")->result_array();
      $data['is_show_button'] = "add";
      $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
      $data['maincontent'] = $this->load->view('divisions/index', $data, true);
      $this->load->view('admin_logins/index', $data);
    }


    function add()
    {
      if($_POST){
        if($this->Admin_login->check_ifexist_division($this->input->post('name', true)))
        {
          $sdata['exception'] = "This division name '".$this->input->post('name', true)."' already exists.";
          $this->session->set_userdata($sdata);
          redirect("divisions/add");
        }

        $this->load->library('upload');
        $config['upload_path'] = 'media/website/images/division/';
        $config['allowed_types'] = 'jpg|png|JPEG|jpeg';
        $config['max_size'] = '6000';
        $config['max_width'] = '4000';
        $config['max_height'] = '4000';
        $this->upload->initialize($config);
        $extension = strtolower(substr(strrchr($_FILES['txtPhoto']['name'], '.'), 1));
        $new_photo_name = "division_" . time() . "_" . date('Y-m-d') . "." . $extension;
        if (move_uploaded_file($_FILES["txtPhoto"]["tmp_name"], "media/website/images/division/" . $new_photo_name))
        {

        } else {
            $sdata['exception'] = "Sorry Photo Does't Upload !";
            $this->session->set_userdata($sdata);
            redirect("division/add");
        }
        $data = array();
        $data['name'] = $this->input->post('name', true);
        $data['image_path'] = $new_photo_name;

        $this->db->insert('tbl_division', $data);
        $sdata['message'] = "You Successfully Added Division Info !";
        $this->session->set_userdata($sdata);
        redirect("divisions/index");
      }else{
        $data = array();
        $data['title'] = 'Add Division Information';
        $data['heading_msg'] = "Add Division Information";
        $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('divisions/add', $data, true);
        $this->load->view('admin_logins/index', $data);
      }
  }

  function edit($id=null)
  {
    if($_POST){
      $data = array();
      if($this->Admin_login->check_ifexist_update_division($this->input->post('name', true),$this->input->post('id', true)))
      {
        $sdata['exception'] = "This division name '".$this->input->post('name', true)."' already exists.";
        $this->session->set_userdata($sdata);
        redirect("divisions/add");
      }
       $new_photo_name=$this->input->post('old_image_path', true);
        $file = $_FILES["txtPhoto"]['name'];
        if ($file != '') {
            $old_file = $new_photo_name;
            if (!empty($old_file)) {
                $filedel = PUBPATH . 'media/website/images/division/' . $old_file;
                unlink($filedel);
            }
            $this->load->library('upload');
            $config['upload_path'] = 'media/website/images/division/';
            $config['allowed_types'] = 'jpg|png|JPEG|jpeg';
            $config['max_size'] = '6000';
            $config['max_width'] = '4000';
            $config['max_height'] = '4000';
            $this->upload->initialize($config);
            $extension = strtolower(substr(strrchr($_FILES['txtPhoto']['name'], '.'), 1));
            $new_photo_name = "area_" . time() . "_" . date('Y-m-d') . "." . $extension;
            if (move_uploaded_file($_FILES["txtPhoto"]["tmp_name"], "media/website/images/division/" . $new_photo_name))
            {

            } else {
                $sdata['exception'] = "Sorry Photo Does't Upload !";
                $this->session->set_userdata($sdata);
                redirect("divisions/edit/".$this->input->post('id', true));
            }
          }
      $data['id'] = $this->input->post('id', true);
      $data['name'] = $this->input->post('name', true);
      $data['image_path'] = $new_photo_name;
      $this->db->where('id', $data['id']);
      $this->db->update('tbl_division', $data);
      $sdata['message'] = "You Successfully Updated Division Info !";
      $this->session->set_userdata($sdata);
      redirect("divisions/index");
    }else{
      $data = array();
      $data['title'] = 'Update Division Information';
      $data['heading_msg'] = "Update Division Information";
      $data['is_show_button'] = "index";
      $data['division_info'] = $this->Admin_login->get_division_info($id);
      $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
      $data['maincontent'] = $this->load->view('divisions/edit', $data, true);
      $this->load->view('admin_logins/index', $data);
    }
}

  function delete($id)
  {
      $this->db->delete('tbl_division', array('id' => $id));
      $sdata['message'] = "Division Information Deleted Successfully !";
      $this->session->set_userdata($sdata);
      redirect("divisions/index");
  }

  public function updateDivisionStatus()
    {
        $status = $this->input->get('status', true);
        $id = $this->input->get('id', true);
        $data = array();
        $data['id'] = $this->input->get('id', true);
        if ($status == 1) {
            $data['status'] = 0;
        } else {
            $data['status'] = 1;
        }
        $this->db->where('id', $data['id']);
        $this->db->update('tbl_division', $data);
        if ($status == 0) {
            echo '<a class="deleteTag" title="Active" href="#" onclick="msgStatusUpdate(' . $id . ',1)"><button type="button" class="btn btn-primary btn-xs mb-1">Active</button></i></a>';
        } else {
            echo '<a class="deleteTag" title="Inactive" href="#" onclick="msgStatusUpdate(' . $id . ',0)"><button type="button" class="btn btn-primary btn-xs mb-1">Inactive</button></a>';
        }
    }

}
