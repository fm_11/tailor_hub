<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin_logins extends CI_Controller
{
public $notification = array();
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $employee_id =  $user_info[0]->employee_id;
        //echo $employee_id; die;
        $this->notification = $this->Admin_login->get_notification($employee_id);
    }

    public function set_module($module_id)
    {
         $this->session->unset_userdata('module_id');
         $this->session->unset_userdata('main_menu_file');
         $this->session->unset_userdata('module_short_name');

         $module_info = $this->db->query("SELECT * FROM `tbl_module`
                               WHERE `id` = '$module_id' AND `is_active` = 1")->result_array();
         if(empty($module_info)){
           $sdata['exception'] = "You do not have access to this module.";
           $this->session->set_userdata($sdata);
           redirect("dashboard/index");
         }
         $arraydata = array(
            'module_id'  => $module_id,
            'main_menu_file'     => $module_info[0]['short_name'] . "_main_menu",
            'module_short_name'     => $module_info[0]['short_name']
          );
         $this->session->set_userdata($arraydata);

         redirect("dashboard/index");
    }

    public function index()
    {
        $data = array();
        $data['title'] = 'Configuration';
        $data['heading_msg'] = "Configuration";
        $data['top_menu'] = $this->load->view('admin_logins/all_content_menu', '', true);
        $this->load->view('admin_logins/index', $data);
    }

    public function readNotification($id)
    {
      $notification_info = $this->db->query("SELECT * FROM `tbl_notification`
                            WHERE `id` = '$id'")->result_array();
      if(!empty($notification_info)){
          $data = array();
          $data['id'] = $id;
          $data['it_seen'] = 1;
          $data['seen_time'] = date('Y-m-d H:i:s');
          if($this->Admin_login->update_notification_status($data)){
              redirect($notification_info[0]['action_url']);
          }
      }
    }

    function timekeeping_config(){
        if($_POST){
            $data = array();
            $data['id'] = $this->input->post('id', true);
            $data['expected_login_time'] = $this->input->post('expected_login_time', true);
            $data['expected_logout_time'] = $this->input->post('expected_logout_time', true);
            $this->Admin_login->timekeeping_config_update($data);
            $sdata['message'] = "You are Successfully Data Updated.";
            $this->session->set_userdata($sdata);
            redirect("admin_logins/timekeeping_config");
        }else{
            $data = array();
            $data['title'] = 'Timekeeping Configuration';
            $data['heading_msg'] = "Timekeeping Configuration";
            $data['config_info'] = $this->Admin_login->get_timekeeping_config_info();
            $data['top_menu'] = $this->load->view('admin_logins/all_content_menu', '', true);
            $data['maincontent'] = $this->load->view('admin_logins/config_info', $data, true);
            $this->load->view('admin_logins/index', $data);
        }
    }


    public function contact_info()
    {
        $data = array();
        $data['title'] = 'Contact Information';
        $data['heading_msg'] = "Contact Information";
        $data['contact_info'] = $this->Admin_login->get_contact_info();
        $data['top_menu'] = $this->load->view('admin_logins/all_content_menu', '', true);
        $data['maincontent'] = $this->load->view('admin_logins/contact_info', $data, true);
        $this->load->view('admin_logins/index', $data);
    }

    function update_contact_info()
    {
        $file_name = $_FILES['txtPhoto']['name'];
        if($file_name != ''){
            $this->load->library('upload');
            $config['upload_path'] = 'media/images/';
            $config['allowed_types'] = 'gif|jpg|png|JPEG|JPG|jpeg';
            $config['max_size'] = '5000';
            $config['max_width'] = '5000';
            $config['max_height'] = '5000';
            $this->upload->initialize($config);
            $extension = strtolower(substr(strrchr($_FILES['txtPhoto']['name'], '.'), 1));
            foreach ($_FILES as $field => $file) {
                if ($file['error'] == 0) {
                    $new_photo_name = "Contact_" . time() . "_" . date('Y-m-d') . "." . $extension;
                    if (move_uploaded_file($_FILES["txtPhoto"]["tmp_name"], "media/images/" . $new_photo_name)) {
                        $data = array();
                        $data['id'] = $this->input->post('id', true);
                        $data['org_name'] = $this->input->post('txtOrgName', true);
                        $data['address'] = $this->input->post('txtAddress', true);
                        $data['email'] = $this->input->post('txtEmail', true);
                        $data['mobile'] = $this->input->post('txtMobile', true);
                        $data['facebook_address'] = $this->input->post('txtFacebookAddress', true);
                        $data['web_address'] = $this->input->post('txtWebAddress', true);
                        $data['picture'] = $new_photo_name;
                        $this->Admin_login->update_contact_info($data);
                        $sdata['message'] = "You are Successfully Updated Contact Info ! ";
                        $this->session->set_userdata($sdata);
                        redirect("admin_logins/contact_info");
                    } else {
                        $sdata['exception'] = "Sorry Contact Info. Doesn't Updated !" . $this->upload->display_errors();
                        $this->session->set_userdata($sdata);
                        redirect("admin_logins/contact_info");
                    }
                } else {
                    $sdata['exception'] = "Sorry Contact Info. Doesn't Updated  !" . $this->upload->display_errors();
                    $this->session->set_userdata($sdata);
                    redirect("admin_logins/contact_info");
                }
            }
        }else{
            $data = array();
            $data['id'] = $this->input->post('id', true);
            $data['org_name'] = $this->input->post('txtOrgName', true);
            $data['address'] = $this->input->post('txtAddress', true);
            $data['email'] = $this->input->post('txtEmail', true);
            $data['mobile'] = $this->input->post('txtMobile', true);
            $data['facebook_address'] = $this->input->post('txtFacebookAddress', true);
            $data['web_address'] = $this->input->post('txtWebAddress', true);
            $this->Admin_login->update_contact_info($data);
            $sdata['message'] = "You are Successfully Updated Contact Info ! ";
            $this->session->set_userdata($sdata);
            redirect("admin_logins/contact_info");
        }
    }

}
