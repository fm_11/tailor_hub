<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tailor_customer_review extends CI_Controller
{
public $notification = array();
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $employee_id =  $user_info[0]->employee_id;
        //echo $employee_id; die;
        $this->notification = $this->Admin_login->get_notification($employee_id);
        $this->db->query('SET SESSION sql_mode = ""');
    }

    public function index()
    {
      $data = array();
      $data['title'] = 'Customer Review';
      $data['heading_msg'] = "Customer Review";
      $data['cr'] = $this->db->query("SELECT * FROM tbl_tailor_customer_review")->result_array();
      $data['is_show_button'] = "add";
      $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
      $data['maincontent'] = $this->load->view('tailor_customer_review/index', $data, true);
      $this->load->view('admin_logins/index', $data);
    }


    function add()
    {
      if($_POST){
        $this->load->library('upload');
        $config['upload_path'] = 'media/customer_review/';
        $config['allowed_types'] = 'jpg|png|JPEG|jpeg';
        $config['max_size'] = '6000';
        $config['max_width'] = '4000';
        $config['max_height'] = '4000';
        $this->upload->initialize($config);
        $extension = strtolower(substr(strrchr($_FILES['txtPhoto']['name'], '.'), 1));
        $new_photo_name = "CustomerReview_" . time() . "_" . date('Y-m-d') . "." . $extension;
        if (move_uploaded_file($_FILES["txtPhoto"]["tmp_name"], "media/customer_review/" . $new_photo_name))
        {

        } else {
            $sdata['exception'] = "Sorry Photo Does't Upload !";
            $this->session->set_userdata($sdata);
            redirect("tailor_customer_review/add");
        }
        $data = array();
        $data['name'] = $this->input->post('name', true);
        $data['description'] = $this->input->post('description', true);
        $data['image_path'] = $new_photo_name;

        $this->db->insert('tbl_tailor_customer_review', $data);
        $sdata['message'] = "You Successfully Added Customer Review!";
        $this->session->set_userdata($sdata);
        redirect("tailor_customer_review/index");
      }else{
        $data = array();
        $data['title'] = 'Add Customer Review';
        $data['heading_msg'] = "Add Customer Review";
        $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('tailor_customer_review/add', $data, true);
        $this->load->view('admin_logins/index', $data);
      }
  }

  function edit($id=null)
  {
    if($_POST){
      $data = array();
       $new_photo_name=$this->input->post('old_image_path', true);
        $file = $_FILES["txtPhoto"]['name'];
        if ($file != '') {
            $old_file = $new_photo_name;
            if (!empty($old_file)) {
                $filedel = PUBPATH . 'media/customer_review/' . $old_file;
                unlink($filedel);
            }
            $this->load->library('upload');
            $config['upload_path'] = 'media/customer_review/';
            $config['allowed_types'] = 'jpg|png|JPEG|jpeg';
            $config['max_size'] = '6000';
            $config['max_width'] = '4000';
            $config['max_height'] = '4000';
            $this->upload->initialize($config);
            $extension = strtolower(substr(strrchr($_FILES['txtPhoto']['name'], '.'), 1));
            $new_photo_name = "CustomerReview_" . time() . "_" . date('Y-m-d') . "." . $extension;
            if (move_uploaded_file($_FILES["txtPhoto"]["tmp_name"], "media/customer_review/" . $new_photo_name))
            {

            } else {
                $sdata['exception'] = "Sorry Photo Does't Upload !";
                $this->session->set_userdata($sdata);
                redirect("tailor_customer_review/edit/".$this->input->post('id', true));
            }
          }
      $data['id'] = $this->input->post('id', true);
      $data['name'] = $this->input->post('name', true);
      $data['description'] = $this->input->post('description', true);
      $data['image_path'] = $new_photo_name;
      $this->db->where('id', $data['id']);
      $this->db->update('tbl_tailor_customer_review', $data);
      $sdata['message'] = "You Successfully Updated Customer Review!";
      $this->session->set_userdata($sdata);
      redirect("tailor_customer_review/index");
    }else{
      $data = array();
      $data['title'] = 'Customer Review';
      $data['heading_msg'] = "Update Customer Review";
      $data['is_show_button'] = "index";
      $data['cr'] = $this->Admin_login->get_customer_review($id);
      $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
      $data['maincontent'] = $this->load->view('tailor_customer_review/edit', $data, true);
      $this->load->view('admin_logins/index', $data);
    }
}

  function delete($id)
  {
      $this->db->delete('tailor_customer_review', array('id' => $id));
      $sdata['message'] = "Customer Review Deleted Successfully !";
      $this->session->set_userdata($sdata);
      redirect("tailor_customer_review/index");
  }

}
