
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admission_Report extends CI_Controller
{
public $notification = array();
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login','Employee','Branch'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        //set timezone
        date_default_timezone_set($user_info[0]->time_zone);
        $employee_id =  $user_info[0]->employee_id;
        //echo $employee_id; die;
        $this->notification = $this->Admin_login->get_notification($employee_id);
    }

    public function index()
    {
      $data = array();
      $user_info = $this->session->userdata('user_info');
      $employee_id =  $user_info[0]->employee_id;
      $user_id =$user_info[0]->id;
      $employee_info=$this->Employee->getEmployeeName($employee_id);

      if($_POST){

        $userid = $this->input->post('user_id', true);
        $intake_years = $this->input->post('intake_years', true);
        $intake_months = $this->input->post('intake_months', true);
        $branch_id = $this->input->post('branch_id', true);
        $intake_years_details = $this->Admin_login->get_intake_years_by_id($intake_years);
        $intake_months_details = $this->Admin_login->get_intake_months_by_id($intake_months);
        $data['intake']=$intake_months_details[0]['name'].'-'.$intake_years_details[0]['name'];
        $data['total_day'] =cal_days_in_month(CAL_GREGORIAN, $intake_months_details[0]['weight'], $intake_years_details[0]['name']);
        $data['title'] = 'Admission Report';
        $data['heading_msg'] = "Admission Report";
        $data['branch'] = $this->Branch->getBranchby_id($branch_id);
        $data['report_to']=$this->Admin_login->get_user_head_of_process_designation($user_id);
        $data['printed_by']=$employee_info;
        $data['rData'] = $this->Admin_login->get_admission_report_data($branch_id,$intake_months,$intake_years);
        if(empty($data['rData'])){
          $sdata['exception'] = "Data not found !";
          $this->session->set_userdata($sdata);
          redirect("admission_report/index");
        }
        $data['org_info'] = $this->Admin_login->getReportHeaderAddress($branch_id);
        $excel = $this->input->post('excel', true);
      //  echo $excel; die;
        if(isset($excel) && $excel != ''){
          $data['excel'] = 1;
          $this->load->view('admission_report/admission_report_table', $data);
          //die;
        }else{
          $data['report'] = $this->load->view('admission_report/admission_report_table', $data, true);
        }
      }

      if(!isset($excel) || $excel == ''){
        $data['title'] = 'Admission Report';
        $data['heading_msg'] = "Admission Report";
        $user_type=$this->Admin_login->get_user_type($user_id);

        if($user_type=="hop")
        {
            $data['branches'] = $this->Admin_login->get_all_branch_list();
        }elseif($user_type=="hoa")
        {
          $data['branches'] = $this->Branch->getBranchby_id($employee_info[0]['branch_id']);
        }else{
           $data['branches'] = $this->Branch->getBranchby_id(0);
        }

        $data['user_ids']=$user_id;
        $data['user_type'] =$user_type;
        $data['intake_years'] =$this->Admin_login->get_intake_years();
        $data['intake_months'] =$this->Admin_login->get_intake_months();
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('admission_report/index', $data, true);
        $this->load->view('admin_logins/index', $data);
      }
    }



}
