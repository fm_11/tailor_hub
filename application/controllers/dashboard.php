<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Dashboard extends CI_Controller
{
    public $notification = array();
    public function __construct()
    {
        parent::__construct();
        $data = array();
        $this->load->helper(array('form', 'url'));
        $this->load->model(array('Timekeeping','Employee','Admin_login', 'common/insert_model', 'common/edit_model', 'common/custom_methods_model'));
        $this->load->library(array('session'));

        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }

        //set timezone
        date_default_timezone_set($user_info[0]->time_zone);

        $employee_id =  $user_info[0]->employee_id;
        //echo $employee_id; die;
        $this->notification = $this->Admin_login->get_notification($employee_id);
    }


    public function index()
    {
        $module_id = $this->session->userdata('module_id');
        if ($module_id == '') {
            $sdata['exception'] = "You do not have any module.";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $module_short_name = $this->session->userdata('module_short_name');
        redirect("dashboard/" . $module_short_name . "_dashboard");
    }

    public function pr_dashboard()
    {
        //echo 'User Real IP - '.$this->Admin_login->getUserIpAddr();
        //die;
        $data = array();
        $date = date('Y-m-d');
        $session_user = $this->session->userdata('user_info');
        $employee_id =  $session_user[0]->employee_id;

        $data['title'] = 'Dashboard';
        $data['heading_msg'] = 'Dashboard';
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('dashboard/pr_index', $data, true);
        $this->load->view('admin_logins/index', $data);
    }

    public function crm_dashboard()
    {
        //echo 'User Real IP - '.$this->Admin_login->getUserIpAddr();
        //die;
        $data = array();
        $date = date('Y-m-d');
        $session_user = $this->session->userdata('user_info');
        $employee_id =  $session_user[0]->employee_id;
        $user_id =  $session_user[0]->id;

        $data['total_lead'] = count($this->db->query("SELECT id FROM leads")->result_array());
        $data['total_my_lead'] = count($this->db->query("SELECT id FROM leads WHERE current_officer = '$user_id'")->result_array());


        $data['title'] = 'Dashboard';
        $data['heading_msg'] = 'Dashboard';
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('dashboard/crm_index', $data, true);
        $this->load->view('admin_logins/index', $data);
    }

    public function hrm_dashboard()
    {
        //echo 'User Real IP - '.$this->Admin_login->getUserIpAddr();
        //die;

        $data = array();
        $date = date('Y-m-d');

        $session_user = $this->session->userdata('user_info');
        $employee_id =  $session_user[0]->employee_id;

        $data['total_employee'] = count($this->db->query("SELECT id FROM tbl_tailor_entry")->result_array());
        $data['total_order'] = count($this->db->query("SELECT id FROM tbl_tailor_order")->result_array());
        $data['total_area'] = count($this->db->query("SELECT id FROM tbl_area")->result_array());
        $data['total_login'] = count($this->db->query("SELECT id FROM tbl_logins WHERE date = '$date'")->result_array());
        $data['total_leave'] = count($this->db->query("SELECT id FROM `tbl_employee_leave_applications` AS t
          WHERE t.`to_date` >= CURDATE()")->result_array());
        $data['total_absentee'] = $data['total_employee'] - ($data['total_login'] + $data['total_leave']);
        $data['late_comers'] = $this->db->query("SELECT l.`date`,l.`login_time`,t.`name`,t.`code`,t.`photo_location` FROM `tbl_logins` AS l
LEFT JOIN `tbl_employee` AS t ON t.`id` = l.`employee_id`
WHERE l.`login_time` > l.`expected_login_time` AND l.date = '$date'
ORDER BY l.`login_time` LIMIT 6")->result_array();

        $data['notices'] = $this->db->query("SELECT n.*,t.name,t.code FROM `tbl_notice` AS n
       LEFT JOIN `tbl_employee` AS t ON t.`id` = n.`employee_id`
       ORDER BY n.`id` desc LIMIT 6")->result_array();


        //for easy login
        $data['todays_att_data'] = $this->db->query("SELECT * FROM tbl_logins WHERE employee_id = '$employee_id' AND date = '$date'")->result_array();


        $data['title'] = 'Dashboard';
        $data['heading_msg'] = 'Dashboard';
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('dashboard/hr_index', $data, true);
        $this->load->view('admin_logins/index', $data);
    }


    public function todays_present_employee()
    {
        $data = array();
        $date = date('Y-m-d');
        $para = array();
        $para['date'] = $date;
        $data['present_data'] = $this->Timekeeping->get_all_employee_timekeeping_list(0, 0, $para);
        $data['title'] = 'Todays Present Employee (' . $date . ')';
        $data['heading_msg'] = 'Todays Present Employee (' . $date . ')';
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('dashboard/todays_present_employee', $data, true);
        $this->load->view('admin_logins/index', $data);
    }

    public function todays_absent_employee()
    {
        $data = array();
        $date = date('Y-m-d');
        $branch_id = '';
        $data['absentee_info'] =  $this->Timekeeping->get_employee_report_absentee($date, $branch_id);
        //  echo '<pre>';
        //  print_r($data['absentee_info']);
        //  die;
        $data['title'] = 'Todays Absent Employee (' . $date . ')';
        $data['heading_msg'] = 'Todays Absent Employee (' . $date . ')';
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('dashboard/todays_absent_employee', $data, true);
        $this->load->view('admin_logins/index', $data);
    }
}
