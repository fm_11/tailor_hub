<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Users extends CI_Controller
{
    public $notification = array();
    public function __construct()
    {
        parent::__construct();
        $data = array();
        $this->load->helper(array('form', 'url'));
        $this->load->model(array('Admin_login','Employee','common/insert_model', 'common/edit_model', 'common/custom_methods_model'));
        $this->load->library(array('session'));

        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $employee_id =  $user_info[0]->employee_id;
        //echo $employee_id; die;
        $this->notification = $this->Admin_login->get_notification($employee_id);
    }

    public function index()
    {

        //$this->checkpermission();
        $data['title'] = 'User Information';
        $data['heading_msg'] = "User Information";
        $data['is_show_button'] = "add";
        $data['users'] = $this->db->query("SELECT u.*,r.role_name,e.name as employee_name,e.code as employee_code FROM tbl_user u
        INNER JOIN user_roles as r on r.id = u.user_role
        INNER JOIN tbl_employee as e on e.id = u.employee_id")->result_array();

        $data['maincontent'] = $this->load->view('users/index', $data, true);
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $this->load->view('admin_logins/index', $data);
    }

    public function add()
    {
        $data = array();

        if ($_POST) {
            $data = array(
                'employee_id' => $this->input->post("employee_id"),
                'user_name' => $this->input->post("user_name"),
                'user_password' => md5($this->input->post("user_password")),
                'is_head_of_process' => $this->input->post("is_head_of_process"),
                'is_head_of_admission' => $this->input->post("is_head_of_admission"),
                'user_role' => $this->input->post("role_id"),
                'is_active' => 1
            );

            $insert = $this->insert_model->add("tbl_user", $data);

            if ($insert == true) {
                $sdata['message'] = "User Information Added";
                $this->session->set_userdata($sdata);
                redirect("users/index");
            } else {
                $sdata['message'] = "User Information could not add";
                $this->session->set_userdata($sdata);
                redirect("users/add");
            }
        } else {
            $data = array();
            $data['title'] = 'Add User Information';
            $data['heading_msg'] = "Add User Information";
            $data['is_show_button'] = "index";
            $data['roles'] = $this->db->query("SELECT * from user_roles")->result_array();
            $cond = array();
            $data['employees'] = $this->Employee->get_all_employees(0, 0, $cond);
            $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
            $data['maincontent'] = $this->load->view('users/add', $data, true);
            $this->load->view('admin_logins/index', $data);
        }
    }

    public function edit($id=null)
    {
        $data = array();

        if ($_POST) {
            $password = $this->input->post("user_password");
            $updated_data = array(
                'employee_id' => $this->input->post("employee_id"),
                'user_name' => $this->input->post("user_name"),
                'is_head_of_process' => $this->input->post("is_head_of_process"),
                'is_head_of_admission' => $this->input->post("is_head_of_admission"),
                'user_role' => $this->input->post("role_id")
            );
            if ($password != '') {
                $updated_data['user_password'] = md5($password);
            }

            $id = $this->input->post("id");
            //$insert=$this->insert_model->add("tbl_user", $data);
            $com_info = $this->edit_model->update_data("id", $id, "tbl_user", $updated_data);
            //  echo $com_info; die;

            if ($com_info == true) {
                $sdata['message'] = "User Information updated";
                $this->session->set_userdata($sdata);
                redirect("users/index");
            } else {
                $sdata['message'] = "User Information could not updated";
                $this->session->set_userdata($sdata);
                redirect("users/index");
            }
        } else {
            $data['title'] = 'Edit User';
            $data['heading_msg'] = "Edit User";
            $data['user_info'] = $this->custom_methods_model->get_where("tbl_user", "id", $id);
            //echo '<pre>';
            //  print_r($data['user_info']);
            //  die;
            $data['roles'] = $this->db->query("SELECT * from user_roles")->result_array();
            //echo '<pre>'; print_r($data['user_info']); die();
            $cond = array();
            $data['employees'] = $this->Employee->get_all_employees(0, 0, $cond);
            $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
            $data['is_show_button'] = "index";
            $data['maincontent'] = $this->load->view('users/edit', $data, true);
            $this->load->view('admin_logins/index', $data);
        }
    }

    public function delete($id)
    {
        $this->edit_model->delete_data("id", $id, "tbl_user");
        $sdata['message'] = "User Information Deleted!";
        $this->session->set_userdata($sdata);
        redirect("users/index");
    }
}
