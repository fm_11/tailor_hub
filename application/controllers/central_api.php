<?php

require APPPATH . '/libraries/REST_Controller.php';

class Central_api extends REST_Controller {

	function __construct($config = 'rest') {
		parent::__construct($config);
		$this->load->model(array('Timekeeping'));
		$this->load->database();
	}

	
	
	function getDateWiseLoginLogout_post(){
		$security_pin = $this->input->post('security_pin');
	
		if ($security_pin == 311556) {
			$date = $this->input->post('date');
			$cond['person_type'] = 'T';
			$cond['date'] = $date;
			$data = array();
			$data['status'] = 'success';
			$data['attendance_data'] = $this->Timekeeping->get_all_employee_timekeeping_list(0, 0, $cond);
			$this->response($data, 200);
		} else {
			$this->response(array('status' => 'fail', 502));
		}
	}
	
	
	function getDateWiseEmployeeAbsentee_post(){
		$security_pin = $this->input->post('security_pin');
	
		if ($security_pin == 311556) {
			$person_type = 'T';
            $date = $this->input->post('date');
            $check_holiday = $this->Timekeeping->check_holiday($date);
            if (empty($check_holiday)) { 	
                $absentee_info = $this->Timekeeping->get_employee_staff_report_absentee($date, $person_type);
				if(empty($absentee_info)){
					$data = array();
					$data['status'] = 'exception';
					$data['message'] = "Data Not Found !";
					$this->response($data, 200);
				}else{
					$data = array();
			        $data['status'] = 'success';
					$data['absentee_info'] = $absentee_info;	
					$this->response($data, 200);					
				}
            } else {
				$data = array();
				$data['status'] = 'exception';
                $data['message'] = $date . ", this date are Holiday !";
                $this->response($data, 200);
            }
		} else {
			    $data = array();
				$data['status'] = 'exception';
                $data['message'] = "Security PIN Error";
                $this->response($data, 200);
		}
	}
	


}