<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Valid_agreements extends CI_Controller
{
   public $notification = array();
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login','Valid_agreement'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->db->query('SET SESSION sql_mode = ""');
        $employee_id =  $user_info[0]->employee_id;
        //echo $employee_id; die;
        $this->notification = $this->Admin_login->get_notification($employee_id);
    }

    public function index()
    {
      $cond = array();
      if($_POST){
        $name = $this->input->post('name', true);
        $sdata['search_name'] = $name;
        $this->session->set_userdata($sdata);
        $cond['name'] = $name;
      }else{
        $name = $this->session->userdata('search_name');
        $cond['name'] = $name;
      }
      $data = array();
      $data['title'] = 'Valid Agreement Information';
      $data['heading_msg'] = "Valid Agreement Information";

      $this->load->library('pagination');
      $config['base_url'] = site_url('valid_agreements/index/');
      $config['per_page'] = 10;
      $config['total_rows'] = count($this->Valid_agreement->get_all_valid_agreement(0, 0, $cond));
      $this->pagination->initialize($config);
      $data['valid_agreement'] = $this->Valid_agreement->get_all_valid_agreement(10, (int)$this->uri->segment(3), $cond);
      $data['counter'] = (int)$this->uri->segment(3);
      $data['is_show_button'] = "add";
      $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
      $data['maincontent'] = $this->load->view('valid_agreements/index', $data, true);
      $this->load->view('admin_logins/index', $data);
    }


    function add()
    {
      if($_POST){
        $data = array();
        $data['name'] = $this->input->post('name', true);
        $count =$this->Valid_agreement->checkifexist_valid_agreement_by_name($this->input->post('name', true));
        if($count)
        {
          $sdata['exception'] = "'".$this->input->post('name', true)."' name already exist !";
          $this->session->set_userdata($sdata);
          redirect("valid_agreements/add");
        }
        if($this->Valid_agreement->add_valid_agreement_info($data)){
            $sdata['message'] = "Data Successfully Saved!";
            $this->session->set_userdata($sdata);
            redirect("valid_agreements/index");
        }else{
            $sdata['exception'] = "Sorry Valid Agreement Data Doesn't Added !";
            $this->session->set_userdata($sdata);
            redirect("valid_agreements/add");
        }
        redirect("valid_agreements/index");
      }else{
        $data = array();
        $data['title'] = 'Add Valid Agreement Information';
        $data['heading_msg'] = "Add Valid Agreement Information";
        $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('valid_agreements/add', $data, true);
        $this->load->view('admin_logins/index', $data);
      }
  }

  function edit($id=null)
  {
    if($_POST){
      $data = array();
      $data['id'] = $this->input->post('id', true);
      $data['name'] = $this->input->post('name', true);
      $count =$this->Valid_agreement->checkifexist_update_valid_agreement_by_name($this->input->post('name', true),$this->input->post('id', true));
      if($count)
      {

        $sdata['exception'] = "'".$this->input->post('name', true)."' name already exist !";
        $this->session->set_userdata($sdata);
        redirect("valid_agreements/index");
      }
      if($this->Valid_agreement->update_valid_agreement_info($data)){
          $sdata['message'] = "Data Successfully Updated!";
          $this->session->set_userdata($sdata);
          redirect("valid_agreements/index");
      }else{
          $sdata['exception'] = "Sorry Valid Agreement Information Doesn't Updated !";
          $this->session->set_userdata($sdata);
          redirect("valid_agreements/index");
      }
    }else{
      $data = array();
      $data['title'] = 'Update Valid Agreement Information';
      $data['heading_msg'] = "Update Valid Agreement Information";
      $data['is_show_button'] = "index";
      $data['valid_agreement'] = $this->Valid_agreement->get_valid_agreement_by_id($id);
      $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
      $data['maincontent'] = $this->load->view('valid_agreements/edit', $data, true);
      $this->load->view('admin_logins/index', $data);
    }
}

  function delete($id)
  {
    if($this->Valid_agreement->delete__valid_agreement_info_by_id($id)){
        $sdata['message'] = "Data Successfully Deleted!";
        $this->session->set_userdata($sdata);
        redirect("valid_agreements/index");
    }else{
        $sdata['exception'] = "Sorry Valid Agreement Information Doesn't Deleted !";
        $this->session->set_userdata($sdata);
        redirect("valid_agreements/index");
    }
  }

}
