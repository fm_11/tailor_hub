<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Branches extends CI_Controller
{
public $notification = array();
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $employee_id =  $user_info[0]->employee_id;
        //echo $employee_id; die;
        $this->notification = $this->Admin_login->get_notification($employee_id);
        $this->db->query('SET SESSION sql_mode = ""');
    }

    public function index()
    {
      $data = array();
      $data['title'] = 'Branch Information';
      $data['heading_msg'] = "Branch Information";
      $data['branches'] = $this->Admin_login->get_branch_info();
      $data['is_show_button'] = "add";
      $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
      $data['maincontent'] = $this->load->view('branches/index', $data, true);
      $this->load->view('admin_logins/index', $data);
    }


    function add()
    {
      if($_POST){
        $data = array();
        $data['name'] = $this->input->post('name', true);
        $data['code'] = $this->input->post('code', true);
        $data['email'] = $this->input->post('email', true);
        $data['mobile'] = $this->input->post('mobile', true);
        $data['country_id'] = $this->input->post('country_id', true);
        $data['time_zone_id'] = $this->input->post('time_zone_id', true);
        $data['address'] = $this->input->post('address', true);
        $data['shift_id'] = $this->input->post('shift_id', true);

        $this->db->insert('tbl_branch', $data);
        $sdata['message'] = "You are Successfully Added Branch Info !";
        $this->session->set_userdata($sdata);
        redirect("branches/index");
      }else{
        $data = array();
        $data['title'] = 'Add Branch Information';
        $data['heading_msg'] = "Add Branch Information";
        $data['is_show_button'] = "index";
        $data['country'] = $this->Admin_login->get_all_country();
        $data['timezones'] = $this->Admin_login->get_all_timezone();
        $data['shifts'] = $this->Admin_login->get_all_shift();
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('branches/add', $data, true);
        $this->load->view('admin_logins/index', $data);
      }
  }

  function edit($id=null)
  {
    if($_POST){
      $data = array();
      $data['id'] = $this->input->post('id', true);
      $data['name'] = $this->input->post('name', true);
      $data['code'] = $this->input->post('code', true);
      $data['email'] = $this->input->post('email', true);
      $data['mobile'] = $this->input->post('mobile', true);
      $data['country_id'] = $this->input->post('country_id', true);
      $data['time_zone_id'] = $this->input->post('time_zone_id', true);
      $data['address'] = $this->input->post('address', true);
      $data['shift_id'] = $this->input->post('shift_id', true);
      $this->db->where('id', $data['id']);
      $this->db->update('tbl_branch', $data);
      $sdata['message'] = "You are Successfully Updated Branch Info !";
      $this->session->set_userdata($sdata);
      redirect("branches/index");
    }else{
      $data = array();
      $data['title'] = 'Update Branch Information';
      $data['heading_msg'] = "Update Branch Information";
      $data['is_show_button'] = "index";
      $data['branch_info'] = $this->Admin_login->get_branch_info_by_unit_id($id);
      $data['country'] = $this->Admin_login->get_all_country();
      $data['timezones'] = $this->Admin_login->get_all_timezone();
      $data['shifts'] = $this->Admin_login->get_all_shift();
      $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
      $data['maincontent'] = $this->load->view('branches/edit', $data, true);
      $this->load->view('admin_logins/index', $data);
    }
}

  function delete($id)
  {
      $this->db->delete('tbl_branch', array('id' => $id));
      $sdata['message'] = "Branch Information Deleted Successfully !";
      $this->session->set_userdata($sdata);
      redirect("branches/index");
  }

}
