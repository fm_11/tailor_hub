<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Report_movement_registers extends CI_Controller
{
public $notification = array();
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login','Timekeeping','Branch'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        //set timezone
        date_default_timezone_set($user_info[0]->time_zone);
        $employee_id =  $user_info[0]->employee_id;
        //echo $employee_id; die;
        $this->notification = $this->Admin_login->get_notification($employee_id);
    }

    public function index()
    {
      $data = array();

      if($_POST){
        //echo '<pre>';
        //print_r($_POST);
        //die;
        $data['employee_id'] = $this->input->post("employee_id");
        $data['from_date'] = $this->input->post("from_date");
        $data['to_date'] = $this->input->post("to_date");

        $pdata = array();
        $pdata['employee_id'] = $this->input->post("employee_id");
        $pdata['from_date'] = $this->input->post("from_date");
        $pdata['to_date'] = $this->input->post("to_date");

        $data['title'] = 'Employee Movement Report';
        $data['heading_msg'] = "Employee Movement Report";
        $data['rData'] = $this->Timekeeping->get_employee_movement_details($pdata);
        //echo '<pre>';
        //print_r($data['rData']);
        //die;


        if(empty($data['rData'])){
          $sdata['exception'] = "Data not found !";
          $this->session->set_userdata($sdata);
          redirect("report_movement_registers/index");
        }
        $branch_id = "";
        $data['org_info'] = $this->Admin_login->getReportHeaderAddress($branch_id);

        $excel = $this->input->post('excel', true);
      //  echo $excel; die;
        if(isset($excel) && $excel != ''){
          $data['excel'] = 1;
          $this->load->view('report_movement_registers/movement_report_table', $data);
          //die;
        }else{
          $data['report'] = $this->load->view('report_movement_registers/movement_report_table', $data, true);
        }
      }

      if(!isset($excel) || $excel == ''){
        $data['title'] = 'Employee Movement Report';
        $session_user = $this->session->userdata('user_info');
        $user_role =  $session_user[0]->user_role;
        if($user_role != 1){
          $data['employees'] = $this->db->query("SELECT id,name,code FROM `tbl_employee` WHERE id = '$employee_id'")->result_array();
        }else{
          $data['employees'] = $this->db->query("SELECT id,name,code FROM `tbl_employee` ORDER BY id")->result_array();
        }
        $data['heading_msg'] = "Employee Movement Report";
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('report_movement_registers/index', $data, true);
        $this->load->view('admin_logins/index', $data);
      }
    }



}
