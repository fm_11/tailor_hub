<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Leave_types extends CI_Controller
{
public $notification = array();
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $employee_id =  $user_info[0]->employee_id;
        //echo $employee_id; die;
        $this->notification = $this->Admin_login->get_notification($employee_id);
    }

    public function index()
    {
      $data = array();
      $data['title'] = 'Leave Type';
      $data['heading_msg'] = "Leave Type";
      $data['leave_types'] = $this->db->query("SELECT * FROM tbl_leave_types")->result_array();
      $data['is_show_button'] = "add";
      $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
      $data['maincontent'] = $this->load->view('leave_types/index', $data, true);
      $this->load->view('admin_logins/index', $data);
    }


    function add()
    {
      if($_POST){
        $data = array();
        $data['name'] = $this->input->post('name', true);
        $data['short_name'] = $this->input->post('short_name', true);
        $data['allocated_days'] = $this->input->post('allocated_days', true);
        $this->db->insert('tbl_leave_types', $data);
        $sdata['message'] = "You are Successfully Added Leave Type.";
        $this->session->set_userdata($sdata);
        redirect("leave_types/index");
      }else{
        $data = array();
        $data['title'] = 'Add Leave Type';
        $data['heading_msg'] = "Add Leave Type";
        $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('leave_types/add', $data, true);
        $this->load->view('admin_logins/index', $data);
      }
  }

  function edit($id=null)
  {
    if($_POST){
      $data = array();
      $data['id'] = $this->input->post('id', true);
      $data['name'] = $this->input->post('name', true);
      $data['short_name'] = $this->input->post('short_name', true);
      $data['allocated_days'] = $this->input->post('allocated_days', true);
      $this->db->where('id', $data['id']);
      $this->db->update('tbl_leave_types', $data);
      $sdata['message'] = "You are Successfully Updated Leave Type.";
      $this->session->set_userdata($sdata);
      redirect("leave_types/index");
    }else{
      $data = array();
      $data['title'] = 'Update Leave Type';
      $data['heading_msg'] = "Update Leave Type";
      $data['is_show_button'] = "index";
      $data['leave_type_info'] = $this->db->query("SELECT * FROM tbl_leave_types WHERE id = '$id'")->row();
      $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
      $data['maincontent'] = $this->load->view('leave_types/edit', $data, true);
      $this->load->view('admin_logins/index', $data);
    }
}

  function delete($id)
  {
      $leave = $this->db->query("SELECT id FROM tbl_employee_leave_applications WHERE leave_type_id = $id")->result_array();
      if(!empty($leave)){
        $sdata['exception'] = "It can't be deleted ! Because it has leave data with it.";
        $this->session->set_userdata($sdata);
        redirect("leave_types/index");
      }
      $this->db->delete('tbl_leave_types', array('id' => $id));
      $sdata['message'] = "Leave Type Deleted Successfully !";
      $this->session->set_userdata($sdata);
      redirect("leave_types/index");
  }

}
