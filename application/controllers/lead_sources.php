<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Lead_sources extends CI_Controller
{
   public $notification = array();
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $employee_id =  $user_info[0]->employee_id;
        //echo $employee_id; die;
        $this->notification = $this->Admin_login->get_notification($employee_id);
        $this->db->query('SET SESSION sql_mode = ""');
    }

    public function index()
    {
      $cond = array();
      if($_POST){
        $name = $this->input->post('name', true);
        $sdata['search_name'] = $name;
        $this->session->set_userdata($sdata);
        $cond['name'] = $name;
      }else{
        $name = $this->session->userdata('search_name');
        $cond['name'] = $name;
      }
      $data = array();
      $data['title'] = 'Lead Sources Information';
      $data['heading_msg'] = "Lead Sources Information";

      $this->load->library('pagination');
      $config['base_url'] = site_url('lead_sources/index/');
      $config['per_page'] = 10;
      $config['total_rows'] = count($this->Admin_login->get_all_lead_sources_info(0, 0, $cond));
      $this->pagination->initialize($config);
      $data['lead_sources'] = $this->Admin_login->get_all_lead_sources_info(10, (int)$this->uri->segment(3), $cond);
      $data['counter'] = (int)$this->uri->segment(3);
      $data['is_show_button'] = "add";
      $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
      $data['maincontent'] = $this->load->view('lead_sources/index', $data, true);
      $this->load->view('admin_logins/index', $data);
    }


    function add()
    {
      if($_POST){
        $data = array();
        $data['name'] = $this->input->post('name', true);
        $data['is_active'] = $this->input->post('is_active', true);
        $count =$this->Admin_login->checkifexist_lead_sources_by_name($this->input->post('name', true));
        if($count)
        {
          $sdata['exception'] = "'".$this->input->post('name', true)."' name already exist !";
          $this->session->set_userdata($sdata);
          redirect("lead_sources/add");
        }
        if($this->Admin_login->add_lead_sources_info($data)){
            $sdata['message'] = "Data Successfully Saved!";
            $this->session->set_userdata($sdata);
            redirect("lead_sources/index");
        }else{
            $sdata['exception'] = "Sorry Lead Sources Data Doesn't Added !";
            $this->session->set_userdata($sdata);
            redirect("lead_sources/add");
        }
        redirect("lead_sources/index");
      }else{
        $data = array();
        $data['title'] = 'Add Lead Sources Information';
        $data['heading_msg'] = "Add Lead Sources Information";
        $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
        $data['maincontent'] = $this->load->view('lead_sources/add', $data, true);
        $this->load->view('admin_logins/index', $data);
      }
  }

  function edit($id=null)
  {
    if($_POST){
      $data = array();
      $data['id'] = $this->input->post('id', true);
      $data['name'] = $this->input->post('name', true);
      $data['is_active'] = $this->input->post('is_active', true);
      $count =$this->Admin_login->checkifexist_update_lead_sources_by_name($this->input->post('name', true),$this->input->post('id', true));
      if($count)
      {
        $sdata['exception'] = "'".$this->input->post('name', true)."' name already exist !";
        $this->session->set_userdata($sdata);
        redirect("lead_sources/index");
      }
      if($this->Admin_login->update_lead_sources_info($data)){
          $sdata['message'] = "Data Successfully Updated!";
          $this->session->set_userdata($sdata);
          redirect("lead_sources/index");
      }else{
          $sdata['exception'] = "Sorry Lead Sources Information Doesn't Updated !";
          $this->session->set_userdata($sdata);
          redirect("lead_sources/index");
      }
    }else{
      $data = array();
      $data['title'] = 'Update Lead Sources Information';
      $data['heading_msg'] = "Update Lead Sources Information";
      $data['is_show_button'] = "index";
      $data['lead_sources'] = $this->Admin_login->get_lead_sources_by_id($id);
      // print_r($data['lead_sources']);
      // die();
      $data['main_menu'] = $this->load->view('admin_logins/' . $this->session->userdata('main_menu_file'), '', true);
      $data['maincontent'] = $this->load->view('lead_sources/edit', $data, true);
      $this->load->view('admin_logins/index', $data);
    }
}
}
