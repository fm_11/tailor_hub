$(function(){
    var ticker = $('.ticker1,.ticker2').easyTicker({
        direction: 'up',
        visible: 4
    });



    $('ul.slimmenu').slimmenu(
        {
            resizeWidth: '1', /* Navigation menu will be collapsed when document width is below this size or equal to it. */
            collapserTitle: 'Main Menu', /* Collapsed menu title. */
            animSpeed: 'medium', /* Speed of the submenu expand and collapse animation. */
            easingEffect: null, /* Easing effect that will be used when expanding and collapsing menu and submenus. */
            indentChildren: false, /* Indentation option for the responsive collapsed submenus. If set to true, all submenus will be indented with the value of the option below. */
            childrenIndenter: '&nbsp;' /* Responsive submenus will be indented with this character according to their level. */
    });

    $("a[rel^='prettyPhoto']").prettyPhoto({
        theme: 'dark_rounded',
        overlay_gallery: false,
        social_tools: false
    });

    $( ".datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });

    startTime();

});


/******* timer ******/
function startTime() {
    var today=new Date();
    var h=today.getHours();
    var m=today.getMinutes();
    var s=today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    document.getElementById('txt').innerHTML = h+":"+m+":"+s;
    var t = setTimeout(function(){startTime()},500);
}
function checkTime(i) {
    if (i<10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}
/**** timer ****/

function LevelWiseLoad(level,mod_html_id){
    var available_departments = "";
    if((level == 'honours') || (level == 'masters') ){
        available_departments += "<option value="+'accounting'+">"+'Accounting'+"</option>";
        available_departments += "<option value="+'bangla'+">"+'Bangla'+"</option>";
        available_departments += "<option value="+'botany'+">"+'Botany'+"</option>";
        available_departments += "<option value="+'chemistry'+">"+'Chemistry'+"</option>";
        available_departments += "<option value="+'economics'+">"+'Economics'+"</option>";
        available_departments += "<option value="+'english'+">"+'English'+"</option>";
        available_departments += "<option value="+'finance_banking'+">"+'Finance & Banking'+"</option>";
        available_departments += "<option value="+'history'+">"+'History'+"</option>";
        available_departments += "<option value="+'islamic_studies'+">"+'Islamic Studies'+"</option>";
        available_departments += "<option value="+'management'+">"+'Management'+"</option>";
        available_departments += "<option value="+'marketing'+">"+'Marketing'+"</option>";
        available_departments += "<option value="+'mathematics'+">"+'Mathematics'+"</option>";
        available_departments += "<option value="+'philosophy'+">"+'Philosophy'+"</option>";
        available_departments += "<option value="+'physics'+">"+'Physics'+"</option>";
        available_departments += "<option value="+'political_science'+">"+'Political Science'+"</option>";
        available_departments += "<option value="+'social_work'+">"+'Social Work'+"</option>";
        available_departments += "<option value="+'sociology'+">"+'Sociology'+"</option>";
        available_departments += "<option value="+'zoology'+">"+'Zoology'+"</option>";
    }else if(level == 'pass'){
        available_departments += "<option value="+'accounting'+">"+'BA'+"</option>";
        available_departments += "<option value="+'accounting'+">"+'Bss'+"</option>";
        available_departments += "<option value="+'accounting'+">"+'B.Sc'+"</option>";
        available_departments += "<option value="+'B.Com'+">"+'B.Com'+"</option>";


    }else if(level == 'hsc'){
        available_departments += "<option value="+'arts'+">"+'Arts'+"</option>";
        available_departments += "<option value="+'commerce'+">"+'Commerce'+"</option>";
        available_departments += "<option value="+'science'+">"+'Science'+"</option>";

    }else if(level == ''){
        available_departments += "<option value= ''"+''+">"+'Please Select Level First'+"</option>";
    }

    if(available_departments){
        $(mod_html_id).html(available_departments);
    }else{
        $(mod_html_id).html("<option value= ''"+''+">"+'Please Select Level First'+"</option>");
    }


    console.log(available_departments);

}

function DelSection(id,section,tr_id){
    //console.log(id);
    //console.log(section);
    //console.log( $("#news_"+tr_id));

    if(confirm("Are you sure?")){

        $.ajax({
            type : 'POST',
            url : '../delete_section.php.htm',
            data : "id="+id+"&section="+section+"&tr_id="+tr_id
        }).done(function( msg ) {
                var succ_msg = "Info Deleted Successfully.";
                //var obj = jQuery.parseJSON( msg );
                $("#"+tr_id).remove();
                $("#not_msg").html(succ_msg);
        })
    }


}