<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		EllisLab Dev Team
 * @copyright		Copyright (c) 2008 - 2014, EllisLab, Inc.
 * @copyright		Copyright (c) 2014 - 2015, British Columbia Institute of Technology (http://bcit.ca/)
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter Application Controller Class
 *
 * This class object is the super class that every library in
 * CodeIgniter will be assigned to.
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		EllisLab Dev Team
 * @link		http://codeigniter.com/user_guide/general/controllers.html
 */
class CI_Controller {

	private static $instance;

	/**
	 * Constructor
	 */
	public function __construct()
	{
		self::$instance =& $this;

		// Assign all the class objects that were instantiated by the
		// bootstrap file (CodeIgniter.php) to local class variables
		// so that CI can run as one big super object.
		foreach (is_loaded() as $var => $class)
		{
			$this->$var =& load_class($class);
		}

		$this->load =& load_class('Loader', 'core');

		$this->load->initialize();

		log_message('debug', "Controller Class Initialized");

		$this->load->model('User_role_wise_privilege', '', TRUE);
		$this->load->library('session');

		$is_access_denied = true;
    $controller = strtolower($this->router->class);
    $action = empty($this->router->method) ? 'index' : $this->router->method;

    $user_info = $this->session->userdata('user_info');
		//echo $this->User_role_wise_privilege->is_globally_allowed_action($controller, $action); die;
    if ($this->User_role_wise_privilege->is_globally_allowed_action($controller, $action)) {
        $is_access_denied = false;
    } else {
        if (!empty($user_info)) {
            $user_role = $user_info[0]->user_role;
            if ($user_role == 1) {
                $is_access_denied = false;
            } else {
                if (!$this->User_role_wise_privilege->check_permission($user_role, $controller, $action)) {
                    $is_access_denied = true;
                } else {
                    $is_access_denied = false;
                }
            }
        } else {
            $sdata = array();
            $sdata['exception'] = "Please Login Valid User !";
            $this->session->set_userdata($sdata);
            redirect('login/index');
        }
    }

    if ($is_access_denied) {
        redirect('login/denied');
    }


	}

	public static function &get_instance()
	{
		return self::$instance;
	}
}
// END Controller class

/* End of file Controller.php */
/* Location: ./system/core/Controller.php */
